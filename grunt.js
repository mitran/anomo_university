module.exports = function(grunt) {
grunt.initConfig({
  ebDeploy: {
    options: {
        application: 'ucla',
	region: 'us-east-1',
	archive: '.tmp/dist.zip',
	profile: 'default'
//	environment:'ucla'
    },
    ucla: {
        options: {
            environment: 'ucla'
        },
        files: [
            { src: ['.ebextensions/*','!README.md','**','.htaccess'] ,expand:true}
//          { cwd:".", src: ['**'], expand: true}
        ]
    },
    demouni: {
        options: {
            environment: 'demouni'
        },
        files: [
            { src: ['.ebextensions/*','!README.md','**','.htaccess'] ,expand:true}
        ]
    }
  }
});
grunt.loadNpmTasks('grunt-eb-deploy');
  grunt.registerTask('default',['ebDeploy']);
};
