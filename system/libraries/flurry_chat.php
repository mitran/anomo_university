<?php

class flurry_chat {

	private $apiUrl = "http://api.flurry.com/eventMetrics/";
	private $apiAccessCode = FLURRY_API_ACCESS_CODE;
	private $apiKeyAndroid = FLURRY_API_KEY_ANDROID;
	private $apiKeyIos = FLURRY_API_KEY_IOS;


	public function getTotalFlurryChatMsgs($eventName,$device = 'ios', $from = '', $to = '', $return = 'int', $metric_name = 'Event', $group_by = '') {
		$arrRes = '';
		$apiKey = $this->apiKeyIos;
		if (isset($device) && 'android' == $device) {
			$apiKey = $this->apiKeyAndroid;
		}
		$from = !empty($from) ? $from : date('Y-m-d');
		$to = !empty($to) ? $to : date('Y-m-d');
		$params = array(
				'apiAccessCode' => $this->apiAccessCode,
				'apiKey' => $apiKey,
				'startDate' => $from,
				'endDate' => $to,
				'eventName' => $eventName,
		);
		if ($group_by != '') {
			$params['groupBy'] = $group_by;
		}
		$urlToCall = $this->apiUrl . $metric_name . '?' . http_build_query($params, null, '&');
		$arrRes = $this->send($urlToCall);
		 
		if ('array' == $return) {
			$aData = array();
			if ('' != $arrRes && isset($arrRes['day']) && 0 < count($arrRes['day'])) {
				if (isset($arrRes['day']['@totalCount'])) {
					$aData[$arrRes['day']['@date']] = $arrRes['day']["@totalCount"];					
				} else {
					foreach ($arrRes['day'] as $v) {
						$aData[$v['@date']] = $v["@totalCount"];
					}
				}
			}
			return $aData;
		} else {
			$itotal = 0;
			if ('' != $arrRes && isset($arrRes['day']) && 0 < count($arrRes['day'])) {
				if (isset($arrRes['day']['@totalCount'])) {
					$itotal = $itotal + $arrRes['day']["@totalCount"];
				} else {
					foreach ($arrRes['day'] as $v) {
						$itotal = $itotal + $v["@totalCount"];
					}
				}
				
			}
			return $itotal;
			
		}
	}


	function send($urlToCall) {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $urlToCall);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FAILONERROR, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

		$response = curl_exec($curl);
		if (!$response) {
			$response = curl_error($curl);
		}
		curl_close($curl);
		return json_decode($response, true);
	}

}

?>