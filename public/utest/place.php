<?php
class Place extends PHPUnit_Framework_TestCase{
	public $base_url = 'http://localhost/wsanomo/index.php/webservice/';
	function testCheckIn(){
		$place_id = 'b1cfb0003ca2f7a5ffbfe74c854388405d6b2d54';
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_check_in';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// 2. checkin this place
    	$url = $this->base_url.'place/check_in/'.$token.'/'.$user_id.'/'.$place_id.'/1';
    	$json = self::send($url);
    	$return = json_decode($json);
    	
    	// 3. verify
    	$this->assertEquals('OK', $return->code);
    	
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	function testPostToPlace(){
		$place_id = 'b1cfb0003ca2f7a5ffbfe74c854388405d6b2d54';
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_post_to_place';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// 2. post to place
    	$url = $this->base_url.'place/post_to_place/'.$token.'/'.$user_id.'/'.$place_id;
    	$content = 'This is content post to place';
    	$post = 'Content='.$content;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	
    	// 3. verify
    	$this->assertEquals('OK', $return->code);
    	$results = $return->post;
    	$this->assertObjectHasAttribute('ID', $results);
    	$this->assertObjectHasAttribute('PlaceID', $results);
    	$this->assertObjectHasAttribute('UserID', $results);
    	$this->assertObjectHasAttribute('Content', $results);
    	$this->assertObjectHasAttribute('Photo', $results);
    	$this->assertObjectHasAttribute('CreatedDate', $results);
    	$this->assertEquals($user_id, $results->UserID);
    	$this->assertEquals($place_id, $results->PlaceID);
    	$this->assertEquals($content, $results->Content);
    	
    	$post_id = $results->ID;
    	
    	// 4. post comment
    	$url = $this->base_url.'place/post_comment/'.$token.'/'.$user_id.'/'.$post_id;
    	$comment = 'This is comment bla bla';
    	$post = 'Content='.$comment;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$results = $return->comment;
    	$this->assertObjectHasAttribute('ID', $results);
    	$this->assertObjectHasAttribute('UserID', $results);
    	$this->assertObjectHasAttribute('Content', $results);
    	$this->assertObjectHasAttribute('UserName', $results);
    	$this->assertObjectHasAttribute('CreatedDate', $results);
    	$this->assertObjectHasAttribute('Gender', $results);
    	$this->assertEquals($user_id, $results->UserID);
    	$this->assertEquals($comment, $results->Content);

    	// delete this post
    	self::send($this->base_url.'place/delete_post/'.$token.'/'.$post_id);
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
    
	function testSearchNearby(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_post_to_place';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// 2. search place
    	$lat = '34';
    	$lon = '-118';
    	$url = $this->base_url.'place/get_nearby_places_v2/'.$token.'/'.$lat.'/'.$lon;
    	$json = self::send($url);
    	$return = json_decode($json);
    	
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('TotalPage', $return);
    	$this->assertObjectHasAttribute('results', $return);
    	
    	$results = $return->results;
    	if (count($results) > 0){
    		foreach ($results as $place){
	    		$this->assertObjectHasAttribute('PlaceID', $place);
	    		$this->assertObjectHasAttribute('Name', $place);
	    		$this->assertObjectHasAttribute('NumberOfUserCheckedIn', $place);
	    		$this->assertObjectHasAttribute('Address', $place);
	    		$this->assertObjectHasAttribute('Phone', $place);
	    		$this->assertObjectHasAttribute('Email', $place);
	    		$this->assertObjectHasAttribute('Website', $place);
	    		$this->assertObjectHasAttribute('Lat', $place);
	    		$this->assertObjectHasAttribute('Lon', $place);
	    		$this->assertObjectHasAttribute('Category', $place);
    		}
    	}
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	function testSearchPlaceByKeyword(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_search_place';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// 2. search place
    	$lat = '34';
    	$lon = '-118';
    	$url = $this->base_url.'place/search_by_keyword_v2/'.$token.'/'.$lat.'/'.$lon;
    	$keyword = 'sushi';
    	$post = 'Keyword='.$keyword;
    	$json = self::send($url);
    	$return = json_decode($json);
    	
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('TotalPage', $return);
    	$this->assertObjectHasAttribute('results', $return);
    	
    	$results = $return->results;
    	if (count($results) > 0){
    		foreach ($results as $place){
	    		$this->assertObjectHasAttribute('PlaceID', $place);
	    		$this->assertObjectHasAttribute('Name', $place);
	    		$this->assertObjectHasAttribute('NumberOfUserCheckedIn', $place);
	    		$this->assertObjectHasAttribute('Address', $place);
	    		$this->assertObjectHasAttribute('Phone', $place);
	    		$this->assertObjectHasAttribute('Email', $place);
	    		$this->assertObjectHasAttribute('Website', $place);
	    		$this->assertObjectHasAttribute('Lat', $place);
	    		$this->assertObjectHasAttribute('Lon', $place);
	    		$this->assertObjectHasAttribute('Category', $place);
    		}
    	}
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	function testGetPlaceDetail(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_search_place';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// 2. search place
    	$lat = '34';
    	$lon = '-118';
    	$url = $this->base_url.'place/search_by_keyword_v2/'.$token.'/'.$lat.'/'.$lon;
    	$keyword = 'sushi';
    	$post = 'Keyword='.$keyword;
    	$json = self::send($url);
    	$return = json_decode($json);
    	
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('TotalPage', $return);
    	$this->assertObjectHasAttribute('results', $return);
    	
    	$results = $return->results;
    	if (count($results) > 0){
    		$placeId = $results[0]->PlaceID;
    	}
    	
    	// get detail
    	if (isset($placeId)){
    		$url = $this->base_url.'place/get_detail_place_v3/'.$token.'/'.$user_id.'/'.$placeId;
    		$json = self::send($url);
    		$return = json_decode($json);
    		
    		// verify
    		$this->assertEquals('OK', $return->code);
    		$results = $return->results;
    		$this->assertEquals($placeId, $results->PlaceID);
    		$this->assertObjectHasAttribute('PlaceID', $results);
    		$this->assertObjectHasAttribute('Name', $results);
    		$this->assertObjectHasAttribute('Address', $results);
    		$this->assertObjectHasAttribute('Lat', $results);
    		$this->assertObjectHasAttribute('Lon', $results);
    		$this->assertObjectHasAttribute('Category', $results);
    		$this->assertObjectHasAttribute('CheckInStatus', $results);
    		$this->assertObjectHasAttribute('ListTag', $results);
    	}
    	
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	function testGetWall(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_search_place';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	$place_id = 'b1cfb0003ca2f7a5ffbfe74c854388405d6b2d54';
    	
    	// 2. wall post
    	$url = $this->base_url.'place/get_wall_post_v2/'.$token.'/'.$place_id;
    	$json = self::send($url);
    	$return = json_decode($json);
    	
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('TotalPage', $return);
    	$this->assertObjectHasAttribute('CurrentPage', $return);
    	$this->assertObjectHasAttribute('WallPostList', $return);
    	
    	$wallPost = $return->WallPostList;
    	if (count($wallPost) > 0){
    		$post = $wallPost[0];
    		$this->assertObjectHasAttribute('UserID', $post);
    		$this->assertObjectHasAttribute('UserName', $post);
    		$this->assertObjectHasAttribute('Avatar', $post);
    		$this->assertObjectHasAttribute('ID', $post);
    		$this->assertObjectHasAttribute('Content', $post);
    		$this->assertObjectHasAttribute('Photo', $post);
    		$this->assertObjectHasAttribute('Photo100', $post);
    		$this->assertObjectHasAttribute('Photo200', $post);
    		$this->assertObjectHasAttribute('Photo300', $post);
    		$this->assertObjectHasAttribute('CreatedDate', $post);
    		$this->assertObjectHasAttribute('TotalComment', $post);
    	}
    	
    	// 3. picture 
    	$url = $this->base_url.'place/get_picture_wall_v2/'.$token.'/'.$place_id;
    	$json = self::send($url);
    	$return = json_decode($json);
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('results', $return);
    	$results = $return->results;
    	$this->assertObjectHasAttribute('CurrentPage', $results);
    	$this->assertObjectHasAttribute('TotalPage', $results);
    	$this->assertObjectHasAttribute('Posts', $results);
    	
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	function testGetAnomoV2(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_search_place';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// 2. get anomo_v2
    	$place_id = 'b1cfb0003ca2f7a5ffbfe74c854388405d6b2d54';
    	$url = $this->base_url.'place/get_anomos_v2/'.$token.'/'.$user_id.'/'.$place_id;
    	$json = self::send($url);
    	$return = json_decode($json);
    	
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('TotalPage', $return);
    	$this->assertObjectHasAttribute('CurrentPage', $return);
    	$this->assertObjectHasAttribute('ListUser', $return);
    	$listUsers = $return->ListUser;
    	if (count($listUsers) > 0){
    		$user = $listUsers[0];
    		$this->assertObjectHasAttribute('UserID', $user);
    		$this->assertObjectHasAttribute('UserName', $user);
    		$this->assertObjectHasAttribute('Avatar', $user);
    		$this->assertObjectHasAttribute('Gender', $user);
    		$this->assertObjectHasAttribute('ProfileStatus', $user);
    		$this->assertObjectHasAttribute('BirthDate', $user);
    		$this->assertObjectHasAttribute('Status', $user);
    		$this->assertObjectHasAttribute('Tags', $user);
    		$this->assertObjectHasAttribute('IsOnline', $user);
    	}
    	
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
    	
	}
	
	function testSendMsg2Place(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_search_place';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// 2. get anomo_v2
    	$place_id = 'b1cfb0003ca2f7a5ffbfe74c854388405d6b2d54';
    	$msg = 'Post msg to place';
    	$url = $this->base_url.'place/send_message/'.$token.'/'.$user_id.'/'.$place_id;
    	$post = 'Content='.$msg;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertEquals($place_id, $return->results->PlaceID);
    	$this->assertEquals($user_id, $return->results->UserID);
    	$this->assertEquals($msg, $return->results->Content);
    	$this->assertObjectHasAttribute('PlaceID', $return->results);
    	$this->assertObjectHasAttribute('UserID', $return->results);
    	$this->assertObjectHasAttribute('Content', $return->results);
    	$this->assertObjectHasAttribute('Photo', $return->results);
    	$this->assertObjectHasAttribute('Photo100', $return->results);
    	$this->assertObjectHasAttribute('Photo200', $return->results);
    	$this->assertObjectHasAttribute('Photo300', $return->results);
    	$this->assertObjectHasAttribute('CreatedDate', $return->results);
    	$this->assertObjectHasAttribute('ID', $return->results);
    	
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
    	
	}
	
	function testGetMsgInPlace(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_search_place';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// . get msg in place
    	$place_id = 'b1cfb0003ca2f7a5ffbfe74c854388405d6b2d54';
    	$url = $this->base_url.'place/get_messages/'.$token.'/'.$user_id.'/'.$place_id;
    	$json = self::send($url);
    	$return = json_decode($json);
    	
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('Messages', $return->results);
    	$this->assertObjectHasAttribute('NumOfChattingUser', $return->results);
    	$msgs = $return->results->Messages;
    	if (count($msgs) > 0){
    		$msg = $msgs[0];
    		$this->assertObjectHasAttribute('ID', $msg);
    		$this->assertObjectHasAttribute('Content', $msg);
    		$this->assertObjectHasAttribute('Photo', $msg);
    		$this->assertObjectHasAttribute('Photo100', $msg);
    		$this->assertObjectHasAttribute('Photo200', $msg);
    		$this->assertObjectHasAttribute('Photo300', $msg);
    		$this->assertObjectHasAttribute('Avatar', $msg);
    		$this->assertObjectHasAttribute('CreatedDate', $msg);
    		$this->assertObjectHasAttribute('UserID', $msg);
    		$this->assertObjectHasAttribute('UserName', $msg);
    		$this->assertObjectHasAttribute('ColorID', $msg);
    	}
    	
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	private function send($urlToCall, $post = ''){
		$curl = curl_init();  
		curl_setopt($curl, CURLOPT_URL, $urlToCall);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  
		curl_setopt($curl, CURLOPT_FAILONERROR, true);  
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  
		if ($post != ''){
			curl_setopt($curl, CURLOPT_POSTFIELDS, $post);  
		}
		
		$response = curl_exec($curl);  
		if (!$response) {  
		    $response = curl_error($curl);  
		}  
		curl_close($curl);  
		return $response;
	}
}
?>