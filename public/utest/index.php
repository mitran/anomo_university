<?php
class Index extends PHPUnit_Framework_TestCase{
	public $base_url = 'http://localhost/wsanomo/index.php/webservice/';
	public function testPushAndPop()
    {
        $stack = array();
        $this->assertEquals(0, count($stack));
 
        array_push($stack, 'foo');
        $this->assertEquals('foo', $stack[count($stack)-1]);
        $this->assertEquals(1, count($stack));
 
        $this->assertEquals('foo', array_pop($stack));
        $this->assertEquals(0, count($stack));
    }
    
    function testRegisterSuccess(){
    	$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_user_register';
    	$fb_id = '99999999955';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	
    	// first time => success 
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertEquals($username, $return->UserName);
    	$this->assertEquals($bdate, $return->BirthDate);
    	$this->assertEquals($gender, $return->Gender);
    	$user_id = $return->UserID;
    	
    	// call again => return Username_existed
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender;
    	$json2 = self::send($url, $post);
    	$return2 = json_decode($json2);
    	// => verify
    	$this->assertEquals('USERNAME_EXISTED', $return2->code);
    	
    	// create new user using same fb id => return FACEBOOK_ID_EXISTED
    	$new_username = $username.'newnewnew';
    	$post_same_fb = 'UserName='.$new_username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json_same_fb = self::send($url, $post_same_fb);
    	$return = json_decode($json_same_fb);
    	// verify
    	$this->assertEquals('FACEBOOK_ID_EXISTED', $return->code);
    	// delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id); 
    }
    
    function testLoginV5(){
    	// 0. Register new account
    	$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_login_v5';
    	$fb_id = '1111111111';
    	$post = 'UserName='.$username.'&BirthDate=1990-09-09&Gender=1&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	
    	// verify  
    	$this->assertEquals('OK', $return->code);
    	$this->assertEquals($username, $return->UserName);
    	$user_id = $return->UserID;
    	
    	// 1. test login by user id with out fb id
    	$login_url = $this->base_url.'user/login_v5/'.$fb_id;
    	$json_login = self::send($login_url);
    	$return_login_v5 = json_decode($json_login);
    	$this->assertEquals('OK', $return_login_v5->code);
    	$this->assertEquals($user_id, $return_login_v5->UserID);
    	$this->assertEquals($username, $return_login_v5->UserName);
    	$this->assertEquals($fb_id, $return_login_v5->FacebookID);
    	
    	// end, delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id); 
    }
    
    function testLoginV6(){
    	// 0. Register new account
    	$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_loginv6';
    	$post = 'UserName='.$username.'&BirthDate=1990-09-09&Gender=1';
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	
    	// verify  
    	$this->assertEquals('OK', $return->code);
    	$this->assertEquals($username, $return->UserName);
    	$user_id = $return->UserID;
    	
    	// 1. test login by user id with out fb id
    	$login_url = $this->base_url.'user/login_v6/'.$user_id;
    	$json_login = self::send($login_url);
    	$return_login_v6 = json_decode($json_login);
    	$this->assertEquals('OK', $return_login_v6->code);
    	$this->assertEquals($user_id, $return_login_v6->UserID);
    	$this->assertEquals($username, $return_login_v6->UserName);
    	$this->assertEquals('', $return_login_v6->FacebookID);
    	
    	// 2. update fb id
    	$fb_id = '1111111111';
    	$login_url_fb = $this->base_url.'user/login_v6/'.$user_id.'/'.$fb_id;
    	$json_login_fb = self::send($login_url_fb);
    	$return_login_fb = json_decode($json_login_fb);
    	$this->assertEquals('OK', $return_login_fb->code);
    	$this->assertEquals($user_id, $return_login_fb->UserID);
    	$this->assertEquals($username, $return_login_fb->UserName);
    	$this->assertEquals($fb_id, $return_login_fb->FacebookID);
    	
    	// end, delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id); 
    }
    
function testGetUserV6(){
		// pre
    	// create new account
    	$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_get_user';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	
    	// first time => success 
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertEquals($username, $return->UserName);
    	$this->assertEquals($fb_id, $return->FacebookID);
    	$this->assertEquals($bdate, $return->BirthDate);
    	$this->assertEquals($gender, $return->Gender);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// get detail user
		$url = $this->base_url.'user/get_user_v6/'.$token.'/'.$user_id.'/'.$user_id;    	
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$results = $return->results;
    	$this->assertEquals('OK', $return->code);
    	$this->assertEquals($username, $results->UserName);
    	$this->assertEquals($bdate, $results->BirthDate);
    	$this->assertEquals($gender, $results->Gender);
    	$this->assertObjectHasAttribute('Avatar', $results);
    	$this->assertObjectHasAttribute('FullPhoto', $results);
    	$this->assertObjectHasAttribute('CoverPicture', $results);
    	$this->assertObjectHasAttribute('AllowSendGiftNotice', $results);
    	$this->assertObjectHasAttribute('College', $results);
    	$this->assertObjectHasAttribute('Major', $results);
    	$this->assertObjectHasAttribute('Looks', $results);
    	$this->assertObjectHasAttribute('Party', $results);
    	$this->assertObjectHasAttribute('Flirty', $results);
    	$this->assertObjectHasAttribute('NeighborhoodID', $results);
    	$this->assertObjectHasAttribute('ListSecretPicture', $results);
    	$this->assertObjectHasAttribute('IsFavorite', $results);
    	$this->assertObjectHasAttribute('IsBlock', $results);
    	$this->assertObjectHasAttribute('IsBlockTarget', $results);
    	$this->assertObjectHasAttribute('Tags', $results);
    	$this->assertObjectHasAttribute('PlaceID', $results);
    	$this->assertObjectHasAttribute('PlaceName', $results);
    	$this->assertObjectHasAttribute('IsOnline', $results);
    	
    	// end delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
    }
    
    function testBlockAndUnBlockUser(){
    	// pre
    	// create 2 user
    	// create user 1
    	$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_get_user1';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// create user 2
    	$username = 'anomo_test_get_user2';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id2 = $return->UserID;
    	
    	// user 1 block user 2
    	$url = $this->base_url.'user/block_msg/'.$token.'/'.$user_id.'/'.$user_id2;
    	$json = self::send($url);
    	$return = json_decode($json);
    	$this->assertEquals('OK', $return->code);
    	
    	// then call ws get user detail v6, should return BLOCK status
    	$url = $this->base_url.'user/get_user_v6/'.$token.'/'.$user_id.'/'.$user_id2;    	
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$this->assertEquals('BLOCK', $return->code);

    	// unblock user
    	$url = $this->base_url.'user/unblock_msg/'.$token.'/'.$user_id.'/'.$user_id2;
    	$json = self::send($url);
    	$return = json_decode($json);
    	$this->assertEquals('OK', $return->code);
    	
    	// delete 2 user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
    	self::send($this->base_url.'user/delete_user/'.$user_id2);
    }
    
	function testUpdateUser(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_update_user';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertEquals($username, $return->UserName);
    	$this->assertEquals($bdate, $return->BirthDate);
    	$this->assertEquals($gender, $return->Gender);
    	// setting values - defaut is 1
    	$this->assertEquals(1, $return->AllowAnomotionNotice);
    	$this->assertEquals(1, $return->AllowCommentPicturePostNotice);
    	$this->assertEquals(1, $return->AllowCommentTextPostNotice);
    	$this->assertEquals(1, $return->AllowChatNotice);
    	
    	// 2. update user
    	$new_user_name = $username.'newnenwenwenwen';
    	$new_bdate = '1980-08-08';
    	$new_gender = '2';
    	$major = 'Accountant';
    	$college = 'College/University';
    	$url = $this->base_url.'user/update_v1/'.$token.'/'.$user_id;
    	$post = 'UserName='.$new_user_name.'&BirthDate='.$new_bdate.'&Gender='.$new_gender.'&Major='.$major.'&College='.$college;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	
    	// then 
    	// verify new value
    	$this->assertEquals('OK', $return->code);
    	$this->assertEquals($new_user_name, $return->UserName);
    	$this->assertEquals($new_bdate, $return->BirthDate);
    	$this->assertEquals($new_gender, $return->Gender);
    	$this->assertEquals($college, $return->College);
    	
    	// end
    	// delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id); 
	}
    
	function testGetListNotificationHistory(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_update_user';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	$url = $this->base_url.'push_notification/get_notification_history/'.$token;
    	$json = self::send($url);
    	$return = json_decode($json);
    	
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$notifyHistories = $return->NotificationHistory;
    	if (count($notifyHistories) > 0){
    		$notify = $notifyHistories[0];
    		$this->assertObjectHasAttribute('ID', $notify);
    		$this->assertObjectHasAttribute('SendUserID', $notify);
    		$this->assertObjectHasAttribute('UserName', $notify);
    		$this->assertObjectHasAttribute('Gender', $notify);
    		$this->assertObjectHasAttribute('Avatar', $notify);
    		$this->assertObjectHasAttribute('Type', $notify);
    		$this->assertObjectHasAttribute('RefID', $notify);
    		$this->assertObjectHasAttribute('CreatedDate', $notify);
    		$this->assertObjectHasAttribute('Total', $notify);
    		$this->assertObjectHasAttribute('IsRead', $notify);
    		$this->assertObjectHasAttribute('AnomotionID', $notify);
    		$this->assertObjectHasAttribute('AnomotionStatus', $notify);
    		$this->assertObjectHasAttribute('Score', $notify);
    		$this->assertObjectHasAttribute('RevealType', $notify);
    		$this->assertObjectHasAttribute('PictureOrder', $notify);
    		$this->assertObjectHasAttribute('ContentType', $notify);
    		$this->assertObjectHasAttribute('ConnectMeter', $notify);
    		$this->assertObjectHasAttribute('IsFavorite', $notify);
    		$this->assertObjectHasAttribute('IsBlock', $notify);
    		$this->assertObjectHasAttribute('IsBlockTarget', $notify);
    		$this->assertObjectHasAttribute('Content', $notify);
    		$this->assertObjectHasAttribute('Photo', $notify);
    		$this->assertObjectHasAttribute('GiftName', $notify);
    		$this->assertObjectHasAttribute('PostID', $notify);
    		$this->assertObjectHasAttribute('PostCreatedDate', $notify);
    		$this->assertObjectHasAttribute('PostContent', $notify);
    		$this->assertObjectHasAttribute('ContentID', $notify);
    		$this->assertObjectHasAttribute('PostOwnerID', $notify);
    		$this->assertObjectHasAttribute('PostOwnerName', $notify);
    	}
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	function testGetAllContact(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_update_user';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// get all contact
    	$url = $this->base_url.'user/get_all_contact/'.$token.'/'.$user_id;
    	$json = self::send($url);
    	$return = json_decode($json);
    	
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('LastUpdateDate', $return);
    	$contacts = $return->Contacts;
    	if (count($contacts) > 0){
    		$contact = $contacts[0];
    		$this->assertObjectHasAttribute('UserID', $contact);
    		$this->assertObjectHasAttribute('UserName', $contact);
    		$this->assertObjectHasAttribute('Email', $contact);
    		$this->assertObjectHasAttribute('ProfileStatus', $contact);
    		$this->assertObjectHasAttribute('Avatar', $contact);
    		$this->assertObjectHasAttribute('BirthDate', $contact);
    		$this->assertObjectHasAttribute('Gender', $contact);
    		$this->assertObjectHasAttribute('FbEmail', $contact);
    		$this->assertObjectHasAttribute('Phone', $contact);
    		$this->assertObjectHasAttribute('RealName', $contact);
    		$this->assertObjectHasAttribute('RealPhoto', $contact);
    		$this->assertObjectHasAttribute('IsFavorite', $contact);
    		$this->assertObjectHasAttribute('Status', $contact);
    		$this->assertObjectHasAttribute('IsBlockTarget', $contact);
    		$this->assertObjectHasAttribute('IsRevealTarget', $contact);
    		$this->assertObjectHasAttribute('ConnectMeter', $contact);
    		$this->assertObjectHasAttribute('Tags', $contact);
    		$this->assertObjectHasAttribute('PlaceID', $contact);
    		$this->assertObjectHasAttribute('PlaceName', $contact);
    		$this->assertObjectHasAttribute('CheckInDate', $contact);
    	}
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	function testSearchUserByLocation(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_update_user';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	$lat = 10.771705139999998;
    	$lon = 106.68336747999999;
    	
    	//. search user 
    	$url = $this->base_url.'user/search_user_v2/'.$token.'/'.$user_id.'/'.$lat.'/'.$lon;
    	$json = self::send($url);
    	$return = json_decode($json);
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('results', $return);
    	$this->assertObjectHasAttribute('ListUser', $return->results);
    	$this->assertObjectHasAttribute('CurrentPage', $return->results);
    	$this->assertObjectHasAttribute('TotalPage', $return->results);
    	$listUsers = $return->results->ListUser;
    	if (count($listUsers) > 0 ){
    		$user = $listUsers[0];
    		$this->assertObjectHasAttribute('UserID', $user);
    		$this->assertObjectHasAttribute('UserName', $user);
    		$this->assertObjectHasAttribute('Avatar', $user);
    		$this->assertObjectHasAttribute('ProfileStatus', $user);
    		$this->assertObjectHasAttribute('Email', $user);
    		$this->assertObjectHasAttribute('NeighborhoodID', $user);
    		$this->assertObjectHasAttribute('BirthDate', $user);
    		$this->assertObjectHasAttribute('Gender', $user);
    		$this->assertObjectHasAttribute('IsOnline', $user);
    		$this->assertObjectHasAttribute('PlaceID', $user);
    		$this->assertObjectHasAttribute('PlaceName', $user);
    		$this->assertObjectHasAttribute('Status', $user);
    		$this->assertObjectHasAttribute('Distance', $user);
    		$this->assertObjectHasAttribute('Tags', $user);
    	}
    	
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	
	function testSearchUserByKeyword(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_update_user';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	$lat = 10.771705139999998;
    	$lon = 106.68336747999999;
    	
    	//. search user 
    	$url = $this->base_url.'user/search_user_v2/'.$token.'/'.$user_id.'/'.$lat.'/'.$lon;
    	$post = 'Keyword='.$username;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('results', $return);
    	$this->assertObjectHasAttribute('ListUser', $return->results);
    	$this->assertObjectHasAttribute('CurrentPage', $return->results);
    	$this->assertObjectHasAttribute('TotalPage', $return->results);
    	$listUsers = $return->results->ListUser;
    	if (count($listUsers) > 0 ){
    		$user = $listUsers[0];
    		$this->assertObjectHasAttribute('UserID', $user);
    		$this->assertObjectHasAttribute('UserName', $user);
    		$this->assertObjectHasAttribute('Avatar', $user);
    		$this->assertObjectHasAttribute('ProfileStatus', $user);
    		$this->assertObjectHasAttribute('Email', $user);
    		$this->assertObjectHasAttribute('NeighborhoodID', $user);
    		$this->assertObjectHasAttribute('BirthDate', $user);
    		$this->assertObjectHasAttribute('Gender', $user);
    		$this->assertObjectHasAttribute('IsOnline', $user);
    		$this->assertObjectHasAttribute('PlaceID', $user);
    		$this->assertObjectHasAttribute('PlaceName', $user);
    		$this->assertObjectHasAttribute('Status', $user);
    		$this->assertObjectHasAttribute('Distance', $user);
    		$this->assertObjectHasAttribute('Tags', $user);
    	}
    	
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	function testGetLatestMsg(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_update_user';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	$url = $this->base_url.'user/get_latest_msg_user/'.$token.'/'.$user_id;
    	$json = self::send($url);
    	$return = json_decode($json);
    	
    	
    	// no msg
    	$this->assertEquals('ZERO_RESULTS', $return->code);
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	private function send($urlToCall, $post = ''){
		$curl = curl_init();  
		curl_setopt($curl, CURLOPT_URL, $urlToCall);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  
		curl_setopt($curl, CURLOPT_FAILONERROR, true);  
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  
		if ($post != ''){
			curl_setopt($curl, CURLOPT_POSTFIELDS, $post);  
		}
		
		$response = curl_exec($curl);  
		if (!$response) {  
		    $response = curl_error($curl);  
		}  
		curl_close($curl);  
		return $response;
	}
 
}
?>