<?php
class Anomotion extends PHPUnit_Framework_TestCase{
	public $base_url = 'http://localhost/wsanomo/index.php/webservice/';
	public $token = '';
	public $user_id1 = '';
	public $user_id2 = '';
	
	/*function testPre(){
		// create 2 account
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_aomotion1';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$this->user_id1 = $return->UserID;
    	$this->token = $return->token;
    	
    	// 
    	$username = 'anomo_test_aomotion2';
    	$fb_id = '9999999999';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$this->user_id2 = $return->UserID;
	}*/
	
	function testRequestAndAnswerGame(){
		// create 2 account
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_aomotion1';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '2';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id1 = $return->UserID;
    	$token = $return->token;
    	
    	// 
    	$username = 'anomo_test_aomotion2';
    	$fb_id = '9999999999';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id2 = $return->UserID;
    
    	// 1. request game
		$url = $this->base_url.'anomotion/request_v2/'.$token.'/'.$user_id1.'/'.$user_id2;
		$json = self::send($url);
    	$return = json_decode($json);
    	$nextQuestion = $return->NextQuestion;
    	$anomotionId = $return->AnomotionID;
    	$questionId = $nextQuestion->QuestionID;
    	$answerId = $nextQuestion->Answer[0]->AnswerID;
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('AnomotionID', $return);
    	$this->assertObjectHasAttribute('NextQuestion', $return);
    	
    	$this->assertObjectHasAttribute('QuestionID', $nextQuestion);
    	$this->assertObjectHasAttribute('QuestionGroupID', $nextQuestion);
    	$this->assertObjectHasAttribute('Content', $nextQuestion);
    	$this->assertObjectHasAttribute('QuestionType', $nextQuestion);
    	$this->assertObjectHasAttribute('Answer', $nextQuestion);
    	
    	// 2. answer game
    	$url = $this->base_url.'anomotion/answer/'.$token.'/'.$user_id1.'/'.$anomotionId.'/'.$questionId.'/'.$answerId;
    	$json = self::send($url);
    	$return = json_decode($json);
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('TargetUserStatus', $return);
    	$this->assertObjectHasAttribute('NextQuestion', $return);
    	
    	$this->assertObjectHasAttribute('QuestionID', $nextQuestion);
    	$this->assertObjectHasAttribute('QuestionGroupID', $nextQuestion);
    	$this->assertObjectHasAttribute('Content', $nextQuestion);
    	$this->assertObjectHasAttribute('QuestionType', $nextQuestion);
    	$this->assertObjectHasAttribute('Answer', $nextQuestion);
    	
    	// 3. get anomotion info
    	$url = $this->base_url.'anomotion/get_anomotion_info/'.$token.'/'.$user_id1.'/'.$anomotionId;
    	$json = self::send($url);
    	$return = json_decode($json);
    	$question = $return->Question;
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('Status', $return);
    	$this->assertObjectHasAttribute('Question', $return);
    	$this->assertObjectHasAttribute('TargetQuestionNumber', $return);
    	$this->assertEquals('ACCEPT', $return->Status);
    	
    	$this->assertObjectHasAttribute('QuestionID', $question);
    	$this->assertObjectHasAttribute('QuestionGroupID', $question);
    	$this->assertObjectHasAttribute('Content', $question);
    	$this->assertObjectHasAttribute('QuestionType', $question);
    	$this->assertObjectHasAttribute('Answer', $question);
    	
    	// end
    	self::send($this->base_url.'user/delete_user/'.$user_id1);
		self::send($this->base_url.'user/delete_user/'.$user_id2);
	}
	
	/*function testEnd(){
		self::send($this->base_url.'user/delete_user/'.$this->user_id1);
		self::send($this->base_url.'user/delete_user/'.$this->user_id2);
	}*/
	
	private function send($urlToCall, $post = ''){
		$curl = curl_init();  
		curl_setopt($curl, CURLOPT_URL, $urlToCall);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  
		curl_setopt($curl, CURLOPT_FAILONERROR, true);  
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  
		if ($post != ''){
			curl_setopt($curl, CURLOPT_POSTFIELDS, $post);  
		}
		
		$response = curl_exec($curl);  
		if (!$response) {  
		    $response = curl_error($curl);  
		}  
		curl_close($curl);  
		return $response;
	}
}
?>