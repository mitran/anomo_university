<?php
class Activity extends PHPUnit_Framework_TestCase{
	public $base_url = 'http://localhost/wsanomo/index.php/webservice/';
	
	function testGetActivities(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_activity';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// 2. get_activities
    	$url = $this->base_url.'activity/get_activities/'.$token;
    	$json = self::send($url);
    	$return = json_decode($json);
    	
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('TotalPage', $return);
    	$this->assertObjectHasAttribute('Page', $return);
    	$this->assertObjectHasAttribute('Activities', $return);
    	$activities = $return->Activities;
    	if (count($activities) > 0){
    		$activity = $activities[0];
    		$this->assertObjectHasAttribute('ActivityID', $activity);
    		$this->assertObjectHasAttribute('FromUserID', $activity);
    		$this->assertObjectHasAttribute('FromUserName', $activity);	
    		$this->assertObjectHasAttribute('Avatar', $activity);
    		$this->assertObjectHasAttribute('Message', $activity);
    		$this->assertObjectHasAttribute('Image', $activity);	
    		$this->assertObjectHasAttribute('RefID', $activity);
    		$this->assertObjectHasAttribute('Type', $activity);
    		$this->assertObjectHasAttribute('ActionType', $activity);
    		$this->assertObjectHasAttribute('PlaceID', $activity);
    		$this->assertObjectHasAttribute('PlaceName', $activity);
    		$this->assertObjectHasAttribute('PlaceIcon', $activity);
    		$this->assertObjectHasAttribute('PlaceAddress', $activity);
    		$this->assertObjectHasAttribute('Lat', $activity);	
    		$this->assertObjectHasAttribute('Lon', $activity);	
    		$this->assertObjectHasAttribute('CreatedDate', $activity);	
    		$this->assertObjectHasAttribute('IsLike', $activity);
    		$this->assertObjectHasAttribute('LatestComment', $activity);
    		$this->assertObjectHasAttribute('LatestLike', $activity);
    		$this->assertObjectHasAttribute('CheckinPlaceID', $activity);
    		$this->assertObjectHasAttribute('CheckinPlaceName', $activity);
    		$this->assertObjectHasAttribute('CheckinStatus', $activity);
    		$this->assertObjectHasAttribute('NeighborhoodID', $activity);
    		$this->assertObjectHasAttribute('NeighborhoodName', $activity);		
    		$this->assertObjectHasAttribute('IsReported', $activity);
    		
    		$contentId = $activity->RefID;
    		$type = $activity->Type;
    		$fromUserName = $activity->FromUserName;
    		$fromUserId = $activity->FromUserID;
    		$msg = $activity->Message;
    		
    		// 3. ws get activity detail
    		$url = $this->base_url.'activity/detail/'.$token.'/'.$contentId.'/'.$type;
    		$json = self::send($url);
    		$return = json_decode($json);
    		
    		// verify
    		$this->assertEquals('OK', $return->code);
    		$this->assertObjectHasAttribute('Activity', $return);
    		$activity = $return->Activity;
    		$this->assertEquals($fromUserId, $activity->FromUserID);
    		$this->assertEquals($fromUserName, $activity->FromUserName);
    		$this->assertEquals($msg, $activity->Message);
    		$this->assertObjectHasAttribute('ActivityID', $activity);
    		$this->assertObjectHasAttribute('FromUserID', $activity);
    		$this->assertObjectHasAttribute('FromUserName', $activity);	
    		$this->assertObjectHasAttribute('Avatar', $activity);
    		$this->assertObjectHasAttribute('Message', $activity);
    		$this->assertObjectHasAttribute('Image', $activity);	
    		$this->assertObjectHasAttribute('RefID', $activity);
    		$this->assertObjectHasAttribute('Type', $activity);
    		$this->assertObjectHasAttribute('ActionType', $activity);
    		$this->assertObjectHasAttribute('PlaceID', $activity);
    		$this->assertObjectHasAttribute('PlaceName', $activity);
    		$this->assertObjectHasAttribute('PlaceIcon', $activity);
    		$this->assertObjectHasAttribute('PlaceAddress', $activity);
    		$this->assertObjectHasAttribute('Lat', $activity);	
    		$this->assertObjectHasAttribute('Lon', $activity);	
    		$this->assertObjectHasAttribute('CreatedDate', $activity);	
    		$this->assertObjectHasAttribute('IsLike', $activity);
    		$this->assertObjectHasAttribute('LatestComment', $activity);
    		$this->assertObjectHasAttribute('LatestLike', $activity);
    		$this->assertObjectHasAttribute('CheckinPlaceID', $activity);
    		$this->assertObjectHasAttribute('CheckinPlaceName', $activity);
    		$this->assertObjectHasAttribute('CheckinStatus', $activity);
    		$this->assertObjectHasAttribute('NeighborhoodID', $activity);
    		$this->assertObjectHasAttribute('NeighborhoodName', $activity);		
    		$this->assertObjectHasAttribute('IsReported', $activity);
    	}
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	function testDetail(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_activity';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// 2. get_activities
    	$url = $this->base_url.'activity/get_activities/'.$token;
    	$json = self::send($url);
    	$return = json_decode($json);
    	
    	// verify
    	$activities = $return->Activities;
    	if (count($activities) > 0){
    		$activity = $activities[0];
    		$contentId = $activity->RefID;
    		$type = $activity->Type;
    		$fromUserName = $activity->FromUserName;
    		$fromUserId = $activity->FromUserID;
    		$msg = $activity->Message;
    		
    		// 3. ws get activity detail
    		$url = $this->base_url.'activity/detail/'.$token.'/'.$contentId.'/'.$type;
    		$json = self::send($url);
    		$return = json_decode($json);
    		
    		// verify
    		$this->assertEquals('OK', $return->code);
    		$this->assertObjectHasAttribute('Activity', $return);
    		$activity = $return->Activity;
    		$this->assertEquals($fromUserId, $activity->FromUserID);
    		$this->assertEquals($fromUserName, $activity->FromUserName);
    		$this->assertEquals($msg, $activity->Message);
    		$this->assertObjectHasAttribute('ActivityID', $activity);
    		$this->assertObjectHasAttribute('FromUserID', $activity);
    		$this->assertObjectHasAttribute('FromUserName', $activity);	
    		$this->assertObjectHasAttribute('Avatar', $activity);
    		$this->assertObjectHasAttribute('Message', $activity);
    		$this->assertObjectHasAttribute('Image', $activity);	
    		$this->assertObjectHasAttribute('RefID', $activity);
    		$this->assertObjectHasAttribute('Type', $activity);
    		$this->assertObjectHasAttribute('ActionType', $activity);
    		$this->assertObjectHasAttribute('PlaceID', $activity);
    		$this->assertObjectHasAttribute('PlaceName', $activity);
    		$this->assertObjectHasAttribute('PlaceIcon', $activity);
    		$this->assertObjectHasAttribute('PlaceAddress', $activity);
    		$this->assertObjectHasAttribute('Lat', $activity);	
    		$this->assertObjectHasAttribute('Lon', $activity);	
    		$this->assertObjectHasAttribute('CreatedDate', $activity);	
    		$this->assertObjectHasAttribute('IsLike', $activity);
    		$this->assertObjectHasAttribute('LatestComment', $activity);
    		$this->assertObjectHasAttribute('LatestLike', $activity);
    		$this->assertObjectHasAttribute('CheckinPlaceID', $activity);
    		$this->assertObjectHasAttribute('CheckinPlaceName', $activity);
    		$this->assertObjectHasAttribute('CheckinStatus', $activity);
    		$this->assertObjectHasAttribute('NeighborhoodID', $activity);
    		$this->assertObjectHasAttribute('NeighborhoodName', $activity);		
    		$this->assertObjectHasAttribute('IsReported', $activity);
    	}
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	function testComment(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_activity';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	$url = $this->base_url.'activity/get_activities/'.$token;
    	$json = self::send($url);
    	$return = json_decode($json);
    	$activities = $return->Activities;
    	if (count($activities) > 0){
    		$activity = $activities[0];
    		$contentId = $activity->RefID;
    		$type = $activity->Type;
    		
    		// 2. comment
    		$content = 'this is comment';
    		$url = $this->base_url.'activity/comment/'.$token.'/'.$contentId.'/'.$type;
    		$post = 'Content='.$content;
    		$json = self::send($url, $post);
    		$return = json_decode($json);

    		// verify
    		$this->assertEquals('OK', $return->code);
    		$this->assertEquals($content, $return->comment->Content);
    		$this->assertObjectHasAttribute('ID', $return->comment);
    		$this->assertObjectHasAttribute('UserID', $return->comment);
    		$this->assertObjectHasAttribute('UserName', $return->comment);
    		$this->assertObjectHasAttribute('Avatar', $return->comment);
    		$this->assertObjectHasAttribute('Content', $return->comment);
    		$this->assertObjectHasAttribute('CreatedDate', $return->comment);
    		$this->assertObjectHasAttribute('PlaceID', $return->comment);
    		$this->assertObjectHasAttribute('PlaceName', $return->comment);
    		$this->assertObjectHasAttribute('CheckinStatus', $return->comment);
    		$this->assertObjectHasAttribute('NeighborhoodID', $return->comment);
    		$this->assertObjectHasAttribute('NeighborhoodName', $return->comment);
    	}
    	
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	function testLike(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_activity';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	$url = $this->base_url.'activity/get_activities/'.$token;
    	$json = self::send($url);
    	$return = json_decode($json);
    	$activities = $return->Activities;
    	if (count($activities) > 0){
    		$activity = $activities[0];
    		$contentId = $activity->RefID;
    		$type = $activity->Type;
    		
    		// 2. like
    		$content = 'this is comment';
    		$url = $this->base_url.'activity/like/'.$token.'/'.$contentId.'/'.$type;
    		$json = self::send($url);
    		$return = json_decode($json);

    		// verify
    		$this->assertEquals('OK', $return->code);
    		$this->assertObjectHasAttribute('NumberOfLike', $return);
    		$this->assertObjectHasAttribute('LatestLikeUserID', $return);
    		$this->assertObjectHasAttribute('LatestLikeUserName', $return);
    		$this->assertObjectHasAttribute('ContentID', $return);
    	}
    	
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	private function send($urlToCall, $post = ''){
		$curl = curl_init();  
		curl_setopt($curl, CURLOPT_URL, $urlToCall);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  
		curl_setopt($curl, CURLOPT_FAILONERROR, true);  
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  
		if ($post != ''){
			curl_setopt($curl, CURLOPT_POSTFIELDS, $post);  
		}
		
		$response = curl_exec($curl);  
		if (!$response) {  
		    $response = curl_error($curl);  
		}  
		curl_close($curl);  
		return $response;
	}
}
?>