<?php
class Neighborhood extends PHPUnit_Framework_TestCase{
	public $base_url = 'http://localhost/wsanomo/index.php/webservice/';
	
	private function send($urlToCall, $post = ''){
		$curl = curl_init();  
		curl_setopt($curl, CURLOPT_URL, $urlToCall);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  
		curl_setopt($curl, CURLOPT_FAILONERROR, true);  
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  
		if ($post != ''){
			curl_setopt($curl, CURLOPT_POSTFIELDS, $post);  
		}
		
		$response = curl_exec($curl);  
		if (!$response) {  
		    $response = curl_error($curl);  
		}  
		curl_close($curl);  
		return $response;
	}
	
	function testGetNbhDetail(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_activity';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	$nbh_id = 191;
    	
    	$url = $this->base_url.'neighborhood/get_detail_v3/'.$token.'/'.$user_id.'/'.$nbh_id;
    	$json = self::send($url);
    	$return = json_decode($json);
    	$this->assertEquals('OK', $return->code);
    	$results = $return->results;
    	$this->assertObjectHasAttribute('PlaceID', $results);
    	$this->assertObjectHasAttribute('Name', $results);
    	$this->assertObjectHasAttribute('Address', $results);
    	$this->assertObjectHasAttribute('Lat', $results);
    	$this->assertObjectHasAttribute('Lon', $results);
    	$this->assertObjectHasAttribute('Category', $results);
    	$this->assertObjectHasAttribute('CheckInStatus', $results);
    	$this->assertObjectHasAttribute('ListTag', $results);
    	
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	function testPostToNbh(){
		$nbh_id = 191;
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_post_to_place';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// 2. post to place
    	$url = $this->base_url.'neighborhood/post_to_neighborhood/'.$token.'/'.$user_id.'/'.$nbh_id;
    	$content = 'This is content post to place';
    	$post = 'Content='.$content;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	
    	// 3. verify
    	$this->assertEquals('OK', $return->code);
    	$results = $return->post;
    	$this->assertObjectHasAttribute('ID', $results);
    	$this->assertObjectHasAttribute('UserID', $results);
    	$this->assertObjectHasAttribute('Content', $results);
    	$this->assertObjectHasAttribute('Photo', $results);
    	$this->assertObjectHasAttribute('CreatedDate', $results);
    	$this->assertEquals($user_id, $results->UserID);
    	$this->assertEquals($content, $results->Content);
    	
    	$post_id = $results->ID;
    	
    	// 4. post comment
    	$url = $this->base_url.'neighborhood/post_comment/'.$token.'/'.$user_id.'/'.$post_id;
    	$comment = 'This is comment bla bla';
    	$post = 'Content='.$comment;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$results = $return->comment;
    	$this->assertObjectHasAttribute('ID', $results);
    	$this->assertObjectHasAttribute('UserID', $results);
    	$this->assertObjectHasAttribute('Content', $results);
    	$this->assertObjectHasAttribute('UserName', $results);
    	$this->assertObjectHasAttribute('CreatedDate', $results);
    	$this->assertObjectHasAttribute('Gender', $results);
    	$this->assertEquals($user_id, $results->UserID);
    	$this->assertEquals($comment, $results->Content);

    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	function testGetWall(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_search_place';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	$nbh_id = 191;
    	
    	// 2. wall post
    	$url = $this->base_url.'neighborhood/get_wall_post_v2/'.$token.'/'.$nbh_id;
    	$json = self::send($url);
    	$return = json_decode($json);
    	
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('TotalPage', $return);
    	$this->assertObjectHasAttribute('CurrentPage', $return);
    	$this->assertObjectHasAttribute('WallPostList', $return);
    	
    	$wallPost = $return->WallPostList;
    	if (count($wallPost) > 0){
    		$post = $wallPost[0];
    		$this->assertObjectHasAttribute('UserID', $post);
    		$this->assertObjectHasAttribute('UserName', $post);
    		$this->assertObjectHasAttribute('Avatar', $post);
    		$this->assertObjectHasAttribute('ID', $post);
    		$this->assertObjectHasAttribute('Content', $post);
    		$this->assertObjectHasAttribute('Photo', $post);
    		$this->assertObjectHasAttribute('Photo100', $post);
    		$this->assertObjectHasAttribute('Photo200', $post);
    		$this->assertObjectHasAttribute('Photo300', $post);
    		$this->assertObjectHasAttribute('CreatedDate', $post);
    		$this->assertObjectHasAttribute('TotalComment', $post);
    	}
    	
    	// 3. picture 
    	$url = $this->base_url.'neighborhood/get_picture_wall_v2/'.$token.'/'.$nbh_id;
    	$json = self::send($url);
    	$return = json_decode($json);
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('results', $return);
    	$results = $return->results;
    	$this->assertObjectHasAttribute('CurrentPage', $results);
    	$this->assertObjectHasAttribute('TotalPage', $results);
    	$this->assertObjectHasAttribute('Posts', $results);
    	
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	function testGetAnomoV2(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_search_place';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// 2. get anomo_v2
    	$nbh_id = 191;
    	$url = $this->base_url.'neighborhood/get_anomos_v2/'.$token.'/'.$user_id.'/'.$nbh_id;
    	$json = self::send($url);
    	$return = json_decode($json);
    	
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('TotalPage', $return);
    	$this->assertObjectHasAttribute('CurrentPage', $return);
    	$this->assertObjectHasAttribute('ListUser', $return);
    	$listUsers = $return->ListUser;
    	if (count($listUsers) > 0){
    		$user = $listUsers[0];
    		$this->assertObjectHasAttribute('UserID', $user);
    		$this->assertObjectHasAttribute('UserName', $user);
    		$this->assertObjectHasAttribute('Avatar', $user);
    		$this->assertObjectHasAttribute('Gender', $user);
    		$this->assertObjectHasAttribute('ProfileStatus', $user);
    		$this->assertObjectHasAttribute('BirthDate', $user);
    		$this->assertObjectHasAttribute('Status', $user);
    		$this->assertObjectHasAttribute('Tags', $user);
    		$this->assertObjectHasAttribute('IsOnline', $user);
    	}
    	
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
    	
	}
}
?>