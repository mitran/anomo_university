<?php
class Gift extends PHPUnit_Framework_TestCase{
	public $base_url = 'http://localhost/wsanomo/index.php/webservice/';
	
	function testGetStuffHistory(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_search_place';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// 2. get stuff history
    	$url = $this->base_url.'user/get_stuff_history/'.$token;
    	$json = self::send($url);
    	$return = json_decode($json);
    	
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('GiftHistory', $return->StuffHistory);
    	$this->assertObjectHasAttribute('CreditHistory', $return->StuffHistory);
    	$giftHistory = $return->StuffHistory->GiftHistory;
    	if (count($giftHistory) > 0){
    		$history = $giftHistory[0];
    		$this->assertObjectHasAttribute('GiftID', $history);
    		$this->assertObjectHasAttribute('Name', $history);
    		$this->assertObjectHasAttribute('SendUserID', $history);
    		$this->assertObjectHasAttribute('ReceiveUserID', $history);
    		$this->assertObjectHasAttribute('CreatedDate', $history);
    		$this->assertObjectHasAttribute('SendUserName', $history);
    		$this->assertObjectHasAttribute('ReceiveUserName', $history);
    	}
    	
    	$creditHistory = $return->StuffHistory->CreditHistory;
    	if (count($creditHistory) > 0){
    		$credit = $creditHistory[0];
    		$this->assertObjectHasAttribute('TransactionID', $credit);
    		$this->assertObjectHasAttribute('CreditPackageID', $credit);
    		$this->assertObjectHasAttribute('Credits', $credit);
    		$this->assertObjectHasAttribute('Price', $credit);
    		$this->assertObjectHasAttribute('CreatedDate', $credit);
    	}
    	
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	function testGetStuffs(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_search_place';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// 2. get stuff history
    	$url = $this->base_url.'user/get_stuffs/'.$token;
    	$json = self::send($url);
    	$return = json_decode($json);
    	
    	// verify
    	$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('ListStuff', $return);
    	$listStuff = $return->ListStuff;
    	if (count($listStuff) > 1){
    		$stuff = $listStuff[0];
    		$this->assertObjectHasAttribute('CategoryName', $stuff);
    		$this->assertObjectHasAttribute('ListGift', $stuff);
    	}
    	
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
	}
	
	function testGetAllGift(){
		// pre
		// 1. create new user
		$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_search_place';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// 2. get stuff history
    	$url = $this->base_url.'user/get_all_gift/'.$token;
    	$json = self::send($url);
    	$return = json_decode($json);
    	
    	// verify
		$this->assertEquals('OK', $return->code);
    	$this->assertObjectHasAttribute('Gifts', $return);
    	$gifts = $return->Gifts;
    	if (count($gifts) > 1){
    		$gift = $gifts[0];
    		$this->assertObjectHasAttribute('CategoryName', $gift);
    		$this->assertObjectHasAttribute('ListGift', $gift);
    		$listGifts = $gift->ListGift[0];
    		$listGift = $listGifts[0];
    		$this->assertObjectHasAttribute('GiftID', $listGift);
    		$this->assertObjectHasAttribute('Name', $listGift);
    		$this->assertObjectHasAttribute('GiftCategoryID', $listGift);
    		$this->assertObjectHasAttribute('Description', $listGift);
    		$this->assertObjectHasAttribute('Credits', $listGift);
    		$this->assertObjectHasAttribute('Photo', $listGift);
    		$this->assertObjectHasAttribute('PricePhoto', $listGift);
    		$this->assertObjectHasAttribute('DescriptionOnBuying', $listGift);
    		$this->assertObjectHasAttribute('Photo194', $listGift);
    		$this->assertObjectHasAttribute('Photo454', $listGift);
    	}
    	
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
    	
	}
	
	function testSendGift(){
		// pre
    	// create 2 user
    	// create user 1
    	$url = $this->base_url.'user/register_v5';
    	$username = 'anomo_test_get_user1';
    	$fb_id = '9999999998';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender.'&FacebookID='.$fb_id;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id = $return->UserID;
    	$token = $return->token;
    	
    	// create user 2
    	$username = 'anomo_test_get_user2';
    	$bdate = '1990-09-09';
    	$gender = '1';
    	$post = 'UserName='.$username.'&BirthDate='.$bdate.'&Gender='.$gender;
    	$json = self::send($url, $post);
    	$return = json_decode($json);
    	$user_id2 = $return->UserID;
    	
    	// user 1 block user 2
    	$gift_id = 1;
    	$url = $this->base_url.'user/send_gift/'.$token.'/'.$user_id2.'/'.$gift_id;
    	$json = self::send($url);
    	$return = json_decode($json);
    	$this->assertEquals('NOT_ENOUGH_CREDITS', $return->code);
//    	$this->assertObjectHasAttribute('Credits', $return);
    	
    	// end - delete this user
    	self::send($this->base_url.'user/delete_user/'.$user_id);
    	self::send($this->base_url.'user/delete_user/'.$user_id2);
	}
	
	private function send($urlToCall, $post = ''){
		$curl = curl_init();  
		curl_setopt($curl, CURLOPT_URL, $urlToCall);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  
		curl_setopt($curl, CURLOPT_FAILONERROR, true);  
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  
		if ($post != ''){
			curl_setopt($curl, CURLOPT_POSTFIELDS, $post);  
		}
		
		$response = curl_exec($curl);  
		if (!$response) {  
		    $response = curl_error($curl);  
		}  
		curl_close($curl);  
		return $response;
	}
}
?>