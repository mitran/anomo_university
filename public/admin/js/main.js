function filter_gift(){
	var action = jQuery('#fm_filter_gift').attr('action');
	var keyword = jQuery('#keyword').val();
	if (keyword != ''){
		action += '/p_keyword/'+keyword;
	}
	var category = jQuery('#category').val();
	if (category != ''){
		action += '/p_category/'+category;
	}
	
	jQuery('#fm_filter_gift').attr('action', action);
}

function filter_promo_code(){
	var action = jQuery('#fm_filter_promo_code').attr('action');
	var status = jQuery('#status').val();
	if (status != ''){
		action += '/p_status/'+status;
	}
	
	jQuery('#fm_filter_promo_code').attr('action', action);
}

function delete_promo_code(id){
	if (confirm('Are you sure you want to delete this record?')){
		jQuery('#promo_'+id).attr('href', base_url+'admin/promo_code/delete/'+id);
	}else{
		return false;
	}
	
}
function change_email_status(user_id){
	jQuery.post(base_url+'admin/user/change_email_status/'+user_id+'/'+jQuery('#status_'+user_id).val(), {}, function(){});
}


function delete_user_account(user_id){
	if (!confirm('Are you sure you want to delete this user ?')){
		return false;
	}else{
		jQuery('#user_del_'+user_id).attr('href', base_url+'admin/user/delete/'+user_id);
	}
}

function filter_picture(){
	var action = jQuery('#fm_filter_user').attr('action');
	var keyword = jQuery('#keyword').val();
	if (keyword != ''){
		action += '/p_keyword/'+keyword;
	}
	jQuery('#fm_filter_user').attr('action', action);
}

function delete_content(content_id, type, divid){
	if (!confirm('Are you sure you want to delete this record ?')){
		return false;
	}else{
		jQuery('#'+divid).attr('href', base_url+'admin/picture/delete_content/'+content_id+'/'+type);
	}
}

function delete_report_content(content_id, type, is_comment, divid, current_offset){
	if (!confirm('Are you sure you want to delete this record ?')){
		return false;
	}else{
		jQuery('#'+divid).attr('href', base_url+'admin/flag/delete_content/'+content_id+'/'+type+'/'+is_comment+'/'+current_offset);
	}
}

function delete_user_status(content_id, divid){
	if (!confirm('Are you sure you want to delete this record ?')){
		return false;
	}else{
		jQuery('#'+divid).attr('href', base_url+'admin/userstatus/delete/'+content_id);
	}
}

function ban_user(user_id, status){
	if (!confirm('Are you sure you want to update status of this user ?')){
		return false;
	}else{
		jQuery('#user_ban_'+user_id).attr('href', base_url+'admin/user/ban_user/'+user_id+'/'+status);
	}
}

function delete_announcement(id){
	if (!confirm('Are you sure you want to delete this record ?')){
		return false;
	}else{
		jQuery('#announcement_del_'+id).attr('href', base_url+'admin/announcement/delete/'+id);
	}
}

function filter_avatar(){
	var action = jQuery('#fm_filter_gift').attr('action');
	var category = jQuery('#gender').val();
	if (category != ''){
		action += '/p_gender/'+category;
	}
	
	jQuery('#fm_filter_gift').attr('action', action);
}

function filter_interests(){
	var action = jQuery('#fm_filter_gift').attr('action');
	var keyword = jQuery('#keyword').val();

	var category = jQuery('#category').val();
	if (category != ''){
		action += '/p_category/'+category;
	}
	
	jQuery('#fm_filter_gift').attr('action', action);
}

function filter_promos() {
    var action = jQuery('#fm_filter_promos').attr('action');
    var vendor = jQuery('#user_id').val();
    if (vendor != '') {
        action += '/p_vendor/' + vendor;
    }
    jQuery('#fm_filter_promos').attr('action', action);
}

function delete_promos(id, divid) {
    if (!confirm('Are you sure you want to delete this record ?')) {
        return false;
    } else {
        jQuery('#' + divid).attr('href', base_url + 'admin/promos/delete/' + id);
    }
}

function delete_promos_fanpost(id, divid) {
    if (!confirm('Are you sure you want to delete this record ?')) {
        return false;
    } else {
        jQuery('#' + divid).attr('href', base_url + 'admin/promos/fanpage_delete/' + id);
    }
}

function openDivPopularHashTag(){
    if(jQuery('#trending_hashtag').is(':visible')) {
        jQuery('#trending_hashtag').hide();
    }else{
        jQuery('#trending_hashtag').show();
    }
}

function fillHashTag(value){
    var hashTagVal = jQuery('#onboarding_hashtag').val();
    if(jQuery('#'+value).is(':checked')){
        if (hashTagVal != ''){
            hashTagVal += ',' + value;
        }else{
            hashTagVal += value;
        }
    }else{
        if (hashTagVal != ''){
            hashTagVal = hashTagVal.replace(','+value, "");
            hashTagVal = hashTagVal.replace(value, "");
        }
    }
    jQuery('#onboarding_hashtag').val(hashTagVal);
}

function post_comment() {
    var comment = jQuery('textarea#txt_comment').val();
    if (comment.length > 0){
        jQuery("#fm_post_comment").submit();
    }else{
        return false;
    }
    
}

function delete_auto_msg(id, divid) {
    if (!confirm('Are you sure you want to delete this record ?')) {
        return false;
    } else {
        jQuery('#' + divid).attr('href', base_url + 'admin/auto_msg/delete/' + id);
    }
}

function verify_account(user_id){
	jQuery.post(base_url+'admin/user/verify_account/'+user_id+'/'+jQuery('#user_verify_'+user_id).val(), {}, function(){});
}

function view_email(){
	var action = base_url + 'admin/export_email/index';
	var from = jQuery('#from').val();
	var to = jQuery('#to').val();
        var type = jQuery('#type').val();
       
	if (from != ''){
            action += '/p_from/'+encodeURIComponent(from);
	}
	if(to != ''){
            action += '/p_to/' + encodeURIComponent(to);
	}
        if(type != ''){
            action += '/p_type/' + type;
	}
	jQuery('#frmExportEmail').attr('action', action);
}

function export_email(){
	var action = base_url + 'admin/export_email/export';
	var from = jQuery('#from').val();
	var to = jQuery('#to').val();
        var type = jQuery('#type').val();
       
	if (from != ''){
            action += '/p_from/'+encodeURIComponent(from);
	}
	if(to != ''){
            action += '/p_to/' + encodeURIComponent(to);
	}
        if(type != ''){
            action += '/p_type/' + type;
	}
	jQuery('#frmExportEmail').attr('action', action);
        jQuery('#frmExportEmail').submit();
}

function sticker_increase_version(cate_id){
	if (!confirm('Are you sure you want to increase version ?')){
		return false;
	}else{
		jQuery('#sticker_cate_'+cate_id).attr('href', base_url+'admin/sticker/increase_version/'+cate_id);
	}
}
