function validMessage() {
    $('#submit').attr("disabled", "disabled");
    var message = jQuery('#message').val();
    var photo = '';
    if (jQuery("#photo").length > 0){
        photo = jQuery('#photo').val();
    }

    if (message == "" && photo == '') {
        alert("The Message field is required.");
        $('#submit').attr("disabled", false);
        return false;
    } else {
        // return true;		
        $.ajax({
            type:"POST",
            url:base_url + "admin/message/getUsersCount",
            data:$('#formMessage').serialize(),
            success:function (data) {
                confirmSubmit(data);
                $('#submit').removeAttr("disabled");
            },
            error:function () {
                alert('Error getting users count, please try again');
                $('#submit').removeAttr("disabled");
            },
            dataType:'json'
        });
    }

    return false;
}

function confirmSubmit(data) {
    var c = confirm("This message will be sent to: " + data + " users");
    if (c == true) {
        $('#requestsubmit').click();
    }
}