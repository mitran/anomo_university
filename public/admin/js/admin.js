function filter_user(){
	var action = jQuery('#fm_filter_user').attr('action');
	var keyword = jQuery('#keyword').val();
	var status = jQuery('#status').val();
	if (keyword != ''){
		action += '/p_keyword/'+encodeURIComponent(keyword);
	}
	if(status != ''){
		action += '/p_status/' + status;
	}
	jQuery('#fm_filter_user').attr('action', action);
}

function change_status_user(user_id){
	jQuery.post(base_url+'admin/index/change_status_user/'+user_id+'/'+jQuery('#status_'+user_id).val(), {}, function(){});
}

function delete_user(user_id){
	if (!confirm('Are you sure you want to delete this user ?')){
		return false;
	}else{
		jQuery('#user_del_'+user_id).attr('href', base_url+'admin/index/delete/'+user_id);
	}
}

function delete_item(divid, url) {
    if (!confirm('Are you sure you want to delete this record ?')) {
        return false;
    } else {
        jQuery('#' + divid).attr('href', url );
    }
}