<?php

class Blocked_User_1day_model extends CI_Model{

    function __construct(){
        parent::__construct();
    }
    
    function add($user_id) {
        $this->db->select('*');
        $this->db->from('blocked_user_1day');
        $this->db->where('UserID', $user_id);
        $this->db->where('CreatedDate', gmdate('Y-m-d'));
        $query = $this->db->get();
        $row = $query->row();
        if (!$row){
            $data = array(
                'UserID'    => $user_id,
                'CreatedDate'   => gmdate('Y-m-d')
            );
            $this->db->insert('blocked_user_1day', $data);
            return $this->db->insert_id();
        }
    }

    function update($cond) {
        $sql = "update blocked_user_1day set IsSent = 1 where ID IN (".implode(',', $cond).")";
        $this->db->query($sql);
    }
    
    function getList($limit = 50){
        $sql = "select t.*, u.UserName
                from (select * from blocked_user_1day where IsSent = 0 limit $limit) as t
                left join user u ON u.UserID = t.UserID
                ";

        $query = $this->db->query($sql);
        return $query->result();
    }
}
?>

