<?php

class Neighborhood_Comment_model extends CI_Model {

    function add($data) {
        $this->db->insert('neighborhoodcomment', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('neighborhoodcomment', $data, $cond);
    }

    function getInfo($comment_id) {
        $this->db->select('pc.ID, pc.NeighborhoodPostID, pc.Content, pc.CreatedDate, pc.UserID, u.UserName, u.Photo as Avatar, u.Gender');
        $this->db->from('neighborhoodcomment pc');
        $this->db->join('user u', 'pc.UserID=u.UserID');
        $this->db->where('pc.ID', $comment_id);
        $query = $this->db->get();
        return $query->row();
    }

    function findById($comment_id) {
        $this->db->select('pp.*');
        $this->db->from('neighborhoodcomment pp');
        $this->db->where('ID', $comment_id);
        $query = $this->db->get();
        return $query->row();
    }

    function getListCommentOf($post_id) {
        $this->db->select('pc.ID, pc.Content, pc.CreatedDate, pc.UserID, u.UserName, u.Photo as Avatar, u.Gender');
        $this->db->from('neighborhoodcomment pc');
        $this->db->join('user u', 'pc.UserID=u.UserID');
        $this->db->where('pc.NeighborhoodPostID', $post_id);
        $this->db->where('pc.IsMark', 0);
        $this->db->where('u.IsBanned !=', 1);
        $query = $this->db->get();
        return $query->result();
    }

    function countCommentOf($post_id) {
        $this->db->from('neighborhoodcomment pc');
        $this->db->where('pc.NeighborhoodPostID', $post_id);
        $this->db->where('pc.IsMark', 0);
        return $this->db->count_all_results();
    }

    function delete($id) {
        $this->db->delete('neighborhoodcomment', array('ID' => $id));
    }

}

?>