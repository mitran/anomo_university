<?php

class Avatar_model extends CI_Model {
    public $cached = false;

    function __construct()
    {
        parent::__construct();
        $CI =& get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
    }
    
    function add($data) {
        $this->db->insert('avatar', $data);
        if ($this->cached) {
            $this->cached->delete('avatar_' . $data['Gender']);
        }
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('avatar', $data, $cond);
    }
    
    function getInfo($id){
        $this->db->select('*');
        $this->db->from('avatar');
        $this->db->where('ID', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    /**get list avatar with paging
     * 
     * @param type $gender
     * @param type $total
     * @param type $from
     * @param type $to
     * @return type
     */
    function getList($gender = 0, &$total = 0, $from = 0, $to = 0){
        $photoUrl = $this->config->item('s3_photo_url');
        $total = 0;
        $where = '';
        $sqlLimit = '';
        if ($gender > 0){
            $where = " where Gender =". $gender;
        }
        if ($from > 0){
            $sqlTotal = "select count(ID) as Total from avatar ".$where;
            $queryTotal = $this->db->query($sqlTotal);
            $rowTotal = $queryTotal->row();
            $total = $rowTotal?$rowTotal->Total:0;
            $sqlLimit = " limit $to, $from";
        }
        $sql = "select ID, Avatar as Name, Gender,
                CONCAT( '$photoUrl',Avatar) AS Avatar
                , CONCAT( '$photoUrl',FullAvatar) AS FullAvatar
                from avatar ".$where . $sqlLimit; 
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    /**return full list avatar
     * 
     * @param type $gender
     * @return type
     */
    function listAvatar($gender = 0){
        if ($this->cached){
            $cached = $this->cached->get('avatar_'.$gender);
            if ($cached){
                return $cached;
            }else{
                $avatar = self::_getAvatars($gender);
                if ($avatar){
                    $this->cached->set('avatar_'.$gender, $avatar);
                }
                return $avatar;
            }
        }else{
            return self::_getAvatars($gender);
        }
    }
    
    private function _getAvatars($gender){
        $photoUrl = $this->config->item('s3_photo_url');
        $sql = "select Avatar as AvatarName
                    , FullAvatar as FullAvatarName
                    , CONCAT( '$photoUrl',Avatar) AS Avatar
                    , CONCAT( '$photoUrl',FullAvatar) AS FullAvatar
                from avatar ";
        if ($gender > 0){
            $sql .= " where Gender =". $gender;
        }
        $query = $this->db->query($sql);
        return $query->result();
    }

}

?>