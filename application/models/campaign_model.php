<?php

class Campaign_model extends CI_Model{
    public $cached = false;

    function __construct(){
        parent::__construct();
        $CI =& get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
        
    }
    function add($data) {
        $this->db->insert('campaign', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('campaign', $data, $cond);
    }
    
    function delete($id){
        $this->db->delete('campaign', array('ID' => $id));                
    }
    
    function getActiveCampaignByCelebrity($celebrityid, $signup_date) {
        //HotFix - keep tracking CAU without enddate
        /*
        $sql = "
            SELECT * FROM campaign 
            WHERE CelebrityID = $celebrityid 
            AND DATE_FORMAT(StartDate, '%Y-%m-%d %H:%i:%s') <= '$signup_date'
            AND DATE_FORMAT(ExpiredDate, '%Y-%m-%d %H:%i:%s') >= '$signup_date' 
            AND DATE_FORMAT(ExpiredDate, '%Y-%m-%d %H:%i:%s') >= '".gmdate('Y-m-d H:i:s')."' 
            AND status = 1 
            ORDER BY CreatedDate desc
            LIMIT 1
        ";
        */
        $sql = "
            SELECT * FROM campaign 
            WHERE CelebrityID = $celebrityid 
            AND DATE_FORMAT(StartDate, '%Y-%m-%d %H:%i:%s') <= '$signup_date'             
            AND DATE_FORMAT(StartDate, '%Y-%m-%d %H:%i:%s') <= '".gmdate('Y-m-d H:i:s')."' 
            AND status = 1 
            ORDER BY CreatedDate desc
            LIMIT 1
        ";
        $result = $this->db->query($sql);
        return $result->row();        
    }
    
    function getInfo($id){
        $sql = "select c.*
                    ,u.UserName
                    ,u.Photo
                    ,(select count(UserID) from campaigncau where CelebrityID != -1 and CampaignID = c.CampaignID) as 'TotalCAU'
                    from campaign c
                    inner join user u on c.CelebrityID = u.UserID
                    where CampaignID=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    function getListCampaignWithPaging(&$total = 0, $from = 0, $to = 0) {
        $total = 0;
      
        $sqlTotal = "select count(CampaignID) as Total from campaign " ;
        $queryTotal = $this->db->query($sqlTotal);
        $rowTotal = $queryTotal->row();
        $total = $rowTotal ? $rowTotal->Total : 0;
        if ($total > 0){
            $sql = "select c.*
                    ,u.UserName
                    ,u.Photo
                    ,(select count(UserID) from campaigncau where CelebrityID != -1 and CampaignID = c.CampaignID) as 'TotalCAU'
                    from campaign c
                    inner join user u on c.CelebrityID = u.UserID                       
                    limit $to, $from ";
            $query = $this->db->query($sql);
            return $query->result();
        }
        return false;
    }
    
    function getDailyReport($CampaignID,$from,$to) {      
        /*
        $sql = "        
        select Date,
        if(dailycau.TotalCau is not null,dailycau.TotalCau,0) as 'DailyCAU',
        if(dailyactive.TotalActive is not null,dailyactive.TotalActive,0) as 'DailyActive'
        from 
        (select * from campaign where CampaignID = $CampaignID) cam
        inner join 
        (select adddate(date('$from'),t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) Date from
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4
        ) date_range on date_range.date between date('$from') and date('$to')
        left join 
        (
        	select CampaignID,date(cc.CreatedDate) as 'CreatedDate',count(cc.UserID) as 'TotalCau' 
        	from campaigncau cc 
            where CelebrityID != -1  	
        	group by  CampaignID,date(cc.CreatedDate)
        ) dailycau
        ON cam.CampaignID = dailycau.CampaignID and date_range.Date = dailycau.CreatedDate
        left join 
        (	select CampaignID,ccd.CreatedDate,count(ccd.UserID) as 'TotalActive'
        	from campaigncau cc 
        	left join campaigncaudaily ccd on cc.UserID = ccd.UserID 
            where cc.CelebrityID != -1
        	group by  CampaignID,ccd.CreatedDate
        ) dailyactive
        ON cam.CampaignID = dailyactive.CampaignID and date_range.Date = dailyactive.CreatedDate
        group by date
        ";
        */
        $sql = "
        select Date,
        if(dailycau.TotalCau is not null,dailycau.TotalCau,0) as 'DailyCAU',
		if(dailycauafter.TotalCau is not null,dailycauafter.TotalCau,0) as 'DailyCAUAfter',
		if(cauactive.CAUActive is not null,cauactive.CAUActive,0) as 'CAUActive',
        if(totalactive.TotalCAUActive is not null,totalactive.TotalCAUActive,0) as 'TotalCAUActive'
        from 
        (select * from campaign where CampaignID = $CampaignID) cam
        inner join 
        (select adddate(date('$from'),t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) Date from
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4
        ) date_range on date_range.date between date('$from') and date('$to')
        left join 
        (
        	select CampaignID,date(cc.CreatedDate) as 'CreatedDate',count(cc.UserID) as 'TotalCau' 
        	from campaigncau cc 
            where CelebrityID != -1  							
        	group by  CampaignID,date(cc.CreatedDate)
        ) dailycau
        ON cam.CampaignID = dailycau.CampaignID and date_range.Date = dailycau.CreatedDate and dailycau.CreatedDate BETWEEN cam.StartDate and cam.ExpiredDate 
				left join 
        (
        	select CampaignID,date(cc.CreatedDate) as 'CreatedDate',count(cc.UserID) as 'TotalCau' 
        	from campaigncau cc 
            where CelebrityID != -1  							
        	group by  CampaignID,date(cc.CreatedDate)
        ) dailycauafter
        ON cam.CampaignID = dailycauafter.CampaignID and date_range.Date = dailycauafter.CreatedDate and dailycauafter.CreatedDate > cam.ExpiredDate
				left join 
        (	select cc.CampaignID,cc.CreatedDate as 'CAUCreatedDate',ccd.CreatedDate,count(ccd.UserID) as 'CAUActive'
        	from campaigncau cc 
					inner join campaign on cc.CampaignID = campaign.CampaignID
        	left join campaigncaudaily ccd on cc.UserID = ccd.UserID 
          where cc.CelebrityID != -1
					and cc.CreatedDate BETWEEN campaign.StartDate and campaign.ExpiredDate
        	group by  CampaignID,ccd.CreatedDate
        ) cauactive
        ON cam.CampaignID = cauactive.CampaignID and date_range.Date = cauactive.CreatedDate 
        left join 
        (	select CampaignID,ccd.CreatedDate,count(ccd.UserID) as 'TotalCAUActive'
        	from campaigncau cc 
        	left join campaigncaudaily ccd on cc.UserID = ccd.UserID 
            where cc.CelebrityID != -1
        	group by  CampaignID,ccd.CreatedDate
        ) totalactive
        ON cam.CampaignID = totalactive.CampaignID and date_range.Date = totalactive.CreatedDate
        group by date
        ";        
        $query = $this->db->query($sql);
        return $query->result();
        
    }
}
?>

