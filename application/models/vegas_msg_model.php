<?php
class Vegas_Msg_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    
    function addVegasMsg($data){
        $this->db->insert('vegas_msg', $data);
        return $this->db->insert_id();
    }
    
    function addMultiMsgTracking($data){
        $sql = "insert into vegas_msg_tracking (`MessageID`, `UserID`, `Message`, `Photo`, `TapAction`, `TapParam`, `Status`, `CreatedDate`)
            value ". implode(',', $data);
        $this->db->query($sql);    
    }
    
    function updateVegagMsg($data, $cond){
        $this->db->update('vegas_msg', $data, $cond);
    }
    
    function getListByCondition($cond, &$total, $offset = 0, $limit = 0) {
        $sqlTotal = "select count(MessageID) as Total from vegas_msg";
        $totalQuery = $this->db->query($sqlTotal);
        $totalRow = $totalQuery->row();
        $total = $totalRow?$totalRow->Total:0;
        
        $sql = "select * from vegas_msg order by CreatedDate DESC
                limit $limit, $offset";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function getMessageTracking($iLimit = 100) {
        $photo_url = $this->config->item('s3_photo_url');
        $sql = "select mt.MessageID, mt.UserID, mt.Message, u.UserName, d.Type, mt.TapAction, mt.TapParam
                ,IF(u.Photo != '', concat('$photo_url', u.Photo), '') as Avatar
                ,IF(mt.Photo != '', concat('$photo_url', mt.Photo), '') as Photo
                from (select * from vegas_msg_tracking where Status = 0 limit $iLimit) as mt
                left join user u ON u.UserID = mt.UserID
                left join devices d ON d.UserID = mt.UserID AND d.Status = 'active'
                ";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function updateStatusTrackings($aData, $conds) {
        $conditionString = "";
        $counterMaxLength = sizeof($conds) - 1;
        $counter = 0;
        foreach ($conds as $cond) {
            $conditionString .= "(MessageID = " . $cond["MessageID"] . " and UserID = " . $cond["UserID"] . ")";
            if ($counter < $counterMaxLength) {
                $conditionString .= " or ";
                $counter++;
            }
        }

        $this->db->update('vegas_msg_tracking', $aData, $conditionString);

        //Get error of DB
        if ($this->db->_error_message()) {
            log_message('error', $this->db->_error_message());
            return false;
        } else {
            return true;
        }
    }
    
    function updateMessageStatus() {
        $sql = "update vegas_msg
                Set Status = 1
                Where Status=0 AND MessageID NOT IN (select MessageID from vegas_msg_tracking where status=0 group by MessageID)
                ";
        $this->db->query($sql);
    }
    
}
?>
