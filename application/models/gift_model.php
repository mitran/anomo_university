<?php

class Gift_model extends CI_Model {

    function add($data) {
        $this->db->insert('gift', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('gift', $data, $cond);
    }

    function getListByCondition($cond, $from, $to, $getSumNumber) {
        $this->db->select('*');
        $this->db->from('gift');
        if (count($cond) > 0) {
            if (isset($cond['keyword'])) {
                $q = "(`Name` LIKE '%{$cond['keyword']}%' OR `Description` LIKE '%{$cond['keyword']}%' OR `DescriptionOnBuying` LIKE '%{$cond['keyword']}%' )";
                $this->db->where($q);
            }
            if (isset($cond['category'])) {
                $this->db->where('GiftCategoryID', $cond['category']);
            }
            if (isset($cond['sort'])) {
                switch ($cond['sort']) {
                    case 'id-asc':
                        $this->db->order_by('GiftID', 'asc');
                        break;
                    case 'id-desc':
                        $this->db->order_by('GiftID', 'desc');
                        break;
                    case 'name-asc':
                        $this->db->order_by('Name', 'asc');
                        break;
                    case 'name-desc':
                        $this->db->order_by('Name', 'desc');
                        break;
                    case 'credits-asc':
                        $this->db->order_by('Credits', 'asc');
                        break;
                    case 'credits-desc':
                        $this->db->order_by('Credits', 'desc');
                        break;
                }
            }
        }
        if ($getSumNumber) {
            $query = $this->db->get();
            return $query->num_rows();
        } else {
            $this->db->limit($from, $to);
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? $query->result() : null;
        }
    }

    function checkExistedName($name, $gift_id) {
        $this->db->select('GiftID');
        $this->db->from('gift');
        $this->db->where('Name', $name);
        if ($gift_id > 0) {
            $this->db->where('GiftID !=', $gift_id);
        }
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? false : true;
    }

}

?>