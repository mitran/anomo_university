<?php

class Neighborhood_model extends CI_Model {

    public $cached = false;

    function __construct() {
        parent::__construct();
        $CI = & get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
        $this->db_slave = $this->load->database('slave', TRUE);
    }

    function find_by_coordinates($lat = 0, $lon = 0) {
        $sql = "CALL sp_getNeighborhood(" . $lat . "," . $lon . ")";
        $query = $this->db->query($sql);
        $row = $query->row();
        $query->next_result();
        return $row;
    }

    function add($data) {
//		$this->db->insert('neighborhood', $data);
//		return $this->db->insert_id();
        $sql = "INSERT INTO `neighborhood` (`STATE`, `COUNTY`, `CITY`, `NAME`, `REGIONID`, `SHAPE`) VALUES ('{$data['STATE']}', '{$data['COUNTY']}', '{$data['CITY']}', '{$data['NAME']}', '', GeomFromText( 'POINT({$data['Lat']} {$data['Lon']})'))";
        $this->db->query($sql);
        return $this->db->insert_id();
    }

    function getInfo($id) {
        if ($this->cached) {
            $cache = $this->cached->get('neighborhood' . $id);
            if ($cache) {
                return $cache;
            } else {
                $sql = "select *, X(Centroid(SHAPE)) as Lat, Y(Centroid(SHAPE)) as Lon,
                                        if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) AS NAME
					From neighborhood nbh
					Where OGR_FID=$id
					";
                $query = $this->db_slave->query($sql);
                $result = $query->row();
                $query->free_result();

                $this->cached->set('neighborhood' . $id, $result);
                return $result;
            }
        } else {
            $sql = "select *, X(Centroid(SHAPE)) as Lat, Y(Centroid(SHAPE)) as Lon,
					if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) AS NAME
                                        From neighborhood nbh
					Where OGR_FID=$id
					";
            $query = $this->db_slave->query($sql);
            $row = $query->row();
            $query->free_result();
            return $row;
        }
    }

    function getByCountryName($name) {
        $this->db_slave->select('*');
        $this->db_slave->from('neighborhood');
        $this->db_slave->where('COUNTY', $name);
        $query = $this->db_slave->get();
        $row = $query->row();
        $query->free_result();
        return $row;
    }

    function getCheckInStatus($user_id, $nbh_id) {
        $this->db_slave->select('Status');
        $this->db_slave->from('neighborhoodcheckin');
        $this->db_slave->where('UserID', $user_id);
        $this->db_slave->where('NeighborhoodID', $nbh_id);
        $this->db_slave->where('CheckOutDate >', gmdate('Y-m-d H:i:s'));
        $query = $this->db_slave->get();
        $row = $query->row();
        $query->free_result();
        return ($row) ? $row->Status : 0;
    }

    function checkExistPlace($place_id) {
        $this->db_slave->select('count(OGR_FID) as num');
        $this->db_slave->from('neighborhood');
        $this->db_slave->where('OGR_FID', $place_id);
        $query = $this->db_slave->get();
        $row = $query->row();
        $query->free_result();
        return (int) $row->num;
    }

    function getListPostOf($nbh_id, $type = 'text', $limit = 0) {
        $this->db_slave->select('pp.UserID, u.UserName, u.Photo as Avatar, u.Gender, pp.ID, pp.Content, pp.Photo, pp.CreatedDate');
        $this->db_slave->from('neighborhoodpost pp');
        $this->db_slave->join('user u', 'u.UserID=pp.UserID');
        $this->db_slave->where('pp.NeighborhoodID', $nbh_id);
        $this->db_slave->where('pp.IsMark', 0);
        if ($type == 'text') {
            $this->db_slave->where('pp.Content !=', "");
            $this->db_slave->where('pp.Photo', "");
        }
        if ($type == 'picture') {
            $this->db_slave->where('pp.Photo !=', "");
        }
        $this->db_slave->order_by('pp.CreatedDate', 'DESC');
        if ($limit > 0) {
            $this->db_slave->limit($limit);
        }
        $query = $this->db_slave->get();
        $retVal = $query->result();
        $query->free_result();
        return $query->result();
    }

    /**
     * return list user checkin of neighborhood
     *
     * @param string $nbh_id
     * @param string $user_id
     * @param $type
     * 		'current': list user checked in /spectate
     * 		'past': list user logout
     * @return array
     */
    function getListUserOf($nbh_id, $user_id = 0, $type = 'current', &$total = '', $offset = '', $limit = 0) {
        $this->db_slave->select('u.UserID, u.UserName, u.Photo as Avatar, u.Gender, u.ProfileStatus, u.Email, u.BirthDate, uc.Status, uc.CheckInDate, u.FacebookID, u.LastActivity');
        $this->db_slave->from('neighborhoodcheckin uc');
        $this->db_slave->join('user u', 'uc.UserID=u.UserID');
        $this->db_slave->where('uc.NeighborhoodID', $nbh_id);
        $this->db_slave->where('u.IsBanned !=', 1);
        if ($type == 'past') {
            $this->db_slave->where('uc.Status ', 2);
        } else {
            $this->db_slave->where('uc.Status ', 1); // check in /spectator
        }
        if ($user_id > 0) {
            $this->db_slave->where('uc.UserID !=', $user_id);
        }
        $this->db_slave->group_by('uc.UserID');
        $this->db_slave->order_by('uc.CheckInDate', 'DESC');
        $query = $this->db_slave->get();
        $last_query = $this->db_slave->last_query();

        $totalResult = $this->db_slave->query($last_query);
        $total = $totalResult->num_rows();

        if ($limit > 0) {
            $last_query .= " limit $offset, $limit";
        }

        $query1 = $this->db_slave->query($last_query);

        $retVal = $query1->result();

        $query->free_result();
        $query1->free_result();

        return $retVal;
    }

    function checkExistCheckin($nbh_id, $user_id, $status = 0) {
        $this->db_slave->select('count(ID) as num');
        $this->db_slave->from('neighborhoodcheckin');
        $this->db_slave->where('UserID', $user_id);
        $this->db_slave->where('NeighborhoodID', $nbh_id);
        if ($status > 0) {
            $this->db_slave->where('Status', $status);
        }
        $query = $this->db_slave->get();
        $row = $query->row()->num;
        $query->free_result();
        return (int) $row;
    }

    function getListUserOfV2($place_id, $user_id = 0, $type = 'current', $limit = 0) {
        $this->db_slave->select('u.Photo as Avatar');
        $this->db_slave->from('neighborhoodcheckin uc');
        $this->db_slave->join('user u', 'uc.UserID=u.UserID');
        $this->db_slave->where('uc.NeighborhoodID', $place_id);
        if ($type == 'past') {
            $this->db_slave->where('uc.Status', 2);
        } else {
            $this->db_slave->where('uc.Status ', 1); // check in /spectator
        }
        if ($user_id > 0) {
            $this->db->where('uc.UserID !=', $user_id);
        }
        if ($limit > 0) {
            $this->db_slave->limit($limit);
        }
        $query = $this->db_slave->get();
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    function getNumberUserCheckedIn($place_id) {
        $this->db_slave->select('count(ID) as num');
        $this->db_slave->from('neighborhoodcheckin');
        $this->db_slave->where('NeighborhoodID', $place_id);
        $this->db_slave->where('Status', 1);
        $query = $this->db_slave->get();
        $retVal = $query->row();
        $query->free_result();
        return (int) $retVal->num;
    }

    function getListTagOf($place_id, $type = '', $limit = 0) {
        $this->db_slave->select('ut.TagID, t.Name, count(ut.TagID) as Count');
        $this->db_slave->from('neighborhoodcheckin uc');
        $this->db_slave->join('usertag ut', 'uc.UserID=ut.UserID');
        $this->db_slave->join('tag t', 'ut.TagID=t.TagID');
        $this->db_slave->where('uc.NeighborhoodID', $place_id);
        if ($type == 'current') {
            $this->db_slave->where('uc.Status', 1); // check in /spectator
        }
        if ($type == 'past') {
            $this->db_slave->where('uc.Status', 2);
        }
        $this->db_slave->group_by('ut.TagID');
        // ANOMO-5917
        if ($limit > 0) {
            $this->db_slave->limit($limit);
        }
        $query = $this->db_slave->get();
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    function getTotalPostOf($nbh_id, $type = 'text') {
        $this->db_slave->select('count(ID) as num');
        $this->db_slave->from('neighborhoodpost');
        $this->db_slave->where('NeighborhoodID', $nbh_id);
        if ($type == 'text') {
            $this->db_slave->where('Content !=', "");
            $this->db_slave->where('Photo', "");
        } else {
            $this->db_slave->where('Photo !=', "");
        }
        $query = $this->db_slave->get();
        $retVal = $query->row();
        $query->free_result();
        return (int) $retVal->num;
    }

    function sp_location($data, $is_age_less = 0) {
        $return = array();
        // ANOMO-11682 - No location displayed for all celeb acts please
        if ($is_age_less == 1){
            $nbhID = NBH_EARTH;
            $updateData = array(
                'Lat' => 0,
                'Lon' => 0,
                'NeighborhoodID' => $nbhID,
                'LastActivity' => gmdate('Y-m-d H:i:s')
            );
            $ret = User_model::updateUser($updateData, array('UserID' => $data['UserID']));
            if ($ret){
                $return['Return'] = $nbhID;
            }
            return $return;
        }
        
        // ANOMO-8358 -  It should display "city, country" when city and state are the same 
        if (!empty($data['City']) && $data['City'] == $data['State']){
            $data['State'] = '';
        }
        
        // 2014-01-18
        // ANOMO-8431 - User locations for United Kingdom and Australia
        // For United Kingdom we will display the county, country. 
        if (!empty($data['CountryName']) && $data['CountryName'] == 'United Kingdom' ){
            $data['City'] = $data['State'];
            $data['State'] = '';
        }
        // For Australia we will display State, Country
        if (!empty($data['CountryName']) && $data['CountryName'] == 'Australia' ){
            $data['City'] = $data['State'];
            $data['State'] = '';
        }
        $this->db->select('OGR_FID');
        $this->db->from('neighborhood');
        $this->db->where('COUNTY', $data['CountryName']);
        $this->db->where('NAME', $data['City']);
        $this->db->where('STATE', $data['State']);
        $query = $this->db->get();
        $row = $query->row();
        $nbhID = $row ? $row->OGR_FID : 0;
        if (!$row) {
            // insert new record
            $value = array(
                'COUNTY' => $data['CountryName'],
                'NAME' => $data['City'],
                'STATE' => $data['State'],
                'CITY' => $data['City'],
                'Lat' => 0,
                'Lon' => 0
            );
            $nbhID = self::add($value);
        }

        if ($nbhID > 0) {
            $lat = $data['Lat']== 'null'?0:$data['Lat'];
            $lon =$data['Lon'] == 'null'?0:$data['Lon'];
            $updateData = array(
                'Lat' => $lat,
                'Lon' => $lon,
                'NeighborhoodID' => $nbhID,
                'LastActivity' => gmdate('Y-m-d H:i:s')
            );
            $ret = User_model::updateUser($updateData, array('UserID' => $data['UserID']));
            if ($ret) {
                $return['Return'] = $nbhID;
            }
            
            // update new usercard
            $new_user_card_sql = "select NeighborhoodID from activity where Type = 28 AND FromUserID = ".$data['UserID'];
            $new_user_card_query = $this->db->query($new_user_card_sql);
            $row = $new_user_card_query->row();
            if ($row && $row->NeighborhoodID == NBH_EARTH){
                $this->db->update('activity', 
                            array('Lat' => $lat, 'Lon' => $lon, 'NeighborhoodID' => $nbhID), 
                            array('FromUserID' => $data['UserID'], 'Type' => ACTIVITY_NEW_USER)
                        );
                if ($this->cached){
                    $this->cached->delete('user_activity_' . $data['UserID']);
                    $this->cached->delete('activity_user_' . $data['UserID']);
                    $this->cached->delete('recent_picture' . $data['UserID']);
                    $age_filter = $this->cached->get('agefilter_' . $data['UserID']);
                    if ($age_filter)
                        $this->cached->delete('activity_' . $age_filter['from_age'] . '_' . $age_filter['to_age']);
                        }
                    }
        }
         // delete cache
        if ($this->cached) {
            $this->cached->delete('user' . $data['UserID']);
            $this->cached->delete('user_detail' . $data['UserID']);
            $this->cached->delete('neighborhood_user' . $data['UserID']);
        }
        
        return $return;
    }

    function getCountryName($lat, $lon) {
        // 1. get from google map
        $latlng = $lat . ',' . $lon;
        $url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=$latlng&sensor=false";
        try {
            $get = @file_get_contents($url);
            $geoData = json_decode($get);
            $countryName = '';
            $city = '';
            $state = '';
            $countryCode = '';
            if (isset($geoData->results[0])) {
                foreach ($geoData->results[0]->address_components as $addressComponent) {
                    if (in_array('country', $addressComponent->types)) {
                        $countryName = $addressComponent->long_name;
                        $countryCode = $addressComponent->short_name;
                    }
                    if (in_array('locality', $addressComponent->types)) {
                        $city = $addressComponent->long_name;
                    }
                    if (in_array('administrative_area_level_1', $addressComponent->types)) {
                        $state = $addressComponent->short_name;
                    }                    
                }
                
            }
            return array('country' => $countryName, 'city' => $city, 'state' => $state, 'code' => $countryCode);
        } catch (Exception $ex) {
            return false;
        }
    }

}