<?php

class Version_model extends CI_Model {

    public $cached = false;

    function __construct() {
        parent::__construct();
        $CI = & get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
    }

    function add($data) {
        $this->db->insert('version', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        if ($this->cached){
            $this->cached->delete('version'.$data['AppVersion'].$data['Platform']);
        }
        $this->db->update('version', $data, $cond);
    }

    function getInfo($version_id) {
        $this->db->select('*');
        $this->db->from('version');
        $this->db->where('ID', $version_id);
        $query = $this->db->get();

        $result = $query->row();
        $query->free_result();
        return $result;
    }

    function getListByCondition($cond, $from, $to, $getSumNumber) {
        $this->db->select('*');
        $this->db->from('version');
        if (count($cond) > 0) {
            if (isset($cond['sort'])) {
                switch ($cond['sort']) {
                    case 'id-asc':
                        $this->db->order_by('ID', 'asc');
                        break;
                    case 'id-desc':
                        $this->db->order_by('ID', 'desc');
                        break;
                    case 'platform-asc':
                        $this->db->order_by('Platform', 'asc');
                        break;
                    case 'platform-desc':
                        $this->db->order_by('Platform', 'desc');
                        break;
                    case 'app-version-asc':
                        $this->db->order_by('AppVersion', 'asc');
                        break;
                    case 'app-version-desc':
                        $this->db->order_by('AppVersion', 'desc');
                        break;
                }
            }
        }
        if ($getSumNumber) {
            $query = $this->db->get();
            $result = $query->num_rows();
            $query->free_result();
            return $result;
        } else {
            $this->db->limit($from, $to);
            $query = $this->db->get();
            $numRow = $query->num_rows();
            $result = $query->result();
            $query->free_result();
            return ($numRow > 0) ? $result : null;
        }
    }

}

?>