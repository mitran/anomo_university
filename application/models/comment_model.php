<?php
class Comment_model extends CI_Model{
    function addLike($data) {
        $this->db->insert('commentlike', $data);
        return $this->db->insert_id();
    }
    
    function unLike($user_id, $comment_id, $content_type) {
        return $this->db->delete('commentlike', array('UserID' => $user_id, 'CommentID' => $comment_id, 'ContentType' => $content_type));
    }
    
    function isLike($user_id, $comment_id, $content_type, $return = 'int') {
        $sql = " select ID from commentlike where UserID = $user_id AND CommentID = $comment_id AND ContentType = $content_type";
        $query = $this->db->query($sql);
        $row = $query->row();
        $query->free_result();
        if ($return == 'object') {
            return $row;
        } else {
            return ($row) ? 1 : 0;
        }
    }
    
    function updateNumberLikeOfComment($comment_id, $type, $add = 'increase') {
        $sql = "Update activitycomment Set `NumberOfLike` = `NumberOfLike` + 1 WHERE ID = $comment_id";
        if ($add == 'decrease') {
            $sql = "Update activitycomment Set `NumberOfLike` = `NumberOfLike` - 1 WHERE ID = $comment_id";
        }
        return $this->db->query($sql);
    }
    
    
    function getListLike($comment_id, $type, $user_id) {
        $sql = "Select c.UserID, u.UserName
                        , u.BirthDate
                        , u.IsAdmin
                        , IF (u.FacebookID != '', 1, '') as FacebookID
			, IF (u.Photo != '', CONCAT('" . $this->config->item('s3_photo_url') . "',u.Photo), u.Photo) as Avatar
                        , if (NeighborhoodID is null, '', NeighborhoodID) as NeighborhoodID
                        , if (OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                                if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), nbh.NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                                )AS NeighborhoodName

                                FROM (
                                        select * from commentlike 
                                        WHERE CommentID = $comment_id AND ContentType = $type
                                              AND UserID NOT IN (select if(CurrentUserID = $user_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = $user_id or TargetUserID = $user_id)
                                       ORDER BY CreatedDate DESC
                                  ) as c
				left JOIN `user` u on c.UserID = u.UserID
                                left join neighborhood nbh ON nbh.OGR_FID = u.NeighborhoodID
                WHERE u.IsBanned = 0
				ORDER BY CreatedDate DESC";

        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function getCommentDetail($comment_id, $type) {

        $sql = "select c.*, u.UserName
                from activitycomment c
                left join user u ON u.UserID = c.UserID
                where ID=" . $comment_id;
        $query = $this->db->query($sql);
        $row = $query->row();
        $query->free_result();
        return $row;
    }
    
    function deleteComment($id){
        $this->db->delete('activitycomment', array('ID' => $id));
        $this->db->delete('notificationhistory', array('RefID' => $id, 'Type' => 14));
    }
    
    function checkBlockUser($comment_owner_id, $content_id, $type){
        $table_name = array(
            ACTIVITY_POST_STATUS => 'userprofilestatus', // change profile status
            ACTIVITY_POST_PICTURE => 'picturepostactivity',
            ACTIVITY_PROMOS => 'promos',
            ACTIVITY_POST_VIDEO => 'video'
        );
        if (!in_array($type, array_keys($table_name))) {
            return false;
        }
        $userIdField = 'UserID';
        if ($type == 9) { // send gift
            $userIdField = 'SendUserID';
        }
        $sql = "select * 
                from userblock where (CurrentUserID=$comment_owner_id AND TargetUserID = (select $userIdField from $table_name[$type] where ID = $content_id ))
                OR (TargetUserID=$comment_owner_id AND CurrentUserID = (select $userIdField from $table_name[$type] where ID = $content_id ));
                ";
        $query = $this->db->query($sql);
        return $query->row();
    }
}
?>
