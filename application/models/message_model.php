<?php

class Message_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function add($aData) {
        $this->db->insert('message', $aData);

        //Get error of DB
        if ($this->db->_error_message()) {
            log_message('error', $this->db->_error_message());
            return false;
        } else {
            $insertID = $this->db->insert_id();
            if ($insertID) {
                return $insertID;
            }
        }
    }

    function addMessageTracking($aData) {
        $this->db->insert('message_tracking', $aData);

        //Get error of DB
        if ($this->db->_error_message()) {
            log_message('error', $this->db->_error_message());
            return false;
        } else {
            return true;
        }
    }

    function getMessageTracking($iLimit = 100) {
        $photo_url = $this->config->item('s3_photo_url');
        $sql = "select mt.MessageID, mt.UserID, mt.Message, u.UserName, d.Type
                ,IF(u.Photo != '', concat('$photo_url', u.Photo), '') as Avatar
                from (select * from message_tracking where Status = 0 limit $iLimit) as mt
                left join user u ON u.UserID = mt.UserID
                left join devices d ON d.UserID = mt.UserID AND d.Status = 'active'
                ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function getTotalStatusTracking($iMessageID) {
        $this->db->select('count(*) as Total');
        $this->db->from('message_tracking');
        $this->db->where('Status', 0);
        $this->db->where('MessageID', $iMessageID);
        $query = $this->db->get();
        $oResult = $query->row();
        if ($oResult) {
            return $oResult->Total;
        }
    }

    function updateTotalRun($aData, $cond) {
        $this->db->update('message', $aData, $cond);

        //Get error of DB
        if ($this->db->_error_message()) {
            log_message('error', $this->db->_error_message());
            return false;
        } else {
            return true;
        }
    }

    function totalMsgTracking() {
        $this->db->select('count(*) as Total');
        $query = $this->db->get('message_tracking');

        $oResult = $query->row();
        if ($oResult) {
            return $oResult->Total;
        }
    }

    function updateStatus($aData, $cond) {
        $this->db->update('message', $aData, $cond);

        //Get error of DB
        if ($this->db->_error_message()) {
            log_message('error', $this->db->_error_message());
            return false;
        } else {
            return true;
        }
    }

    function updateStatusTracking($aData, $cond) {
        $this->db->update('message_tracking', $aData, $cond);

        //Get error of DB
        if ($this->db->_error_message()) {
            log_message('error', $this->db->_error_message());
            return false;
        } else {
            return true;
        }
    }

    function updateStatusTrackings($aData, $conds) {
        $conditionString = "";
        $counterMaxLength = sizeof($conds) - 1;
        $counter = 0;
        foreach ($conds as $cond) {
            $conditionString .= "(MessageID = " . $cond["MessageID"] . " and UserID = " . $cond["UserID"] . ")";
            if ($counter < $counterMaxLength) {
                $conditionString .= " or ";
                $counter++;
            }
        }

        $this->db->update('message_tracking', $aData, $conditionString);

        //Get error of DB
        if ($this->db->_error_message()) {
            log_message('error', $this->db->_error_message());
            return false;
        } else {
            return true;
        }
    }

    function deleteTracking($iID) {
        $this->db->delete('message_tracking', array('MessageID' => $iID, 'Status' => 1));

        //Get error of DB
        if ($this->db->_error_message()) {
            log_message('error', $this->db->_error_message());
            return false;
        } else {
            return true;
        }
    }

    function updateMessageStatus() {
        $sql = "update message
                Set Status = 1
                Where Status=0 AND MessageID NOT IN (select MessageID from message_tracking where status=0 group by MessageID)
                ";
        $this->db->query($sql);
    }

    function getListByCondition($cond, &$total, $offset = 0, $limit = 0) {
        $sql = "select MessageID, Message, Status, CreatedDate, TotalTracking
				from message
		";

        $totalResult = $this->db->query($sql);
        $total = $totalResult->num_rows();
        $sql .= " order by CreatedDate DESC";
        $sql .= " limit $limit, $offset";
        $query1 = $this->db->query($sql);
        return $query1->result();
    }
    
    function getPushReceiverInfo($aUserID){
        $within = 7;
        $sql = 'select u.UserID, u.AllowChatNotice,
                       (select count(ID) from notificationhistory where UserID = u.UserID and IsRead = 1 and CreatedDate >= ' . strtotime("-" . $within . " day", strtotime(gmdate('Y-m-d H:i:s'))) . ') as Number,
                       (select ChatCount from chat_count where UserID = u.UserID) as ChatCount,
                       d.DeviceID, d.Type
                from (select * from user where UserID IN ('.  implode(',', $aUserID).')) as u
                left join devices d on u.UserID = d.UserID and d.Status = \'active\'
                ';

        $query = $this->db->query($sql);
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

}

?>