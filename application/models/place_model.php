<?php

class Place_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->db_slave = $this->load->database('slave', TRUE);
    }

    function checkExistPlace($place_id) {
        $this->db_slave->select('count(PlaceID) as num');
        $this->db_slave->from('place');
        $this->db_slave->where('PlaceID', $place_id);
        $query = $this->db_slave->get();
        $row = $query->row();
        $query->free_result();
        return (int) $row->num;
    }

    function addPlace($data) {
        $data['CreatedDate'] = gmdate('Y-m-d H:i:s');
        $data['Provider'] = Config_model::getConfig('SEARCH_SERVICE_PROVIDER');
        $this->db->insert('place', $data);
        return $this->db->insert_id();
    }

    function getPlace($place_id) {
        $this->db_slave->select('*');
        $this->db_slave->from('place');
        $this->db_slave->where('PlaceID', $place_id);
        $query = $this->db_slave->get();
        $row = $query->row();
        $query->free_result();
        return $row;
    }

    function checkIn($place_id, $user_id, $status) {
        $data = array(
            'UserID' => $user_id,
            'PlaceID' => $place_id,
            'CheckInDate' => gmdate('Y-m-d H:i:s'),
            'Status' => $status
        );
        return $this->db->insert('usercheckin', $data);
    }

    function checkExistCheckin($place_id, $user_id, $status = 0) {
        $this->db_slave->select('ID');
        $this->db_slave->from('usercheckin');
        $this->db_slave->where('UserID', $user_id);
        $this->db_slave->where('PlaceID', $place_id);
        if ($status > 0) {
            $this->db_slave->where('Status', $status);
        }
        $query = $this->db_slave->get();
        $row = $query->row();
        $query->free_result();
        return $row;
    }

    /**
     * return number user checked in this place
     *
     * @param string $place_id
     * @return int
     */
    function getNumberUserCheckedIn($place_id) {
        $this->db_slave->select('count(ID) as num');
        $this->db_slave->from('usercheckin');
        $this->db_slave->where('PlaceID', $place_id);
        $this->db_slave->where('Status', 1);
        $this->db_slave->where('CheckOutDate >', gmdate('Y-m-d H:i:s'));
        $query = $this->db_slave->get();
        $row = $query->row();
        $query->free_result();
        return (int) $row->num;
    }

    /**
     * return status of user
     *
     * @param int $user_id
     * @param string $place_id
     * @return int
     *     - 0: not check in
     *     - 1: checked in
     *  - 2: check out
     *  - 3: spectator
     */
    function getCheckInStatus($user_id, $place_id) {
        $this->db_slave->select('Status');
        $this->db_slave->from('usercheckin');
        $this->db_slave->where('UserID', $user_id);
        $this->db_slave->where('PlaceID', $place_id);
        $this->db_slave->where('CheckOutDate >', gmdate('Y-m-d H:i:s'));
        $this->db_slave->order_by('CheckOutDate', 'DESC');
        $query = $this->db_slave->get();
        $row = $query->row();
        $query->free_result();
        return ($row) ? $row->Status : 0;
    }

    /**
     * return list user checkin of place
     *
     * @param string $place_id
     * @param string $user_id
     * @param $type
     *         'current': list user checked in /spectate
     *         'past': list user logout
     * @return array
     */
    function getListUserOf($place_id, $user_id = 0, $type = 'current', &$total = '', $offset = '', $limit = '') {
        $this->db_slave->select('u.UserID, u.UserName, u.Photo as Avatar, u.Gender, u.ProfileStatus, u.Email, u.BirthDate, uc.Status, uc.CheckInDate, u.FacebookID, u.LastActivity');
        $this->db_slave->from('usercheckin uc');
        $this->db_slave->join('user u', 'uc.UserID=u.UserID');
        $this->db_slave->where('uc.PlaceID', $place_id);
        $this->db_slave->where('u.IsBanned !=', 1);
        if ($type == 'past') {
            $sql = '(uc.Status = 2 OR (uc.Status != 2 AND uc.CheckOutDate < "' . gmdate('Y-m-d H:i:s') . '"))';
            $this->db_slave->where($sql);
        } else {
            $this->db_slave->where('uc.Status !=', 2); // check in /spectator
            $this->db_slave->where('uc.CheckOutDate >=', gmdate('Y-m-d H:i:s')); //
        }
        if ($user_id > 0) {
            $this->db_slave->where('uc.UserID !=', $user_id);
        }
        $this->db_slave->group_by('uc.UserID');
        $this->db_slave->order_by("uc.CheckOutDate", "DESC");
        $query = $this->db_slave->get();
        $last_query = $this->db_slave->last_query();

        $totalResult = $this->db_slave->query($last_query);
        $total = $totalResult->num_rows();

        if ($limit > 0) {
            $last_query .= " limit $offset, $limit";
        }

        $query1 = $this->db_slave->query($last_query);
        $retVal = $query1->result();
        $query->free_result();
        $query1->free_result();
        return $retVal;
    }

    /**
     * return list user checkin of place
     *
     * @param string $place_id
     * @param string $user_id
     * @param $type
     *         'current': list user checked in /spectate
     *         'past': list user logout
     * @return array
     */
    function getListUserOfV2($place_id, $user_id = 0, $type = 'current', $limit = 0) {
        $this->db_slave->select('u.Photo as Avatar');
        $this->db_slave->from('usercheckin uc');
        $this->db_slave->join('user u', 'uc.UserID=u.UserID');
        $this->db_slave->where('uc.PlaceID', $place_id);
        if ($type == 'past') {
            $sql = '(uc.Status = 2 OR (uc.Status != 2 AND uc.CheckOutDate < "' . gmdate('Y-m-d H:i:s') . '"))';
            $this->db_slave->where($sql);
        } else {
            $this->db_slave->where('uc.Status !=', 2); // check in /spectator
            $this->db_slave->where('uc.CheckOutDate >', gmdate('Y-m-d H:i:s'));
        }
        if ($user_id > 0) {
            $this->db_slave->where('uc.UserID !=', $user_id);
        }
        if ($limit > 0) {
            $this->db_slave->limit($limit);
        }
        $query = $this->db_slave->get();
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    /**
     * return list post of place
     *
     * @param string $place_id
     * @param string $type {place/user}
     * @return array
     */
    function getListPostOf($id, $type = 'place') {
        $this->db_slave->select('p.ID, p.Content, p.Photo, p.CreatedDate, p.UserID, u.UserName, u.Photo as Avatar, u.Gender');
        $this->db_slave->from('placepost p');
        $this->db_slave->join('user u', 'u.UserID=p.UserID');
        if ($type == 'place') {
            $this->db_slave->where('p.PlaceID', $id);
        } else {
            $this->db->where('p.UserID', $id);
        }
        $this->db_slave->where('p.IsMark', 0);
        $this->db_slave->order_by('p.ID', 'DESC');
        $query = $this->db_slave->get();
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    /**
     * return list post of place
     *
     * @param string $place_id
     * @param string $type {place/user}
     * @return array
     */
    function getListPostOfUser($id) {
        $provider = Config_model::getConfig('SEARCH_SERVICE_PROVIDER');
        $this->db_slave->select('p.ID, p.Content, p.Photo, p.CreatedDate, p.UserID, u.UserName, u.Photo as Avatar, u.Gender, pp.PlaceID, pp.Name');
        $this->db_slave->from('placepost p');
        $this->db_slave->join('user u', 'u.UserID=p.UserID');
        $this->db_slave->join('place pp', 'p.PlaceID=pp.PlaceID');
        // get provider
        $this->db_slave->where('pp.Provider', $provider);
        $this->db_slave->where('p.UserID', $id);
        $this->db_slave->order_by('p.ID', 'DESC');
        $query = $this->db_slave->get();
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    /**
     * return list user(checked in/ spectate) nearby
     *
     * @param int $user_id
     * @param float $lat
     * @param float $lon
     * @return object
     */
    function getNearbyUser($user_id, $lat, $lon) {
        $provider = Config_model::getConfig('SEARCH_SERVICE_PROVIDER');
        $this->db_slave->select("uc.UserID, u.UserName, u.Photo as Avatar, u.Gender, u.ProfileStatus, uc.Status, uc.PlaceID, p.Name, p.Address, p.Lat, p.Lon,  69.09*DEGREES(acos(sin(radians($lat))*sin(radians(p.Lat)) + cos(radians($lat))*cos(radians(p.Lat))*cos(radians($lon)-radians(p.Lon)))) as Distance");
        $this->db_slave->from('usercheckin uc');
        $this->db_slave->join('user u', 'u.UserID=uc.UserID');
        $this->db_slave->join('place p', 'p.PlaceID=uc.PlaceID');
        $this->db_slave->where('uc.UserID !=', $user_id);
        $this->db_slave->where('uc.Status !=', 2); // checked in or spectate
        $this->db_slave->where('uc.CheckOutDate >', gmdate('Y-m-d H:i:s'));
        // get provider
        $this->db_slave->where('p.Provider', $provider);
        $this->db_slave->order_by('Distance', 'ASC');
        $this->db_slave->limit(20);
        $query = $this->db_slave->get();
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    /* :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
    /* ::                                                                         : */
    /* ::  this routine calculates the distance between two points (given the     : */
    /* ::  latitude/longitude of those points). it is being used to calculate     : */
    /* ::  the distance between two zip codes or postal codes using our           : */
    /* ::  zipcodeworld(tm) and postalcodeworld(tm) products.                     : */
    /* ::                                                                         : */
    /* ::  definitions:                                                           : */
    /* ::    south latitudes are negative, east longitudes are positive           : */
    /* ::                                                                         : */
    /* ::  passed to function:                                                    : */
    /* ::    lat1, lon1 = latitude and longitude of point 1 (in decimal degrees)  : */
    /* ::    lat2, lon2 = latitude and longitude of point 2 (in decimal degrees)  : */
    /* ::    unit = the unit you desire for results                               : */
    /* ::           where: 'm' is statute miles                                   : */
    /* ::                  'k' is kilometers (default)                            : */
    /* ::                  'n' is nautical miles                                  : */
    /* ::                                                                         : */
    /* :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */

    function distance($lat1, $lon1, $lat2, $lon2, $unit = 'm') {
        $theta = $lon1 - $lon2;
        $lat1 = (float) $lat1;
        $lon1 = (float) $lon1;
        $lat2 = (float) $lat2;
        $lon2 = (float) $lon2;
        $theta = (float) $theta;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            $distance = ($miles * 1.609344);
        } else if ($unit == "N") {
            $distance = ($miles * 0.8684);
        } else {
            $distance = $miles;
        }
        return number_format($distance, 3, '.', '');
    }

    function getPlaceActivity($lat, $lon, $from = 0, $to = 0, $getSumNumber = false) {
        $provider = Config_model::getConfig('SEARCH_SERVICE_PROVIDER');
        $this->db_slave->select("pa.PlaceID, p.Name, pa.Type, pa.UserID, pa.PostID, pa.CreatedDate, 69.09*DEGREES(acos(sin(radians($lat))*sin(radians(p.Lat)) + cos(radians($lat))*cos(radians(p.Lat))*cos(radians($lon)-radians(p.Lon)))) as Distance");
        $this->db_slave->from('placeactivity pa');
        $this->db_slave->join('place p', 'p.PlaceID=pa.PlaceID');
        // get provider
        $this->db_slave->where('p.Provider', $provider);
        $this->db_slave->order_by('Distance', 'ASC');
        if ($getSumNumber) {
            $query = $this->db_slave->get();
            return $query->num_rows();
        } else {
            $this->db_slave->limit($from, $to);
            $query = $this->db_slave->get();
//			echo $this->db->last_query();
            $row = $query->num_rows();
            $retVal = ($row > 0) ? $query->result() : null;
            $query->free_result();
            return $retVal;
        }
    }

    function getListTagOf($place_id, $type = '', $limit = 0) {
        $this->db_slave->select('ut.TagID, t.Name, count(ut.TagID) as Count');
        $this->db_slave->from('usercheckin uc');
        $this->db_slave->join('usertag ut', 'uc.UserID=ut.UserID');
        $this->db_slave->join('tag t', 'ut.TagID=t.TagID');
        $this->db_slave->where('uc.PlaceID', $place_id);
        if ($type == 'current') {
            $this->db_slave->where('uc.Status !=', 2); // check in /spectator
            $this->db_slave->where('uc.CheckOutDate >', gmdate('Y-m-d H:i:s'));
        }
        if ($type == 'past') {
            $sql = '(uc.Status = 2 OR (uc.Status != 2 AND uc.CheckOutDate < "' . gmdate('Y-m-d H:i:s') . '"))';
            $this->db_slave->where($sql);
        }
        $this->db_slave->group_by('ut.TagID');
        // ANOMO-5917 
        if ($limit > 0) {
            $this->db_slave->limit($limit);
        }
        $query = $this->db_slave->get();
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    function getAnomoPick($lat, $lon, $from = 0, $to = 0, $getSumNumber = false) {
        $provider = Config_model::getConfig('SEARCH_SERVICE_PROVIDER');
        $this->db_slave->select("uc.PlaceID, p.Name, count(uc.PlaceID) as NumberAnomo, 69.09*DEGREES(acos(sin(radians($lat))*sin(radians(Lat)) + cos(radians($lat))*cos(radians(Lat))*cos(radians($lon)-radians(Lon)))) as Distance");
        $this->db_slave->from('usercheckin uc');
        $this->db_slave->join('place p', 'p.PlaceID=uc.PlaceID');
        $this->db_slave->where('uc.Status !=', 2); // checked in or spectate
        $this->db_slave->where('uc.CheckOutDate >', gmdate('Y-m-d H:i:s'));
        // get provider
        $this->db_slave->where('p.Provider', $provider);
        $this->db_slave->group_by('uc.PlaceID');
        $this->db_slave->having('Distance < 2');
        $this->db_slave->order_by('NumberAnomo', 'DESC');
        $retVal = NULL;
        if ($getSumNumber) {
            $query = $this->db_slave->get();
            $retVal = $query->num_rows();
        } else {
            $this->db_slave->limit($from, $to);
            $query = $this->db_slave->get();
            $retVal = ($query->num_rows() > 0) ? $query->result() : null;
        }
        $query->free_result();
        return $retVal;
    }

    function getAnomoPickV2($lat, $lon, $from = 0, $to = 0, $getSumNumber = false) {
        /*
          $this->db->select('u.UserID, u.UserName, u.Photo as Avatar, u.Gender, u.ProfileStatus, u.Email, u.BirthDate, uc.Status, uc.CheckInDate');
          $this->db->from('usercheckin uc');
          $this->db->join('user u', 'uc.UserID=u.UserID');
          $this->db->where('uc.PlaceID', $place_id);
         */
        $provider = Config_model::getConfig('SEARCH_SERVICE_PROVIDER');
        $this->db_slave->select("uc.PlaceID, p.Name, p.Address, count(uc.PlaceID) as NumberAnomo, 69.09*DEGREES(acos(sin(radians($lat))*sin(radians(Lat)) + cos(radians($lat))*cos(radians(Lat))*cos(radians($lon)-radians(Lon)))) as Distance");
        $this->db_slave->from('usercheckin uc');
        $this->db_slave->join('place p', 'p.PlaceID=uc.PlaceID');
        $this->db_slave->where('uc.Status', 1); // checked in
        $this->db_slave->where('uc.CheckOutDate >=', gmdate('Y-m-d H:i:s')); //
        // get provider
//		$providerWhere = "(p.Provider = $provider OR p.Provider = 'local')";
        //	$this->db->where('p.Provider', $provider);


        /* sync with getListUserOfV2 */
        // WHY
//		$sql = '(uc.Status = 2 OR (uc.Status != 2 AND uc.CheckOutDate < "'.gmdate('Y-m-d H:i:s').'"))';
//		$this->db->where($sql);


        $this->db_slave->group_by('uc.PlaceID');
        $this->db_slave->order_by('Distance', 'ASC');
        $retVal = NULL;
        if ($getSumNumber) {
            $query = $this->db_slave->get();
            $retVal = $query->num_rows();
        } else {
            $this->db_slave->limit($from, $to);
            $query = $this->db_slave->get();
            $retVal = ($query->num_rows() > 0) ? $query->result() : null;
        }
        $query->free_result();
        return $retVal;
    }

    function getListUserBelongTagOf($place_id, $tag_id, $type = 'current') {
        $this->db_slave->select('uc.UserID, u.UserName, u.Photo as Avatar, u.Gender, u.ProfileStatus');
        $this->db_slave->from('usercheckin uc');
        $this->db_slave->join('usertag ut', 'uc.UserID=ut.UserID');
        $this->db_slave->join('user u', 'u.UserID=ut.UserID');
        $this->db_slave->where('uc.PlaceID', $place_id);
        $this->db_slave->where('ut.TagID', $tag_id);
        if ($type == 'past') {
            $sql = '(uc.Status = 2 OR (uc.Status != 2 AND uc.CheckOutDate < "' . gmdate('Y-m-d H:i:s') . '"))';
            $this->db_slave->where($sql);
        } else {
            $this->db_slave->where('uc.Status !=', 2); // check in /spectator
            $this->db_slave->where('uc.CheckOutDate >', gmdate('Y-m-d H:i:s'));
        }
        $this->db_slave->group_by('uc.UserID');
        $query = $this->db_slave->get();
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    function getUserStatus($user_id) {
        $this->db_slave->select('p.PlaceID, p.Name, uc.CheckInDate');
        $this->db_slave->from('usercheckin uc');
        $this->db_slave->join('place p', 'p.PlaceID=uc.PlaceID');
        $this->db_slave->where('uc.UserID', $user_id);
        $this->db_slave->where('uc.Status', 1); // check in
        $this->db_slave->where('uc.CheckOutDate >', gmdate('Y-m-d H:i:s'));
        $query = $this->db_slave->Get();
        $retVal = $query->row();
        $query->free_result();
        return $retVal;
        ;
    }

    function getUserCheckinStatus($user_id) {
        $this->db_slave->select('uc.Status, p.PlaceID, p.Name, uc.CheckInDate');
        $this->db_slave->from('usercheckin uc');
        $this->db_slave->join('place p', 'p.PlaceID=uc.PlaceID');
        $this->db_slave->where('uc.UserID', $user_id);
        $this->db_slave->where('uc.Status', 1); // check in - ANOMO-5119 - only get check in status
        $this->db_slave->where('uc.CheckOutDate >', gmdate('Y-m-d H:i:s'));
        $this->db_slave->order_by('uc.CheckInDate', 'desc');
        $query = $this->db_slave->Get();
        $retVal = $query->row();
        $query->free_result();
        return $retVal;
    }

    function getNumberCheckInOfUser($user_id) {
        $this->db_slave->select('Count(ID) as NumberCheckIn');
        $this->db_slave->from('usercheckin');
        $this->db_slave->where('UserID', $user_id);
        $this->db_slave->where('Status !=', 3);
        $query = $this->db_slave->get();
        $result = $query->row();
        $query->free_result();
        return ($result) ? $result->NumberCheckIn : 0;
    }

    function searchPlaceNearby($lat, $lon) {
        $provider = Config_model::getConfig('SEARCH_SERVICE_PROVIDER');
        $this->db_slave->select("uc.PlaceID, p.Name, count(uc.PlaceID) as NumberAnomo");
        $this->db_slave->from('usercheckin uc');
        $this->db_slave->join('place p', 'p.PlaceID=uc.PlaceID');
        $this->db_slave->where("69.09*DEGREES(acos(sin(radians($lat))*sin(radians(p.Lat)) + cos(radians($lat))*cos(radians(p.Lat))*cos(radians($lon)-radians(p.Lon)))) < 50");
        $this->db_slave->where('uc.Status !=', 2);
        $this->db_slave->where('uc.CheckOutDate >', gmdate('Y-m-d H:i:s'));
        // get provider
        $this->db_slave->where('p.Provider', $provider);
        $this->db_slave->group_by('uc.PlaceID');
        $this->db_slave->having('NumberAnomo > 0');
        $this->db_slave->order_by('RAND()');
        $this->db_slave->limit(4);
        $query = $this->db_slave->get();
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    function searchUserNearby($user_id, $lat, $lon) {
        $provider = Config_model::getConfig('SEARCH_SERVICE_PROVIDER');
        $this->db_slave->select("uc.UserID, u.UserName, u.Photo as Avatar, u.FullPhoto as FullAvatar, p.Name, p.PlaceID");
        $this->db_slave->from('usercheckin uc');
        $this->db_slave->join('place p', 'p.PlaceID=uc.PlaceID');
        $this->db_slave->join('user u', 'u.UserID=uc.UserID');
        $this->db_slave->where('uc.Status !=', 2); // checked in or spectate
        $this->db_slave->where('uc.CheckOutDate >', gmdate('Y-m-d H:i:s'));
        $this->db_slave->where('uc.UserID !=', $user_id);
        $this->db_slave->where("69.09*DEGREES(acos(sin(radians($lat))*sin(radians(p.Lat)) + cos(radians($lat))*cos(radians(p.Lat))*cos(radians($lon)-radians(p.Lon)))) < 50");
        // get provider
        $this->db_slave->where('p.Provider', $provider);
        $this->db_slave->group_by('uc.UserID');
        $this->db_slave->order_by('RAND()');
        $this->db_slave->limit(4);
        $query = $this->db_slave->get();
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    function searchUserNearbyV2($user_id, $lat, $lon, $from = 0, $to = 0, $getSumNumber = false) {
        $this->db_slave->select("u.UserID, u.UserName, u.Photo as Avatar, u.FullPhoto as FullAvatar, 69.09*DEGREES(acos(sin(radians($lat))*sin(radians(u.Lat)) + cos(radians($lat))*cos(radians(u.Lat))*cos(radians($lon)-radians(u.Lon)))) as Distance");
        $this->db_slave->from('user u');
        $this->db_slave->where('u.UserID !=', $user_id);
        $this->db_slave->having('Distance >= 0');
        $this->db_slave->order_by('Distance', 'ASC');
        $retVal = NULL;
        if ($getSumNumber) {
            $query = $this->db_slave->get();
            $retVal = $query->num_rows();
        } else {
            $this->db_slave->limit($from, $to);
            $query = $this->db_slave->get();
            $retVal = ($query->num_rows() > 0) ? $query->result() : null;
        }
        $query->free_result();
        return $retVal;
    }

    function getCheckInPlaceOfUser($user_id) {
        $provider = Config_model::getConfig('SEARCH_SERVICE_PROVIDER');
        $this->db_slave->select('p.PlaceID, p.Name');
        $this->db_slave->from('usercheckin uc');
        $this->db_slave->join('place p', 'uc.PlaceID=p.PlaceID');
        $this->db_slave->where('uc.Status', 1); // checked in only
        $this->db_slave->where('uc.CheckOutDate >', gmdate('Y-m-d H:i:s'));
        $this->db_slave->where('p.Provider', $provider);
        $this->db_slave->where('uc.UserID', $user_id);
        $this->db_slave->order_by('uc.CheckInDate', 'DESC');
        $this->db_slave->limit(1);
        $query = $this->db_slave->get();
        $retVal = $query->row();
        $query->free_result();
        return $retVal;
    }

    function getListCheckoutUser($place_id, $user_id = 0, $within = 0, $limit = 0) {
        $this->db_slave->select('u.UserID, u.UserName, u.Photo as Avatar, u.Gender, u.ProfileStatus, u.Email, u.BirthDate, uc.Status, uc.CheckInDate');
        $this->db_slave->from('usercheckin uc');
        $this->db_slave->join('user u', 'uc.UserID=u.UserID');
        $this->db_slave->where('uc.PlaceID', $place_id);
        $sql = '(uc.Status = 2 OR (uc.Status != 2 AND uc.CheckOutDate < "' . gmdate('Y-m-d H:i:s') . '"))';
        $this->db_slave->where($sql);
        if ($within > 0) {
            $this->db_slave->where('uc.CheckOutDate >=', date('Y-m-d H:i:s', strtotime("-" . $within . " hour", strtotime(gmdate('Y-m-d H:i:s')))));
        }
        if ($user_id > 0) {
            $this->db_slave->where('uc.UserID !=', $user_id);
        }
        if ($limit > 0) {
            $this->db_slave->limit($limit);
        }
        $this->db_slave->group_by('uc.UserID');
        $this->db_slave->order_by('uc.CheckOutDate', 'DESC');
        $query = $this->db_slave->get();
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    function getListSpectatePlaceOf($user_id) {
        $this->db_slave->select('uc.PlaceID, p.Reference');
        $this->db_slave->from('usercheckin uc');
        $this->db_slave->join('place p', 'uc.PlaceID=p.PlaceID');
        $this->db_slave->where('uc.UserID', $user_id);
        $this->db_slave->where('uc.Status', 3); // check in /spectator
        $this->db_slave->where('uc.CheckOutDate >', gmdate('Y-m-d H:i:s'));
        $this->db_slave->group_by('uc.PlaceID');
        $this->db_slave->order_by('uc.CheckOutDate', 'DESC');
        $query = $this->db_slave->get();
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    function getStatusOf($user_id, $place_id) {
        $this->db_slave->select('*');
        $this->db_slave->from('usercheckin');
        $this->db_slave->where('UserID', $user_id);
        $this->db_slave->where('PlaceID', $place_id);
//		$this->db_slave->where('CheckOutDate >', gmdate('Y-m-d H:i:s'));
        $this->db_slave->order_by('CheckOutDate', 'DESC');
        $this->db_slave->limit(1);
        $query = $this->db_slave->get();
        $retVal = $query->row();
        $query->free_result();
        return $retVal;
    }

    function getActivitiesByUserID($user_id, $page = 1, $offset = 10) {
        $this->db_slave->select('pa.ID, pa.Type, p.Name, p.Address, pa.CreatedDate, p.PostID ');
        $this->db_slave->from('placeactivity pa');
        $this->db_slave->join('place p', 'p.PlaceID=pa.PlaceID');
        $this->db_slave->where('UserID', $user_id);
        $this->db_slave->order_by('pa.CreatedDate', 'DESC');
        $query = $this->db_slave->get();
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    function getTotalCheckin($place_id) {
        $this->db_slave->select('count(UserID) as Number');
        $this->db_slave->from('usercheckin uc');
        $this->db_slave->where('PlaceID', $place_id);
        $this->db_slave->where('uc.Status !=', 2); // check in /spectator
        $this->db_slave->where('uc.CheckOutDate >=', gmdate('Y-m-d H:i:s')); //
        $query = $this->db_slave->get();
        $row = $query->row();
        $query->free_result();
        return ($row) ? $row->Number : 0;
    }

    function checkCheckIn($user_id, $place_id) {
        $this->db_slave->select('*');
        $this->db_slave->from('usercheckin');
        $this->db_slave->where('UserID', $user_id);
        $this->db_slave->where('PlaceID', $place_id);
        $this->db_slave->where('CheckOutDate >', gmdate('Y-m-d H:i:s'));
        $query = $this->db_slave->get();
        $res = $query->row();
        $query->free_result();
        if ($res) {
            return ($res->Status == 1) ? 1 : 0;
        }
        return 0;
    }

    //function to find place that is close to a location with a distance (unit: mile)
    function getLocalPlace($latitude, $longitude, $fromlat, $fromlon, $tolat, $tolon, $distance = 2, $keyword, $page = 1, $isGetTotal = false) {
        //set start limit
        $offset = ($page - 1) * 10;
        $SearchKeyword = false;

        //check keyword
        if (isset($keyword) && $keyword) {
            $SearchKeyword = true;
        }

        $qry = "SELECT *,(((acos(sin((" . $latitude . "*pi()/180)) * sin((`Lat`*pi()/180))+cos((" . $latitude . "*pi()/180)) * cos((`Lat`*pi()/180)) * cos((" . $longitude . " - `Lon`)*pi()/180))))*180/pi())*60*1.1515 as distance FROM `place` ";
        $qry .= " WHERE Provider='local' and PlaceID is not Null ";
        //$qry .= " and Status = 1 "; //use for get active local place
        if ($SearchKeyword) {
            $qry .= " and `Name` like '%$keyword%'";
        }

        //is Search by Range
        if (
                isset($fromlat) && is_numeric($fromlat) && $fromlat != 0 &&
                isset($fromlon) && is_numeric($fromlon) && $fromlon != 0 &&
                isset($tolat) && is_numeric($tolat) && $tolat != 0 &&
                isset($tolon) && is_numeric($tolon) && $tolon != 0 &&
                $SearchKeyword == false
        ) {
            //greater than or equal
            $qry .= " and ((((acos(sin((" . $latitude . "*pi()/180)) * sin((`Lat`*pi()/180))+cos((" . $latitude . "*pi()/180)) * cos((`Lat`*pi()/180)) * cos((" . $longitude . " - `Lon`)*pi()/180))))*180/pi())*60*1.1515)
                    >= ((((acos(sin((" . $latitude . "*pi()/180)) * sin((" . $fromlat . "*pi()/180))+cos((" . $latitude . "*pi()/180)) * cos((" . $fromlat . "*pi()/180)) * cos((" . $longitude . " - " . $fromlon . ")*pi()/180))))*180/pi())*60*1.1515)";

            //less than
            $qry .= " and ((((acos(sin((" . $latitude . "*pi()/180)) * sin((`Lat`*pi()/180))+cos((" . $latitude . "*pi()/180)) * cos((`Lat`*pi()/180)) * cos((" . $longitude . " - `Lon`)*pi()/180))))*180/pi())*60*1.1515)
                    < ((((acos(sin((" . $latitude . "*pi()/180)) * sin((" . $tolat . "*pi()/180))+cos((" . $latitude . "*pi()/180)) * cos((" . $tolat . "*pi()/180)) * cos((" . $longitude . " - " . $tolon . ")*pi()/180))))*180/pi())*60*1.1515)";
        } else {
            $qry .= " having distance < $distance ";
        }


        if ($isGetTotal) {
            $query = $this->db_slave->query($qry);
            $result = $query->result();
            $query->free_result();
            return ($result) ? count($result) : 0;
        } else {
            //$qry .= " limit $offset,10"; 
            //$qry .= " limit 10";
            $query = $this->db_slave->query($qry);
            $retVal = $query->result();
            $query->free_result();
            return $retVal;
        }
    }

    function postToPlaceSP($data) {
        $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
        $data['UserName'] = mysqli_real_escape_string($con, $data['UserName']);
        $data['Content'] = mysqli_real_escape_string($con, $data['Content']);
        $sql = "CALL sp_createPost('" . $data['UserID'] . "',
                                        '" . $data['UserName'] . "',
                                        '" . $data['Avatar'] . "',
                                        '" . $data['Lat'] . "',
                                        '" . $data['Lon'] . "',
                                        '" . $data['BirthDate'] . "',
                                        '" . $data['Gender'] . "',
                                        '" . $data['PlaceID'] . "',
                                        '" . $data['PlaceName'] . "',
                                        '" . $data['PlaceAddress'] . "',
                                        '" . $data['PlaceIcon'] . "',
                                        '" . $data['Photo'] . "',
                                        '" . $data['Photo100'] . "',
                                        '" . $data['Photo200'] . "',
                                        '" . $data['Photo300'] . "',
                                        '" . $data['Content'] . "',
                                        '" . $data['CreatedDate'] . "',
                                        '" . $data['Type'] . "',
                                         @result,@postId          
                                        )";
        $return = array();
        try {
            $this->db->trans_begin();
            $query = $this->db->query($sql);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
                $query->next_result();
                $query2 = $this->db->query("select @result as Result, @postId as PostID");

                foreach ($query2->result_array() as $row) {
                    $return['Return'] = $row['Result'];
                    $return['PostID'] = $row['PostID'];
                }

                $query2->free_result();
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }

        return $return;
    }

}

?>