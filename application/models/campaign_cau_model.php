<?php

class Campaign_CAU_model extends CI_Model{
    public $cached = false;

    function __construct(){
        parent::__construct();
        $CI =& get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
        
    }
    function add($data) {
        $this->db->insert('campaigncau', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('campaigncau', $data, $cond);
    }
        
    function getCampaignCAUByUser($UserID) {
        $sql = "
            SELECT * FROM campaigncau 
            WHERE UserID = $UserID          
        ";
        $result = $this->db->query($sql);
        return $result->row();        
    }       
}
?>

