<?php

class Neighborhood_User_Chat_Messages_model extends CI_Model {

    function add($data) {
        $this->db->insert('neighborhooduserchatmessages', $data);
        return $this->db->insert_id();
    }

    function find50MessagesbyMessageID($nbhID, $messageID = -1, $reverse = false, $user_id) {
        $this->db->select('pucm.ID, pucm.Content, pucm.Photo, pucm.Photo100, pucm.Photo200, pucm.Photo300, pucm.CreatedDate, pucm.ColorID, pucm.UserID, u.UserName, u.Photo as Avatar');
        $this->db->from('neighborhooduserchatmessages pucm');
        $this->db->join('user u', 'pucm.UserID=u.UserID');
        $this->db->where('pucm.NeighborhoodID', $nbhID);
        $this->db->where('u.IsBanned !=', 1);
        if ($messageID > 0) {
            if ($reverse) {
                $this->db->where('pucm.ID <', $messageID);
            } else {
                $this->db->where('pucm.ID >', $messageID);
            }
            $this->db->where('pucm.UserID !=', $user_id);
        }
        $this->db->order_by('pucm.CreatedDate', 'ASC');
        $this->db->limit(50);
        $query = $this->db->get();
        return $query->result();
    }

    function countChattingUser($nbh_id) {
        $this->db->select('distinct(pucm.UserID)');
        $this->db->from('neighborhooduserchatmessages pucm');
        $this->db->join('neighborhoodcheckin uc', 'pucm.UserID=uc.UserID and pucm.NeighborhoodID=uc.NeighborhoodID', 'inner');
        $this->db->where('pucm.NeighborhoodID', $nbh_id);
        $this->db->where('uc.Status !=', 2);
        $query = $this->db->get();
        return $query->num_rows();
    }

}