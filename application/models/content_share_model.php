<?php

class Content_Share_model extends CI_Model {

    function add($data) {
        $this->db->insert('contentshare', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        return $this->db->update('contentshare', $data, $cond);
    }

    function isShare($user_id, $content_id, $type, $share_type) {
        $sql = "select ID from contentshare where 
    			UserID=$user_id AND 
    			ContentID = $content_id AND 
    			ContentType = $type AND";
        if ($share_type == 2) {
            $sql .= " ShareType = 0 || ShareType = 1";
        } else {
            $sql .= " ShareType = $share_type";
        }
        $query = $this->db->query($sql);
        $row = $query->row();
        return $row ? 1 : 0;
    }

}

?>