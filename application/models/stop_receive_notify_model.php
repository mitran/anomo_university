<?php

class Stop_Receive_Notify_model extends CI_Model{
     function add($data) {
        $this->db->insert('stopreceivenotify', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        return $this->db->update('stopreceivenotify', $data, $cond);
    }
    
    function delete($cond) {
        return $this->db->delete('stopreceivenotify', $cond);
    }
    
    function getInfo($user_id, $content_id, $type){
        $sql = "select ID from stopreceivenotify where UserID = $user_id and ContentID = $content_id and Type = $type";        
        $query = $this->db->query($sql);
        return $query->row();
    }
}
?>
