<?php

class Auto_Msg_model extends CI_Model
{

    protected $messageTableName = 'auto_message';
    protected $messageQueueTableName = 'auto_message_queue';
    protected $messageConstraintsTableName = 'auto_message_constraints';

    function __construct()
    {
        parent::__construct();
    }

    function add($data)
    {
        $messageData = array(
            'message' => $data['message'],
            'photo' => $data['photo'],
            'status' => $data['status'],
            'created_date' => gmdate('Y-m-d H:i:s'),
            'tap_action' => $data['tap_action'],
            'tap_param' => $data['tap_param'],
            'expire_date' => $data['expire_date'],
        );
        $this->db->insert($this->messageTableName, $messageData);
        $message_id = $this->db->insert_id();

        $sql = array();
        $constraints = $data['constraints'];
        foreach ($constraints as $constraint) {
            $sql[] = "('" . $message_id . "','" . $constraint['cond_key'] . "','" . $constraint['value'] . "')";
        }

        $this->db->query('INSERT INTO ' . $this->messageConstraintsTableName . ' (message_id, cond_key, value) VALUES ' . implode(',', $sql));
        return $message_id;
    }

    function update($message_id, $data)
    {
        $messageData = array(
            'message' => $data['message'],
            'photo' => $data['photo'],
            'status' => $data['status'],
            'tap_action' => $data['tap_action'],
            'tap_param' => $data['tap_param'],
            'expire_date' => $data['expire_date'],
        );
        $this->db->update($this->messageTableName, $messageData, array('message_id' => $message_id));

        $constraints = $data['constraints'];
        // delete old constraints
        $this->db->query('delete from ' . $this->messageConstraintsTableName . ' where message_id =' . $message_id);

        // then insert new
        if (sizeof($constraints) > 0) {
            $sql = array();
            $constraints = $data['constraints'];
            foreach ($constraints as $constraint) {
                $sql[] = "('" . $message_id . "','" . $constraint['cond_key'] . "','" . $constraint['value'] . "')";
            }
            $this->db->query('INSERT INTO ' . $this->messageConstraintsTableName . ' (message_id, cond_key, value) VALUES ' . implode(',', $sql));
        }
    }

    function getListByCondition($cond, &$total, $offset = 0, $limit = 0)
    {
        $sqlTotal = "select count(message_id) as Total from auto_message";
        $totalQuery = $this->db->query($sqlTotal);
        $totalRow = $totalQuery->row();
        $total = $totalRow ? $totalRow->Total : 0;

        //$sql = "select * from auto_message order by created_date DESC limit $limit, $offset";
        $sql = "
        select temp.*,pl.`name` as 'location_name',pl.radius
        from 
        (
        select am.*,
        max(if(amc.cond_key = 'promo_location_id',amc.value,'')) as 'promo_location_id',
        max(if(amc.cond_key = 'age_from',amc.value,'')) as 'age_from',
        max(if(amc.cond_key = 'age_to',amc.value,'')) as 'age_to',
        max(if(amc.cond_key = 'gender',amc.value,'')) as 'gender'
        from auto_message  am 
        inner join auto_message_constraints amc on am.message_id = amc.message_id
        GROUP BY am.message_id
        order by created_date DESC
        ) temp
        inner join promo_location pl on temp.promo_location_id = pl.id
        limit $limit, $offset                
        ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    /**
     * This function help collect the list of auto messages to be sent to the user. What this does is that it will fetch
     * all messages in the auto_message table and its constraints, then check whether the user meets all those constraints
     * or not. If users meet all constraints of a message, add the message to the return result list. Repeat until all messages
     * are considered.
     *
     * @NOTE: WE WILL NEED TO OPTIMIZE THIS IN THE LONG RUN. ALSO, PUTTING THIS METHOD IN THE MODEL CLASS IS NOT ADVISABLE, BUT
     *        FOR THE MEAN TIME, JUST LEAVE IT HERE
     * @param $user
     * @param $messageIds
     * @param $nearbyLocs
     * @return mixed
     */
    function getListByConstraints($user, $messageIds, $nearbyLocs)
    {
        // setup $result array
        $result = array();

        // first get all the message items
        $messageIdStr = "";
        if (sizeof($messageIds) > 0) {
            /*
            $messageIdStr .= "message_id NOT IN (";
            foreach ($messageIds as $messageId) {
                $messageIdStr .= $messageId;
            }
            $messageIdStr .= ") AND ";
            */
            $messageIdStr = "message_id NOT IN (".implode(',', $messageIds).") AND";
        }
        $sql = "SELECT * FROM " . $this->messageTableName . " WHERE " . $messageIdStr . " status = 1 AND DATEDIFF(NOW(),expire_date) < 0";
        $query = $this->db->query($sql);


        // prep the res and get constraints:
        $messages = $query->result();
        if (sizeof($messages > 0)) {
            foreach ($messages as $message) {
                $sql = "select * from " . $this->messageConstraintsTableName . " where message_id =" . $message->message_id;
                $query = $this->db->query($sql);
                $res = $query->result();

                // now do filtering...
                if ($res) {
                    $satisfyConstraints = true;
                    foreach ($res as $re) {
                        $condKey = $re->cond_key;
                        $val = $re->value;

                        if ($condKey == 'promo_location_id') {
                            $satisfyConstraints = false;
                            foreach ($nearbyLocs as $nearbyLoc) {
                                if ($val == $nearbyLoc) {
                                    $satisfyConstraints = true;
                                }
                            }
                        } elseif ($condKey == 'age_from') {
                            $now = gmdate('Y-m-d');
                            $youngest_date = date('Y-m-d', strtotime('-'.$val .' year', strtotime($now)));
                            $satisfyConstraints = ($user->BirthDate <= $youngest_date); 
                        } elseif ($condKey == 'age_to') {
                            $now = gmdate('Y-m-d');
                            $oldest_date = date('Y-m-d', strtotime('-'.$val - 1 .' year', strtotime($now)));
                            $satisfyConstraints = ($user->BirthDate > $oldest_date);
                        } elseif ($condKey == 'gender') {
                            $satisfyConstraints = ($user->Gender == $val);
                        }

                        if (!$satisfyConstraints) break;
                    }
                }

                if ($satisfyConstraints) {
                    $result[] = $message;
                }
            }
        }

        return $result;
    }

    function getInfo($message_id)
    {
        $this->db->select('*');
        $this->db->from('auto_message');
        $this->db->where('message_id', $message_id);
        $query = $this->db->get();
        $row = $query->row();
        if ($row) {
            $sql = "select * from " . $this->messageConstraintsTableName . " where message_id =" . $message_id;
            $query = $this->db->query($sql);
            $result = $query->result();
            if ($result) {
                foreach ($result as $re) {
                    $row->{$re->cond_key} = $re->value;
                }
            }
        }
        return $row;
    }

    function delete($message_id)
    {
        $this->db->delete($this->messageQueueTableName, array('message_id' => $message_id));
        $this->db->delete($this->messageConstraintsTableName, array('message_id' => $message_id));
        $this->db->delete($this->messageTableName, array('message_id' => $message_id));
    }

    function getUserQueuedMessage($user_id)
    {
        $this->db->select('*');
        $this->db->from($this->messageQueueTableName);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        return $query->result();
    }

    function addMessageToQueue($user_id, $messages)
    {
        $sql = array();
        if (sizeof($messages) > 0) {
            foreach ($messages as $message) {
                $sql[] = '("' . $user_id . '", ' . $message->message_id . ', 0, "' . $message->expire_date . '")';
            }
            return $this->db->query('INSERT INTO ' . $this->messageQueueTableName . ' (user_id, message_id, status, expire_date) VALUES ' . implode(',', $sql));
        }
        return 0;
    }

    function getMessagesFromQueue($limit)
    {
        $photo_url = $this->config->item('s3_photo_url');
        $sql = "select amq.message_id, amq.user_id, am.message, u.UserName, d.Type, am.tap_action, am.tap_param
                ,IF(u.Photo != '', concat('$photo_url', u.Photo), '') as Avatar
                ,IF(am.photo != '', concat('$photo_url', am.photo), '') as photo
                from (select * from " . $this->messageQueueTableName . " where status = 0 AND DATEDIFF(NOW(),expire_date) < 0 limit $limit) as amq
                left join " . $this->messageTableName . " am on am.message_id = amq.message_id
                left join user u ON u.UserID = amq.user_id
                left join devices d ON d.UserID = amq.user_id AND d.Status = 'active'
                ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function updateStatusTrackings($aData, $conds)
    {
        $conditionString = "";
        $counterMaxLength = sizeof($conds) - 1;
        $counter = 0;
        foreach ($conds as $cond) {
            $conditionString .= "(message_id = " . $cond["message_id"] . " and user_id = " . $cond["user_id"] . ")";
            if ($counter < $counterMaxLength) {
                $conditionString .= " or ";
                $counter++;
            }
        }

        $this->db->update($this->messageQueueTableName, $aData, $conditionString);

        //Get error of DB
        if ($this->db->_error_message()) {
            log_message('error', $this->db->_error_message());
            return false;
        } else {
            return true;
        }
    }
}

?>
