<?php
class Promos_model extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->db_slave = $this->load->database('slave', TRUE);
    }
    
    function add($data) {
        $this->db->insert('promos', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        return $this->db->update('promos', $data, $cond);
    }
    
    function getListVendor($sUserId = ''){
        if (empty($sUserId)){
            $sUserId = VENDOR_IDS;
        }
        $data = array();
        if (!empty($sUserId)){
            $sql = "select UserID, UserName from user where UserID IN (".$sUserId.")";
            $query = $this->db->query($sql);
            $result = $query->result();
            if ($result){
                foreach($result as $row){
                    $data[$row->UserID] = $row->UserName;
                }
            }
        }
        return $data;
    }
    
    function getInfo($id){
        $this->db->select('*');
        $this->db->from('promos');
        $this->db->where('ID', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function getListPromosWithPaging($cond, &$total = 0, $from = 0, $to = 0) {
        $total = 0;
        $where = '';
        if (isset($cond['vendor']) && $cond['vendor'] > 0){
            $where = 'where UserID ='.$cond['vendor'];
        }
        $sqlTotal = "select count(ID) as Total from promos $where " ;
        $queryTotal = $this->db->query($sqlTotal);
        $rowTotal = $queryTotal->row();
        $total = $rowTotal ? $rowTotal->Total : 0;
        if ($total > 0){
            $sql = "select *
                    from promos $where order by ID DESC 
                    limit $to, $from ";
            $query = $this->db->query($sql);
            return $query->result();
        }
        return false;
    }
    
    function delete($id){
        $return = false;
        try {

            $this->db->trans_begin();

            $this->db->delete('promos', array('ID' => $id));
            $commentIDs = Activity_model::getCommentIDOf($id, 10);
            if (count($commentIDs) > 0){
                $this->db->delete('notificationhistory', "RefID IN (".implode(',', $commentIDs).") AND Type=14 AND ContentType=10"); // comment activity
            }
            $likeIDs = Activity_model::getLikeIDOf($id, 10);
            if (count($likeIDs) > 0){
                $this->db->delete('notificationhistory', "RefID IN (".implode(',', $likeIDs).") AND Type=13 AND ContentType=10"); // like activity
            }
            
            $this->db->delete('activitycomment', array('ContentID' => $id, 'Type' => 10));
            $this->db->delete('activitylike', array('ContentID' => $id, 'Type' => 10));

            $return = true;
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }
        return $return;
    }
    
    function randomAds($user_id = 0, $ads_id = 0, $is_follow = 0) {
        $photoUrl = $this->config->item('s3_photo_url');
        $cond = '';
        $order = '';
        if ($ads_id > 0){
            $cond = " AND ID = $ads_id";
        }else{
            $order = " order by rand() limit 1";
        }
        if ($is_follow > 0){
            $cond = " AND UserID IN (select TargetUserID from userconnect where CurrentUserID = $user_id AND TargetUserID IN (".VENDOR_IDS."))";
        }
        
        $sql = "select t.*
                    , u.UserName as FromUserName
                    , IF (u.Photo != '', CONCAT('$photoUrl', u.Photo), '') AS Avatar
                    , if ((select ID from activitylike where ContentID = ActivityID AND Type = 10 AND UserID = $user_id limit 1) is not null, 1, 0) as IsLike
                    , if ((select ID from activitycomment where ContentID = ActivityID AND Type = 10 AND UserID = $user_id limit 1) is not null, 1, 0) as IsComment
                    , if ((select TargetUserID from userconnect where CurrentUserID = $user_id AND TargetUserID = FromUserID AND IsFavorite = 1) is not null, 1, 0) as IsFavorite
                from (
                    select 
                     ID as ActivityID
                    , UserID as FromUserID
                    , ID as RefID
                    , Content as Message
                    , IF (Photo != '', CONCAT('$photoUrl', Photo), '') AS Image
                    , 10 as Type
                    , 10 as ActionType
                    , Link
                    , CreatedDate
                    , `Like`
                    , Comment
                    
                   from promos
                   where IsPublished = 1 $cond
                   $order
                ) as t
                left join user u ON u.UserID = t.FromUserID
                ";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    function listPromoPostOfVendor($user_id, $target_user_id, $id = 0){
        $photoUrl = $this->config->item('s3_photo_url');
        $limit = 10;
        $where = '';
        if ($id > 0){
            $where .= " AND ID < $id";
        }
        $sql = "select     
                    ID as ActivityID
                    , UserID as FromUserID
                    , '' as FromUserName
                    , '' as Avatar
                    , ID as RefID
                    , Content as Message
                    , IF (Photo != '', CONCAT('$photoUrl', Photo), '') AS Image
                    , 10 as Type
                    , 10 as ActionType
                    , Link
                    , CreatedDate
                    , `Like`
                    , Comment
                    , if ((select ID from activitylike where ContentID = ID AND Type = 10 AND UserID = $user_id limit 1) is not null, 1, 0) as IsLike
                    , if ((select ID from activitycomment where ContentID = ID AND Type = 10 AND UserID = $user_id limit 1) is not null, 1, 0) as IsComment
                    , if ((select TargetUserID from userconnect where CurrentUserID = $user_id AND TargetUserID = FromUserID AND IsFavorite = 1) is not null, 1, 0) as IsFavorite
               from promos
               where UserID = $target_user_id AND IsPublished = 1 $where
               order by ID DESC
               limit $limit     
        ";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function getDetailPromoPost($id){
        $this->db->select('p.*, u.UserName');
        $this->db->from('promos p');
        $this->db->join('user u', 'p.UserID = u.UserID', 'left');
        $this->db->where('ID', $id);
        $query = $this->db->get();
        return $query->row();
    }
}
?>
