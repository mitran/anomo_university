<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class qanda_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insert($QGroupID, $QContent, $QType, $AContent, $QWeight)
    {
        $Order = '`' . Order . '`';

        // Hung mod... find the question order...
        $sql = 'Select max(`Order`) as QOrder from question where QuestionGroupID = ' . $QGroupID;
        $query = $this->db->query($sql);
        $QOrder = ($query->row()->QOrder) ? $query->row()->QOrder + 1 : 1;

        $newDataQ = array(
            'QuestionGroupID' => $QGroupID,
            'Content' => $QContent,
            'QuestionType' => $QType,
            $Order => $QOrder,
            'Weight'    => $QWeight,
            'CreatedDate' => date("Y-m-d H:i:s", time())
        );

        $this->db->insert('question', $newDataQ);
        $lastidadded = $this->db->insert_id();

        $answers = preg_split('/\,/', $AContent);
        $num_answers = count($answers);
        for ($i = 0; $i < $num_answers; $i++) {
            $newDataA = array(
                'QuestionID' => $lastidadded,
                'Content' => $answers[$i],
                'CreatedDate' => date("Y-m-d H:i:s", time())
            );
            $this->db->insert('answer', $newDataA);
        }

    }

    function insert_answer($AContent)
    {
        $newData = array(
            'Content' => $AContent
        );
        $this->db->insert('answer', $newData);
    }


    function update($ids, $columnname, $newvalue)
    {

        if ($columnname != 'id') { // One dirty hack to prevent the PK column to be updated

            $ids = explode("_", $ids);
            $qid = $ids[0];
            $qgid = $ids[1]; // Hung added... for updating question group info
            $aid = $ids[2];

            $aids = preg_split('/\,/', $aid);
            $answers = preg_split('/\,/', $newvalue);

            if ($columnname == 'Answers') {
                $num_answers = count($answers);
                $num_oldanswers = count($aids);

                for ($i = 0; $i < $num_oldanswers; $i++) {
                    $newData = array(
                        'Content' => $answers[$i]
                    );
                    $this->db->update('answer', $newData, array('AnswerID' => ($aids[$i])? $aids[$i] : ''));
                }

                // do the adding for new answers...
                if ($num_answers > $num_oldanswers) {
                    for ($j = $num_oldanswers; $j < $num_answers; $j++) {
                        $newDataA = array(
                            'QuestionID' => $qid,
                            'Content' => $answers[$j],
                            'CreatedDate' => date("Y-m-d H:i:s", time())
                        );
                        $this->db->insert('answer', $newDataA);
                    }
                }
            } elseif ($columnname == 'Category' || $columnname == 'ExpireDate') {
                $newData = array(
                    $columnname => $newvalue
                );
                $this->db->update('questiongroup', $newData, array('QuestionGroupID' => $qgid));
            } else {
                $newData = array(
                    $columnname => $newvalue
                );
                $this->db->update('question', $newData, array('QuestionID' => $qid));
            }
        }

    }

    function delete($id)
    {

        $this->db->where('QuestionID', $id);
        $this->db->delete('question');
        $this->db->where('QuestionID', $id);
        $this->db->delete('answer');
    }
}
