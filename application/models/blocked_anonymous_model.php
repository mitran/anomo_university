<?php
class Blocked_Anonymous_model extends CI_Model{
	function __construct() {
		parent::__construct();
	}
	
    
    function checkBlockAnonymous($current_user_id, $target_user_id)
    {
        $this->db->select('*');
        $this->db->from('anonymousblock');
        $this->db->where('CurrentUserID', $current_user_id);
        $this->db->where('TargetUserID', $target_user_id);
        $query = $this->db->get();
        $retVal = $query->row();
        $query->free_result();
        return $retVal;
    }
    
    function blockAnonymous($current_user_id, $target_user_id)
    {
        $data = array(
            'CurrentUserID' => $current_user_id,
            'TargetUserID' => $target_user_id,
            'Date' => gmdate('Y-m-d H:i:s')
        );
        return $this->db->insert('anonymousblock', $data);
    }

    function unBlockAnonymous($current_user_id, $target_user_id)
    {
        $this->db->delete('anonymousblock', array('CurrentUserID' => $current_user_id, 'TargetUserID' => $target_user_id));
    }
}       