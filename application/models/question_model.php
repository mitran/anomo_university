<?php
class Question_model extends CI_Model{
    function add($data) {
        $this->db->insert('question', $data);
        return $this->db->insert_id();
    }
    function update($data, $cond) {
        $this->db->update('question', $data, $cond);
    }
    function addAnswer($data) {
        $this->db->insert('answer', $data);
        return $this->db->insert_id();
    }
    function updateAnswer($data, $cond) {
        $this->db->update('answer', $data, $cond);
    }
    
    function getAnswerOf($question_id){
        $this->db->select('AnswerID, Content');
        $this->db->from('answer');
        $this->db->where('QuestionID', $question_id);
        $query = $this->db->get();
        return $query->result();
    }

    function getQuestionInfo($question_id, $group_id = 0){
        $this->db->select('*');
        $this->db->from('question');
        $this->db->where('QuestionID', $question_id);
        if ($group_id > 0){
            $this->db->where('QuestionGroupID', $group_id);
        }
        $query = $this->db->get();
        return $query->row();
    }

    function getAnswerInfo($answer_id, $question_id = 0){
        $this->db->select('*');
        $this->db->from('answer');
        $this->db->where('AnswerID', $answer_id);
        if ($question_id > 0){
            $this->db->where('QuestionID', $question_id);
        }
        $query = $this->db->get();
        return $query->row();
    }

    function getNextQuestion($question_group_id, $order){
        $this->db->select('*');
        $this->db->from('question');
        $this->db->where('QuestionGroupID', $question_group_id);
        $this->db->where('Order', $order + 1);
        $query = $this->db->get();
        return $query->row();
    }

    function getNumberQuestionOfGroup($question_group_id){
        $this->db->select('count(QuestionID) as num');
        $this->db->from('question');
        $this->db->where('QuestionGroupID', $question_group_id);
        $query = $this->db->get();
        $row = $query->row();
        return ($row)?$row->num:0;
    }

    function getQuestionOfGroup($question_group_id){
        $this->db->select('*');
        $this->db->from('question');
        $this->db->where('QuestionGroupID', $question_group_id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getNumberAnswerOfQuestion($question_id){
        $this->db->select('count(AnswerID) as num');
        $this->db->from('answer');
        $this->db->where('QuestionID', $question_id);
        $query = $this->db->get();
        $row = $query->row();
        return ($row)?$row->num:0;
    }

    function getNextOrder($questionGroupID){
        $this->db->select('count(QuestionID) as num');
        $this->db->from('question');
        $this->db->where('QuestionGroupID', $questionGroupID);
        $query = $this->db->get();
        $row = $query->row();
        $num = ($row)?$row->num:0;
        return $num+1;
    }

    function addQuestionGroup($data) {
        $this->db->insert('questiongroup', $data);
        return $this->db->insert_id();
    }
    function getListDropDownQuestionGroup(){
        $this->db->select('*');
        $this->db->from('questiongroup');
        $query = $this->db->get();
        $result = $query->result();
        $data = array();
        foreach ($result as $re) {
            $data[$re->QuestionGroupID] = 'Group '.$re->QuestionGroupID;
        }
        return $data;
    }
    
    function getListByCondition($cond, &$total, $offset = 0, $limit = 0) {
        if (count($cond) > 0) {
            if (isset($cond['keyword'])) {
                $where[] = "(`Content` LIKE '%{$cond['keyword']}%')";
            }
            if (isset($cond['group'])) {
                $where[] = "QuestionGroupID='" . $cond['group'] . "'";
            }
        }
        $sWhere = '';
        if (isset($where)) {
            $sWhere = "where " . implode(' AND', $where);
        }
        $sqlTotal = "select count(QuestionID) as Total from question $sWhere";

        $totalResult = $this->db->query($sqlTotal);
        $totalRow = $totalResult->row();
        $total = $totalRow ? $totalRow->Total : 0;
        if ($total > 0) {
            $sql = "select * from (select * from question $sWhere order by QuestionID DESC  limit $limit, $offset  ) as q
                    left join questiongroup qg ON q.QuestionGroupID=qg.QuestionGroupID
                    ";

            if (isset($cond['sort'])) {
                switch ($cond['sort']) {
                    case 'id-asc':
                        $sql .= " order by QuestionID asc";
                        break;
                    case 'id-desc':
                        $sql .= " order by QuestionID desc";
                        break;
                    case 'group-asc':
                        $sql .= " order by q.QuestionGroupID asc";
                        break;
                    case 'group-desc':
                        $sql .= " order by q.QuestionGroupID desc";
                        break;
                }
            } else {
                $sql .= " order by QuestionID DESC";
            }

            $query = $this->db->query($sql);
            return $query->result();
        }
        return null;
    }

    function deleteAnswer($answerId){
        $result = $this->db->delete('answer', array('AnswerID' => $answerId));
        if ($result){
            return 1;
        }
        else{
            return 0;
        }
    }
    function getNumberAnswerOfUser($userId){
        $this->db->select('count(AnswerID) as num');
        $this->db->from('result');
        $this->db->where('UserID', $userId);
        $query = $this->db->get();
        $row = $query->row();
        return ($row)?$row->num:0;
    }

    function getQuestionPlayed($user_id, $category){
        $this->db->select('QuestionGroupIDs');
        $this->db->from('questionplayed');
        $this->db->where('UserID', $user_id);
        $this->db->where('Category', $category);
        $query = $this->db->get();
        $row = $query->row();
        return $row?$row->QuestionGroupIDs:'';
    }

    function updateQuestionPlayed($data, $cond){
        return $this->db->update('questionplayed', $data, $cond);
    }

    function addQuestionPlayed($data) {
        $this->db->insert('questionplayed', $data);
        return $this->db->insert_id();
    }

    function randomQuestion($category = '', $question_group_ids = ''){
        $sql = "select * from question q 
                where QuestionGroupID = ( select t.* from(
                           select QuestionGroupID from questiongroup  qg
                           where (qg.ExpireDate Is Null or qg.ExpireDate = '0000-00-00' OR qg.ExpireDate > CURDATE())";
        if ($category != ''){
            $sql .= " AND qg.Category IN ( '$category', 'EVERYONE') ";
        }
        if ($question_group_ids != ''){
            $aIds = explode(',', $question_group_ids);
            $aIds = array_filter($aIds);
            $question_group_ids = implode(',', array_unique($aIds));
            $sql .= "AND qg.QuestionGroupID NOT IN (". $question_group_ids.")";
        }
        $sql .= " order by RAND() limit 1";
        $sql .= ") as t )";
        $sql .= " order by q.Order asc";

        $query = $this->db->query($sql);
        return $query->result();
    }

    function sortBy($array, $key) {
        $new_array = array();
        $sortable_array = array();
        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                $sortable_array[$k] = $v->$key;
            }
            asort($sortable_array);
            foreach ($sortable_array as $k => $v) {
                $new_array[] = $array[$k];
            }
        }
        return $new_array;
    }

    // for game 4 people
    function validateGameResult($oData) {
        foreach ($oData->Questions as $oQuestion) {
            if (!isset($oQuestion->QuestionID) || !isset($oQuestion->AnswerID) || !isset($oQuestion->Order)) {
                return false;
            }
            $cond[] = "(a.AnswerID= $oQuestion->AnswerID AND a.QuestionID= $oQuestion->QuestionID AND q.QuestionGroupID = $oData->QuestionGroupID AND q.Order = $oQuestion->Order)";
        }
        if (sizeof($cond) > 0) {
            $sql = "select q.Weight, q.QuestionID from answer a 
                    left join question q ON a.QuestionID=q.QuestionID
                    where " . implode(' OR ', $cond);
            $query = $this->db->query($sql);
            $result = $query->result();
            $numberQuestion = $result ? sizeof($result) : 0;
            if ($numberQuestion != sizeof($oData->Questions)) {
                return false;
            }else{
                foreach($oData->Questions as $oQuestion){
                    foreach($result as $row){
                        if ($oQuestion->QuestionID == $row->QuestionID){
                            $oQuestion->Weight = $row->Weight;
                        }
                    }
                }
            }
        }
        return $oData;
    }
    
    
    function createGame4People($user_id, $oData) {
        $is_finished = 0;
        try {
            $this->db->trans_begin();

            $question_group_id = $oData->QuestionGroupID;
            $anomotion_id = $oData->AnomotionID;
            $created_date = gmdate('Y-m-d H:i:s');
            $listQuestions = self::sortBy($oData->Questions, 'Order');
            $aAnomotionIDs = array($anomotion_id);
            // if new game
            if ($anomotion_id == -1){
                // 1. create new game
                // create 4 games
                $sql = "INSERT INTO anomotion (`UserID`,`TargetUserID`,`Status`,`QuestionGroupID`,`CreatedDate`) VALUES";
                foreach($oData->UserID as $partner_id){
                    $value [] = " ($user_id, $partner_id, 1, $question_group_id, '$created_date')";
                }
                $sql .= implode(',', $value);
                $this->db->query($sql);
                $game_id = $this->db->insert_id();

                // get gameid has been created
                $aAnomotionIDs = array();
                for($i = 0; $i < count($oData->UserID); $i ++){
                    $aAnomotionIDs[] = $game_id + $i;
                }
                
                // 2. add to notification history
                $sql1 = "INSERT INTO notificationhistory(`AnomotionID`,`UserID`,`SendUserID`,`Type`,`Total`,`IsRead`, `UnView`,`CreatedDate`,`IsFacebookGame`)  ";
                $sql1 .= " select AnomotionID, TargetUserID, '$user_id' as SendUserID, '3' as Type, '1' as Total, '1' as IsRead, '1' as UnView, '$created_date' as CreatedDate, '0' as IsFacebookGame from anomotion where AnomotionID IN (".  implode(',', $aAnomotionIDs).") ";
                $this->db->query($sql1);
                
                // if this is game 4 people then
                if (sizeof($aAnomotionIDs) > 1){
                // 3. log this groupquestion as played
                // get category 
                    $s = "select Category from questiongroup where QuestionGroupID = $question_group_id";
                    $q = $this->db->query($s);
                    $row = $q->row();
                    $category = $row?$row->Category:'';

                    $aPartnerID = $oData->UserID;
                    $aPartnerID[] = $user_id;
                    $sUserID = implode(',', array_values($aPartnerID));
                    $sql = "select * from questionplayed where UserID IN ($sUserID) AND Category = '$category'";
                    $query = $this->db->query($sql);
                    $result = $query->result();
                    if ($result){
                        foreach($result as $re){
                            if (in_array($re->UserID, array_values($aPartnerID))){
                                $newQuestionGroupIDs = ($re->QuestionGroupIDs)?$re->QuestionGroupIDs.','.$question_group_id:$question_group_id;
                                $subQ = " update questionplayed set QuestionGroupIDs = '$newQuestionGroupIDs' where UserID=$re->UserID AND Category='$category'";
                                $this->db->query($subQ);
                            }else{
                                $subQ = " insert into questionplayed (`UserID`,`QuestionGroupIDs`,`Category`) value";
                                $subQ .= " ('$re->UserID', $question_group_id, '$category')";
                                $this->db->query($subQ);
                            }
                        }
                    }else{
                        $subQ = " insert into questionplayed (`UserID`,`QuestionGroupIDs`,`Category`) values";
                        foreach($aPartnerID as $id){
                            $v[] = "($id, $question_group_id, '$category')";
                        }
                        $subQ .= implode(',', $v);
                        $this->db->query($subQ);
                    }
                }
            }
            
            // if is existed game - should delete all answer of game (incase this game is created on V1 version and this game is not completed)
            if($anomotion_id > 0){ 
                $sql = " delete from result where AnomotionID = $anomotion_id AND UserID = $user_id";
                $this->db->query($sql);
            }
            // add game result
            // for requester  
            $result_unique = array();
            $result_value = array();
            foreach ($aAnomotionIDs as $anomotionID) {
                foreach ($listQuestions as $oQuestion) {
                    $result_value[] = " ($anomotionID, $user_id, $oQuestion->QuestionID, $oQuestion->AnswerID, '$created_date')";
                    // ANOMO-11832 UPSERT resultunique table
                    $sql = "select * from resultunique where UserID = $user_id AND QuestionID = $oQuestion->QuestionID";
                    $query = $this->db->query($sql);
                    $row = $query->row();
                    if ($row){
                        $sql_update = "update resultunique set AnomotionID = $anomotionID, AnswerID = $oQuestion->AnswerID, CreatedDate = '$created_date', Weight = $oQuestion->Weight where ID = ".$row->ID;
                        $this->db->query($sql_update);
                    }else{
                        $result_unique[$user_id.'_'.$oQuestion->QuestionID] = " ($anomotionID, $user_id, $oQuestion->QuestionID, $oQuestion->AnswerID, '$created_date', $oQuestion->Weight)";
                    }
                }
            }
            if (sizeof($result_value) > 0) {
                $sql = "INSERT INTO result(`AnomotionID`,`UserID`,`QuestionID`,`AnswerID`,`CreatedDate`) VALUES";
                $sql .= implode(',', $result_value);  
                $this->db->query($sql);
            }
            
            // ANOMO-11832 UPSERT resultunique table
            if (sizeof($result_unique) > 0){
                $sql = "INSERT INTO resultunique(`AnomotionID`,`UserID`,`QuestionID`,`AnswerID`,`CreatedDate`, `Weight`) VALUES";
                $sql .= implode(',', array_values($result_unique));
                $this->db->query($sql);
            }
            
            // if is existed game its mean this game become finished 
            // then push notify to partner and yourself

            if ($anomotion_id > 0){ // it means answer single game
                $target_user_id = $oData->UserID[0];
                
                // check 2 user is finised game
                $sql = "select (select count(ID)  from result where AnomotionID = $anomotion_id AND AnswerID > 0) as TotalAnswer
                    , (select count(QuestionID) from question where QuestionGroupID = $question_group_id) as TotalQuestion
                ";
                $query = $this->db->query($sql);
                $isFinishedGame = $query->row();
                if ($isFinishedGame){
                    if ($isFinishedGame->TotalAnswer == 2*(int)$isFinishedGame->TotalQuestion){
                        //1. Calculate #score & Update status game to finished
                        $score = self::_calculateScore($anomotion_id, $user_id, $target_user_id, $question_group_id);
                        $updateSql = "update anomotion set Score = '$score', Status = 3 where AnomotionID = '$anomotion_id'";
                        $this->db->query($updateSql);

                        //2. Push notify & save notification history           
                        $sql1 = "INSERT INTO notificationhistory(`AnomotionID`,`UserID`,`SendUserID`,`Type`,`Total`,`IsRead`, `UnView`,`CreatedDate`, `Score`,`IsFacebookGame`)  ";
                        $sql1 .= " values ";
                        $sql1 .= " ($anomotion_id, $user_id, $target_user_id, 4, 1, 1, 1, '$created_date', '$score', 0)";
                        $sql1 .= " ,($anomotion_id, $target_user_id, $user_id, 4, 1, 1, 1, '$created_date', '$score', 0)";
                        $this->db->query($sql1);
                        
                        // set this game is finished
                        $is_finished = 1;
                    }
                }
            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();

            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }

        return isset($aAnomotionIDs)?array('ids' => $aAnomotionIDs, 'is_finished' => $is_finished):false;
    }
    
    private function _calculateScore($anomotion_id, $user_id, $target_user_id, $question_group_id){
        $result1 = $this->Result_model->getListResultOf($user_id, $anomotion_id);
        $result2 = $this->Result_model->getListResultOf($target_user_id, $anomotion_id);
        $sameResult = array_intersect_key($result1, $result2);
        $numberQuestion = $this->Question_model->getNumberQuestionOfGroup($question_group_id);
        $percentPerQuestion = 100 / $numberQuestion;
        $percent = 0;
        if ($sameResult) {
            foreach ($sameResult as $key => $value) {
                if ($result1[$key] == $result2[$key]) {
                    $percent += $percentPerQuestion;
                }
            }
        }

        return $percent;
    }


}
?>