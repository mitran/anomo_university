<?php

class Gift_Send_model extends CI_Model {

    public $cached = false;

    function __construct() {
        parent::__construct();
        $CI = & get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
    }

    function findById($id) {
        $this->db->select('*');
        $this->db->from('giftsend');
        $this->db->where('ID', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getMyReceivedGift($user_id) {
        $this->db->select('g.GiftID, g.Name, g.Description, g.DescriptionOnBuying, gc.ID as GiftCategoryID, gc.Name as GiftCategoryName, g.Photo, g.Photo194, g.Photo454, g.Credits, gs.SendUserID, u.UserName, u.Gender, gs.CreatedDate');
        $this->db->from('giftsend gs');
        $this->db->join('gift g', 'g.GiftID=gs.GiftID');
        $this->db->join('giftcategory gc', 'g.GiftCategoryID=gc.ID', 'left');
        $this->db->join('user u', 'u.UserID=gs.SendUserID AND u.IsBanned !=1', 'inner'); //ANOMO-5526
        $this->db->where('gs.ReceiveUserID', $user_id);
        $this->db->order_by('gs.ID', 'DESC');
        $query = $this->db->get();
        $result = $query->result();

        $data = array();
        if ($result) {
            for ($i = 0; $i < count($result); $i++) {
                for ($j = $i; $j <= $i; $j++) {
                    if ($result[$i]->GiftCategoryID == $result[$j]->GiftCategoryID) {
                        $data[$result[$i]->GiftCategoryName][] = array(
                            'GiftID' => $result[$i]->GiftID,
                            'Name' => $result[$i]->Name,
                            'Description' => $result[$i]->Description,
                            'DescriptionOnBuying' => $result[$i]->DescriptionOnBuying,
                            'Credits' => $result[$i]->Credits,
                            'Photo' => $result[$i]->Photo != NULL ? $this->config->item('s3_photo_url') . $result[$i]->Photo : '',
                            'Photo194' => $result[$i]->Photo194 != NULL ? $this->config->item('s3_photo_url') . $result[$i]->Photo194 : '',
                            'Photo454' => $result[$i]->Photo454 != NULL ? $this->config->item('s3_photo_url') . $result[$i]->Photo454 : '',
                            'SendUserID' => $result[$i]->SendUserID,
                            'UserName' => $result[$i]->UserName,
                            'Gender' => $result[$i]->Gender,
                            'CreatedDate' => $result[$i]->CreatedDate,
                            'IsBlock' => User_Model::checkBlock($user_id, $result[$i]->SendUserID),
                            'IsBlockTarget' => User_Model::checkBlock($result[$i]->SendUserID, $user_id)
                        );
                    }
                }
            }
        }

        $return = array();
        foreach ($data as $key => $value) {
            $return[] = array(
                'CategoryName' => $key,
                'ListGift' => $value
            );
        }
        return $return;
    }

    function getMyGiftHistory($user_id) {
        // ANOMO-5526
        $sql = "select g.GiftID, g.Name, gs.SendUserID, gs.ReceiveUserID, gs.CreatedDate, u1.UserName as SendUserName, u2.UserName as ReceiveUserName
				from giftsend gs
				left join gift g ON g.GiftID=gs.GiftID
				inner join user u1 ON u1.UserID=gs.SendUserID AND u1.IsBanned != 1
				inner join user u2 ON u2.UserID=gs.ReceiveUserID AND u2.IsBanned != 1
				where (gs.ReceiveUserID = $user_id OR gs.SendUserID = $user_id)
		";
        $query = $this->db->query($sql);
        $result = $query->result();
        $data['GiftHistory'] = $result;
        $data['CreditHistory'] = $this->getMyCreditHistory($user_id);
        return $data;
    }

    function getMyCreditHistory($user_id) {
        $this->db->select('ct.TransactionID, ct.CreditPackageID, cp.Credits, cp.Price, ct.CreatedDate');
        $this->db->from('credittransaction ct');
        $this->db->join('creditpackage cp', 'cp.ID=ct.CreditPackageID', 'left');
        $this->db->where('ct.UserID', $user_id);
        $this->db->where('ct.CreditPackageID >', 0);
        $query = $this->db->get();
        return $query->result();
    }

    function getGiftInfo($gift_id) {
        $this->db->select('*');
        $this->db->from('gift');
        $this->db->where('GiftID', $gift_id);
        $query = $this->db->get();
        return $query->row();
    }

    function sendGift($data) {
        $this->db->insert('giftsend', $data);
        return $this->db->insert_id();
    }

    function getListGift() {
        $this->db->select('*');
        $this->db->from('gift');
        $query = $this->db->get();
        return $query->result();
    }

    function getAllGiftDividedByCategory() {
        $this->db->select('g.GiftCategoryID, gc.Name as GiftCategoryName');
        $this->db->from('gift g');
        $this->db->join('giftcategory gc', 'g.GiftCategoryID=gc.ID', 'left');
        $this->db->group_by('g.GiftCategoryID');
        $query = $this->db->get();
        $result = $query->result();
        $data = array();
        if ($result) {
            foreach ($result as $re) {
                $data[] = array(
                    'CategoryName' => $re->GiftCategoryName,
                    'ListGift' => self::getListGiftByCategory($re->GiftCategoryID)
                );
            }
        }
        return $data;
    }

    function getListGiftByCategory($category_id) {
        $sql = " select *, 
				IF (Photo != '', CONCAT('" . $this->config->item('s3_photo_url') . "',Photo), '') as Photo,
				IF (PricePhoto != '', CONCAT('" . $this->config->item('s3_photo_url') . "',PricePhoto), '') as PricePhoto,
				IF (Photo194 != '', CONCAT('" . $this->config->item('s3_photo_url') . "',Photo194), '') as Photo194,
				IF (Photo454 != '', CONCAT('" . $this->config->item('s3_photo_url') . "',Photo454), '') as Photo454
				from gift
				where GiftCategoryID = $category_id
				";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function getTotalGiftOfUser($user_id) {
        $this->db->select('count(ID) as Total');
        $this->db->from('giftsend');
        $this->db->where('ReceiveUserID', $user_id);
        $query = $this->db->get();
        $result = $query->row();
        return ($result) ? $result->Total : 0;
    }

    function deleteGiftSend($id, $user_id = 0) {
        try {
            Activity_model::invalidUserCached($user_id);
             
            $this->db->trans_begin();

            $commentIDs = Activity_model::getCommentIDOf($id, 9);
            if (count($commentIDs) > 0) {
                $this->db->delete('notificationhistory', "RefID IN (" . implode(',', $commentIDs) . ") AND Type=14 AND ContentType=9"); // comment activity
            }
            $likeIDs = Activity_model::getLikeIDOf($id, 9);
            if (count($likeIDs) > 0) {
                $this->db->delete('notificationhistory', "RefID IN (" . implode(',', $likeIDs) . ") AND Type=13 AND ContentType=9"); // like activity
            }

            $this->db->delete('activitycomment', array('ContentID' => $id, 'Type' => 9));
            $this->db->delete('activitylike', array('ContentID' => $id, 'Type' => 9));
            $this->db->delete('activity', array('RefID' => $id, 'Type' => 9));
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }
    }

    function getReceivedGift($user_id, &$total, $offset, $limit) {
        $sqlTotal = "select count(ID) as Total from giftsend where ReceiveUserID = $user_id";
        $queryTotal = $this->db->query($sqlTotal);
        $rowTotal = $queryTotal->row();
        $total = $rowTotal ? $rowTotal->Total : 0;
        $result = array();
        if ($total > 0) {
            $photo_url = $this->config->item('s3_photo_url');
            $sql = "select tmp.SendUserID, u.UserName as SendUserName, u.BirthDate, g.Description, tmp.CreatedDate
                            ,IF (u.Photo != '', CONCAT('" . $photo_url . "', u.Photo), '') as Avatar
                            ,IF (g.Photo != '', CONCAT('" . $photo_url . "', g.Photo), '') as Photo
                            ,IF (g.Photo194 != '', CONCAT('" . $photo_url . "', g.Photo194), '') as Photo194
                            ,IF (g.Photo454 != '', CONCAT('" . $photo_url . "', g.Photo454), '') as Photo454
                            ,u.NeighborhoodID
                            ,if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                                    if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), nbh.NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) )AS NeighborhoodName     	
                   from (
                        select SendUserID, CreatedDate, GiftID from giftsend where ReceiveUserID = $user_id limit $offset, $limit
                   ) as tmp
                   left join user u ON u.UserID = tmp.SendUserID
                   left join gift g ON g.GiftID = tmp.GiftID
                   left join neighborhood nbh ON nbh.OGR_FID = u.NeighborhoodID";
            $query = $this->db->query($sql);
            $result = $query->result();
        }
        return $result;
    }

}

?>