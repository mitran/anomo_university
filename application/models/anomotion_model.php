<?php

class Anomotion_model extends CI_Model {
    public $cached = false;
    function __construct() {
        parent::__construct();
        $CI = & get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
    }

    function update($data, $cond) {
        return $this->db->update('anomotion', $data, $cond);
    }

    function delete($cond) {
        $this->db->delete('anomotion', $cond);
    }

    function requestAnomotion($data) {
        $this->db->insert('anomotion', $data);
        return $this->db->insert_id();
    }
    
    function isHavePendingGame($user_id, $target_user_id, $anomotion_id){
        $this->db->select('*');
        $this->db->from('anomotion');
        $this->db->where("((UserID = $user_id AND TargetUserID = $target_user_id) OR (UserID = $target_user_id AND TargetUserID = $user_id))");
        if ($anomotion_id > 0){
            $this->db->where('AnomotionID', $anomotion_id);
        }else{
            $this->db->where('Status', 1);
        }
        $this->db->order_by('CreatedDate', 'DESC');
        $this->db->limit(1, 0);
        $query = $this->db->get();
        return $query->row();
    }
    
    function isCompleteGame($user_id, $anomotion_id, $question_group_id){
        $sql = "select (select count(ID)  from result where UserID = $user_id AND AnomotionID = $anomotion_id AND AnswerID > 0) as TotalAnswer
                    , (select count(QuestionID) from question where QuestionGroupID = $question_group_id) as TotalQuestion
                ";
        $query = $this->db->query($sql);
        return $query->row();
    }

    function checkRequest($user_id, $target_user_id) {
        $this->db->select('*');
        $this->db->from('anomotion');
        $this->db->where("((UserID = $user_id AND TargetUserID = $target_user_id) OR (UserID = $target_user_id AND TargetUserID = $user_id))");
        $this->db->where("Status !=", 2); // cancel
        $this->db->where('Status !=', 3); // finish
        $this->db->order_by('CreatedDate', 'DESC');
        $this->db->limit(1, 0);
        $query = $this->db->get();
        return $query->row();
    }

    function checkAnomotion($user_id, $anomotion_id) {
        $this->db->select('*');
        $this->db->from('anomotion');
        $this->db->where('AnomotionID', $anomotion_id);
        if ($user_id > 0) {
            $sql = "(UserID = $user_id OR TargetUserID = $user_id)";
            $this->db->where($sql);
        }
        $query = $this->db->get();
        return $query->row();
    }

    function getAnomotion($anomotion_id) {
        $this->db->select('*');
        $this->db->from('anomotion');
        $this->db->where('AnomotionID', $anomotion_id);
        $query = $this->db->get();
        return $query->row();
    }

    function getAnomotionsByUserID($userID) {
        $this->db->select('a.UserID, a.AnomotionID, a.QuestionGroupID, a.Status, a.CreatedDate, u.UserID as TargetUserID, u.UserName as TargetUserName, u.Photo as TargetAvatar');
        $this->db->from('anomotion a');
        $this->db->join('user u', "u.UserID = a.UserID OR u.UserID = a.TargetUserID");
        $this->db->where("u.UserID != $userID");
        $this->db->where("(a.UserID = $userID OR a.TargetUserID = $userID)");
        $query = $this->db->get();
        return $query->result();
    }

    function getAnomotionsBy2User($userID, $targetUserID) {
        $currentTime = gmdate("Y-m-d H:i:s", strtotime($this->config->item('expired')));
        $this->db->select('a.*, u1.UserName, u2.UserName as TargetUserName, u1.Photo as Avatar, u2.Photo as TargetAvatar');
        $this->db->from('anomotion a');
        $this->db->join('user u1', "u1.UserID = a.UserID");
        $this->db->join('user u2', "u2.UserID = a.TargetUserID");
        $this->db->where("((a.UserID = $userID AND a.TargetUserID = $targetUserID AND a.IsUserDelete = 0) OR (a.UserID = $targetUserID AND a.TargetUserID = $userID AND a.IsTargetUserDelete = 0))");

        $sql = "((a.Status = 1 AND a.CreatedDate >= '$currentTime') OR (a.Status = 3))";
        $this->db->where($sql);
        $this->db->order_by('a.CreatedDate', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    function getAllAnomotionBy2User($user_id, $target_user_id) {
        $this->db->select('*');
        $this->db->from('anomotion a');
        $this->db->where("((a.UserID = $user_id AND a.TargetUserID = $target_user_id ) OR (a.UserID = $target_user_id AND a.TargetUserID = $user_id ))");
        $query = $this->db->get();
        return $query->result();
    }

    function getResult($anomotion_id, $user_id) {
        $this->db->select('*, q.Content as QuestionContent');
        $this->db->from('result r');
        $this->db->join('question q', 'q.QuestionID=r.QuestionID');
        $this->db->join('answer a', 'a.AnswerID=r.AnswerID');
        $this->db->where('r.AnomotionID', $anomotion_id);
        $this->db->where('r.UserID', $user_id);
        $this->db->order_by('q.Order', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    function checkAnomotionIsFinished($anomotion_id, $user_id) {
        $this->db->select('a.AnomotionID');
        $this->db->from('anomotion a');
        $this->db->where('a.AnomotionID', $anomotion_id);
        $this->db->where("(a.UserID = $user_id OR a.TargetUserID = $user_id)");
        $this->db->where('a.Status', 3);
        $query = $this->db->get();
        return $query->row();
    }
    
    function getHistoryIdOfGame($user_id, $type, $game_ids){
        $sql = "select t.*, u.AllowAnomotionNotice
                from (
                    select ID, UserID, Type, AnomotionID from notificationhistory 
                    where SendUserID = $user_id AND Type = $type AND AnomotionID IN (".  implode(',', $game_ids).")
                ) as t
                left join user u ON u.UserID = t.UserID 
               ";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function _getFinishedGame($anomotion_id){
        $sql = "select r.QuestionID, q.Content as Question, r.UserID, a.Content as Answer
                from result r
                left join question q ON q.QuestionID = r.QuestionID
                left join answer a ON a.AnswerID = r.AnswerID
                where r.AnomotionID = $anomotion_id";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function getFinishedGame($anomotion_id){
        if ($this->cached){
            $cached = $this->cached->get('game_finished'.$anomotion_id);
            if ($cached){
                return $cached;
            }else{
                $results = self::_getFinishedGame($anomotion_id);
                if ($results){
                    $this->cached->set('game_finished'.$anomotion_id, $results);
                }
                return $results;
            }
        }else{
            return self::_getFinishedGame($anomotion_id);
        }
    }
    
    function countAnomotionByUser($user_id){
        $this->db->select('count(AnomotionID) as total');
        $this->db->from('anomotion');
        $this->db->where('UserID', $user_id);
        $query = $this->db->get();
        $result = $query->result();        
        if($result[0]){
            return $result[0]->total;
        }
        return 0;
    }
    
}

?>