<?php

class Flag_Content_model extends CI_Model {

    function add($data) {
        $this->db->insert('flagcontent', $data);
        $ret = $this->db->insert_id();
        $sql = "update user set FlagsCount = FlagsCount + 1 where UserID = " . $data['ContentOwnerID'];
        $this->db->query($sql);
        return $ret;
    }

    function update($data, $cond) {
        return $this->db->update('flagcontent', $data, $cond);
    }

    function isAddFlag($user_id, $content_id, $type, $is_comment = 0) {
        $this->db->select('ID');
        $this->db->from('flagcontent');
        $this->db->where('UserID', $user_id);
        $this->db->where('ContentID', $content_id);
        $this->db->where('Type', $type);
        $this->db->where('IsComment', $is_comment);
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? 1 : 0;
    }

    function getListByCondition($cond, &$total, $offset = 0, $limit = 0) {
        $where = '';
        if (isset($cond['flag'])) {
            $where .= " where ContentOwnerID=" . $cond['flag'];
        }
        $tSql = "select count(ID) as Total from flagcontent $where";
        $tQuery = $this->db->query($tSql);
        $row = $tQuery->row();
        $total = ($row)?$row->Total:0;
        
        $sql = "select f.*, u.UserName, u1.UserName as ContentOwner
				from (
                                    select * from flagcontent $where 
                                    order by CreatedDate DESC
                                    limit $limit, $offset
                                    ) as f
				left join user u ON (u.UserID = f.UserID)
				left join user u1 ON (u1.UserID=f.ContentOwnerID)
		";
        $query1 = $this->db->query($sql);
        return $query1->result();
    }

    function delete($cond) {
        $this->db->delete('flagcontent', $cond);
    }
    
    function isReport($content_id, $type, $is_comment){
        $this->db->select('*');
        $this->db->from('flagcontent');
        $this->db->where('ContentID', $content_id);
        $this->db->where('Type', $type);
        $this->db->where('IsComment', $is_comment);
        $query = $this->db->get();
        return $query->result();
    }

}

?>