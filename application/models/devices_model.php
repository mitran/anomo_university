<?php

class Devices_model extends CI_Model {

    function add($data) {
        $this->db->insert('devices', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('devices', $data, $cond);
    }

    function getDevice($user_id, $device_id) {
        $this->db->select('*');
        $this->db->from('devices');
        $this->db->where('UserID', $user_id);
        $this->db->where('DeviceID', $device_id);
        $query = $this->db->get();
        return $query->row();
    }

    function getDeviceOfUser($user_id) {
        $this->db->select('d.*, u.AllowSendGiftNotice, u.AllowRevealNotice, u.AllowChatNotice');
        $this->db->from('devices d');
        $this->db->join('user u', 'u.UserID=d.UserID');
        $this->db->where('d.UserID', $user_id);
        $this->db->where('d.Status', 'active');
        $query = $this->db->get();
        return $query->row();
    }

    function getDeviceOfFbUser($user_id)
    {
        $sql = "select u.UserID, d.Type, d.DeviceID from (
	                select UserID from `user` where FacebookID = '" . $user_id . "'
                  ) as u
                  left join (
	                select UserID, Type, DeviceID from devices where `Status` = 'active') as d
                  on u.UserID = d.UserID";
        $query = $this->db->query($sql);
        return $query->row();
    }

}

?>