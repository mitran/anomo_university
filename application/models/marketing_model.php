<?php

class Marketing_model extends CI_Model {

    function addSchedule($data) {
        $this->db->insert('schedule', $data);
        return $this->db->insert_id();
    }

    function addSocialMedia($data) {
        $this->db->insert('socialmedia', $data);
        return $this->db->insert_id();
    }

    function getListSchedule(&$total, $from = 0, $to = 0) {
        $sql = "select * from schedule";
        $query = $this->db->query($sql);
        $total = $query->num_rows();
        $sql .= " order by Week DESC";
        $sql .= " limit $to, $from";
        $query1 = $this->db->query($sql);
        return $query1->result();
    }

    function getUserRegister($from, $to) {
        $this->db->select('UserID, SignUpDate');
        $this->db->from('user');
        if ($from != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d')>='" . $from . "'");
        }
        if ($to != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d')<='" . $to . "'");
        }
        $query = $this->db->get();
        return $query->result();
    }

    function getListSocialMedia(&$total, $from = 0, $to = 0) {
        $sql = "select * from socialmedia";
        $query = $this->db->query($sql);
        $total = $query->num_rows();
        $sql .= " order by Week DESC";
        $sql .= " limit $to, $from";
        $query1 = $this->db->query($sql);
        return $query1->result();
    }

    function getListValues($from, $to) {
        $this->db->select('AdDollar, Day');
        $this->db->from('marketingvalue');
        $this->db->where("Day between '$from' AND '$to'");
        $query = $this->db->get();
        return $query->result();
    }

    function addValue($data) {
        $this->db->insert('marketingvalue', $data);
        return $this->db->insert_id();
    }

}

?>