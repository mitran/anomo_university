<?php
class User_model extends CI_Model
{
    private $timezone = '- 7 hour'; // PST time
    private $pst_start = '';
    private $pst_end = '';
    public $cached = false;


    function __construct()
    {
        parent::__construct();
        $CI =& get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();

        $pst = gmdate('Y-m-d', strtotime($this->timezone)); //GMT date
        $pst_start = date('Y-m-d H:i:s', strtotime($pst . '07:00:00'));
        $tmp_date = strtotime($pst . '06:59:59');
        $pst_end = date('Y-m-d H:i:s', strtotime('+1 days', $tmp_date));
        $this->pst_start = $pst_start;
        $this->pst_end = $pst_end;
    }

    function add($data)
    {
        log_message('error', '----- log add -----');
        $logMsg = var_export($data, TRUE);
        log_message('error', $logMsg);
        log_message('error', date('l jS \of F Y h:i:s A'));
        $this->db->insert('user', $data);
        return $this->db->insert_id();
    }

    function update($user_id, $data)
    {
        if (array_key_exists("Password", $data)) {
            $data['Password'] = md5($data['Password']);
        }

        // Hard coded for user JENNY RED PANDA GIRL! - to be moved into config later... but let's put it here for now. @Hung Pham
        if ($user_id == 32772) {
            $data['NeighborhoodID'] = 27578;
            $data['Lat'] = 47.668850;
            $data['Lon'] = 122.333006;
        }

        $ret = $this->db->update('user', $data, array('UserID' => $user_id));
        self::invalidateCache($user_id);

        return $ret;
    }

    public function invalidateCache($user_id) {
        if ($this->cached) {
            $this->cached->delete('user' . $user_id);
            $this->cached->delete('user_detail' . $user_id);
            $this->cached->delete('neighborhood_user' . $user_id);
        }
    }

    function updateLastActivity($user_id, $force_update = 0)
    {
        self::invalidateCache($user_id);
        if ($force_update || UPDATE_LAST_ACTIVITY)
            return $this->db->update('user', array('LastActivity' => gmdate('Y-m-d H:i:s')), array('UserID' => $user_id));
    }

    /**
     * login
     *
     * @param string $username or $email
     * @param string $password
     * @return array
     */
    function login($user_name, $password) {
        $photo_url = $this->config->item('s3_photo_url');
        $sql = "select UserID, UserName, Gender, BirthDate, IsBanned, Deactive, Credits, GenderDating, IsVendor, IsAdmin,IsVerify, IsEmailVerify
                        , if (Email is null, '', Email) as Email
                        , if (FacebookID is null, '', FacebookID) as FacebookID
                        , if (FbEmail is null, '', FbEmail) as FbEmail
                        , if (ProfileStatus is null, '', ProfileStatus) as ProfileStatus
                        , if (AboutMe is null, '', AboutMe) as AboutMe
                        , IF (Photo != '', CONCAT('" . $photo_url . "', Photo), '') as Avatar
                        , IF (FullPhoto != '', CONCAT('" . $photo_url . "', FullPhoto), '') as FullPhoto
                        , IF (CoverPicture != '', CONCAT('" . $photo_url. "', CoverPicture), CONCAT('" . $photo_url. "','".COVER_PICTURE_DEFAULT."') ) as CoverPicture
                        , AllowSendGiftNotice, AllowRevealNotice, AllowChatNotice, AllowAnomotionNotice, AllowCommentActivityNotice, AllowLikeActivityNotice, AllowFollowNotice, OnlyShowCountry
                from user
                where (UserName = '$user_name' OR Email = '$user_name' ) 
                AND Password = '" . $password . "'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    /**
     * logout
     *
     * @param int $user_id
     */
    function logout($user_id, $token = '')
    {
        // delete token
        // this is a temp fix for the token issue when logout for those admin users...
        if ($user_id != 6 && $user_id != 2 && $user_id != 32772){
            $this->db->delete('wstoken', array('UserID' => $user_id));
            
            // 2014-02-10 should delete cache
            if ($this->cached) {
                if ($token != ""){
                    $this->cached->delete($token);
                }
            }
        }
    }

    /**get full information of user
     * 
     * @param type $user_id
     * @return type
     */
    function getUserInfo($user_id)
    {
        if ($this->cached) {            
            $cache = $this->cached->get('user' . $user_id);  
            if ($cache) {               
                return $cache;
            } else {
                $this->db->select('*, Photo as Avatar');
                $this->db->from('user');
                $this->db->where('UserID', $user_id);
                $query = $this->db->get();
                $result = $query->row();
                $query->free_result();
                $this->cached->set('user' . $user_id, $result);
                return $result;
            }
        } else {
            $this->db->select('*, Photo as Avatar');
            $this->db->from('user');
            $this->db->where('UserID', $user_id);
            $query = $this->db->get();
            $result = $query->row();
            $query->free_result();
            return $result;
        }
    }
    
    /**get user detail [using for ws user/get_user_info]
     * 
     * @param type $user_id
     * @return type
     */
    function getUserDetail($user_id)
    {
        if ($this->cached) {            
            $cache = $this->cached->get('user_detail' . $user_id);  
            if ($cache) {               
                return $cache;
            } else {
                $result = self::_getUserDetail($user_id);
                if ($result){
                    $this->cached->set('user_detail' . $user_id, $result);
                }
                return $result;
            }
        } else {
            return self::_getUserDetail($user_id);
        }
    }
    
    function _getUserDetail($user_id){
        $photo_url = $this->config->item('s3_photo_url');
        $sql = "select UserID, UserName, Gender, BirthDate, Credits, NumberOfFollower, NumberOfFollowing, GenderDating, Point, IsVendor,IsAgeLess, IsAdmin, HasFanPage, IsVerify, IsBanned, Deactive, IsEmailVerify
                        , 0 as IsFavorite
                        , 0 as IsReported
                        , '' as Email
                        , if (FacebookID != '', 1, '') as FacebookID
                        , '' as FbEmail
                        , if (ProfileStatus is null, '', ProfileStatus) as ProfileStatus
                        , if (AboutMe is null, '', AboutMe) as AboutMe
                        , IF (Photo != '', CONCAT('" . $photo_url. "', Photo), '') as Avatar
                        , IF (FullPhoto != '', CONCAT('" . $photo_url. "', FullPhoto), '') as FullPhoto
                        , IF (CoverPicture != '', CONCAT('" . $photo_url. "', CoverPicture), CONCAT('" . $photo_url. "','".COVER_PICTURE_DEFAULT."') ) as CoverPicture
                        , if (NeighborhoodID is null, '', NeighborhoodID) as NeighborhoodID
                        , if (OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                                if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), nbh.NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                                )AS NeighborhoodName
                from user u
                left join neighborhood nbh ON nbh.OGR_FID = u.NeighborhoodID
                where UserID = ".$user_id;
        $query = $this->db->query($sql);
        return $query->row();
    }


    function isShare($user_id)
    {
        $this->db->select('Shared');
        $this->db->from('user');
        $this->db->where('UserID', $user_id);
        $query = $this->db->get();
        $row = $query->row();
        $query->free_result();
        return $row->Shared;
    }

    function share($user_id)
    {
        $query = $this->db->query('Update user set Credits = Credits + 1000 where UserID=' . $user_id);
        if ($query)
            $this->db->update('user', array('Shared' => 1), array('UserID' => $user_id));
        return $query;

    }

    function logShare($data)
    {
        $this->db->insert('fbshare', $data);
        return $this->db->insert_id();
    }

    function blockMsg($current_user_id, $target_user_id)
    {
        $data = array(
            'CurrentUserID' => $current_user_id,
            'TargetUserID' => $target_user_id,
            'Date' => gmdate('Y-m-d H:i:s')
        );
        $this->db->insert('userblock', $data);
        return $this->db->insert_id();
    }

    function unBlockMsg($current_user_id, $target_user_id)
    {
        $this->db->delete('userblock', array('CurrentUserID' => $current_user_id, 'TargetUserID' => $target_user_id));
    }

    function checkBlockMsg($current_user_id, $target_user_id)
    {
        $this->db->select('*');
        $this->db->from('userblock');
        $this->db->where('CurrentUserID', $current_user_id);
        $this->db->where('TargetUserID', $target_user_id);
        $query = $this->db->get();
        $retVal = $query->row();
        $query->free_result();
        return $retVal;
    }

    function checkExistedEmail($email, $user_id = 0, $is_email_verify = 0)
    {
        $this->db->select('count(UserID) as num');
        $this->db->from('user');
        $this->db->where('Email', $email);
        if ($user_id) {
            $this->db->where("UserID != $user_id");
        }
        if ($is_email_verify > 0){
            $this->db->where('IsEmailVerify', 1);
        }
        $query = $this->db->get();
        $retVal = $query->row();
        return (int)$retVal->num;
    }

    function checkExistedUserName($username, $user_id)
    {
        $this->db->select('count(UserID) as num');
        $this->db->from('user');
        $this->db->where('UPPER(UserName)', strtoupper(trim($username)));
        if ($user_id) {
            $this->db->where("UserID != $user_id");
        }
        $query = $this->db->get();
        $retVal = $query->row();
        return (int)$retVal->num;
    }
    
    function checkExistedPhone($phone, $user_id)
    {
        $this->db->select('count(UserID) as num');
        $this->db->from('user');
        $this->db->where('Phone',trim($phone));
        if ($user_id) {
            $this->db->where("UserID != $user_id");
        }
        $query = $this->db->get();
        $retVal = $query->row();
        return (int)$retVal->num;
    }
    
    function checkExistedUserNameOrEmail($user_name = '', $email = ''){
        $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
        $where = array();
        if ($user_name != ''){
            $user_name = mysqli_real_escape_string($con, $user_name);
            $where[] = "UPPER(UserName)= '".  strtoupper($user_name)."'";
        }
        if ($email != ''){
            $email = mysqli_real_escape_string($con, $email);
            $where[] = "Email = '".$email."'";
        }
        
        if (sizeof($where) > 0){
            $sql = "select UserName, Email from user where ";
            $sql .= implode(' OR ', $where);
            $query = $this->db->query($sql);
            return $query->row();
        }
        return false;
    }

    function getFbExist($fb_id, $user_id)
    {
        $this->db->select('UserName, UserID');
        $this->db->from('user');
        $this->db->where('FacebookID', $fb_id);
        if ($user_id > 0) {
            $this->db->where("UserID != $user_id");
        }
        $query = $this->db->get();
        return $query->row();
    }

    /**
     * return is_block
     *
     * @param int $current_user_id
     * @param int $target_user_id
     * @return int
     */
    function checkBlock($current_user_id, $target_user_id)
    {
        $sql = "select * from userblock where 
                (CurrentUserID = $current_user_id AND TargetUserID = $target_user_id) 
              OR (CurrentUserID = $target_user_id AND TargetUserID = $current_user_id)";
        $query = $this->db->query($sql);
        $row = $query->row();
        return $row?1:0;
    }

    /**
     * get version info
     *
     * @param string $app_version
     * @param string $platform
     * @return object
     */
    function checkVersion($app_version, $platform)
    {
        if ($this->cached){
            $cached = $this->cached->get('version'.$app_version.$platform);
            if ($cached){
                return $cached;
            }else{
                $version = self::_checkVersion($app_version, $platform);
                if ($version){
                    $this->cached->set('version'.$app_version.$platform, $version);
                }
                return $version;
            }
        }else{
            return self::_checkVersion($app_version, $platform);
        }
    }
    
    function _checkVersion($app_version, $platform)
    {
        $this->db->select('IsLatest, IsSupport');
        $this->db->from('version');
        $this->db->where('AppVersion', trim($app_version));
        $this->db->where('Platform', trim($platform));
        $query = $this->db->get();
        $retVal = $query->row();
        return $retVal;
    }

    /**
     * add tag for profile
     *
     * @param int $profile_id
     * @param array $data
     */
    function addTags($user_id, $data)
    {
        if (is_array($data)) {
            $this->db->delete('usertag', array('UserID' => $user_id));
            foreach ($data as $tag) {
                if (trim($tag) != '') {
                    $tag_id = Tag_model::getTagId($tag);
                    if ($tag_id > 0) {
                        $this->db->insert('usertag', array('UserID' => $user_id, 'TagID' => $tag_id));
                    } else {
                        $new_tag_id = Tag_model::add(array('Name' => trim($tag)));
                        $this->db->insert('usertag', array('UserID' => $user_id, 'TagID' => $new_tag_id));
                    }
                }
            }
            if ($this->cached) {
                $this->cached->delete('tag' . $user_id);
            }
        }
    }

    /**
     * return listTagOfUser
     *
     * @param int $user_id
     * @return array
     */
    function getListTagOf($user_id)
    {
        if ($this->cached) {
            $cache = $this->cached->get('tag' . $user_id);
            if ($cache) {
                return $cache;
            } else {
                $this->db->select('t.TagID, t.Name, t.TagCategoryID');
                $this->db->from('tag t');
                $this->db->join('UserTag ut', 'ut.TagID=t.TagID');
                $this->db->where('ut.UserID', $user_id);

                $this->db->order_by('t.Name', 'asc');
                $query = $this->db->get();
                $result = $query->result();
                $query->free_result();
                $this->cached->set('tag' . $user_id, $result);
                return $result;
            }
        } else {
            $this->db->select('t.TagID, t.Name, t.TagCategoryID');
            $this->db->from('tag t');
            $this->db->join('UserTag ut', 'ut.TagID=t.TagID');
            $this->db->where('ut.UserID', $user_id);
            $this->db->order_by('t.Name', 'asc');
            $query = $this->db->get();
            $retVal = $query->result();
            $query->free_result();
            return $retVal;
        }
    }

    /**
     * return connect meter between 2 user
     *
     * @param int $current_user_id
     * @param int $target_user_id
     * @param string $return {int/object}
     * @return int
     *         - 0: Never met
     *         - 1: Anomo
     *         - 2: Reveal
     *
     */
    function getConnectMeter($current_user_id, $target_user_id, $return = 'int')
    {
        $this->db->select('*');
        $this->db->from('userconnect');
        $this->db->where('CurrentUserID', $current_user_id);
        $this->db->where('TargetUserID', $target_user_id);
        $query = $this->db->get();
        $result = $query->row();
        $query->free_result();
        if ($return == 'int') {
            return ($result) ? $result->Status : 0;
        } else {
            return $result;
        }
    }
    
    function getStatusBtw2User($current_user_id, $target_user_id){
        $sql = "select (select if (IsFavorite is not null, IsFavorite, 0) as IsFavorite from userconnect where CurrentUserID = $current_user_id AND TargetUserID = $target_user_id) as IsFavorite
                        ,(select if (ID is not null, 1, 0) as IsReported from flag where FromUserID = $current_user_id AND ToUserID = $target_user_id) as IsReported
                ";
        $query = $this->db->query($sql);
        return $query->row();
    }

    /**
     * set connect meter between 2 user
     *
     * @param int $user_id
     * @param int $target_user_id
     */
    function setConnectMeter($user_id, $target_user_id)
    {
        $connectMeter1 = self::getConnectMeter($user_id, $target_user_id, 'object');
        if (!$connectMeter1) {
            $this->db->insert('userconnect', array('CurrentUserID' => $user_id, 'TargetUserID' => $target_user_id, 'Status' => 1, 'CreatedDate' => gmdate('Y-m-d H:i:s')));
        } else {
            if ($connectMeter1->Status == 0) {
                $this->db->update('userconnect', array('Status' => 1), array('CurrentUserID' => $user_id, 'TargetUserID' => $target_user_id));
            }
        }
        $connectMeter2 = self::getConnectMeter($target_user_id, $user_id, 'object');
        if (!$connectMeter2) {
            $this->db->insert('userconnect', array('CurrentUserID' => $target_user_id, 'TargetUserID' => $user_id, 'Status' => 1, 'CreatedDate' => gmdate('Y-m-d H:i:s')));
        } else {
            if ($connectMeter2->Status == 0) {
                $this->db->update('userconnect', array('Status' => 1), array('CurrentUserID' => $target_user_id, 'TargetUserID' => $user_id));
            }
        }
    }

    /**
     * set connect meter between 2 user
     *
     * @param int $user_id
     * @param int $target_user_id
     */
    function setConnectMeterForFavorite($user_id, $target_user_id)
    {
        $this->db->insert('userconnect', array('CurrentUserID' => $user_id, 'TargetUserID' => $target_user_id, 'Status' => 0, 'CreatedDate' => gmdate('Y-m-d H:i:s')));
    }
    
    function follow($user_id, $target_user_id)
    {
        return $this->db->insert('userconnect', array('CurrentUserID' => $user_id, 'TargetUserID' => $target_user_id, 'Status' => 0, 'IsFavorite' => 1, 'IsIndexing' => 2, 'CreatedDate' => gmdate('Y-m-d H:i:s')));
    }

    /**
     * add favorite
     *
     * @param int $user_id
     * @param int $target_user_id
     */
    function addFavorite($user_id, $target_user_id)
    {
        return $this->db->update('userconnect', array('IsFavorite' => 1, 'IsIndexing' => 2), array('CurrentUserID' => $user_id, 'TargetUserID' => $target_user_id));
    }

    /**
     * remove favorite
     *
     * @param int $user_id
     * @param int $target_user_id
     */
    function removeFavorite($user_id, $target_user_id) {
        return $this->db->update('userconnect', array('IsFavorite' => 0, 'IsIndexing' => 3), array('CurrentUserID' => $user_id, 'TargetUserID' => $target_user_id));
    }

    function removeContact($user_id, $target_user_id)
    {
        $this->db->delete('userconnect', array('CurrentUserID' => $user_id, 'TargetUserID' => $target_user_id));
    }

    function addReveal($current_user_id, $target_user_id)
    {
        $this->db->update('userconnect', array('Status' => 2), array('CurrentUserID' => $current_user_id, 'TargetUserID' => $target_user_id));
    }

    function unReveal($current_user_id, $target_user_id)
    {
        $this->db->update('userconnect', array('Status' => 1), array('CurrentUserID' => $current_user_id, 'TargetUserID' => $target_user_id));
    }

    /**
     * update user info
     *
     * @param array $data
     * @param array $cond
     */
    function updateUser($data, $cond, $token = "")
    {
        if ($this->cached) {
            $user_id = isset($cond['UserID']) ? $cond['UserID'] : '';
            self::invalidateCache($user_id);
            if ($token != "")
                $this->cached->delete($token);
        }
        // Hard coded for user JENNY RED PANDA GIRL! - to be moved into config later... but let's put it here for now. @Hung Pham
        if (isset($cond['UserID']) && $cond['UserID'] == 32772) {
            $data['NeighborhoodID'] = 27578;
            $data['Lat'] = 47.668850;
            $data['Lon'] = 122.333006;
        }
        return $this->db->update('user', $data, $cond);
    }

    /**
     * get user info by email
     *
     * @param string $email
     * @return object
     */
    function getUserInfoByEmail($email, $user_id = 0)
    {
        $this->db->select('UserID, UserName, Email, FacebookID, Password');
        $this->db->from('user');
        $this->db->where('Email', $email);
        if ($user_id > 0){
            $this->db->where('UserID', $user_id);
        }
        $query = $this->db->get();
        $retVal = $query->row();
        $query->free_result();
        return $retVal;
    }

    function getAllContactOf($user_id)
    {
        $this->db->select('u.UserID, u.UserName, u.Email, u.ProfileStatus, u.Photo as Avatar, u.BirthDate, u.Gender, u.FbEmail, u.Phone, u.RealName, u.RealPhoto, uc.IsFavorite, uc.Status');
        $this->db->from('userconnect uc');
        $this->db->join('user u', 'u.UserID=uc.TargetUserID');
        $this->db->where('uc.CurrentUserID', $user_id);
        $this->db->where('u.IsBanned !=', 1);
        $query = $this->db->get();
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    function getUpdatedContactOf($user_id, $from_date)
    {
        $this->db->select('u.UserID, u.UserName, u.Email, u.ProfileStatus, u.Photo as Avatar, u.BirthDate, u.Gender, u.FbEmail, u.Phone, u.RealName, u.RealPhoto, uc.IsFavorite, uc.Status');
        $this->db->from('userconnect uc');
        $this->db->join('user u', 'u.UserID=uc.TargetUserID');
        $this->db->where('uc.CurrentUserID', $user_id);
        $this->db->where('uc.CreatedDate >=', $from_date);
        $query = $this->db->get();
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    /**
     * for admin page
     * get list user
     *
     * @param array $cond
     * @param int $from
     * @param int $to
     * @param bool $getSumNumber
     * @return int / object
     */
    function getListByCondition($cond, $from = '', $to = '', $getSumNumber = false)
    {
        if ($getSumNumber){
            $select = " count(UserID) as Total";
        }else{
            $select = " *
                        , (select count(*) from userblock ub where ub.TargetUserID=user.UserID) as TotalBlock
                        , (select count(ID) from flag f where f.ToUserID=user.UserID) as TotalFlag
                        , (select count(ID) from flagcontent fc where fc.ContentOwnerID=user.UserID) as TotalFlagContent
                        ";
            }
        $this->db->select($select);
        $this->db->from('user');
        if (count($cond) > 0) {
            if (isset($cond['status'])) {
                $this->db->where('EmailAccountStatus', $cond['status']);
            }

            if (isset($cond['keyword'])) {
                $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
                $cond['keyword'] = mysqli_real_escape_string($con, $cond['keyword']);
                $q = "(`UserName` LIKE '%{$cond['keyword']}%'  OR `Email` LIKE '%{$cond['keyword']}%' )";
                $this->db->where($q);
            }

            if (isset($cond['shared'])) {
                $this->db->where('Shared', 1);
                if ($cond['shared'] == 'month') {
                    $this->db->where('MONTH(SharedLatestDate)', gmdate('m', strtotime($this->timezone)));
                    //  $this->db->where('MONTH(SharedLatestDate)', gmdate('m'));
                } elseif ($cond['shared'] == 'day') {
                    $this->db->where("DATE_FORMAT(SharedLatestDate, '%Y-%m-%d %H:%i:%s') BETWEEN '" . $this->pst_start . "' AND '" . $this->pst_end . "'");
                    // $this->db->where("DATE_FORMAT(SharedLatestDate, '%Y-%m-%d')='" . gmdate('Y-m-d') . "'");
                } elseif ($cond['shared'] == 'week') {
                    $this->db->where("SharedLatestDate >= '" . gmdate('Y-m-d H:i:s', strtotime("- 1 week")) . "'");
                }
            }

            if (isset($cond['share_to_wall'])) {
                if ($cond['share_to_wall'] == 'first_time') {
                    $this->db->where('IsShareToWall >', 0);
                }

                if ($cond['share_to_wall'] == '3days') {
                    $this->db->where('IsShareToWall3Days >', 0);
                }
            }
            
            // ANOMO-7854
            if (isset($cond['block'])){
                $where = "UserID in (select CurrentUserID from userblock where TargetUserID=".$cond['block'].")";
                $this->db->where($where);
            }
            
            if (isset($cond['sort']) && !$getSumNumber) {
                switch ($cond['sort']) {
                    case 'id-asc':
                        $this->db->order_by('UserID', 'asc');
                        break;
                    case 'id-desc':
                        $this->db->order_by('UserID', 'desc');
                        break;
                    case 'user-name-asc':
                        $this->db->order_by('UserName', 'asc');
                        break;
                    case 'user-name-desc':
                        $this->db->order_by('UserName', 'desc');
                        break;
                    case 'email-asc':
                        $this->db->order_by('Email', 'asc');
                        break;
                    case 'email-desc':
                        $this->db->order_by('Email', 'desc');
                        break;
                    case 'real-name-asc':
                        $this->db->order_by('RealName', 'asc');
                        break;
                    case 'real-name-desc':
                        $this->db->order_by('RealName', 'desc');
                        break;
                    case 'fb-share-desc':
                        $this->db->order_by('SharedLatestDate', 'desc');
                        break;
                    case 'flags-asc':
                        $this->db->order_by('TotalFlag', 'asc');
                        break;
                    case 'flags-desc':
                        $this->db->order_by('TotalFlag', 'desc');
                        break;
                    case 'flagcontent-asc':
                        $this->db->order_by('TotalFlagContent', 'asc');
                        break;
                    case 'flagcontent-desc':
                        $this->db->order_by('TotalFlagContent', 'desc');
                        break;
                    case 'follower-asc':
                        $this->db->order_by('NumberOfFollower', 'asc');
                        break;
                    case 'follower-desc':
                        $this->db->order_by('NumberOfFollower', 'desc');
                        break;
                    case 'fbgame-asc':
                        $this->db->order_by('TotalFbGame', 'asc');
                        break;
                    case 'fbgame-desc':
                        $this->db->order_by('TotalFbGame', 'desc');
                        break;
                    case 'block-asc':
                        $this->db->order_by('TotalBlock', 'asc');
                        break;
                    case 'block-desc':
                        $this->db->order_by('TotalBlock', 'desc');
                        break;
                    case 'completefbgame-asc':
                        $this->db->order_by('TotalCompleteFbGame', 'asc');
                        break;
                    case 'completefbgame-desc':
                        $this->db->order_by('TotalCompleteFbGame', 'desc');
                        break;
                }
            }
        }
        $retVal = NULL;
        if ($getSumNumber) {      
            $query = $this->db->get();
            $row = $query->row();
            $retVal = $row?$row->Total:0;
        } else {
//			if ($from != '' && $to != ''){
            $this->db->limit($from, $to);
//			}
            $query = $this->db->get();
            $retVal = ($query->num_rows() > 0) ? $query->result() : null;
        }

        $query->free_result();
        return $retVal;
    }

    function getLastUpdateContact($user_id)
    {
        $this->db->select('CreatedDate');
        $this->db->from('userconnect');
        $this->db->where('CurrentUserID', $user_id);
        $this->db->order_by('CreatedDate', 'DESC');
        $query = $this->db->get();
        $result = $query->result();
        $query->free_result();
        return ($result) ? $result[0]->CreatedDate : "";
    }

    function getRevealInfo($user_id)
    {
        $this->db->select('FbEmail, RealName, BasicPicture, College, Major, Ethnicity, GoToDrink, WhatAttractsMe, WhatIDoOnTheWeekend, HowIDescribeMyPersonality, HowIDescribeMyBody, Looks, Party, Flirty, Look_description, Party_description, Flirty_description, Major_description');
        $this->db->from('user');
        $this->db->where('UserID', $user_id);
        $query = $this->db->get();
        $retVal = $query->row();
        $query->free_result();
        return $retVal;
    }

    function returnBlockStatus($current_user_id, $target_user_id)
    {
        $code = '';
        $is_block = self::checkBlockMsg($current_user_id, $target_user_id);
        if ($is_block) {
            $code = 'BLOCK';
        }
        if (empty($code)){
            $is_target_block = self::checkBlockMsg($target_user_id, $current_user_id);
            if ($is_target_block) {
                $code = 'BLOCK_TARGET';
            }
        }
        return $code;
    }

    function getByFbID($fb_id)
    {
        $this->db->select('UserID, UserName, Email, Gender, BirthDate, Password, Credits, AboutMe, Photo as Avatar, FullPhoto, CoverPicture, FbEmail, FacebookID, Phone, RealName, ProfileStatus, IsBanned, Deactive, AllowSendGiftNotice, AllowRevealNotice, AllowChatNotice, AllowAnomotionNotice, College, NeighborhoodID,  AllowCommentActivityNotice, AllowLikeActivityNotice, AllowFollowNotice');
        $this->db->from('user');
        $this->db->where('FacebookID', $fb_id);
        $query = $this->db->get();
        $retVal = $query->row();
        $query->free_result();
        return $retVal;
    }

    function deleteAccount($user_id) {
        $return = false;
        try {
            if ($this->cached) {
                self::invalidateCache($user_id);
                $tokenInfo = Wstoken_model::getTokenOfUser($user_id);
                if ($tokenInfo){
                    $this->cached->delete($tokenInfo->Token);
                }
                    
            }
            $this->db->trans_begin();
           //  ANOMO-8082  When deleting an account, archive and move to another table.
            $sql = "insert into user_archive 
                    select * from user where UserID=$user_id
                ";
            $this->db->query($sql);
            $this->db->delete('userconnect', "CurrentUserID = $user_id OR  TargetUserID = $user_id");
            $this->db->delete('userblock', "CurrentUserID = $user_id OR  TargetUserID = $user_id");
            $this->db->delete('usertag', array('UserID' => $user_id));
            $this->db->delete('secretpicture', array('UserID' => $user_id));
            $this->db->delete('reveal', "CurrentUserID = $user_id OR  TargetUserID = $user_id");
            $this->db->delete('result', array('UserID' => $user_id));
            $this->db->delete('notificationhistory', "SendUserID = $user_id OR  UserID = $user_id");
            $this->db->delete('giftsend', "SendUserID = $user_id OR  ReceiveUserID = $user_id");
            $this->db->delete('anomotion', "UserID = $user_id OR  TargetUserID = $user_id");

            // delete hashtag
            $hashtag_sql = "delete from hashtag where ActivityID IN (select ActivityID from activity where FromUserID = $user_id)";
            $this->db->query($hashtag_sql);
            
            $this->db->delete('activity', array('FromUserID' => $user_id));
            $this->db->delete('activitycomment', array('UserID' => $user_id));
            $this->db->delete('activitylike', array('UserID' => $user_id));
            $this->db->delete('userprofilestatus', array('UserID' => $user_id));
            $this->db->delete('wstoken', array('UserID' => $user_id));
            $this->db->delete('commentlike', array('UserID' => $user_id));
            $this->db->delete('stopreceivenotify', array('UserID' => $user_id));
            
            // topic 
            $sql = "select TopicID from topic where UserID = $user_id";
            $query = $this->db->query($sql);
            $result = $query->result();
            if ($result){
                $aTopicID = array();
                foreach($result as $re){
                    $aTopicID[] = $re->TopicID;
                }
                if (sizeof($aTopicID) > 0){
                    $this->db->query("update activity set IsInvalid = 1 where TopicID IN (".  implode(',', $aTopicID) . ")");
                    $this->db->query("update topic set IsDelete = 1 where TopicID IN (".  implode(',', $aTopicID) . ")");
                    // remove topic favorite
                    $this->db->query("delete from topicfavorite where TopicID IN (".implode(',', $aTopicID).")");
                }
            }
            
            
            
            $this->db->delete('user', array('UserID' => $user_id));

            $return = true;
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }
        return $return;
    }

    function getLatestCheckin($user_id)
    {
        $this->db->select('uc.Status, uc.CheckOutDate, p.Name');
        $this->db->from('usercheckin uc');
        $this->db->join('place p', 'uc.PlaceID = p.PlaceID');
        $this->db->where('UserID', $user_id);
        $this->db->order_by('uc.CheckInDate', 'desc');
        $this->db->limit(1, 0);
        $query = $this->db->get();
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    function getBlockUserOf($user_id)
    {
        $this->db->select('u.UserID, u.UserName, u.Email, u.ProfileStatus, u.Photo as Avatar, u.BirthDate, u.Gender, u.FbEmail, u.Phone, u.RealName, u.RealPhoto');
        $this->db->from('userblock ub');
        $this->db->join('user u', 'ub.TargetUserID=u.UserID');
        $this->db->where('ub.CurrentUserID', $user_id);
        $this->db->where('u.IsBanned !=', 1);
        $query = $this->db->get();
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    /**return current neighborhood of user
     * 
     * @param type $user_id
     * @return type
     */
    function getNeighborhoodInfoOfUser($user_id) {
        if ($this->cached) {
            $cached = $this->cached->get('neighborhood_user' . $user_id);
            if ($cached) {
                return $cached;
            } else {
                $nbh = self::_getNeighborhoodInfoOfUser($user_id);
                if ($nbh) {
                    $this->cached->set('neighborhood_user' . $user_id, $nbh);
                }
                return $nbh;
            }
        } else {
            return self::_getNeighborhoodInfoOfUser($user_id);
        }
    }

    private function _getNeighborhoodInfoOfUser($user_id) {
        $sql = "select u.NeighborhoodID,
        			
                                if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                                if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), nbh.NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                                )AS NAME
 
                                from user u
    				join neighborhood nbh ON (u.NeighborhoodID=nbh.OGR_FID)
    				where UserID=$user_id";
        $query = $this->db->query($sql);
        $retVal = $query->row();
        $query->free_result();
        return $retVal;
    }
   
    function returnAge($birthdate)
    {
        $age = date('Y') - date('Y', strtotime($birthdate));
        $from = '';
        $to = '';
        if ($age <= 17) {
            $to = 17;
        } elseif ($age >= 18 && $age <= 22) {
            $from = 18;
            $to = 22;
        } elseif ($age >= 23 && $age < 30) {
            $from = 23;
            $to = 29;
        } elseif ($age >= 30 && $age < 40) {
            $from = 30;
            $to = 39;
        } elseif ($age >= 40 && $age < 50) {
            $from = 40;
            $to = 49;
        } elseif ($age >= 50) {
            $from = 50;
        }
        return array('from' => $from, 'to' => $to);
    }


    /** Phuoc Nguyen - Get list UserID
     *  age <= 17 : teen
    18 >= age <= 22: college
    23 >= age < 30 : twenties
    30>= age <40 : thirties
    40>= age < 50 : forties
    50 >= age : fifties
     **/
    function getListUserID($aCond, $iLimit = '')
    {
        if (isset($aCond['getTotal'])){
            $this->db->select('count(u.UserID) as Total');
        }else{
            $this->db->select('u.UserID');
        }
        
        $this->db->from('user u');
        
        if (isset($aCond['lat']) && isset($aCond['lon']) && $aCond['lat'] != '' && $aCond['lon'] != ''){
            $dist = isset($aCond['radius']) && $aCond['radius'] != '' ? $aCond['radius']:75; // mile
            $lat = $aCond['lat'];
            $lon = $aCond['lon'];
            $distanceFactor = 69; 
            $lon1 = $lon - $dist / abs(cos(deg2rad($lat)) * $distanceFactor) ;
            $lon2 = $lon + $dist / abs(cos(deg2rad($lat)) * $distanceFactor); 
            $lat1 = $lat - ($dist / $distanceFactor);
            $lat2 = $lat + ($dist / $distanceFactor);
            
            $isMeridian180 = 0;
            // from store procedure distanceSearch3_OP_2_v200
            if ($lon1 < -180){
                $isMeridian180 = 1;
                
                $lon1_2 = 360 + $lon1;
                $lon2_2 = 179.999;
                $lat1_2 = $lat1;
                $lat2_2 = $lat2;
                
                $lon1 = -179.999;
            }elseif($lon2 > 180){
                $isMeridian180 = 1;

                $lon1_2 = -179.999;
                $lon2_2 = -360 + $lon2;
                $lat1_2 = $lat1;
                $lat2_2 = $lat2;

                $lon2 = 179.999;
            }
            
            if ($isMeridian180 != 0){
                $where = "(Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2) OR (Lon BETWEEN $lon1_2 AND $lon2_2 AND Lat BETWEEN $lat1_2 AND $lat2_2)";
                $this->db->where($where);
            }else{
                $where = "Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2 ";
                $this->db->where($where);
            }
        }
        
        
        if ($aCond['gender'] != "") {
            $this->db->where("u.Gender", $aCond['gender']);
        }

        if ($aCond['from'] != "" && $aCond['to'] != "") {
            $this->db->where("SignUpDate > '" . $aCond['from'] . "' AND SignUpdate < '" . $aCond['to'] . "'");
        }

        if ($aCond['app_used'] != "") {
            $gmtNow = gmdate('Y-m-d h:i:s');
            $day_7 = date('Y-m-d H:i:s', strtotime("-7 day", strtotime($gmtNow)));
            if ($aCond['app_used'] == "Used") {
                $this->db->where("DATE(u.LastActivity) BETWEEN DATE('" . $day_7 . "') AND DATE('" . $gmtNow . "')");
            } else {
                $this->db->where("DATE(u.LastActivity) < DATE('" . $day_7 . "')");
            }
        }
        
        
        // filter by age
        if (isset($aCond['from_age']) && $aCond['from_age'] > 0){
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-'.$aCond['from_age'] .' year', strtotime($now)));
            $this->db->where('BirthDate <=', $youngest_date);
        }
        if (isset($aCond['to_age']) && $aCond['to_age'] > 0){
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-'.$aCond['to_age'] - 1 .' year', strtotime($now)));
            $this->db->where('BirthDate >', $oldest_date);
        }
        

        if (isset($aCond['age']) && $aCond['age'] != "") {
            switch ($aCond['age']) {     
                case '17':
                    $now = gmdate('Y-m-d');
                    $oldest_date = date('Y-m-d', strtotime('-17 year', strtotime($now)));
                    $this->db->where('BirthDate >', $oldest_date);
                    break;
                case '18_22':
                    $now = gmdate('Y-m-d');
                    $youngest_date = date('Y-m-d', strtotime('- 18 year', strtotime($now)));
                    $oldest_date = date('Y-m-d', strtotime('- 22 year', strtotime($now)));
                    $this->db->where('BirthDate <=', $youngest_date);
                    $this->db->where('BirthDate >', $oldest_date);
                    break;
                case '23_30':
                    $now = gmdate('Y-m-d');
                    $youngest_date = date('Y-m-d', strtotime('- 23 year', strtotime($now)));
                    $oldest_date = date('Y-m-d', strtotime('- 29 year', strtotime($now)));
                    $this->db->where('BirthDate <=', $youngest_date);
                    $this->db->where('BirthDate >', $oldest_date);
                    break;
                case '30_40':
                    
                    $now = gmdate('Y-m-d');
                    $youngest_date = date('Y-m-d', strtotime('- 30 year', strtotime($now)));
                    $oldest_date = date('Y-m-d', strtotime('- 39 year', strtotime($now)));
                    $this->db->where('BirthDate <=', $youngest_date);
                    $this->db->where('BirthDate >', $oldest_date);
                    break;
                case '40_50':
                    $now = gmdate('Y-m-d');
                    $youngest_date = date('Y-m-d', strtotime('- 40 year', strtotime($now)));
                    $oldest_date = date('Y-m-d', strtotime('- 49 year', strtotime($now)));
                    $this->db->where('BirthDate <=', $youngest_date);
                    $this->db->where('BirthDate >', $oldest_date);
                    break;
                case '50':
                    $now = gmdate('Y-m-d');
                    $youngest_date = date('Y-m-d', strtotime('- 50 year', strtotime($now)));
                    $this->db->where('BirthDate <=', $youngest_date);
                    break;
            }
        }

        // Hung added here...
        $this->db->where('u.UserID not in (SELECT CurrentUserID from userblock WHERE TargetUserID = ' . MASS_MESSAGE_SENDER . ')');

        $query = $this->db->get();

        if (isset($aCond['getTotal'])){
            $oRow = $query->row();
            return $oRow?$oRow->Total:0;
        }else{
            $oResults = $query->result();
            $query->free_result();
            return $oResults;
        }
    }
    /** End - Get list user **/

    function getListFollower($user_id, &$total, $keyword = '', $offset = '', $limit = '', $current_user_id = '')
    {
        $totalSql = "select count(temp.UserID) as Total from (
                select CurrentUserID as UserID from userconnect where TargetUserID=$user_id AND IsFavorite = 1
                     AND CurrentUserID NOT IN (select if(CurrentUserID = $current_user_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = $current_user_id or TargetUserID = $current_user_id)
                ) as temp " ;
        
        $sql = "select temp.UserID, u.UserName, u.BirthDate, u.Gender, u.NeighborhoodID, '' as LastCheckinID, u.IsAdmin,
                IF (u.FacebookID != '', 1, '') as FacebookID,
    		IF (u.Photo != '', CONCAT('" .$this->config->item('s3_photo_url') . "',u.Photo), '') as Avatar
    		from (
                    select CurrentUserID as UserID from userconnect where TargetUserID=$user_id AND IsFavorite = 1
                        AND CurrentUserID NOT IN (select if(CurrentUserID = $current_user_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = $current_user_id or TargetUserID = $current_user_id)
                     ) as temp
    		left join user u ON u.UserID=temp.UserID
    		WHERE u.IsBanned = 0
    		";
        if ($keyword != '') {
            $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
            $keyword = mysqli_real_escape_string($con, $keyword);
            $sql .= " AND LCASE(u.UserName) like '%$keyword%'";
            $totalSql .= " left join user u ON u.UserID = temp.UserID";
            $totalSql .= " where LCASE(u.UserName) like '%$keyword%'";
        }
        $totalQuery = $this->db->query($totalSql);
        $totalRow = $totalQuery->row();
        $total = $totalRow?$totalRow->Total:0;
        
        if ($total > 0){

            $sql .= " order by u.UserName ASC"; 
            if ($limit > 0) {
                $sql .= " limit $offset, $limit";
            }

            // will get IsFavorite, Current checkin place, neighborhood
            $fullSql = " select t.* 
                            ,if(cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite
                            ,'' as PlaceID
                            ,'' as PlaceName
                            ,'' AS CheckinStatus
                            ,if(nbh.OGR_FID is not null, nbh.OGR_FID, '') as NeighborhoodID
                            ,if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '', concat(nbh.NAME, ', ', nbh.STATE), nbh.NAME), if (nbh.COUNTY is not null, nbh.COUNTY, '')) AS NeighborhoodName
                        from ($sql) as t
                        LEFT JOIN userconnect cc ON cc.TargetUserID=t.UserID AND cc.CurrentUserID = $current_user_id
                        LEFT JOIN neighborhood nbh ON t.NeighborhoodID = nbh.OGR_FID
                    ";



            $query1 = $this->db->query($fullSql);
            $retVal = $query1->result();
            $query1->free_result();

            return $retVal;
        }
        return array();
    }

    function getListFollowing($user_id, &$total, $keyword = '', $offset = '', $limit = '', $current_user_id = '')
    {
        $totalSql = "select count(temp.UserID) as Total from (
                    select TargetUserID as UserID from userconnect where CurrentUserID=$user_id AND IsFavorite = 1
                     AND TargetUserID NOT IN (select if(CurrentUserID = $current_user_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = $current_user_id or TargetUserID = $current_user_id)
                ) as temp " ;
        
        $sql = "select temp.UserID, u.UserName, u.BirthDate, u.Gender, u.NeighborhoodID, u.IsAdmin
                    , '' as LastCheckinID,
                    IF (u.FacebookID != '', 1, '') as FacebookID,
                    IF (u.Photo != '', CONCAT('" . $this->config->item('s3_photo_url') . "',u.Photo), '') as Avatar
    		from (
                        select TargetUserID as UserID from userconnect where CurrentUserID=$user_id AND IsFavorite = 1
                        AND TargetUserID NOT IN (select if(CurrentUserID = $current_user_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = $current_user_id or TargetUserID = $current_user_id)
                     ) as temp
    		left join user u ON u.UserID=temp.UserID
    		where u.IsBanned = 0
    		";
        if ($keyword != '') {
            $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
            $keyword = mysqli_real_escape_string($con, $keyword);
            $sql .= " AND LCASE(u.UserName) like '%$keyword%'";
            $totalSql .= " left join user u ON u.UserID = temp.UserID";
            $totalSql .= " where LCASE(u.UserName) like '%$keyword%'";
        }
        
        $totalQuery = $this->db->query($totalSql);
        $totalRow = $totalQuery->row();
        $total = $totalRow?$totalRow->Total:0;
        if ($total > 0){
            $sql .= " order by u.UserName ASC";
            if ($limit > 0) {
                $sql .= " limit $offset, $limit";
            }

            // will get IsFavorite, Current checkin place, neighborhood
            $fullSql = " select t.* 
                            ,if(cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite
                            ,'' as PlaceID
                            ,'' as PlaceName
                            ,'' AS CheckinStatus
                            ,if(nbh.OGR_FID is not null, nbh.OGR_FID, '') as NeighborhoodID
                            ,if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '', concat(nbh.NAME, ', ', nbh.STATE), nbh.NAME), if (nbh.COUNTY is not null, nbh.COUNTY, '')) AS NeighborhoodName
                        from ($sql) as t
                        LEFT JOIN userconnect cc ON cc.TargetUserID=t.UserID AND cc.CurrentUserID = $current_user_id
                        LEFT JOIN neighborhood nbh ON t.NeighborhoodID = nbh.OGR_FID
                    ";

            $query1 = $this->db->query($fullSql);
            $retVal = $query1->result();
            $query1->free_result();
            return $retVal;
        }
        return array();
    }

    function updateNumberFollow($user_id, $type = '', $action = 'add')
    {
        self::invalidateCache($user_id);
        if ($action == 'add') {
            $sql = "Update user Set `" . $type . "` = `" . $type . "` + 1 WHERE UserID = $user_id";
        } elseif ($action == 'sub') {
            $sql = "Update user Set `" . $type . "` = `" . $type . "` - 1  WHERE UserID = $user_id AND $type > 0";
        }
        return $this->db->query($sql);
    }

    /* --------------------------- OPTIMIZATION SECTIONS ---------------------------------- */

    // Hung added.. 9/8/2013 - group 3 queries for receiver info into 1 single query to reduce amount of processing
    function getPushNotificationReceiverInfo($user_id)
    {
        $within = 7;
        $sql = 'select u.UserID, u.UserName, u.Photo as Avatar, u.AllowChatNotice,
                       (select count(ID) from notificationhistory where UserID = ' . $user_id . ' and IsRead = 1 and CreatedDate >= ' . strtotime("-" . $within . " day", strtotime(gmdate('Y-m-d H:i:s'))) . ') as Number,
                       (select ChatCount from chat_count where UserID ='. $user_id.') as ChatCount,
                       d.ID, d.DeviceID, d.Type, d.Status, d.Development
                from `user` u left join devices d on u.UserID = d.UserID and d.Status = \'active\'
                where u.UserID = ' . $user_id;

        $query = $this->db->query($sql);
        $retVal = $query->row();
        $query->free_result();
        return $retVal;
    }

    function sp_register($data) {
        $sql = "CALL sp_register_v202('" . $data['UserName'] . "'
                                    , '" . $data['Email'] . "'
                                    ,'" . $data['Gender'] . "'
                                    ,'" . $data['BirthDate'] . "'
                                    ,'" . $data['FbEmail'] . "'
                                    ,'" . $data['FacebookID'] . "'
                                    ,'" . $data['Photo'] . "'
                                    ,'" . $data['FullPhoto'] . "'
                                    ,'" . $data['Password'] . "'
                                    ,'" . $data['Tags'] . "'
                                    ,'" . $data['IntentIDs'] . "'
                                    ,'" . $data['GenderDating'] . "' 
                                    ,@userID)";

        try {
            $this->db->trans_begin();
            $query1 = $this->db->query($sql);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }

        $query1->next_result();
        $query1->free_result();
        $query2 = $this->db->query("select @userID as UserID");
        $user_id = 0;
        foreach ($query2->result_array() as $row) {
            $user_id = $row['UserID'];
        }

        $query2->free_result();

        return $user_id;
    }

    function plusInvite($user_id, $type = 0, $number)
    {
        // update number of friend invited
        $sql1 = "update user set NumberInvite=`NumberInvite` + $number where UserID=$user_id";
        $this->db->query($sql1);

        // for share to wall first time
        // plus 1000pts
        if ($type == 0) {
            $sql = "update user set Credits=`Credits` + 1000, IsShareToWall = 1 where UserID=$user_id AND IsShareToWall = 0";
            $this->db->query($sql);
        } else {
            // share to wall after 3 days
            $sql = "update user set Credits=`Credits` + 1000, IsShareToWall3Days = 1 where UserID=$user_id AND IsShareToWall3Days = 0";
            $this->db->query($sql);
        }
    }


    function returnUserForGame4People($user_id, $from = '', $to = '', $number = 1, $age_from = '', $age_to = '', $gender = '', $active_match = 0) {
        $where = '';
        // ANOMO-10714 - Change Ice Breaker algorithm to find new users
        $field_match = 'SignUpDate';
        if ($active_match > 0){
            $field_match = 'LastActivity';
        }
        if ($from != ''){
            $where .= " AND $field_match > '$from'";
        }
        if ($to != ''){
            $where .= " AND $field_match < '$to'";
        }
        if ($age_from != ''){
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-'.$age_from .' year', strtotime($now)));
            $where .= " AND BirthDate <= '".$youngest_date."'";
        }
        if ($age_to != ''){
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-'.$age_to - 1 .' year', strtotime($now)));
            $where .= " AND BirthDate > '".$oldest_date."'";
        }

        if ($gender > 0){
            $where .= " AND Gender = ".$gender;
        }
        
        // ANOMO-11023 - don't display vendor account
        $where .= " AND IsVendor != 1";

        $subQuery = "select u.UserID, UserName, Gender, BirthDate, NeighborhoodID, LastActivity, Photo, FacebookID, IsAdmin from
		(select UserID, UserName, Gender, BirthDate, NeighborhoodID, LastActivity, Photo, FacebookID, IsAdmin from user where UserID != $user_id  $where) as u
		left outer join (select IF(UserID = $user_id, TargetUserID, UserID) as UserID, `Status` from anomotion where (UserID = $user_id OR TargetUserID = $user_id)) as temp1
                on temp1.`Status` = 1 AND u.UserID = temp1.UserID
                left outer join (select TargetUserID from userblock where CurrentUserID=$user_id) as temp2
                on u.UserID = temp2.TargetUserID
                where temp1.UserID is null and temp2.TargetUserID is null

                ";
//         AND u.UserID NOT IN (select TargetUserID from userblock where CurrentUserID=$user_id)
       /* $subQuery1 = "select max(u.UserID) from
		(select UserID from user where UserID != $user_id  $where) as u
		left outer join (select IF(UserID = $user_id, TargetUserID, UserID) as UserID, `Status` from anomotion where (UserID = $user_id OR TargetUserID = $user_id)) as temp1
                on temp1.`Status` = 1 AND u.UserID = temp1.UserID

                left outer join (select TargetUserID from userblock where CurrentUserID=$user_id) as temp2
                on u.UserID = temp2.TargetUserID
                where temp1.UserID is null and temp2.TargetUserID is null

                ";
*/

        $datesearch = date('Y-m-d H:i:s', strtotime(US_USER_ACTIVE_TIME . ' hour', strtotime(gmdate('Y-m-d H:i:s'))));

        $sql = "select UserID, UserName, Gender, BirthDate, NeighborhoodID, LastActivity, IsAdmin,
                        IF (u.FacebookID != '', 1, '')  as FacebookID,
                        if( u.LastActivity >= '$datesearch' ,1,0 ) AS 'IsOnline',
    			IF (u.Photo != '', CONCAT('" .
            $this->config->item('s3_photo_url') . "',u.Photo), u.Photo) as Avatar
    			from ($subQuery) as u order by rand()";
//where u.UserID >= rand() * ($subQuery1)
        $sql .= " limit $number";

        $query = $this->db->query($sql);
        $retVal = $query->result();
        $query->free_result();
        return $retVal;
    }

    /**search user by keyword function
     * 
     * @param type $keyword
     * @param type $user_id
     * @param type $limit
     * @param type $offset
     * @param type $total
     * @return type
     */
    function searchUserbyKeyword($keyword = '', $user_id = 0, $limit = 12, $offset = 0, &$total)
    {
        $datesearch = date('Y-m-d H:i:s', strtotime('-2 hour', strtotime(gmdate('Y-m-d H:i:s'))));
        $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
        $keyword = mysqli_real_escape_string($con, $keyword);  
        $totalSql = "
            SELECT count(UserID) as Total
            FROM user u1
            left outer join (select if(CurrentUserID = $user_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = $user_id or TargetUserID = $user_id) as t
            on u1.UserID = t.uID
            WHERE LCASE( u1.UserName ) LIKE '%$keyword%'
            AND u1.UserID AND IsBanned != 1 
            AND t.uID is null
        ";
        $totalQuery = $this->db->query($totalSql);
        $totalRow = $totalQuery->row();
        $total = $totalRow?$totalRow->Total:0;
        $totalQuery->free_result();
        $result = array();
        if ($total > 0){
            $sql = "
            SELECT 
                DISTINCT(u.UserID),u.UserName,u.Lat,u.Lon,u.Photo AS Avatar,u.ProfileStatus,u.Email,u.NeighborhoodID, u.Gender, u.IsAdmin,u.CoverPicture,
                if(u.BirthDate is not null,u.BirthDate, '') as BirthDate,
                if(u.FacebookID != '', 1, '') as FacebookID,
                0 AS IsOnline,
                '' AS PlaceID,
                0 AS Status,
                if (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,
                0 as IsBlock,
                0 as IsBlockTarget,

                if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), nbh.NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                )AS PlaceName
                  ,'' as NeighborhoodName
            FROM (
                    SELECT DISTINCT(u1.UserID),UserName,Lat,Lon,Photo,ProfileStatus,Email,NeighborhoodID, BirthDate,Gender, LastActivity,LastCheckInID,FacebookID,OnlyShowCountry, IsAdmin, CoverPicture
                    FROM user u1  
                    left outer join (select if(CurrentUserID = $user_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = $user_id or TargetUserID = $user_id) as t
                    on u1.UserID = t.uID

                    WHERE u1.UserID != $user_id AND IsBanned != 1 
                    AND LCASE( u1.UserName ) LIKE '%$keyword%'
                    AND t.uID is null
                    order by u1.UserName = '$keyword' desc
                    LIMIT $offset, $limit

            )as u
            LEFT JOIN userconnect cc ON cc.TargetUserID=u.UserID AND cc.CurrentUserID = $user_id
            left join neighborhood nbh ON u.NeighborhoodID=nbh.OGR_FID    
            ";
            $query = $this->db->query($sql);
            $result = $query->result();
            $query->free_result();
        }
        return $result;
    }
    
    /**search user by keyword function
     * 
     * @param type $keyword
     * @param type $user_id
     * @param type $limit
     * @param type $offset
     * @param type $is_search_tag
     * @param type $total
     * @return type
     */
    function searchUserbyTag($keyword = '', $user_id = 0, $limit = 12, $offset = 0, &$total)
    {
        $datesearch = date('Y-m-d H:i:s', strtotime(US_USER_ACTIVE_TIME.' hour', strtotime(gmdate('Y-m-d H:i:s'))));
        $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
        $keyword = mysqli_real_escape_string($con, $keyword);  
        
        $totalSql = "
            SELECT count(u.UserID) as Total
	    FROM (select TagID from tag where LCASE( Name )  = '$keyword') as t
            left join usertag ut on ut.TagID = t.TagID
            left join user u ON u.UserID = ut.UserID
            left outer join (select if(CurrentUserID = $user_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = $user_id or TargetUserID = $user_id) as t1
            on u.UserID = t1.uID
            where u.UserID != $user_id AND IsBanned != 1 AND t1.uID is null
        ";
        $totalQuery = $this->db->query($totalSql);
        $totalRow = $totalQuery->row();
        $total = $totalRow?$totalRow->Total:0;
        $totalQuery->free_result();
        $result = array();
        if ($total > 0){
            $sql = "
            SELECT 
                DISTINCT(u.UserID),u.UserName,u.Lat,u.Lon,u.Photo AS Avatar,u.ProfileStatus,u.Email,u.NeighborhoodID, u.BirthDate,u.Gender, u.IsAdmin, u.CoverPicture,
                if(u.FacebookID != '', 1, '') as FacebookID,
                if( u.LastActivity >= '$datesearch' ,1,0 ) AS IsOnline,
                '' AS PlaceID,
                0 AS Status,
                if (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,
                0 as IsBlock,
                0 as IsBlockTarget,
                if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                               if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), nbh.NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
               )AS PlaceName
               ,'' AS NeighborhoodName

            FROM (
                SELECT DISTINCT(u.UserID),UserName,Lat,Lon,Photo,ProfileStatus,Email,NeighborhoodID, BirthDate,Gender, LastActivity,LastCheckInID,FacebookID,OnlyShowCountry, IsAdmin, CoverPicture
                FROM (select TagID from tag where LCASE(Name )  = '$keyword') as t
                left join usertag ut on ut.TagID = t.TagID
                left join user u ON u.UserID = ut.UserID
                left outer join (select if(CurrentUserID = $user_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = $user_id or TargetUserID = $user_id) as t1
                on u.UserID = t1.uID
                where u.UserID != $user_id AND IsBanned != 1 AND t1.uID is null
                LIMIT $offset, $limit
           )as u
            LEFT JOIN userconnect cc ON cc.TargetUserID=u.UserID AND cc.CurrentUserID = $user_id
            left join neighborhood nbh ON u.NeighborhoodID=nbh.OGR_FID   
            ";            
            $query = $this->db->query($sql);
            $result = $query->result();
            $query->free_result();
        }
        return $result;
    }

    /**search user by location
     * 
     * @param type $total
     * @param type $user_id
     * @param type $lat
     * @param type $lon
     * @param type $limit
     * @param type $offset
     * @param type $gender
     * @param type $from_age
     * @param type $to_age
     * @return type
     */
    function searchUserbyCoordinate(&$total, $user_id = 0, $lat = null, $lon = null, $limit = 12, $offset = 0, $gender = '', $from_age = '', $to_age = '')
    {
        $active_day = Config_model::getConfig('MINGLE_ACTIVE_DAY');
        if (!$active_day){
            $active_day = 3;
        }
        $datesearch = date('Y-m-d H:i:s', strtotime( "-$active_day day", strtotime(gmdate('Y-m-d H:i:s'))));
        $where = '';
        // ANOMO-10561 Update Mingle query to show actives within the last 30 days
        if ($lat != null && $lon != null){
            $dist = 500; // mile
            $distanceFactor = 69; 
            $lon1 = $lon - $dist / abs(cos(deg2rad($lat)) * $distanceFactor) ;
            $lon2 = $lon + $dist / abs(cos(deg2rad($lat)) * $distanceFactor); 
            $lat1 = $lat - ($dist / $distanceFactor);
            $lat2 = $lat + ($dist / $distanceFactor);
            
            $isMeridian180 = 0;
            // from store procedure distanceSearch3_OP_2_v200
            if ($lon1 < -180){
                $isMeridian180 = 1;
                
                $lon1_2 = 360 + $lon1;
                $lon2_2 = 179.999;
                $lat1_2 = $lat1;
                $lat2_2 = $lat2;
                
                $lon1 = -179.999;
            }elseif($lon2 > 180){
                $isMeridian180 = 1;

                $lon1_2 = -179.999;
                $lon2_2 = -360 + $lon2;
                $lat1_2 = $lat1;
                $lat2_2 = $lat2;

                $lon2 = 179.999;
            }
            
            if ($isMeridian180 != 0){
                $where .= "AND ( (Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2) OR (Lon BETWEEN $lon1_2 AND $lon2_2 AND Lat BETWEEN $lat1_2 AND $lat2_2))";
            }else{
                $where .= "AND Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2 ";
            }
        }
        
        if ($from_age <= 13){
            $from_age = 0;
        }
        if ($to_age > 90) {
            $to_age = 0;
        }
        
        // Luong edited - Filter AgeLess User        
        $where .= " AND (( 1=1 ";
        if ($from_age > 0){
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-'.$from_age .' year', strtotime($now)));
            $where .= " AND BirthDate <= '".$youngest_date."'";
        }
        if ($to_age > 0){
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-'.$to_age - 1 .' year', strtotime($now)));
            $where .= " AND BirthDate > '".$oldest_date."'";
        }
        $where .= " ) or user.IsAgeLess = 1) ";
        
        //$where = " AND ((1=1 $where) or user.IsAgeLess = 1) ";
        //End
        
        if ($gender > 0){
            $where .= " AND Gender = '".$gender."'";
        }
        
        $totalSql = "
            SELECT count(UserID) as Total
            FROM user 
            left outer join (select if(CurrentUserID = $user_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = $user_id  or TargetUserID = $user_id) as t
            on user.UserID = t.uID
            WHERE user.UserID != $user_id and LastActivity >= '$datesearch' AND IsBanned != 1
                and Lat is not null and Lon is not null
                $where
                and t.uID is null
        ";            
        $totalQuery = $this->db->query($totalSql);
        $totalRow = $totalQuery->row();
        $total = $totalRow?$totalRow->Total:0;
        $totalQuery->free_result();
        $result = array();
        if ($total > 0){
            $sql = "
                SELECT
                    DISTINCT(u.UserID),u.UserName, u.Lat, u.Lon,u.Photo AS Avatar,u.ProfileStatus,u.Email,u.NeighborhoodID,u.BirthDate,u.Gender, u.IsAdmin, u.CoverPicture,
                    if(u.FacebookID != '', 1, '') as FacebookID,
                    0 AS IsOnline,
                    '' AS PlaceID,
                    0 AS Status ,
                    u.Distance,
                    if (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,
                    0 as IsBlock,
                    0 as IsBlockTarget,
                    if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                    if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), nbh.NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                    )AS PlaceName

                    , '' as NeighborhoodName

                FROM ( 
                        SELECT * 
                            , sqrt (pow($lat - Lat, 2) + pow($lon - Lon, 2)) as distance
                        FROM user 
                        left outer join (select if(CurrentUserID = $user_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = $user_id  or TargetUserID = $user_id) as t
                        on user.UserID = t.uID

                        WHERE user.UserID != $user_id and LastActivity >= '$datesearch' AND IsBanned != 1 
                        and Lat is not null and Lon is not null
                        $where
                        and t.uID is null
                        ORDER BY distance asc
                        LIMIT $offset, $limit
                ) as u
                LEFT JOIN userconnect cc ON cc.TargetUserID=u.UserID AND cc.CurrentUserID = $user_id
                left join neighborhood nbh ON u.NeighborhoodID=nbh.OGR_FID
            ";            
            $query = $this->db->query($sql);
            $result = $query->result();
            $query->free_result();
        }                
        return $result;
    }

    function getCurrentLocationOfUser($user_id){
        $sql = "select
                if(plc.PlaceID is not null, plc.PlaceID, '') as PlaceID
                ,if (plc.Name is not null, plc.Name, '') as PlaceName
                ,if(u.NeighborhoodID is not null, u.NeighborhoodID, '') as NeighborhoodID

                 from(
                        select neighborhoodID, lastCheckinID  from user where userid =$user_id
                ) as u

                LEFT JOIN usercheckin uc ON uc.Status = 1 and u.LastCheckinID is not null  and uc.ID = u.LastCheckinID AND uc.CheckOutDate > UTC_TIMESTAMP()
                LEFT JOIN place plc ON uc.PlaceID = plc.PlaceID";
       $query = $this->db->query($sql);
       return $query->row();
    }
    
    /*
     * block user function
     * 
     * push into a transaction
     */
    function blockUser($current_user_id, $target_user_id) {
        $return = false;
        try {
            $this->db->trans_begin();
            self::blockMsg($current_user_id, $target_user_id);
            
            //ANOMO-10922 Do not count blocked users in followers/following profile badges
            // remove favorite status if have
            $sql = "select * from userconnect where (CurrentUserID = $current_user_id AND TargetUserID = $target_user_id AND IsFavorite = 1)
                    OR (CurrentUserID = $target_user_id AND TargetUserID = $current_user_id AND IsFavorite = 1)";
            $query = $this->db->query($sql);
            $objFavorite = $query->result();
            if ($objFavorite){ 
                $aFollowing = array();
                $aFollower = array();
                $aCond = array();
                $aInfluencePoint = array();
                $createdDate = gmdate('Y-m-d H:i:s');
                foreach($objFavorite as $obj){
                    $aFollowing[] = $obj->CurrentUserID;
                    $aFollower[] = $obj->TargetUserID;
                    $aCond[] = "(CurrentUserID = $obj->CurrentUserID AND TargetUserID = $obj->TargetUserID)";
                    $aInfluencePoint[] = "($obj->TargetUserID, $obj->CurrentUserID, 2, -10, '$createdDate')";
                }
                
                // Influence point - log unfollower 
                if (sizeof($aInfluencePoint) > 0){
                    $insert_sql = "insert into influence_point (`UserID`, `FromUserID`, `Type`, `Point`, `CreatedDate`)
                                value ".implode(',', $aInfluencePoint);
                    $this->db->query($insert_sql);
                }

                // decrease #following     
                if (sizeof($aFollowing) > 0){
                    $update_sql = "update user set NumberOfFollowing = NumberOfFollowing - 1 where UserID IN (".implode(',', $aFollowing).")";
                    $this->db->query($update_sql);
                }

                // decrease #follower
                if (sizeof($aFollower) > 0){
                    $update_sql = "update user set `NumberOfFollower` = `NumberOfFollower` - 1, `Point` = `Point` - 10 where UserID IN (".implode(',', $aFollower).")";
                    $this->db->query($update_sql);
                }
                
                // should remove indexing on Solr server
                if (sizeof($aCond) > 0){
                    $remove_contact_sql = " update userconnect set IsIndexing = 4, IsFavorite = 0 where ".  implode('OR', $aCond);
                    $this->db->query($remove_contact_sql);
                }
                
                // then clear cache 
                self::invalidateCache($current_user_id);
                self::invalidateCache($target_user_id);
            }

            // remove notification history
            $sql = " delete from notificationhistory where (UserID = $current_user_id AND SendUserID = $target_user_id) OR (UserID = $target_user_id AND SendUserID = $current_user_id)";
            $this->db->query($sql);


            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $return = true;
                $this->db->trans_commit();
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }
        return $return;
    }
    
    function loginWithFb($fb_id, $email = '') {
        $photo_url = $this->config->item('s3_photo_url');
        $sql = "select UserID
                        ,if(UserName is null, '',UserName) as 'UserName'
                        , Gender, BirthDate, IsBanned, Deactive, Credits, GenderDating, IsVendor, IsAdmin,IsVerify, IsEmailVerify
                        , if (Password != '', 1, 0) as IsPassword
                        , if (Email is null, '', Email) as Email
                        , if (FacebookID is null, '', FacebookID) as FacebookID
                        , if (FbEmail is null, '', FbEmail) as FbEmail
                        , if (ProfileStatus is null, '', ProfileStatus) as ProfileStatus
                        , if (AboutMe is null, '', AboutMe) as AboutMe
                        , IF (Photo != '', CONCAT('" . $photo_url. "', Photo), '') as Avatar
                        , IF (FullPhoto != '', CONCAT('" . $photo_url. "', FullPhoto), '') as FullPhoto
                        , IF (CoverPicture != '', CONCAT('" . $photo_url. "', CoverPicture), CONCAT('" . $photo_url. "','".COVER_PICTURE_DEFAULT."') ) as CoverPicture
                        , AllowSendGiftNotice, AllowRevealNotice, AllowChatNotice, AllowAnomotionNotice, AllowCommentActivityNotice, AllowLikeActivityNotice, AllowFollowNotice, OnlyShowCountry

                from user 
                where FacebookID = '". $fb_id."'";
        if (!empty($email)){
            $sql .= " OR Email ='".$email."'";
        }
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    /**
     * 
     * @param type $data
     * 1: Like
     * 2: Follow
     * 3: Friend invite
     */
    function updateUserPoint($data, $type = 'plus') {
        if (!isset($data['UserID']) || !isset($data['Point'])){
            return false;
        }
        self::invalidateCache($data['UserID']);
        if ($type == 'plus'){
            $set = '';
            if ($data['Type'] == INF_POINT_LIKE){// like
                $set = ", `NumberPostLike` =  `NumberPostLike` + 1";
            }
            if ($data['Type'] == INF_POINT_INVITE_SMS){// invite friend
                $set = ", `NumberFriendInvite` = `NumberFriendInvite` + ".$data['NumberFriendInvite'];
            }
            
            if ($data['Type'] == INF_POINT_SHARE_FB){ // share to fb wall
                $set = ", `NumberShareFbWall` =  `NumberShareFbWall` + 1";
            }
            $sql = "update user set `Point` = `Point` + '" . $data['Point'] . "' $set where UserID = '" . $data['UserID'] . "'";
        }elseif ($type == 'minus'){
            $set = '';
            if ($data['Type'] == INF_POINT_LIKE){// like
                $set = ", `NumberPostLike` =  `NumberPostLike` - 1";
            }
            $sql = "update user set `Point` = `Point` - " . $data['Point'] . " $set where UserID = '" . $data['UserID'] . "'";
        }
        return $this->db->query($sql);
    }
    
    function numberBlock1Day($user_id){
        $sql = "select count(*) as Number from userblock
                where TargetUserID = $user_id AND DATE_FORMAT(Date, '%Y-%m-%d' ) = '".gmdate('Y-m-d')."'";
        $query = $this->db->query($sql);
        $row = $query->row();
        return $row?$row->Number: 0;
    }

    function getUserById($user_id)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('UserID', $user_id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function getListBanWords(){
        $this->db->select('word');
        $this->db->from('banword');
        $query = $this->db->get();
        $result = $query->result();
        $query->free_result();
        return $result;
    }
    
    function trackUserName($data){
        $this->db->insert('track_username', $data);
        return $this->db->insert_id();
    }
    
    function listUserBy($cond, &$total, $offset, $limit){
        $where = array();
        $sWhere = '';
        $result = null;
        if (isset($cond['SignUpDate']) && $cond['SignUpDate'] != ''){
            $where[] = "SignUpDate >='".$cond['SignUpDate']."'";
        }
        if (isset($cond['IsAddToWatchList']) && $cond['IsAddToWatchList'] > 0){
            $where[] = "IsAddToWatchList = ".$cond['IsAddToWatchList'];
        }
        if (sizeof($where) > 0){
            $sWhere = ' where '. implode(' AND ', $where);
        }
        $sqlTotal = "select count(UserID) as Total from user $sWhere";
        $queryTotal = $this->db->query($sqlTotal);
        $row = $queryTotal->row();
        $total = $row?$row->Total:0;
        if ($total > 0){
            $sql = "select UserID, UserName, Gender, BirthDate, IsAddToWatchList
                , if (Photo != '', concat('".$this->config->item('s3_photo_url')."', Photo), '') as Avatar from user $sWhere order by SignUpDate DESC 
                    limit $limit, $offset";
            $query = $this->db->query($sql);
            $result = $query->result();
        }
        return $result;
    }
    
    function detail_user($user_id, $target_user_id = 0){
        $select = "";
        if ($target_user_id > 0){
            $select = ", (select NumberOfChat from dailymatch where (CurrentUserID = $user_id AND TargetUserID = $target_user_id) OR (CurrentUserID = $target_user_id AND TargetUserID = $user_id)) as NumberOfChat";
        }
        $sql = "select UserID, UserName, Gender, BirthDate, SignUpDate, LastActivity, Notes, IsVerify
                $select
                , (select count(AnomotionID) as Total from anomotion where (UserID = $user_id OR TargetUserID = $user_id) AND Status = 3) as NumberGame
                , (select count(ActivityID) as Total from activity where FromUserID = $user_id) as NumberPost
                , (select count(ActivityID) as Total from activity_archive where FromUserID = $user_id) as NumberPostArchive
                , (select count(ID) as Total from activitycomment where UserID = $user_id ) as NumberComment
                , (select count(*) as Total from userconnect where TargetUserID = $user_id AND IsFavorite = 1) as NumberFollower
                from user where UserID = $user_id
            ";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    function updateHasFanPage($point = 0){
        if($point > 0){
            $sql = "update `user` set HasFanPage = 1 where point > $point and IsAgeLess = 0 and IsAdmin = 0 ";    
            $this->db->query($sql);
        }
        
    }
        
    function searchUserMatched($user_id = 0,$birthdate,$searcher_gender, $dating_gender = 0, $match_gender = 0, $from_age = '', $to_age = '',$total_interest = 0,$total_question = 0, $total_major = 0, $loop = 0)
    {
        
        //GET CONFIGURATION POINT        
        $INTEREST_POINT = Config_model::getConfig('MATCH_INTEREST_POINT');
        if (!$INTEREST_POINT){
            $INTEREST_POINT = 10;
        }
        $QUESTION_POINT = Config_model::getConfig('MATCH_QUESTION_POINT');
        if (!$QUESTION_POINT){
            $QUESTION_POINT = 1;
        }
        
        $MAJOR_POINT = 10;
        
        //RECENT
        $limit = Config_model::getConfig('MATCH_USER_LIMIT');
        if (!$limit){
            $limit = 250;
        }
         
        //RECENT
        $active_day = Config_model::getConfig('MATCH_ACTIVE_DAY');
        if (!$active_day){
            $active_day = 16;
        }        
        $datesearch = date('Y-m-d H:i:s', strtotime( "-$active_day day", strtotime(gmdate('Y-m-d H:i:s'))));
        
        //ELIMINATE MATCHING FROM
        $eliminate_from = Config_model::getConfig('MATCH_REPEAT_LIMITATION');
        if (!$eliminate_from){
            $eliminate_from = 12;
        }
        $date_eliminate = date('Y-m-d H:i:s', strtotime( "-$eliminate_from hour", strtotime(gmdate('Y-m-d H:i:s'))));
        //MINIMUM INTEREST TAG
        $MINIMUM_INTEREST = Config_model::getConfig('MATCH_MINIMUM_INTEREST');
        if (!$MINIMUM_INTEREST){
            $MINIMUM_INTEREST = $total_interest;
        }    
        if($total_interest < $MINIMUM_INTEREST){
            $MINIMUM_INTEREST = $total_interest;
        }  
        //REDUCE REQUIRED SHARE INTEREST in case looping
        if($loop > 0){
            if($loop == 2){
                $MINIMUM_INTEREST = 1;
            }else{
                $MINIMUM_INTEREST -= 2*($loop);                    
            }  
        }
        
        if($MINIMUM_INTEREST < 1){
            $MINIMUM_INTEREST = 1;
        }
        
        
        //MINIMUM MAJOR
        $MINIMUM_MAJOR = Config_model::getConfig('MATCH_MINIMUM_MAJOR');
        if (!$MINIMUM_MAJOR){
            $MINIMUM_MAJOR = $total_major;
        }    
        if($total_major < $MINIMUM_MAJOR){
            $MINIMUM_MAJOR = $total_major;
        }
        //REDUCE REQUIRED SHARE INTEREST in case looping
        if($loop > 0){
            if($loop == 2){
                $MINIMUM_MAJOR = 1;
            }else{
                $MINIMUM_MAJOR -= 2*($loop);                    
            }  
        }
        
        if($MINIMUM_MAJOR < 1){
            $MINIMUM_MAJOR = 1;
        }
        
        //MINIMUM ICEBREKER QUESTION
        $MINIMUM_QUESTION = Config_model::getConfig('MATCH_MINIMUM_QUESTION');
        if (!$MINIMUM_QUESTION){
            $MINIMUM_QUESTION = $total_question;
        }
        //SET REQUIRED QUESTION AS Total Question
        if($total_question < $MINIMUM_QUESTION){
            $MINIMUM_QUESTION = $total_question;
        }
        //REDUCE REQUIRED SHARE QUESTION in case looping
        if($loop > 0){
            if($loop == 2){
                $MINIMUM_QUESTION = 1;
            }else{
                $MINIMUM_QUESTION -= 5*($loop);
            }
        }    
        
        if($MINIMUM_QUESTION < 1){
            $MINIMUM_QUESTION = 1;
        }
        
        //BUILDING QUERIES            
        $where = '';
        
        //DATING
        $want_to_date = 0;
        if($dating_gender > 0){
            $want_to_date = 1;
        }        
        if($want_to_date == 1 && $match_gender == $dating_gender){
            $where .= " AND GenderDating > 0 ";
        }else if($want_to_date == 0){
            $where .= " AND GenderDating = 0 ";
        }
        if ($match_gender > 0 && $match_gender < 3){
            $where .= " AND Gender = $match_gender ";
        }
        
        //AGE
        /*
        $searcher_birthday = new DateTime($birthdate);                
        $now = new DateTime('now');                
        $interval = $searcher_birthday->diff($now);                
        $age = $interval->y;   

        if($loop < 3){                                        
            $search_from_age = 0;
            $search_to_age = 0;                                                 
            if($age && $age>0){
                if($searcher_gender == 1){
                    $search_from_age = $age - 5 - (5*$loop);
                    $search_to_age = $age + 4 + (4*$loop);;
                }else if($searcher_gender == 2){
                    $search_from_age = $age - 2 - (2*$loop);
                    $search_to_age = $age + 6 + (6*$loop);
                }
            }
            //If the above ranges exceed user bounds, use user bounds instead.
            if($search_from_age > $from_age){
                $from_age = $search_from_age;
            }
            if($search_to_age < $to_age){
                $to_age =  $search_to_age;
            }
            // age limit                       
            if ($from_age <= 13){
                $from_age = 0;
            }
            if ($to_age > 90) {
                $to_age = 0;
            }
        }else{
            //On 3rd expand� no age filter.
            $from_age = 0;
            $to_age = 0;
        }         
                            
        if ($from_age > 0){
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-'.$from_age .' year', strtotime($now)));
            $where .= " AND BirthDate <= '".$youngest_date."'";
        }
        if ($to_age > 0){
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-'.$to_age - 1 .' year', strtotime($now)));
            $where .= " AND BirthDate > '".$oldest_date."'";
        }
        */
        
        $sql = "
        SELECT
            DISTINCT(u.UserID),u.UserName, u.Lat, u.Lon,u.Photo AS Avatar,u.FullPhoto,u.ProfileStatus,u.Email,u.NeighborhoodID,u.BirthDate,u.Gender,
            if(u.FacebookID != '', 1, '') as FacebookID,
            0 AS IsOnline,
            '' AS PlaceID,
            0 AS Status ,                    
            if (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,
            0 as IsBlock,
            0 as IsBlockTarget,
            NumberOfFollower,NumberOfFollowing,Point,
            if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                            if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), nbh.NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
            )AS PlaceName

            , '' as NeighborhoodName  
            ,if(matched_tag.common_tag_count is null,0,matched_tag.common_tag_count) as 'SharedInterest'
            ,if(matched_major.common_major_count is null,0,matched_major.common_major_count) as 'SharedMajor'
            ,if(matched_ib.match_answer_count is null,0,matched_ib.match_answer_count) as 'SharedQuestion'
            ,(if(matched_tag.common_tag_count is null,0,matched_tag.common_tag_count)*$INTEREST_POINT+ 
              if(matched_major.common_major_count is null,0,matched_major.common_major_count)*$MAJOR_POINT+ 
            if(matched_ib.match_answer_count is null,0,matched_ib.match_answer_count)) as 'Matching'       
        FROM ( 
                SELECT u.*                         
                FROM user u
                left outer join (select if(CurrentUserID = $user_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = $user_id  or TargetUserID = $user_id) as t
                on u.UserID = t.uID
                left outer join (select TargetUserID as PreviousUserID from dailymatch where CurrentUserID = $user_id) as m
                on u.UserID = m.PreviousUserID                
                WHERE u.UserID != $user_id AND LastActivity >= '$datesearch' AND IsBanned != 1 and t.uID is null and m.PreviousUserID is null 
                and NOT EXISTS(select TargetUserID from dailymatch where (CreatedDate > '$date_eliminate' and (CurrentUserID = u.UserID or TargetUserID = u.UserID )) or (CurrentUserID = $user_id and TargetUserID = u.UserID) or (CurrentUserID = u.UserID and TargetUserID = $user_id) )   
                $where

                LIMIT $limit
        ) as u
        LEFT JOIN
		(
			select uid, common_tag_count					
			from
			(
				select ut1.UserID as uid, count(ut1.TagID) AS common_tag_count
				FROM usertag AS ut1 INNER JOIN usertag as ut2 ON ut1.TagID = ut2.TagID
				INNER JOIN tag AS t1 ON t1.TagID=ut1.TagID
				WHERE ut2.UserID = $user_id
				AND t1.TagCategoryID <> 0
				GROUP BY ut1.UserID
				HAVING ut1.UserId != $user_id						
				ORDER BY count(ut1.TagId) DESC
			) as innerquery			               
		) matched_tag ON u.UserID = matched_tag.uid
        
        LEFT JOIN
		(
			select uid, common_major_count					
			from
			(
				select ut1.UserID as uid, count(ut1.MajorID) AS common_major_count
				FROM usermajor AS ut1 INNER JOIN usermajor as ut2 ON ut1.MajorID = ut2.MajorID
				INNER JOIN major AS t1 ON t1.MajorID=ut1.MajorID
				WHERE ut2.UserID = $user_id
				GROUP BY ut1.UserID
				HAVING ut1.UserId != $user_id						
				ORDER BY count(ut1.MajorID) DESC
			) as innerquery			               
		) matched_major ON u.UserID = matched_major.uid
        
        LEFT JOIN
        (
            SELECT a2.UserID,sum(a2.Weight) as 'match_answer_count'
            FROM
            (select UserID,questionId, answerId FROM resultunique where userId = $user_id) a1
            inner join resultunique a2
            ON a1.answerId = a2.answerId and a1.QuestionID = a2.QuestionID and a2.UserId != $user_id
            GROUP BY a2.UserID
        ) matched_ib ON u.UserID = matched_ib.UserID
        LEFT JOIN userconnect cc ON cc.TargetUserID=u.UserID AND cc.CurrentUserID = $user_id
        LEFT JOIN neighborhood nbh ON u.NeighborhoodID=nbh.OGR_FID  
        HAVING (SharedInterest >= $MINIMUM_INTEREST or SharedQuestion >= $MINIMUM_QUESTION or SharedMajor >= $MINIMUM_MAJOR) 
        and (SharedInterest>0 or SharedQuestion>0 or SharedMajor >0)
        ORDER BY Matching desc
        LIMIT 1        
        ";                           
//        echo $sql . "<br/>==============================";
        $query = $this->db->query($sql);
        $result = $query->result();
        $query->free_result();       
        return $result;
    }

    function searchUserMatched2_bk($user_id = 0, $birthdate, $searcher_gender, $dating_gender = 0, $match_gender = 0, $from_age = '', $to_age = '', $total_interest = 0, $total_question = 0, $loop = 0)
    {

        //GET CONFIGURATION POINT
        $INTEREST_POINT = Config_model::getConfig('MATCH_INTEREST_POINT');
        if (!$INTEREST_POINT) {
            $INTEREST_POINT = 10;
        }
        $QUESTION_POINT = Config_model::getConfig('MATCH_QUESTION_POINT');
        if (!$QUESTION_POINT) {
            $QUESTION_POINT = 1;
        }

        //RECENT
        $limit = Config_model::getConfig('MATCH_USER_LIMIT');
        if (!$limit) {
            $limit = 250;
        }

        //RECENT
        $active_day = Config_model::getConfig('MATCH_ACTIVE_DAY');
        if (!$active_day) {
            $active_day = 16;
        }
        $datesearch = date('Y-m-d H:i:s', strtotime("-$active_day day", strtotime(gmdate('Y-m-d H:i:s'))));

        //ELIMINATE MATCHING FROM
        $eliminate_from = Config_model::getConfig('MATCH_REPEAT_LIMITATION');
        if (!$eliminate_from) {
            $eliminate_from = 12;
        }        
        $date_eliminate = date('Y-m-d H:i:s', strtotime("-$eliminate_from hour", strtotime(gmdate('Y-m-d H:i:s')))); 
        //MINIMUM INTEREST TAG
        $MINIMUM_INTEREST = Config_model::getConfig('MATCH_MINIMUM_INTEREST');
        if (!$MINIMUM_INTEREST) {
            $MINIMUM_INTEREST = $total_interest;
        }        
        if ($total_interest < $MINIMUM_INTEREST) {
            $MINIMUM_INTEREST = $total_interest-1;
        }
        //REDUCE REQUIRED SHARE INTEREST in case looping
        if ($loop > 0) {
            if ($loop == 2) {
                $MINIMUM_INTEREST = 1;
            } else {
                $MINIMUM_INTEREST -= 2*($loop);
            }
        }
        if($MINIMUM_INTEREST < 1){
            $MINIMUM_INTEREST = 1;
        }

        //MINIMUM ICEBREKER QUESTION
        $MINIMUM_QUESTION = Config_model::getConfig('MATCH_MINIMUM_QUESTION');
        if (!$MINIMUM_QUESTION) {
            $MINIMUM_QUESTION = $total_question;
        }        
        //SET REQUIRED QUESTION AS Total Question
        if ($total_question < $MINIMUM_QUESTION) {
            $MINIMUM_QUESTION = $total_question - 2;
        }

        //REDUCE REQUIRED SHARE QUESTION in case looping
        if ($loop > 0) {
            if ($loop == 2) {
                $MINIMUM_QUESTION = 0;
            } else {
                $MINIMUM_QUESTION -= 5*($loop);
            }
        }
        if($MINIMUM_QUESTION < 0){
            $MINIMUM_QUESTION = 0;
        }
                
        //BUILDING QUERIES
        $where = '';

        //DATING
        $want_to_date = 0;
        if ($dating_gender > 0) {
            $want_to_date = 1;
        }
        if ($want_to_date == 1 && $match_gender == $dating_gender) {
            $where .= " AND GenderDating > 0 ";
        } else if ($want_to_date == 0) {
            $where .= " AND GenderDating = 0 ";
        }
        if ($match_gender > 0 && $match_gender < 3) {
            $where .= " AND Gender = $match_gender ";
        }

        //AGE
        $searcher_birthday = new DateTime($birthdate);
        $now = new DateTime('now');
        $interval = $searcher_birthday->diff($now);
        $age = $interval->y;

        $search_from_age = 0;
        $search_to_age = 0;
        // Hung mod...
        $prev_search_from_age = -1;
        $prev_search_to_age = -1;

        if ($loop < 2) {
            if ($age && $age > 0) {
                if($loop == 0){
                    if($age >= 13 && $age <= 27){
                        $search_from_age = $age - 5;
                        $search_to_age = $age + 5;
                    }else if($age >= 28 && $age <= 35){
                        $search_from_age = $age - 7;
                        $search_to_age = $age + 7;
                    }else if($age >= 36 && $age <= 45){
                        $search_from_age = $age - 10;
                        $search_to_age = $age + 10;
                    }else{
                        $search_from_age = $age - 12;
                        $search_to_age = $age + 12;
                    }
                }else{
                    if($age >= 13 && $age <= 27){
                        $search_from_age = $age - 6;
                        $search_to_age = $age + 6;
                    }else if($age >= 28 && $age <= 35){
                        $search_from_age = $age - 10;
                        $search_to_age = $age + 10;
                    }else if($age >= 36 && $age <= 45){
                        $search_from_age = $age - 12;
                        $search_to_age = $age + 12;
                    }else{
                        $search_from_age = $age - 15;
                        $search_to_age = $age + 15;
                    }
                }
            }

            //If the above ranges exceed user bounds, use user bounds instead.
            if ($search_from_age < $from_age) {
                $search_from_age = $from_age;
            }
            if ($search_to_age > $to_age) {
                $search_to_age = $to_age;
            }
            if ($prev_search_from_age > 0 && $prev_search_from_age < $from_age) {
                $prev_search_from_age = $from_age;
            }
            if ($prev_search_to_age > 0 && $prev_search_to_age > $to_age) {
                $prev_search_to_age = $to_age;
            }
            // age limit
            if ($search_from_age <= 13) {
                $search_from_age = 0;
            }
            if ($search_to_age > 90) {
                $search_to_age = 0;
            }
            // age limit
            if ($prev_search_from_age > 0 && $prev_search_from_age <= 13) {
                $prev_search_from_age = 0;
            }
            if ($prev_search_to_age > 90) {
                $prev_search_to_age = 0;
            }
        }else{
            $search_from_age = $from_age;
            $search_to_age = $to_age;
        }
        
        /*
        echo "<br/>";
        echo "User age:" . $age . "<br/>";
        echo "Filter from: $from_age<br/>";
        echo "Filter To: $to_age<br/>";
        echo "from age: " . $search_from_age . "<br/>";
        echo "to age: " . $search_to_age . "<br/>";
        echo "Prev from age: " . $prev_search_from_age . "<br/>";
        echo "Prev to age: " . $prev_search_to_age . "<br/>";
        echo "Interest require: " . $MINIMUM_INTEREST. "<br/>";
        echo "Question require: " . $MINIMUM_QUESTION. "<br/>";
        */
        $ageQuery = '';
        if ($search_from_age > 0) {
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-' . $search_from_age . ' year', strtotime($now)));
            if ($prev_search_from_age < 0){
                $ageQuery .= " BirthDate <= '" . $youngest_date . "'";
            }
            if ($prev_search_from_age > 0 && $prev_search_from_age != $search_from_age) {
                $prev_youngest_date = date('Y-m-d', strtotime('-' . $prev_search_from_age . ' year', strtotime($now)));
                $ageQuery .= " (BirthDate <= '$youngest_date' AND BirthDate > '$prev_youngest_date')";
            }
        }
        if ($search_to_age > 0) {
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-' . $search_to_age - 1 . ' year', strtotime($now)));
            if ($prev_search_to_age < 0){
                $condKey = ($ageQuery)? 'AND' : '';
                $ageQuery .= " $condKey BirthDate > '" . $oldest_date . "'";
            }
            if ($prev_search_to_age > 0 && $prev_search_to_age != $search_to_age) {
                $prev_oldest_date = date('Y-m-d', strtotime('-' . $prev_search_to_age - 1 . ' year', strtotime($now)));
                $condKey = ($prev_search_from_age > 0 && $prev_search_from_age != $search_from_age)? 'OR' : 'AND';
                $ageQuery .= " $condKey (BirthDate > '$oldest_date' AND BirthDate < '$prev_oldest_date')";
            }
        }

        if ($ageQuery != '')
            $where .= "AND ($ageQuery)";

        $sql = "
		SELECT u.UserID,u.UserName, u.Lat, u.Lon,u.Photo AS Avatar,u.FullPhoto,u.ProfileStatus,u.Email,u.NeighborhoodID,u.BirthDate,u.Gender,
					if(u.FacebookID != '', 1, '') as FacebookID,
					0 AS IsOnline,
					'' AS PlaceID,
					0 AS Status ,
					if (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,
					0 as IsBlock,
					0 as IsBlockTarget,
					NumberOfFollower,NumberOfFollowing,Point,
					if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ),
													if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), nbh.NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, ''))
					)AS PlaceName

					, '' as NeighborhoodName
					,if(matched_tag.common_tag_count is null,0,matched_tag.common_tag_count) as 'SharedInterest'
					,count(ut1.AnswerId) as 'SharedQuestion'
					,(if(matched_tag.common_tag_count is null,0,matched_tag.common_tag_count)*$INTEREST_POINT+sum(ut1.Weight)) as 'Matching'
        FROM (
                SELECT u.*
		        FROM user u
		        left outer join (
			        select if(CurrentUserID = $user_id, TargetUserID, CurrentUserID) as uID
			        from userblock
			        where CurrentUserID = $user_id or TargetUserID = $user_id) as t on u.UserID = t.uID
		        LEFT join (
		            select CurrentUserID, TargetUserID
		            from dailymatch
		            where
                    CreatedDate > '$date_eliminate' OR
                    TargetUserID = $user_id OR CurrentUserID = $user_id
		        ) as prematch on prematch.CurrentUserID = u.UserID or prematch.TargetUserID = u.UserID
		        WHERE u.UserID != $user_id
					AND LastActivity >= '$datesearch'
					AND IsBanned != 1
					and t.uID is null
					$where					
			        AND prematch.CurrentUserID is null and prematch.TargetUserID is null
                limit $limit
        ) as u
        LEFT JOIN userconnect cc ON cc.TargetUserID=u.UserID AND cc.CurrentUserID = $user_id
        LEFT JOIN neighborhood nbh ON u.NeighborhoodID=nbh.OGR_FID
        LEFT join
        (
        		select uid, common_tag_count,
        		(select common_tag_count/count(*) from usertag as ut3 INNER JOIN tag ON ut3.TagID = tag.TagID
        						WHERE ut3.userID = uid AND tag.TagCategoryID <> 0)
        						 AS matchPercentage
        		from
        		(select ut1.UserID as uid, count(ut1.TagID) AS common_tag_count
        		FROM usertag AS ut1 INNER JOIN usertag as ut2 ON ut1.TagID = ut2.TagID INNER JOIN tag AS t1 ON t1.TagID=ut1.TagID
        		WHERE ut2.UserID = $user_id
        		AND t1.TagCategoryID <> 0
        		GROUP BY ut1.UserID
        		HAVING ut1.UserId != $user_id
        		AND common_tag_count>=$MINIMUM_INTEREST
        		ORDER BY count(ut1.TagId) DESC) as innerquery
        		ORDER BY matchPercentage DESC, common_tag_count DESC
        ) as matched_tag
        on u.UserID = matched_tag.uid
        inner join resultunique as ut1 on matched_tag.uid = ut1.UserID
        LEFT JOIN (select * from resultunique where userid=$user_id) as ut2
        ON ut1.AnswerId = ut2.AnswerId
        WHERE ut2.UserID = $user_id
        GROUP BY ut1.UserID
        HAVING ut1.UserId != $user_id
        AND SharedQuestion>=$MINIMUM_QUESTION
        ORDER BY sum(ut1.Weight) DESC
        LIMIT 1;
        ";
//        echo $sql . "<br/>==============================";
        $query = $this->db->query($sql);
        $result = $query->result();
        $query->free_result();
        return $result;
    }
    
    function countCommonTagByUser($user_id){
        $sql = "
        select count(uc.TagID) as 'total'
        from usertag uc inner join tag on uc.TagID = tag.TagID and TagCategoryID <> 0 and uc.UserID = $user_id
        ";
        $query = $this->db->query($sql);
        $result = $query->result();        
        if($result[0]){
            return $result[0]->total;
        }
        return 0;
    }
    
    function countCommonMajorByUser($user_id){
        $sql = "
        select count(uc.MajorID) as 'total'
        from usermajor uc inner join major on uc.MajorID = major.MajorID  and uc.UserID = $user_id
        ";
        $query = $this->db->query($sql);
        $result = $query->result();        
        if($result[0]){
            return $result[0]->total;
        }
        return 0;
    }
    
    function countUniqueQuestionByUser($user_id){
        $this->db->select('count(QuestionID) as total');
        $this->db->from('resultunique');
        $this->db->where('UserID', $user_id);
        $query = $this->db->get();
        $result = $query->result();        
        if($result[0]){
            return $result[0]->total;
        }
        return 0;
    }
    
    function getListTagOfMatchUser($user_id, $target_user_id, $matched_only = 0){
        $having = "";
        if($matched_only){
           $having = " HAVING Matched = 1 "; 
        }
        $sql = "
        SELECT ut1.TagID,t1.`Name`,t1.TagCategoryID,if(ut2.TagID is null,0,1) as 'Matched'
        FROM (select * from usertag where UserID = $target_user_id) AS ut1
        INNER JOIN tag AS t1 ON t1.TagID=ut1.TagID
        LEFT JOIN usertag as ut2 ON ut1.TagID = ut2.TagID and t1.TagCategoryID <>0 and ut2.UserID = $user_id
        $having
        ORDER BY Matched desc, t1.`Name` asc
        ";
        
        $query = $this->db->query($sql);
        $result = $query->result();
        $query->free_result();
                      
        return $result;
        
    }
    
    function getListMajorOfMatchUser($user_id, $target_user_id, $matched_only = 0){
        $having = "";
        if($matched_only){
           $having = " HAVING Matched = 1 "; 
        }
        $sql = "
        SELECT ut1.MajorID as TagID,t1.`Name`,if(ut2.MajorID is null,0,1) as 'Matched', 1 as TagCategoryID 
        FROM (select * from usermajor where UserID = $target_user_id) AS ut1
        INNER JOIN major AS t1 ON t1.MajorID=ut1.MajorID
        LEFT JOIN usermajor as ut2 ON ut1.MajorID = ut2.MajorID and ut2.UserID = $user_id
        $having
        ORDER BY Matched desc, t1.`Name` asc
        ";
        
        $query = $this->db->query($sql);
        $result = $query->result();
        $query->free_result();
                      
        return $result;
        
    }
    
    function getListQuestionOfMatchUser($user_id, $target_user_id,$size = 0, $last_id = 0){
        
        $limit = "";
        $where = "";
        if($size){
            $limit = " Limit $size";
        }
        
        if($last_id){
            $where = " AND question.QuestionID > $last_id";    
        }
        
        $sql = "
        select question.QuestionID, answer.AnswerID, question.Content as 'Question', answer.Content  as 'Answer'
        from resultunique a1 
        inner join resultunique a2 ON a1.AnswerID = a2.AnswerID 
        inner join question on a2.QuestionID = question.QuestionID
        inner join answer on a2.AnswerID = answer.AnswerID
        WHERE a1.UserID = $target_user_id and a2.UserID = $user_id
        $where
        ORDER BY question.QuestionID
        $limit
        ";        
        $query = $this->db->query($sql);
        $result = $query->result();
        $query->free_result();
                      
        return $result;
        
    }
    
    function clearFanpageNofify($user_id, $targetUserInfo){
        // the celeb views his profile
        // clear all notification for that celeb
        if ($user_id == $targetUserInfo->UserID && ($targetUserInfo->IsAgeLess == 1 || $targetUserInfo->IsAdmin == 1 || $targetUserInfo->HasFanPage == 1)){
            $sql = "update notificationhistory set IsRead = 0, UnView = 0
                    where UserID = $user_id  AND Type = ". HISTORY_FANPAGE_POST;
            $this->db->query($sql);
        }elseif ($user_id != $targetUserInfo->UserID){
            // if you followed the celeb
            $sql = "update notificationhistory set IsRead = 0, UnView = 0
                    where UserID = $user_id AND SendUserID = $targetUserInfo->UserID AND Type = ". HISTORY_AGELESS_POST;
            $this->db->query($sql);
        }
    }
    
    function getListNewUser($cond, &$total, $offset, $limit) {
        $where = " DATE_FORMAT(SignUpDate, '%Y-%m-%d') >= '". $cond['new_user_date'] ."'";
        if (isset($cond['keyword'])) {
            $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
            $cond['keyword'] = mysqli_real_escape_string($con, $cond['keyword']);
            $where .= " AND (`UserName` LIKE '%{$cond['keyword']}%'  OR `Email` LIKE '%{$cond['keyword']}%' )";
            
        }
        $sqlTotal = 'select count(UserID) as Total from user where '.$where;
        $totalResult = $this->db->query($sqlTotal);
        $row = $totalResult->row();
        $total = ($row)?$row->Total:0;
        if ($total > 0){
            $sql = "select u.*, v.VerifiedDate, v.Phone as VerifiedPhone, v.SendStatus
                    , (select count(*) from dailymatch d where d.CurrentUserID=u.UserID) as DailyMatch
                    from (select * from user where $where order by SignUpDate DESC limit $offset, $limit) as u
                    left join userverification v ON v.UserID = u.UserID
                    order by SignUpDate DESC
                    ";
            $query1 = $this->db->query($sql);
            return $query1->result();
        }
        return null;
    }
    
    function getListDailyMatch($user_id, &$total, $offset, $limit) {
        $sqlTotal = 'select count(*) as Total from dailymatch where CurrentUserID = '. $user_id;
        $totalResult = $this->db->query($sqlTotal);
        $row = $totalResult->row();
        $total = ($row)?$row->Total:0;
        if ($total > 0){
            $sql = "select u.UserID, u.UserName, d.*
                    from (
                        select * from dailymatch where CurrentUserID = $user_id order by CreatedDate DESC limit $offset, $limit
                     ) as d
                    left join user u ON u.UserID = d.TargetUserID
                    order by d.CreatedDate DESC
                    ";
            $query1 = $this->db->query($sql);
            return $query1->result();
        }
        return null;
    }
    
    function searchFollowing($keyword, $user_id, $limit, $offset, &$total)
    {
        $totalSql = "select count(temp.UserID) as Total from (
                    select TargetUserID as UserID from userconnect where CurrentUserID=$user_id AND IsFavorite = 1
                    
                ) as temp " ;
        
        $sql = "select u.UserID ,u.UserName,u.Lat,u.Lon,u.Photo AS Avatar,u.ProfileStatus, u.NeighborhoodID,u.Email, u.BirthDate,u.Gender, u.IsAdmin, u.CoverPicture,
                    if(u.FacebookID != '', 1, '') as FacebookID,
                    0 AS IsOnline,
                    '' AS PlaceID,
                    0 AS Status,
                    1 as IsFavorite,
                    0 as IsBlock,
                    0 as IsBlockTarget
    		from (
                        select TargetUserID as UserID from userconnect where CurrentUserID=$user_id AND IsFavorite = 1
                        
                     ) as temp
    		left join user u ON u.UserID=temp.UserID
    		where u.IsBanned = 0
    		";
        if ($keyword != '') {
            $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
            $keyword = mysqli_real_escape_string($con, $keyword);
            $sql .= " AND LCASE(u.UserName) like '%$keyword%'";
            $totalSql .= " left join user u ON u.UserID = temp.UserID";
            $totalSql .= " where LCASE(u.UserName) like '%$keyword%'";
        }
        
        $totalQuery = $this->db->query($totalSql);
        $totalRow = $totalQuery->row();
        $total = $totalRow?$totalRow->Total:0;
        if ($total > 0){
            $sql .= " order by u.UserName ASC";
            if ($limit > 0) {
                $sql .= " limit $offset, $limit";
            }

            // will get IsFavorite, Current checkin place, neighborhood
            $fullSql = " select t.* 
                            ,if(nbh.OGR_FID is not null, nbh.OGR_FID, '') as NeighborhoodID
                            ,if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '', concat(nbh.NAME, ', ', nbh.STATE), nbh.NAME), if (nbh.COUNTY is not null, nbh.COUNTY, '')) AS PlaceName
                            ,'' AS NeighborhoodName
                        from ($sql) as t

                        LEFT JOIN neighborhood nbh ON t.NeighborhoodID = nbh.OGR_FID
                    ";
            $query1 = $this->db->query($fullSql);
            $retVal = $query1->result();
            $query1->free_result();
            return $retVal;
        }
        return array();
    }
    
    /**ANOMO-12912
        [UW only] Relax the constraints of daily match
    */
    function searchUserMatched2($user_id = 0, $birthdate, $searcher_gender, $dating_gender = 0, $match_gender = 0, $from_age = '', $to_age = '', $total_interest = 0, $total_question = 0, $loop = 0)
    {

        //GET CONFIGURATION POINT
        $INTEREST_POINT = Config_model::getConfig('MATCH_INTEREST_POINT');
        if (!$INTEREST_POINT) {
            $INTEREST_POINT = 10;
        }
        $QUESTION_POINT = Config_model::getConfig('MATCH_QUESTION_POINT');
        if (!$QUESTION_POINT) {
            $QUESTION_POINT = 1;
        }

        //RECENT
        $limit = Config_model::getConfig('MATCH_USER_LIMIT');
        if (!$limit) {
            $limit = 250;
        }

        //RECENT
        $active_day = Config_model::getConfig('MATCH_ACTIVE_DAY');
        if (!$active_day) {
            $active_day = 16;
        }
        $datesearch = date('Y-m-d H:i:s', strtotime("-$active_day day", strtotime(gmdate('Y-m-d H:i:s'))));

        //ELIMINATE MATCHING FROM
        $eliminate_from = Config_model::getConfig('MATCH_REPEAT_LIMITATION');
        if (!$eliminate_from) {
            $eliminate_from = 12;
        }        
        $date_eliminate = date('Y-m-d H:i:s', strtotime("-$eliminate_from hour", strtotime(gmdate('Y-m-d H:i:s')))); 
        //MINIMUM INTEREST TAG
        $MINIMUM_INTEREST = Config_model::getConfig('MATCH_MINIMUM_INTEREST');
        if (!$MINIMUM_INTEREST) {
            $MINIMUM_INTEREST = $total_interest;
        }        
        if ($total_interest < $MINIMUM_INTEREST) {
            $MINIMUM_INTEREST = $total_interest-1;
        }
        //REDUCE REQUIRED SHARE INTEREST in case looping
        if ($loop > 0) {
            if ($loop == 2) {
                $MINIMUM_INTEREST = 1;
            } else {
                $MINIMUM_INTEREST -= 2*($loop);
            }
        }
        if($MINIMUM_INTEREST < 1){
            $MINIMUM_INTEREST = 1;
        }

        //MINIMUM ICEBREKER QUESTION
        $MINIMUM_QUESTION = Config_model::getConfig('MATCH_MINIMUM_QUESTION');
        if (!$MINIMUM_QUESTION) {
            $MINIMUM_QUESTION = $total_question;
        }        
        //SET REQUIRED QUESTION AS Total Question
        if ($total_question < $MINIMUM_QUESTION) {
            $MINIMUM_QUESTION = $total_question - 2;
        }

        //REDUCE REQUIRED SHARE QUESTION in case looping
        if ($loop > 0) {
            if ($loop == 2) {
                $MINIMUM_QUESTION = 0;
            } else {
                $MINIMUM_QUESTION -= 5*($loop);
            }
        }
        if($MINIMUM_QUESTION < 0){
            $MINIMUM_QUESTION = 0;
        }
                
        //BUILDING QUERIES
        $where = '';

        //DATING
        $want_to_date = 0;
        if ($dating_gender > 0) {
            $want_to_date = 1;
        }
        if ($want_to_date == 1 && $match_gender == $dating_gender) {
            $where .= " AND GenderDating > 0 ";
        } else if ($want_to_date == 0) {
            $where .= " AND GenderDating = 0 ";
        }
        if ($match_gender > 0 && $match_gender < 3) {
            $where .= " AND Gender = $match_gender ";
        }

        // ANOMO-12912 [UW only] Relax the constraints of daily match
        // Ignore age filter
        //AGE
        /*
        $searcher_birthday = new DateTime($birthdate);
        $now = new DateTime('now');
        $interval = $searcher_birthday->diff($now);
        $age = $interval->y;

        $search_from_age = 0;
        $search_to_age = 0;
        // Hung mod...
        $prev_search_from_age = -1;
        $prev_search_to_age = -1;

        if ($loop < 2) {
            if ($age && $age > 0) {
                if($loop == 0){
                    if($age >= 13 && $age <= 27){
                        $search_from_age = $age - 5;
                        $search_to_age = $age + 5;
                    }else if($age >= 28 && $age <= 35){
                        $search_from_age = $age - 7;
                        $search_to_age = $age + 7;
                    }else if($age >= 36 && $age <= 45){
                        $search_from_age = $age - 10;
                        $search_to_age = $age + 10;
                    }else{
                        $search_from_age = $age - 12;
                        $search_to_age = $age + 12;
                    }
                }else{
                    if($age >= 13 && $age <= 27){
                        $search_from_age = $age - 6;
                        $search_to_age = $age + 6;
                    }else if($age >= 28 && $age <= 35){
                        $search_from_age = $age - 10;
                        $search_to_age = $age + 10;
                    }else if($age >= 36 && $age <= 45){
                        $search_from_age = $age - 12;
                        $search_to_age = $age + 12;
                    }else{
                        $search_from_age = $age - 15;
                        $search_to_age = $age + 15;
                    }
                }
            }

            //If the above ranges exceed user bounds, use user bounds instead.
            if ($search_from_age < $from_age) {
                $search_from_age = $from_age;
            }
            if ($search_to_age > $to_age) {
                $search_to_age = $to_age;
            }
            if ($prev_search_from_age > 0 && $prev_search_from_age < $from_age) {
                $prev_search_from_age = $from_age;
            }
            if ($prev_search_to_age > 0 && $prev_search_to_age > $to_age) {
                $prev_search_to_age = $to_age;
            }
            // age limit
            if ($search_from_age <= 13) {
                $search_from_age = 0;
            }
            if ($search_to_age > 90) {
                $search_to_age = 0;
            }
            // age limit
            if ($prev_search_from_age > 0 && $prev_search_from_age <= 13) {
                $prev_search_from_age = 0;
            }
            if ($prev_search_to_age > 90) {
                $prev_search_to_age = 0;
            }
        }else{
            $search_from_age = $from_age;
            $search_to_age = $to_age;
        }
        */
        /*
        echo "<br/>";
        echo "User age:" . $age . "<br/>";
        echo "Filter from: $from_age<br/>";
        echo "Filter To: $to_age<br/>";
        echo "from age: " . $search_from_age . "<br/>";
        echo "to age: " . $search_to_age . "<br/>";
        echo "Prev from age: " . $prev_search_from_age . "<br/>";
        echo "Prev to age: " . $prev_search_to_age . "<br/>";
        echo "Interest require: " . $MINIMUM_INTEREST. "<br/>";
        echo "Question require: " . $MINIMUM_QUESTION. "<br/>";
        */
        /*
        $ageQuery = '';
        if ($search_from_age > 0) {
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-' . $search_from_age . ' year', strtotime($now)));
            if ($prev_search_from_age < 0){
                $ageQuery .= " BirthDate <= '" . $youngest_date . "'";
            }
            if ($prev_search_from_age > 0 && $prev_search_from_age != $search_from_age) {
                $prev_youngest_date = date('Y-m-d', strtotime('-' . $prev_search_from_age . ' year', strtotime($now)));
                $ageQuery .= " (BirthDate <= '$youngest_date' AND BirthDate > '$prev_youngest_date')";
            }
        }
        if ($search_to_age > 0) {
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-' . $search_to_age - 1 . ' year', strtotime($now)));
            if ($prev_search_to_age < 0){
                $condKey = ($ageQuery)? 'AND' : '';
                $ageQuery .= " $condKey BirthDate > '" . $oldest_date . "'";
            }
            if ($prev_search_to_age > 0 && $prev_search_to_age != $search_to_age) {
                $prev_oldest_date = date('Y-m-d', strtotime('-' . $prev_search_to_age - 1 . ' year', strtotime($now)));
                $condKey = ($prev_search_from_age > 0 && $prev_search_from_age != $search_from_age)? 'OR' : 'AND';
                $ageQuery .= " $condKey (BirthDate > '$oldest_date' AND BirthDate < '$prev_oldest_date')";
            }
        }

        if ($ageQuery != '')
            $where .= "AND ($ageQuery)";
        */ // end Ignore age filter
        
        $sql = "
		SELECT u.UserID,u.UserName, u.Lat, u.Lon,u.Photo AS Avatar,u.FullPhoto,u.ProfileStatus,u.Email,u.NeighborhoodID,u.BirthDate,u.Gender,
					if(u.FacebookID != '', 1, '') as FacebookID,
					0 AS IsOnline,
					'' AS PlaceID,
					0 AS Status ,
					if (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,
					0 as IsBlock,
					0 as IsBlockTarget,
					NumberOfFollower,NumberOfFollowing,Point,
					if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ),
													if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), nbh.NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, ''))
					)AS PlaceName

					, '' as NeighborhoodName
                                        ,0 as 'SharedMajor'
					,if(matched_tag.common_tag_count is null,0,matched_tag.common_tag_count) as 'SharedInterest'
					,count(ut1.AnswerId) as 'SharedQuestion'
					,(if(matched_tag.common_tag_count is null,0,matched_tag.common_tag_count)*$INTEREST_POINT+sum(ut1.Weight)) as 'Matching'
        FROM (
                SELECT u.*
		        FROM user u
		        left outer join (
			        select if(CurrentUserID = $user_id, TargetUserID, CurrentUserID) as uID
			        from userblock
			        where CurrentUserID = $user_id or TargetUserID = $user_id) as t on u.UserID = t.uID
		        LEFT join (
		            select CurrentUserID, TargetUserID
		            from dailymatch
		            where
                    CreatedDate > '$date_eliminate' OR
                    TargetUserID = $user_id OR CurrentUserID = $user_id
		        ) as prematch on prematch.CurrentUserID = u.UserID or prematch.TargetUserID = u.UserID
		        WHERE u.UserID != $user_id
					AND LastActivity >= '$datesearch'
					AND IsBanned != 1
					and t.uID is null
					$where					
			        AND prematch.CurrentUserID is null and prematch.TargetUserID is null
                limit $limit
        ) as u
        LEFT JOIN userconnect cc ON cc.TargetUserID=u.UserID AND cc.CurrentUserID = $user_id
        LEFT JOIN neighborhood nbh ON u.NeighborhoodID=nbh.OGR_FID
        LEFT join
        (
        		select uid, common_tag_count,
        		(select common_tag_count/count(*) from usertag as ut3 INNER JOIN tag ON ut3.TagID = tag.TagID
        						WHERE ut3.userID = uid AND tag.TagCategoryID <> 0)
        						 AS matchPercentage
        		from
        		(select ut1.UserID as uid, count(ut1.TagID) AS common_tag_count
        		FROM usertag AS ut1 INNER JOIN usertag as ut2 ON ut1.TagID = ut2.TagID INNER JOIN tag AS t1 ON t1.TagID=ut1.TagID
        		WHERE ut2.UserID = $user_id
        		AND t1.TagCategoryID <> 0
        		GROUP BY ut1.UserID
        		HAVING ut1.UserId != $user_id
        		AND common_tag_count>=$MINIMUM_INTEREST
        		ORDER BY count(ut1.TagId) DESC) as innerquery
        		ORDER BY matchPercentage DESC, common_tag_count DESC
        ) as matched_tag
        on u.UserID = matched_tag.uid
        inner join resultunique as ut1 on matched_tag.uid = ut1.UserID
        LEFT JOIN (select * from resultunique where userid=$user_id) as ut2
        ON ut1.AnswerId = ut2.AnswerId
        WHERE ut2.UserID = $user_id
        GROUP BY ut1.UserID
        HAVING ut1.UserId != $user_id
        AND SharedQuestion>=$MINIMUM_QUESTION
        ORDER BY sum(ut1.Weight) DESC
        LIMIT 1;
        ";
//        echo $sql . "<br/>==============================";
        $query = $this->db->query($sql);
        $result = $query->result();
        $query->free_result();
        return $result;
    }
    
    /**Change match algorithm to just pick a random person if it fails
     * 
     * @param type $user_id
     */
    function matchRandom($user_id){
        //ELIMINATE MATCHING FROM
        $eliminate_from = Config_model::getConfig('MATCH_REPEAT_LIMITATION');
        if (!$eliminate_from){
            $eliminate_from = 12;
        }
        $date_eliminate = date('Y-m-d H:i:s', strtotime( "-$eliminate_from hour", strtotime(gmdate('Y-m-d H:i:s'))));
        
        $sql = "
        SELECT
            DISTINCT(u.UserID),u.UserName, u.Lat, u.Lon,u.Photo AS Avatar,u.FullPhoto,u.ProfileStatus,u.Email,u.NeighborhoodID,u.BirthDate,u.Gender,
            if(u.FacebookID != '', 1, '') as FacebookID,
            0 AS IsOnline,
            '' AS PlaceID,
            0 AS Status ,                    
            if (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,
            0 as IsBlock,
            0 as IsBlockTarget,
            NumberOfFollower,NumberOfFollowing,Point,
            if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                            if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), nbh.NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
            )AS PlaceName

            ,'' as NeighborhoodName  
            ,0 as 'SharedInterest'
            ,0 as 'SharedMajor'
            ,0 as 'SharedQuestion'
            ,0 as 'Matching'       
        FROM ( 
                SELECT u.*                         
                FROM user u
                left outer join (select if(CurrentUserID = $user_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = $user_id  or TargetUserID = $user_id) as t
                on u.UserID = t.uID
                left outer join (select TargetUserID as PreviousUserID from dailymatch where CurrentUserID = $user_id) as m
                on u.UserID = m.PreviousUserID                
                WHERE u.UserID != $user_id  AND IsBanned != 1 and t.uID is null and m.PreviousUserID is null 
                and NOT EXISTS(select TargetUserID from dailymatch where (CreatedDate > '$date_eliminate' and (CurrentUserID = u.UserID or TargetUserID = u.UserID )) or (CurrentUserID = $user_id and TargetUserID = u.UserID) or (CurrentUserID = u.UserID and TargetUserID = $user_id) )   
                
                order by rand()
                limit 1
        ) as u
        LEFT JOIN userconnect cc ON cc.TargetUserID=u.UserID AND cc.CurrentUserID = $user_id
        LEFT JOIN neighborhood nbh ON u.NeighborhoodID=nbh.OGR_FID";

        $query = $this->db->query($sql);
        return $query->result();
    }
}

?>