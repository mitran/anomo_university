<?php
class Secret_Picture_model extends CI_Model {
    public $cached = false;
    function __construct() {
        parent::__construct();
        $CI = & get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
    }
    
	function add($data) {
		$this->db->insert('secretpicture', $data);
                if ($this->cached){
                    $this->cached->delete('secret_picture_of_user'.$data['UserID']);
                }
		return $this->db->insert_id();
	}
	
	function update($data, $cond) {
		return $this->db->update('secretpicture', $data, $cond);
	}
	
	function delete($cond){
                if ($this->cached){
                    $this->cached->delete('secret_picture_of_user'.$cond['UserID']);
                }
		$this->db->delete('secretpicture', $cond);
	}
	
	function check($user_id, $order){
		$this->db->select('*');
		$this->db->from('secretpicture');
		$this->db->where('UserID', $user_id);
		$this->db->where('Order', $order);
		$query = $this->db->get();
		return $query->row();
	}
	
	function getSecretPictureInfo($picture_id){
		$this->db->select('*');
		$this->db->from('secretpicture');
		$this->db->where('PictureID', $picture_id);
		$query = $this->db->get();
		return $query->row();
	}
	
	function _getListSecretPictureOf($user_id){
		$sql = "select PictureID, `Order`,
					IF (FileName != '' || FileName != null, CONCAT('".$this->config->item('s3_photo_url')."',FileName), '') as FileName,
					IF (Photo100 != '' || Photo100 != NULL, CONCAT('".$this->config->item('s3_photo_url')."',Photo100), '') as Photo100,
					IF (Photo200 != '' || Photo200 != NULL, CONCAT('".$this->config->item('s3_photo_url')."',Photo200), '') as Photo200,
					IF (Photo300 != '' || Photo300 != NULL, CONCAT('".$this->config->item('s3_photo_url')."',Photo300), '') as Photo300
				from secretpicture
				where UserID = $user_id
		";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
    function getListSecretPictureOf($user_id){
        if ($this->cached){
            $cached = $this->cached->get('secret_picture_of_user'.$user_id);
            if ($cached){
                return $cached;
            }else{
                $secret_picture = self::_getListSecretPictureOf($user_id);
                if ($secret_picture){
                    $this->cached->set('secret_picture_of_user'.$user_id, $secret_picture);
                }
                return $secret_picture;
            }
        }else{
            return self::_getListSecretPictureOf($user_id);
        }
    }
    
	function getListSecretPictureReveal($user_id, $orders){
		$this->db->select('PictureID, FileName, Photo100, Photo200, Photo300, Order');
		$this->db->from('secretpicture');
		$this->db->where('UserID', $user_id);
		$this->db->where_in('Order', $orders);
		$this->db->order_by('Order', 'ASC');
		$query = $this->db->get();
		$result = $query->result();
		if ($result){
			foreach ($result as $re){
				$re->FileName = $this->config->item('s3_photo_url').$re->FileName;
				if ($re->Photo100){
					$re->Photo100 = $this->config->item('s3_photo_url').$re->Photo100;
				}
				if ($re->Photo200){
					$re->Photo200 = $this->config->item('s3_photo_url').$re->Photo200;
				}
				if ($re->Photo300){
					$re->Photo300 = $this->config->item('s3_photo_url').$re->Photo300;
				}
			}
			return $result;
		}else{
			return array();
		}
	}
	
	function getListSecretPictureRevealByListId($user_id, $ids){
		$this->db->select('PictureID, FileName, Photo100, Photo200, Photo300');
		$this->db->from('secretpicture');
		$this->db->where('UserID', $user_id);
		$this->db->where_in('PictureID', $ids);
		$query = $this->db->get();
		$result = $query->result();
		if ($result){
			foreach ($result as $re){
				$re->FileName = $this->config->item('s3_photo_url').$re->FileName;
				if ($re->Photo100){
					$re->Photo100 = $this->config->item('s3_photo_url').$re->Photo100;
				}
				if ($re->Photo200){
					$re->Photo200 = $this->config->item('s3_photo_url').$re->Photo200;
				}
				if ($re->Photo300){
					$re->Photo300 = $this->config->item('s3_photo_url').$re->Photo300;
				}
			}
			return $result;
		}else{
			return array();
		}
	}
}
?>