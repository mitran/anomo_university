<?php
class Sticker_model extends CI_Model{
    public $stickerUrl = '';
    function __construct() {
        parent::__construct();
        $this->stickerUrl = $this->config->item('s3_sticker_url');
    }
    function getListCategory(){
        $sql = "select CateID, Name , IF (Photo != '', concat('$this->stickerUrl', Photo), '') as Photo, Version
                from sticker_category";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function getListStickerByCategory($cate_id = 0){
        if ($cate_id > 0){
            $sql = "select ID , CateID, IF (Photo != '', concat('$this->stickerUrl', Photo), '') as Photo
                    from sticker
                    where CateID = '$cate_id'";
        }else{ // get default sticker
            $sql = "select s.ID , s.CateID, IF (s.Photo != '', concat('$this->stickerUrl', s.Photo), '') as Photo, sc.Version, sc.Name
                    from sticker s
                    left join sticker_category sc ON s.CateID = sc.CateID
                    where sc.Default = 1";
        }
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function getListStickerByCond($cate_id, &$total, $offset, $limit){
        $sqlTotal = "select count(ID) as Total from sticker where  CateID = $cate_id";
        $totalResult = $this->db->query($sqlTotal);
        $row = $totalResult->row();
        $total = ($row)?$row->Total:0;
        if ($total > 0){
            $sql = "select ID, CateID, IF (Photo != '', concat('$this->stickerUrl', Photo), '') as Photo 
                    from sticker where  CateID = $cate_id";
            $sql .= " order by CreatedDate DESC";
            $sql .= " limit  $offset, $limit";
            $query1 = $this->db->query($sql);
            return $query1->result();
        }
        return null;
    }
    
    function getStickerCategoryInfo($cate_id){
        $sql = "select *
                from sticker_category
                where CateID = '$cate_id'";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    function getStickerInfo($id){
        $sql = "select *
                from sticker
                where ID = '$id'";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    function addSticker($data){
        $this->db->insert('sticker', $data);
        $id = $this->db->insert_id();
        if ($id){
            if (isset($data['CateID']) && $data['CateID'] > 0){
                $this->db->update('sticker_category', array('HaveUpdate' => 1), array('CateID' => $data['CateID']));
            }
        }
        return $id;
    }
    
    function addStickerCategory($data){
        $this->db->insert('sticker_category', $data);
        return $this->db->insert_id();
    }
    
    function updateSticker($data, $cond){
        $return = $this->db->update('sticker', $data, $cond);
        if ($return){
            if (isset($data['CateID']) && $data['CateID'] > 0){
                $this->db->update('sticker_category', array('HaveUpdate' => 1), array('CateID' => $data['CateID']));
            }
        }
        return $return;
    }
    
    function updateStickerCategory($data, $cond){
        return $this->db->update('sticker_category', $data, $cond);
    }
    
    function check_version($data){
        $cond = $aCateID = $noNeedUpdateCateID = array();
        $json = json_decode($data);
        if (isset($json->Sticker)){
            foreach($json->Sticker as $obj){
                if (isset($obj->CateID) && isset($obj->Version)){
                    $aCateID[] = $obj->CateID;
                    $cond[] = "(CateID = '$obj->CateID' AND Version = '$obj->Version')"; 
                }
            }
        }
        if (sizeof($cond) > 0){
            $sql = "select CateID from sticker_category where ".  implode(' OR ', $cond);
            $query = $this->db->query($sql);
            $result = $query->result();
            if ($result){
                foreach($result as $row){
                    $noNeedUpdateCateID[] = $row->CateID;
                }
            }
            $needUpdateCateID = array_diff($aCateID, $noNeedUpdateCateID);
            if (sizeof($needUpdateCateID) > 0){
                $sql = "select s.ID , s.CateID, IF (s.Photo != '', concat('$this->stickerUrl', s.Photo), '') as Photo
                            , sc.Name, sc.Version
                        from sticker s
                        left join sticker_category sc ON s.CateID = sc.CateID
                        where sc. CateID IN (". implode(',', $needUpdateCateID) .")";
                $query = $this->db->query($sql);
                return $query->result();
            }
        }
        return array();
    }
    
    function listCategory(){
        $sql = "select CateID, Name , IF (Photo != '', concat('$this->stickerUrl', Photo), '') as Photo, Version, HaveUpdate
                from sticker_category";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function increaseVersion($cate_id){
        $sql = "update sticker_category SET `Version` = `Version` + 1, HaveUpdate = 0 where CateID = $cate_id";
        $this->db->query($sql);
    }
}
?>
