<?php

class Flag_model extends CI_Model {

    function add($data) {
        $this->db->insert('flag', $data);
        $ret = $this->db->insert_id();
        $sql = "update user set FlagsCount = FlagsCount + 1 where UserID = " . $data['ToUserID'];
        $this->db->query($sql);
        return $ret;
    }

    function update($data, $cond) {
        return $this->db->update('flag', $data, $cond);
    }

    function isAddFlag($from_user_id, $to_user_id) {
        $this->db->select('ID');
        $this->db->from('flag');
        $this->db->where('FromUserID', $from_user_id);
        $this->db->where('ToUserID', $to_user_id);
        $query = $this->db->get();
        return $query->row();
    }

    function getListByCondition($cond, &$total, $offset = 0, $limit = 0) {
        $where = '';
        if (isset($cond['flag'])) {
            $where .= " where ToUserID=" . $cond['flag'];
        }
        $tSql = "select count(ID) as Total from flag $where";
        $tQuery = $this->db->query($tSql);
        $row = $tQuery->row();
        $total = ($row)?$row->Total:0;
        
        $sql = "select f.*, u.UserName as FromUserName, u1.UserName as ToUserName, f.FromUserID, f.ToUserID
				from (
                                    select * from flag $where 
                                    order by CreatedDate DESC
                                    limit $limit, $offset
                                    ) as f
				left join user u ON (u.UserID = f.FromUserID)
				left join user u1 ON (u1.UserID = f.ToUserID)
		";
        $query1 = $this->db->query($sql);
        return $query1->result();
    }

}

?>