<?php

class Announcement_model extends CI_Model{
    public $cached = false;

    function __construct(){
        parent::__construct();
        $CI =& get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
        
    }
    function add($data) {
        $this->db->insert('announcement', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('announcement', $data, $cond);
    }
    
    function delete($id){
        $this->db->delete('announcement', array('ID' => $id));
        // should delete announcement close
        $this->db->delete('announcementclosed', array('AnnouncementID' => $id));
    }
    
    function getListByCondition(&$total, $offset, $limit) {
        $sqlTotal = "select count(ID) as Total from announcement where  DATE_FORMAT(ExpireDate, '%Y-%m-%d %H:%i:%s') >= '".gmdate('Y-m-d H:i:s')."'";
        $totalResult = $this->db->query($sqlTotal);
        $row = $totalResult->row();
        $total = ($row)?$row->Total:0;
        if ($total > 0){
            $sql = "select * from announcement where DATE_FORMAT(ExpireDate, '%Y-%m-%d %H:%i:%s') >= '".gmdate('Y-m-d H:i:s')."'";
            $sql .= " order by CreatedDate DESC";
            $sql .= " limit  $offset, $limit";
            $query1 = $this->db->query($sql);
            return $query1->result();
        }
        return null;
    }
    
    function getInfo($id){
        $sql = "select * from announcement where ID='$id'";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    function getListAnnouncementClosed(&$total, $offset, $limit) {
        $sqlTotal = "select count(ID) as Total from announcementclosed ";
        $totalResult = $this->db->query($sqlTotal);
        $row = $totalResult->row();
        $total = ($row)?$row->Total:0;
        
        $sql = "select ac.*, u.UserName, a.Title, a.Content
            from (select * from announcementclosed order by CreatedDate DESC limit $offset, $limit) as ac
            left join user u ON u.UserID=ac.UserID
            left join announcement a on a.ID=ac.AnnouncementID";
        $query1 = $this->db->query($sql);
        return $query1->result();
    }
    
    function addAnnouncementClose($data){
        $this->db->insert('announcementclosed', $data);
        return $this->db->insert_id();
    }
    
    function getAnnouncementCloseInfo($user_id, $announcement_id){
        $sql = "select * from announcementclosed where UserID = $user_id AND AnnouncementID = '$announcement_id'";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    function showAnnouncement($user_id, $signup_date, $activity_type){
 
        $select_announcement = " select
            ID as ActivityID
            , if(RefID is null,ID,RefID) as RefID
            , RefType
            , Content as Message
            , Title
            , 29 as Type
            , 29 as ActionType
            , CreatedDate
            ,0 as IsBlock
            ,0 as IsBlockTarget";
        
        // ANOMO-8493 7 Day Challenge Announcement card
        $end = strtotime(gmdate('Y-m-d H:i:s'));
        $start = strtotime($signup_date);
        $number_register_day = floor(($end - $start) / 86400);
        if ($number_register_day < 0){
            $number_register_day = 0;
        }
        $row = false;
        if ($number_register_day < 5){
            $day = $number_register_day + 1;
            $sql = " $select_announcement 
                    from announcement
                    where Type = 3 AND Day = $day
                        AND ID NOT IN (select AnnouncementID from announcementclosed where UserID = $user_id) 
                    limit 1
                    ";
            $query = $this->db->query($sql);
            $row = $query->row();
        }

        if (!$row){
            $sql = " $select_announcement
                from announcement 
                where DATE_FORMAT(ExpireDate, '%Y-%m-%d %H:%i:%s') >='".gmdate('Y-m-d H:i:s')."'
                    AND ((Type = 1 AND CreatedDate < '$signup_date') OR (Type = 2 AND CreatedDate > '$signup_date'))
                    AND ID NOT IN (select AnnouncementID from announcementclosed where UserID = $user_id)
                limit 1
                    ";
            $query = $this->db->query($sql);
           $row =  $query->row();
        }
        
        $cards = array();
        if ($row){
            $cards['announcement_card'] = $row;
        }
        return $cards;
    }
    
    function addMulti($data){
        $sql = "INSERT INTO announcement (`Title`,`Content`,`Type`,`ExpireDate`,`CreatedDate`,`RefType`,`RefID`)
		VALUE ".implode(',', $data);
        $this->db->query($sql);
    }
}
?>

