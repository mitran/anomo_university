<?php
class Deleted_Content_model extends CI_Model{
    public $photoUrl = '';
    function __construct() {
        parent::__construct();
        $this->photoUrl = $this->config->item('s3_photo_url');
    }
    function getListDeletedContent($user_id = 0, $last_activity_id = 0){
        $limit = 20;
        
        $where = '';
        if ($user_id > 0){
            $where .= " AND FromUserID = $user_id";
        }
        if ($last_activity_id > 0){
            $where .= " AND ActivityID < $last_activity_id";
        }
        $sql = "select a.*, u.UserName, u.UserID
                , IF (Image != '', CONCAT('$this->photoUrl',Image), '') AS Image
                from (select * from activity where IsInvalid = 1 ". $where . " order by ActivityID DESC limit ". $limit .") as a
                left join user u on u.UserID = a.FromUserID";
        $query = $this->db->query($sql);
        $result = $query->result();
        if (!$result){
            $sql = "select a.*, u.UserName, u.UserID
                 , IF (Image != '', CONCAT('$this->photoUrl',Image), '') AS Image
                from (select * from activity_archive where IsInvalid = 1 ". $where . " order by ActivityID DESC limit ". $limit .") as a
                left join user u on u.UserID = a.FromUserID";
            $query = $this->db->query($sql);
            $result = $query->result();
        }
        return $result;
    }
    
    function getTotalDeletedContent($user_id = 0){
        $where = '';
        if ($user_id > 0){
            $where .= " AND FromUserID = $user_id";
        }
        
        $sql = "select (select count(ActivityID) as total1 from activity where IsInvalid = 1 $where) + (select count(ActivityID) as total2 from activity_archive where IsInvalid = 1 $where) as total";
        $query = $this->db->query($sql);
        $row = $query->row();
        return $row?$row->total:0;
    }
    
    function getContentDetail($content_id, $type){
        $table_name = array(
            ACTIVITY_POST_STATUS => 'userprofilestatus', 
            ACTIVITY_POST_PICTURE => 'picturepostactivity',
            ACTIVITY_PROMOS => 'promos',
            ACTIVITY_POST_VIDEO => 'video'
        );
        if (!in_array($type, array_keys($table_name))) {
            return false;
        }
        $sql = "select c.*
               from $table_name[$type] c	
	       where c.ID = $content_id ";
        $query = $this->db->query($sql);
        $retValue = $query->row();
        $query->free_result();
        return $retValue;
    }
}
?>
