<?php
class Hashtag_model extends CI_Model {
    public $cached = false;

    function __construct(){
        parent::__construct();
        $CI =& get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
        
    }
    
    function clean($string) {
        $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return strtolower($string);
    }
    
    function add($text, $activity_id, $topic_cate_id = null, $topic_id = null){
        if (trim($text) != ''){
            $matches = array();
//            preg_match_all('/#(\w+)/', $text, $matches);
            preg_match_all('/#([a-zA-Z0-9]+)/', $text, $matches);
            $aHashTag = $matches[1];
            if (sizeof($aHashTag) > 0){
                $aHashTag = array_unique($aHashTag);
                $value = array();
                foreach ($aHashTag as $hashTag){
                    $hashTag = strtolower($hashTag);
                    $value[] = "('$hashTag', $activity_id, '$topic_cate_id', '$topic_id')";
                }
                $sqlInsert = "insert into hashtag (`HashTag`, `ActivityID`, `CateID`, `TopicID`) value ".implode(',', $value);
                $this->db->query($sqlInsert);
            }
        }
    }
    
    function topTrendingHashTag($cate_id = 0, $topic_id = 0){ 
        $key = $cate_id.'_'.$topic_id;
        if ($this->cached){
            $trending_cached = $this->cached->get('trending_hashtag'.$key);
            if ($trending_cached){
                if ((microtime(true) - $trending_cached['cached_time']) < intval(Config_Model::getConfig('CACHE_TRENDING_CARD_TIME_OUT'))) {
                    $result = $trending_cached['result'];
                }else{     
                    $result = self::getTrendingHashTag($cate_id, $topic_id);
                    if ($result){
                        // set new cache
                        $this->cached->set('trending_hashtag'.$key, array('result' => $result, 'cached_time' => microtime(true)));
                    }
                }
            }else{ 
                $result = self::getTrendingHashTag($cate_id, $topic_id);
                if ($result){
                    // set new cache
                    $this->cached->set('trending_hashtag'.$key, array('result' => $result, 'cached_time' => microtime(true)));
                }
            }
        }else{
            $result = self::getTrendingHashTag($cate_id, $topic_id);
        }
        return $result;
    }
    
    function getTrendingHashTag($cate_id = 0, $topic_id = 0){
        $where = array();
        if ($cate_id > 0){
            $where[] = " CateID = $cate_id";
        }
        if ($topic_id > 0){
            $where[] = " TopicID = $topic_id";
        }
        $sWhere = implode( ' AND ', $where);
        $sWhere = !empty($sWhere) ? $sWhere . ' AND ': $sWhere;
        $limit = 10;
        $sql = "select HashTag
                from hashtag
                where $sWhere ActivityID in (select ActivityID from activity where IsInvalid != 1 AND CreatedDate >= (now() - INTERVAL ". intval(Config_Model::getConfig('TRENDING_TAG_INTERVAL')) ." HOUR))
                group by HashTag
                order by count(HashTag) desc limit $limit";

            // "select HashTag from hashtag group by HashTag ORDER BY count(HashTag) DESC limit $limit";
        $query = $this->db->query($sql);
        return $query->result();
    }
}
?>
