<?php

class Cron_Push_Notification_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function add($arrData) {
        $this->db->insert('cronpushnotification', $arrData);
        return $this->db->insert_id();
    }

    function update($arrData, $cond) {
        $this->db->update('cronpushnotification', $arrData, $cond);
    }

    function delete($iID) {
        $this->db->delete('cronpushnotification', array('ID' => $iID));
    }

    /*
     * Get Comment for push notify cron comment
     */

    function getCommentForPushNotify($iLimit = 100) {    
        $sql = "select c.*, u.AllowCommentActivityNotice from (select * from cronpushnotification where Status = 0 AND FailCount < 3 limit $iLimit) as c
                left join user u ON u.UserID = c.UserID ";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    
    function insertMulti($data) {
        
        if (sizeof($data) > 0) {
            $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
            foreach($data as $re){
                $IsAnonymousComment = isset($re['IsAnonymousComment'])?$re['IsAnonymousComment']:0;
                $value[] = "('".$re['UserID']."', '".$re['FromUserID']."', '".mysqli_real_escape_string($con, $re['Message'])."','".$re['ContentID']."','".$re['Type']."','".$re['ActionType']."','".$re['Status']."','".$re['Total']."','".$re['HistoryID']."','".$re['CreatedDate']."',".$IsAnonymousComment.")";
            }
            if (isset($value)){
                $sql = "INSERT INTO cronpushnotification (`UserID`, `FromUserID`,`Message`,`ContentID`,`Type`,`ActionType`,`Status`,`Total`,`HistoryID`, `CreatedDate`,`IsAnonymousComment` ) 
                        VALUES " . implode(',', $value);
                $this->db->query($sql);
            }
        }
    }
    
    function updateCron($data, $result = 'success'){
        if ($result == 'success'){
            $sql = "update cronpushnotification set `Status` = 1 where ID IN (".implode(',', $data).")";
        }else{
            $sql = "update cronpushnotification set `FailCount` = `FailCount` + 1 where ID IN (".implode(',', $data).")";
        }
        $this->db->query($sql);
    }

}

?>
