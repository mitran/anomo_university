<?php

class Campaign_CAU_Daily_model extends CI_Model{
    public $cached = false;

    function __construct(){
        parent::__construct();
        $CI =& get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
        
    }
    function add($data) {
        $this->db->insert('campaigncaudaily', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('campaigncaudaily', $data, $cond);
    }
        
    function getCurrentDateInfo($UserID) {
        $sql = "
            SELECT * FROM campaigncaudaily
            WHERE UserID = $UserID           
            AND DATE_FORMAT(CreatedDate, '%Y-%m-%d') = '".gmdate('Y-m-d')."'             
            LIMIT 1
        ";
        $result = $this->db->query($sql);
        return $result->row();        
    }    
}
?>

