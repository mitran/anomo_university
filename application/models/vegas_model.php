<?php

class Vegas_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function increaseView(){
        $sql = "update vegas_count set `Count` = `Count` + 1";
        return $this->db->query($sql);
    }
    
    function totalVegasView(){
        $sql = "select Count from vegas_count limit 1";
        $query = $this->db->query($sql);
        $row = $query->row();
        return $row?$row->Count:0;
    }

}

?>
