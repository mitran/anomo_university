<?php
class Topic_Category_model extends CI_Model {
    public $photoStreamUrl = '';
    function __construct() {
        parent::__construct();
        $this->photoStreamUrl = $this->config->item('s3_stream_url');
    }
    function getListCategory($is_admin = 0){
        $sql = "select CateID, CateName, OrderID, `Desc`
            , IF ($is_admin = 1, 1, AllowCreateTopic) as AllowCreateTopic
            , IF (Photo != '', concat('$this->photoStreamUrl', Photo), '') as Photo
            , (select count(TopicID) from topic p where p.CateID = tc.CateID and IsDelete != 1) as TotalTopic
            from topiccategory tc
            where Status = 1
            order by OrderID ASC";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function getTotalTopic(){
        $sql = "select count(TopicID) as Total from topic where  IsDelete != 1";
        $query = $this->db->query($sql);
        $row = $query->row();
        return $row ? $row->Total: 0;
    }
    
    function getCateInfo($cate_id){
        $this->db->select ('*');
        $this->db->from('topiccategory');
        $this->db->where('CateID', $cate_id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function showTopicOnCateScreen() {
        $sql = "select TopicID, TopicName, TotalPost, UserID, `Desc`
                , IF (Photo != '', concat('$this->photoStreamUrl', Photo), '') as Photo
                , IF (Photo100 != '', concat('$this->photoStreamUrl', Photo100), '') as Photo100
                from topic where CateID = -1 AND IsDelete = 0
                ";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function addTopicCate($data){
        $this->db->insert('topiccategory', $data);
        return $this->db->insert_id();
    }
    
    function updateTopicCate($data, $cond){
        return $this->db->update('topiccategory', $data, $cond);
    }
}
?>
