<?php

class Mark_Post_Comment_model extends CI_Model {

    function add($data) {
        $this->db->insert('markpostcomment', $data);
        return $this->db->insert_id();
    }

    function checkMark($user_id, $target_id, $type) {
        $this->db->select('MarkID');
        $this->db->from('markpostcomment');
        $this->db->where('UserID', $user_id);
        if ($type == 'post') {
            $this->db->where('PostID', $target_id);
        } else {
            $this->db->where('CommentID', $target_id);
        }
        $query = $this->db->get();
        return $query->row();
    }

    function getNumberMarkOf($target_id, $type) {
        $this->db->select('Count(MarkID) as NUM');
        $this->db->from('markpostcomment');
        if ($type == 'post') {
            $this->db->where('PostID', $target_id);
        } else {
            $this->db->where('CommentID', $target_id);
        }
        $query = $this->db->get();
        $row = $query->row();
        return (int) $row->NUM;
    }

    function markTarget($target_id, $type) {
        if ($type == 'post') {
            Place_Post_model::update(array('IsMark' => 1), array('ID' => $target_id));
        } else {
            Place_Post_model::update(array('IsMark' => 1), array('ID' => $target_id));
        }
    }

}

?>