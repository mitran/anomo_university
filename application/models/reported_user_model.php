<?php

class reported_user_model extends CI_Model {

    function add($data) {
        $this->db->insert('flag', $data);
        $ret = $this->db->insert_id();
        $sql = "update user set FlagsCount = FlagsCount + 1 where UserID = " . $data['ToUserID'];
        $this->db->query($sql);
        return $ret;
    }

    function update($data, $cond) {
        return $this->db->update('flag', $data, $cond);
    }

    function isAddFlag($from_user_id, $to_user_id) {
        $this->db->select('ID');
        $this->db->from('flag');
        $this->db->where('FromUserID', $from_user_id);
        $this->db->where('ToUserID', $to_user_id);
        $query = $this->db->get();
        return $query->row();
    }

    function getListByCondition($cond, &$total, $offset = 0, $limit = 0) {
        if (isset($cond['flag'])) {
            $where = " where ToUserID=" . $cond['flag'];
        }
        if (isset($cond['ToUserID'])) {
//        	$where = " where FromUserID=" . $cond['FromUserID'];
        	$con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
        	$cond['ToUserID'] = mysqli_real_escape_string($con, $cond['ToUserID']);
        	$q = "(`ToUserID` = '{$cond['ToUserID']}')";
        	$where = "where $q";
        }
        
        $tSql = "select count(ID) as Total from flag $where";
        $tQuery = $this->db->query($tSql);
        $row = $tQuery->row();
        $total = ($row)?$row->Total:0;

        $groupby = "group by ToUserID";
        if (isset($cond['ToUserID'])) {
        	$groupby = "";
        }        
        $sql = "select f.*, u.UserName as ReportingUser, u1.UserName as ReportedUser, f.FromUserID, f.ToUserID,u1.IsBanned
				from (
                                    select * from flag $where 
                                    $groupby
                                    order by CreatedDate 
                                    limit $limit, $offset
                                    ) as f
				left join user u ON (u.UserID = f.FromUserID)
				left join user u1 ON (u1.UserID = f.ToUserID)
		";
        $query1 = $this->db->query($sql);
        return $query1->result();
    }

   
    function getsql(&$total,$offset = 0, $limit = 0) { 
    	
    	$tSql = "select count(UserID) as Total from user where UserID in (
    	select distinct ToUserID from flag
    	where ID IN (select ID from flag where IsForgiven IS NULL))
    	and IsBanned = '0' ";
    			
    	$tQuery = $this->db->query($tSql);
    	$row = $tQuery->row();
    	$total = ($row)?$row->Total:0;
    
    	$groupby = "group by ToUserID";
    
    	$sql = "select A.* from
    	(
    	select f1.*, u.UserName as ReportingUser, u1.UserName as
    	ReportedUser,u1.IsBanned,group_concat(ifnull(u.UserName,'NULL') separator ' : ') as ReportingUsers,count(distinct ifnull(u.UserName,'NULL')) as c_users,
    	group_concat(f1.Content separator ' : ') as Contents,
    	group_concat(ifnull(f1.FromUserID,'NULL') separator ' : ') as ReportingUserIds
    	from
    	(select ID,FromUserID,ToUserID,Content,CreatedDate,IsForgiven
    	from flag where ID IN (select ID from flag where IsForgiven IS NULL)
    	AND FromUserID NOT IN (SELECT UserID from ExcludedReportingUsers)
    	) as f1
    	left join user u ON (u.UserID = f1.FromUserID)
    	left join user u1 ON (u1.UserID=f1.ToUserID)
    	where u1.IsBanned = '0' 
    	group by ToUserID
    	order by CreatedDate DESC
    	) A where A.IsBanned = '0'
    	limit $limit, $offset
    	";
    	$query1 = $this->db->query($sql);
    	
    	return $query1->result();
    }

    function getListChatUsers($user_id){
    
    	$sql1 = "select temp.ReceiveUserID,
    	u1.UserName as ReceiveUserName
    	from (select
    	distinct SUBSTRING_INDEX(toJID, '@', 1) as ReceiveUserID
    	from ofmessagearchive
    	where
    	(fromJID in ('" . $user_id . "@" . XMPP_HOST . "')
    			or toJID in ('" . $user_id . "@" . XMPP_HOST . "'))
    			AND body != ''
    					AND body NOT IN ('filter_request_game',
    							'filter_message_status_typing',
    							'filter_message_status_endtyping',
    							'filter_message_status_delivered')
    
    	) as temp
    	inner join user u1 ON u1.UserID = temp.ReceiveUserID
    	";
    	$query1 = $this->db->query($sql1);
    	return $query1->result();
    }
    
    function getListMsgXMPP($user_id,$with_user_id){
    	$sql1 = "select temp.ChatID,temp.Content,temp.Photo,temp.SendUserID,temp.ReceiveUserID,temp.CreatedDate,
    	u1.UserName as SendUserName,
    	u2.UserName as ReceiveUserName
    	from (select conversationID as ChatID,
    	IF (SUBSTRING(TRIM(body), 1,16)='filter_image_tag', '',body) as Content,
    	IF (SUBSTRING(TRIM(body), 1,16)='filter_image_tag',
    	SUBSTRING(body, 17, INSTR(body, 'filter_image_untag') - 17),'') as Photo,
    	SUBSTRING_INDEX(fromJID, '@', 1) as SendUserID,
    	SUBSTRING_INDEX(toJID, '@', 1) as ReceiveUserID,
    	FROM_UNIXTIME(sentDate/1000) as CreatedDate
    	from ofmessagearchive
    	where
		(fromJID in ('" . $user_id . "@" . XMPP_HOST . "','" . $with_user_id . "@" . XMPP_HOST . "')
		and toJID in ('" . $user_id . "@" . XMPP_HOST . "','" . $with_user_id . "@" . XMPP_HOST . "'))
						AND body != ''
    					AND body NOT IN ('filter_request_game',
    							'filter_message_status_typing',
    							'filter_message_status_endtyping',
    							'filter_message_status_delivered')
    							order by CreatedDate DESC
    	) as temp
    	inner join user u1 ON u1.UserID = temp.SendUserID
    	inner join user u2 ON u2.UserID = temp.ReceiveUserID
		order by CreatedDate
    	";
    	$query1 = $this->db->query($sql1);
    	return $query1->result();
    }

    function get_userid($user_name){
    	$sql = "select UserID from user where UserName LIKE '%$user_name%'";
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
    function update_reported_user($data, $cond){
    	return $this->db->update('flag', $data, $cond);
    }

    function get_username($user_id){
    	$sql = "select UserName from user where UserID = $user_id ";
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
}

?>
