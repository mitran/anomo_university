<?php

class Post_Comment_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function add($data) {
        $this->db->insert('postcomment', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('postcomment', $data, $cond);
    }

    function findById($comment_id) {
        $this->db->select('pc.ID, pc.Content, pc.CreatedDate, pc.UserID, pc.PlacePostID');
        $this->db->from('postcomment pc');
        $this->db->where('pc.ID', $comment_id);
        $query = $this->db->get();
        return $query->row();
    }

    function getInfo($comment_id) {
        $this->db->select('pc.ID, pc.Content, pc.CreatedDate, pc.UserID, u.UserName, u.Photo as Avatar, u.Gender');
        $this->db->from('postcomment pc');
        $this->db->join('user u', 'pc.UserID=u.UserID');
        $this->db->where('pc.ID', $comment_id);
        $query = $this->db->get();
        return $query->row();
    }

    function getListCommentOf($post_id) {
        $this->db->select('pc.ID, pc.Content, pc.CreatedDate, pc.UserID, u.UserName, u.Photo as Avatar, u.Gender');
        $this->db->from('postcomment pc');
        $this->db->join('user u', 'pc.UserID=u.UserID');
        $this->db->where('pc.PlacePostID', $post_id);
        $this->db->where('pc.IsMark', 0);
        $this->db->where('u.IsBanned !=', 1);
        $query = $this->db->get();
        return $query->result();
    }

    function countCommentOf($post_id) {
        $this->db->from('postcomment pc');
        $this->db->where('pc.PlacePostID', $post_id);
        $this->db->where('pc.IsMark', 0);
        return $this->db->count_all_results();
    }

    function delete($id) {
        $this->db->delete('postcomment', array('ID' => $id));
    }

}

?>