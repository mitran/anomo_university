<?php

class Tag_model extends CI_Model {
    
    public $cached = false;
    function __construct() {
        parent::__construct();
        $CI = & get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
    }

    function getTagId($name) {
        $name = strtolower($name);
        if ($this->cached) {
            $cache = $this->cached->get('tag_name' . $name);
            if ($cache) {
                return $cache;
            } else {
                $this->db->select('TagID');
                $this->db->from('tag');
                $this->db->where('Name', trim($name));
                $query = $this->db->get();
                $row = $query->row();
                $id =  ($row) ? $row->TagID : 0;
                if ($id > 0){
                    $this->cached->set('tag_name' . $name, $id);
                }
                return $id;
            }
        }else{
            $this->db->select('TagID');
            $this->db->from('tag');
            $this->db->where('Name', trim($name));
            $query = $this->db->get();
            $row = $query->row();
            return ($row) ? $row->TagID : 0;
        }
        
    }

    function add($data) {
        $this->db->insert('tag', $data);
        if ($this->cached){
            $this->cached->delete('list_interests');
        }
        return $this->db->insert_id();
    }
    
    function update($data, $cond) {
        $this->db->update('tag', $data, $cond);
        if ($this->cached){
            $this->cached->delete('list_interests');
        }
    }

    function getTagInfo($tag_id) {
        $this->db->select('*');
        $this->db->from('tag');
        $this->db->where('TagID', $tag_id);
        $query = $this->db->get();
        return $query->row();
    }

    function returnListTagId($data) {
        foreach ($data as $value) {
            $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
            $tag[] = "'" . mysqli_real_escape_string($con, $value) . "'";
        }
        $sName = implode(',', $tag);
        $sql = "select TagID, Name from tag where Name IN ($sName)";
        $query = $this->db->query($sql);
        $result = $query->result();
        $data = array();
        if ($result) {
            foreach ($result as $re) {
                $data[$re->TagID] = $re->Name;
            }
        }
        return $data;
    }

    function insertMulti($data) {
        foreach ($data as $value) {
            $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
            $tag[] = "('" . mysqli_real_escape_string($con, $value) . "')";
        }
        if (isset($tag)) {
            $sql = "INSERT INTO tag (`Name`) VALUES " . implode(',', $tag);
            $this->db->query($sql);
            $id = $this->db->insert_id();
            return $id;
        }
    }
    
    function getListInterests(){
        if ($this->cached){
            $cached = $this->cached->get('list_interests');
            if ($cached){
                return $cached;
            }else{
                $interests = self::_getInterests();
                if ($interests){
                    $this->cached->set('list_interests', $interests);
                }
                return $interests;
            }
        }else{
            return self::_getInterests();
        }
    }
    
    function _getInterests(){
        $sql = "select t.TagID, t.Name, tc.Name as Category, tc.ID 
                from (select * from tag where TagCategoryID > 0 ) as t 
                left join tagcategory tc ON t.TagCategoryID= tc.ID
                order by Name ASC
                ";
        $query = $this->db->query($sql);
        $result = $query->result();
        $data = array();
        if ($result){
            $cats = array();
            foreach($result as $re){
                $cats[$re->ID] = $re->Category; 
            }
            ksort($cats);
            foreach($cats as $key => $value){
               $tagList = array();
               foreach($result as $row){
                   if ($key == $row->ID){
                       $tagList[] = array(
                         'TagID'    => $row->TagID,
                         'Name'     => $row->Name
                       );
                   }
               } 
               $data[] = array(
                   'Category'   => $value,
                   'TagCategoryID'  => (string)$key,
                   'TagList'    => $tagList
               );
            }
        }
        return $data;
    }
    
    /* * get list interests with paging
     * 
     * @param type $total
     * @param type $from
     * @param type $to
     * @return type
     */

    function getListWithPaging($cond = array(), &$total = 0, $from = 0, $to = 0) {
        $total = 0;
        $where = "";
        if (isset($cond['category'])){
            $where .= ' AND TagCategoryID ='.$cond['category'];
        }
        $sqlTotal = "select count(TagID) as Total from tag where TagCategoryID > 0 ".$where ;
        $queryTotal = $this->db->query($sqlTotal);
        $rowTotal = $queryTotal->row();
        $total = $rowTotal ? $rowTotal->Total : 0;
        if ($total > 0){
            $sql = "select t.TagID, t.Name, tc.Name as Category, t.TagCategoryID 
                    from (select * from tag  where TagCategoryID > 0 $where limit $to, $from) as t
                    left join tagcategory tc ON t.TagCategoryID= tc.ID
                     ";
            $query = $this->db->query($sql);
            return $query->result();
        }
        return false;
    }
    
    function getTagCategory() {
        $this->db->select('*');
        $this->db->from('tagcategory');
        $query = $this->db->get();
        $result = $query->result();
        $data = array();
        foreach ($result as $re) {
            $data[$re->ID] = $re->Name;
        }
        return $data;
    }

    function getInfo($id){
        $this->db->select('*');
        $this->db->from('tag');
        $this->db->where('TagID', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function checkExistedName($name, $id) {
        $this->db->select('TagID');
        $this->db->from('tag');
        $this->db->where('Name', $name);
        if ($id > 0) {
            $this->db->where('TagID !=', $id);
        }
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? false : true;
    }
    
    function addCat($data) {
        $this->db->insert('tagcategory', $data);
        if ($this->cached){
            $this->cached->delete('list_interests');
        }
        return $this->db->insert_id();
    }
    
    function updateCat($data, $cond) {
        $this->db->update('tagcategory', $data, $cond);
        if ($this->cached){
            $this->cached->delete('list_interests');
        }
    }

    function getCatInfo($id) {
        $this->db->select('*');
        $this->db->from('tagcategory');
        $this->db->where('ID', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function searchTag($keyword, $user_id){
        $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
        $keyword = mysqli_real_escape_string($con, $keyword);
        $sql = "select t.TagID from (select TagID from tag where LCASE(Name)='$keyword' limit 1) as t
                inner join usertag ut ON t.TagID = ut.TagID AND ut.UserID != $user_id limit 1";
        $query = $this->db->query($sql);
        $row = $query->row();
        return $row?1:0;
    }
    
    function insertUserTag($user_id, $data){
        foreach ($data as $tag_id) {
            $tag[] = "('$user_id', '$tag_id')";
        }
        if (isset($tag)) {
            $sql = "INSERT INTO usertag (`UserID`, `TagID`) VALUES " . implode(',', $tag);
            $this->db->query($sql);
            return $this->db->insert_id();
        }
    }
    
    
    
}

?>