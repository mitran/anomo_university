<?php
class blocked_users_model extends CI_Model{
	function __construct() {
		parent::__construct();
	}
	function getBlockedUsers($time = 'all', $from = '', $to = ''){
		$sql = 'select A.UserName,B.blocks
				from (select UserID,UserName from user group by UserID) as A
				RIGHT JOIN (select TargetUserID,count(*) as blocks from userblock';

				if ($time == 'week'){
					$sql .= ' where Date >= DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(), INTERVAL 7 Day),INTERVAL 7 HOUR), "%Y-%m-%d %H:%i:%s")' ;
				}
				if(($from != '') & ($to != '')){
					$sql .= ' where DATE_FORMAT(DATE_SUB(Date,INTERVAL 7 HOUR), "%Y-%m-%d") >="' . $from .'"' 
					. ' and DATE_FORMAT(DATE_SUB(Date,INTERVAL 7 HOUR), "%Y-%m-%d") <="' . $to .'"';					
				}
		$sql .=	' group by TargetUserID order by blocks desc) as B
				ON A.UserID=B.TargetUserID';				
		$query1 = $this->db->query($sql);
		$d = $query1->result_array();
		return $d;	
	}
}       