<?php

class Intent_model extends CI_Model {
    public $cached = false;

    function __construct()
    {
        parent::__construct();
        $CI =& get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
    }
    
    function add($data) {
        $this->db->insert('intent', $data);
        if ($this->cached) {
            $this->cached->delete('list_intent');
        }
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('intent', $data, $cond);
    }
    
    function getInfo($id){
        $this->db->select('*');
        $this->db->from('intent');
        $this->db->where('IntentID', $id);
        $query = $this->db->get();
        return $query->row();
    }

    /**return full list intent
     * 
     * 
     */
    function listIntent(){
        if ($this->cached){
            $cached = $this->cached->get('list_intent');
            if ($cached){
                return $cached;
            }else{
                $intents = self::_getIntents();
                if ($intents){
                    $this->cached->set('list_intent', $intents);
                }
                return $intents;
            }
        }else{
            return self::_getIntents();
        }
    }
    
    private function _getIntents(){
        $sql = "select IntentID, Name from intent";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function getListIntentOfUser($user_id)
    {
        if ($this->cached) {
            $cache = $this->cached->get('intent' . $user_id);
            if ($cache) {
                return $cache;
            } else {
                $this->db->select('t.IntentID, t.Name');
                $this->db->from('intent t');
                $this->db->join('userintent ut', 'ut.IntentID=t.IntentID');
                $this->db->where('ut.UserID', $user_id);

                $this->db->order_by('t.Name', 'asc');
                $query = $this->db->get();
                $result = $query->result();
                $query->free_result();
                $this->cached->set('intent' . $user_id, $result);
                return $result;
            }
        } else {
            $this->db->select('t.IntentID, t.Name');
            $this->db->from('intent t');
            $this->db->join('userintent ut', 'ut.IntentID=t.IntentID');
            $this->db->where('ut.UserID', $user_id);
            $this->db->order_by('t.Name', 'asc');
            $query = $this->db->get();
            $retVal = $query->result();
            $query->free_result();
            return $retVal;
        }
    }
    
    function addMulti($user_id, $data){
        if ($this->cached){
            $this->cached->delete('intent' . $user_id);
        }
        foreach ($data as $value) {
            $intent[] = "('" . $user_id . "', '".$value."')";
        }
        if (isset($intent)) {
            $deleteSql = "delete from userintent where UserID =".$user_id;
            $this->db->query($deleteSql);
            $sql = "INSERT INTO userintent (`UserID`, `IntentID`) VALUES " . implode(',', $intent);
            $this->db->query($sql);
            return $this->db->insert_id();
        }
    }
    
    /* * get list intetn with paging
     * 
     * @param type $total
     * @param type $from
     * @param type $to
     * @return type
     */

    function getListIntentWithPaging(&$total = 0, $from = 0, $to = 0) {
        $total = 0;
        $sqlTotal = "select count(IntentID) as Total from intent " ;
        $queryTotal = $this->db->query($sqlTotal);
        $rowTotal = $queryTotal->row();
        $total = $rowTotal ? $rowTotal->Total : 0;
        if ($total > 0){
            $sql = "select *
                    , (select count(*) from userintent ui where n.IntentID = ui.IntentID) as Number
                    from intent n order by Number DESC 
                    limit $to, $from ";
            $query = $this->db->query($sql);
            return $query->result();
        }
        return false;
    }
    
    function insertUserIntent($user_id, $data){
        foreach ($data as $value) {
            $intent[] = "('" . $user_id . "', '".$value."')";
        }
        if (isset($intent)) {
            $sql = "INSERT INTO userintent (`UserID`, `IntentID`) VALUES " . implode(',', $intent);
            $this->db->query($sql);
            return $this->db->insert_id();
        }
    }

}

?>