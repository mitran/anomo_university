<?php

class User_Profile_Status_model extends CI_Model {

    public $cached = false;

    function __construct() {
        parent::__construct();
        $this->db_slave = $this->load->database('slave', TRUE);
        $CI = & get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
    }

    function add($data) {
        $this->db->insert('userprofilestatus', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        return $this->db->update('userprofilestatus', $data, $cond);
    }

    function deleteStatus($id, $user_id = 0) {
        try {

            Activity_model::invalidUserCached($user_id);

            $this->db->trans_begin();

            $this->db->delete('userprofilestatus', array('ID' => $id));
            $commentIDs = Activity_model::getCommentIDOf($id, ACTIVITY_POST_STATUS);
            if (count($commentIDs) > 0){
                $this->db->delete('notificationhistory', "RefID IN (".implode(',', $commentIDs).") AND Type=14 AND ContentType=1"); // comment activity
            }
            $likeIDs = Activity_model::getLikeIDOf($id, ACTIVITY_POST_STATUS);
            if (count($likeIDs) > 0){
                $this->db->delete('notificationhistory', "RefID IN (".implode(',', $likeIDs).") AND Type=13 AND ContentType=1"); // like activity
            }

            $this->db->delete('activitycomment', array('ContentID' => $id, 'Type' => ACTIVITY_POST_STATUS));
            $this->db->delete('activitylike', array('ContentID' => $id, 'Type' => ACTIVITY_POST_STATUS));

            // delete hashtag 
            $sql = "delete from hashtag where ActivityID IN (select ActivityID from activity where RefID = $id AND Type = 1)";
            $this->db->query($sql);
            $this->db->delete('activity', array('RefID' => $id, 'Type' => ACTIVITY_POST_STATUS));
            $this->db->delete('activity_archive', array('RefID' => $id, 'Type' => ACTIVITY_POST_STATUS));

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }
    }

    function getListStatus($cond, &$total, $offset = 0, $limit = 0) {
        $where = array();
        $sWhere = '';
        $where[] = " IsDeleted != 1 ";
        if (isset($cond['keyword']) && $cond['keyword'] != ''){
            $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
            $keyword = mysqli_real_escape_string($con, $cond['keyword']);  
            $where[] = " Content like '%".$keyword."%'";
        }
        if (isset($cond['user_id']) && $cond['user_id'] > 0){
            $where[] = " UserID ='".$cond['user_id']."'";
        }
        if (sizeof($where) > 0){
            $sWhere = 'where '.implode('AND', $where);
        }
        
        $sqlTotal = "select count(ID) as Total from userprofilestatus $sWhere";
        $totalResult = $this->db->query($sqlTotal);
        $totalRow = $totalResult->row();
        $total = $totalRow?$totalRow->Total:0;
        if ($total > 0){
            $sql = "select us.*, u.UserName
                                    from (select * from userprofilestatus $sWhere order by CreatedDate DESC limit $limit, $offset ) as  us
                                    left join user u ON (u.UserID = us.UserID)
                    ";
            $query1 = $this->db->query($sql);
            return $query1->result();
        }
        return null;
    }

    function parseData($text = '', $user_id, $is_vegas = 0, $content_type = '', $is_anonymous = 0) {
        $return = array();
        $response = $text;
        if (trim($text) != '') {
            $json = json_decode($text);
            if ($json) {
                // ANOMO-10831 - auto hashtag with #vegas
                // ANOMO-11565 Hotfix - Turn off all #vegas autohashtaging
                /*
                if ($is_vegas == 1){
                    $vegasHashTag = " #".VEGAS_HASHTAG;
                    if (isset($json->message)){
                        $json->message = $json->message . $vegasHashTag;
                    }
                    $response = json_encode($json);
                }
                */
                
                $msg = isset($json->message) ? $json->message : '';
                if ($msg != '') {
                    // don't allow tag if this post is secret
                    if ($is_anonymous == 1){
                        if (isset($json->message_tags)){
                            $json->message_tags = array();
                        }
                        $response = json_encode($json);
                    }else{
                        // detect if have youtube link - ANOMO-11390
                        if ($content_type == ACTIVITY_POST_STATUS){
                            $CI = & get_instance();
                            $aVideo = $CI->commonlib->getVideoIDFromString($msg);
                            if ($aVideo){
                                // remove text link in case post status
                                if (isset($aVideo['VideoLink']) && !empty($aVideo['VideoLink'])){
                                   // $msg = trim(str_replace($aVideo['VideoLink'], '', $msg));
                                    
                                    $pos = strpos($msg, $aVideo['VideoLink']);
                                    if ($pos !== false) {
                                        $msg = substr_replace($msg, '', $pos, strlen($aVideo['VideoLink']));
                                    }
                                    $json->message = $msg;
                                    $response = json_encode($json);
                                }
                                $return['video'] = $aVideo;
                            }
                        }

                        // count #of @ in msg
                        $matches = array();
                        preg_match_all('/@([a-zA-Z0-9]+)/', $msg, $matches);
                        $n = count($matches[1]);
                        if ($n > 0) {
                            $msgTags = isset($json->message_tags) ? $json->message_tags : '';
                            if ($msgTags != '') {

                                if (is_array($msgTags)) {
                                    if (sizeof($msgTags) < $n) {
                                        $aTags = array();
                                        foreach ($msgTags as $tag) {
                                            $aTags[] = $tag->name;
                                        }
                                        $sTags = implode(' ', $aTags);
                                        $matches1 = array();
                                        preg_match_all('/@([a-zA-Z0-9]+)/', $sTags, $matches1);

                                        $unTags = array_diff($matches[1], $matches1[1]);
                                        if (sizeof($unTags) > 0) {
                                            foreach ($unTags as $tag) {
                                                $userInfo = self::getUserByName($tag, $user_id);
                                                if ($userInfo) {
                                                    $msgTags[] = array(
                                                        'id' => $userInfo->UserID,
                                                        'name' => '@' . $userInfo->UserName
                                                    );
                                                }
                                            }
                                        }
                                        $jsonObj = new stdClass();
                                        $jsonObj->message = $msg;
                                        $jsonObj->message_tags = $msgTags;
                                        $response = json_encode($jsonObj);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $return['content'] = $response;
        return $return;
    }

    function getUserByName($name, $user_id) {
        $sql = "select UserID, UserName 
                from user 
                where UserName = '" . $name . "'
                and UserID not in(
                                select IF(CurrentUserID = $user_id, TargetUserID, CurrentUserID) as uID 
                                from userblock 
                                where CurrentUserID = $user_id OR TargetUserID = $user_id
                                 )
                ";
        $query = $this->db->query($sql);
        return $query->row();
    }

}

?>