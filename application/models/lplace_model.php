<?php

class LPlace_model extends CI_Model {

    function add($data) {
        log_message('error', '----- log add -----');
        $logMsg = var_export($data, TRUE);
        log_message('error', $logMsg);
        log_message('error', date('l jS \of F Y h:i:s A'));
        $this->db->insert('place', $data);
        return $this->db->insert_id();
    }

    function update($id, $data) {
        $ret = $this->db->update('place', $data, array('ID' => $id));
        return $ret;
    }

    function delete($id) {
        $this->db->delete('place', array('ID' => $id));
    }

    function getLocalPlaceInfo($id) {
        $this->db->select('*');
        $this->db->from('place');
        $this->db->where('ID', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getAllLocalPlace() {
        $this->db->select('*');
        $this->db->from('place');
        $this->db->where('Provider', 'local');
        $query = $this->db->get();
        return $query->result_array();
    }

    function getCountTotal() {
        $this->db->select('count(*) as `total`');
        $this->db->from('place');
        $this->db->where('Provider', 'local');
        $query = $this->db->get();
        $row = $query->row_array();
        return $row['total'];
    }

    function getListByCondition($cond, $from, $to, $getSumNumber) {
        $this->db->select('*');
        $this->db->from('place');
        $this->db->where('Provider', 'local');
        $this->db->order_by('ID', 'desc');

        if ($getSumNumber) {
            $query = $this->db->get();
            return $query->num_rows();
        } else {
            $this->db->limit($from, $to);
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? $query->result() : null;
        }
    }

    //generate PlaceID for Local Place
    function generateLocalPlaceID() {
        $checkcount = 0;
        while ($checkcount < 2) {
            $placeid = sha1('local' . time() . random_string(time()));
            if ($this->checkExistPlaceID($placeid) == 0) {
                return $placeid;
            }
            $checkcount++;
        }
        return null;
    }

    //check exist Local PlaceID with other provider
    function checkExistPlaceID($placeid) {
        $this->db->select('*');
        $this->db->from('place');
        $this->db->where('PlaceID', $placeid);
        $query = $this->db->get();
        $result = $query->result();
        if ($result) {
            return 1;
        }
        return 0;
    }

}

?>