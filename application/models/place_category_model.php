<?php

class Place_Category_model extends CI_Model {

    function returnListCategoryByName($names) {
        $provider = Config_model::getConfig('SEARCH_SERVICE_PROVIDER');
        $data = array();
        if (is_array($names)) {
            foreach ($names as $name) {
                $icon = self::getIconByName($name, $provider);
                $data[] = array(
                    'Name' => $name,
                    'Icon' => $icon ? base_url() . $this->config->item('category_icon_url') . $icon : ''
                );
            }
        } else {
            $icon = self::getIconByName($names, $provider);
            $data[] = array(
                'Name' => $names,
                'Icon' => $icon ? base_url() . $this->config->item('category_icon_url') . $icon : ''
            );
        }
        return $data;
    }

    function returnOnePhotoByName($names, $provider = '') {
        if ($provider == '') {
            $provider = Config_model::getConfig('SEARCH_SERVICE_PROVIDER');
        }
        $icon = "";
        if (is_array($names)) {
            foreach ($names as $name) {
                $icon = self::getIconByName($name, $provider);
                $icon = $icon ? base_url() . $this->config->item('category_icon_url') . $icon : '';
                break;
            }
        } else {
            $icon = self::getIconByName($names, $provider);
            $icon = $icon ? base_url() . $this->config->item('category_icon_url') . $icon : '';
        }
        return $icon;
    }

    /**
     * Author: Luong Luu
     * function : return one icon name without base_url
     * @param $names : array category or category name
     * @param $provider : string  
     * return : $icon as string
     * */
    function returnOneIconByName($names, $provider = '') {
        if ($provider == '') {
            $provider = Config_model::getConfig('SEARCH_SERVICE_PROVIDER');
        }
        $icon = "";
        if (is_array($names)) {
            foreach ($names as $name) {
                $icon = self::getIconByName($name, $provider);
                $icon = $icon ? $icon : '';
                break;
            }
        } else {
            $icon = self::getIconByName($names, $provider);
            $icon = $icon ? $icon : '';
        }
        return $icon;
    }

    function getIconByName($name, $provider = '') {
        if ($provider == '') {
            $provider = Config_model::getConfig('SEARCH_SERVICE_PROVIDER');
        }
        $this->db->select('Icon');
        $this->db->from('placecategory');
        $this->db->where('Name', $name);
        //$this->db->where('Provider', $provider);
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Icon : '';
    }

    function findAll() {
        $this->db->select('*');
        $this->db->from('placecategory');
        $this->db->order_by('Name', 'asc');
        $query = $this->db->get();
        return $query->result();
    }

    function findAllCategoryName() {
        $this->db->select('Name');
        $this->db->from('placecategory');
        $this->db->order_by('Name', 'asc');
        $query = $this->db->get();
        return $query->result_array();
    }

}

?>