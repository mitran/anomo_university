<?php

class Offline_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function update($data, $cond) {
        return $this->db->update('offlinemessagenotification', $data, $cond);
    }

    function updateStatus($ids) {
        $counter = 0;
        $queueLength = sizeof($ids) - 1;
        $condStr = "ID IN (";
        foreach ($ids as $id) {
            $condStr .= $id;
            if ($counter < $queueLength)
                $condStr .= ",";
            else
                $condStr .= ")";
            $counter++;
        }

        return $this->db->update('offlinemessagenotification', array('IsPush' => 1), $condStr);
    }

    function getMsgOffline($iLimit = 100) {
        $sql = "select d.DeviceID, d.Type as DeviceType, temp.* 
            from (
                    select * from offlinemessagenotification o
                    inner join ofpresence ofp ON o.UserID=ofp.username  AND o.CreatedDate > FROM_UNIXTIME(ofp.offlineDate/1000)
                    where IsPush = 0 AND FailCount < 3
                   ) as temp
            inner join devices d ON d.UserID=temp.UserID AND d.Status = 'active'
            inner join user u ON u.UserID=temp.UserID AND AllowChatNotice = 1
            limit $iLimit
        ";

        $query = $this->db->query($sql);

        $result = $query->result();
        return $result;
    }

    function updateNumberFail($id) {
        $sql = "Update offlinemessagenotification SET FailCount = FailCount + 1 Where ID=$id";
        return $this->db->query($sql);
    }

}

?>
