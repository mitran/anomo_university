<?php
class Reveal_model extends CI_Model {
	function add($data) {
		$this->db->insert('reveal', $data);
		return $this->db->insert_id();
	}
	
	function update($data, $cond) {
		return $this->db->update('reveal', $data, $cond);
	}
	
	function delete($cond){
		$this->db->delete('reveal', $cond);
	}
	
	function getRequestReveal($current_user_id, $target_user_id, $type, $picture_order = 0){
		$this->db->select('*');
		$this->db->from('reveal');
		$this->db->where('CurrentUserID', $current_user_id);
		$this->db->where('TargetUserID', $target_user_id);
		$this->db->where('Type', $type);
		if ($picture_order > 0){
			$this->db->where('PictureOrder', $picture_order);
		}
		$query = $this->db->get();
		return $query->row();
	}
	
	function getListReveal($current_user_id, $target_user_id, $select = '', $type = ''){
		$select_sql = '*';
		if ($select != ''){
			$select_sql = $select;
		}
		$this->db->select($select_sql);
		$this->db->from('reveal');
		$this->db->where('CurrentUserID', $current_user_id);
		$this->db->where('TargetUserID', $target_user_id);
		if($type !== 0){
			$this->db->where('Status', 1);
		}
		if ($type !== ''){
			$this->db->where('Type', $type);
		}
		$query = $this->db->get();
		return $query->result();
	}
	
	function getTotalReveal($current_user_id, $target_user_id){
		$this->db->select('count(ID) as num');
		$this->db->from('reveal');
		$this->db->where('CurrentUserID', $current_user_id);
		$this->db->where('TargetUserID', $target_user_id);
		$this->db->where('Status', 1);
		$query = $this->db->get();
		$row = $query->row();
		return ($row)?$row->num:0;
		
	}
	
	/**
	 * return reveal status
	 *
	 * @param int $current_user_id
	 * @param int $target_user_id
	 * @param int $type
	 * @return int 	0: NOT REVEAL
	 * 				1: REQUESTED
	 * 				2: REVEALED
	 */
	function getRevealStatus($current_user_id, $target_user_id, $type){
		$this->db->select('Status');
		$this->db->from('reveal');
		$this->db->where('CurrentUserID', $current_user_id);
		$this->db->where('TargetUserID', $target_user_id);
		$this->db->where('Type', $type);
		$query = $this->db->get();
		$row = $query->row();
		$status = 0;
		if ($row){
			if ($row->Status == 0){
				$status = 1;
			}elseif ($row->Status == 1){
				$status = 2;
			}
		}
		return $status;
	}

	function getListTypeRequesting($current_user_id, $target_user_id){
		$this->db->select('Type');
		$this->db->from('reveal');
		$this->db->where('CurrentUserID', $current_user_id);
		$this->db->where('TargetUserID', $target_user_id);
		$this->db->where('Status', 0);
		$this->db->group_by('Type');
		$query = $this->db->get();
		$result = $query->result();
		$data = array();
		if ($result){
			foreach ($result as $re){
				$data[] = $re->Type;
			}
		}
		return $data;
	}
	
	function getListPictureRevealedOf($user_id){
		$this->db->select('ID, PictureIDs');
		$this->db->from('reveal');
		$this->db->where('TargetUserID', $user_id);
		$this->db->where('Type', 0);
		$query = $this->db->get();
		return $query->result();
	}
	
	function deleteSecretPicture($user_id, $picture_id){
		$listReveal = self::getListPictureRevealedOf($user_id);
		if (count($listReveal) > 0){
			foreach ($listReveal as $reveal){
				$pictureIds = explode(',', $reveal->PictureIDs);
				$pictureIds = array_diff($pictureIds, array($picture_id));
				if (count($pictureIds) > 0){
					// update
					// remove this picture revealed
					self::update(array('PictureIDs' => implode(',', $pictureIds)), array('ID' => $reveal->ID)); 
				}else{
					// delete
					self::delete(array('ID' => $reveal->ID));
				}
			}
		}
	}
        
        function getRevealInfoBtw2User($current_user_id, $target_user_id, $type = -1){
            $this->db->select('Type, Status, PictureIDs, PictureOrder');
            $this->db->from('reveal');
            $this->db->where('CurrentUserID', $current_user_id);
            $this->db->where('TargetUserID', $target_user_id);
            if ($type > -1){
                $this->db->where('Type', $type);
            }
            $query = $this->db->get();
	    return $query->result();
        }
}
?>