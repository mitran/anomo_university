<?php

class Notification_History_model extends CI_Model {
    public $cached = false;

    function __construct()
    {
        parent::__construct();
        $CI =& get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
    }
    function add($data) {
        $data['UnView'] = isset($data['IsRead'])?$data['IsRead']:1;
        $this->db->insert('notificationhistory', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('notificationhistory', $data, $cond);
    }

    function delete($cond) {
        $this->db->delete('notificationhistory', $cond);
    }

    function updateIsReadWithoutMessage($user_id) {
        $this->db->update('notificationhistory', array('IsRead' => 0, 'Total' => 1), "UserID = $user_id ");
    }

    function getListNotificationHistory($user_id, $is_get_new = 0, &$total, $offset, $limit, &$new_notification_number) { 
        $where = '';
        if ($is_get_new == 1){
            $where .= " AND IsRead = 1 ";
        }
        $within = Config_model::getConfig('NOTIFICATION_HISTORY_RETURN_WITHIN');
        $sqlTotal = "select count(ID) as Total 
                    from notificationhistory n
                    where n.UserID=$user_id $where
                         AND Type != 0 AND Type != 1
                         AND n.CreatedDate >='".date('Y-m-d H:i:s', strtotime("-" . $within . " day", strtotime(gmdate('Y-m-d H:i:s'))))."'
                     ";
        $queryTotal = $this->db->query($sqlTotal);
        $rowTotal = $queryTotal->row();
        $total = $rowTotal?$rowTotal->Total:0;
        $result = array();
        if ($total > 0){
            $new_notification_number = $total;
            $photo_url = $this->config->item('s3_photo_url');
            $sql = "
                    select n.ID, n.SendUserID, n.Type, n.CreatedDate, n.Total, n.UnView, n.IsAnonymous,n.IsAnonymousComment
                        , if (n.RefID is null, '', n.RefID) as RefID
                        , if (n.AnomotionID is null, '', n.AnomotionID) as AnomotionID
                        , if (n.AnomotionStatus is null, '', n.AnomotionStatus) as AnomotionStatus
                        , if (n.Score is null, '', n.Score) as Score
                        , if (n.RevealType is null, '', n.RevealType) as RevealType
                        , if (n.PictureOrder is null, '', n.PictureOrder) as PictureOrder
                        , if (n.IsFacebookGame is null, '', n.IsFacebookGame) as IsFacebookGame
                        , if (n.ContentType is null, '', n.ContentType) as ContentType
                        ,IF (u.UserID is null, n.SendFacebookName, u.UserName) as UserName
                        ,IF (u.UserID is null, '', u.Gender) as Gender
                        ,IF (u.UserID is null, '', concat('$photo_url', u.Photo)) as Avatar
                        , if(u.FacebookID != '', 1, '') as FacebookID
                        ,IF (n.IsFacebookGame = 1, n.SendFacebookName, '') as FacebookName
                        , if (n.ContentID is null, '', n.ContentID) as ContentID
                        , u.IsAdmin
                        from (
                            select * 
                            from notificationhistory  
                            where UserID=$user_id $where
                                AND Type != 0 AND Type != 1
                                AND CreatedDate >='".date('Y-m-d H:i:s', strtotime("-" . $within . " day", strtotime(gmdate('Y-m-d H:i:s'))))."'
                            order by CreatedDate DESC
                            limit $offset, $limit
                        ) as n
                        left join user u ON u.UserID=n.SendUserID
                        where u.IsBanned != 1
                        order by CreatedDate DESC
                   ";            
            $query = $this->db->query($sql);
            $result = $query->result();
            
            if ($is_get_new == 0){
                // need calculate #numberNewNotification
                $where = " AND IsRead = 1 ";
                $newNotifySql = $sqlTotal .$where ;
                $newNotifyQuery = $this->db->query($newNotifySql);
                $newNotifyTotal = $newNotifyQuery->row();
                $new_notification_number = $newNotifyTotal?$newNotifyTotal->Total:0;      
            }        
        }
        return $result;
    }

    function getBadge($userInfo) {
        $within = Config_model::getConfig('NOTIFICATION_HISTORY_RETURN_WITHIN');
        $this->db->select('count(ID) as Number');
        $this->db->from('notificationhistory n');
        $this->db->where('UserID', $userInfo->UserID);
        $this->db->where('IsRead', 1);
        $this->db->where('n.CreatedDate >=', date('Y-m-d H:i:s', strtotime("-" . $within . " day", strtotime(gmdate('Y-m-d H:i:s')))));
        $query = $this->db->get();
        $row = $query->row();
        return $row?$row->Number:0;
    }
    
    /**Decrease #Badge and mark history as read
     * 
     * @param type $history_id
     * @param type $content_id
     * @param type $content_type
     * @param type $user_id
     * @return type
     */
    function read($history_id = -1, $content_id = -1, $content_type = -1, $user_id = -1) {
        $ret = false;
        if ($history_id > 0){ // just update one record 
            $ret = $this->db->update('notificationhistory', array('IsRead' => 0, 'UnView' => 0, 'Total' => 0), "ID = $history_id");
        }
        if ($content_id > 0){ // it means when user view a post directly so all notify of post should be marked as read and #badge auto decrease
            // include comment, like, mention a post, like on comment
            $sql = "update notificationhistory set IsRead = 0, UnView = 0, Total = 0
                    where UserID = $user_id 
                        AND( 
                            (Type = 14 AND RefID IN (select ID from activitycomment where ContentID = $content_id AND Type = $content_type))
                          OR(Type = 13 AND RefID IN (select ID from activitylike where ContentID = $content_id AND Type = $content_type))
                          OR(Type IN( 16, 17 ) AND RefID = $content_id AND ContentType = $content_type)
                        )
                    ";
            $ret = $this->db->query($sql);
        }
        return $ret;
    }

    function addMulti($data){
        $sql = "INSERT INTO notificationhistory (`UserID`,`SendUserID`,`Type`,`RefID`,`ContentType`,`IsRead`, `UnView`,`CreatedDate`)
		VALUE ".implode(',', $data);
        $this->db->query($sql);
    }
}

?> 