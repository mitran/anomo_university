<?php

class FbAnomotion_model extends CI_Model {

    function update($data, $cond) {
        return $this->db->update('fbanomotion', $data, $cond);
    }

    function delete($cond) {
        $this->db->delete('fbanomotion', $cond);
    }

    function requestAnomotion($data) {
        $this->db->insert('fbanomotion', $data);
        return $this->db->insert_id();
    }

    function checkRequest($user_id, $target_user_id, $is_facebook_game = 0) {
        $this->db->select('*');
        $this->db->from('fbanomotion');
        $this->db->where("((UserID = $user_id AND TargetUserID = $target_user_id) OR (UserID = $target_user_id AND TargetUserID = $user_id))");
        $this->db->where("Status !=", 2); // cancel
        $this->db->where('Status !=', 3); // finish
        $this->db->where('IsFacebookGame', $is_facebook_game);
        $this->db->order_by('CreatedDate', 'DESC');
        $this->db->limit(1, 0);
        $query = $this->db->get();
        return $query->row();
    }

    function checkAnomotion($user_id, $anomotion_id) {
        $this->db->select('*');
        $this->db->from('fbanomotion');
        $this->db->where('AnomotionID', $anomotion_id);
        if ($user_id > 0) {
            $this->db->where('TargetUserID', $user_id);
        }
//		$this->db->where('Status', 0);
        $query = $this->db->get();
        return $query->row();
    }

    function getAnomotion($anomotion_id) {
        $this->db->select('*');
        $this->db->from('fbanomotion');
        $this->db->where('AnomotionID', $anomotion_id);
        $query = $this->db->get();
        return $query->row();
    }

    function getAnomotionsByUserID($userID) {
        $this->db->select('a.UserID, a.AnomotionID, a.QuestionGroupID, a.Status, a.CreatedDate, u.UserID as TargetUserID, u.UserName as TargetUserName, u.Photo as TargetAvatar');
        $this->db->from('fbanomotion a');
        $this->db->join('user u', "u.UserID = a.UserID OR u.UserID = a.TargetUserID");
        $this->db->where("u.UserID != $userID");
        $this->db->where("(a.UserID = $userID OR a.TargetUserID = $userID)");
        $query = $this->db->get();
        return $query->result();
    }

    function getAnomotionsBy2User($userID, $targetUserID, $is_facebook_game =0) {
        $currentTime = gmdate("Y-m-d H:i:s", strtotime($this->config->item('expired')));
        $this->db->select("a.*");
        $this->db->from('fbanomotion a');

        $this->db->where("((a.UserID = $userID AND a.TargetUserID = $targetUserID AND a.IsUserDelete = 0) OR (a.UserID = $targetUserID AND a.TargetUserID = $userID AND a.IsTargetUserDelete = 0))");

        $sql = "((a.Status = 1 AND a.CreatedDate >= '$currentTime') OR (a.Status = 3))";
        $this->db->where($sql);

        $this->db->order_by('a.CreatedDate', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    function getAllAnomotionBy2User($user_id, $target_user_id) {
        $this->db->select('*');
        $this->db->from('fbanomotion a');
        $this->db->where("((a.UserID = $user_id AND a.TargetUserID = $target_user_id ) OR (a.UserID = $target_user_id AND a.TargetUserID = $user_id ))");
        $query = $this->db->get();
        return $query->result();
    }

    function getRequestByUser($userId, $limit = 0) {
        $this->db->distinct();
        $this->db->select('UserID as UserID1,TargetUserID as UserID2, CreatedDate');
        $this->db->from('fbanomotion');
        $this->db->where("UserID = $userId OR TargetUserID = $userId");
        $this->db->where("Status !=", 2); // cancel
        $this->db->where('Status !=', 3); // finish
        $this->db->order_by('CreatedDate', 'DESC');
        if ($limit > 0) {
            $this->db->limit($limit);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function deleteAnomotionBwt2User($user_id, $target_user_id) {
        // get all anomotion of 2user
        $anomotions = self::getAllAnomotionBy2User($user_id, $target_user_id);
        if ($anomotions) {
            foreach ($anomotions as $anomotion) {
                // del notification history
                $this->db->delete('notificationhistory', array('AnomotionID' => $anomotion->AnomotionID));
                $this->db->delete('fbresult', array('AnomotionID' => $anomotion->AnomotionID));
                $this->db->delete('fbanomotion', array('AnomotionID' => $anomotion->AnomotionID));
            }
        }
    }

    function getResult($anomotion_id, $user_id) {
        $this->db->select('*, q.Content as QuestionContent');
        $this->db->from('fbresult r');
        $this->db->join('question q', 'q.QuestionID=r.QuestionID');
        $this->db->join('answer a', 'a.AnswerID=r.AnswerID');
        $this->db->where('r.AnomotionID', $anomotion_id);
        $this->db->where('r.UserID', $user_id);
        $this->db->order_by('q.Order', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    function checkAnomotionIsFinished($anomotion_id, $user_id) {
        $this->db->select('a.AnomotionID');
        $this->db->from('fbanomotion a');
        $this->db->where('a.AnomotionID', $anomotion_id);
        $this->db->where("(a.UserID = $user_id OR a.TargetUserID = $user_id)");
        $this->db->where('a.Status', 3);
        $query = $this->db->get();
        return $query->row();
    }
    
    function getFbGame($user_id, &$total, $offset, $limit){
        // get total
        $expire_date = $this->config->item('expired');
        $sqlTotal = "select count(AnomotionID) as Total
                    from fbanomotion
                    where Status = 1 AND TargetUserID = $user_id AND IsFacebookGame = 1
                    AND CreatedDate >= '".date('Y-m-d H:i:s', strtotime($expire_date, strtotime(gmdate('Y-m-d H:i:s'))))."'";
                    
        $qTotal = $this->db->query($sqlTotal);
        $qResult = $qTotal->row();
        $total = $qResult?$qResult->Total:0;
        
        $sql = "select AnomotionID, UserID, UserName as FacebookName, UserID as FacebookID, CreatedDate 
               from fbanomotion
               where Status = 1 AND TargetUserID = $user_id AND IsFacebookGame = 1
                    AND CreatedDate >= '".date('Y-m-d H:i:s', strtotime($expire_date, strtotime(gmdate('Y-m-d H:i:s'))))."'
               order by CreatedDate desc
               limit $offset, $limit
            ";      
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function isFBGamePlaying($user_id, $sFbID){
        $expire_date = $this->config->item('expired');        
        $sql = "select IF (UserID = $user_id, TargetUserID, UserID) as FacebookID
                        ,IF (UserID = $user_id, TargetUserName, UserName) as FacebookName
                        ,IF (UserID = $user_id, 0, 1) as IsTargetRequest
               from fbanomotion
               where Status = 1  
               AND (  (UserID = $user_id AND TargetUserID IN ($sFbID) )
                  OR (TargetUserID = $user_id AND UserID IN ($sFbID) ) )
               AND CreatedDate >= '".date('Y-m-d H:i:s', strtotime($expire_date, strtotime(gmdate('Y-m-d H:i:s'))))."' ";

        $query = $this->db->query($sql);
        return  $query->result();

    }
    
    function getListFbGame($user_id, &$total, $offset, $limit){
        
        // total
        $sqlTotal = "select count(*) as Total from
                        (select if (UserID = $user_id, TargetUserID, UserID) as fUserID from fbanomotion
                        where (UserID = $user_id OR TargetUserID = $user_id) AND IsFacebookGame = 1
                        group by fUserID ) as a";
        $qTotal = $this->db->query($sqlTotal);
        $qResult = $qTotal->row();
        $total = $qResult?$qResult->Total:0;
        
        $sql = "SELECT t1.AnomotionID, t1.fUserID as UserID, t1.fUserID as FacebookID, t1.FacebookName, t1.CreatedDate, 1 as IsGame
                FROM (select *, if (UserID = $user_id, TargetUserID, UserID) as fUserID
                               ,if (UserID = $user_id, TargetUserName, UserName) as FacebookName
                      from fbanomotion 
                      where (UserID = $user_id OR TargetUserID = $user_id) AND IsFacebookGame = 1) as t1
                inner JOIN
                      (SELECT max(AnomotionID) as maxAnomotionID, if (UserID = $user_id, TargetUserID, UserID) as fUserID
                       FROM fbanomotion where (UserID = $user_id OR TargetUserID = $user_id) AND IsFacebookGame = 1
                       GROUP BY fUserID desc) t2
                ON t1.AnomotionID = t2.maxAnomotionID AND t1.fUserID=t2.fUserID
		order by t1.CreatedDate desc
                limit $offset,$limit
            ";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function createGameFb($user_id, $oData, $isFbGame = 0) {
        try {
            $this->db->trans_begin();
            
            $question_group_id = $oData->QuestionGroupID;
            $created_date = gmdate('Y-m-d H:i:s');
           
            $fb_id = $oData->RequestFacebookID;
            $fb_name = $oData->RequestFacebookName;
            if (is_null($fb_name) || $fb_name == '' || $fb_name == '(null)'){
                $fb_name = self::getFbName($fb_id);
            }
            
            // create 4 games
            $sql = "INSERT INTO fbanomotion (`UserID`,`TargetUserID`,`Status`,`QuestionGroupID`,`CreatedDate`, `IsFacebookGame`, `UserName`, `TargetUserName`) VALUES ";
            foreach($oData->FBs as $oFb){
                $value [] = " ($fb_id, '$oFb->FacebookID', 1, $question_group_id, '$created_date', '$isFbGame', '$fb_name', '$oFb->FacebookName')";
            }
            $sql .= implode(',', $value);

            $this->db->query($sql);
            $anomotion_id = $this->db->insert_id();
            
            // get gameid has been created
            $aAnomotionIDs = array();
            for($i = 0; $i < count($oData->FBs); $i ++){
                $aAnomotionIDs[] = $anomotion_id + $i;
            }
            
            // add result
            // for requester  
//            $listQuestions = self::sortBy($oData->Questions, 'Order');
            $listQuestions = $oData->Questions;
            foreach($aAnomotionIDs as $anomotionID){
                $sql = "INSERT INTO fbresult(`AnomotionID`,`UserID`,`QuestionID`,`AnswerID`,`CreatedDate`) VALUES";
                $value = array();
                foreach($listQuestions as $oQuestion){
                    $value[] = " ($anomotionID, $fb_id, $oQuestion->QuestionID, $oQuestion->AnswerID, '$created_date')";
                }
                $sql .= implode(',', $value);
                $this->db->query($sql);
            }
            
            // for partner - add first question
            // get first question
            $iFirstQuestionID = '';
            foreach ($listQuestions as $oQuestion) {
                if ($oQuestion->Order == 1) {
                    $iFirstQuestionID = $oQuestion->QuestionID;
                }
            }
            $sID = implode(',', $aAnomotionIDs);          
            $sql = "INSERT INTO fbresult(`AnomotionID`,`UserID`,`QuestionID`,`CreatedDate`)  ";
            $sql .= " select AnomotionID, TargetUserID, '$iFirstQuestionID' as QuestionID, '$created_date' as CreatedDate from fbanomotion where AnomotionID IN ($sID) ";
            $this->db->query($sql);
            
            // log this groupquestion as played
            // get category 
            $s = "select Category from questiongroup where QuestionGroupID = $question_group_id";
            $q = $this->db->query($s);
            $row = $q->row();
            $category = $row?$row->Category:'';

            $aPartnerID[] = $user_id;
            $sUserID = implode(',', array_values($aPartnerID));
            $sql = "select * from questionplayed where UserID IN ($sUserID) AND Category = '$category'";
            $query = $this->db->query($sql);
            $result = $query->result();
            if ($result){
                foreach($result as $re){
                    if (in_array($re->UserID, array_values($aPartnerID))){
                        $newQuestionGroupIDs = ($re->QuestionGroupIDs)?$re->QuestionGroupIDs.','.$question_group_id:$question_group_id;
                        $subQ = " update questionplayed set QuestionGroupIDs = '$newQuestionGroupIDs' where UserID=$re->UserID AND Category='$category'";
                        $this->db->query($subQ);
                    }else{
                        $subQ = " insert into questionplayed (`UserID`,`QuestionGroupIDs`,`Category`) value";
                        $subQ .= " ('$re->UserID', $question_group_id, '$category')";                  
                        $this->db->query($subQ);
                    }
                }
            }else{
                $subQ = " insert into questionplayed (`UserID`,`QuestionGroupIDs`,`Category`) values"; 
                foreach($aPartnerID as $id){
                    $v[] = "($id, $question_group_id, '$category')";
                }
                $subQ .= implode(',', $v);
                $this->db->query($subQ);
            }
            
            // add to notification history
            $sql1 = "INSERT INTO notificationhistory(`AnomotionID`,`UserID`,`SendUserID`,`Type`,`Total`,`IsRead`,`CreatedDate`,`IsFacebookGame`,`FacebookID`,`SendFacebookID`,`SendFacebookName`)  ";
            $sql1 .= " select AnomotionID
                        ,IF (u.UserID is not null, u.UserID, '') as UserID
                        ,'$user_id' as SendUserID, '3' as Type, '1' as Total, '1' as IsRead, '$created_date' as CreatedDate , IsFacebookGame                        
                        ,a.TargetUserID
                        ,'$fb_id' as SendFacebookID
                        ,'$fb_name' as SendFacebookName
                       from (select * from fbanomotion where AnomotionID IN ($sID)) as a
                       left join user u ON a.TargetUserID = u.FacebookID
                    ";
            
            $this->db->query($sql1);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
                
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }

        return isset($aAnomotionIDs)?$aAnomotionIDs:false;
    }

    function getFbGamesBy($ids){
        $sql = "select t.* from (select AnomotionID, TargetUserID, UserID from fbanomotion where AnomotionID IN (".  implode(',', $ids).")) as t
                inner join user u ON u.FacebookID=t.TargetUserID AND u.AllowAnomotionNotice = 1
            ";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function getFbName($fb_id = 0){
        $name = '';
        if ($fb_id > 0) {
            $url = "http://graph.facebook.com/$fb_id?fields=name";
            $json = @file_get_contents($url);
            if ($json) {;  
               $data = json_decode($json);
               if ($data){
                   $name = isset($data->name)?$data->name:'';
               }
            }
        }
        return $name;
    }
}

?>