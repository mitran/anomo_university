<?php
class Video_model extends CI_Model{
    public $youtubeVideoUrl = '';
    public $youtubeVideoThumbnailUrl = '';
    public $youtubeImageDefault = '';
    public $photoUrl = '';
    public $vineVideoUrl = '';
    
    function __construct() {
        parent::__construct();
        $this->youtubeVideoUrl = YOUTUBE_VIDEO_URL;
        $this->youtubeVideoThumbnailUrl = YOUTUBE_VIDEO_THUMBNAIL_URL;
        $this->youtubeImageDefault = YOUTUBE_IMAGE_DEFAULT;
        $this->vineVideoUrl = VINE_VIDEO_URL;
        $this->photoUrl = $this->config->item('s3_photo_url');
        $this->videoUrl = $this->config->item('s3_video_url');
    }
    
    function add($data) {
        $this->db->insert('video', $data);
        return $this->db->insert_id();
    }
    
    function getListVideo($cond, &$total, $offset = 0, $limit = 0) {
        $where = array();
        $sWhere = '';
        $where[] = " IsDeleted != 1 ";
        if (isset($cond['keyword']) && $cond['keyword'] != ''){
            $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
            $keyword = mysqli_real_escape_string($con, $cond['keyword']);  
            $where[] = " Content like '%".$keyword."%'";
        }

        if (sizeof($where) > 0){
            $sWhere = 'where '.implode('AND', $where);
        }
        
        $sqlTotal = "select count(ID) as Total from video $sWhere";
        $totalResult = $this->db->query($sqlTotal);
        $totalRow = $totalResult->row();
        $total = $totalRow?$totalRow->Total:0;
        if ($total > 0){
            $sql = "select u.UserName, Content, v.UserID, v.CreatedDate, v.ID
                        , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                        , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                        , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID, VideoSource 
                   from (select * from video $sWhere order by ID DESC limit $limit, $offset ) as  v
                   left join user u ON (u.UserID = v.UserID)
                    ";
            $query1 = $this->db->query($sql);
            return $query1->result();
        }
        return null;
    }
}
?>
