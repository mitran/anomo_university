<?php
class Wstoken_model extends CI_Model
{
    public $cached = false;

    function __construct()
    {
        parent::__construct();
        $CI =& get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
    }

    function getToken($token)
    {
        if ($this->cached) {
            $cache = $this->cached->get($token);

            if ($cache) {
                return $cache;
            } else {
                $this->db->select('u.EmailAccountStatus, u.SignUpDate, t.UserID, u.UserName, u.Photo as Avatar, u.Lat, u.Lon, u.NeighborhoodID, u.BirthDate, u.Gender, u.NumberInvite, u.FacebookID, u.IsAgeLess,u.IsAdmin,u.IsVerify,u.HasFanPage,u.GenderDating, u.Email, u.IsEmailVerify');
                $this->db->from('wstoken t');
                $this->db->join('user u', 't.UserID=u.UserID', 'left');
                $this->db->where('t.Token', $token);
                $query = $this->db->get();
                $result = $query->row();
                $query->free_result();
                $this->cached->set($token, $result);
                return $result;
            }
        } else {
            $this->db->select('u.EmailAccountStatus, u.SignUpDate, t.UserID, u.UserName, u.Photo as Avatar, u.Lat, u.Lon, u.NeighborhoodID, u.BirthDate, u.Gender, u.NumberInvite, u.FacebookID, u.IsAgeLess,u.IsAdmin,u.IsVerify,u.HasFanPage,u.GenderDating, u.Email, u.IsEmailVerify');
            $this->db->from('wstoken t');
            $this->db->join('user u', 't.UserID=u.UserID', 'left');
            $this->db->where('t.Token', $token);
            $query = $this->db->get();
            return $query->row();
        }
    }

    function checkTokenOfUser($user_id, $fb_id = 0)
    {
        $this->db->select('WsTokenID, Token');
        $this->db->from('wstoken');
        $this->db->where('UserID', $user_id);
        if ($fb_id > 0) {
            $this->db->where('FacebookID', $fb_id);
        }
        $query = $this->db->get();
        return $query->row();
    }

    function getTokenOfUser($user_id)
    {
        $this->db->select('Token, UserName, Photo');
        $this->db->from('wstoken t');
        $this->db->join('user u', 't.UserID=u.UserID');
        $this->db->where('u.UserID', $user_id);
        $query = $this->db->get();
        return $query->row();
    }

    function delete($cond)
    {
        $this->db->delete('wstoken', $cond);
    }

    function create($data)
    {
        $this->db->insert('wstoken', $data);
        return $this->db->insert_id();
    }

    function update($cond, $data, $token = '')
    {
        if ($this->cached) {
            if ($token != ""){
                $this->cached->delete($token);
            }
        }
        $this->db->update('wstoken', $data, $cond);
    }

    function getCacheInfo()
    {
        var_dump($this->cached->cache_info());
    }
}

?>