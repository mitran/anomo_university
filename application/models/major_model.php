<?php

class Major_model extends CI_Model {
    public $cached = false;

    function __construct()
    {
        parent::__construct();
        $CI =& get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
    }
    
    function add($data) {
        $this->db->insert('major', $data);
        if ($this->cached) {
            $this->cached->delete('list_majors');
        }
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('major', $data, $cond);
    }
    
    function getInfo($id){
        $this->db->select('*');
        $this->db->from('major');
        $this->db->where('MajorID', $id);
        $query = $this->db->get();
        return $query->row();
    }

    
    function getListMajorOfUser($user_id)
    {
        if ($this->cached) {
            $cache = $this->cached->get('major' . $user_id);
            if ($cache) {
                return $cache;
            } else {
                $this->db->select('t.MajorID, t.Name');
                $this->db->from('major t');
                $this->db->join('usermajor ut', 'ut.MajorID=t.MajorID');
                $this->db->where('ut.UserID', $user_id);

                $this->db->order_by('t.Name', 'asc');
                $query = $this->db->get();
                $result = $query->result();
                $query->free_result();
                $this->cached->set('major' . $user_id, $result);
                return $result;
            }
        } else {
            $this->db->select('t.MajorID, t.Name');
            $this->db->from('major t');
            $this->db->join('usermajor ut', 'ut.MajorID=t.MajorID');
            $this->db->where('ut.UserID', $user_id);
            $this->db->order_by('t.Name', 'asc');
            $query = $this->db->get();
            $retVal = $query->result();
            $query->free_result();
            return $retVal;
        }
    }
    
    function addMulti($user_id, $data){
        if ($this->cached){
            $this->cached->delete('major' . $user_id);
        }
        foreach ($data as $value) {
            if ($value > 0)
            $intent[] = "('" . $user_id . "', '".$value."')";
        }
        if (isset($intent)) {
            $deleteSql = "delete from usermajor where UserID =".$user_id;
            $this->db->query($deleteSql);
            $sql = "INSERT INTO usermajor (`UserID`, `MajorID`) VALUES " . implode(',', $intent);
            $this->db->query($sql);
            return $this->db->insert_id();
        }
    }

    
    function insertUserMajor($user_id, $data){
        foreach ($data as $value) {
            if ($value > 0)
            $intent[] = "('" . $user_id . "', '".$value."')";
        }
        if (isset($intent)) {
            $sql = "INSERT INTO usermajor (`UserID`, `MajorID`) VALUES " . implode(',', $intent);
            $this->db->query($sql);
            return $this->db->insert_id();
        }
    }
    
    
    function getListMajors(){
        if ($this->cached){
            $cached = $this->cached->get('list_majors');
            if ($cached){
                return $cached;
            }else{
                $interests = self::_getMajors();
                if ($interests){
                    $this->cached->set('list_majors', $interests);
                }
                return $interests;
            }
        }else{
            return self::_getMajors();
        }
    }
    
    function _getMajors(){
        $sql = "select t.MajorID, t.Name, tc.Name as Category, tc.ID 
                from (select * from major where MajorCategoryID > 0 ) as t 
                left join majorcategory tc ON t.MajorCategoryID= tc.ID
                order by Name ASC
                ";
        $query = $this->db->query($sql);
        $result = $query->result();
        $data = array();
        if ($result){
            $cats = array();
            foreach($result as $re){
                $cats[$re->ID] = $re->Category; 
            }
            ksort($cats);
            foreach($cats as $key => $value){
               $tagList = array();
               foreach($result as $row){
                   if ($key == $row->ID){
                       $tagList[] = array(
                         'MajorID'    => $row->MajorID,
                         'Name'     => $row->Name
                       );
                   }
               } 
               $data[] = array(
                   'Category'   => $value,
                   'MajorCategoryID'  => (string)$key,
                   'MajorList'    => $tagList
               );
            }
        }
        return $data;
    }
    
    function getMajorCategory() {
        $this->db->select('*');
        $this->db->from('majorcategory');
        $query = $this->db->get();
        $result = $query->result();
        $data = array();
        foreach ($result as $re) {
            $data[$re->ID] = $re->Name;
        }
        return $data;
    }
    
    function getListWithPaging($cond = array(), &$total = 0, $from = 0, $to = 0) {
        $total = 0;
        $where = "";
        if (isset($cond['category'])){
            $where .= ' AND MajorCategoryID ='.$cond['category'];
        }
        $sqlTotal = "select count(MajorID) as Total from major where MajorCategoryID > 0 ".$where ;
        $queryTotal = $this->db->query($sqlTotal);
        $rowTotal = $queryTotal->row();
        $total = $rowTotal ? $rowTotal->Total : 0;
        if ($total > 0){
            $sql = "select t.MajorID, t.Name, tc.Name as Category, t.MajorCategoryID 
                    from (select * from major  where MajorCategoryID > 0 $where limit $to, $from) as t
                    left join tagcategory tc ON t.MajorCategoryID= tc.ID
                     ";
            $query = $this->db->query($sql);
            return $query->result();
        }
        return false;
    }
    
    function checkExistedName($name, $id) {
        $this->db->select('MajorID');
        $this->db->from('major');
        $this->db->where('Name', $name);
        if ($id > 0) {
            $this->db->where('MajorID !=', $id);
        }
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? false : true;
    }
    
    function getCatInfo($id) {
        $this->db->select('*');
        $this->db->from('majorcategory');
        $this->db->where('ID', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addCat($data) {
        $this->db->insert('majorcategory', $data);
        if ($this->cached){
            $this->cached->delete('list_majors');
        }
        return $this->db->insert_id();
    }
    
    function updateCat($data, $cond) {
        $this->db->update('majorcategory', $data, $cond);
        if ($this->cached){
            $this->cached->delete('list_majors');
        }
    }

}

?>