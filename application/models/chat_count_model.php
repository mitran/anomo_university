<?php

class Chat_Count_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**get current #badge Chat and increase to 1
     * 
     * @param type $user_id
     * @return type
     */
    function getChatCountOfUser($user_id) {
        $chatCount = 0;
        $this->db->select('ChatCount');
        $this->db->from('chat_count');
        $this->db->where('UserID', $user_id);
        $query = $this->db->get();
        $row = $query->row();
        if ($row) {
            $chatCount = $row->ChatCount;
            $update = "update chat_count SET `ChatCount` = `ChatCount` + 1 where UserID = $user_id";
            $this->db->query($update);
        } else {
            $insert = "insert INTO chat_count (UserID, ChatCount) value ($user_id, 1)";
            $this->db->query($insert);
        }
        return $chatCount;
    }
    
    function updateChatCount($user_id, $chat_count){
        $sql = "update chat_count set ChatCount = $chat_count where UserID = $user_id";
        return $this->db->query($sql);
    }
    
    function returnChatCount($user_id){
        $this->db->select('ChatCount');
        $this->db->from('chat_count');
        $this->db->where('UserID', $user_id);
        $query = $this->db->get();
        $row = $query->row();
        return $row?$row->ChatCount:0;
    }
    
    function insertMulti($data){
        $value = array();
        foreach($data as $re){
            $value[] = "($re, 1)";
        }
        if (sizeof($value) > 0){
            $sql = "insert into chat_count (UserID, ChatCount) values ".implode(',', $value);
            $this->db->query($sql);
        }
    }

    function updateMulti($data){
        $sql = "update chat_count set `ChatCount` = `ChatCount` + 1 where UserID IN (".implode(',', $data).")";
        return $this->db->query($sql);
    }
}

?>
