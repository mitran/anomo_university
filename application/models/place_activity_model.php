<?php

class Place_Activity_model extends CI_Model {

    function add($data) {
        $this->db->insert('placeactivity', $data);

        $id = $this->db->insert_id();
        $this->addDetail($id);
        return $id;
    }

    function update($data, $cond) {
        $this->db->update('placeactivity', $data, $cond);

        //update detail
        $this->updateDetail($data, $cond);
    }

    function checkPlaceActivity($place_id, $user_id = 0, $type = '') {
        $this->db->select('ID');
        $this->db->from('placeactivity');
        $this->db->where('PlaceID', $place_id);
        if ($user_id > 0) {
            $this->db->where('UserID', $user_id);
        }
        if ($type != '') {
            $this->db->where('Type', $type);
        }
        $query = $this->db->get();
        return $query->row();
    }

    function getActivitiesByUserID($user_id) {
        /* $this->db->select("pa.ID, p.PlaceID, p.Reference, pa.Type, p.Name, p.Address, pa.CreatedDate, pa.RefID, pa.Content as Status");
          $this->db->from('placeactivity pa');
          $this->db->join('place p', 'p.PlaceID=pa.PlaceID', 'left');
          $this->db->where('UserID', $user_id);
          $this->db->order_by('pa.CreatedDate', 'DESC');
          $this->db->limit(30);
          $query = $this->db->get();
         */
        $strsql = "SELECT pa.ID, p.PlaceID, p.Reference, pa.Type, p.Name, p.Address, 
		pa.CreatedDate, pa.RefID, pa.Content AS Status FROM ((SELECT * FROM placeactivity pac 
		WHERE UserID=$user_id ORDER BY CreatedDate DESC LIMIT 24) as pa LEFT JOIN place p ON 
		p.PlaceID=pa.PlaceID)";

        $query = $this->db->query($strsql);

        return $query->result();
    }

    function delete($id) {
        $this->db->delete('placeactivity', array('ID' => $id));
        $this->db->delete('placeactivitydetail', array('ID' => $id));
    }

    function getActivityInfo($id) {
        $this->db->select('*');
        $this->db->from('placeactivity');
        $this->db->where('ID', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getActivitiesByUserIDWithPaging($user_id, &$total = 0, $from = 0, $to = 0) {
        /*
          $strsql = "SELECT pa.ID, p.PlaceID, p.Reference, pa.Type, p.Name, p.Address,
          pa.CreatedDate, pa.RefID, pa.Content AS Status FROM ((SELECT * FROM PlaceActivity pac
          WHERE userid=$user_id ORDER BY CreatedDate DESC %limit% ) as pa LEFT JOIN Place p ON
          p.PlaceID=pa.PlaceID)";
          $strsql1 = str_replace('%limit%', '', $strsql);
          $query = $this->db->query($strsql1);

          $total = $query->num_rows();
          $strsql2 = str_replace('%limit%', "LIMIT $from, $to", $strsql);
          $query = $this->db->query($strsql2);

          return $query->result();
         */

        //Get total rows
        $strsqltotal = "SELECT count(ID) as 'total' FROM PlaceActivity pac WHERE UserID=$user_id";
        $query = $this->db->query($strsqltotal);
        $total = $query->row()->total;


        //Get results
        $strsql = "SELECT pa.ID, p.PlaceID, p.Reference, pa.Type, p.Name, p.Address, 
		pa.CreatedDate, pa.RefID, pa.Content AS Status, pa.PostID, pa.IsShare FROM ((SELECT * FROM placeactivity pac 
		WHERE UserID=$user_id ORDER BY CreatedDate DESC %limit% ) as pa LEFT JOIN Place p ON 
		p.PlaceID=pa.PlaceID)";
        $strsql = str_replace('%limit%', "LIMIT $from, $to", $strsql);
        $query = $this->db->query($strsql);

        return $query->result();
    }

    function getActivitiesDetailByUserIDWithPaging($user_id, &$total = 0, $from = 0, $to = 0) {


        //Get total rows
        $strsqltotal = "SELECT count(pa.ID) as 'total'
                        FROM (select ID from placeactivity where UserID = $user_id) pa
                        INNER JOIN placeactivitydetail pad on pad.ID = pa.ID";
        $query = $this->db->query($strsqltotal);
        $total = $query->row()->total;


        $photo_base = $this->config->item('s3_photo_url');
        $icon_base = base_url() . $this->config->item('category_icon_url');

        //Get results
        $strsql = "select
                   pad.ID,
                   if(pad.PlaceID is NULL,'',pad.PlaceID) as PlaceID,
                   pad.Type,
                   pad.Name,
                   pad.Address,
                   pad.CreatedDate,
                   pad.PostID,
                   pad.IsShare,
                   pad.ContentFeedType,
                   pad.ContentID,
                   pad.Content,
                   if(pad.Photo is null or pad.Photo = '',pad.Photo,if(pad.Type in (0,4,8),if(pad.PostID >0,concat('$photo_base',pad.Photo),concat('$icon_base',pad.Photo)),concat('$photo_base',pad.Photo))) as 'Photo',
                   if(pad.Photo194 is null or pad.Photo194 = '',pad.Photo194,concat('$photo_base',pad.Photo194)) as 'Photo194',
                   if(pad.Photo454 is null or pad.Photo454 = '',pad.Photo454,concat('$photo_base',pad.Photo454)) as 'Photo454',
                   pad.Username,
                   pad.IsSend,
                   if(pad.NeighborhoodID is NULL,'',pad.NeighborhoodID) as 'NeighborhoodID',
                   if(pad.Photo100 is null or pad.Photo100 = '',pad.Photo100,concat('$photo_base',pad.Photo100)) as 'Photo100',
                   if(pad.Photo200 is null or pad.Photo200 = '',pad.Photo200,concat('$photo_base',pad.Photo200)) as 'Photo200',
                   if(pad.Photo300 is null or pad.Photo300 = '',pad.Photo300,concat('$photo_base',pad.Photo300)) as 'Photo300'
                   from (select ID from placeactivity where UserID = $user_id) pa
                   inner join placeactivitydetail pad on pad.ID = pa.ID
                   order by CreatedDate desc
                   limit $from,$to";

        $query = $this->db->query($strsql);

        return $query->result();
    }

    function getActivitiesDetailByUserIDWithPaging_SP($user_id, &$total = 0, $from = 0, $to = 0) {
        $photo_base = $this->config->item('s3_photo_url');
        $icon_base = base_url() . $this->config->item('category_icon_url');
        $sql = "CALL sp_getUserActivity($user_id,$from,$to,'" . $photo_base . "','" . $icon_base . "',@total)";
        $query = $this->db->query($sql);
        $result = $query->result();
        $query->next_result();
        $query->free_result();
        $query2 = $this->db->query("select @total as total");
        foreach ($query2->result_array() as $row) {
            $total = $row['total'];
        }

        $query2->free_result();
        return $result;
    }

    function getActivityNotExistDetail($limit = 10) {
        $this->db->select('ID');
        $this->db->from('placeactivity');
        $this->db->where('ID NOT IN (Select ID from placeactivitydetail)');
        $this->db->limit($limit);
        $query = $this->db->get();
        return $query->result();
    }

    function updateDetail($data, $cond) {
        if (isset($data['CreatedDate']) && isset($cond['ID'])) {
            $this->db->update('placeactivitydetail', array('CreatedDate' => $data['CreatedDate']), array('ID' => $cond['ID']));
        }
    }

    function updateUserDetail($data, $cond) {
        if (isset($cond['UserID']) || isset($cond['GiftUserID'])) {
            return $this->db->update('placeactivitydetail', $data, $cond);
        }
    }

    function addDetail($ActivityId = 0) {
        $CI = &get_instance();
        $CI->load->model('Activity_model');
        $CI->load->model('Place_model');
        $CI->load->model('Place_Category_model');
        $CI->load->model('Post_Comment_model');
        $CI->load->model('Gift_Send_model');
        $CI->load->model('User_model');
        $CI->load->model('Place_Post_model');
        $CI->load->model('Neighborhood_model');
        $CI->load->model('Neighborhood_Post_model');
        $CI->load->model('Picture_Post_Activity_model');


        $data = $this->getActivityInfo($ActivityId);

        $detail = new stdClass();
        $detail->ID = $ActivityId;
        $detail->UserID = $data->UserID;
        $detail->PlaceID = $data->PlaceID ? $data->PlaceID : NULL;
        $detail->Type = $data->Type;
        $detail->Name = "";
        $detail->Address = "";
        if ($detail->PlaceID) {
            $placeinfo = $CI->Place_model->getPlace($detail->PlaceID);
            if ($placeinfo) {
                $detail->Name = $placeinfo->Name;
                $detail->Address = $placeinfo->Address;
            }
        }
        $detail->CreatedDate = $data->CreatedDate;
        $detail->ContentFeedType = -1;
        $detail->ContentID = $data->RefID;
        $detail->Content = $data->Content;
        $detail->Photo = "";
        $detail->Photo194 = "";
        $detail->Photo454 = "";
        $detail->Username = "";
        $detail->IsSend = 0;
        //$detail->NeighborhoodID = "";
        $detail->Photo100 = "";
        $detail->Photo200 = "";
        $detail->Photo300 = "";
        $detail->PostID = $data->PostID;
        $detail->IsShare = ($data->IsShare) ? $data->IsShare : 0;

        //convert feed type
        $feedTypes = array(
            0 => 0, // check in place
            5 => 1, // change profile status
            3 => 2, // post picture to place
            10 => 7, // post picture to neighborhood
            12 => 27, // post picture status
            7 => 6, // post text to place
            9 => 8, // post text to neighborhood
            2 => 9
        );

        $contentFeedType = -1;
        if (in_array($detail->Type, array_keys($feedTypes)) && $data->RefID) {
            $contentFeedType = $feedTypes[$detail->Type];
            /* $feedInfo = $CI->Activity_model->getContentDetail($data->RefID, $contentFeedType);
              if (!$feedInfo)
              {
              $contentFeedType = -1;
              } */
        }

        $detail->ContentFeedType = $contentFeedType;

        switch ($detail->Type) {
            case 0: //When user check in to one place
            case 4: //When user spectate to one place
                // ANOMO-4981
                if ($data->PostID) {
                    $info = $CI->Place_Post_model->findById($data->PostID);
                    $photo = isset($info->Photo) ? $info->Photo : '';
                    $detail->Photo = $photo;
                    if ($info->Photo100) {
                        $detail->Photo100 = $info->Photo100;
                    }
                    if ($info->Photo200) {
                        $detail->Photo200 = $info->Photo200;
                    }
                    if ($info->Photo300) {
                        $detail->Photo300 = $info->Photo300;
                    }
                } else {
                    $placeInfo = $CI->Place_model->getPlace($detail->PlaceID);
                    if (isset($placeInfo->Category)) {
                        $category = explode('|', $placeInfo->Category);
                        $detail->Photo = $CI->Place_Category_model->returnOneIconByName($category);
                    }
                }
                break;
            case 1: //When user comment for one wall post

                $info = $CI->Post_Comment_model->findById($data->RefID);
                $detail->Content = isset($info->Content) ? $info->Content : '';

                break;
            case 2: //When user send/receive a gift to some one else
                $info = $CI->Gift_Send_model->findById($data->RefID);
                $giftId = isset($info->GiftID) ? $info->GiftID : '';
                $giftinfo = $CI->Gift_Send_model->getGiftInfo($giftId);

                $photo = isset($giftinfo->Photo) ? $giftinfo->Photo : '';
                $detail->Photo = isset($giftinfo->Photo) ? $giftinfo->Photo : "";
                $detail->Photo194 = isset($giftinfo->Photo194) ? $giftinfo->Photo194 : "";
                $detail->Photo454 = isset($giftinfo->Photo454) ? $giftinfo->Photo454 : "";


                $sendUserId = isset($info->SendUserID) ? $info->SendUserID : '';

                $detail->IsSend = ($sendUserId == $data->UserID) ? '1' : '0';
                if ($sendUserId == $data->UserID) {
                    $detailceiveUserId = isset($info->ReceiveUserID) ? $info->ReceiveUserID : '';
                    $otherUser = $CI->User_model->getUserInfo($detailceiveUserId, 'only');
                } else {
                    $otherUser = $CI->User_model->getUserInfo($sendUserId, 'only');
                }
                $detail->GiftUserID = isset($otherUser->UserID) ? $otherUser->UserID : '';
                $detail->Username = isset($otherUser->UserName) ? $otherUser->UserName : '';
                break;
            case 3: //When user post a picture

                $info = $CI->Place_Post_model->findById($data->RefID);
                $photo = isset($info->Photo) ? $info->Photo : '';
                $detail->Photo = $photo;
                if ($info->Photo100) {
                    $detail->Photo100 = $info->Photo100;
                }
                if ($info->Photo200) {
                    $detail->Photo200 = $info->Photo200;
                }
                if ($info->Photo300) {
                    $detail->Photo300 = $info->Photo300;
                }

                break;
            case 5: //When user change his status
                $detail->Content = isset($data->Content) ? $data->Content : '';
                break;
            case 6: //Clarifying
                $detail->Content = isset($data->Content) ? $data->Content : '';
                break;
            case 7: //When user post some text to wall

                $info = $CI->Place_Post_model->findById($data->RefID);
                $detail->Content = isset($info->Content) ? $info->Content : '';

                break;

            case 8: // checkin to neighborhood

                $info = $CI->Neighborhood_model->getInfo($data->RefID);
                $detail->NeighborhoodID = $data->RefID;
                $detail->Content = isset($info->NAME) ? $info->NAME : '';
                $detail->Name = isset($info->NAME) ? $info->NAME : '';
                $detail->Photo = $CI->Place_Category_model->returnOneIconByName('neighborhood');

                break;

            case 9: //post text to neighborhood

                $info = $CI->Neighborhood_Post_model->findById($data->RefID);
                $detail->Content = isset($info->Content) ? $info->Content : '';
                if ($info) {
                    $neigborhoodInfo = $CI->Neighborhood_model->getInfo($info->NeighborhoodID);
                    if ($neigborhoodInfo) {
                        $detail->NeighborhoodID = $info->NeighborhoodID;
                        $detail->Name = isset($neigborhoodInfo->NAME) ? $neigborhoodInfo->NAME : '';
                    }
                }

                break;

            case 10: //When user post a picture -> neighborhood

                $info = $CI->Neighborhood_Post_model->findById($data->RefID);
                if ($info) {
                    $neigborhoodInfo = $CI->Neighborhood_model->getInfo($info->NeighborhoodID);
                    $detail->Name = isset($neigborhoodInfo->NAME) ? $neigborhoodInfo->NAME : '';
                    $detail->NeighborhoodID = $info->NeighborhoodID;
                }

                $photo = isset($info->Photo) ? $info->Photo : '';
                $detail->Photo = $photo;
                if ($info->Photo100) {
                    $detail->Photo100 = $info->Photo100;
                }
                $detail->Photo200 = $info->Photo200;
                if ($info->Photo200) {
                    
                }
                if ($info->Photo300) {
                    $detail->Photo300 = $info->Photo300;
                }

                break;

            case 11: //When user comment for one wall post -> neighborhood

                $info = $CI->Neighborhood_Comment_model->getInfo($data->RefID);
                if ($info) {
                    // get post
                    $postInfo = $CI->Neighborhood_Post_model->findById($info->NeighborhoodPostID);
                    if ($postInfo) {
                        $neigborhoodInfo = $CI->Neighborhood_model->getInfo($postInfo->NeighborhoodID);
                        if ($neigborhoodInfo) {
                            $detail->Name = isset($neigborhoodInfo->NAME) ? $neigborhoodInfo->NAME : '';
                            $detail->NeighborhoodID = $postInfo->NeighborhoodID;
                        }
                    }
                }
                $detail->Content = isset($info->Content) ? $info->Content : '';

                break;
            case 12: // post picture activity

                $info = $CI->Picture_Post_Activity_model->findById($data->RefID);
                if ($info) {
                    $detail->Content = $info->PictureCaption;
                    $photo = isset($info->Photo) ? $info->Photo : '';
                    $detail->Photo = $photo;
                    if ($info->Photo100) {
                        $detail->Photo100 = $info->Photo100;
                    }
                    if ($info->Photo200) {
                        $detail->Photo200 = $info->Photo200;
                    }
                    if ($info->Photo300) {
                        $detail->Photo300 = $info->Photo300;
                    }
                }

                break;
        }


        $ret = $this->db->insert('placeactivitydetail', $detail);
        return $ret;
    }

    function getActivityBy($refID, $type) {
        $sql = "select * from placeactivity where RefID=$refID AND Type=$type AND (IsShare is null OR IsShare=0)";
        $query = $this->db->query($sql);
        return $query->row();
    }

}

?>