<?php
class Topic_model extends CI_Model {
    private $photoUrl = '';
    private $photoStreamUrl = '';
    
    function __construct() {
        parent::__construct();
        $this->photoStreamUrl = $this->config->item('s3_stream_url');
        $this->photoUrl = $this->config->item('s3_photo_url');
        $this->db_slave = $this->load->database('slave', TRUE);
    }
    
    function add($data)
    {        
        $this->db->insert('topic', $data);
        return $this->db->insert_id();
    }
    
    function update($data,$topic_id){
        return $this->db->update('topic',$data,array('TopicID' => $topic_id));
    }
    
    
    function increasePost($topic_id){
        $this->db->set('TotalPost','TotalPost+1',false);
        $this->db->set('LastModified',gmdate('Y-m-d H:i:s'));
        $this->db->where('TopicID',$topic_id);
        $this->db->update('topic');
    }
    
    function decreasePost($topic_id){
        $this->db->set('TotalPost','TotalPost-1',false);
        $this->db->set('LastModified',gmdate('Y-m-d H:i:s'));
        $this->db->where('TopicID',$topic_id);
        $this->db->update('topic');
    }
    
    function getListFavorite($user_id, &$total = 0, $offset = 0, $limit = 0){
        $sqlTotal = "select (select count(TopicID) as Total from topicfavorite where UserID = $user_id)
                            + (select count(TopicID) as Total from topic where UserID = $user_id AND IsDelete != 1)
                        as Total";
        $queryTotal = $this->db->query($sqlTotal);
        $row = $queryTotal->row();
        $total = $row? $row->Total : 0;
        $result = array();
        if($total > 0){
            $sql = "select * from (
                        select t.TopicID, t.TopicName, t.UserID, t.Desc, t.CreatedDate, t.TotalPost
                        , IF (Photo != '', concat('$this->photoStreamUrl', Photo), '') as Photo
                        , IF (Photo100 != '', concat('$this->photoStreamUrl', Photo100), '') as Photo100
                        , 1 as IsFavorite
                        from topic t
                        where t.UserID = $user_id AND t.IsDelete != 1
                        order by t.TotalPost DESC
                    ) as temp
                    UNION (  
                        select t.TopicID, t.TopicName, t.UserID, t.Desc, t.CreatedDate, t.TotalPost
                        , IF (Photo != '', concat('$this->photoStreamUrl', Photo), '') as Photo
                        , IF (Photo100 != '', concat('$this->photoStreamUrl', Photo100), '') as Photo100
                        , 1 as IsFavorite
                        from (select TopicID from topicfavorite where UserID = $user_id) as tf
                        inner join topic t ON tf.TopicID = t.TopicID
                        order by t.TotalPost DESC
                    )
                    limit $offset, $limit
                    ";
            $query = $this->db->query($sql);
            $result = $query->result();
        }
        return $result;
    }
    
    function getListTopicByCategory($user_id, $cate_id, &$total = 0, $offset = 0, $limit = 0){
        $where = "IsDelete != 1 ";
        if ($cate_id > 0){
            $where .= " AND CateID = $cate_id";
        }
        $sqlTotal = "SELECT count(TopicID) as Total from topic t where $where";
        $queryTotal = $this->db->query($sqlTotal);
        $row = $queryTotal->row();
        $total = $row? $row->Total : 0;
        $result = array();
        if($total > 0){
            $sql = "SELECT t.TopicID, t.TopicName, t.UserID, t.Desc, t.CreatedDate, t.TotalPost
                    , IF (Photo != '', concat('$this->photoStreamUrl', Photo), '') as Photo
                    , IF (Photo100 != '', concat('$this->photoStreamUrl', Photo100), '') as Photo100
                    , IF (tf.TopicID is not null,1,0) as IsFavorite
                    FROM (select * from topic where $where  ORDER BY TotalPost desc, TopicID desc
                            LIMIT $offset,$limit)t
                    LEFT JOIN topicfavorite tf ON t.TopicID = tf.TopicID and tf.UserID = $user_id
                    ";
            $query = $this->db->query($sql);
            $result = $query->result();
        }
        return $result;
    }
    
    function getTopicInfo($topic_id){
        $this->db->select ('*');
        $this->db->from('topic');
        $this->db->where('TopicID', $topic_id);
        $this->db->where('IsDelete !=', 1);
        $query = $this->db->get();
        return $query->row();
    }
    
    function isFavorite($user_id, $topic_id){
        $this->db->select('*');
        $this->db->from('topicfavorite');
        $this->db->where('UserID', $user_id);
        $this->db->where('TopicID', $topic_id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addFavorite($user_id, $topic_id){
        $data = array('UserID' => $user_id,
                       'TopicID' => $topic_id,
                       'CreatedDate' => gmdate('Y-m-d H:i:s'));
        $this->db->insert('topicfavorite', $data);
        return $this->db->insert_id();
    }
    
    function removeFavorite($user_id, $topic_id){
        return $this->db->delete('topicfavorite', array('UserID' => $user_id, 'TopicID' => $topic_id));
    }
    
    function searchTopic($user_id, $keyword, $cate_id = 0, &$total = 0, $offset = 0, $limit = 0){
        $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
        $keyword = mysqli_real_escape_string($con, $keyword);  
        
        $where = " t.IsDelete != 1 AND CateID > 0 ";
        if ($cate_id > 0){
            $where .= " AND t.CateID = $cate_id";
        }
        $where .= " AND t.TopicName like '%$keyword%' ";
        
        $totalSql = "select count(TopicID) as Total from topic t where $where ";
        $totalQuery = $this->db->query($totalSql);
        $row = $totalQuery->row();
        $total = $row?$row->Total:0;
        $result = array();
        if ($total > 0){
            $sql = "select t.TopicID, t.TopicName, t.Desc, t.TotalPost, t.CreatedDate, t.UserID, t.CateID
                    , IF (Photo != '', concat('$this->photoStreamUrl', Photo), '') as Photo
                    , IF (Photo100 != '', concat('$this->photoStreamUrl', Photo100), '') as Photo100
                    , IF (tf.TopicID is not null, 1, 0) as IsFavorite
                    from (select * from topic t where $where
                            order by t.TopicName = '$keyword' desc
                            limit $offset, $limit 
                          ) as t
                    left join topicfavorite tf ON t.TopicID = tf.TopicID AND tf.UserID = $user_id 
                    ";
            $query = $this->db->query($sql);
            $result = $query->result();
        }
        return $result;
    }
    
    function getTopicDetail($user_id, $topic_id){
        $sql = "select t.TopicID, t.TopicName, t.Desc, t.TotalPost, t.CreatedDate, t.UserID, t.CateID, tc.CateName
                    , IF (u.UserName is not null, u.UserName, '') as UserName
                    , IF (t.Photo != '', concat('$this->photoStreamUrl', t.Photo), '') as Photo
                    , IF (t.Photo100 != '', concat('$this->photoStreamUrl', t.Photo100), '') as Photo100
                    , IF (u.Photo != '', concat('$this->photoUrl', u.Photo), '') as Avatar
                    , IF (tf.TopicID is not null, 1, 0) as IsFavorite
                from topic t
                left join topiccategory tc ON t.CateID = t.CateID
                left join topicfavorite tf ON t.TopicID = tf.TopicID AND tf.UserID = $user_id
                left join user u ON t.UserID = u.UserID
                where t.TopicID = $topic_id AND t.IsDelete != 1
                ";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    function deleteTopic($topic_id, $user_id = 0){
        $ret = 0;
        $this->db->trans_begin();
        
        // delete topicfavorite
        $this->db->delete('topicfavorite', array('TopicID' => $topic_id));
        
        // delete notifications have been relating this topic
        $sql = "select RefID, Type from activity where TopicID = $topic_id";
        $query = $this->db->query($sql);
        $result = $query->result();
        if ($result){
            $cond = array();
            $aContentId = array();
            foreach($result as $item){
                $cond[] = "(ContentID = $item->RefID AND ContentType = $item->Type)";
                $aContentId[$item->Type][] = $item->RefID;
            }
            
            if (sizeof($aContentId) > 0){
                foreach ($aContentId as $type => $value){
                    switch($type){
                        case ACTIVITY_POST_STATUS:
                            $this->db->query("update userprofilestatus SET IsDeleted = 1 where ID IN (".implode(',', array_values($value)).")");
                            break;
                        case ACTIVITY_POST_PICTURE:
                            $this->db->query("update picturepostactivity SET IsDeleted = 1 where ID IN (".implode(',', array_values($value)).")");
                            break;
                        case ACTIVITY_POST_VIDEO:
                            $this->db->query("update video SET IsDeleted = 1 where ID IN (".implode(',', array_values($value)).")");
                            break;
                        
                    }
                }
            }
            
            if (sizeof($cond) > 0){
                $deleteNotifySql = "delete from notificationhistory where ".implode(' OR ', $cond);
                $this->db->query($deleteNotifySql);
            }
        }
        
        // inactive all post belong this topic
        $this->db->update('activity', array('IsInvalid' => 1), array('TopicID' => $topic_id));
        $this->db->update('activity_archive', array('IsInvalid' => 1), array('TopicID' => $topic_id));
        
        // inactive this topic
        $this->db->update('topic', array('IsDelete' => 1), array('TopicID' => $topic_id));
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
            $ret = 1;
        }
        return $ret;
    }
    
    function checkExistTopic($cate_id,$topic_name,$topic_id = 0){
        $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
        $topic_name = mysqli_real_escape_string($con, $topic_name);  
        $otherTopic = '';
        if($topic_id){
            $otherTopic = " AND TopicID != $topic_id ";
        }
        $sql = "SELECT * from topic where CateID = $cate_id $otherTopic AND TopicName like '$topic_name' AND IsDelete != 1 limit 1 ";        
        $query = $this->db->query($sql);
        $row =  $query->row();
        if($row){
            return $row;
        }
        return false;
    }
    
    function getTopicDescription($topic_id){
        $sql = " select
             TopicID as ActivityID
            , TopicID
            , `Desc` as Message
            , TopicName
            , '32' as Type
            , '32' as ActionType
            , CreatedDate
            , IF (u.UserName is not null, u.UserName, '') as FromUserName
            , IF (u.UserID is not null, u.UserID, '') as FromUserID
            , IF (u.BirthDate is not null, u.BirthDate, '') as BirthDate
            , IF (u.Photo != '', CONCAT('$this->photoUrl',u.Photo), '') AS Avatar
            , IF (t.Photo != '', CONCAT('$this->photoStreamUrl',t.Photo), '') AS Photo
            , IF (t.Photo100 != '', CONCAT('$this->photoStreamUrl',t.Photo100), '') AS Photo100
            , IF (nbh.OGR_FID is not null,  nbh.OGR_FID, '') AS NeighborhoodID
            , if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                           if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
           )AS NeighborhoodName
        from topic t
        left join user u ON u.UserID = t.UserID
        LEFT JOIN neighborhood nbh ON u.NeighborhoodID = nbh.OGR_FID
        where t.TopicID = $topic_id";
        $query = $this->db->query($sql);
        return $query->row();     
    }
    
    function isCloseTopic($user_id, $topic_id){
        $this->db->select('*');
        $this->db->from('user_topic_activity');
        $this->db->where('UserID', $user_id);
        $this->db->where('TopicID', $topic_id);
        $this->db->where('CloseTopic', 1);
        $query = $this->db->get();
        return $query->row();
    }
    
    function closeTopic($user_id, $topic_id){
        $data = array(
          'UserID'     => $user_id,
          'TopicID'     => $topic_id,
          'CloseTopic'  => 1,
          'CreatedDate' => gmdate('Y-m-d H:i:s')
        );
        $this->db->insert('user_topic_activity', $data);
    }
   
    function findTopic($user_id, $keyword, $cate_id = 0){
        $con = mysqli_connect($this->db_slave->hostname, $this->db_slave->username, $this->db_slave->password);
        $keyword = mysqli_real_escape_string($con, $keyword);
        $limit = 3;
        $where = ' IsDelete != 1 AND CateID > 0';
        if ($cate_id > 0){
            $where .= " AND CateID = $cate_id ";
        }  
        $sql = "select t.TopicID, TopicName, TotalPost, CateID, t.UserID, t.Desc
                , IF (Photo != '', concat('$this->photoStreamUrl', Photo), '') as Photo
                , IF (Photo100 != '', concat('$this->photoStreamUrl', Photo100), '') as Photo100
                , IF (tf.UserID is not null, 1, 0) as IsFavorite
                from (select * from topic where $where AND TopicName like '%". $keyword ."%' 
                        order by TopicName = '$keyword' desc limit $limit
                        ) t 
                left join topicfavorite tf ON  tf.TopicID = t.TopicID AND tf.UserID = $user_id
                " ;
        $query = $this->db_slave->query($sql);
        return $query->result();
    }
    

    function banUser($topic_id, $user_id){
        $data = array('UserID' => $user_id,
                       'TopicID' => $topic_id,
                       'CreatedDate' => gmdate('Y-m-d H:i:s'));
        $this->db->insert('topicban', $data);
        
        // remove favorite
        $this->db->query("delete from topicfavorite where UserID = $user_id AND TopicID = $topic_id");
    }
    
    function unBanUser($topic_id, $user_id){
        return $this->db->delete('topicban', array('UserID' => $user_id, 'TopicID' => $topic_id));
    }
    
    function isBan($topic_id, $user_id){
        $this->db->select('*');
        $this->db->from('topicban');
        $this->db->where('UserID', $user_id);
        $this->db->where('TopicID', $topic_id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function checkValidTopic($topic_id, $user_id){
        $sql = "select t.*
                , IF (tb.TopicID is not null,1,0) as IsTopicBan
               from topic t
               left join topicban tb on t.TopicID = tb.TopicID AND tb.UserID = $user_id
               where t.TopicID = $topic_id AND t.IsDelete = 0";
        $query = $this->db->query($sql);
        return $query->row();
        
    }
    
    function getTotalTopicCreatedByUser($user_id){
        $sql = "select count(TopicID) as Total from topic where IsDelete = 0 AND UserID = $user_id";
        $query = $this->db->query($sql);
        $row = $query->row();
        return $row ? $row->Total: 0;
    }
    
    
    function getListPostInTopic($cond, &$total, $limit = 0, $offset = 0) {
        if (!isset($cond['TopicID'])) return null;
            
        $where = " IsInvalid != 1 AND TopicID = ".$cond['TopicID'];
        $sqlTotal = "select count(ActivityID) as Total from activity where $where";
        $totalResult = $this->db->query($sqlTotal);
        $totalRow = $totalResult->row();
        $total = $totalRow?$totalRow->Total:0;
        if ($total > 0){
            $sql = "select a.* 
                    , IF (a.Image != '', concat('$this->photoUrl', a.Image), '') as Image
                    , IF (u.Photo != '', concat('$this->photoUrl', u.Photo), '') as Avatar
                    , u.UserName
                    , t.TopicName
                    from (
                        select * from activity where  $where
                        order by CreatedDate DESC limit $offset, $limit
                         ) as a
                    left join user u ON u.UserID = a.FromUserID
                    left join topic t ON t.TopicID = a.TopicID
                    ";

            $query1 = $this->db->query($sql);
            return $query1->result();
        }
        return null;
    }
    
}
?>
