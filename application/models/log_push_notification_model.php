<?php

class Log_Push_Notification_model extends CI_Model {

    function add($data) {
        $this->db->insert('logpushnotification', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('logpushnotification', $data, $cond);
    }

    function getListPushByDate($cond, $from, $to, $getSumNumber) {
        $this->db->select('Platform, CreatedDate, count(ID) as Number');
        $this->db->from('logpushnotification');
        $this->db->where('Status', 1);
        if (count($cond) > 0) {
            if (isset($cond['sort'])) {
                switch ($cond['sort']) {
                    case 'date-asc':
                        $this->db->order_by('CreatedDate', 'asc');
                        break;
                    case 'date-desc':
                        $this->db->order_by('CreatedDate', 'desc');
                        break;
                    case 'total-asc':
                        $this->db->order_by('Number', 'asc');
                        break;
                    case 'total-desc':
                        $this->db->order_by('Number', 'desc');
                        break;
                }
            }
        }
        $this->db->group_by('DATE(CreatedDate), Platform');
        $this->db->order_by('CreatedDate', 'DESC');
        if ($getSumNumber) {
            $query = $this->db->get();
            return $query->num_rows();
        } else {
            $this->db->limit($from, $to);
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? $query->result() : null;
        }
    }

}

?>