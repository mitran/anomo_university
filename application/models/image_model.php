<?php

class Image_model extends CI_Model {

    public $error_msg = '';

    function __construct() {
        parent::__construct();
    }

    function do_upload($field_name) {
        $this->load->library('upload');
        $config['upload_path'] = $this->config->item('photo_url');
        $config['allowed_types'] = 'png|gif|jpg|jpeg';
        $config['max_size'] = '1000'; // 1000kb
        $config['max_width'] = '0'; // no limit px
        $config['max_height'] = '0'; // no limit px
        $config['encrypt_name'] = TRUE;
        $config['remove_spaces'] = TRUE;
        $this->upload->initialize($config);
        $_FILES[$field_name]['name'] = preg_replace('/([^\w|\d\.]+)/i', '_', $_FILES[$field_name]['name']);
        if ($this->upload->do_upload($field_name)) {
            $data = $this->upload->data();
            $return['file_name'] = $data['file_name'];
            $return['error'] = false;
        } else {
            $return['error'] = true;
            $return['error_msg'] = $this->upload->display_errors();
        }
        return $return;
    }

    function resize($file_name = '', $width = 120, $height = 100) {
        $upload_path = $this->config->item('photo_url');
        $file_path = $upload_path . $file_name;
        if (!file_exists($file_path)) {
            return base_url() . $this->config->item('photo_url') . 'default.jpg';
        }
        $new_file_name = substr($file_name, 0, strrpos($file_name, '.')) . '_' . $width . 'x' . $height . substr($file_name, strrpos($file_name, '.'));
        if (file_exists($upload_path . $new_file_name)) {
            return base_url() . $this->config->item('photo_url') . $new_file_name;
        }
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = $file_path;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $width;
        $config['height'] = $height;
        $config['new_image'] = $upload_path . $new_file_name;
        $this->image_lib->initialize($config);
        if ($this->image_lib->resize()) {
            return base_url() . $this->config->item('photo_url') . $new_file_name;
        }
        return base_url() . $this->config->item('photo_url') . 'default.jpg';
    }

    function delete_image($file_name) {
        $upload_path = FCPATH . $this->config->item('upload_path');
        $file_path = $upload_path . $file_name;
        if (file_exists($file_path)) {
            @unlink($file_path);
        }
    }

    function create_thumbnail($file_name, $width, $height) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = FCPATH . $this->config->item('photo_url') . $file_name;
        $config['create_thumb'] = TRUE;
        $config['thumb_marker'] = '_thumb_' . $width . 'x' . $height;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $width;
        $config['height'] = $height;
        $this->load->library('image_lib');
        $this->image_lib->initialize($config);
        $ret = '';
        if ($this->image_lib->resize()) {
            $xp = $this->image_lib->explode_name($file_name);
            $ret = $xp['name'] . $config['thumb_marker'] . $xp['ext'];
        }
        $this->image_lib->clear();
        return $ret;
    }

}

?>