<?php

class Dashboard_model extends CI_Model {

    private $timezone = '- 7 hour'; // PST time
    private $pst_start = '';
    private $pst_end = '';

    function __construct() {
        parent::__construct();
        //date_default_timezone_set('America/Los_Angeles');
        $pst = gmdate('Y-m-d', strtotime($this->timezone)); //GMT date
        //date_default_timezone_set('America/Los_Angeles');
        //$pst = date('Y-m-d');
        //$pst_start = date('Y-m-d H:i:s', strtotime($this->timezone, strtotime($pst.'00:00:00')));
        //$pst_end = date('Y-m-d H:i:s', strtotime($this->timezone, strtotime($pst.'23:59:59')));
        $pst_start = date('Y-m-d H:i:s', strtotime($pst . '07:00:00'));
        $tmp_date = strtotime($pst . '06:59:59');
        $pst_end = date('Y-m-d H:i:s', strtotime('+1 days', $tmp_date));
        $this->pst_start = $pst_start;
        $this->pst_end = $pst_end;
    }

    function getTotalResgisterUser($time = 'all', $type = '') {
        $this->db->select('count(UserID) as Total');
        $this->db->from('user');
        if ($time == 'month') {
            $this->db->where('MONTH(SignUpDate)', gmdate('m', strtotime($this->timezone)));
        } elseif ($time == 'day') {
            $this->db->where("DATE_FORMAT(SignUpDate, '%Y-%m-%d %H:%i:%s') BETWEEN '" . $this->pst_start . "' AND '" . $this->pst_end . "'");
            //$this->db->where("DATE_FORMAT(SignUpDate, '%Y-%m-%d %H:%i:%s ')>='".$this->pst_start."'");
            //$this->db->where("DATE_FORMAT(SignUpDate, '%Y-%m-%d %H:%i:%s ')<='".$this->pst_end."'");
        } elseif ($time == 'week') {
            $this->db->where("SignUpDate >= '" . gmdate('Y-m-d H:i:s', strtotime("- 1 week")) . "'");
        }
        if ($type == 'verified') {
            $this->db->where('FacebookID > 0');
        } elseif ($type == 'unverified') {
            $this->db->where("FacebookID = ''");
        }
        $query = $this->db->get();
        /* if ($time == 'day'){
          echo "<pre>Pp: "; print_r($this->db); echo "</pre>"; die();
          } */
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getTotalUser($type = 'ios', $time = 'all') {
        //SELECT u.UserName, u.UserID, d.`Status`, d.Type FROM user u, devices d WHERE (DATE_FORMAT(SignUpDate, '%Y-%m-%d %H:%i:%s') BETWEEN '2013-07-01 07:00:00' AND '2013-07-02 06:59:00') AND u.UserID = d.UserID AND d.Type LIKE 'ios' AND d.`Status` LIKE 'active'
        $this->db->select('count(u.UserID) as Total');
        $this->db->from('user as u');
        $this->db->from('devices as d');
        if ($time == 'month') {
            $this->db->where('MONTH(u.SignUpDate)', gmdate('m', strtotime($this->timezone)));
        } elseif ($time == 'day') {
            $this->db->where("DATE_FORMAT(u.SignUpDate, '%Y-%m-%d %H:%i:%s') BETWEEN '" . $this->pst_start . "' AND '" . $this->pst_end . "'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')>='".$this->pst_start."'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')<='".$this->pst_end."'");
        } elseif ($time == 'week') {
            $this->db->where("u.SignUpDate >= '" . gmdate('Y-m-d H:i:s', strtotime("- 1 week")) . "'");
        }
        $this->db->where('u.UserID = d.UserID');
        $this->db->where('d.Type', $type);
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getTotalGame($time = 'all') {
        $this->db->select('count(AnomotionID) as Total');
        $this->db->from('anomotion');
        if ($time == 'month') {
            $this->db->where('MONTH(CreatedDate)', gmdate('m', strtotime($this->timezone)));
        } elseif ($time == 'day') {
            $this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s') BETWEEN '" . $this->pst_start . "' AND '" . $this->pst_end . "'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')>='".$this->pst_start."'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')<='".$this->pst_end."'");
        } elseif ($time == 'week') {
            $this->db->where("CreatedDate >= '" . gmdate('Y-m-d H:i:s', strtotime("- 1 week")) . "'");
        }
        $this->db->where('Status', 3);
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getTotalChat($time = 'all', $exclude_user_id = 0) {
        $this->db->select('count(conversationID) as Total');
        $this->db->from('ofconversation');
        $this->db->where('lastMessage is not null and lastMessage != "" and lastMessage != "filter_request_game"');
        if ($time == 'month') {
            $this->db->where('MONTH(FROM_UNIXTIME(lastActivity/1000))', gmdate('m', strtotime($this->timezone)));
        } elseif ($time == 'day') {
            $this->db->where("FROM_UNIXTIME(lastActivity/1000) BETWEEN '" . $this->pst_start . "' AND '" . $this->pst_end . "'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')>='".$this->pst_start."'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')<='".$this->pst_end."'");
        } elseif ($time == 'week') {
            $this->db->where("FROM_UNIXTIME(lastActivity/1000) >= '" . gmdate('Y-m-d H:i:s', strtotime("- 1 week")) . "'");
        }
        if ($exclude_user_id > 0) {

            $exclude_user_id = MASS_MESSAGE_SENDER_IDS;
            $where = "conversationID NOT IN (select conversationID from ofconparticipant where SUBSTRING_INDEX(bareJID,'@',1) IN ( $exclude_user_id))";
            $this->db->where($where);
        }
        $query = $this->db->get();

        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getTotalChatMsg($time = 'all', $exclude_user_id = 0, $type = '') {
        $this->db->select('count(conversationID) as Total');
        $this->db->from('ofmessagearchive');
        $this->db->where("body NOT IN ('filter_request_game',
                                            'filter_block_user',
                                            'filter_unblock_user',
                                            'filter_message_status_typing',
                                            'filter_message_status_endtyping',
                                            'filter_message_status_delivered')");
        if ($type == 'picture'){
            $this->db->where("SUBSTRING(body,1,16)='filter_image_tag'");
        }
        if ($time == 'month') {
            $this->db->where('MONTH(FROM_UNIXTIME(sentDate/1000))', gmdate('m', strtotime($this->timezone)));
        } elseif ($time == 'day') {
            $this->db->where("DATE_FORMAT(FROM_UNIXTIME(sentDate/1000), '%Y-%m-%d %H:%i:%s') BETWEEN '" . $this->pst_start . "' AND '" . $this->pst_end . "'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')>='".$this->pst_start."'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')<='".$this->pst_end."'");
        } elseif ($time == 'week') {
            $this->db->where("FROM_UNIXTIME(sentDate/1000) >= '" . gmdate('Y-m-d H:i:s', strtotime("- 1 week")) . "'");
        }
        if ($exclude_user_id > 0) {
            $where = "conversationID NOT IN (select conversationID from ofconparticipant where SUBSTRING_INDEX(bareJID,'@',1)= $exclude_user_id)";
            $this->db->where($where);
        }
//		$this->db->where('IsDelivered', 1);
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getTotalGiftSent($time = 'all') {
        $this->db->select('count(ID) as Total');
        $this->db->from('giftsend');
        if ($time == 'month') {
            $this->db->where('MONTH(CreatedDate)', gmdate('m', strtotime($this->timezone)));
        } elseif ($time == 'day') {
            $this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s') BETWEEN '" . $this->pst_start . "' AND '" . $this->pst_end . "'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')>='".$this->pst_start."'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')<='".$this->pst_end."'");
        } elseif ($time == 'week') {
            $this->db->where("CreatedDate >= '" . gmdate('Y-m-d H:i:s', strtotime("- 1 week")) . "'");
        }
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getTotalReveal($time = 'all', $from = '', $to = '') {
        $this->db->select('count(ID) as Total');
        $this->db->from('reveal');
        if ($time == 'month') {
            $this->db->where('MONTH(CreatedDate)', gmdate('m', strtotime($this->timezone)));
        } elseif ($time == 'day') {
            $this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s') BETWEEN '" . $this->pst_start . "' AND '" . $this->pst_end . "'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')>='".$this->pst_start."'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')<='".$this->pst_end."'");
        } elseif ($time == 'week') {
            $this->db->where("CreatedDate >= '" . gmdate('Y-m-d H:i:s', strtotime("- 1 week")) . "'");
        }

        if ($from != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'");
        }
        if ($to != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'");
        }
        $this->db->where('Status', 1);
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getTotalCheckin($time = 'all', $from = '', $to = '') {
        $this->db->select('count(ID) as Total');
        $this->db->from('usercheckin');
        if ($time == 'month') {
            $this->db->where('MONTH(CheckOutDate)', gmdate('m', strtotime($this->timezone)));
        } elseif ($time == 'day') {
            $this->db->where("DATE_FORMAT(CheckOutDate, '%Y-%m-%d %H:%i:%s') BETWEEN '" . $this->pst_start . "' AND '" . $this->pst_end . "'");
            //$this->db->where("DATE_FORMAT(CheckOutDate, '%Y-%m-%d %H:%i:%s ')>='".$this->pst_start."'");
            //$this->db->where("DATE_FORMAT(CheckOutDate, '%Y-%m-%d %H:%i:%s ')<='".$this->pst_end."'");
        } elseif ($time == 'week') {
            $this->db->where("CheckOutDate >= '" . gmdate('Y-m-d H:i:s', strtotime("- 1 week")) . "'");
        }
        if ($from != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(CheckOutDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'");
        }
        if ($to != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(CheckOutDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'");
        }
        $this->db->where('Status', 1);
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getTotalUserBlocked() {
        $this->db->select('count(*) as Total');
        $this->db->from('userblock');
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getTotalFavorite() {
        $this->db->select('count(*) as Total');
        $this->db->from('userconnect');
        $this->db->where('IsFavorite', 1);
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getTotalFbShares($time = 'all') {
        $this->db->select('count(UserID) as Total');
        $this->db->from('user');
        if ($time == 'month') {
            $this->db->where('MONTH(SharedLatestDate)', gmdate('m', strtotime($this->timezone)));
        } elseif ($time == 'day') {
            $this->db->where("DATE_FORMAT(SharedLatestDate, '%Y-%m-%d %H:%i:%s') BETWEEN '" . $this->pst_start . "' AND '" . $this->pst_end . "'");
            //$this->db->where("DATE_FORMAT(SharedLatestDate, '%Y-%m-%d %H:%i:%s ')>='".$this->pst_start."'");
            //$this->db->where("DATE_FORMAT(SharedLatestDate, '%Y-%m-%d %H:%i:%s ')<='".$this->pst_end."'");
        } elseif ($time == 'week') {
            $this->db->where("SharedLatestDate >= '" . gmdate('Y-m-d H:i:s', strtotime("- 1 week")) . "'");
        }
        $this->db->where('Shared', 1);
        $query = $this->db->get();

        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getPictureChat($time = 'all') {
        $this->db->select('count(ID) as Total');
        $this->db->from('userchatmessages');
        $this->db->where('Photo !=', "");
        if ($time == 'month') {
            $this->db->where('MONTH(CreatedDate)', gmdate('m', strtotime($this->timezone)));
        } elseif ($time == 'day') {
            $this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s') BETWEEN '" . $this->pst_start . "' AND '" . $this->pst_end . "'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')>='".$this->pst_start."'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')<='".$this->pst_end."'");
        } elseif ($time == 'week') {
            $this->db->where("CreatedDate >= '" . gmdate('Y-m-d H:i:s', strtotime("- 1 week")) . "'");
        }
        $query = $this->db->get();

        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getPictureActivity($time = 'all') {
        $this->db->select('count(ID) as Total');
        $this->db->from('picturepostactivity');
        if ($time == 'month') {
            $this->db->where('MONTH(CreatedDate)', gmdate('m', strtotime($this->timezone)));
        } elseif ($time == 'day') {
            $this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s') BETWEEN '" . $this->pst_start . "' AND '" . $this->pst_end . "'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')>='".$this->pst_start."'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')<='".$this->pst_end."'");
        } elseif ($time == 'week') {
            $this->db->where("CreatedDate >= '" . gmdate('Y-m-d H:i:s', strtotime("- 1 week")) . "'");
        }
        $query = $this->db->get();

        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getTotalTimeFbShare($time = 'all', $type = '') {
        $this->db->select('count(ID) as Total');
        $this->db->from('fbshare');
        if ($time == 'month') {
            $this->db->where('MONTH(CreatedDate)', gmdate('m', strtotime($this->timezone)));
        } elseif ($time == 'day') {
            $this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s') BETWEEN '" . $this->pst_start . "' AND '" . $this->pst_end . "'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')>='".$this->pst_start."'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')<='".$this->pst_end."'");
        } elseif ($time == 'week') {
            $this->db->where("CreatedDate >= '" . gmdate('Y-m-d H:i:s', strtotime("- 1 week")) . "'");
        }
        if ($type == 'first') {
            $this->db->where('IsFirst', 1);
        }
        $query = $this->db->get();

        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function totalUserRegister($from = '', $to = '', $type = 'all', $timezone = '') {
        $this->db->select('count(UserID) as Total');
        $this->db->from('user');
        if ($from != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'");
        }
        if ($to != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'");
        }
        if ($type == 'verified') {
            $this->db->where('FacebookID > 0');
        } elseif ($type == 'unverified') {
            $this->db->where("FacebookID = ''");
        }

        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function totalByDevices($from = '', $to = '', $type = 'all') {
        $this->db->select('count(ID) as Total');
        $this->db->from('devices');
        if ($from != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'");
        }
        if ($to != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'");
        }
        if ($type != 'all') {
            $this->db->where('Type', $type);
        }
        $this->db->where('Status', 'active');

        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function totalOther($from = '', $to = '', $type = '', $time = '') {
        $tables = array(
            'Anomotion' => 'anomotion',
            'UserChat' => 'ofconversation',
            'UserChatMessages' => 'ofmessagearchive',
            'GiftSend' => 'giftsend',
            'ActivityLike' => 'activitylike',
            'PicturePostActivity' => 'picturepostactivity',
            'ActivityComment' => 'activitycomment',
            'PostComment' => 'postcomment',
            'NeighborhoodComment' => 'neighborhoodcomment',
            'Status' => 'userprofilestatus',
            'Activity' => 'activity',
            'Post' => 'activity',
            'AppShare' => 'fbshare'
        );
        if ($type != '' && in_array($type, array_keys($tables))) {
            $createdDate = 'CreatedDate';

            $this->db->select('count(*) as Total');
            $this->db->from($tables[$type]);

            if ($from != '') {
                $this->db->where("DATE_FORMAT(DATE_SUB($createdDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'");
            }
            if ($to != '') {
                $this->db->where("DATE_FORMAT(DATE_SUB($createdDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'");
            }
            if ($time == 'day') {
                $this->db->where("DATE_FORMAT($createdDate, '%Y-%m-%d %H:%i:%s') BETWEEN '" . $this->pst_start . "' AND '" . $this->pst_end . "'");
            } elseif ($time == 'month') {
                $this->db->where("MONTH($createdDate)", gmdate('m', strtotime($this->timezone)));
            } elseif ($time == 'week') {
                $this->db->where("$createdDate >= '" . gmdate('Y-m-d H:i:s', strtotime("- 1 week")) . "'");
            }
            if ($type == 'Activity') {
                $this->db->where("ActionType != 28");
                $this->db->where("IsInvalid", 0);
            }
            if ($type == 'Post') {
                $this->db->where("ActionType != 28");
                $this->db->where("IsInvalid", 0);
            }

            if ($type == 'Anomotion') {
                $this->db->where('Status', 3);
            }



            $query = $this->db->get();
            $row = $query->row();
            return ($row) ? $row->Total : 0;
        }
    }

    function getListMetrics(&$total, $from = 0, $to = 0) {
        $sql = "select * from advancemetrics";
        $query = $this->db->query($sql);
        $total = $query->num_rows();
        $sql .= " order by ID DESC";
        $sql .= " limit $to, $from";
        $query1 = $this->db->query($sql);
        return $query1->result();
    }

    function addMetrics($data) {
        $this->db->insert('advancemetrics', $data);
        return $this->db->insert_id();
    }

    function getTotalPicturePosted($type, $time = "") {
        $this->db->select('count(*) as Total');
        if ($type == 'neighborhood')
            $this->db->from('neighborhoodpost');
        else
            $this->db->from('placepost');
        $this->db->where("Photo != ''");
        if ($time == 'day') {
            $this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s') BETWEEN '" . $this->pst_start . "' AND '" . $this->pst_end . "'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')>='".$this->pst_start."'");
            //$this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s ')<='".$this->pst_end."'");
        } elseif ($time == 'month') {
            $this->db->where('MONTH(CreatedDate)', gmdate('m', strtotime($this->timezone)));
        } elseif ($time == 'week') {
            $this->db->where("CreatedDate >= '" . gmdate('Y-m-d H:i:s', strtotime("- 1 week")) . "'");
        }
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    /* ANOMO-5609
     * in the last 6 hours
      in the last 12 hours
      in the last 24 hours
      in the last 48 hours
      in the last week.
     *
     */

    function countActivity($time) {
        $sql = "select count(ActivityID) as NUM from activity
			where CreatedDate >= '" . gmdate('Y-m-d H:i:s', strtotime("-" . $time)) . "'";
        $query = $this->db->query($sql);
        $row = $query->row();
        return ($row) ? $row->NUM : 0;
    }

    /* Query that return actualy activity feeds count
      - For the past 6 hours
      - For the past 12 hours
      - For the past 24 hours
      - For the past 48 hours
      - For the past week
     */

    function countActivityNoComments($time) {
        $sql = "select count(ActivityID) as NUM from activity
            where CreatedDate >= '" . gmdate('Y-m-d H:i:s', strtotime("-" . $time)) . "' and (ActionType = 0 or ActionType = 1 or ActionType = 2 or ActionType = 7 or ActionType = 27 or ActionType = 6 or ActionType = 8 )";
        $query = $this->db->query($sql);
        $row = $query->row();
        return ($row) ? $row->NUM : 0;
    }

    function getTotalContentShare($share_type = 0, $time = 'all') {
        $this->db->select('count(ID) as Total');
        $this->db->from('contentshare');
        if ($time == 'month') {
            $this->db->where('MONTH(CreatedDate)', gmdate('m', strtotime($this->timezone)));
        } elseif ($time == 'day') {
            $this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s') BETWEEN '" . $this->pst_start . "' AND '" . $this->pst_end . "'");
        } elseif ($time == 'week') {
            $this->db->where("CreatedDate >= '" . gmdate('Y-m-d H:i:s', strtotime("- 1 week")) . "'");
        }
        $this->db->where('ShareType', $share_type);
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getTotalFollowerAdded($from = '', $to = '') {
        $this->db->select('count(*) as Total');
        $this->db->from('userconnect');
        if ($from != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'");
        }
        if ($to != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'");
        }
        $this->db->where('IsFavorite', 1);
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getTotalContent($user_id, $from, $to, $type, $return = 'only') {
        $tables = array(
            'ActivityLike' => 'activitylike',
            'PicturePostActivity' => 'picturepostactivity',
            'ActivityComment' => 'activitycomment',
            'PostComment' => 'postcomment',
            'NeighborhoodComment' => 'neighborhoodcomment',
            'Post' => 'activity',
            'ExtShare' => 'contentshare'
        );
        if ($type != '' && in_array($type, array_keys($tables))) {
            if ($type == 'Post') {
                $this->db->select('count(ActivityID) as Total');
            } else {
                $this->db->select('count(ID) as Total');
            }

            $this->db->from($tables[$type]);
            if ($from != '') {
                $this->db->where("DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'");
            }
            if ($to != '') {
                $this->db->where("DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'");
            }
            $field = 'UserID';
            if ($type == 'Post') {
                $field = 'FromUserID';
                $actionType = '1, 2, 7, 27, 6, 8';
                $this->db->where("ActionType IN (" . $actionType . ")");
            }
            if ($type == 'ExtShare') {
                $this->db->where('ShareType', 0);
            }

            if ($return == 'only') {
                $this->db->where($field, $user_id);
            } else {
                $this->db->where("$field !=", $user_id);
            }
            $query = $this->db->get();

            $row = $query->row();
            return ($row) ? $row->Total : 0;
        }
    }

    function totalGameOf($user_id, $from, $to) {
        $this->db->select('count(*) as Total');
        $this->db->from('anomotion');
        if ($from != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'");
        }
        if ($to != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'");
        }
        $this->db->where('UserID', $user_id);
        $this->db->or_where('TargetUserID', $user_id);
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function totalChatOf($user_id, $from, $to) {
        $this->db->select('count(*) as Total');
        $this->db->from('ofconparticipant');
        if ($from != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(FROM_UNIXTIME(joinedDate/1000), INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'");
        }
        if ($to != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(FROM_UNIXTIME(joinedDate/1000), INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'");
        }
        $this->db->where("SUBSTRING_INDEX(bareJID,'@' , 1)=", $user_id);
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function totalCheckinOf($user_id, $from, $to) {
        $this->db->select('count(*) as Total');
        $this->db->from('usercheckin');
        if ($from != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(CheckOutDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'");
        }
        if ($to != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(CheckOutDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'");
        }
        $this->db->where('UserID', $user_id);
        $this->db->group_by('PlaceID');
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getAllTotalComment($from, $to, $type, $num_day = 2) {
        $sql = '';
        if ($type == 'Post') {
            $sql = "select sum(tmp.Total)as Total
            from(
            SELECT count(*) as Total,activity.FromUserID
            FROM activity
            Inner join user on user.UserID=activity.FromUserID";
            if ($from != "") {
                $sql.=" AND DATE_FORMAT(DATE_SUB(user.SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'";
            }
            if ($to != "") {
                $sql.=" AND DATE_FORMAT(DATE_SUB(user.`SignUpDate`, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'";
            }

            $sql .= " WHERE DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>=DATE_FORMAT(DATE_SUB(user.SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s') AND DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<=DATE_FORMAT(DATE_ADD(DATE_SUB(user.SignUpDate, INTERVAL 7 HOUR ),INTERVAL " . ($num_day * 24) . " HOUR) , '%Y-%m-%d %H:%i:%s') AND ActionType IN (1, 2, 7, 27, 6, 8)
            GROUP BY activity.FromUserID
            )as tmp";
        } elseif ($type == 'ActivityLike') {
            $sql = "select sum(tmp.Total)as Total
            from(
            SELECT count(*) as Total,activitylike.UserID
            FROM activitylike
            Inner join user on user.UserID=activitylike.UserID";
            if ($from != "") {
                $sql.=" AND DATE_FORMAT(DATE_SUB(user.SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'";
            }
            if ($to != "") {
                $sql.=" AND DATE_FORMAT(DATE_SUB(user.`SignUpDate`, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'";
            }
            $sql.=" WHERE DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>=DATE_FORMAT(DATE_SUB(user.SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s') AND DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<=DATE_FORMAT(DATE_ADD(DATE_SUB(user.SignUpDate, INTERVAL 7 HOUR ),INTERVAL " . ($num_day * 24) . " HOUR) , '%Y-%m-%d %H:%i:%s') 
            GROUP BY activitylike.UserID
            )as tmp";
        } elseif ($type == "ActivityComment") {
            $sql = "select sum(tmp.Total)as Total
            from(
            SELECT count(*) as Total,user.UserID
            FROM activitycomment
            Inner join user on user.UserID=activitycomment.UserID";
            if ($from != "") {
                $sql.=" AND DATE_FORMAT(DATE_SUB(user.SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'";
            }
            if ($to != "") {
                $sql.=" AND DATE_FORMAT(DATE_SUB(user.`SignUpDate`, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'";
            }
            $sql.=" WHERE DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>=DATE_FORMAT(DATE_SUB(user.SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s') AND DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<=DATE_FORMAT(DATE_ADD(DATE_SUB(user.SignUpDate, INTERVAL 7 HOUR ),INTERVAL " . ($num_day * 24) . " HOUR) , '%Y-%m-%d %H:%i:%s') 
            GROUP BY user.UserID
            )as tmp";
        } elseif ($type == "PostComment") {
            $sql = "select sum(tmp.Total)as Total
            from(
            SELECT count(*) as Total,user.UserID
            FROM postcomment
            Inner join user on user.UserID=postcomment.UserID";
            if ($from != "") {
                $sql.=" AND DATE_FORMAT(DATE_SUB(user.SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'";
            }
            if ($to != "") {
                $sql.=" AND DATE_FORMAT(DATE_SUB(user.`SignUpDate`, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'";
            }
            $sql.=" WHERE DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>=DATE_FORMAT(DATE_SUB(user.SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s') AND DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<=DATE_FORMAT(DATE_ADD(DATE_SUB(user.SignUpDate, INTERVAL 7 HOUR ),INTERVAL " . ($num_day * 24) . " HOUR) , '%Y-%m-%d %H:%i:%s') 
            GROUP BY user.UserID
            )as tmp";
        } elseif ($type == "NeighborhoodComment") {
            $sql = "select sum(tmp.Total)as Total
            from(
            SELECT count(*) as Total,user.UserID
            FROM neighborhoodcomment
            Inner join user on user.UserID=neighborhoodcomment.UserID";
            if ($from != "") {
                $sql.=" AND DATE_FORMAT(DATE_SUB(user.SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'";
            }
            if ($to != "") {
                $sql.=" AND DATE_FORMAT(DATE_SUB(user.`SignUpDate`, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'";
            }
            $sql.=" WHERE DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>=DATE_FORMAT(DATE_SUB(user.SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s') AND DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<=DATE_FORMAT(DATE_ADD(DATE_SUB(user.SignUpDate, INTERVAL 7 HOUR ),INTERVAL " . ($num_day * 24) . " HOUR) , '%Y-%m-%d %H:%i:%s') 
            GROUP BY user.UserID
            )as tmp";
        }
        $query = $this->db->query($sql);
        $row = $query->row();
        return (($row) && $row->Total) ? $row->Total : 0;
    }

    function gettoAllTotalGameOf($from, $to, $num_day = 2) {
        $sql = '';

        $sql = "select sum(tmp.Total)as Total
        from(
        SELECT count(*) as Total,anomotion.TargetUserID
        FROM anomotion
        Inner join user on (user.UserID = anomotion.TargetUserID or user.UserID= anomotion.UserID)";
        if ($from != "") {
            $sql.=" AND DATE_FORMAT(DATE_SUB(user.`SignUpDate`, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'";
        }
        if ($to != "") {
            $sql.=" AND DATE_FORMAT(DATE_SUB(user.`SignUpDate`, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'";
        }
        $sql.=" WHERE DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>=DATE_FORMAT(DATE_SUB(user.`SignUpDate`, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s') AND DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<=DATE_FORMAT(DATE_ADD(DATE_SUB(user.`SignUpDate`, INTERVAL 7 HOUR ),INTERVAL " . ($num_day * 24) . " HOUR) , '%Y-%m-%d %H:%i:%s')
        GROUP BY anomotion.TargetUserID
        )as tmp";

        $query = $this->db->query($sql);
        $row = $query->row();
        return (($row) && $row->Total) ? $row->Total : 0;
    }

    function gettoAllTotalChatOf($from, $to, $num_day = 2) {
        $sql = '';

        $sql = "select sum(tmp.Total)as Total
        from(
        SELECT count(*) as Total,user.UserID 
        FROM ofconparticipant
        Inner join user on (user.UserID = SUBSTRING_INDEX(ofconparticipant.bareJID,'@' , 1))";
        if ($from != "") {
            $sql.=" AND DATE_FORMAT(DATE_SUB(user.`SignUpDate`, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'";
        }
        if ($to != "") {
            $sql.=" AND DATE_FORMAT(DATE_SUB(user.`SignUpDate`, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'";
        }
        $sql.=" WHERE DATE_FORMAT(DATE_SUB(FROM_UNIXTIME(joinedDate/1000), INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>=DATE_FORMAT(DATE_SUB(user.`SignUpDate`, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s') AND DATE_FORMAT(DATE_SUB(FROM_UNIXTIME(joinedDate/1000), INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<=DATE_FORMAT(DATE_ADD(DATE_SUB(user.`SignUpDate`, INTERVAL 7 HOUR ),INTERVAL " . ($num_day * 24) . " HOUR) , '%Y-%m-%d %H:%i:%s')
        GROUP BY user.UserID 
        )as tmp";

        $query = $this->db->query($sql);
        $row = $query->row();
        return (($row) && $row->Total) ? $row->Total : 0;
    }

    function gettoAllTotalCheckinOf($from, $to, $num_day = 2) {
        $sql = '';

        $sql = "select sum(tmp.Total)as Total
        from(
        SELECT count(*) as Total,user.UserID 
        FROM usercheckin
        Inner join user on (user.UserID = usercheckin.UserID)";
        if ($from != "") {
            $sql.=" AND DATE_FORMAT(DATE_SUB(user.`SignUpDate`, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'";
        }
        if ($to != "") {
            $sql.=" AND DATE_FORMAT(DATE_SUB(user.`SignUpDate`, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'";
        }
        $sql.=" WHERE DATE_FORMAT(DATE_SUB(CheckOutDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>=DATE_FORMAT(DATE_SUB(user.`SignUpDate`, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s') AND DATE_FORMAT(DATE_SUB(CheckOutDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<=DATE_FORMAT(DATE_ADD(DATE_SUB(user.`SignUpDate`, INTERVAL 7 HOUR ),INTERVAL " . ($num_day * 24) . " HOUR) , '%Y-%m-%d %H:%i:%s')
        GROUP BY user.UserID 
        )as tmp";

        $query = $this->db->query($sql);
        $row = $query->row();
        return (($row) && $row->Total) ? $row->Total : 0;
    }

    function getUserByCond($flagdate = 0, $from = '', $to = '') {
        $sql = 'select UserID,Email,UserName,SignUpDate  
        from user where Email is not NULL AND Email !=""';
        if ($flagdate == 1) {
            $sql.=' And DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(), INTERVAL 7 Day),INTERVAL 7 HOUR), "%Y-%m-%d %H:%i:%s")<=DATE_FORMAT(user.`SignUpDate`,"%Y-%m-%d %H:%i:%s")';
        }
        if ($from != '' && $to != ''){
            $sql.=" AND '$from' < DATE_FORMAT(user.`SignUpDate`,'%Y-%m-%d %H:%i:%s')";
            $sql.=" AND '$to' >= DATE_FORMAT(user.`SignUpDate`,'%Y-%m-%d %H:%i:%s')";
        }

        $arr = array();
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $temp = array();
                $temp[] = $row->UserID;
                $temp[] = $row->UserName;
                $temp[] = $row->Email;
                $temp[] = $row->SignUpDate;
                $arr[] = $temp;
            }
        }

        return $arr;
    }
    
    function getNewEmailFrom($date) {
        $sql = 'select UserID,Email,UserName,SignUpDate  
        from user where Email is not NULL AND Email !=""';
        if ($date) {
            $sql.=" And DATE_FORMAT(DATE_SUB('$date',INTERVAL 7 HOUR), '%Y-%m-%d %H:%i:%s')<=DATE_FORMAT(user.`SignUpDate`,'%Y-%m-%d %H:%i:%s') and  
            DATE_FORMAT(user.`LastActivity`,'%Y-%m-%d %H:%i:%s') < DATE_FORMAT(DATE_SUB(DATE_SUB('".gmdate('Y-m-d H:i:s')."', INTERVAL 1 Day),INTERVAL 7 HOUR), '%Y-%m-%d %H:%i:%s')
            "; 
        }        
        $arr = array();
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $temp = array();
                $temp[] = $row->UserID;
                $temp[] = $row->UserName;
                $temp[] = $row->Email;
                $temp[] = $row->SignUpDate;
                $arr[] = $temp;
            }
        }

        return $arr;
    }

    function getTotalFriendInvited() {
        $sql = "select sum(NumberInvite) as Total from user";
        $query = $this->db->query($sql);
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getTotalUserShareToWall($type = 'first_time') {
        $field = 'IsShareToWall';
        if ($type == '3days') {
            $field = 'IsShareToWall3Days';
        }
        $sql = "select count(UserID) as Total from user where $field > 0";
        $query = $this->db->query($sql);
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    function getTopLike($cond, $limit = 100){
        $photo_url = $this->config->item('s3_photo_url');
        $where = '';
        if (isset($cond['day']) && !empty($cond['day'])) {
//                $where = " AND DATE_FORMAT(CreatedDate,'%Y-%m-%d') = '".$cond['day']."'";

            $pst_start = date('Y-m-d H:i:s', strtotime($cond['day'] . '07:00:00'));
            $tmp_date = strtotime($cond['day'] . '06:59:59');
            $pst_end = date('Y-m-d H:i:s', strtotime('+1 days', $tmp_date));
            $where = " AND CreatedDate BETWEEN '$pst_start' AND '$pst_end'";
        }
            
        // if existed hashtag
        if (isset($cond['hashtag']) && !empty($cond['hashtag'])){
            $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
            $hashtag = mysqli_real_escape_string($con, $cond['hashtag']); 
            
            $sql = "select *
                        , if(Image is not null && Image != '', concat('$photo_url', '', Image), '') as Image
                        , (select count(*) as TopLike from activitylike where ContentID = t.RefID AND Type = t.Type $where) as TotalLike
                    from (select a.ActivityID, a.FromUserID, a.FromUserName, a.Message, a.Image, a.CreatedDate, a.RefID, a.Type
                          from (select ActivityID from hashtag h where HashTag = '$hashtag') as h
                          left join activity a ON h.ActivityID = a.ActivityID
                          where a.ActivityID is not null
                    ) as t
                    
                    having TotalLike > 0
                    ORDER BY TotalLike DESC
                    limit $limit";
        }else{
            $sql = "select a.ActivityID, a.FromUserID, a.FromUserName, a.Message
                        , if(a.Image is not null && a.Image != '', concat('$photo_url', '', a.Image), '') as Image
                        , a.CreatedDate
                        , temp.TotalLike
                    from
                    (
                            select ContentID, Type, count(*) as TotalLike, concat(ContentID,'-',Type) as TempID
                            from activitylike
                            where 1 $where
                            group by TempID ORDER BY TotalLike DESC limit $limit
                    ) as temp
                    left join activity a on a.RefID=temp.ContentID AND a.ActionType=temp.Type
                    where a.ActivityID is not null
                    order by temp.TotalLike desc
                    ";
        }

        $query = $this->db->query($sql);
        return $query->result();
    }

    function formatMsg($msg){
        $return = $msg;
        $json = json_decode($msg);
        if ($json){
            $return = isset($json->message)?$json->message:$msg;
        }
        return $return;
    }
    
    function getTopPoints($from = '', $to = ''){
        $photo_url = $this->config->item('s3_photo_url');
        $iLimit = 20;
        $where = '';
        if ($from != '' && $to != ''){
            $where = " where CreatedDate BETWEEN '$from' AND '$to' ";
        }
        $sql = "select u.UserID, u.UserName, u.BirthDate, t.numPoint as Point
                        , if (u.Photo != '', concat('$photo_url', Photo), '') as Avatar
                from (
                    select UserID, sum(Point) as numPoint from influence_point 
                    $where
                    group by UserID
                    having numPoint > 0
                    ORDER BY numPoint DESC
                    limit $iLimit
                ) as t
                left join user u on u.UserID = t.UserID";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    function getTotalMatch($from = '', $to = '', $isSuccess = 0) {
        $this->db->select('count(*) as Total');
        $this->db->from('dailymatch');
        if ($from != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='" . $from . "'");
        }
        if ($to != '') {
            $this->db->where("DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='" . $to . "'");
        }   
        if($isSuccess){
            $this->db->where("NumberOfChat >= 4");
        }     
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }
    
    function totalNewUserVerify($from = '', $to = '',$type = 'unverified') {
        
        if($type == 'sms'){
            $sql = "select count(u.UserID) as Total 
                    from `user` u left join userverification uv on u.UserID = uv.UserID
                    where
                    DATE_FORMAT(DATE_SUB(SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='$from' 
                    and DATE_FORMAT(DATE_SUB(SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='$to'
                    and u.IsVerify = 1                      
                    and uv.VerifiedDate is not NULL ";                       
        }else if($type == 'facebook'){
            $sql = "select count(u.UserID) as Total 
                    from `user` u left join userverification uv on u.UserID = uv.UserID
                    where
                    DATE_FORMAT(DATE_SUB(SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='$from' 
                    and DATE_FORMAT(DATE_SUB(SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='$to'
                    and u.IsVerify = 1 and FacebookID > 0  and uv.VerifiedDate is NULL
                    ";
        }else if($type == 'manual'){
             $sql = "select count(u.UserID) as Total 
                    from `user` u left join userverification uv on u.UserID = uv.UserID
                    where
                    DATE_FORMAT(DATE_SUB(SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='$from' 
                    and DATE_FORMAT(DATE_SUB(SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='$to'
                    and u.IsVerify = 1 
                    and (u.FacebookID is null or u.FacebookID = '')
                    and uv.VerifiedDate is NULL ";     
        }else{
            $sql = "select count(u.UserID) as Total
                    from `user` u 
                    where
                    DATE_FORMAT(DATE_SUB(SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')>='$from' 
                    and DATE_FORMAT(DATE_SUB(SignUpDate, INTERVAL 7 HOUR ) , '%Y-%m-%d %H:%i:%s')<='$to'
                    and u.IsVerify = 0  
                    ";
        }     
        $query = $this->db->query($sql);
        $row = $query->row();
        return ($row) ? $row->Total : 0;
    }

    /**
     * 
     * @param type $view_only
     * @param type $type
     * @param type $from_date
     * @param type $to_date
     * @param type $total
     * @param type $offset
     * @param type $limit
     * @return type
     */
    function getEmailUserToExport($time = -1, $from_date = '', $to_date = '', &$total =0, $offset = 0, $limit = 0) {
        $sql = 'select UserID,Email,UserName,SignUpDate  
                from user where Email is not NULL AND Email !=""';
        $where = '';
        if ($time == 'last7Day') {
            $where.=' And DATE_FORMAT(DATE_SUB(DATE_SUB(NOW(), INTERVAL 7 Day),INTERVAL 7 HOUR), "%Y-%m-%d %H:%i:%s")<=DATE_FORMAT(user.`SignUpDate`,"%Y-%m-%d %H:%i:%s")';
        }
        
        if ($time == 'newEmailsOfflinePreviousDay'){
            $date = '2014-11-26';
            $where.= " And DATE_FORMAT(DATE_SUB('$date',INTERVAL 7 HOUR), '%Y-%m-%d %H:%i:%s')<=DATE_FORMAT(user.`SignUpDate`,'%Y-%m-%d %H:%i:%s') and  
            DATE_FORMAT(user.`LastActivity`,'%Y-%m-%d %H:%i:%s') < DATE_FORMAT(DATE_SUB(DATE_SUB('".gmdate('Y-m-d H:i:s')."', INTERVAL 1 Day),INTERVAL 7 HOUR), '%Y-%m-%d %H:%i:%s')
            "; 
          
        }
        if ($from_date != '' && $to_date != ''){
            $where.= " AND '$from_date' < DATE_FORMAT(user.`SignUpDate`,'%Y-%m-%d %H:%i:%s')";
            $where.= " AND '$to_date' >= DATE_FORMAT(user.`SignUpDate`,'%Y-%m-%d %H:%i:%s')";
        }
        
        $sql .= $where;
        $total = 0;
        if ($limit > 0){
            $totalSql = 'select count(UserID) as Total  
                        from user where Email is not NULL AND Email !=""';
            $totalSql .= $where;
            $totalQuery = $this->db->query($totalSql);
            $row = $totalQuery->row();
            $total = $row?$row->Total:0;
            
            // paging
            $sql .= " limit $offset, $limit";
        }
        $query = $this->db->query($sql);
        return $query->result();

    }
}

?>