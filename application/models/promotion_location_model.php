<?php

class Promotion_location_model extends CI_Model
{
    protected $tableName = 'promo_location';

    function add($data)
    {
        $this->db->insert($this->tableName, $data);
        return $this->db->insert_id();
    }

    function update($id, $data)
    {
        return $this->db->update($this->tableName, $data, 'WHERE id = ' . $id);
    }

    function deleteLocation($id)
    {
        return $this->db->delete($this->tableName, 'WHERE id = ' . $id);
    }

    function getLocation($id)
    {
        $this->db->select('*');
        $this->db->from($this->tableName);
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getLocations()
    {
        $this->db->select('*');
        $this->db->from($this->tableName);
        $this->db->where('enable', 1);
        $query = $this->db->get();
        return $query->result();
    }

    function getNearbyLocations($lat = '', $lon = '')
    {
        $nearbyLocs = array();
        if (trim($lat) != '' && trim($lon) != '') {
            $CI = & get_instance();
            $locations = $this->getLocations();

            if (sizeof($locations) > 0) {
                foreach ($locations as $location) {
                    $lLat = $location->lat;
                    $lLon = $location->lon;
                    $lRadius = $location->radius;

                    $miles = $CI->commonlib->distance($lat, $lon, $lLat, $lLon, 'M');
                    if ($miles < $lRadius) {
                        $nearbyLocs[] = $location->id;
                    }
                }
            }
        }

        return $nearbyLocs;
    }

    function getListDropDown()
    {
        $this->db->select('*');
        $this->db->from($this->tableName);
        $this->db->where('enable', 1);
        $query = $this->db->get();
        $result = $query->result();
        $data = array();
        foreach ($result as $re) {
            $data[$re->id] = $re->name;
        }
        return $data;
    }
}

?>