<?php

class Credit_model extends CI_Model {

    function add($data) {
        $this->db->insert('creditpackage', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('creditpackage', $data, $cond);
    }

    function getCreditPackageInfo($credit_package_id) {
        $this->db->select('*');
        $this->db->from('creditpackage');
        $this->db->where('ID', $credit_package_id);
        $query = $this->db->get();
        return $query->row();
    }

    function addCreditTransaction($data) {
        $this->db->insert('credittransaction', $data);
        return $this->db->insert_id();
    }

    function getListCredit() {
        $this->db->select('*');
        $this->db->from('creditpackage');
        $query = $this->db->get();
        return $query->result();
    }

    function getListByCondition($cond, $from, $to, $getSumNumber) {
        $this->db->select('*');
        $this->db->from('creditpackage');
        if (count($cond) > 0) {
            if (isset($cond['sort'])) {
                switch ($cond['sort']) {
                    case 'id-asc':
                        $this->db->order_by('ID', 'asc');
                        break;
                    case 'id-desc':
                        $this->db->order_by('ID', 'desc');
                        break;
                    case 'price-asc':
                        $this->db->order_by('Price', 'asc');
                        break;
                    case 'price-desc':
                        $this->db->order_by('Price', 'desc');
                        break;
                    case 'credits-asc':
                        $this->db->order_by('Credits', 'asc');
                        break;
                    case 'credits-desc':
                        $this->db->order_by('Credits', 'desc');
                        break;
                }
            }
        }
        if ($getSumNumber) {
            $query = $this->db->get();
            return $query->num_rows();
        } else {
            $this->db->limit($from, $to);
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? $query->result() : null;
        }
    }

    function checkReceipt($receipt, $platform) {
        $this->db->select('*');
        $this->db->from('credittransaction');
        $this->db->where('Receipt', $receipt);
        $this->db->where('Platform', $platform);
        $query = $this->db->get();
        return $query->row();
    }

}

?>