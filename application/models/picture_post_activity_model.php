<?php

class Picture_Post_Activity_model extends CI_Model {

    public $cached = false;

    function __construct() {
        parent::__construct();
        $this->db_slave = $this->load->database('slave', TRUE);
        $CI = & get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
    }

    function add($data) {
        $this->db->insert('picturepostactivity', $data);
        return $this->db->insert_id();
    }

    function findById($id) {
        $this->db->select('p.*');
        $this->db->from('picturepostactivity p');
        $this->db->where('ID', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function deletePicStatus($id, $user_id = 0) {
        try {
            Activity_model::invalidUserCached($user_id);

            $this->db->trans_begin();

            $this->db->delete('picturepostactivity', array('ID' => $id));
            
            $commentIDs = Activity_model::getCommentIDOf($id, ACTIVITY_POST_PICTURE);
            if (count($commentIDs) > 0) {
                $this->db->delete('notificationhistory', "RefID IN (" . implode(',', $commentIDs) . ") AND Type=14 AND ContentType=27"); // comment activity
            }
            $likeIDs = Activity_model::getLikeIDOf($id, ACTIVITY_POST_PICTURE);
            if (count($likeIDs) > 0) {
                $this->db->delete('notificationhistory', "RefID IN (" . implode(',', $likeIDs) . ") AND Type=13 AND ContentType=27"); // like activity
            }

            $this->db->delete('activitycomment', array('ContentID' => $id, 'Type' => ACTIVITY_POST_PICTURE));
            $this->db->delete('activitylike', array('ContentID' => $id, 'Type' => ACTIVITY_POST_PICTURE));

            // delete hashtag 
            $sql = "delete from hashtag where ActivityID IN (select ActivityID from activity where RefID = $id AND Type = 27)";
            $this->db->query($sql);
            $this->db->delete('activity', array('RefID' => $id, 'Type' => ACTIVITY_POST_PICTURE));
            $this->db->delete('activity_archive', array('RefID' => $id, 'Type' => ACTIVITY_POST_PICTURE));
            
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }
    }

}

?>