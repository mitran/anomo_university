<?php

class User_Match_model extends CI_Model{
    public $cached = false;

    function __construct(){
        parent::__construct();
        $CI =& get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
        
    }
    function add($data) {
        $this->db->insert('dailymatch', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('dailymatch', $data, $cond);
    }
        
    function checkPreviousMatch($user_id){
        $sql = "select * from dailymatch where CurrentUserID = $user_id order by CreatedDate desc limit 1";
        $query = $this->db->query($sql);
        $result = $query->row();
        $query->free_result();
        $query->free_result();
        return $result;                       
    }      
    
    function getMatch($user_id,$target_user_id){
       $sql = "select * from dailymatch 
               where (CurrentUserID = $user_id and TargetUserID = $target_user_id) 
               or (CurrentUserID = $target_user_id and TargetUserID = $user_id )  
               limit 1";
        $query = $this->db->query($sql);
        $result = $query->row();
        $query->free_result();
        $query->free_result();
        return $result; 
    }
    
    function addFailTracking($data){
        $this->db->insert('dailymatchtracking', $data);
        return $this->db->insert_id();
    }
    
    function increaseNumberChatMsg($user_id, $target_user_id){
        $sql = "update dailymatch set `NumberOfChat` = `NumberOfChat` + 1 
                where (CurrentUserID = $user_id AND TargetUserID = $target_user_id)
                OR (CurrentUserID = $target_user_id AND TargetUserID = $user_id)";
        $this->db->query($sql);
    }
    
    function countUserMatch($user_id){
        $this->db->select('count(TargetUserID) as total');
        $this->db->from('dailymatch');
        $this->db->where('CurrentUserID', $user_id);
        $query = $this->db->get();
        $result = $query->result();        
        if($result[0]){
            return $result[0]->total;
        }
        return 0;
    }
    
    function updateFirstChatAndReplyDate($user_id,$target_user_id){
       $previous = $this->getMatch($user_id,$target_user_id);
       if($previous){
         if($previous->CurrentUserID == $user_id){
            //Update FirstChatDate
            $sql = " update dailymatch set FirstChatDate = '".gmdate('Y-m-d H:i:s')."' 
                where FirstChatDate is null and CurrentUserID = $user_id and TargetUserID = $target_user_id
            ";
            $this->db->query($sql);
         }else{
            //Update FirstReplyDate
            $sql = " update dailymatch set FirstReplyDate = '".gmdate('Y-m-d H:i:s')."' 
                where FirstChatDate is not null and FirstReplyDate is null and CurrentUserID = $target_user_id and TargetUserID = $user_id
            ";
            $this->db->query($sql);
         }
       }
    }
}
?>

