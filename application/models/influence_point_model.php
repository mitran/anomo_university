<?php
class Influence_Point_model extends CI_Model {
    function __construct(){
        parent::__construct();
        $CI =& get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
    }
    
    function add($data) {
        User_model::updateUserPoint($data, 'plus');
        $this->db->insert('influence_point', $data);
        return $this->db->insert_id();
    }
    
    function minusPoint($data){
        User_model::updateUserPoint($data, 'minus');
        $point_minus = isset($data['Point'])? - $data['Point']:0;
        $data['Point'] = $point_minus;
        $this->db->insert('influence_point', $data);
        return $this->db->insert_id();
    }

    function weekPoint($user_id) {
        $number_week = 6;
        $now = gmdate('Y-m-d');
        $CI = & get_instance();
        $nowWeek = $CI->commonlib->x_week_range($now);
        $now_start_w = $nowWeek[0];
        $last_date = date('Y-m-d', strtotime('- ' . $number_week . ' week', strtotime($now)));
        $dateWeek = $CI->commonlib->x_week_range($last_date);
        $start_w = $dateWeek[0];
        $end_w = $dateWeek[1];
        
        // ANOMO-10715 - Show cumulative points on the Influence graph
        $last6WeekSql = "select SUM(Point) as Total from influence_point where UserID = $user_id AND CreatedDate <= '$start_w' ";
        $query1 = $this->db->query($last6WeekSql);
        $row = $query1->row();
        $pointLast6Week = $row?(int)$row->Total:0;

        $sql = "select * from influence_point where UserID = $user_id AND CreatedDate > '$start_w' AND CreatedDate <= '$now_start_w'";
        $query = $this->db->query($sql);
        $result = $query->result();
        $data = array();
        if ($result) {
            $point = $pointLast6Week;
            for ($i = 0; $i < $number_week; $i++) {
                $start = date('Y-m-d', strtotime("+ $i week", strtotime($start_w)));
                $end = date('Y-m-d', strtotime("+ $i week", strtotime($end_w)));
                foreach ($result as $row) {
                    if ($row->CreatedDate > $start && $row->CreatedDate <= $end) {
                        $point += $row->Point;
                    }
                }
                $data[] = array(
                    'Week' => $number_week - $i,
                    'Point' => $point,
                );
            }
        }else{
            // just return 0 point for each week
            for ($i = 0; $i < $number_week; $i++) {
                $data[] = array(
                    'Week' => $number_week - $i,
                    'Point' => $pointLast6Week
                );
            }
        }
        return $data;
    }
    
    function maxPointPerDay($user_id, $maxPoint){
        $now = gmdate('Y-m-d');
        $sql = "select SUM(Point) as sumPoint from influence_point where UserID = $user_id
                AND Type = 4 AND DATE_FORMAT(CreatedDate, '%Y-%m-%d') = '".$now."'";
        $query = $this->db->query($sql);
        $row = $query->row();
        if ($row){
            $point = $row->sumPoint;
            if ($point >= $maxPoint){
                return true;
            }
        }
        return false;
    }

}
?>
