<?php
class FbResult_model extends CI_Model {
	function add($data){
		$this->db->insert('fbresult', $data);
		return $this->db->insert_id();
	}
	
	function update($data, $cond){
		return $this->db->update('fbresult', $data, $cond);
	}
	
	function delete($cond){
		$this->db->delete('fbresult', $cond);
	}
	
	function getCurrentQuestionOf ($user_id, $anomotion_id){
		$this->db->select('q.*');
		$this->db->from('fbresult r');
		$this->db->join('question q', 'r.QuestionID=q.QuestionID');
		$this->db->where('r.AnomotionID', $anomotion_id);
		$this->db->where('UserID', $user_id);
		$this->db->order_by('r.CreatedDate', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->row();
	}
	
	function checkAnomotionQuestion($user_id, $anomotion_id, $question_id){
		$this->db->select('*');
		$this->db->from('fbresult');
		$this->db->where('UserID', $user_id);
		$this->db->where('AnomotionID', $anomotion_id);
		$this->db->where('QuestionID', $question_id);
		$this->db->order_by('CreatedDate', 'DESC');
		$query = $this->db->get();
		return $query->row();
	}
	
	function getQuestionStatusOf($user_id, $anomotion_id){
		$this->db->select('*');
		$this->db->from('fbresult');
		$this->db->where('UserID', $user_id);
		$this->db->where('AnomotionID', $anomotion_id);
		$this->db->order_by('CreatedDate', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->row();
	}
	
	function getProgressOfUser($user_id, $anomotion_id){
                $currentTime = gmdate("Y-m-d H:i:s", strtotime( $this->config->item('expired') ) );
		$this->db->select('q.*, r.AnswerID');
		$this->db->from('fbresult r');
		$this->db->join('question q', 'q.QuestionID=r.QuestionID');
		$this->db->where('r.UserID', $user_id);
		$this->db->where('r.AnomotionID', $anomotion_id);
                $this->db->where("r.CreatedDate >= '".$currentTime."'");//game was created 24hrs more than
		$this->db->order_by('r.ID', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->row();
	}
	
	function getListresultOf($user_id, $anomotion_id){
		$this->db->select('*');
		$this->db->from('fbresult');
		$this->db->where('UserID', $user_id);
		$this->db->where('AnomotionID', $anomotion_id);
		$this->db->where('AnswerID !=', 'NULL');
		$query = $this->db->get();
		$results = $query->result();
		$data = array();
		if ($results){
			foreach ($results as $re){
				$data[$re->QuestionID] = $re->AnswerID;
			}
		}
		return $data;
	}
	function getListresultOfAnomotion($anomotion_id) {
		$this->db->select('*');
		$this->db->from('fbresult');
		$this->db->where('AnomotionID', $anomotion_id);
		$this->db->where('AnswerID !=', 'NULL');
		$query = $this->db->get();
		$results = $query->result();
		return $results;
	}
	function getListResultOfQuestion($anomotion_id, $question_id) {              
                $sql = "select r.*
                        from fbresult r                      
                        where r.AnomotionID=$anomotion_id
                            AND r.QuestionID=$question_id
                            AND r.AnswerID is not null
                    ";
                $query = $this->db->query($sql);
                return $query->result();
	}
        
        function getListResultOfQuestion_v2($anomotion_id, $question_id) {
                $sql = "select r.UserID, a.Content from result r
                        inner join answer a ON a.QuestionID=r.QuestionID and a.AnswerID=r.AnswerID
                        where r.AnomotionID=$anomotion_id AND r.QuestionID=$question_id and r.AnswerID is not null
                    ";
                $query = $this->db->query($sql);
		return $query->result();
	}
}
?>