<?php

class reported_content_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	
    function add($data) {
        $this->db->insert('flagcontent', $data);
        $ret = $this->db->insert_id();
        $sql = "update user set FlagsCount = FlagsCount + 1 where UserID = " . $data['ContentOwnerID'];
        $this->db->query($sql);
        return $ret;
    }

    function update($data, $cond) {
        return $this->db->update('flagcontent', $data, $cond);
    }

    function isAddFlag($user_id, $content_id, $type) {
        $this->db->select('ID');
        $this->db->from('flagcontent');
        $this->db->where('UserID', $user_id);
        $this->db->where('ContentID', $content_id);
        $this->db->where('Type', $type);
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? 1 : 0;
    }

    function getListByCondition($cond, &$total, $offset = 0, $limit = 0) {
    	
        if (isset($cond['flag'])) {
            $where = " where ContentOwnerID=" . $cond['flag'];
        }

        if (isset($cond['ContentID'])) {
        	$con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
        	$cond['ContentID'] = mysqli_real_escape_string($con, $cond['ContentID']);
        	$q = "(`ContentID` = '{$cond['ContentID']}')";
			$where = "where $q";
        }		
        $tSql = "select count(ID) as Total from flagcontent $where";
        $tQuery = $this->db->query($tSql);
        $row = $tQuery->row();
        $total = ($row)?$row->Total:0;
        
        $groupby = "group by ContentID";

        if (isset($cond['ContentID'])) {
        	$groupby = "";
        }
        
        $sql = "select f.*, u.UserName as ReportingUser, u1.UserName as ReportedUser,u1.IsBanned
				from (
                                    select * from flagcontent $where 
									$groupby
                                    order by CreatedDate DESC
                                    limit $limit, $offset
                                    ) as f
				left join user u ON (u.UserID = f.UserID)
				left join user u1 ON (u1.UserID=f.ContentOwnerID)
		";
        $query1 = $this->db->query($sql);
        return $query1->result();
    }

    function getsql(&$total,$offset = 0, $limit = 0) { 
    	$tsql = "select count(B.UserID) as Total from
				(select ContentOwnerID from flagcontent 
    					where ID IN (select ID from flagcontent where IsForgiven IS NULL) group by ContentID) as A
				left join (select UserID from user where IsBanned = '0') as B
				on A.ContentOwnerID = B.UserID
    			";

    	$tQuery = $this->db->query($tsql);
    	$row = $tQuery->row();
    	$total = ($row)?$row->Total:0;   
 	  
  
    	$sql = "select A.* from
		(
		select f1.*, u.UserName as ReportingUser, u1.UserName as
    	ReportedUser,u1.IsBanned,group_concat(ifnull(u.UserName,'NULL') separator ' : ') as ReportingUsers,count(distinct ifnull(u.UserName,'NULL')) as c_users,
    	group_concat(f1.Content separator ' : ') as Contents,
    	group_concat(ifnull(f1.UserID,'NULL') separator ' : ') as ReportingUserIds
    	from    	
    	(select ID,UserID,ContentOwnerID,ContentID,Content,Type,CreatedDate,IsForgiven,IsComment
    	from flagcontent where ID IN (select ID from flagcontent where IsForgiven IS NULL)
    	AND UserID NOT IN (SELECT UserID from ExcludedReportingUsers)		    			
    	) as f1
    	left join user u ON (u.UserID = f1.UserID) 
    	left join user u1 ON (u1.UserID=f1.ContentOwnerID)  
    	where u1.IsBanned = '0'  	
    	group by ContentID,Type
    	order by CreatedDate DESC    	
		) A
    	limit $limit, $offset
    	";    	
    	$query1 = $this->db->query($sql);
    	return $query1->result();
    }    
    
    function getListChatUsers($user_id){

   		$sql1 = "select temp.ReceiveUserID, 
    	u1.UserName as ReceiveUserName
    	from (select
    	distinct SUBSTRING_INDEX(toJID, '@', 1) as ReceiveUserID
    	from ofmessagearchive
    	where
    	(fromJID in ('" . $user_id . "@" . XMPP_HOST . "')
    			or toJID in ('" . $user_id . "@" . XMPP_HOST . "'))
    			AND body != ''
    					AND body NOT IN ('filter_request_game',
    							'filter_message_status_typing',
    							'filter_message_status_endtyping',
    							'filter_message_status_delivered')

    	) as temp
    	inner join user u1 ON u1.UserID = temp.ReceiveUserID
    	";  	   	
    	$query1 = $this->db->query($sql1);
    	return $query1->result();
    }
    
    function getListMsgXMPP($user_id,$with_user_id){
    	$sql1 = "select temp.ReceiveUserID,temp.ChatID,temp.Content,temp.Photo,temp.SendUserID,temp.CreatedDate,
    	u1.UserName as SendUserName,
    	u2.UserName as ReceiveUserName
    	from (select 
    	distinct SUBSTRING_INDEX(toJID, '@', 1) as ReceiveUserID,
    	conversationID as ChatID,
    	IF (SUBSTRING(TRIM(body), 1,16)='filter_image_tag', '',body) as Content,
    	IF (SUBSTRING(TRIM(body), 1,16)='filter_image_tag',
    	SUBSTRING(body, 17, INSTR(body, 'filter_image_untag') - 17),'') as Photo,
    	SUBSTRING_INDEX(fromJID, '@', 1) as SendUserID,    	
    	FROM_UNIXTIME(sentDate/1000) as CreatedDate
    	from ofmessagearchive
    	where
		(fromJID in ('" . $user_id . "@" . XMPP_HOST . "','" . $with_user_id . "@" . XMPP_HOST . "') 
		and toJID in ('" . $user_id . "@" . XMPP_HOST . "','" . $with_user_id . "@" . XMPP_HOST . "'))        			
						AND body != ''
    					AND body NOT IN ('filter_request_game',
    							'filter_message_status_typing',
    							'filter_message_status_endtyping',
    							'filter_message_status_delivered')
    							order by CreatedDate DESC 
    	) as temp
    	inner join user u1 ON u1.UserID = temp.SendUserID
    	inner join user u2 ON u2.UserID = temp.ReceiveUserID
		order by CreatedDate
    	";
    	$query1 = $this->db->query($sql1);
    	return $query1->result();
    }
    
    
    function update_reported_content($data, $cond) {
        $this->db->update('flagcontent', $data, $cond);
        if (isset($data['IsForgiven']) && $data['IsForgiven'] == 1) {
            // enable this post on activity
            $activityCond = array(
                'RefID' => $cond['ContentID'],
                'Type' => $cond['Type']
            );
            $this->db->update('activity', array('IsInvalid' => 0), $activityCond);
        }
    }

    
    function deletecontent($id, $type, $is_comment = 0) {
        return $this->db->delete('flagcontent', "ContentID = $id AND Type = $type AND IsComment = $is_comment");
    }
    
    function get_username($user_id){
    	$sql = "select UserName from user where UserID = $user_id ";
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
    function getExcludedReportingUsers(){
    	$sql = "select u.UserName UserNames from ExcludedReportingUsers eru left join user u on eru.UserID = u.UserID";
    	$query = $this->db->query($sql);
    	return $query->result();
    }
    
    function add_exclude_reporting_users($excludeUser){

    	$this->db->select('UserID'); 
    	$this->db->from('user');
    	$this->db->where('UserName', $excludeUser);
    	$query = $this->db->get();
    	$result = $query->result();
    	$userid = $result[0]->UserID;
    	$query->free_result();

    	$add_data = array('UserID' => $userid);
    	return $this->db->insert('ExcludedReportingUsers', $add_data);    	
    	
    }
    
    function updateActivityArchive($data, $cond){
        return $this->db->update('activity_archive', $data, $cond);
    }
}

?>
