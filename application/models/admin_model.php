<?php

class Admin_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function login($user_name, $password) {
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('UserName', $user_name);
        $this->db->where('Password', md5($password));
        $this->db->where('Status', 1); // active
        $query = $this->db->get();
        $row = $query->row();
        if (count($row) > 0) {
            $login = true;
            $this->session->set_userdata('user_name', $row->UserName);
            $this->session->set_userdata('nick_name', $row->NickName);
            $this->session->set_userdata('role', 'admin');
            $this->session->set_userdata('admin_id', $row->AdminID);
            $this->db->update('admin', array('LastLogin' => date('Y-m-d H:i:s')), array('AdminID' => $row->AdminID));
        } else {
            $login = false;
        }
        return $login;
    }

    function logout() {
        $this->session->unset_userdata('user_name');
        $this->session->unset_userdata('role');
        $this->session->unset_userdata('nick_name');
        $this->session->unset_userdata('admin_id');
    }

    function getInfo($admin_id, $use = '') {
        $this->db->select('*');
        $this->db->from('admin');
        if ($use == 'user_name') {
            $this->db->where('UserName', $admin_id);
        } else {
            $this->db->where('AdminID', $admin_id);
        }
        $query = $this->db->get();
        return $query->row();
    }

    function add($data) {
        $this->db->insert('admin', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('admin', $data, $cond);
    }

    function check_user_name($user_name) {
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('UserName', $user_name);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? false : true;
    }

    function getListByCondition($cond, $from, $to, $getSumNumber) {
        $this->db->select('*');
        $this->db->from('admin');
        if (count($cond) > 0) {
            if (isset($cond['status'])) {
                $this->db->where('Status', $cond['status']);
            }

            if (isset($cond['keyword'])) {
//				$this->db->like('user_name', $cond['keyword']);
//				$this->db->or_like('nick_name', $cond['keyword']);
//				$this->db->or_like('email', $cond['keyword']);

                $q = "(`UserName` LIKE '%{$cond['keyword']}%' OR `NickName` LIKE '%{$cond['keyword']}%' OR `Email` LIKE '%{$cond['keyword']}%' )";
                $this->db->where($q);
            }

            if (isset($cond['sort'])) {
                switch ($cond['sort']) {
                    case 'id-asc':
                        $this->db->order_by('AdminID', 'asc');
                        break;
                    case 'id-desc':
                        $this->db->order_by('AdminID', 'desc');
                        break;
                    case 'user-name-asc':
                        $this->db->order_by('UserName', 'asc');
                        break;
                    case 'user-name-desc':
                        $this->db->order_by('UserName', 'desc');
                        break;
                    case 'nick-name-asc':
                        $this->db->order_by('NickName', 'asc');
                        break;
                    case 'nick-name-desc':
                        $this->db->order_by('NickName', 'desc');
                        break;
                    case 'email-asc':
                        $this->db->order_by('Email', 'asc');
                        break;
                    case 'email-desc':
                        $this->db->order_by('Email', 'desc');
                        break;
                }
            }
        }
        if ($getSumNumber) {
            $query = $this->db->get();
            return $query->num_rows();
        } else {
            $this->db->limit($from, $to);
            $query = $this->db->get();
//			echo $this->db->last_query();die;
            return ($query->num_rows() > 0) ? $query->result() : null;
        }
    }

    function check_user_reset_pass($user_name, $key) {
        $this->db->from('admin');
        $this->db->where('UserName', $user_name);
        $this->db->where('ForgotPassKey', $key);
        $query = $this->db->get();
        return $query->row();
    }

    function delete($admin_id) {
        $this->db->delete('admin', array('AdminID' => $admin_id));
    }

}

?>