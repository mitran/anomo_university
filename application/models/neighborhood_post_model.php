<?php

class Neighborhood_Post_Model extends CI_Model {

    function add($data) {
        $this->db->insert('neighborhoodpost', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('neighborhoodpost', $data, $cond);
    }

    function getInfo($post_id) {
        $this->db->select('pp.*, u.UserName, u.Gender, u.Photo as Avatar');
        $this->db->from('neighborhoodpost pp');
        $this->db->join('user u', 'pp.UserID=u.UserID');
        $this->db->where('ID', $post_id);
        $query = $this->db->get();
        return $query->row();
    }

    function findById($post_id) {
        $this->db->select('pp.*');
        $this->db->from('neighborhoodpost pp');
        $this->db->where('ID', $post_id);
        $query = $this->db->get();
        return $query->row();
    }

    function getListPostOf($nbh_id, $type = 'text', &$total = '', $offset = '', $limit = 0) {
        $where = '';
        if ($type == 'text') {
            $where .= " AND pp.Content != '' AND pp.Photo = ''";
        }
        if ($type == 'picture') {
            $where .= " AND pp.Photo != ''";
        }
        $sql = "select count(*)  as total
                from(select ID, UserID 
                    from neighborhoodpost pp 
                    where pp.NeighborhoodID='$nbh_id' $where
                 ) as temp 
                 inner join user u ON temp.UserID=u.UserID AND u.IsBanned != 1
        ";
        $query = $this->db->query($sql);
        $row = $query->row();
        $total = $row?$row->total:0;
        
        // get result
         $sql1 = "select pp.UserID, u.UserName, u.Photo as Avatar, u.Gender, pp.ID, pp.Content, pp.Photo, pp.Photo100, pp.Photo200, pp.Photo300, pp.CreatedDate
                    , (select count(ID) from neighborhoodcomment where NeighborhoodPostID = pp.ID ) as TotalComment
                from(select *
                    from neighborhoodpost pp 
                    where pp.NeighborhoodID='$nbh_id' $where
                 ) as pp 
                 inner join user u ON pp.UserID=u.UserID AND u.IsBanned != 1
                 order by pp.CreatedDate desc
                 limit $offset, $limit
        ";
         $query = $this->db->query($sql1);
         return $query->result();
    }

    function getListPostOfV2($place_id, $type = 'text', $limit = 0) {
        $this->db->select('pp.Photo');
        $this->db->from('neighborhoodpost pp');
        $this->db->join('user u', 'u.UserID=pp.UserID');
        $this->db->where('pp.NeighborhoodID', $place_id);
        $this->db->where('pp.IsMark', 0);
        if ($type == 'text') {
            $this->db->where('pp.Content !=', "");
            $this->db->where('pp.Photo', "");
        } else {
            $this->db->where('pp.Photo !=', "");
        }
        $this->db->order_by('pp.CreatedDate', 'DESC');
        if ($limit > 0) {
            $this->db->limit($limit);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function getTotalPostInNbh($nbh_id, $type = 'text'){
        $where = '';
        if ($type == 'text') {
            $where .= " AND pp.Content != '' AND pp.Photo = ''";
        }
        if ($type == 'picture') {
            $where .= " AND pp.Photo != ''";
        }
        $sql = "select count(ID)  as total
                from(select ID, UserID 
                    from neighborhoodpost pp 
                    where pp.NeighborhoodID='$nbh_id'  $where
                 ) as temp 
                 inner join user u ON temp.UserID=u.UserID AND u.IsBanned != 1
        ";
        $query = $this->db->query($sql);
        $row = $query->row();
        return ($row)?$row->total:0;
        
    }
    
    function getListByCondition($cond, $from, $to, $getSumNumber) {
        $this->db->select('np.*,n.Name,u.Username');
        $this->db->from('neighborhoodpost np');
        $this->db->join('user u', 'u.UserID=np.UserID');
        $this->db->join('neighborhood n', 'np.NeighborhoodID=n.OGR_FID');


        if (count($cond) > 0) {
            if (isset($cond['status'])) {
                $this->db->where('np.Status', $cond['status']);
            }

            if (isset($cond['keyword'])) {
                $q = "concat(u.Username,' ',n.Name,' ',np.Content) like '%{$cond['keyword']}%'";
                $this->db->where($q);
            }

            if (isset($cond['sort'])) {
                switch ($cond['sort']) {
                    case 'date-asc':
                        $this->db->order_by('np.CreatedDate', 'asc');
                        break;
                    case 'date-desc':
                        $this->db->order_by('np.CreatedDate', 'desc');
                    default:
                        $this->db->order_by('np.CreatedDate', 'desc');
                        break;
                }
            }
        } else {
            $this->db->order_by('np.CreatedDate', 'desc');
        }
        if ($getSumNumber) {
            $query = $this->db->get();
            return $query->num_rows();
        } else {
            $this->db->limit($from, $to);
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? $query->result() : null;
        }
    }

    function deletePost($post_id) {
        try {
            $this->db->trans_begin();

            $commentIDs = self::getCommentIDOf($post_id);
            if (count($commentIDs) > 0) {
                $this->db->delete('placeactivity', "RefID IN (" . implode(',', $commentIDs) . ") AND Type = 11");
                $this->db->delete('placeactivitydetail', "ContentID IN (" . implode(',', $commentIDs) . ") AND Type = 11");
                $this->db->delete('notificationhistory', "RefID IN (" . implode(',', $commentIDs) . ") AND Type IN (11, 12)"); // comment
                $this->db->delete('notificationhistory', "RefID IN (" . implode(',', $commentIDs) . ") AND Type=14 AND ContentType IN (7,8)"); // comment activity
                // delete this comment
                $this->db->delete('neighborhoodcomment', array('NeighborhoodPostID' => $post_id));
            }

            $likeIDs = Activity_model::getLikeIDOf($post_id, '7,8');
            if (count($likeIDs) > 0) {
                $this->db->delete('notificationhistory', "RefID IN (" . implode(',', $likeIDs) . ") AND Type=13 AND ContentType IN (7, 8)"); // like activity
            }

            $this->db->delete('placeactivity', "RefID = $post_id AND Type IN (9, 10)"); // post text OR post picture
            $this->db->delete('placeactivitydetail', "ContentID = $post_id AND Type IN (9, 10)"); // post text OR post picture
            $this->db->delete('neighborhoodpost', array('ID' => $post_id));

            $this->db->delete('activitylike', "ContentID = $post_id AND Type IN (7, 8)");
            // delete hashtag 
            $sql = "delete from hashtag where ActivityID IN (select ActivityID from activity where RefID = $post_id AND Type IN (7, 8))";
            $this->db->query($sql);
            $this->db->delete('activity', "RefID = $post_id AND Type IN (7, 8)");

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }
    }

    function getCommentIDOf($post_id) {
        $this->db->select('ID');
        $this->db->from('neighborhoodcomment');
        $this->db->where('NeighborhoodPostID', $post_id);
        $query = $this->db->get();
        $result = $query->result();
        $data = array();
        if ($result) {
            foreach ($result as $re) {
                $data[] = $re->ID;
            }
        }
        return $data;
    }

}

?>