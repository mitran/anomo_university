<?php

class Neighborhood_CheckIn_model extends CI_Model {

    function update($data, $cond) {
        return $this->db->update('neighborhoodcheckin', $data, $cond);
    }

    function add($data) {
        $this->db->insert('neighborhoodcheckin', $data);
        return $this->db->insert_id();
    }

}

?>