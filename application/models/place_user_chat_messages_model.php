<?php

class Place_User_Chat_Messages_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function add($data) {
        $this->db->insert('placeuserchatmessages', $data);
        return $this->db->insert_id();
    }

    function find50MessagesbyMessageID($placeID, $messageID = -1, $reverse = false, $user_id) {
        $sql = "select pucm.ID, pucm.Content, 
			IF (pucm.Photo != '', CONCAT('" . $this->config->item('s3_photo_url') . "', pucm.Photo) , pucm.Photo) as Photo,
			IF (pucm.Photo100 != '', CONCAT('" . $this->config->item('s3_photo_url') . "', pucm.Photo100) , pucm.Photo100) as Photo100,
			IF (pucm.Photo200 != '', CONCAT('" . $this->config->item('s3_photo_url') . "', pucm.Photo200) , pucm.Photo200) as Photo200,
			IF (pucm.Photo300 != '', CONCAT('" . $this->config->item('s3_photo_url') . "', pucm.Photo300) , pucm.Photo300) as Photo300,
			IF (u.Photo != '', CONCAT('" . $this->config->item('s3_photo_url') . "', u.Photo) , u.Photo) as Avatar,
			pucm.CreatedDate, pucm.ColorID, pucm.UserID, u.UserName ";
        $sql .= ' FROM placeuserchatmessages pucm ';
        $sql .= ' JOIN user u ON pucm.UserID=u.UserID ';
        $sql .= " WHERE pucm.PlaceID ='" . $placeID . "'";
        $sql .= "  AND u.IsBanned != 1";
        if ($messageID > 0) {
            if ($reverse) {
                $sql .= ' AND pucm.ID <' . $messageID;
            } else {
                $sql .= ' AND pucm.ID >' . $messageID;
            }
            $sql .= ' AND pucm.UserID !=' . $user_id;
        }

        $sql .= ' ORDER BY pucm.CreatedDate DESC'; // get latest
        $sql .= ' limit 50';
        $query = $this->db->query($sql);
        return $query->result();
    }

    function countChattingUser($placeID) {
        $this->db->select('distinct(pucm.UserID)');
        $this->db->from('placeuserchatmessages pucm');
        $this->db->join('usercheckin uc', 'pucm.UserID=uc.UserID and pucm.PlaceID=uc.PlaceID', 'inner');
        $this->db->where('pucm.PlaceID', $placeID);
        $this->db->where('uc.Status !=', 2);
        $query = $this->db->get();
        return $query->num_rows();
    }

}