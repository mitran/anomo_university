<?php
class Anonymous_Post_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    function getListByCondition(&$total, $offset, $limit) {
        $sqlTotal = "select count(ActivityID) as Total from activity where  IsInvalid != 1 AND IsAnonymous = 1";
        $totalResult = $this->db->query($sqlTotal);
        $row = $totalResult->row();
        $total = ($row)?$row->Total:0;
        if ($total > 0){
            $photo_url = $this->config->item('s3_photo_url');
            $sql = "select FromUserID, u.UserName, Message, CreatedDate,
                    if (Image != '', concat('$photo_url', Image), '') as Image
                    from (
                        select ActivityID, FromUserID, Message, Image, CreatedDate 
                        from activity where IsInvalid != 1 AND IsAnonymous = 1
                        order by CreatedDate DESC limit $offset, $limit) as t
                    left join user u ON u.UserID = t.FromUserID
                    order by CreatedDate DESC
                    ";
            
            $query1 = $this->db->query($sql);
            return $query1->result();
        }
        return null;
    }
}
?>
