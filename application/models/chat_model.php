<?php

class Chat_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getConversation_SP_XMPP_Test($user_id, $offset = 0, $limit = 10, &$total) {
        $photo_url = $this->config->item('s3_photo_url');
        $sql = "call sp_GetConversationXMPP_Test($user_id,$offset,$limit, '$photo_url', @total)";
        $query = $this->db->query($sql);
        //  return $query->result();

        $result = array();
        $result = $query->result();
        $query->next_result();
        $query2 = $this->db->query("select @total as total");
        foreach ($query2->result_array() as $row) {
            $total = $row['total'];
        }
        $query->free_result();
        $query2->free_result();
        return $result;
    }

    function returnLatestMsg_XMPP($user_id, $with_user_id) {
        $sql = "select IF (SUBSTRING(TRIM(lastMessage), 1,16)='filter_image_tag', 'Photo',lastMessage) as LatestMessage
					from ofconversation where conversationID in (
						select conversationID
						from ofconparticipant
						where 	conversationID in
								(	select conversationID
									from ofconparticipant
									where substring_index(bareJID, '@', 1) = " . $user_id . " )
						and substring_index(bareJID, '@', 1) = " . $with_user_id . ")
					and lastMessage != ''
					order by lastActivity desc limit 1";

        $query = $this->db->query($sql);
        $row = $query->row();
        return $row ? $row->LatestMessage : "";
    }

    function getListMsgXMPP($user_id, $with_user_id, $date) {
        $where = "";
        if (trim($date) != '') {
            $where = " AND FROM_UNIXTIME(sentDate/1000) < '$date'";
        }
        $photo_url = $this->config->item('s3_photo_url');
        $sql = "select temp.*,
                            u1.UserName as SendUserName,
                            u2.UserName as ReceiveUserName,
                            IF (u1.Photo != '', concat('" . $photo_url . "','',u1.Photo), '') as SendUserAvatar,
                            IF (u2.Photo != '', concat('" . $photo_url . "','',u2.Photo), '') as ReceiveUserAvatar
                    from (select conversationID as ChatID, 
                                    IF (SUBSTRING(TRIM(body), 1,16)='filter_image_tag', '',body) as Content,
                                    IF (SUBSTRING(TRIM(body), 1,16)='filter_image_tag', 
                                            SUBSTRING(body, 17, INSTR(body, 'filter_image_untag') - 17)
                                        ,'') as Photo,
                                    SUBSTRING_INDEX(fromJID, '@', 1) as SendUserID,
                                    SUBSTRING_INDEX(toJID, '@', 1) as ReceiveUserID,
                                    FROM_UNIXTIME(sentDate/1000) as CreatedDate
                            from ofmessagearchive 
                            where 
							(fromJID in ('" . $user_id . "@" . XMPP_HOST . "','" . $with_user_id . "@" . XMPP_HOST . "') 
							and toJID in ('" . $user_id . "@" . XMPP_HOST . "','" . $with_user_id . "@" . XMPP_HOST . "'))                           
                            AND body != ''
                            AND body NOT IN ('filter_request_game', 
                                            'filter_message_status_typing',
                                            'filter_message_status_endtyping',
                                            'filter_message_status_delivered')
                            $where
                             order by CreatedDate DESC limit 50
                            ) as temp
                     inner join user u1 ON u1.UserID = temp.SendUserID
                     inner join user u2 ON u2.UserID = temp.ReceiveUserID";

        $query = $this->db->query($sql);
        $result = $query->result();
        $query->free_result();
        return $result;
    }
    
     function getUserToShare($user_id, $offset = 0, $limit = 10, &$total) {
        $datesearch = date('Y-m-d H:i:s', strtotime('-2 hour', strtotime(gmdate('Y-m-d H:i:s'))));
        $photo_url = $this->config->item('s3_photo_url');
        $sql = "call sp_getUserToShare_v117($user_id,$offset,$limit, '$photo_url', '$datesearch', @total)";
        $query = $this->db->query($sql);

        $result = $query->result();
        $query->next_result();
        $query2 = $this->db->query("select @total as total");
        foreach ($query2->result_array() as $row) {
            $total = $row['total'];
        }
        $query->free_result();
        $query2->free_result();
        return $result;
    }

}

?>