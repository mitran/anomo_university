<?php

class Gift_Category_model extends CI_Model {

    function getListDropDown() {
        $this->db->select('*');
        $this->db->from('giftcategory');
        $query = $this->db->get();
        $result = $query->result();
        $data = array();
        foreach ($result as $re) {
            $data[$re->ID] = $re->Name;
        }
        return $data;
    }

    function getInfo($id) {
        $this->db->select('*');
        $this->db->from('giftcategory');
        $this->db->where('ID', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function add($data) {
        $this->db->insert('giftcategory', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('giftcategory', $data, $cond);
    }

    function getListByCondition($cond, $from, $to, $getSumNumber) {
        $this->db->select('*');
        $this->db->from('giftcategory');
        if (count($cond) > 0) {
            if (isset($cond['sort'])) {
                switch ($cond['sort']) {
                    case 'id-asc':
                        $this->db->order_by('ID', 'asc');
                        break;
                    case 'id-desc':
                        $this->db->order_by('ID', 'desc');
                        break;
                        break;
                    case 'name-asc':
                        $this->db->order_by('Name', 'asc');
                        break;
                    case 'name-desc':
                        $this->db->order_by('Name', 'desc');
                        break;
                }
            }
        }
        if ($getSumNumber) {
            $query = $this->db->get();
            return $query->num_rows();
        } else {
            $this->db->limit($from, $to);
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? $query->result() : null;
        }
    }

}

?>