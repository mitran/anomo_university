<?php

class Promo_Code_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function validatePromoCode($code){
        $this->db->select('*');
        $this->db->from('promocode');
        $this->db->where('Code', $code);
        $this->db->where('Status', 0);
        $this->db->where('ExpireDate >', gmdate('Y-m-d H:i:s'));
        $query = $this->db->get();
        return $query->row();
    }
    
        
    function getEmailCode($email){
        $this->db->select('*');
        $this->db->from('email_verification');
        $this->db->where('Email', $email);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addEmailCode($data) {
        $this->db->insert('email_verification', $data);
        return $this->db->insert_id();
    }

    function updateEmailCode($data, $cond) {
        $this->db->update('email_verification', $data, $cond);
    }
    
    function validateEmailCode($email, $code){
        $this->db->select('*');
        $this->db->from('email_verification');
        $this->db->where('Email', $email);
        $this->db->where('Code', $code);
        $query = $this->db->get();
        return $query->row();
    }
}

?>