<?php

class Place_Post_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function add($data) {
        $this->db->insert('placepost', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('placepost', $data, $cond);
    }

    function findById($post_id) {
        $this->db->select('pp.*');
        $this->db->from('placepost pp');
        $this->db->where('ID', $post_id);
        $query = $this->db->get();
        return $query->row();
    }

    function getInfo($post_id) {
        $this->db->select('pp.*, u.UserName, u.Gender, u.Photo as Avatar');
        $this->db->from('placepost pp');
        $this->db->join('user u', 'pp.UserID=u.UserID');
        $this->db->where('ID', $post_id);
        $query = $this->db->get();
        return $query->row();
    }

    function getNumberPostOf($place_id, $type = 'text') {
        $this->db->select('count(ID) as Number');
        $this->db->from('placepost');
        $this->db->where('PlaceID', $place_id);
        if ($type == 'text') {
            $this->db->where('Content !=', "");
            $this->db->where('Photo', "");
        } else {
            $this->db->where('Photo !=', "");
        }
        $query = $this->db->get();
        $row = $query->row();
        return ($row) ? $row->Number : 0;
    }

    function getListPostOf($place_id, $type = 'text', &$total = '', $offset = '', $limit = 0) {
        $where = '';
        if ($type == 'text') {
            $where .= " AND pp.Content != '' AND pp.Photo = ''";
        }
        if ($type == 'picture') {
            $where .= " AND pp.Photo != ''";
        }
        $sql = "select count(*)  as total
                from(select ID, UserID 
                    from placepost pp 
                    where pp.PlaceID='$place_id' $where
                 ) as temp 
                 inner join user u ON temp.UserID=u.UserID AND u.IsBanned != 1
        ";
        $query = $this->db->query($sql);
        $row = $query->row();
        $total = $row ? $row->total : 0;

        // get result
        $sql1 = "select pp.UserID, u.UserName, u.Photo as Avatar, u.Gender, pp.ID, pp.Content, pp.Photo, pp.Photo100, pp.Photo200, pp.Photo300, pp.CreatedDate
                    , (select count(ID) from postcomment where PlacePostID = pp.ID ) as TotalComment
                from(select *
                    from placepost pp 
                    where pp.PlaceID='$place_id' $where
                 ) as pp 
                 inner join user u ON pp.UserID=u.UserID AND u.IsBanned != 1
                 order by pp.CreatedDate desc
                 limit $offset, $limit
        ";
        $query = $this->db->query($sql1);
        return $query->result();
    }

    function getTotalPostInPlace($place_id, $type = 'text') {
        $where = '';
        if ($type == 'text') {
            $where .= " AND pp.Content != '' AND pp.Photo = ''";
        }
        if ($type == 'picture') {
            $where .= " AND pp.Photo != ''";
        }
        $sql = "select count(ID)  as total
                from(select ID, UserID 
                    from placepost pp 
                    where pp.PlaceID='$place_id'  $where
                 ) as temp 
                 inner join user u ON temp.UserID=u.UserID AND u.IsBanned != 1
        ";
        $query = $this->db->query($sql);
        $row = $query->row();
        return ($row) ? $row->total : 0;
    }

    function getListPostOfV2($place_id, $type = 'text', $limit = 0) {
        $this->db->select('pp.Photo');
        $this->db->from('placepost pp');
        $this->db->join('user u', 'u.UserID=pp.UserID');
        $this->db->where('pp.PlaceID', $place_id);
        $this->db->where('pp.IsMark', 0);
        if ($type == 'text') {
            $this->db->where('pp.Content !=', "");
            $this->db->where('pp.Photo', "");
        } else {
            $this->db->where('pp.Photo !=', "");
        }
        $this->db->order_by('pp.CreatedDate', 'DESC');
        if ($limit > 0) {
            $this->db->limit($limit);
        }
        $query = $this->db->get();
        return $query->result();
    }

    function deletePost($post_id) {
        try {
            $this->db->trans_begin();

            $commentIDs = self::getCommentIDOf($post_id);
            if (count($commentIDs) > 0) {
                $this->db->delete('placeactivity', "RefID IN (" . implode(',', $commentIDs) . ") AND Type = 1");
                $this->db->delete('placeactivitydetail', "ContentID IN (" . implode(',', $commentIDs) . ") AND Type = 1");
                $this->db->delete('notificationhistory', "RefID IN (" . implode(',', $commentIDs) . ") AND Type IN (9, 10)"); // comment
                $this->db->delete('notificationhistory', "RefID IN (" . implode(',', $commentIDs) . ") AND Type=14 AND ContentType IN (2,6)"); // comment activity
                // delete this comment
                $this->db->delete('postcomment', array('placepostID' => $post_id));
            }
            $likeIDs = Activity_model::getLikeIDOf($post_id, '2,6');
            if (count($likeIDs) > 0) {
                $this->db->delete('notificationhistory', "RefID IN (" . implode(',', $likeIDs) . ") AND Type=13 AND ContentType IN (2, 6)"); // like activity
            }

            $this->db->delete('placeactivity', "RefID = $post_id AND Type IN (7, 3)"); // post text OR post picture
            $this->db->delete('placeactivitydetail', "ContentID = $post_id AND Type IN (7, 3)"); // post text OR post picture
            $this->db->delete('placepost', array('ID' => $post_id));

            $this->db->delete('activitylike', "ContentID = $post_id AND Type IN (2, 6)");

            // delete hashtag 
            $sql = "delete from hashtag where ActivityID IN (select ActivityID from activity where RefID = $post_id AND Type IN (2, 6))";
            $this->db->query($sql);

            $this->db->delete('activity', "RefID = $post_id AND Type IN (2, 6)");

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }
    }

    function getCommentIDOf($post_id) {
        $this->db->select('ID');
        $this->db->from('postcomment');
        $this->db->where('placepostID', $post_id);
        $query = $this->db->get();
        $result = $query->result();
        $data = array();
        if ($result) {
            foreach ($result as $re) {
                $data[] = $re->ID;
            }
        }
        return $data;
    }

    function getListByCondition($cond, $from, $to, $getSumNumber) {
        $this->db->select('pp.*,p.Name,u.Username');
        $this->db->from('placepost pp');
        $this->db->join('user u', 'u.UserID=pp.UserID');
        $this->db->join('place p', 'pp.PlaceID=p.PlaceID');


        if (count($cond) > 0) {
            if (isset($cond['status'])) {
                $this->db->where('pp.Status', $cond['status']);
            }

            if (isset($cond['keyword'])) {
                $q = "concat(u.Username,' ',p.Name,' ',pp.Content) like '%{$cond['keyword']}%'";
                $this->db->where($q);
            }

            if (isset($cond['sort'])) {
                switch ($cond['sort']) {
                    case 'date-asc':
                        $this->db->order_by('pp.CreatedDate', 'asc');
                        break;
                    case 'date-desc':
                        $this->db->order_by('pp.CreatedDate', 'desc');
                    default:
                        $this->db->order_by('pp.CreatedDate', 'desc');
                        break;
                }
            }
        } else {
            $this->db->order_by('pp.CreatedDate', 'desc');
        }

        if ($getSumNumber) {
            $query = $this->db->get();
            return $query->num_rows();
        } else {
            $this->db->limit($from, $to);
            $query = $this->db->get();
            return ($query->num_rows() > 0) ? $query->result() : null;
        }
    }

    function getListPictureInPlace($cond, &$total, $offset = 0, $limit = 0) {

        $sqlTotal = "select count(ID) as Total from  placepost pp where pp.Photo != ''";
        $totalResult = $this->db->query($sqlTotal);
        $totalRow = $totalResult->row();
        $total = $totalRow?$totalRow->Total:0;
        if ($total > 0){
        
            $sql = "select pp.ID, pp.UserID, pp.Content, u.UserName, p.Name, pp.CreatedDate, pp.Photo as FileName,
                                    IF (Photo200 != '', CONCAT('" . $this->config->item('s3_photo_url') . "',Photo200), '') as Photo200,
                                    IF (pp.Photo != '', CONCAT('" . $this->config->item('s3_photo_url') . "',pp.Photo), '') as Photo
                                    from (select * from placepost pp where pp.Photo != ''  order by pp.CreatedDate DESC limit $limit, $offset) as  pp
                                    left join user u ON (u.UserID = pp.UserID)
                                    left join place p ON (pp.PlaceID=p.PlaceID)

                    ";

            if (isset($cond['sort'])) {
                switch ($cond['sort']) {
                    case 'date-asc':
                        $sql .= " order by pp.CreatedDate asc";
                        break;
                    case 'date-desc':
                        $sql .= " order by pp.CreatedDate desc";
                        break;
                    default:
                        $sql .= " order by pp.CreatedDate desc";
                        break;
                }
            } else {
                $sql .= " order by pp.CreatedDate DESC";
            }
            $query1 = $this->db->query($sql);
            return $query1->result();
        }
        return null;
    }

    function getListPictureInNbh($cond, &$total, $offset = 0, $limit = 0) {
        $sqlTotal = "select count(ID) as Total from neighborhoodpost where Photo != ''";
        $totalResult = $this->db->query($sqlTotal);
        $totalRow = $totalResult->row();
        $total = $totalRow?$totalRow->Total:0;
        if ($total > 0){

            $sql = "select pp.ID, pp.UserID, pp.Content, u.UserName, p.NAME as Name, pp.CreatedDate, pp.Photo as FileName,
                                    IF (Photo200 != '', CONCAT('" . $this->config->item('s3_photo_url') . "',Photo200), '') as Photo200,
                                    IF (pp.Photo != '', CONCAT('" . $this->config->item('s3_photo_url') . "',pp.Photo), '') as Photo
                                    from (select * from neighborhoodpost where Photo != ''  order by CreatedDate DESC  limit $limit, $offset) as pp
                                    left join user u ON (u.UserID = pp.UserID)
                                    left join neighborhood p ON (pp.NeighborhoodID=p.OGR_FID)
                                    
                    ";
            if (isset($cond['keyword'])) {
                $sql .= " AND concat(u.Username,' ',p.NAME,' ',pp.Content) like '%{$cond['keyword']}%'";
            }
            if (isset($cond['sort'])) {
                switch ($cond['sort']) {
                    case 'date-asc':
                        $sql .= " order by pp.CreatedDate asc";
                        break;
                    case 'date-desc':
                        $sql .= " order by pp.CreatedDate desc";
                        break;
                    default:
                        $sql .= " order by pp.CreatedDate desc";
                        break;
                }
            } else {
                $sql .= " order by pp.CreatedDate DESC";
            }
            $query1 = $this->db->query($sql);
            return $query1->result();
        }
        return null;
    }

    function getListPictureStatus($cond, &$total, $offset = 0, $limit = 0) {
        $where = ' AND IsDeleted != 1 ';
        if (isset($cond['keyword'])) {
            $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
            $keyword = mysqli_real_escape_string($con, $cond['keyword']);  
            $where = " AND pp.PictureCaption like '%{$keyword}%'";
        }
        $sqlTotal = "select count(ID) as Total from picturepostactivity pp where pp.Photo != '' $where";
        $totalResult = $this->db->query($sqlTotal);
        $totalRow = $totalResult->row();
        $total = $totalRow?$totalRow->Total:0;
        if ($total > 0){
            $sql = "select pp.ID, pp.UserID, pp.PictureCaption as Content, u.UserName,  pp.CreatedDate, pp.Photo as FileName,
                                    IF (Photo200 != '', CONCAT('" . $this->config->item('s3_photo_url') . "',Photo200), '') as Photo200,
                                    IF (pp.Photo != '', CONCAT('" . $this->config->item('s3_photo_url') . "',pp.Photo), '') as Photo
                                    from (select * from picturepostactivity pp where pp.Photo != '' $where  order by pp.CreatedDate DESC  limit $limit, $offset) as pp
                                    left join user u ON (u.UserID = pp.UserID)

                    ";
            if (isset($cond['sort'])) {
                switch ($cond['sort']) {
                    case 'date-asc':
                        $sql .= " order by pp.CreatedDate asc";
                        break;
                    case 'date-desc':
                        $sql .= " order by pp.CreatedDate desc";
                        break;
                    default:
                        $sql .= " order by pp.CreatedDate desc";
                        break;
                }
            } else {
                $sql .= " order by pp.CreatedDate DESC";
            }

            $query1 = $this->db->query($sql);
            return $query1->result();
        }
    return null;
    }

}

?>