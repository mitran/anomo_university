<?php

class Cron_Mention_User_model extends CI_Model {

    function addMulti($data) {
        $sql = "INSERT INTO cronmentionuser (`UserID`,`FromUserID`,`Type`,`ContentID`,`ContentType`, `IsPush`, `FailCount`,`CreatedDate`, `HistoryID`, `IsAnonymous`)
		VALUE " . implode(',', $data);
        $this->db->query($sql);
    }

    function getList($iLimit = 100) {
        $this->db->from('cronmentionuser');
        $this->db->where('IsPush', 0);
        $this->db->where('FailCount < 3');
        $this->db->limit($iLimit);
        $query = $this->db->get();
        return $query->result();
    }

    function updateStatus($ids, $type = 'success') {
        if ($type == 'success') {
            $sql = "update cronmentionuser set IsPush = 1 where ID IN(" . implode(',', $ids) . ")";
        } else {
            $sql = "update cronmentionuser set  FailCount = `FailCount` + 1 where ID IN(" . implode(',', $ids) . ")";
        }
        $this->db->query($sql);
    }

    /** Save history & push notify to @mentionUser
     * 
     * @param type $text
     * @param type $content_id
     * @param type $content_type
     * 
     * 
     */

    function saveHistoryAndPushNotify($text, $user_id, $content_id, $content_type, $is_anonymous = 0) {
        $json = json_decode($text);
        if ($json) {
            $message = isset($json->message) ? strtolower($json->message) : "";
            $mentionUserObj = isset($json->message_tags) ? $json->message_tags : null;
            $aHistory = array();
            $aPushNotify = array();
            $time = gmdate('Y-m-d H:i:s');
            $aAdminIDs = array();
            // ANOMO-11561 When a user adds to a post @admins, all admins will get notified
            if ($message != '') {
                $matches = array();
                preg_match_all('/@([a-zA-Z0-9]+)/', $message, $matches);
                if (isset($matches[1]) && count($matches[1]) > 0) {
                    $isPushNotifyToAdmin = 0;
                    foreach ($matches[1] as $match) {
                        if ($match == 'admin' || $match == 'admins') {
                            $isPushNotifyToAdmin = 1;
                        }
                    }
                    if ($isPushNotifyToAdmin == 1) {
                        // get all admin user
                        $sql = "select UserID from user where IsAdmin = 1";
                        $query = $this->db->query($sql);
                        $adminObj = $query->result();
                        if ($adminObj) {
                            foreach ($adminObj as $admin) {
                                $aAdminIDs[] = $admin->UserID;
                                if ($admin->UserID != $user_id) {
                                    $aHistory = array(
                                        'UserID' => $admin->UserID,
                                        'SendUserID' => $user_id,
                                        'Type' => 16,
                                        'RefID' => $content_id,
                                        'ContentType' => $content_type,
                                        'ContentID' => $content_id,
                                        'IsAnonymous' => $is_anonymous,
                                        'IsRead' => 1,
                                        'UnView' => 1,
                                        'CreatedDate' => $time
                                    );
                                    $history_id = Notification_History_model::add($aHistory);
                                    if ($history_id) {
                                        $aPushNotify [] = "($admin->UserID, $user_id, 13, $content_id, $content_type, 0, 0, '$time', '$history_id', '$is_anonymous')";
                                    }
                                }
                            }
                        }
                    }
                }
            }


            if ($mentionUserObj) {
                // ANOMO-10054 - Don't allow a user to tag me if I have blocked them
                $aUserID = array();
                foreach ($mentionUserObj as $user) {
                    if (isset($user->id) && isset($user->name) && $user->id > 0) {
                        $aUserID[] = $user->id;
                    }
                }

                if (sizeof($aUserID) > 0) {
                    $aValidUser = self::_validUser($user_id, $aUserID);
                    if (sizeof($aValidUser) > 0) {
                        foreach ($mentionUserObj as $userObj) {
                            // push notify     
                            // dont push to myself
                            if (isset($userObj->id) && $userObj->id != $user_id && in_array($userObj->id, $aValidUser)) {
                                $pos = strpos($message, strtolower($userObj->name));
                                if ($pos !== false && !in_array($userObj->id, $aAdminIDs)) {
                                    // should add to queue
                                    $aHistory = array(
                                        'UserID' => $userObj->id,
                                        'SendUserID' => $user_id,
                                        'Type' => 16,
                                        'RefID' => $content_id,
                                        'ContentType' => $content_type,
                                        'ContentID' => $content_id,
                                        'IsAnonymous' => $is_anonymous,
                                        'IsRead' => 1,
                                        'UnView' => 1,
                                        'CreatedDate' => $time
                                    );
                                    $history_id = Notification_History_model::add($aHistory);
                                    if ($history_id) {
                                        $aPushNotify [] = "($userObj->id, $user_id, 13, $content_id, $content_type, 0, 0, '$time', '$history_id', '$is_anonymous')";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // add to queue to push notify
            if (sizeof($aPushNotify) > 0) {
                self::addMulti(array_unique($aPushNotify));
            }
        }
    }

    function _validUser($user_id, $aUserID = array()) {
        $return = array();
        if (sizeof($aUserID) > 0) {
            $sql = "select UserID from user 
                    where 
                        UserID IN (" . implode(',', $aUserID) . ")
                        AND UserID NOT IN (
                                select IF(CurrentUserID = $user_id, TargetUserID, CurrentUserID) as uID 
                                from userblock 
                                where CurrentUserID = $user_id OR TargetUserID = $user_id)
                    ";
            $query = $this->db->query($sql);
            $userObj = $query->result();
            if ($userObj) {
                foreach ($userObj as $obj) {
                    $return[] = $obj->UserID;
                }
            }
        }
        return $return;
    }

    function addNotifyFollowerToQueue($data) {
        $this->db->insert('notify_follower_queue', $data);
        return $this->db->insert_id();
    }
    
    function updateNotifyFollower($ids){
        $sql = "update notify_follower_queue set Status = 1 where ID IN (".implode(',', $ids).")";
        $this->db->query($sql);
    }

    function getListNotifyFollower($limit = 20) {
        $sql = "select * from notify_follower_queue where Status = 0 limit $limit";
        $query = $this->db->query($sql);
        return $query->result();
    }

}

?>
