<?php

class Activity_model extends CI_Model {

    public $cached = false;
    public $activityType = '';
    public $youtubeVideoUrl = '';
    public $youtubeVideoThumbnailUrl = '';
    public $youtubeImageDefault = '';
    public $photoUrl = '';
    public $vineVideoUrl = '';

    function __construct() {
        parent::__construct();
        $this->db_slave = $this->load->database('slave', TRUE);
        $CI = & get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
        $this->activityType = ACTIVITY_POST_STATUS . ",". ACTIVITY_POST_PICTURE. ",".ACTIVITY_POST_VIDEO; // post status / picture
        // ANOMO-7190
        $CI->load->model('Config_model');
        $isUserCardOn = $CI->Config_model->getConfig('NEW_USER_ACTIVITY_CARD');
        if ($isUserCardOn && $isUserCardOn == 'on') {
            $this->activityType .= ",".ACTIVITY_NEW_USER;
        }
        $this->youtubeVideoUrl = YOUTUBE_VIDEO_URL;
        $this->youtubeVideoThumbnailUrl = YOUTUBE_VIDEO_THUMBNAIL_URL;
        $this->youtubeImageDefault = YOUTUBE_IMAGE_DEFAULT;
        $this->vineVideoUrl = VINE_VIDEO_URL;
        $this->photoUrl = $this->config->item('s3_photo_url');
        $this->videoUrl = $this->config->item('s3_video_url');
        
    }

    function add($data) {
        $this->db->insert('activity', $data);
        self::invalidUserCached($data['FromUserID']);
        return $this->db->insert_id();
    }

    function update($data, $cond, $user_id = 0) {
        if ($user_id > 0){
            self::invalidUserCached($user_id);
        }
        return $this->db->update('activity', $data, $cond);
    }

    function getNearbyActivities_sp($user_id = 0, $hours = 12, $radius = -1, $cutOffVal = 50, &$radiusOut, &$total, $offset = '', $limit = '', $radiusSet,  $gender = 0, $from_age = 0, $to_age = 0, $activity_id = 0) {
        $photoUrl = $this->config->item('s3_photo_url');
        $categoryIconUrl = base_url() . $this->config->item('category_icon_url');
        $radiusSet = str_replace(" ", "", $radiusSet);
        //ANOMO-5775
        $actionType = $this->activityType;

        // end ANOMO-5775
        // ANOMO-7190
        $isUserCardOn = Config_Model::getConfig('NEW_USER_ACTIVITY_CARD');
        if ($isUserCardOn && $isUserCardOn == 'on') {
            $actionType .= ",28";
        }

        $sql = "CALL distanceSearch3_OP_2_v200(" . $user_id . "," . $hours . ",'" . $actionType . "','" . $photoUrl . "','" . $categoryIconUrl . "','" . $radiusSet . "'," . sizeof(explode(',', $radiusSet)) . "," . $offset . "," . $limit . "," . $radius . "," . $cutOffVal . "," . $gender . "," . $from_age . "," . $to_age . "," . $activity_id . ",@radiusOut,@totalResults, " . $total . ")";
        $query1 = $this->db_slave->query($sql);
        $query1Result = array();
        $activities = $query1->result();
        foreach ($activities as $act) {
            $query1Result[] = $act;
        }
        $query1->next_result(); // Dump the extra resultset.

        $query2 = $this->db_slave->query("select @radiusOut as radiusOut, @totalResults as totalResults");
        foreach ($query2->result_array() as $row) {
            $radiusOut = $row['radiusOut'];
            $total = $row['totalResults'];
        }

        $query1->free_result();
        $query2->free_result();

        return $query1Result;
    }

    
    /**get recent activity
     * 
     * @param int $user_id
     * @param int $total
     * @param int $offset
     * @param int $limit
     * @param string $more_type
     * @param int $gender
     * @param int $from_age
     * @param int $to_age
     * @param int $activity_id
     * @return object result
     */
    function getRecentActivities($user_id = 0, &$total, $offset = '', $limit = '', $gender = 0, $from_age = 0, $to_age = 0, $is_favourite_search = 0, $activity_id = 0) {
        $actionType = $this->activityType;
        
        if ($from_age <= 13){
            $from_age = 0;
        }
        if ($to_age > 90) {
            $to_age = 0;
        }
        
        $where = array();
        
        /*
        // 2014-07-09 - ANOMO-10288
        $activity_condition = array();
        $maxID = $activity_id;
        if ($maxID <= 0){
            // temporary get maxID from table instead of shmop
            $maxSql = "select MAX(ActivityID) as maxID from activity";
            $maxQuery = $this->db_slave->query($maxSql);
            $row = $maxQuery->row();
            $maxID = $row?$row->maxID:0;
        }
        if ($maxID > 0){
            $baseActivityID = $maxID - 2500;
            $activity_condition = " ActivityID > ". $baseActivityID;
        }
         * 
         */

        $where []= "FIND_IN_SET(ActionType, '$actionType') > 0 AND IsInvalid != 1";

        // filter by gender
        if ($gender > 0){
            $where []= "Gender = '".$gender."'";
        }
        
        
        // get by ActivityID - fix duplicate issue 
        if ($activity_id > 0){
            $where []= " ActivityID < '".$activity_id."'";
        }
        
        //$condition = $where;
        //array_unshift($condition, $activity_condition);
        $sWhere = implode(' and ', $where);
        
        // filter by age
        //Luong modified - AgeLess user
        $sWhere .= " AND (( 1=1 ";
        if ($from_age > 0){
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-'.$from_age .' year', strtotime($now)));
            $sWhere .= " AND BirthDate <= '".$youngest_date."' ";
        }
        if ($to_age > 0){
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-'.$to_age - 1 .' year', strtotime($now)));
            $sWhere .= " AND BirthDate > '".$oldest_date."' ";
        }
        $sWhere .= " ) OR EXISTS(SELECT IsAgeLess from `user` where UserID = a.FromUserID and IsAgeLess = 1))";
        
        
        //ANOMO-10815 - Filter out anonymous posts have been blocked
        $sWhere .= " AND (a.IsAnonymous = 0 OR (a.IsAnonymous = 1 AND NOT EXISTS(SELECT TargetUserID from `anonymousblock` where CurrentUserID = $user_id and TargetUserID = a.FromUserID))) ";
        
        //ANOMO-11539 - Apply filter and blocking to the Fanpage
        $sWhere .= " AND NOT EXISTS (SELECT TargetUserID from userblock where CurrentUserID = $user_id and TargetUserID = a.FanPage) ";
           
        // get total result
        // if first call then calculate total record
        if ($total <= 0){
            $totalSql = "select count(ActivityID) as Total from activity a where $sWhere";    
            $totalQuery = $this->db_slave->query($totalSql);
            $row = $totalQuery->row();
            $totalQuery->free_result();
            $total = $row?$row->Total:0;
        }

        $sql = "
        SELECT temp.*,
            IF (u.Photo != '', CONCAT('$this->photoUrl',u.Photo), '') AS Avatar,
            '' AS OwnerAvatar,
            IF (Image != '', CONCAT('$this->photoUrl',Image), '') AS Image,
            '' AS PlaceIcon,
            '' AS PlaceID,
            '' AS PlaceName,
            '' AS PlaceAddress,
            0 as IsBlock,
            0 as IsBlockTarget,
            0 as IsReported,
            0 as IsFavorite,
            IF (u.FacebookID != '', 1, '') as FacebookID,
            IF (u.UserName is not null, u.UserName, '') as FromUserName, 
            '' as OwnerName, 
            '' as GiftReceiverName, 
            0 as IsStopReceiveNotify,
            '' AS CheckinPlaceID,
            '' AS CheckinPlaceName,
            0 AS CheckinStatus,
            u.IsAdmin,
            IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,
            
            if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                           if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
           )AS NeighborhoodName
           
           , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
           , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
           , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   
          
           , IF (u2.Photo != '', CONCAT('$this->photoUrl',u2.Photo), '') AS AboutAvatarURL
           , IF (u2.UserName is not null, u2.UserName, '') as AboutName 
           , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName
           , IF (topic.UserID is not null, topic.UserID, '') as ModID
           , IF ((select UserID from topicban where UserID = temp.FromUserID AND TopicID = temp.TopicID) is not null, 1, 0) as IsTopicBan
           FROM (
               select a.ActivityID, a.FromUserID, a.Message, a.Image, a.RefID, a.Type, a.ActionType, a.PlaceID, a.PlaceName, a.PlaceIcon, a.PlaceAddress, a.Lat, a.Lon, a.CreatedDate, a.IsInvalid, a.LastUpdate, a.Gender, a.BirthDate, a.OwnerID, a.OrginalDate, a.GiftReceiverID, a.CheckinPlaceID, a.CheckinPlaceName, a.NeighborhoodID, a.IsAnonymous, a.FanPage
                      , a.VideoID, a.VideoSource, a.VideoThumbnail, a.TopicID
               FROM activity a
               WHERE $sWhere
               ORDER BY CreatedDate DESC LIMIT $offset, $limit
           ) as temp

           LEFT JOIN `user` u ON u.UserID=temp.FromUserID
           LEFT JOIN `user` u2 ON u2.UserID =temp.FanPage
           LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID
           left join topic ON topic.TopicID = temp.TopicID
           where u.IsBanned = 0
           
            ORDER BY temp.ActivityID desc
        ";

        $query = $this->db_slave->query($sql);
        $activities = $query->result();
        $query->free_result();
        
        
        return $activities;
    }
    
    /**get following activity
     * 
     * @param int $user_id
     * @param int $total
     * @param int $offset
     * @param int $limit
     * @param int $gender
     * @param int $from_age
     * @param int $to_age
     * @param int $activity_id
     * @return object
     */
    function getFollowingActivities($user_id = 0, &$total, $offset = '', $limit = '', $gender = 0, $from_age = 0, $to_age = 0, $activity_id = 0, $hashtag = '', $topic_id = 0) {
        $actionType = $this->activityType;
        
        if ($from_age <= 13){
            $from_age = 0;
        }

        if ($to_age > 90) {
            $to_age = 0;
        }

        $where = " FIND_IN_SET(ActionType, '$actionType') > 0 AND IsInvalid != 1";
        
        // filter by topicID
        if ($topic_id > 0){
            $where .= " AND TopicID = $topic_id";
        }
        
        // filter by gender
        if ($gender > 0){
            $where .= " AND Gender = '".$gender."'";
        }
        
        // filter by age
        /*
        if ($from_age > 0){
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-'.$from_age .' year', strtotime($now)));
            $where .= " AND BirthDate <= '".$youngest_date."'";
        }
        if ($to_age > 0){
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-'.$to_age - 1 .' year', strtotime($now)));
            $where .= " AND BirthDate > '".$oldest_date."'";
        }
        */
        
        // filter by age
        //Luong modified - AgeLess user
        $where .= " AND (( 1=1 ";
        if ($from_age > 0){
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-'.$from_age .' year', strtotime($now)));
            $where .= " AND BirthDate <= '".$youngest_date."'";
        }
        if ($to_age > 0){
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-'.$to_age - 1 .' year', strtotime($now)));
            $where .= " AND BirthDate > '".$oldest_date."'";
        }
        $where .= " ) or EXISTS(SELECT IsAgeLess from `user` where UserID = FromUserID and IsAgeLess = 1))";
        //End
        
        //ANOMO-11539 - Apply filter and blocking to the Fanpage
        $where .= " AND NOT EXISTS(SELECT TargetUserID from userblock where CurrentUserID = $user_id and TargetUserID = FanPage) ";
        
        // get by ActivityID - fix duplicate issue 
        if ($activity_id > 0){
            $where .= " AND ActivityID < '".$activity_id."'";
        }
        
        // don't show anonymous post 
        $where .= " AND IsAnonymous != 1";
                
        $left_join1 = "";
        $left_join2 = "";
        if ($hashtag != ''){
            $left_join1 = " left join (select ActivityID from hashtag where HashTag = '".$hashtag."') as h ON h.ActivityID = a.ActivityID ";
            $left_join2 = " left join (select ActivityID from hashtag where HashTag = '".$hashtag."') as h ON h.ActivityID = b.ActivityID ";
            $where .= " AND h.ActivityID is not null";
        }
        
        // get total result
        // if first call then calculate total record
        if ($total <= 0){
            $totalSql = "
                select SUM(totalcount) as Total FROM (
                    (SELECT COUNT(a.ActivityID) as totalcount
                    FROM (SELECT TargetUserID FROM userconnect WHERE CurrentUserID = $user_id AND IsFavorite = 1) fav1
                    left JOIN activity a ON a.FromUserID = fav1.TargetUserID
                    $left_join1
                    WHERE $where
                    ) 

                    UNION ALL

                    (SELECT COUNT(b.ActivityID) as totalcount
                    FROM (SELECT TargetUserID FROM userconnect WHERE CurrentUserID = $user_id AND IsFavorite = 1) fav2
                    left JOIN activity_archive b ON b.FromUserID = fav2.TargetUserID
                    $left_join2
                    WHERE $where
                    )
                ) a ";
            $totalQuery = $this->db_slave->query($totalSql);
            $row = $totalQuery->row();
            $totalQuery->free_result();
            $total = $row?$row->Total:0;
        }
        $activities = array();
        if ($total > 0){
            $sql = "
                SELECT temp.*,
                IF (u.Photo != '', CONCAT('$this->photoUrl',u.Photo), '') AS Avatar,
                '' AS OwnerAvatar,
                IF (Image != '', CONCAT('$this->photoUrl',Image), '') AS Image,
                '' AS PlaceIcon,
                '' AS PlaceID,
                '' AS PlaceName,
                '' AS PlaceAddress,
                IF (ak.ID is not null, 1, 0) as IsLike,
                if (ub1.CurrentUserID is not null, 1, 0) as IsBlock,
                if (ub2.CurrentUserID is not null, 1, 0) as IsBlockTarget,
                IF (fc.ID is not null, 1, 0) as IsReported,
                IF (u.FacebookID != '', 1, '') as FacebookID,
                IF (u.UserName is not null, u.UserName, '') as FromUserName, 
                '' as OwnerName, 
                '' as GiftReceiverName, 
                IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,
                if (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,
                '' AS CheckinPlaceID,
                '' AS CheckinPlaceName,
                0 AS CheckinStatus,
                u.IsAdmin,
                IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,
                if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                               if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
               )AS NeighborhoodName
               
                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   

               , IF (u2.Photo != '', CONCAT('$this->photoUrl',u2.Photo), '') AS AboutAvatarURL
                , IF (u2.UserName is not null, u2.UserName, '') as AboutName
                , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName 
                , IF (topic.UserID is not null, topic.UserID, '') as ModID
                , IF ((select UserID from topicban where UserID = temp.FromUserID AND TopicID = temp.TopicID) is not null, 1, 0) as IsTopicBan
               FROM (
                    SELECT a.ActivityID, a.FromUserID, a.Message, a.Image, a.RefID, a.Type, a.ActionType, a.PlaceID, a.PlaceName, a.PlaceIcon, a.PlaceAddress, a.Lat, a.Lon, a.CreatedDate, a.IsInvalid, a.LastUpdate, a.Gender, a.BirthDate, a.OwnerID, a.OrginalDate, a.GiftReceiverID, a.CheckinPlaceID, a.CheckinPlaceName, a.NeighborhoodID, a.IsAnonymous, a.FanPage
                        , a.VideoID, a.VideoSource, a.VideoThumbnail, a.TopicID
                    FROM (SELECT TargetUserID FROM userconnect WHERE CurrentUserID = $user_id AND IsFavorite = 1) fav1
                    LEFT JOIN activity a ON a.FromUserID = fav1.TargetUserID
                    $left_join1
                    WHERE $where

                    UNION 
                    (
                    SELECT b.ActivityID, b.FromUserID, b.Message, b.Image, b.RefID, b.Type, b.ActionType, b.PlaceID, b.PlaceName, b.PlaceIcon, b.PlaceAddress, b.Lat, b.Lon, b.CreatedDate, b.IsInvalid, b.LastUpdate, b.Gender, b.BirthDate, b.OwnerID, b.OrginalDate, b.GiftReceiverID, b.CheckinPlaceID, b.CheckinPlaceName, b.NeighborhoodID, b.IsAnonymous, b.FanPage
                       , b.VideoID, b.VideoSource, b.VideoThumbnail, b.TopicID
                    FROM (SELECT TargetUserID FROM userconnect WHERE CurrentUserID = $user_id AND IsFavorite = 1) fav2
                    LEFT JOIN activity_archive b ON b.FromUserID = fav2.TargetUserID
                    $left_join2
                    WHERE $where
                    )
                    ORDER BY CreatedDate DESC LIMIT $offset, $limit
               ) as temp

                LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=$user_id
                left JOIN userblock ub1 ON ub1.TargetUserID=temp.FromUserID AND ub1.CurrentUserID=$user_id
                left JOIN userblock ub2 ON ub2.CurrentUserID=temp.FromUserID AND ub2.TargetUserID=$user_id
                LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=$user_id
                LEFT JOIN `user` u ON u.UserID=temp.FromUserID
                LEFT JOIN `user` u2 ON u2.UserID = temp.FanPage 
                LEFT JOIN userconnect cc ON cc.TargetUserID=temp.FromUserID AND cc.CurrentUserID = $user_id
                LEFT JOIN stopreceivenotify srn ON srn.UserID = $user_id AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType
                LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID
                left join topic ON topic.TopicID = temp.TopicID
                where u.IsBanned = 0
                ORDER BY temp.ActivityID desc
            ";
            
            $query = $this->db_slave->query($sql);
            $activities = $query->result();
            $query->free_result();
        }
        return $activities;
    }
    
    /*
     * get popular activities
     */
    function getPopularActivities($user_id = 0, &$total, $offset = '', $limit = '', $gender = 0, $from_age = 0, $to_age = 0, $hashtag = '', $topic_id = 0) {
        $actionType = $this->activityType;
        
        if ($from_age <= 13){
            $from_age = 0;
        }
        if ($to_age > 90) {
            $to_age = 0;
        }
        
        $where = " FIND_IN_SET(ActionType, '$actionType') > 0 AND IsInvalid != 1";
        
        // filter by topic 
        if ($topic_id > 0){
            $where .= " AND TopicID = $topic_id ";
        }
        
        // Hung added:
        $where .= " AND not exists (select Id from popularfeedban WHERE BannedUserID = FromUserID)";
        
        // filter by gender
        if ($gender > 0){
            $where .= " AND Gender = '".$gender."'";
        }
        
        // filter by age
        /*
        if ($from_age > 0){
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-'.$from_age .' year', strtotime($now)));
            $where .= " AND BirthDate <= '".$youngest_date."'";
        }
        if ($to_age > 0){
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-'.$to_age - 1 .' year', strtotime($now)));
            $where .= " AND BirthDate > '".$oldest_date."'";
        }
        */
        
        // filter by age
        //Luong modified - AgeLess user
        $where .= " AND (( 1=1 ";
        if ($from_age > 0){
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-'.$from_age .' year', strtotime($now)));
            $where .= " AND BirthDate <= '".$youngest_date."' ";
        }
        if ($to_age > 0){
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-'.$to_age - 1 .' year', strtotime($now)));
            $where .= " AND BirthDate > '".$oldest_date."' ";
        }
        $where .= " ) or EXISTS(SELECT IsAgeLess from `user` where UserID = a.FromUserID and IsAgeLess = 1))";
        //End
        
        //ANOMO-10815 - Filter out anonymous posts have been blocked
        $where .= " AND (a.IsAnonymous = 0 OR (a.IsAnonymous = 1 AND NOT EXISTS(SELECT TargetUserID from `anonymousblock` where CurrentUserID = $user_id and TargetUserID = a.FromUserID))) ";
                
        //ANOMO-11539 - Apply filter and blocking to the Fanpage
        $where .= " AND NOT EXISTS (SELECT TargetUserID from userblock where CurrentUserID = $user_id and TargetUserID = a.FanPage) ";
                
        $left_join = "";
        if ($hashtag != ''){
            $left_join = " left join (select ActivityID from hashtag where HashTag = '".$hashtag."') as h ON h.ActivityID = a.ActivityID ";
            $where .= " AND h.ActivityID is not null";
        }
        
        if ($total <= 0){
            $totalSql = "select count(a.ActivityID) as Total
                    from
                    (
                            select ContentID, Type, concat(ContentID,'-',Type) as TempID
                            ,count(*) as TotalLike
                            from activitylike
                            where CreatedDate > DATE_SUB(Now(), INTERVAL 16 HOUR)
                            group by TempID ORDER BY TotalLike DESC Limit 1000
                    ) as temp
                    left join activity a on a.RefID=temp.ContentID AND a.ActionType=temp.Type
                    $left_join
                    WHERE $where
                ";            
      
            $totalQuery = $this->db_slave->query($totalSql);
            $row = $totalQuery->row();
            $totalQuery->free_result();
            $total = $row?$row->Total:0;
        }
        $activities = array();
        if ($total > 0){
            $sql = "SELECT temp.*,
                        IF (u.Photo != '', CONCAT('$this->photoUrl',u.Photo), '') AS Avatar,
                        '' AS OwnerAvatar,
                        IF (Image != '', CONCAT('$this->photoUrl',Image), '') AS Image,
                        '' AS PlaceIcon,
                        '' AS PlaceID,
                        '' AS PlaceName,
                        '' AS PlaceAddress,
                        IF (ak.ID is not null, 1, 0) as IsLike,
                        if (ub1.CurrentUserID is not null, 1, 0) as IsBlock,
                        if (ub2.CurrentUserID is not null, 1, 0) as IsBlockTarget,
                        IF (fc.ID is not null, 1, 0) as IsReported,
                        IF (u.FacebookID != '', 1, '') as FacebookID,
                        IF (u.UserName is not null, u.UserName, '') as FromUserName, 
                        '' as OwnerName, 
                        '' as GiftReceiverName, 
                        IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,

                        '' AS CheckinPlaceID,
                        '' AS CheckinPlaceName,
                        0 AS CheckinStatus,
                        u.IsAdmin,
                        IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,

                        if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                       if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                        )AS NeighborhoodName
                        
                        , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                        , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                        , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   
                        

                        , IF (u2.Photo != '', CONCAT('$this->photoUrl',u2.Photo), '') AS AboutAvatarURL
                        , IF (u2.UserName is not null, u2.UserName, '') as AboutName
                        , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName 
                        , IF (topic.UserID is not null, topic.UserID, '') as ModID
                        , IF ((select UserID from topicban where UserID = temp.FromUserID AND TopicID = temp.TopicID) is not null, 1, 0) as IsTopicBan
                   FROM (
                           select a.ActivityID, a.FromUserID, a.Message, a.Image, a.RefID, a.Type, a.ActionType, a.PlaceID, a.PlaceName, a.PlaceIcon
                                                   , a.PlaceAddress, a.Lat, a.Lon, a.CreatedDate, a.IsInvalid, a.LastUpdate, a.Gender, a.BirthDate, a.OwnerID, a.OrginalDate
                                                   , a.GiftReceiverID, a.CheckinPlaceID, a.CheckinPlaceName, a.NeighborhoodID, a.IsAnonymous,a.FanPage
                                                   , a.VideoID, a.VideoSource, a.VideoThumbnail, a.TopicID
                                                   , ak.TotalLike as `Like`
                                                   ,(ak.TotalLike + if(totalcomments, totalcomments, 0)) / 
                                                    (POWER(1.25, TIMESTAMPDIFF(HOUR, CreatedDate, NOW()))) as PopularRank
                           from
                            (
                                select ContentID, Type,count(*) as TotalLike, 
                                    (select count(distinct ac.UserID) from activitycomment ac where ac.ContentID=al.ContentID AND ac.Type=al.Type) as totalcomments
                                     , concat(ContentID,'-',Type) as TempID
                                  from activitylike al
                                  where CreatedDate > DATE_SUB(Now(), INTERVAL 16 HOUR)
                                  group by TempID ORDER BY TotalLike DESC Limit 1000
                             ) as ak
                            left join activity a on a.RefID=ak.ContentID AND a.ActionType=ak.Type
                            $left_join
                                WHERE $where
                           ORDER BY PopularRank DESC LIMIT $offset,$limit
                   ) as temp 

                   LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=$user_id

                   LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=$user_id
                   LEFT JOIN `user` u ON u.UserID=temp.FromUserID
                   LEFT JOIN `user` u2 ON u2.UserID = temp.FanPage
                   LEFT JOIN userblock ub1 ON ub1.TargetUserID=temp.FromUserID AND ub1.CurrentUserID=$user_id
                   LEFT JOIN userblock ub2 ON ub2.CurrentUserID=temp.FromUserID AND ub2.TargetUserID=$user_id
                   LEFT JOIN stopreceivenotify srn ON srn.UserID = $user_id AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType
                   LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID
                   left join topic ON topic.TopicID = temp.TopicID
                   where u.IsBanned = 0
            ";

            $query = $this->db_slave->query($sql);
            $activities = $query->result();
            $query->free_result();
        }
        return $activities;
    }

    function searchHashTag($user_id = 0, &$total, $offset = '', $limit = '', $gender = 0, $from_age = 0, $to_age = 0, $activity_id = 0, $hashtag = '', $topic_id = 0, $cate_id = 0) {
        $actionType = $this->activityType;
        if ($from_age <= 13){
            $from_age = 0;
        }
        if ($to_age > 90) {
            $to_age = 0;
        }
        
        $where = "";
        $sub_where = '';
        // filter by topic or category
        if ($topic_id > 0){
            $sub_where .= " AND TopicID = $topic_id ";
        }
        if ($cate_id > 0){
            $sub_where .= " AND CateID = $cate_id ";
        }
        
        // filter by gender
        if ($gender > 0){
            $where .= " AND Gender = '".$gender."'";
        }
        
        // filter by age
        //Luong modified - AgeLess user
        $where .= " AND (( 1=1 ";
        if ($from_age > 0){
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-'.$from_age .' year', strtotime($now)));
            $where .= " AND BirthDate <= '".$youngest_date."' ";
        }
        if ($to_age > 0){
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-'.$to_age - 1 .' year', strtotime($now)));
            $where .= " AND BirthDate > '".$oldest_date."' ";
        }
        $where .= " ) or EXISTS(SELECT IsAgeLess from `user` where UserID = a.FromUserID and IsAgeLess = 1))";
        //End
        
        if ($total <= 0){
            $totalSql = "
                select count(*) as Total from (
				select ActivityID 
				from hashtag 
				where HashTag = '$hashtag' $sub_where
				GROUP BY ActivityID
				) as temp 
                left join activity a on a.ActivityID = temp.ActivityID
				where a.ActivityID is not null  $where
                AND (a.IsAnonymous = 0 OR (a.IsAnonymous = 1 AND NOT EXISTS(SELECT TargetUserID from `anonymousblock` where CurrentUserID = $user_id and TargetUserID = a.FromUserID)))
            ";
            //ANOMO-11539 - Apply filter and blocking to the Fanpage 
            $totalSql .= " AND NOT EXISTS (SELECT TargetUserID from userblock where CurrentUserID = $user_id and TargetUserID = a.FanPage) ";

            $totalQuery = $this->db_slave->query($totalSql);
            $row = $totalQuery->row();
            $totalQuery->free_result();
            $total = $row?$row->Total:0;
        }
    
        $activities = array();
        if ($total > 0){
            $sql = "
            SELECT temp.*,
                IF (u.Photo != '', CONCAT('$this->photoUrl',u.Photo), '') AS Avatar,
                '' AS OwnerAvatar,
                IF (Image != '', CONCAT('$this->photoUrl',Image), '') AS Image,
                '' AS PlaceIcon,
                '' AS PlaceID,
                '' AS PlaceName,
                '' AS PlaceAddress,
                IF (ak.ID is not null, 1, 0) as IsLike,
                if (ub1.CurrentUserID is not null, 1, 0) as IsBlock,
                if (ub2.CurrentUserID is not null, 1, 0) as IsBlockTarget,
                IF (fc.ID is not null, 1, 0) as IsReported,
                IF (u.FacebookID != '', 1, '') as FacebookID,
                IF (u.UserName is not null, u.UserName, '') as FromUserName, 
                '' as OwnerName, 
                '' as GiftReceiverName, 
                IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,
                if (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,
                '' AS CheckinPlaceID,
                '' AS CheckinPlaceName,
                '' AS CheckinStatus,
                u.IsAdmin,
                IF (temp.PlaceID is not null, '', temp.NeighborhoodID) AS NeighborhoodID,
                if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                               if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
               )AS NeighborhoodName
               
                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   
               

               , IF (u2.Photo != '', CONCAT('$this->photoUrl',u2.Photo), '') AS AboutAvatarURL
               , IF (u2.UserName is not null, u2.UserName, '') as AboutName
               , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName
               , IF (topic.UserID is not null, topic.UserID, '') as ModID
               , IF ((select UserID from topicban where UserID = temp.FromUserID AND TopicID = temp.TopicID) is not null, 1, 0) as IsTopicBan
               FROM (
                     select a.ActivityID, FromUserID, Message, Image, RefID, Type, ActionType, PlaceID, PlaceName, PlaceIcon, PlaceAddress, Lat, Lon, CreatedDate, IsInvalid, LastUpdate, Gender, BirthDate, OwnerID, OrginalDate, GiftReceiverID, CheckinPlaceID, CheckinPlaceName, NeighborhoodID, IsAnonymous, a.FanPage
                            , a.VideoID, a.VideoSource, a.VideoThumbnail, a.TopicID
                     from (select ActivityID from hashtag 
                           where
                            HashTag = '$hashtag' $sub_where
                            GROUP BY ActivityID            		
                    ) as temp 
                    left join activity a ON a.ActivityID = temp.ActivityID
                    where FIND_IN_SET(ActionType, '$actionType') > 0 AND IsInvalid != 1 
                    $where
                    AND NOT EXISTS (SELECT TargetUserID from userblock where CurrentUserID = $user_id and TargetUserID = a.FanPage)
                    AND (a.IsAnonymous = 0 OR (a.IsAnonymous = 1 AND NOT EXISTS(SELECT TargetUserID from `anonymousblock` where CurrentUserID = $user_id and TargetUserID = a.FromUserID)))
                    ORDER BY CreatedDate DESC
                    LIMIT $offset,$limit
               ) as temp
               LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=$user_id
               LEFT JOIN userblock ub1 ON ub1.TargetUserID=temp.FromUserID AND ub1.CurrentUserID=$user_id
               LEFT JOIN userblock ub2 ON ub2.CurrentUserID=temp.FromUserID AND ub2.TargetUserID=$user_id
               LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=$user_id
               LEFT JOIN `user` u ON u.UserID=temp.FromUserID
               LEFT JOIN `user` u2 ON u2.UserID = temp.FanPage
               LEFT JOIN userconnect cc ON cc.TargetUserID=temp.FromUserID AND cc.CurrentUserID = $user_id
               LEFT JOIN stopreceivenotify srn ON srn.UserID = $user_id AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType
               LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID    
               left join topic ON topic.TopicID = temp.TopicID
               where u.IsBanned = 0
               ORDER BY temp.ActivityID desc
            ";
            $query = $this->db_slave->query($sql);
            $activities = $query->result();
            $query->free_result();
        }
        return $activities;
    }

    function searchNearbyHashTag_sp($user_id = 0, $hours = 12, $radius = -1, $cutOffVal = 50, &$radiusOut, &$total, $offset = '', $limit = '', $radiusSet, $gender = 0, $from_age = 0, $to_age = 0, $activity_id = 0, $hashtag) {
        $photoUrl = $this->config->item('s3_photo_url');
        $categoryIconUrl = base_url() . $this->config->item('category_icon_url');
        $radiusSet = str_replace(" ", "", $radiusSet);

        $actionType = $this->activityType;


        $sql = "CALL sp_searchNearbyHashTag_v200(" . $user_id . "," . $hours . ",'" . $actionType . "','" . $photoUrl . "','" . $categoryIconUrl . "','" . $radiusSet . "'," . sizeof(explode(',', $radiusSet)) . "," . $offset . "," . $limit . "," . $radius . "," . $cutOffVal . "," . $gender . "," . $from_age . "," . $to_age . "," . $activity_id . ",'" . $hashtag . "',@radiusOut,@totalResults)";
        $query1 = $this->db_slave->query($sql);
        $query1Result = array();
        $activities = $query1->result();
        foreach ($activities as $act) {
            $query1Result[] = $act;
        }
        $query1->next_result(); // Dump the extra resultset.

        $query2 = $this->db_slave->query("select @radiusOut as radiusOut, @totalResults as totalResults");
        foreach ($query2->result_array() as $row) {
            $radiusOut = $row['radiusOut'];
            $total = $row['totalResults'];
        }

        $query1->free_result();
        $query2->free_result();

        return $query1Result;
    }

    function increase($content_id, $type, $action, $add = 'increase') {
        $table_name = array(
            ACTIVITY_POST_STATUS => 'userprofilestatus', // change profile status
            ACTIVITY_PROMOS => 'promos',
            ACTIVITY_POST_PICTURE => 'picturepostactivity',
            ACTIVITY_POST_VIDEO => 'video'
        );
        if (!in_array($type, array_keys($table_name))) {
            return false;
        }
        $sql = "Update " . $table_name[$type] . " Set `" . ucfirst($action) . "` = `" . ucfirst($action) . "` + 1 WHERE ID = $content_id";
        if ($add == 'decrease') {
            $sql = "Update " . $table_name[$type] . " Set `" . ucfirst($action) . "` = `" . ucfirst($action) . "` - 1 WHERE ID = $content_id";
        }
        return $this->db->query($sql);
    }

    function getContentInfo($content_id, $type) {
        $table_name = array(
            ACTIVITY_POST_STATUS => 'userprofilestatus', // change profile status
            ACTIVITY_POST_PICTURE => 'picturepostactivity',
            ACTIVITY_PROMOS => 'promos',
            ACTIVITY_POST_VIDEO => 'video'
        );
        if (!in_array($type, array_keys($table_name))) {
            return false;
        }
        $userIdField = 'UserID';
        if ($type == 9) { // send gift
            $userIdField = 'SendUserID';
        }
        $cond = '';
        if ($type == ACTIVITY_POST_STATUS || $type == ACTIVITY_POST_PICTURE || $type == ACTIVITY_POST_VIDEO){
            $cond = " AND c.IsDeleted != 1";
        }
        $sql = "select c.*, c.$userIdField as UserID, u.UserName
                        , if (u.FacebookID != '', 1, '') as FacebookID
                        from $table_name[$type] c
						left join user u ON u.UserID=c.$userIdField
			 	where c.ID = $content_id  $cond";
        $query = $this->db_slave->query($sql);
        // Hung added to manually handle data free
        $retValue = $query->row();
        $query->free_result();
        return $retValue;
    }

    function getListComment($content_id, $type, $user_id = 0, $topic_id = 0) {
        $limit = 1000;
        $sql = "select c.ID, c.UserID, u.UserName, u.BirthDate, c.Content, c.CreatedDate, u.IsVendor,
                    if(u.BirthDate is not null,u.BirthDate, '') as BirthDate,
                    u.IsAdmin,
                    c.NumberOfLike, 
                    IF (cl.ID is not null, 1, 0) as IsLike,
                    IF (fc.ID is not null, 1, 0) as IsReported,
                    IF (u.FacebookID != '', 1, '') as FacebookID,
                    IF (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,
                    IF ( u.Photo != '', CONCAT('" . $this->config->item('s3_photo_url') . "',u.Photo), u.Photo) as Avatar
                    ,IF (nbh.OGR_FID is not null, nbh.OGR_FID, '')    as NeighborhoodID
                    ,if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                    if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                    )AS NeighborhoodName,
                    c.IsAnonymous
                    , IF ((select UserID from topicban where UserID = c.UserID AND TopicID = $topic_id) is not null, 1, 0) as IsTopicBan
                    FROM (
                           select * from activitycomment WHERE  ContentID=$content_id AND Type = $type
                           AND UserID NOT IN (select if(CurrentUserID = $user_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = $user_id or TargetUserID = $user_id)
                           order by CreatedDate ASC limit $limit
                         ) as c
                    LEFT JOIN user u ON c.UserID=u.UserID
                    LEFT JOIN commentlike cl ON c.ID = cl.CommentID AND cl.ContentType = $type AND cl.UserID = $user_id
                    LEFT JOIN flagcontent fc ON fc.ContentID=c.ID AND fc.Type=$type AND IsComment = 1 AND fc.UserID=$user_id
                    LEFT JOIN userconnect cc ON cc.TargetUserID = c.UserID AND cc.CurrentUserID = $user_id
                    LEFT JOIN neighborhood nbh ON nbh.OGR_FID = u.NeighborhoodID
                    WHERE u.IsBanned = 0
                    ";
        $query = $this->db_slave->query($sql);
        // Hung added to manually handle data free
        $retValue = $query->result();
        $query->free_result();
        return $retValue;
    }

    function getListLike($content_id, $type, $user_id) {
        $sql = "Select c.UserID, u.UserName, u.BirthDate
                        , u.IsAdmin
                        , IF (u.FacebookID != '', 1, '') as FacebookID
			, IF (u.Photo != '', CONCAT('" . $this->config->item('s3_photo_url') . "',u.Photo), u.Photo) as Avatar
                        , if (NeighborhoodID is null, '', NeighborhoodID) as NeighborhoodID
                        , if (OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                                if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), nbh.NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                                )AS NeighborhoodName
                FROM (
                      select * from activitylike 
                      WHERE ContentID = $content_id AND Type = $type
                            AND UserID NOT IN (select if (CurrentUserID = $user_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = $user_id OR TargetUserID = $user_id)
                     ORDER BY CreatedDate DESC
                ) as c
		        LEFT JOIN `user` u on c.UserID = u.UserID
                left join neighborhood nbh ON nbh.OGR_FID = u.NeighborhoodID
                WHERE u.IsBanned = 0
                ";

        $query = $this->db_slave->query($sql);
        // Hung added to manually handle data free
        $retValue = $query->result();
        $query->free_result();
        return $retValue;
    }

    function comment($user_id, $content_id, $type, $comment, $isAnonymous = 0) {
        $data = array(
            'UserID' => $user_id,
            'ContentID' => $content_id,
            'Type' => $type,
            'Content' => $comment,
            'CreatedDate' => gmdate('Y-m-d H:i:s'),
            'IsAnonymous' => $isAnonymous
        );
        $this->db->insert('activitycomment', $data);
        return $this->db->insert_id();
    }

    function getActivitiesFeedInfo($content_id, $type, $user_id = 0, $filter_type = -1, $from_user_id = -1) {
        //ANOMO-11651 new user card: It should hide new user card of blocked user
        if ($type == ACTIVITY_NEW_USER){
            $sql = "select 0 as `Like`
                        , 0 as `Comment`
                        , 0 as `Share`
                        , 0 as IsLike
                        , 0 as IsReported
                        , 0 as IsComment
                        , 0 as IsStopReceiveNotify
                        , if ((select CurrentUserID from userblock where TargetUserID = $from_user_id AND CurrentUserID = $user_id) is not null, 1, 0) as IsBlock
                        , if ((select CurrentUserID from userblock where CurrentUserID = $from_user_id AND TargetUserID = $user_id) is not null, 1, 0) as IsBlockTarget
                        , if ((select CurrentUserID from userconnect where TargetUserID = $from_user_id AND CurrentUserID = $user_id AND IsFavorite = 1) is not null, 1, 0) as IsFavorite
                ";
             $query = $this->db_slave->query($sql);
            $retValue = $query->row();
            $query->free_result();
            return $retValue;
        }
        $table_name = array(
            ACTIVITY_POST_STATUS => 'userprofilestatus', // change profile status
            ACTIVITY_PROMOS => 'promos', // change tag
            ACTIVITY_POST_PICTURE => 'picturepostactivity',
            ACTIVITY_POST_VIDEO => 'video'
            
        );
        if (!in_array($type, array_keys($table_name))) {
            return false;
        }

        //ANOMO-7868 - This button should only appear when a user has commented on a status
        // return IsComment 

        $isCommentSql = ", (select count(ID) from activitycomment where ContentID = $content_id AND Type = $type AND UserID = $user_id limit 1) as IsComment";

        if ($filter_type != 0)
            $sql = "select c.Like, c.Comment, c.Share $isCommentSql from $table_name[$type] c
			 	where c.ID = $content_id";
        else
            $sql = "select c.Like, c.Comment, c.Share,
                                    if ((select ID from activitylike ak where ak.ContentID = $content_id AND ak.Type = $type AND ak.UserID = $user_id limit 1) is not null, 1, 0) as IsLike,
                                    if ((select CurrentUserID from userblock where TargetUserID = $from_user_id AND CurrentUserID = $user_id) is not null, 1, 0) as IsBlock,
                                    if ((select CurrentUserID from userblock where CurrentUserID = $from_user_id AND TargetUserID = $user_id) is not null, 1, 0) as IsBlockTarget,
                                    if ((select CurrentUserID from userconnect where TargetUserID = $from_user_id AND CurrentUserID = $user_id AND IsFavorite = 1) is not null, 1, 0) as IsFavorite,
                                    IF ((select ID from flagcontent fc where fc.ContentID=$content_id AND fc.Type=$type AND fc.UserID=$user_id) is not null, 1, 0) as IsReported 
                                    $isCommentSql
                                    , IF ((select ID from stopreceivenotify srn where srn.UserID = $user_id AND srn.ContentID = $content_id AND srn.Type = $type) is not null, 1, 0) as IsStopReceiveNotify   
                        from $table_name[$type] c
                        where c.ID = $content_id
                ";

        $query = $this->db_slave->query($sql);
        // Hung added to manually handle data free
        $retValue = $query->row();
        $query->free_result();
        return $retValue;
    }

    function getContentDetail($content_id, $type, $activity_id = 0, $user_id = 0) {
        //ANOMO-7868 - This button should only appear when a user has commented on a status
        // return IsComment 

        $isCommentSql = " (select count(ID) from activitycomment where ContentID = $content_id AND Type = $type AND UserID = $user_id limit 1) as IsComment";

        $sql = "select temp.*
                   , IF (u.Photo != '', CONCAT('" . $this->photoUrl . "',u.Photo), '') as Avatar
                   , u.UserName as FromUserName
                   , u.IsAdmin
                   , IF (Image != '' || Image != null, CONCAT('" . $this->photoUrl . "',Image), '') as Image
                   , '' as PlaceIcon
                   , '' as PlaceID
                   , '' as PlaceName
                   , '' as PlaceAddress
                   , IF ((select count(ID) from activitylike where UserID = $user_id AND ContentID = $content_id AND Type = $type)> 0, 1, 0) as IsLike
                   , (select count(ID) from stopreceivenotify where UserID = $user_id AND ContentID = $content_id AND Type = $type) as IsStopReceiveNotify
                   , $isCommentSql
                   , if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                                if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), nbh.NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                                )AS NeighborhoodName
                   , IF (fc.ID is not null, 1, 0) as IsReported
                   , IF (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite
                   , IF (topic.TopicID is not null, topic.TopicID, '') as TopicID 
                   , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName
                   , IF (topic.UserID is not null, topic.UserID, '') as ModID
                   , IF ((select UserID from topicban where UserID = temp.FromUserID AND TopicID = temp.TopicID) is not null, 1, 0) as IsTopicBan
                   , IF ((select UserID from topicban where UserID = $user_id AND TopicID = temp.TopicID) is not null, 1, 0) as TopicBanStatus
                from (";
        $sql .= "SELECT ActivityID, FromUserID, Message, Image, RefID, Type, ActionType, PlaceID, PlaceName, PlaceIcon, PlaceAddress, Lat, Lon, CreatedDate, IsInvalid, LastUpdate, Gender, BirthDate, OwnerID, OrginalDate, GiftReceiverID, CheckinPlaceID, CheckinPlaceName, NeighborhoodID, IsAnonymous, FanPage
                      
                        , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                        , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                        , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   

                        , VideoSource , TopicID
                      
                from activity where RefID = $content_id AND ActionType = $type AND IsInvalid != 1";

        if ($activity_id > 0) {
            $sql .= " AND ActivityID=" . $activity_id;
        }
        $sql .= ") as temp";
        $sql .= " left join user u ON u.UserID = temp.FromUserID
                  left join neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID 
                  LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=$user_id
                  left JOIN userconnect cc ON cc.TargetUserID=temp.FromUserID AND cc.CurrentUserID=$user_id
                  left join topic ON topic.TopicID = temp.TopicID
                ";

        $query = $this->db_slave->query($sql);
        $row = $query->row();

        // check if this is null - if it is, check the archive table:
        if (!$row) {
            $sql = "select temp.*
                   , IF (u.Photo != '', CONCAT('" . $this->photoUrl . "',u.Photo), '') as Avatar
                   , u.UserName as FromUserName
                   , u.IsAdmin
                   , IF (Image != '' || Image != null, CONCAT('" . $this->photoUrl . "',Image), '') as Image
                   , '' as PlaceIcon
                   , '' as PlaceID
                   , '' as PlaceName
                   , '' as PlaceAddress
                   , IF ((select count(ID) from activitylike where UserID = $user_id AND ContentID = $content_id AND Type = $type)> 0, 1, 0) as IsLike
                   , (select count(ID) from stopreceivenotify where UserID = $user_id AND ContentID = $content_id AND Type = $type) as IsStopReceiveNotify
                   , $isCommentSql
                   , if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                                if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), nbh.NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                                )AS NeighborhoodName
                   , IF (fc.ID is not null, 1, 0) as IsReported
                   , IF (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite
                   , IF (topic.TopicID is not null, topic.TopicID, '') as TopicID 
                   , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName
                   , IF (topic.UserID is not null, topic.UserID, '') as ModID
                   , IF ((select UserID from topicban where UserID = temp.FromUserID AND TopicID = temp.TopicID) is not null, 1, 0) as IsTopicBan
                   , IF ((select UserID from topicban where UserID = $user_id AND TopicID = temp.TopicID) is not null, 1, 0) as TopicBanStatus
                from (";
            $sql .= "SELECT ActivityID, FromUserID, Message, Image, RefID, Type, ActionType, PlaceID, PlaceName, PlaceIcon, PlaceAddress, Lat, Lon, CreatedDate, IsInvalid, LastUpdate, Gender, BirthDate, OwnerID, OrginalDate, GiftReceiverID, CheckinPlaceID, CheckinPlaceName, NeighborhoodID, IsAnonymous, FanPage
                      
                        , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                        , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                        , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   
                      
                        , VideoSource, TopicID
                    from activity_archive where RefID = $content_id AND ActionType = $type AND IsInvalid != 1";

            if ($activity_id > 0) {
                $sql .= " AND ActivityID=" . $activity_id;
            }
            $sql .= ") as temp";
            $sql .= " left join user u ON u.UserID = temp.FromUserID
                      left join neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID 
                      LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=$user_id
                      left JOIN userconnect cc ON cc.TargetUserID=temp.FromUserID AND cc.CurrentUserID=$user_id
                      left join topic ON topic.TopicID = temp.TopicID
                    ";

            $query = $this->db_slave->query($sql);
            $row = $query->row();
        }

        $query->free_result();

        return $row;
    }

    function getListUserIDIsCommented($content_id, $type, $exclude = '', $mentionUserIds = array(), $user_id, $post_owner_id) {

        $sql = "select * from (";

            $sql .= "select DISTINCT UserID FROM activitycomment";
            $sql .= " WHERE ContentID=" . $content_id;
            $sql .= " AND Type=$type";

        if ($exclude != '') {
            $sql .= " AND UserID NOT IN($exclude)";
        }
        // ANOMO-7868 [Server] Ability to stop receiving notifications on specific threads
        $sql .= " AND UserID NOT IN (select UserID from stopreceivenotify where ContentID = $content_id AND Type = $type)";

        // ANOMO-9583 It shouldn't push notification to blocked user who is mention/comment in post
        $sql .= " AND UserID NOT IN (select IF(CurrentUserID = $user_id || CurrentUserID = $post_owner_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID IN( $user_id, $post_owner_id ) OR TargetUserID IN ($user_id, $post_owner_id))";
        
        $sql .= ") as t ";
        // ANOMO-8198 comment on activity: should push notify to mention user
        if (sizeof($mentionUserIds) > 0) {
            $sql .= "UNION 
                select UserID from user where UserID in (" . implode(',', $mentionUserIds) . ")
                    and UserID NOT IN ($exclude)
                    and UserID not in (select UserID from stopreceivenotify where ContentID=$content_id and Type =$type)
                    AND UserID NOT IN (select IF(CurrentUserID = $user_id || CurrentUserID = $post_owner_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID IN ( $user_id, $post_owner_id) OR TargetUserID IN( $user_id, $post_owner_id))
                    ";
        }        
        $query = $this->db_slave->query($sql);
        $retVal = $query->result();
        $query->free_result();

        return $retVal;
    }

    function getComment($comment_id, $type = 0) {
        $sql = "select *
                from activitycomment where ID=" . $comment_id;
        $query = $this->db_slave->query($sql);
        $row = $query->row();
        $query->free_result();
        return $row;
    }

    function getLike($like_id, $type) {
        $sql = "select * from activitylike where ID=" . $like_id . " AND Type=" . $type;
        $query = $this->db_slave->query($sql);
        $row = $query->row();
        $query->free_result();
        return $row;
    }

    function returnUserSetting($user_id) {
        $sql = "select AllowCommentActivityNotice, AllowLikeActivityNotice from user where UserID=$user_id";
        $query = $this->db_slave->query($sql);
        $row = $query->row();
        $query->free_result();
        return $row;
    }

    function getContentShareDetail($content_id, $type) {
        $sql = "SELECT * from activity where RefID = $content_id AND Type = $type  AND ActionType = $type";
        $query = $this->db_slave->query($sql);
        $row = $query->row();
        if (!$row) {
            $sql = "SELECT * from activity_archive where RefID = $content_id AND Type = $type  AND ActionType = $type";
            $query = $this->db_slave->query($sql);
            $row = $query->row();
        }
        $query->free_result();

        return $row;
    }

    function likeSP($data) {
        $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
        $data['UserName'] = mysqli_real_escape_string($con, $data['UserName']);
        $sql = "CALL sp_likeActivity_v200('" . $data['UserID'] . "',
                                        '" . $data['ContentID'] . "',
                                        '" . $data['Type'] . "',
                                        '" . $data['UserName'] . "',
                                        '" . $data['Avatar'] . "',
                                        '" . $data['Lat'] . "',
                                        '" . $data['Lon'] . "',
                                         @numberLike,@isLike,@result,@ownerID,@historyID          
                                        )";
        $return = array();
        try {
            $this->db->trans_begin();
            $query = $this->db->query($sql);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
                $query->next_result();
                $query2 = $this->db->query("select @numberLike as NumberLike,@isLike as IsLike,@result as Result,@ownerID as OwnerID, @historyID as HistoryID");

                foreach ($query2->result_array() as $row) {
                    $return['Return'] = $row['Result'];
                    $return['NumberLike'] = $row['NumberLike'];
                    $return['IsLike'] = $row['IsLike'];
                    $return['OwnerID'] = $row['OwnerID'];
                    $return['HistoryID'] = $row['HistoryID'];
                }

                $query2->free_result();
            }

            if ($this->cached) {
                $age_filter = $this->cached->get('agefilter_' . $data['UserID']);
                $this->cached->delete('activity_' . $age_filter['from_age'] . '_' . $age_filter['to_age']);
                $this->cached->delete('activity_user_' . $data['UserID']);
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }

        return $return;
    }

    function returnMentionUserID($text) {
        $ids = array();
        $json = json_decode($text);
        if ($json) {
            $mentionUserObj = isset($json->message_tags) ? $json->message_tags : null;
            if ($mentionUserObj) {
                foreach ($mentionUserObj as $obj) {
                    if (isset($obj->id)) {
                        $ids[] = $obj->id;
                    }
                }
            }
        }
        return array_unique($ids);
    }

    public function getAgeRangeCache($from_age, $to_age, $offset, $user_id, $total, $limit,  $gender, $activity_id) { // get cache of the current age range...
        $activity_id = 0; // Hung added to remove the need of the activity id since we're always getting from the beginning...
        $cache = $this->cached->get('activity_' . $from_age . '_' . $to_age);
        if ($cache) {
            // if cache's still valid...
            if ((microtime(true) - $cache['cached_time']) < intval(Config_Model::getConfig('CACHE_TIME_OUT'))) {
                $result = $cache['result'];
                $total = $cache['total'];
                return array('result' => $result, 'total' => $total, 'cached_time' => microtime(true));
            } else {
                $limit = 12 * intval(Config_Model::getConfig('CACHE_PAGE_COUNT'));
                $result = $this->getRecentActivities($user_id, $total, 0, $limit,  $gender, $from_age, $to_age, 0, $activity_id);
                $this->cached->set('activity_' . $from_age . '_' . $to_age, array('result' => $result, 'total' => $total, 'cached_time' => microtime(true)));
                return array('result' => $result, 'total' => $total, 'cached_time' => microtime(true));
            }
        } else {
            $limit = 12 * intval(Config_Model::getConfig('CACHE_PAGE_COUNT'));
            $result = $this->getRecentActivities($user_id, $total, 0, $limit,  $gender, $from_age, $to_age, 0, $activity_id);
            $this->cached->set('activity_' . $from_age . '_' . $to_age, array('result' => $result, 'total' => $total, 'cached_time' => microtime(true)));
            return array('result' => $result, 'total' => $total, 'cached_time' => microtime(true));
        }
    }

    function getActivityOfUser($target_user_id, &$total, $offset, $limit) {
        $numberPageCache = Config_Model::getConfig('CACHE_PAGE_COUNT') * 2; // 10 page
        $total = 0;
        $result = array();
        if ($this->cached) {
            if ($offset < ($numberPageCache * $limit)) {
                $dataCache = $this->cached->get('user_activity_' . $target_user_id);
                if ($dataCache) {
                    $result = array_slice($dataCache['result'], $offset, $limit);
                    $total = $dataCache['total'];
                } else {
                    // set new cache
                    $limitCache = $limit * $numberPageCache;
                    $userActivity = self::_getActivityOfUser($target_user_id, 0, $limitCache);
                    if (sizeof($userActivity) > 0) {
                        $total = $userActivity['total'];
                        $result = array_slice($userActivity['result'], $offset, $limit);
                        $this->cached->set('user_activity_' . $target_user_id, $userActivity);
                    }
                }
            } else {
                $userActivity = self::_getActivityOfUser($target_user_id, $offset, $limit);
                if (sizeof($userActivity) > 0) {
                    $total = $userActivity['total'];
                    $result = $userActivity['result'];
                }
            }
        } else {
            $userActivity = self::_getActivityOfUser($target_user_id, $offset, $limit);
            if (sizeof($userActivity) > 0) {
                $total = $userActivity['total'];
                $result = $userActivity['result'];
            }
        }
        return $result;
    }

    private function _getActivityOfUser($target_user_id, $offset, $limit) {
        $actionType = $this->activityType;
        $sqlTotal = "select count(ActivityID) as Total
                     from activity 
                     WHERE ActionType IN ($actionType) AND IsInvalid != 1 
                     AND FromUserID = $target_user_id
                    ";
        $queryTotal = $this->db_slave->query($sqlTotal);
        $rowTotal = $queryTotal->row();
        $total = $rowTotal ? $rowTotal->Total : 0;
        $data = array();
        if ($total > 0) {
            $catUrl = base_url() . $this->config->item('category_icon_url');
            $sql = "select temp.*,            
                    IF (u.Photo != '', CONCAT('$this->photoUrl',u.Photo), '') AS Avatar,
                    IF (Image != '', CONCAT('$this->photoUrl',Image), '') AS Image,
                    IF (PlaceIcon != NULL || PlaceIcon != '', CONCAT('$catUrl',PlaceIcon), '') AS PlaceIcon,
                    IF (temp.PlaceID != NULL || temp.PlaceID != '', temp.PlaceID, '') AS PlaceID,
                    IF (PlaceName != NULL || PlaceName != '', PlaceName, '') AS PlaceName,
                    IF (PlaceAddress != '' || PlaceAddress != NULL, PlaceAddress, '' ) AS PlaceAddress,

                    0 as IsBlock,
                    0 as IsBlockTarget,
                    0 as IsReported,
		    0 as IsLike,
                    IF (u.FacebookID != '', 1, '') as FacebookID,
                    IF (u.UserName is not null, u.UserName, '') as FromUserName, 
                    '' as OwnerName, 
                    IF (u2.UserName is not null, u2.UserName, '') as GiftReceiverName, 
                    0  as IsStopReceiveNotify,
                    '' AS CheckinPlaceID,
                    '' AS CheckinPlaceName,
                    0  AS CheckinStatus,
                    ''  AS NeighborhoodName
                    
                    , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                    , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                    , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   
                    
                    FROM (
                          select a.ActivityID, a.FromUserID, a.Message, a.Image, a.RefID, a.Type, a.ActionType, a.PlaceID, a.PlaceName, a.PlaceIcon, a.PlaceAddress, a.Lat, a.Lon, a.CreatedDate, a.IsInvalid, a.LastUpdate, a.Gender, a.BirthDate, a.OwnerID, a.OrginalDate, a.GiftReceiverID, a.CheckinPlaceID, a.CheckinPlaceName, a.NeighborhoodID
                            , a.VideoID, a.VideoSource, a.VideoThumbnail
                          FROM activity a
                          WHERE ActionType IN ($actionType) AND IsInvalid != 1 
                          AND FromUserID = $target_user_id
                          ORDER BY CreatedDate DESC LIMIT $offset,$limit
                    ) as temp
                    LEFT JOIN `user` u ON u.UserID=temp.FromUserID
                    LEFT JOIN `user` u2 ON u2.UserID = temp.GiftReceiverID
            ";
            $query = $this->db_slave->query($sql);
            $result = $query->result();
            $data = array('result' => $result, 'total' => $total);
        }
        return $data;
    }
    
    function getAllActivityOfUser($user_id, $target_user_id, $activity_id = -1, $isFanPage = 0, $from_age = 0,$to_age = 0) {        
        $limit = 10;
        $where = '';
        if ($activity_id > 0){
            $where .= " AND ActivityID < $activity_id";
        }
        
        // don't show anonypost of user 
        if ($user_id != $target_user_id || $isFanPage == 1){
            $where .= " AND IsAnonymous != 1";
        }
        
        // ANOMO-11733 [server] Should not return new user card for the owner
        if ($user_id == $target_user_id){
            $where .= " AND ActionType != ".ACTIVITY_NEW_USER;
        }
        
        //get fan page's post
        if ($isFanPage == 1){
            $where .= " AND FanPage = $target_user_id ";
            //ANOMO-11539 - Apply filter and blocking to the Fanpage         
            if($user_id != $target_user_id){ 
                if ($from_age <= 13) $from_age = 0;
                if ($to_age > 90) $to_age = 0;                
                $where .= " AND (( 1=1 ";
                if ($from_age > 0){
                    $now = gmdate('Y-m-d');
                    $youngest_date = date('Y-m-d', strtotime('-'.$from_age .' year', strtotime($now)));
                    $where .= " AND BirthDate <= '".$youngest_date."' ";
                }
                if ($to_age > 0){
                    $now = gmdate('Y-m-d');
                    $oldest_date = date('Y-m-d', strtotime('-'.$to_age - 1 .' year', strtotime($now)));
                    $where .= " AND BirthDate > '".$oldest_date."' ";
                }
                $where .= " ) 
                OR FromUserID = FanPage                 
                OR EXISTS(SELECT IsAgeLess from `user` where UserID = a.FromUserID and IsAgeLess = 1)             
                )";                                
            }
            
             $where .= " AND NOT EXISTS(SELECT TargetUserID from `userblock` where (CurrentUserID = $user_id and TargetUserID = a.FromUserID) or (CurrentUserID = a.FromUserID and TargetUserID = $user_id)) ";      
             
        }else{
            $where .= " AND FromUserID = $target_user_id ";
        }
        
        $actionType = $this->activityType;
        $sql = "select temp.*,            
                    IF (u.Photo != '', CONCAT('$this->photoUrl',u.Photo), '') AS Avatar,
                    IF (Image != '', CONCAT('$this->photoUrl',Image), '') AS Image,
                    u.IsAdmin,
                    '' AS PlaceIcon,
                    '' AS PlaceID,
                    '' AS PlaceName,
                    '' AS PlaceAddress,

                    0 as IsBlock,
                    0 as IsBlockTarget,
                    0 as IsReported,
		    0 as IsLike,
                    IF (u.FacebookID != '', 1, '') as FacebookID,
                    IF (u.UserName is not null, u.UserName, '') as FromUserName, 
                    '' as OwnerName, 
                    IF (u2.UserName is not null, u2.UserName, '') as GiftReceiverName, 
                    0  as IsStopReceiveNotify,
                    '' AS CheckinPlaceID,
                    '' AS CheckinPlaceName,
                    0  AS CheckinStatus,
                    IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,
                    IF (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                           if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                    )AS NeighborhoodName
                    
                    , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                    , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                    , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   

                    , IF (topic.TopicID is not null, topic.TopicID, '') as TopicID
                    , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName
                    , IF (topic.UserID is not null, topic.UserID, '') as ModID
                    
                    FROM (
                          select a.ActivityID, a.FromUserID, a.Message, a.Image, a.RefID, a.Type, a.ActionType, a.PlaceID, a.PlaceName, a.PlaceIcon, a.PlaceAddress, a.Lat, a.Lon, a.CreatedDate, a.IsInvalid, a.LastUpdate, a.Gender, a.BirthDate, a.OwnerID, a.OrginalDate, a.GiftReceiverID, a.CheckinPlaceID, a.CheckinPlaceName, a.NeighborhoodID, a.IsAnonymous, a.FanPage
                            , a.VideoID, a.VideoSource, a.VideoThumbnail, a.TopicID
                          FROM activity a
                          WHERE ActionType IN ($actionType) AND IsInvalid != 1                           
                          $where
                          ORDER BY ActivityID DESC LIMIT $limit
                    ) as temp
                    LEFT JOIN `user` u ON u.UserID=temp.FromUserID
                    LEFT JOIN `user` u2 ON u2.UserID = temp.GiftReceiverID
                    LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID
                    left join topic ON topic.TopicID = temp.TopicID
                    ORDER BY temp.ActivityID desc
            ";
        $query = $this->db_slave->query($sql);
        $result = $query->result();
        if (!$result) {
            $sql = "select temp.*,            
                    IF (u.Photo != '', CONCAT('$this->photoUrl',u.Photo), '') AS Avatar,
                    IF (Image != '', CONCAT('$this->photoUrl',Image), '') AS Image,
                    u.IsAdmin,
                    '' AS PlaceIcon,
                    '' AS PlaceID,
                    '' AS PlaceName,
                    '' AS PlaceAddress,

                    0 as IsBlock,
                    0 as IsBlockTarget,
                    0 as IsReported,
		    0 as IsLike,
                    IF (u.FacebookID != '', 1, '') as FacebookID,
                    IF (u.UserName is not null, u.UserName, '') as FromUserName, 
                    '' as OwnerName, 
                    IF (u2.UserName is not null, u2.UserName, '') as GiftReceiverName, 
                    0  as IsStopReceiveNotify,
                    '' AS CheckinPlaceID,
                    '' AS CheckinPlaceName,
                    0  AS CheckinStatus,
                    IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,
                    IF (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                           if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                    )AS NeighborhoodName
                    
                    , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                    , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                    , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID    
                    
                    , IF (topic.TopicID is not null, topic.TopicID, '') as TopicID
                    , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName
                    , IF (topic.UserID is not null, topic.UserID, '') as ModID
                     
                    FROM (
                          select a.ActivityID, a.FromUserID, a.Message, a.Image, a.RefID, a.Type, a.ActionType, a.PlaceID, a.PlaceName, a.PlaceIcon, a.PlaceAddress, a.Lat, a.Lon, a.CreatedDate, a.IsInvalid, a.LastUpdate, a.Gender, a.BirthDate, a.OwnerID, a.OrginalDate, a.GiftReceiverID, a.CheckinPlaceID, a.CheckinPlaceName, a.NeighborhoodID, a.IsAnonymous, a.FanPage
                           , a.VideoID, a.VideoSource, a.VideoThumbnail, a.TopicID
                          FROM activity_archive a
                          WHERE ActionType IN ($actionType) AND IsInvalid != 1 
                          AND FromUserID = $target_user_id
                          $where
                          ORDER BY ActivityID DESC LIMIT $limit
                    ) as temp
                    LEFT JOIN `user` u ON u.UserID=temp.FromUserID
                    LEFT JOIN `user` u2 ON u2.UserID = temp.GiftReceiverID
                    LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID
                    left join topic ON topic.TopicID = temp.TopicID
                    ORDER BY temp.ActivityID desc
            ";
            $query = $this->db_slave->query($sql);
            $result = $query->result();
        }
        return $result;
    }
    
    function getActivityInfo($activity_id){
        $sql = "select * from activity where ActivityID = '".$activity_id."'";
        $query = $this->db_slave->query($sql);
        $row = $query->row();
        if (!$row){
            $sql = "select * from activity_archive where ActivityID = '".$activity_id."'";
            $query = $this->db_slave->query($sql);
            $row = $query->row();
        }
        return $row;
    }
    
    
     
    function getListPromosWithPaging($cond, &$total = 0, $from = 0, $to = 0) {
        $total = 0;
        $where = '';
        if (isset($cond['vendor']) && $cond['vendor'] > 0){
            $where = 'where UserID ='.$cond['vendor'];
        }
        $sqlTotal = "select count(ID) as Total from promos $where " ;
        $queryTotal = $this->db->query($sqlTotal);
        $rowTotal = $queryTotal->row();
        $total = $rowTotal ? $rowTotal->Total : 0;
        if ($total > 0){
            $sql = "select *
                    from promos $where order by ID DESC 
                    limit $to, $from ";
            $query = $this->db->query($sql);
            return $query->result();
        }
        return false;
    }
    
    /**
     * Get all Fan posts of Vendor
     * Version: v207
     */
    function getListFanPostOfVendor($cond, &$total, $from = 0, $to = 0) {        
        $total = 0;
        $where = '';
        if (isset($cond['vendor']) && $cond['vendor'] > 0){
            $where = ' AND FanPage ='.$cond['vendor'];
        }
        
        $actionType = $this->activityType;
        $photoUrl = $this->config->item('s3_photo_url');
        $catUrl = base_url() . $this->config->item('category_icon_url');
        
        $sqlTotal = "        
                  select count(ActivityID) as 'Total'
                  FROM  (select UserID from user where IsVendor = 1) as u
                  inner join activity a ON u.UserID = a.FanPage  
                  WHERE ActionType IN ($actionType) AND IsInvalid != 1 $where                  
                   " ;
        //echo $sqlTotal;die;
        $queryTotal = $this->db->query($sqlTotal);
        $rowTotal = $queryTotal->row();
        $total = $rowTotal ? $rowTotal->Total : 0;
        
        if($total > 0){
            $sql = "select temp.*,                     
                    IF (u.Photo != '', CONCAT('$photoUrl',u.Photo), '') AS Avatar,
                    IF (Image != '', CONCAT('$photoUrl',Image), '') AS Image,
                    IF (PlaceIcon != NULL || PlaceIcon != '', CONCAT('$catUrl',PlaceIcon), '') AS PlaceIcon,
                    IF (temp.PlaceID != NULL || temp.PlaceID != '', temp.PlaceID, '') AS PlaceID,
                    IF (PlaceName != NULL || PlaceName != '', PlaceName, '') AS PlaceName,
                    IF (PlaceAddress != '' || PlaceAddress != NULL, PlaceAddress, '' ) AS PlaceAddress,

                    0 as IsBlock,
                    0 as IsBlockTarget,
                    0 as IsReported,
		    0 as IsLike,
                    IF (u.FacebookID != '', 1, '') as FacebookID,
                    IF (u.UserName is not null, u.UserName, '') as FromUserName, 
                    '' as OwnerName, 
                    IF (u2.UserName is not null, u2.UserName, '') as GiftReceiverName, 
                    0  as IsStopReceiveNotify,
                    '' AS CheckinPlaceID,
                    '' AS CheckinPlaceName,
                    0  AS CheckinStatus,
                    IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,
                    IF (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                           if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                    )AS NeighborhoodName
                    
                    , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                    , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                    , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   
                    
                    FROM  
                    (
                          select a.ActivityID, a.FromUserID, a.Message, a.Image, a.RefID, a.Type, a.ActionType, a.PlaceID, a.PlaceName, a.PlaceIcon, a.PlaceAddress, a.Lat, a.Lon, a.CreatedDate, a.IsInvalid, a.LastUpdate, a.Gender, a.BirthDate, a.OwnerID, a.OrginalDate, a.GiftReceiverID, a.CheckinPlaceID, a.CheckinPlaceName, a.NeighborhoodID, a.IsAnonymous, a.FanPage, vendor.UserName as VendorName
                              , a.VideoID, a.VideoSource, a.VideoThumbnail
                          FROM
                          (select UserID,UserName from user where IsVendor = 1) as vendor 
                          INNER JOIN activity_archive a ON vendor.UserID = a.FanPage
                          WHERE ActionType IN ($actionType) AND IsInvalid != 1                           
                          $where
                          ORDER BY CreatedDate DESC LIMIT $from,$to
                    ) as temp
                    LEFT JOIN `user` u ON u.UserID=temp.FromUserID
                    LEFT JOIN `user` u2 ON u2.UserID = temp.GiftReceiverID
                    LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID
                    ORDER BY temp.ActivityID desc
            ";                    
        $query = $this->db_slave->query($sql);
        $result = $query->result();
        if (!$result) {
            $sql = "select temp.*,                           
                    IF (u.Photo != '', CONCAT('$photoUrl',u.Photo), '') AS Avatar,
                    IF (Image != '', CONCAT('$photoUrl',Image), '') AS Image,
                    IF (PlaceIcon != NULL || PlaceIcon != '', CONCAT('$catUrl',PlaceIcon), '') AS PlaceIcon,
                    IF (temp.PlaceID != NULL || temp.PlaceID != '', temp.PlaceID, '') AS PlaceID,
                    IF (PlaceName != NULL || PlaceName != '', PlaceName, '') AS PlaceName,
                    IF (PlaceAddress != '' || PlaceAddress != NULL, PlaceAddress, '' ) AS PlaceAddress,

                    0 as IsBlock,
                    0 as IsBlockTarget,
                    0 as IsReported,
		    0 as IsLike,
                    IF (u.FacebookID != '', 1, '') as FacebookID,
                    IF (u.UserName is not null, u.UserName, '') as FromUserName, 
                    '' as OwnerName, 
                    IF (u2.UserName is not null, u2.UserName, '') as GiftReceiverName, 
                    0  as IsStopReceiveNotify,
                    '' AS CheckinPlaceID,
                    '' AS CheckinPlaceName,
                    0  AS CheckinStatus,
                    IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,
                    IF (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                           if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                    )AS NeighborhoodName
                    
                    , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                    , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                    , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   

                    FROM  
                    (
                          select a.ActivityID, a.FromUserID, a.Message, a.Image, a.RefID, a.Type, a.ActionType, a.PlaceID, a.PlaceName, a.PlaceIcon, a.PlaceAddress, a.Lat, a.Lon, a.CreatedDate, a.IsInvalid, a.LastUpdate, a.Gender, a.BirthDate, a.OwnerID, a.OrginalDate, a.GiftReceiverID, a.CheckinPlaceID, a.CheckinPlaceName, a.NeighborhoodID, a.IsAnonymous, a.FanPage, vendor.UserName as VendorName
                             , a.VideoID, a.VideoSource, a.VideoThumbnail
                          FROM
                          (select UserID,UserName from user where IsVendor = 1) as vendor 
                          INNER JOIN activity a ON vendor.UserID = a.FanPage
                          WHERE ActionType IN ($actionType) AND IsInvalid != 1                           
                          $where
                          ORDER BY CreatedDate DESC LIMIT $from,$to
                    ) as temp
                    LEFT JOIN `user` u ON u.UserID=temp.FromUserID
                    LEFT JOIN `user` u2 ON u2.UserID = temp.GiftReceiverID
                    LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID
                    ORDER BY temp.ActivityID desc
            ";            
            $query = $this->db_slave->query($sql);
            $result = $query->result();
            }
        }
        
        
        return $result;
    } 
    
    function getFanPostToVendorInfo($activity_id){
        $sql = "select a.*,u.UserName as 'VendorName' from activity a inner join user u on a.FanPage = u.UserID 
                where ActivityID = '".$activity_id."'";
        $query = $this->db_slave->query($sql);
        $row = $query->row();
        if (!$row){
            $sql = "select a.*,u.UserName as 'VendorName' from activity_archive a inner join user u on a.FanPage = u.UserID where ActivityID = '".$activity_id."'";            
            $query = $this->db_slave->query($sql);
            $row = $query->row();
        }
        return $row;
    }
    
    /** should delete user activity cache when delete activity
     * 
     * @param type $user_id
     */
    function invalidUserCached($user_id) {
        if ($this->cached && $user_id > 0) {
            $this->cached->delete('user_activity_' . $user_id);
            $this->cached->delete('activity_user_' . $user_id);
            $this->cached->delete('recent_picture' . $user_id);
            $age_filter = $this->cached->get('agefilter_' . $user_id);
            if ($age_filter)
                $this->cached->delete('activity_' . $age_filter['from_age'] . '_' . $age_filter['to_age']);
        }
    }
    
    //search function in activity feed screen
    // 1. Search user
    /**
     * 
     * @param type $keyword
     * @return types
     */
    function searchUser($keyword, $user_id){
        $photo_url = $this->config->item('s3_photo_url');
        $con = mysqli_connect($this->db_slave->hostname, $this->db_slave->username, $this->db_slave->password);
        $keyword = mysqli_real_escape_string($con, $keyword);
        $limit = 3;
        $sql = "select u.UserID, u.UserName, u.Avatar, u.BirthDate, u. NeighborhoodID,
                    if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                        if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), nbh.NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                    )AS NeighborhoodName

                from(
                    select UserID, UserName, BirthDate, NeighborhoodID, OnlyShowCountry
                        ,IF (Photo != '', CONCAT('" . $photo_url . "', Photo), '') as Avatar
                    from user
                    where UserID != $user_id
                    AND LCASE(UserName) like '%".$keyword."%'
                    AND UserID NOT IN (select if(CurrentUserID = $user_id, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = $user_id or TargetUserID = $user_id)
                    order by UserName = '$keyword' desc
                    limit $limit
                ) as u
                left join neighborhood nbh ON nbh.OGR_FID = u.NeighborhoodID
            ";
        $query = $this->db_slave->query($sql);
        return $query->result();
    }
    
    function getCommentIDOf($content_id, $type) {
        $this->db->select('ID');
        $this->db->from('activitycomment');
        $this->db->where('ContentID', $content_id);
        $this->db->where('Type', $type);
        $query = $this->db->get();
        $result = $query->result();
        $data = array();
        if ($result) {
            foreach ($result as $re) {
                $data[] = $re->ID;
            }
        }
        return $data;
    }

    function getLikeIDOf($content_id, $type) {
        $sql = "select ID from activitylike where ContentID=$content_id AND Type =$type";
        $query = $this->db_slave->query($sql);
        $result = $query->result();
        $data = array();
        if ($result) {
            foreach ($result as $re) {
                $data[] = $re->ID;
            }
        }
        return $data;
    }
    
    function findHashTag($keyword, $cate_id = 0, $topic_id = 0){
        $con = mysqli_connect($this->db_slave->hostname, $this->db_slave->username, $this->db_slave->password);
        $keyword = mysqli_real_escape_string($con, $keyword);
        $limit = 3;
        $where = array();
        if ($cate_id > 0){
            $where[] = " CateID = $cate_id";
        }
        if ($topic_id > 0){
            $where[] = " TopicID = $topic_id";
        }
        $sWhere = implode( ' AND ', $where);
        $sWhere = !empty($sWhere) ? $sWhere . ' AND ': $sWhere;
        
        $sql = "select distinct HashTag from hashtag where $sWhere HashTag like '". $keyword ."%' limit $limit" ;
        $query = $this->db_slave->query($sql);
        return $query->result();
    }
    
    function getActivities($user_id = 0, $hours = 12, $radius = -1, $cutOffVal = 50, $type = 0, &$radiusOut, &$total, $offset = '', $limit = '', $radiusSet = '15,40,100,300,1000,3000,8000,13000,20000', $gender = 0, $from_age = 0, $to_age = 0, $activity_id = 0, $hashtag = '', $lat, $lon, $topic_id = 0, $cate_id = 0) {
        $radiusOut = -1;
        switch ($type) {
            case FEED_RECENT: // recent activity
                // search recent hashtag
                if ($hashtag != ''){
                    if ($activity_id != 0)
                            $offset = 0;
                    return $this->searchHashTag($user_id, $total, $offset, $limit, $gender, $from_age, $to_age, $activity_id, $hashtag, $topic_id, $cate_id);
                }else{
                    if ($topic_id > 0){
                        if ($activity_id != 0)
                            $offset = 0;
                        $result = $this->getRecentActivitiesByTopicID($user_id, $total, $offset, $limit,  $gender, $from_age, $to_age, 0, $activity_id, $topic_id);
                    }else{
                
                $result = null;
                if ($this->cached) {
                    if ($offset < (intval(Config_Model::getConfig('CACHE_PAGE_COUNT')) * $limit)) {
                        $this->cached->set('agefilter_' . $user_id, array('from_age' => $from_age, 'to_age' => $to_age));
                        // get the user cache... first:
                        $user_cache = $this->cached->get('activity_user_' . $user_id);
                        if ($user_cache) {
                            if ((microtime(true) - $user_cache['cached_time']) < intval(Config_Model::getConfig('CACHE_TIME_OUT'))) {
                                $temp_result = $user_cache['result'];
                                $result = array_slice($temp_result, $offset, $limit);
                                $total = $user_cache['total'];
                            } else {
                                // if it's page 1, then reset cache, otherwise don't bother:
                                if ($offset == 0) {
                                    $ageRangeCache = $this->getAgeRangeCache($from_age, $to_age, $offset, $user_id, $total, $limit,  $gender, $activity_id);
                                    // cache this mofo...
                                    $this->cached->set('activity_user_' . $user_id, $ageRangeCache);
                                    $temp_result = $ageRangeCache['result'];
                                    $result = array_slice($temp_result, $offset, $limit);
                                    $total = $ageRangeCache['total'];
                                } else {
                                    $temp_result = $user_cache['result'];
                                    $result = array_slice($temp_result, $offset, $limit);
                                    $total = $user_cache['total'];
                                }
                            }
                        } else {
                            $ageRangeCache = $this->getAgeRangeCache($from_age, $to_age, $offset, $user_id, $total, $limit,  $gender, $activity_id);
                            // cache this mofo...
                            $this->cached->set('activity_user_' . $user_id, $ageRangeCache);
                            $temp_result = $ageRangeCache['result'];
                            $result = array_slice($temp_result, $offset, $limit);
                            $total = $ageRangeCache['total'];
                        }
                    } else {
                        if ($activity_id != 0)
                            $offset = 0;
                        $result = $this->getRecentActivities($user_id, $total, $offset, $limit,  $gender, $from_age, $to_age, 0, $activity_id);
                    }
                }
                else {
                    if ($activity_id != 0)
                        $offset = 0;
                    $result = $this->getRecentActivities($user_id, $total, $offset, $limit,  $gender, $from_age, $to_age, 0, $activity_id);
                }
                    }
                }
                return $result;
                break;
            case FEED_NEARBY: // nearby activity
                if ($activity_id != 0)
                    $offset = 0;
                // search  nearby hashtag
                if ($hashtag != ''){
                    return $this->searchNearbyHashTag($user_id, $hours, $radius, $cutOffVal, $radiusOut, $total, $offset, $limit, $radiusSet, $gender, $from_age, $to_age, $activity_id, $hashtag, $lat, $lon, $topic_id, $cate_id);
                }else{
                    return $this->getNearbyActivities($user_id, $hours, $radius, $cutOffVal, $radiusOut, $total, $offset, $limit, $radiusSet,  $gender, $from_age, $to_age, $activity_id, $lat, $lon, $topic_id);
                }
                break;
            case FEED_POPULAR: // popular activity
                return $this->getPopularActivities($user_id, $total, $offset, $limit, $gender, $from_age, $to_age, $hashtag, $topic_id);
                break;
            case FEED_FOLLOWING: // following activity
                if ($activity_id != 0)
                    $offset = 0;
                return $this->getFollowingActivities($user_id, $total, $offset, $limit,  $gender, $from_age, $to_age, $activity_id, $hashtag, $topic_id);
                break;
            case FEED_FAVORITE: // following activity
                if ($activity_id != 0)
                    $offset = 0;
                return $this->getFavoriteActivities($user_id, $total, $offset, $limit,  $gender, $from_age, $to_age, $activity_id, $hashtag);
                break;
            case FEED_RECENT_HASHTAG: // recent hashtag
                 return $this->searchHashTag($user_id, $total, $offset, $limit, $gender, $from_age, $to_age, $activity_id, $hashtag, $topic_id, $cate_id);
                break;
            case FEED_NEARBY_HASHTAG: // nearby hashtag
                if ($activity_id != 0)
                    $offset = 0;
                return $this->searchNearbyHashTag($user_id, $hours, $radius, $cutOffVal, $radiusOut, $total, $offset, $limit, $radiusSet, $gender, $from_age, $to_age, $activity_id, $hashtag, $lat, $lon, $topic_id, $cate_id);
                break;
            default:
                return null;
                break;
        }
    }
    
    /**ANOMO-9399
     * 
     * @param type $user_id
     */
    function getRecentPictureOfUser($user_id){
        if ($this->cached){
            $cached = $this->cached->get('recent_picture'.$user_id);
            if ($cached){
                return $cached;
            }else{
                $recent_picture = self::_getRecentPictureOfUser($user_id);
                if ($recent_picture){
                    $this->cached->set('recent_picture'.$user_id, $recent_picture);
                }
                return $recent_picture;
            }
        }else{
            return self::_getRecentPictureOfUser($user_id);
        }
    }
    
    function _getRecentPictureOfUser($user_id){
        $limit = 4;
        $sql = "select ActivityID as ID, IF (Image != '', CONCAT('" . $this->photoUrl . "', Image), '') as Photo
                from activity 
                where FromUserID = $user_id AND Type = 27 AND IsInvalid != 1 AND Image != ''
                order by ActivityID DESC
                limit $limit
            ";
        $query = $this->db_slave->query($sql);
        return $query->result();
    }
    
    function deleteActivity($content_id, $type, $owner_id) {
        $ret = 0;
        $this->db->trans_begin();
        $commentIDs = self::getCommentIDOf($content_id, $type);
        if (count($commentIDs) > 0) {
            $this->db->delete('notificationhistory', "RefID IN (" . implode(',', $commentIDs) . ") AND Type=14 AND ContentType=$type"); // comment activity
        }
        $likeIDs = self::getLikeIDOf($content_id, $type);
        if (count($likeIDs) > 0) {
            $this->db->delete('notificationhistory', "RefID IN (" . implode(',', $likeIDs) . ") AND Type=13 AND ContentType=$type"); // like activity
        }
        //ANOMO - 11680 - Remove related notifications if post has been deleted
        $this->db->delete('notificationhistory',array('RefID' => $content_id,'ContentType' => $type));
        
        self::update(array('IsInvalid' => 1), array('RefID' => $content_id, 'Type' => $type), $owner_id);
        $this->db->update('activity_archive', array('IsInvalid' => 1), array('RefID' => $content_id, 'Type' => $type));
        
        $table = array(
          ACTIVITY_POST_STATUS => 'userprofilestatus'  ,
          ACTIVITY_POST_PICTURE => 'picturepostactivity',
          ACTIVITY_POST_VIDEO => 'video'
        );
        
        $table_name = isset($table[$type])?$table[$type]:'';
        if (!empty($table_name)){
            $this->db->update($table_name, array('IsDeleted' => 1, 'DeletedDate' => gmdate('Y-m-d H:i:s')), array('ID' => $content_id));
        }
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
            $ret = 1;
        }
        return $ret;
    }
    
    function isLike($user_id, $content_id, $type){
        $this->db_slave->select('ID');
        $this->db_slave->from('activitylike');
        $this->db_slave->where('UserID', $user_id);
        $this->db_slave->where('ContentID', $content_id);
        $this->db_slave->where('Type', $type);
        $query = $this->db_slave->get();
        return $query->row();
    }
    
    function unLike($user_id, $content_id, $type){
        return $this->db->delete('ActivityLike', array('UserID' => $user_id, 'ContentID' => $content_id, 'Type' => $type));
    }
    
     function addLike($data){
        $this->db->insert('ActivityLike', $data);
        return $this->db->insert_id();
    }
    
    
    function getNearbyActivities($user_id = 0, $hours = 12, $radius = -1, $cutOffVal = 50, &$radiusOut, &$total, $offset = '', $limit = '', $radiusSet,  $gender = 0, $from_age = 0, $to_age = 0, $activity_id = 0, $lat, $lon, $topic_id = 0) {
        $photoUrl = $this->config->item('s3_photo_url');
        $datesearch = date('Y-m-d H:i:s', strtotime( "-$hours hour", strtotime(gmdate('Y-m-d H:i:s'))));
        //ANOMO-5775
        $actionType = $this->activityType;

        if ($from_age <= 13){
            $from_age = 0;
        }
        if ($to_age > 90) {
            $to_age = 0;
        }
        
        $where = " AND FIND_IN_SET(ActionType, '$actionType') > 0 AND IsInvalid != 1";
        
        // filter by TopicID 
        if ($topic_id > 0){
            $where .= " AND TopicID = $topic_id ";
        }
      
        // filter by gender
        if ($gender > 0){
            $where .= " AND Gender = '".$gender."'";
        }
        
        
        
        // filter by age
        //Luong modified - AgeLess user
        $where .= " AND (( 1=1 ";
        if ($from_age > 0){
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-'.$from_age .' year', strtotime($now)));
            $where .= " AND BirthDate <= '".$youngest_date."'";
        }
        if ($to_age > 0){
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-'.$to_age - 1 .' year', strtotime($now)));
            $where .= " AND BirthDate > '".$oldest_date."'";
        }
        $where .= " ) or EXISTS(SELECT IsAgeLess from `user` where UserID = FromUserID and IsAgeLess = 1))";
        //End
        
        
        if ($activity_id > 0){
            $where .= " AND ActivityID < '".$activity_id."'";
        }
          
        //ANOMO-10815 - Filter out anonymous posts have been blocked
        $where .= " AND (IsAnonymous = 0 OR (IsAnonymous = 1 AND NOT EXISTS(SELECT TargetUserID from `anonymousblock` where CurrentUserID = $user_id and TargetUserID = FromUserID))) ";
        
        //ANOMO-11539 - Apply filter and blocking to the Fanpage
        $where .= " AND NOT EXISTS (SELECT TargetUserID from userblock where CurrentUserID = $user_id and TargetUserID = FanPage) ";
        
        
        // end ANOMO-5775
        // ANOMO-7190
        $isUserCardOn = Config_Model::getConfig('NEW_USER_ACTIVITY_CARD');
        if ($isUserCardOn && $isUserCardOn == 'on') {
            $actionType .= ",28";
        }

        $activities = array();
        
        // get user's location
        $startLat = floatval($lat);
        $startLon = floatval($lon);
        $distanceType = 1;
        $distanceFactor = 69;// miles
        if ($distanceType != 1){
            $distanceFactor = 111.04;//km
        }
        $radiusIn = $radius;
        if ($radiusIn > - 1){ // ****** radius was provided so use it ******
            $dist = $radiusIn;
            $lon1 = $startLon - $dist / abs(cos(deg2rad($startLat)) * $distanceFactor) ;
            $lon2 = $startLon + $dist / abs(cos(deg2rad($startLat)) * $distanceFactor); 
            $lat1 = $startLat - ($dist / $distanceFactor);
            $lat2 = $startLat + ($dist / $distanceFactor);
            $isMeridian180 = 0;
            if ($lon1 < - 180){
                $isMeridian180 = 1;
                $lon1_2 = 360 + $lon1;
                $lon2_2 = 179.999;
                $lat1_2 = $lat1;
                $lat2_2 = $lat2;
                $lon1 = -179.999;
            }else if ($lon2 > 180){
                $isMeridian180 = 1;
                $lon1_2 = -179.999;
                $lon2_2 = -360 + $lon2;
                $lat1_2 = $lat1;
                $lat2_2 = $lat2;
                $lon2 = 179.999;
            }
            
            if ($isMeridian180 == 0){
                
                $totalSql = "select count(*) as Total
                            from
                            (
                                    select haversineDistance($startLat,$startLon,activity.Lat,activity.Lon,$distanceType) AS Distance
                                    from activity
                                    WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                        AND Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2
                                        $where
                                    HAVING  Distance < $dist
                            ) as t
                        ";
                $totalQuery = $this->db_slave->query($totalSql);
                $row = $totalQuery->row();
                $totalQuery->free_result();
                $total = $row ? $row->Total : 0;
                
                
                if ($total > 0){
                    $sql = "SELECT temp.*,
                                IF (u.Photo != '', CONCAT('$photoUrl',u.Photo), '') AS Avatar,
                                '' AS OwnerAvatar,
                                IF (Image != '', CONCAT('$photoUrl',Image), '') AS Image,
                                '' AS PlaceIcon,
                                '' AS PlaceID,
                                '' AS PlaceName,
                                '' AS PlaceAddress,
                                IF (ak.ID is not null, 1, 0) as IsLike,
                                if (ub1.CurrentUserID is not null, 1, 0) as IsBlock,
                                if (ub2.CurrentUserID is not null, 1, 0) as IsBlockTarget,
                                IF (fc.ID is not null, 1, 0) as IsReported,
                                IF (u.FacebookID != '', 1, '') as FacebookID,
                                IF (u.UserName is not null, u.UserName, '') as FromUserName, 
                                '' as OwnerName, 
                                '' as GiftReceiverName, 
                                IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,

                                '' AS CheckinPlaceID,
                                '' AS CheckinPlaceName,
                                0 AS CheckinStatus,
                                u.IsAdmin,
                                IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,

                                if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                               if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                                )AS NeighborhoodName
                                
                                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                                , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   
                                
                                , IF (u2.Photo != '', CONCAT('$this->photoUrl',u2.Photo), '') AS AboutAvatarURL
                                , IF (u2.UserName is not null, u2.UserName, '') as AboutName
                                , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName
                                , IF (topic.UserID is not null, topic.UserID, '') as ModID
                                , IF ((select UserID from topicban where UserID = temp.FromUserID AND TopicID = temp.TopicID) is not null, 1, 0) as IsTopicBan
                           FROM (
                                   
                                select ActivityID, FromUserID, Message, Image, RefID, Type, ActionType, PlaceID, PlaceName, PlaceIcon, PlaceAddress, Lat, Lon, CreatedDate, IsInvalid, LastUpdate, Gender, BirthDate, OwnerID, OrginalDate, GiftReceiverID, CheckinPlaceID, CheckinPlaceName, NeighborhoodID,
                                    haversineDistance($startLat, $startLon, activity.Lat, activity.Lon, $distanceType) AS Distance, IsAnonymous, FanPage, VideoID, VideoSource, VideoThumbnail, TopicID
                                FROM activity
                                WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                AND Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2
                                $where

                                HAVING Distance < $dist ORDER BY CreatedDate DESC LIMIT $offset, $limit

                           ) as temp 

                           LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=$user_id

                           LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=$user_id
                           LEFT JOIN `user` u ON u.UserID=temp.FromUserID
                           LEFT JOIN `user` u2 ON u.UserID = temp.FanPage
                           LEFT JOIN userblock ub1 ON ub1.TargetUserID=temp.FromUserID AND ub1.CurrentUserID=$user_id
                           LEFT JOIN userblock ub2 ON ub2.CurrentUserID=temp.FromUserID AND ub2.TargetUserID=$user_id
                           LEFT JOIN stopreceivenotify srn ON srn.UserID = $user_id AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType
                           LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID
                           left join topic ON topic.TopicID = temp.TopicID
                           where u.IsBanned = 0
                           ORDER BY temp.ActivityID desc
                    ";
                    $query = $this->db_slave->query($sql);
                    $activities = $query->result();
                    $query->free_result();
                }

                
            }else{ //isMeridian180 != 0
                $totalSql = "select count(*) as Total
                            from
                            (
                                    select haversineDistance($startLat,$startLon,activity.Lat,activity.Lon,$distanceType) AS Distance
                                    from activity
                                    WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                        AND ( (Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2) OR (Lon BETWEEN $lon1_2 AND $lon2_2 AND Lat BETWEEN $lat1_2 AND $lat2_2))
                                        $where
                                    HAVING  Distance < $dist
                            ) as t
                        ";
                $totalQuery = $this->db_slave->query($totalSql);
                $row = $totalQuery->row();
                $totalQuery->free_result();
                $total = $row ? $row->Total : 0;
                
                
                if ($total > 0){
                    $sql = "SELECT temp.*,
                                IF (u.Photo != '', CONCAT('$photoUrl',u.Photo), '') AS Avatar,
                                '' AS OwnerAvatar,
                                IF (Image != '', CONCAT('$photoUrl',Image), '') AS Image,
                                '' AS PlaceIcon,
                                '' AS PlaceID,
                                '' AS PlaceName,
                                '' AS PlaceAddress,
                                IF (ak.ID is not null, 1, 0) as IsLike,
                                if (ub1.CurrentUserID is not null, 1, 0) as IsBlock,
                                if (ub2.CurrentUserID is not null, 1, 0) as IsBlockTarget,
                                IF (fc.ID is not null, 1, 0) as IsReported,
                                IF (u.FacebookID != '', 1, '') as FacebookID,
                                IF (u.UserName is not null, u.UserName, '') as FromUserName, 
                                '' as OwnerName, 
                                '' as GiftReceiverName, 
                                IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,

                                '' AS CheckinPlaceID,
                                '' AS CheckinPlaceName,
                                0 AS CheckinStatus,
                                u.IsAdmin,
                                IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,

                                if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                               if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                                )AS NeighborhoodName
                                
                                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                                , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   
                                
                                , IF (u2.Photo != '', CONCAT('$this->photoUrl',u2.Photo), '') AS AboutAvatarURL
                                , IF (u2.UserName is not null, u2.UserName, '') as AboutName
                                , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName
                                , IF (topic.UserID is not null, topic.UserID, '') as ModID
                                , IF ((select UserID from topicban where UserID = temp.FromUserID AND TopicID = temp.TopicID) is not null, 1, 0) as IsTopicBan
                           FROM (
                                   
                                select ActivityID, FromUserID, Message, Image, RefID, Type, ActionType, PlaceID, PlaceName, PlaceIcon, PlaceAddress, Lat, Lon, CreatedDate, IsInvalid, LastUpdate, Gender, BirthDate, OwnerID, OrginalDate, GiftReceiverID, CheckinPlaceID, CheckinPlaceName, NeighborhoodID,
                                    haversineDistance($startLat, $startLon, activity.Lat, activity.Lon, $distanceType) AS Distance, IsAnonymous, FanPage, VideoID, VideoSource, VideoThumbnail, TopicID
                                FROM activity
                                WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                AND ( (Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2) OR (Lon BETWEEN $lon1_2 AND $lon2_2 AND Lat BETWEEN $lat1_2 AND $lat2_2))
                                $where

                                HAVING Distance < $dist ORDER BY CreatedDate DESC LIMIT $offset, $limit

                           ) as temp 

                           LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=$user_id

                           LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=$user_id
                           LEFT JOIN `user` u ON u.UserID=temp.FromUserID
                           LEFT JOIN `user` u2 ON u2.UserID = temp.FanPage
                           LEFT JOIN userblock ub1 ON ub1.TargetUserID=temp.FromUserID AND ub1.CurrentUserID=$user_id
                           LEFT JOIN userblock ub2 ON ub2.CurrentUserID=temp.FromUserID AND ub2.TargetUserID=$user_id
                           LEFT JOIN stopreceivenotify srn ON srn.UserID = $user_id AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType
                           LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID
                           left join topic ON topic.TopicID = temp.TopicID
                           where u.IsBanned = 0
                           ORDER BY temp.ActivityID desc
                    ";
                    $query = $this->db_slave->query($sql);
                    $activities = $query->result();
                    $query->free_result();
                }
            }
            $radiusOut = $dist;
        }else{ //****** radius was not provided so find it ******
            $aRadiusSet = explode(',', $radiusSet);
            foreach($aRadiusSet as $value){
                $dist = $value;
                $lon1 = $startLon - $dist / abs(cos(deg2rad($startLat)) * $distanceFactor) ;
                $lon2 = $startLon + $dist / abs(cos(deg2rad($startLat)) * $distanceFactor); 
                $lat1 = $startLat - ($dist / $distanceFactor);
                $lat2 = $startLat + ($dist / $distanceFactor);
                $isMeridian180 = 0;
                if ($lon1 < - 180){
                    $isMeridian180 = 1;
                    $lon1_2 = 360 + $lon1;
                    $lon2_2 = 179.999;
                    $lat1_2 = $lat1;
                    $lat2_2 = $lat2;
                    $lon1 = -179.999;
                }else if ($lon2 > 180){
                    $isMeridian180 = 1;
                    $lon1_2 = -179.999;
                    $lon2_2 = -360 + $lon2;
                    $lat1_2 = $lat1;
                    $lat2_2 = $lat2;
                    $lon2 = 179.999;
                }
               
                if ($isMeridian180 == 0){
                    $totalSql = "select count(*) as Total
                            from
                            (
                                    select haversineDistance($startLat,$startLon,activity.Lat,activity.Lon,$distanceType) AS Distance
                                    from activity
                                    WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                        AND CreatedDate >= '$datesearch'
                                        AND Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2
                                        $where
                                    HAVING Distance < $dist ORDER BY CreatedDate DESC LIMIT $offset, $cutOffVal
                            ) as t
                        ";
                    $totalQuery = $this->db_slave->query($totalSql);
                    $row = $totalQuery->row();
                    $totalQuery->free_result();
                    $queryCount = $row ? $row->Total : 0;
                }else{
                    $totalSql = "select count(*) as Total
                            from
                            (
                                    select haversineDistance($startLat,$startLon,activity.Lat,activity.Lon,$distanceType) AS Distance
                                    from activity
                                    WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                        AND CreatedDate >= '$datesearch'
                                        AND ( (Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2) OR (Lon BETWEEN $lon1_2 AND $lon2_2 AND Lat BETWEEN $lat1_2 AND $lat2_2))
                                        $where
                                    HAVING Distance < $dist ORDER BY CreatedDate DESC LIMIT $offset, $cutOffVal
                            ) as t
                        ";
                    $totalQuery = $this->db_slave->query($totalSql);
                    $row = $totalQuery->row();
                    $totalQuery->free_result();
                    $queryCount = $row ? $row->Total : 0;
                }
                    
                if ($queryCount >= $cutOffVal){
                    break;
                }
            }
            
            if ($isMeridian180 == 0){
                $totalSql = "select count(*) as Total
                            from
                            (
                                    select haversineDistance($startLat,$startLon,activity.Lat,activity.Lon,$distanceType) AS Distance
                                    from activity
                                    WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                        AND CreatedDate >= '$datesearch'
                                        AND Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2
                                        $where
                                    HAVING  Distance < $dist
                            ) as t
                        ";
                $totalQuery = $this->db_slave->query($totalSql);
                $row = $totalQuery->row();
                $totalQuery->free_result();
                $total = $row ? $row->Total : 0;
                
                
                if ($total > 0){
                    $sql = "SELECT temp.*,
                                IF (u.Photo != '', CONCAT('$photoUrl',u.Photo), '') AS Avatar,
                                '' AS OwnerAvatar,
                                IF (Image != '', CONCAT('$photoUrl',Image), '') AS Image,
                                '' AS PlaceIcon,
                                '' AS PlaceID,
                                '' AS PlaceName,
                                '' AS PlaceAddress,
                                IF (ak.ID is not null, 1, 0) as IsLike,
                                if (ub1.CurrentUserID is not null, 1, 0) as IsBlock,
                                if (ub2.CurrentUserID is not null, 1, 0) as IsBlockTarget,
                                IF (fc.ID is not null, 1, 0) as IsReported,
                                IF (u.FacebookID != '', 1, '') as FacebookID,
                                IF (u.UserName is not null, u.UserName, '') as FromUserName, 
                                '' as OwnerName, 
                                '' as GiftReceiverName, 
                                IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,

                                '' AS CheckinPlaceID,
                                '' AS CheckinPlaceName,
                                0 AS CheckinStatus,
                                u.IsAdmin,
                                IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,

                                if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                               if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                                )AS NeighborhoodName
                                
                                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                                , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   

                                , IF (u2.Photo != '', CONCAT('$this->photoUrl',u2.Photo), '') AS AboutAvatarURL
                                , IF (u2.UserName is not null, u2.UserName, '') as AboutName
                                , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName
                                , IF (topic.UserID is not null, topic.UserID, '') as ModID
                                , IF ((select UserID from topicban where UserID = temp.FromUserID AND TopicID = temp.TopicID) is not null, 1, 0) as IsTopicBan
                           FROM (
                                   
                                select ActivityID, FromUserID, Message, Image, RefID, Type, ActionType, PlaceID, PlaceName, PlaceIcon, PlaceAddress, Lat, Lon, CreatedDate, IsInvalid, LastUpdate, Gender, BirthDate, OwnerID, OrginalDate, GiftReceiverID, CheckinPlaceID, CheckinPlaceName, NeighborhoodID,
                                    haversineDistance($startLat, $startLon, activity.Lat, activity.Lon, $distanceType) AS Distance, IsAnonymous, FanPage, VideoID, VideoSource, VideoThumbnail, TopicID
                                FROM activity
                                WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                AND CreatedDate >= '$datesearch'
                                AND Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2
                                $where

                                HAVING Distance < $dist ORDER BY CreatedDate DESC LIMIT $offset, $limit

                           ) as temp 

                           LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=$user_id

                           LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=$user_id
                           LEFT JOIN `user` u ON u.UserID=temp.FromUserID
                           LEFT JOIN `user` u2 ON u2.UserID = temp.FanPage 
                           LEFT JOIN userblock ub1 ON ub1.TargetUserID=temp.FromUserID AND ub1.CurrentUserID=$user_id
                           LEFT JOIN userblock ub2 ON ub2.CurrentUserID=temp.FromUserID AND ub2.TargetUserID=$user_id
                           LEFT JOIN stopreceivenotify srn ON srn.UserID = $user_id AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType
                           LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID
                           left join topic ON topic.TopicID = temp.TopicID
                           where u.IsBanned = 0
                           ORDER BY temp.ActivityID desc
                    ";
                    $query = $this->db_slave->query($sql);
                    $activities = $query->result();
                    $query->free_result();
                }
            }else{
                 $totalSql = "select count(*) as Total
                            from
                            (
                                    select haversineDistance($startLat,$startLon,activity.Lat,activity.Lon,$distanceType) AS Distance
                                    from activity
                                    WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                        AND CreatedDate >= '$datesearch'
                                        AND ( (Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2) OR (Lon BETWEEN $lon1_2 AND $lon2_2 AND Lat BETWEEN $lat1_2 AND $lat2_2))
                                        $where
                                    HAVING  Distance < $dist
                            ) as t
                        ";
                $totalQuery = $this->db_slave->query($totalSql);
                $row = $totalQuery->row();
                $totalQuery->free_result();
                $total = $row ? $row->Total : 0;
                
                
                if ($total > 0){
                    $sql = "SELECT temp.*,
                                IF (u.Photo != '', CONCAT('$photoUrl',u.Photo), '') AS Avatar,
                                '' AS OwnerAvatar,
                                IF (Image != '', CONCAT('$photoUrl',Image), '') AS Image,
                                '' AS PlaceIcon,
                                '' AS PlaceID,
                                '' AS PlaceName,
                                '' AS PlaceAddress,
                                IF (ak.ID is not null, 1, 0) as IsLike,
                                if (ub1.CurrentUserID is not null, 1, 0) as IsBlock,
                                if (ub2.CurrentUserID is not null, 1, 0) as IsBlockTarget,
                                IF (fc.ID is not null, 1, 0) as IsReported,
                                IF (u.FacebookID != '', 1, '') as FacebookID,
                                IF (u.UserName is not null, u.UserName, '') as FromUserName, 
                                '' as OwnerName, 
                                '' as GiftReceiverName, 
                                IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,

                                '' AS CheckinPlaceID,
                                '' AS CheckinPlaceName,
                                0 AS CheckinStatus,
                                u.IsAdmin,
                                IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,

                                if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                               if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                                )AS NeighborhoodName
                                
                                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                                , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   
                                
                                , IF (u2.Photo != '', CONCAT('$this->photoUrl',u2.Photo), '') AS AboutAvatarURL
                                , IF (u2.UserName is not null, u2.UserName, '') as AboutName 
                                , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName
                                , IF (topic.UserID is not null, topic.UserID, '') as ModID
                                , IF ((select UserID from topicban where UserID = temp.FromUserID AND TopicID = temp.TopicID) is not null, 1, 0) as IsTopicBan
                           FROM (
                                   
                                select ActivityID, FromUserID, Message, Image, RefID, Type, ActionType, PlaceID, PlaceName, PlaceIcon, PlaceAddress, Lat, Lon, CreatedDate, IsInvalid, LastUpdate, Gender, BirthDate, OwnerID, OrginalDate, GiftReceiverID, CheckinPlaceID, CheckinPlaceName, NeighborhoodID,
                                    haversineDistance($startLat, $startLon, activity.Lat, activity.Lon, $distanceType) AS Distance, IsAnonymous, FanPage, VideoID, VideoSource, VideoThumbnail, TopicID
                                FROM activity
                                WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                AND CreatedDate >= '$datesearch'
                                AND ( (Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2) OR (Lon BETWEEN $lon1_2 AND $lon2_2 AND Lat BETWEEN $lat1_2 AND $lat2_2))
                                $where

                                HAVING Distance < $dist ORDER BY CreatedDate DESC LIMIT $offset, $limit

                           ) as temp 

                           LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=$user_id

                           LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=$user_id
                           LEFT JOIN `user` u ON u.UserID=temp.FromUserID
                           LEFT JOIN `user` u2 ON u2.UserID = temp.FanPage 
                           LEFT JOIN userblock ub1 ON ub1.TargetUserID=temp.FromUserID AND ub1.CurrentUserID=$user_id
                           LEFT JOIN userblock ub2 ON ub2.CurrentUserID=temp.FromUserID AND ub2.TargetUserID=$user_id
                           LEFT JOIN stopreceivenotify srn ON srn.UserID = $user_id AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType
                           LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID
                           left join topic ON topic.TopicID = temp.TopicID
                           where u.IsBanned = 0
                           ORDER BY temp.ActivityID desc
                    ";
                    $query = $this->db_slave->query($sql);
                    $activities = $query->result();
                    $query->free_result();
                }
            }
            $radiusOut = $dist;
        }

        return $activities;

    }
    
    function searchNearbyHashTag($user_id = 0, $hours = 12, $radius = -1, $cutOffVal = 50, &$radiusOut, &$total, $offset = '', $limit = '', $radiusSet, $gender = 0, $from_age = 0, $to_age = 0, $activity_id = 0, $hashtag, $lat = '', $lon = '', $topic_id = 0, $cate_id = 0) {
        $photoUrl = $this->config->item('s3_photo_url');
        $datesearch = date('Y-m-d H:i:s', strtotime( "-$hours hour", strtotime(gmdate('Y-m-d H:i:s'))));
        //ANOMO-5775
        $actionType = $this->activityType;

        if ($from_age <= 13){
            $from_age = 0;
        }
        if ($to_age > 90) {
            $to_age = 0;
        }
        
        $where = " AND FIND_IN_SET(ActionType, '$actionType') > 0 AND IsInvalid != 1";
        
        $sub_where = "";
        //filter by topic 
        if ($topic_id > 0){
            $sub_where .= " AND TopicID = $topic_id ";
        }
        // filter by category
        if ($cate_id > 0){
            $sub_where .= " AND CateID = $cate_id";
        }
        
        // filter by gender
        if ($gender > 0){
            $where .= " AND Gender = '".$gender."'";
        }
        
        // filter by age
        //Luong modified - AgeLess user
        $where .= " AND (( 1=1 ";
        if ($from_age > 0){
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-'.$from_age .' year', strtotime($now)));
            $where .= " AND BirthDate <= '".$youngest_date."'";
        }
        if ($to_age > 0){
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-'.$to_age - 1 .' year', strtotime($now)));
            $where .= " AND BirthDate > '".$oldest_date."'";
        }
        $where .= " ) or EXISTS(SELECT IsAgeLess from `user` where UserID = FromUserID and IsAgeLess = 1))";
        //End
        
        if ($activity_id > 0){
            $where .= " AND activity.ActivityID < '".$activity_id."'";
        }
        
        //ANOMO-10815 - Filter out anonymous posts have been blocked
        $where .= " AND (IsAnonymous = 0 OR (IsAnonymous = 1 AND NOT EXISTS(SELECT TargetUserID from `anonymousblock` where CurrentUserID = $user_id and TargetUserID = FromUserID))) ";
        
        //ANOMO-11539 - Apply filter and blocking to the Fanpage 
        $where .= " AND NOT EXISTS (SELECT TargetUserID from userblock where CurrentUserID = $user_id and TargetUserID = FanPage) ";

        $activities = array();
        
        // get user's location
        $startLat = floatval($lat);
        $startLon = floatval($lon);
        $distanceType = 1;
        $distanceFactor = 69;// miles
        if ($distanceType != 1){
            $distanceFactor = 111.04;//km
        }
        $radiusIn = $radius;
        if ($radiusIn > - 1){ // ****** radius was provided so use it ******
            $dist = $radiusIn;
            $lon1 = $startLon - $dist / abs(cos(deg2rad($startLat)) * $distanceFactor) ;
            $lon2 = $startLon + $dist / abs(cos(deg2rad($startLat)) * $distanceFactor); 
            $lat1 = $startLat - ($dist / $distanceFactor);
            $lat2 = $startLat + ($dist / $distanceFactor);
            $isMeridian180 = 0;
            if ($lon1 < - 180){
                $isMeridian180 = 1;
                $lon1_2 = 360 + $lon1;
                $lon2_2 = 179.999;
                $lat1_2 = $lat1;
                $lat2_2 = $lat2;
                $lon1 = -179.999;
            }else if ($lon2 > 180){
                $isMeridian180 = 1;
                $lon1_2 = -179.999;
                $lon2_2 = -360 + $lon2;
                $lat1_2 = $lat1;
                $lat2_2 = $lat2;
                $lon2 = 179.999;
            }
            
            if ($isMeridian180 == 0){
                
                $totalSql = "select count(*) as Total
                            from
                            (
                                    select haversineDistance($startLat,$startLon,activity.Lat,activity.Lon,$distanceType) AS Distance
                                    FROM (select ActivityID from hashtag where HashTag = '$hashtag' $sub_where)  as h 
				    LEFT JOIN activity  ON h.ActivityID = activity.ActivityID 
                                    WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                        AND Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2
                                        $where
                                    HAVING  Distance < $dist
                            ) as t
                        ";
                $totalQuery = $this->db_slave->query($totalSql);
                $row = $totalQuery->row();
                $totalQuery->free_result();
                $total = $row ? $row->Total : 0;
                
                
                if ($total > 0){
                    $sql = "SELECT temp.*,
                                IF (u.Photo != '', CONCAT('$photoUrl',u.Photo), '') AS Avatar,
                                '' AS OwnerAvatar,
                                IF (Image != '', CONCAT('$photoUrl',Image), '') AS Image,
                                '' AS PlaceIcon,
                                '' AS PlaceID,
                                '' AS PlaceName,
                                '' AS PlaceAddress,
                                IF (ak.ID is not null, 1, 0) as IsLike,
                                if (ub1.CurrentUserID is not null, 1, 0) as IsBlock,
                                if (ub2.CurrentUserID is not null, 1, 0) as IsBlockTarget,
                                IF (fc.ID is not null, 1, 0) as IsReported,
                                IF (u.FacebookID != '', 1, '') as FacebookID,
                                IF (u.UserName is not null, u.UserName, '') as FromUserName, 
                                '' as OwnerName, 
                                '' as GiftReceiverName, 
                                IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,

                                '' AS CheckinPlaceID,
                                '' AS CheckinPlaceName,
                                0 AS CheckinStatus,
                                u.IsAdmin,
                                IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,

                                if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                               if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                                )AS NeighborhoodName
                                
                                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                                , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   
                                

                                , IF (u2.Photo != '', CONCAT('$this->photoUrl',u2.Photo), '') AS AboutAvatarURL
                                , IF (u2.UserName is not null, u2.UserName, '') as AboutName 
                                , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName
                                , IF (topic.UserID is not null, topic.UserID, '') as ModID
                                , IF ((select UserID from topicban where UserID = temp.FromUserID AND TopicID = temp.TopicID) is not null, 1, 0) as IsTopicBan
                           FROM (
                                   
                                select activity.ActivityID, FromUserID, Message, Image, RefID, Type, ActionType, PlaceID, PlaceName, PlaceIcon, PlaceAddress, Lat, Lon, CreatedDate, IsInvalid, LastUpdate, Gender, BirthDate, OwnerID, OrginalDate, GiftReceiverID, CheckinPlaceID, CheckinPlaceName, NeighborhoodID,
                                    haversineDistance($startLat, $startLon, activity.Lat, activity.Lon, $distanceType) AS Distance, IsAnonymous, FanPage, VideoID, VideoSource, VideoThumbnail, TopicID
                                FROM (select ActivityID from hashtag where HashTag = '$hashtag' $sub_where)  as h 
				LEFT JOIN activity  ON h.ActivityID = activity.ActivityID
                                WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                AND Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2
                                $where

                                HAVING Distance < $dist ORDER BY CreatedDate DESC LIMIT $offset, $limit

                           ) as temp 

                           LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=$user_id

                           LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=$user_id
                           LEFT JOIN `user` u ON u.UserID=temp.FromUserID
                           LEFT JOIN `user` u2 ON u2.UserID = temp.FanPage
                           LEFT JOIN userblock ub1 ON ub1.TargetUserID=temp.FromUserID AND ub1.CurrentUserID=$user_id
                           LEFT JOIN userblock ub2 ON ub2.CurrentUserID=temp.FromUserID AND ub2.TargetUserID=$user_id
                           LEFT JOIN stopreceivenotify srn ON srn.UserID = $user_id AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType
                           LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID
                           left join topic ON topic.TopicID = temp.TopicID
                           where u.IsBanned = 0
                           ORDER BY temp.ActivityID desc
                    ";
                    $query = $this->db_slave->query($sql);
                    $activities = $query->result();
                    $query->free_result();
                }

                
            }else{ //isMeridian180 != 0
                $totalSql = "select count(*) as Total
                            from
                            (
                                    select haversineDistance($startLat,$startLon,activity.Lat,activity.Lon,$distanceType) AS Distance
                                    FROM (select ActivityID from hashtag where HashTag = '$hashtag' $sub_where)  as h 
                                    LEFT JOIN activity  ON h.ActivityID = activity.ActivityID
                                    WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                        AND ( (Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2) OR (Lon BETWEEN $lon1_2 AND $lon2_2 AND Lat BETWEEN $lat1_2 AND $lat2_2))
                                        $where
                                    HAVING  Distance < $dist
                            ) as t
                        ";
                $totalQuery = $this->db_slave->query($totalSql);
                $row = $totalQuery->row();
                $totalQuery->free_result();
                $total = $row ? $row->Total : 0;
                
                
                if ($total > 0){
                    $sql = "SELECT temp.*,
                                IF (u.Photo != '', CONCAT('$photoUrl',u.Photo), '') AS Avatar,
                                '' AS OwnerAvatar,
                                IF (Image != '', CONCAT('$photoUrl',Image), '') AS Image,
                                '' AS PlaceIcon,
                                '' AS PlaceID,
                                '' AS PlaceName,
                                '' AS PlaceAddress,
                                IF (ak.ID is not null, 1, 0) as IsLike,
                                if (ub1.CurrentUserID is not null, 1, 0) as IsBlock,
                                if (ub2.CurrentUserID is not null, 1, 0) as IsBlockTarget,
                                IF (fc.ID is not null, 1, 0) as IsReported,
                                IF (u.FacebookID != '', 1, '') as FacebookID,
                                IF (u.UserName is not null, u.UserName, '') as FromUserName, 
                                '' as OwnerName, 
                                '' as GiftReceiverName, 
                                IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,

                                '' AS CheckinPlaceID,
                                '' AS CheckinPlaceName,
                                0 AS CheckinStatus,
                                u.IsAdmin,
                                IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,

                                if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                               if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                                )AS NeighborhoodName
                                
                                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                                , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   
                                
                                , IF (u2.Photo != '', CONCAT('$this->photoUrl',u2.Photo), '') AS AboutAvatarURL
                                , IF (u2.UserName is not null, u2.UserName, '') as AboutName
                                , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName
                                , IF (topic.UserID is not null, topic.UserID, '') as ModID
                                , IF ((select UserID from topicban where UserID = temp.FromUserID AND TopicID = temp.TopicID) is not null, 1, 0) as IsTopicBan
                           FROM (
                                   
                                select activity.ActivityID, FromUserID, Message, Image, RefID, Type, ActionType, PlaceID, PlaceName, PlaceIcon, PlaceAddress, Lat, Lon, CreatedDate, IsInvalid, LastUpdate, Gender, BirthDate, OwnerID, OrginalDate, GiftReceiverID, CheckinPlaceID, CheckinPlaceName, NeighborhoodID,
                                    haversineDistance($startLat, $startLon, activity.Lat, activity.Lon, $distanceType) AS Distance, IsAnonymous, FanPage, VideoID, VideoSource, VideoThumbnail, TopicID
                                FROM (select ActivityID from hashtag where HashTag = '$hashtag' $sub_where)  as h 
				LEFT JOIN activity  ON h.ActivityID = activity.ActivityID
                                WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                AND ( (Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2) OR (Lon BETWEEN $lon1_2 AND $lon2_2 AND Lat BETWEEN $lat1_2 AND $lat2_2))
                                $where

                                HAVING Distance < $dist ORDER BY CreatedDate DESC LIMIT $offset, $limit

                           ) as temp 

                           LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=$user_id

                           LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=$user_id
                           LEFT JOIN `user` u ON u.UserID=temp.FromUserID
                           LEFT JOIN `user` u2 ON u2.UserID = temp.FanPage
                           LEFT JOIN userblock ub1 ON ub1.TargetUserID=temp.FromUserID AND ub1.CurrentUserID=$user_id
                           LEFT JOIN userblock ub2 ON ub2.CurrentUserID=temp.FromUserID AND ub2.TargetUserID=$user_id
                           LEFT JOIN stopreceivenotify srn ON srn.UserID = $user_id AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType
                           LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID
                           left join topic ON topic.TopicID = temp.TopicID
                           where u.IsBanned = 0
                           ORDER BY temp.ActivityID desc
                    ";
                    $query = $this->db_slave->query($sql);
                    $activities = $query->result();
                    $query->free_result();
                }
            }
            $radiusOut = $dist;
        }else{ //****** radius was not provided so find it ******
            $aRadiusSet = explode(',', $radiusSet);
            foreach($aRadiusSet as $value){
                $dist = $value;
                $lon1 = $startLon - $dist / abs(cos(deg2rad($startLat)) * $distanceFactor) ;
                $lon2 = $startLon + $dist / abs(cos(deg2rad($startLat)) * $distanceFactor); 
                $lat1 = $startLat - ($dist / $distanceFactor);
                $lat2 = $startLat + ($dist / $distanceFactor);
                $isMeridian180 = 0;
                if ($lon1 < - 180){
                    $isMeridian180 = 1;
                    $lon1_2 = 360 + $lon1;
                    $lon2_2 = 179.999;
                    $lat1_2 = $lat1;
                    $lat2_2 = $lat2;
                    $lon1 = -179.999;
                }else if ($lon2 > 180){
                    $isMeridian180 = 1;
                    $lon1_2 = -179.999;
                    $lon2_2 = -360 + $lon2;
                    $lat1_2 = $lat1;
                    $lat2_2 = $lat2;
                    $lon2 = 179.999;
                }
               
                if ($isMeridian180 == 0){
                    $totalSql = "select count(*) as Total
                            from
                            (
                                    select haversineDistance($startLat,$startLon,activity.Lat,activity.Lon,$distanceType) AS Distance
                                    FROM (select ActivityID from hashtag where HashTag = '$hashtag' $sub_where)  as h 
                                    LEFT JOIN activity  ON h.ActivityID = activity.ActivityID
                                    WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                        AND CreatedDate >= '$datesearch'
                                        AND Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2
                                        $where
                                    HAVING Distance < $dist ORDER BY CreatedDate DESC LIMIT $offset, $cutOffVal
                            ) as t
                        ";
                    $totalQuery = $this->db_slave->query($totalSql);
                    $row = $totalQuery->row();
                    $totalQuery->free_result();
                    $queryCount = $row ? $row->Total : 0;
                }else{
                    $totalSql = "select count(*) as Total
                            from
                            (
                                    select haversineDistance($startLat,$startLon,activity.Lat,activity.Lon,$distanceType) AS Distance
                                    FROM (select ActivityID from hashtag where HashTag = '$hashtag' $sub_where)  as h 
                                    LEFT JOIN activity  ON h.ActivityID = activity.ActivityID
                                    WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                        AND CreatedDate >= '$datesearch'
                                        AND ( (Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2) OR (Lon BETWEEN $lon1_2 AND $lon2_2 AND Lat BETWEEN $lat1_2 AND $lat2_2))
                                        $where
                                    HAVING Distance < $dist ORDER BY CreatedDate DESC LIMIT $offset, $cutOffVal
                            ) as t
                        ";
                    $totalQuery = $this->db_slave->query($totalSql);
                    $row = $totalQuery->row();
                    $totalQuery->free_result();
                    $queryCount = $row ? $row->Total : 0;
                }
                    
                if ($queryCount >= $cutOffVal){
                    break;
                }
            }
            
            if ($isMeridian180 == 0){
                $totalSql = "select count(*) as Total
                            from
                            (
                                    select haversineDistance($startLat,$startLon,activity.Lat,activity.Lon,$distanceType) AS Distance
                                    FROM (select ActivityID from hashtag where HashTag = '$hashtag' $sub_where)  as h 
                                    LEFT JOIN activity  ON h.ActivityID = activity.ActivityID
                                    WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                        AND CreatedDate >= '$datesearch'
                                        AND Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2
                                        $where
                                    HAVING  Distance < $dist
                            ) as t
                        ";
                $totalQuery = $this->db_slave->query($totalSql);
                $row = $totalQuery->row();
                $totalQuery->free_result();
                $total = $row ? $row->Total : 0;
                
                
                if ($total > 0){
                    $sql = "SELECT temp.*,
                                IF (u.Photo != '', CONCAT('$photoUrl',u.Photo), '') AS Avatar,
                                '' AS OwnerAvatar,
                                IF (Image != '', CONCAT('$photoUrl',Image), '') AS Image,
                                '' AS PlaceIcon,
                                '' AS PlaceID,
                                '' AS PlaceName,
                                '' AS PlaceAddress,
                                IF (ak.ID is not null, 1, 0) as IsLike,
                                if (ub1.CurrentUserID is not null, 1, 0) as IsBlock,
                                if (ub2.CurrentUserID is not null, 1, 0) as IsBlockTarget,
                                IF (fc.ID is not null, 1, 0) as IsReported,
                                IF (u.FacebookID != '', 1, '') as FacebookID,
                                IF (u.UserName is not null, u.UserName, '') as FromUserName, 
                                '' as OwnerName, 
                                '' as GiftReceiverName, 
                                IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,

                                '' AS CheckinPlaceID,
                                '' AS CheckinPlaceName,
                                0 AS CheckinStatus,
                                u.IsAdmin,
                                IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,

                                if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                               if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                                )AS NeighborhoodName
                                
                                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                                , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   

                                , IF (u2.Photo != '', CONCAT('$this->photoUrl',u2.Photo), '') AS AboutAvatarURL
                                , IF (u2.UserName is not null, u2.UserName, '') as AboutName
                                , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName
                                , IF (topic.UserID is not null, topic.UserID, '') as ModID
                                , IF ((select UserID from topicban where UserID = temp.FromUserID AND TopicID = temp.TopicID) is not null, 1, 0) as IsTopicBan
                           FROM (
                                   
                                select activity.ActivityID, FromUserID, Message, Image, RefID, Type, ActionType, PlaceID, PlaceName, PlaceIcon, PlaceAddress, Lat, Lon, CreatedDate, IsInvalid, LastUpdate, Gender, BirthDate, OwnerID, OrginalDate, GiftReceiverID, CheckinPlaceID, CheckinPlaceName, NeighborhoodID,
                                    haversineDistance($startLat, $startLon, activity.Lat, activity.Lon, $distanceType) AS Distance, IsAnonymous, FanPage, VideoID, VideoSource, VideoThumbnail, TopicID
                                FROM (select ActivityID from hashtag where HashTag = '$hashtag' $sub_where)  as h 
				LEFT JOIN activity  ON h.ActivityID = activity.ActivityID
                                WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                AND CreatedDate >= '$datesearch'
                                AND Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2
                                $where

                                HAVING Distance < $dist ORDER BY CreatedDate DESC LIMIT $offset, $limit

                           ) as temp 

                           LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=$user_id

                           LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=$user_id
                           LEFT JOIN `user` u ON u.UserID=temp.FromUserID
                           LEFT JOIN `user` u2 ON u2.UserID = temp.FanPage 

                           LEFT JOIN userblock ub1 ON ub1.TargetUserID=temp.FromUserID AND ub1.CurrentUserID=$user_id
                           LEFT JOIN userblock ub2 ON ub2.CurrentUserID=temp.FromUserID AND ub2.TargetUserID=$user_id
                           LEFT JOIN stopreceivenotify srn ON srn.UserID = $user_id AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType
                           LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID
                           left join topic ON topic.TopicID = temp.TopicID
                           where u.IsBanned = 0
                           ORDER BY temp.ActivityID desc
                    ";
                    $query = $this->db_slave->query($sql);
                    $activities = $query->result();
                    $query->free_result();
                }
            }else{
                 $totalSql = "select count(*) as Total
                            from
                            (
                                    select haversineDistance($startLat,$startLon,activity.Lat,activity.Lon,$distanceType) AS Distance
                                    FROM (select ActivityID from hashtag where HashTag = '$hashtag' $sub_where)  as h 
                                    LEFT JOIN activity  ON h.ActivityID = activity.ActivityID
                                    WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                        AND CreatedDate >= '$datesearch'
                                        AND ( (Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2) OR (Lon BETWEEN $lon1_2 AND $lon2_2 AND Lat BETWEEN $lat1_2 AND $lat2_2))
                                        $where
                                    HAVING  Distance < $dist
                            ) as t
                        ";

                $totalQuery = $this->db_slave->query($totalSql);
                $row = $totalQuery->row();
                $totalQuery->free_result();
                $total = $row ? $row->Total : 0;
                
                
                if ($total > 0){
                    $sql = "SELECT temp.*,
                                IF (u.Photo != '', CONCAT('$photoUrl',u.Photo), '') AS Avatar,
                                '' AS OwnerAvatar,
                                IF (Image != '', CONCAT('$photoUrl',Image), '') AS Image,
                                '' AS PlaceIcon,
                                '' AS PlaceID,
                                '' AS PlaceName,
                                '' AS PlaceAddress,
                                IF (ak.ID is not null, 1, 0) as IsLike,
                                if (ub1.CurrentUserID is not null, 1, 0) as IsBlock,
                                if (ub2.CurrentUserID is not null, 1, 0) as IsBlockTarget,
                                IF (fc.ID is not null, 1, 0) as IsReported,
                                IF (u.FacebookID != '', 1, '') as FacebookID,
                                IF (u.UserName is not null, u.UserName, '') as FromUserName, 
                                '' as OwnerName, 
                                '' as GiftReceiverName, 
                                IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,

                                '' AS CheckinPlaceID,
                                '' AS CheckinPlaceName,
                                0 AS CheckinStatus,
                                u.IsAdmin,
                                IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,

                                if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                                               if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
                                )AS NeighborhoodName
                                
                                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                                , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   
                                
                                , IF (u2.Photo != '', CONCAT('$this->photoUrl',u2.Photo), '') AS AboutAvatarURL
                                , IF (u2.UserName is not null, u2.UserName, '') as AboutName 
                                , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName
                                , IF (topic.UserID is not null, topic.UserID, '') as ModID
                                , IF ((select UserID from topicban where UserID = temp.FromUserID AND TopicID = temp.TopicID) is not null, 1, 0) as IsTopicBan
                           FROM (
                                   
                                select activity.ActivityID, FromUserID, Message, Image, RefID, Type, ActionType, PlaceID, PlaceName, PlaceIcon, PlaceAddress, Lat, Lon, CreatedDate, IsInvalid, LastUpdate, Gender, BirthDate, OwnerID, OrginalDate, GiftReceiverID, CheckinPlaceID, CheckinPlaceName, NeighborhoodID,
                                    haversineDistance($startLat, $startLon, activity.Lat, activity.Lon, $distanceType) AS Distance, IsAnonymous, FanPage, VideoID, VideoSource, VideoThumbnail, TopicID
                                FROM (select ActivityID from hashtag where HashTag = '$hashtag' $sub_where)  as h 
				LEFT JOIN activity  ON h.ActivityID = activity.ActivityID
                                WHERE Lat IS NOT NULL AND Lon IS NOT NULL 
                                AND CreatedDate >= '$datesearch'
                                AND ( (Lon BETWEEN $lon1 AND $lon2 AND Lat BETWEEN $lat1 AND $lat2) OR (Lon BETWEEN $lon1_2 AND $lon2_2 AND Lat BETWEEN $lat1_2 AND $lat2_2))
                                $where

                                HAVING Distance < $dist ORDER BY CreatedDate DESC LIMIT $offset, $limit

                           ) as temp 

                           LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=$user_id

                           LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=$user_id
                           LEFT JOIN `user` u ON u.UserID=temp.FromUserID
                           LEFT JOIN `user` u2 ON u2.UserID = temp.FanPage 
                           LEFT JOIN userblock ub1 ON ub1.TargetUserID=temp.FromUserID AND ub1.CurrentUserID=$user_id
                           LEFT JOIN userblock ub2 ON ub2.CurrentUserID=temp.FromUserID AND ub2.TargetUserID=$user_id
                           LEFT JOIN stopreceivenotify srn ON srn.UserID = $user_id AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType
                           LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID
                           left join topic ON topic.TopicID = temp.TopicID
                           where u.IsBanned = 0
                           ORDER BY temp.ActivityID desc
                    ";

                    $query = $this->db_slave->query($sql);
                    $activities = $query->result();
                    $query->free_result();
                }
            }
            $radiusOut = $dist;
        }

        return $activities;
    }
    
    function getRecentActivitiesByTopicID($user_id = 0, &$total, $offset = '', $limit = '', $gender = 0, $from_age = 0, $to_age = 0, $is_favourite_search = 0, $activity_id = 0, $topic_id = 0) {
        $actionType = $this->activityType;
        
        if ($from_age <= 13){
            $from_age = 0;
        }
        if ($to_age > 90) {
            $to_age = 0;
        }
        
        $where = array();

        $where []= "FIND_IN_SET(ActionType, '$actionType') > 0 AND IsInvalid != 1";

        if ($topic_id > 0){
            $where []= " TopicID = $topic_id";
        }
        // filter by gender
        if ($gender > 0){
            $where []= "Gender = '".$gender."'";
        }
        
        
        // get by ActivityID - fix duplicate issue 
        if ($activity_id > 0){
            $where []= " ActivityID < '".$activity_id."'";
        }
        
       
        
        //$condition = $where;
        //array_unshift($condition, $activity_condition);
        $sWhere = implode(' and ', $where);
        
        // filter by age
        //Luong modified - AgeLess user
        $sWhere .= " AND (( 1=1 ";
        if ($from_age > 0){
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-'.$from_age .' year', strtotime($now)));
            $sWhere .= " AND BirthDate <= '".$youngest_date."' ";
        }
        if ($to_age > 0){
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-'.$to_age - 1 .' year', strtotime($now)));
            $sWhere .= " AND BirthDate > '".$oldest_date."' ";
        }
        $sWhere .= " ) OR EXISTS(SELECT IsAgeLess from `user` where UserID = a.FromUserID and IsAgeLess = 1))";

        
        
        //ANOMO-10815 - Filter out anonymous posts have been blocked
        $sWhere .= " AND (a.IsAnonymous = 0 OR (a.IsAnonymous = 1 AND NOT EXISTS(SELECT TargetUserID from `anonymousblock` where CurrentUserID = $user_id and TargetUserID = a.FromUserID))) ";
        
        //ANOMO-11539 - Apply filter and blocking to the Fanpage
        $sWhere .= " AND NOT EXISTS (SELECT TargetUserID from userblock where CurrentUserID = $user_id and TargetUserID = a.FanPage) ";
           
        // get total result
        // if first call then calculate total record
        if ($total <= 0){
            $totalSql = "select count(ActivityID) as Total from activity a where $sWhere";    
            $totalQuery = $this->db_slave->query($totalSql);
            $row = $totalQuery->row();
            $totalQuery->free_result();
            $total = $row?$row->Total:0;
        }

        $sql = "
        SELECT temp.*,
            IF (u.Photo != '', CONCAT('$this->photoUrl',u.Photo), '') AS Avatar,
            '' AS OwnerAvatar,
            IF (Image != '', CONCAT('$this->photoUrl',Image), '') AS Image,
            '' AS PlaceIcon,
            '' AS PlaceID,
            '' AS PlaceName,
            '' AS PlaceAddress,
            0 as IsBlock,
            0 as IsBlockTarget,
            0 as IsReported,
            0 as IsFavorite,
            IF (u.FacebookID != '', 1, '') as FacebookID,
            IF (u.UserName is not null, u.UserName, '') as FromUserName, 
            '' as OwnerName, 
            '' as GiftReceiverName, 
            0 as IsStopReceiveNotify,
            '' AS CheckinPlaceID,
            '' AS CheckinPlaceName,
            0 AS CheckinStatus,
            u.IsAdmin,
            IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,
            
            if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                           if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
           )AS NeighborhoodName
           
           , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
           , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
           , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   
           
           , IF (u2.Photo != '', CONCAT('$this->photoUrl',u2.Photo), '') AS AboutAvatarURL
           , IF (u2.UserName is not null, u2.UserName, '') as AboutName  
           , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName
           , IF (topic.UserID is not null, topic.UserID, '') as ModID
           , IF ((select UserID from topicban where UserID = temp.FromUserID AND TopicID = temp.TopicID) is not null, 1, 0) as IsTopicBan
           FROM (
               select a.ActivityID, a.FromUserID, a.Message, a.Image, a.RefID, a.Type, a.ActionType, a.PlaceID, a.PlaceName, a.PlaceIcon, a.PlaceAddress, a.Lat, a.Lon, a.CreatedDate, a.IsInvalid, a.LastUpdate, a.Gender, a.BirthDate, a.OwnerID, a.OrginalDate, a.GiftReceiverID, a.CheckinPlaceID, a.CheckinPlaceName, a.NeighborhoodID, a.IsAnonymous, a.FanPage
                      , a.VideoID, a.VideoSource, a.VideoThumbnail, a.TopicID
               FROM activity a
               WHERE $sWhere
               ORDER BY CreatedDate DESC LIMIT $offset, $limit
           ) as temp

           LEFT JOIN `user` u ON u.UserID=temp.FromUserID
           LEFT JOIN `user` u2 ON u2.UserID =temp.FanPage
           LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID
           left join topic ON topic.TopicID = temp.TopicID
           where u.IsBanned = 0
           ORDER BY temp.ActivityID desc
        ";

        $query = $this->db_slave->query($sql);
        $activities = $query->result();
        $query->free_result();
        
        return $activities;
    }
    
    function getTotalTopicByCond($user_id = 0, $gender = 0, $from_age = 0, $to_age = 0, $topic_id = 0) {
        $actionType = $this->activityType;

        if ($from_age <= 13) {
            $from_age = 0;
        }
        if ($to_age > 90) {
            $to_age = 0;
        }

        $where = array();

        $where [] = "FIND_IN_SET(ActionType, '$actionType') > 0 AND IsInvalid != 1";

        if ($topic_id > 0) {
            $where [] = " TopicID = $topic_id";
        }
        // filter by gender
        if ($gender > 0) {
            $where [] = "Gender = '" . $gender . "'";
        }

        //$condition = $where;
        //array_unshift($condition, $activity_condition);
        $sWhere = implode(' and ', $where);

        // filter by age
        //Luong modified - AgeLess user
        $sWhere .= " AND (( 1=1 ";
        if ($from_age > 0) {
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-' . $from_age . ' year', strtotime($now)));
            $sWhere .= " AND BirthDate <= '" . $youngest_date . "' ";
        }
        if ($to_age > 0) {
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-' . $to_age - 1 . ' year', strtotime($now)));
            $sWhere .= " AND BirthDate > '" . $oldest_date . "' ";
        }
        $sWhere .= " ) OR EXISTS(SELECT IsAgeLess from `user` where UserID = a.FromUserID and IsAgeLess = 1))";



        //ANOMO-10815 - Filter out anonymous posts have been blocked
        $sWhere .= " AND (a.IsAnonymous = 0 OR (a.IsAnonymous = 1 AND NOT EXISTS(SELECT TargetUserID from `anonymousblock` where CurrentUserID = $user_id and TargetUserID = a.FromUserID))) ";

        //ANOMO-11539 - Apply filter and blocking to the Fanpage
        $sWhere .= " AND NOT EXISTS (SELECT TargetUserID from userblock where CurrentUserID = $user_id and TargetUserID = a.FanPage) ";

        // get total result

        $totalSql = "select count(ActivityID) as Total from activity a where $sWhere";
        $totalQuery = $this->db_slave->query($totalSql);
        $row = $totalQuery->row();
        $totalQuery->free_result();
        $total = $row ? $row->Total : 0;
        return $total;
    }
    
    
    function getTopicInfoOfContent($content_id, $type) {
        $sql = "SELECT a.*, t.UserID as ModID from activity a
                left join topic t on a.TopicID = t.TopicID
                where RefID = $content_id AND Type = $type  AND ActionType = $type";
        $query = $this->db_slave->query($sql);
        $row = $query->row();
        if (!$row) {
            $sql = "SELECT a.*, t.UserID as ModID from activity_archive a
                left join topic t on a.TopicID = t.TopicID
                where RefID = $content_id AND Type = $type  AND ActionType = $type";
            $query = $this->db_slave->query($sql);
            $row = $query->row();
        }
        $query->free_result();

        return $row;
    }
    
    function getTopicInfoByActivityID($activity_id) {
        $sql = "SELECT a.*, t.UserID as ModID from activity a
                left join topic t on a.TopicID = t.TopicID
                where ActivityID = $activity_id";
        $query = $this->db_slave->query($sql);
        $row = $query->row();
        if (!$row) {
            $sql = "SELECT a.*, t.UserID as ModID from activity_archive a
                left join topic t on a.TopicID = t.TopicID
                where ActivityID = $activity_id";
            $query = $this->db_slave->query($sql);
            $row = $query->row();
        }
        $query->free_result();

        return $row;
    }
    
    function getTotalPopular($user_id = 0, $gender = 0, $from_age = 0, $to_age = 0, $hashtag = '', $topic_id = 0) {
        $actionType = $this->activityType;

        if ($from_age <= 13) {
            $from_age = 0;
        }
        if ($to_age > 90) {
            $to_age = 0;
        }

        $where = " FIND_IN_SET(ActionType, '$actionType') > 0 AND IsInvalid != 1";

        // filter by topic 
        if ($topic_id > 0) {
            $where .= " AND TopicID = $topic_id ";
        }

        // Hung added:
        $where .= " AND not exists (select Id from popularfeedban WHERE BannedUserID = FromUserID)";

        // filter by gender
        if ($gender > 0) {
            $where .= " AND Gender = '" . $gender . "'";
        }

        // filter by age
        //Luong modified - AgeLess user
        $where .= " AND (( 1=1 ";
        if ($from_age > 0) {
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-' . $from_age . ' year', strtotime($now)));
            $where .= " AND BirthDate <= '" . $youngest_date . "' ";
        }
        if ($to_age > 0) {
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-' . $to_age - 1 . ' year', strtotime($now)));
            $where .= " AND BirthDate > '" . $oldest_date . "' ";
        }
        $where .= " ) or EXISTS(SELECT IsAgeLess from `user` where UserID = a.FromUserID and IsAgeLess = 1))";
        //End
        //ANOMO-10815 - Filter out anonymous posts have been blocked
        $where .= " AND (a.IsAnonymous = 0 OR (a.IsAnonymous = 1 AND NOT EXISTS(SELECT TargetUserID from `anonymousblock` where CurrentUserID = $user_id and TargetUserID = a.FromUserID))) ";

        //ANOMO-11539 - Apply filter and blocking to the Fanpage
        $where .= " AND NOT EXISTS (SELECT TargetUserID from userblock where CurrentUserID = $user_id and TargetUserID = a.FanPage) ";

        $left_join = "";
        if ($hashtag != '') {
            $left_join = " left join (select ActivityID from hashtag where HashTag = '" . $hashtag . "') as h ON h.ActivityID = a.ActivityID ";
            $where .= " AND h.ActivityID is not null";
        }

        $totalSql = "select count(a.ActivityID) as Total
                    from
                    (
                            select ContentID, Type, concat(ContentID,'-',Type) as TempID
                            ,count(*) as TotalLike
                            from activitylike
                            where CreatedDate > DATE_SUB(Now(), INTERVAL 16 HOUR)
                            group by TempID ORDER BY TotalLike DESC Limit 1000
                    ) as temp
                    left join activity a on a.RefID=temp.ContentID AND a.ActionType=temp.Type
                    $left_join
                    WHERE $where
                ";

        $totalQuery = $this->db_slave->query($totalSql);
        $row = $totalQuery->row();
        $totalQuery->free_result();
        $total = $row ? $row->Total : 0;
        return $total;
    }
    
    function getFavoriteActivities($user_id = 0, &$total, $offset = '', $limit = '', $gender = 0, $from_age = 0, $to_age = 0, $activity_id = 0, $hashtag = '') {
        $actionType = $this->activityType;
        
        if ($from_age <= 13){
            $from_age = 0;
        }

        if ($to_age > 90) {
            $to_age = 0;
        }

        $where = " FIND_IN_SET(ActionType, '$actionType') > 0 AND IsInvalid != 1";

        // filter by gender
        if ($gender > 0){
            $where .= " AND Gender = '".$gender."'";
        }

        // filter by age
        //Luong modified - AgeLess user
        $where .= " AND (( 1=1 ";
        if ($from_age > 0){
            $now = gmdate('Y-m-d');
            $youngest_date = date('Y-m-d', strtotime('-'.$from_age .' year', strtotime($now)));
            $where .= " AND BirthDate <= '".$youngest_date."'";
        }
        if ($to_age > 0){
            $now = gmdate('Y-m-d');
            $oldest_date = date('Y-m-d', strtotime('-'.$to_age - 1 .' year', strtotime($now)));
            $where .= " AND BirthDate > '".$oldest_date."'";
        }
        $where .= " ) or EXISTS(SELECT IsAgeLess from `user` where UserID = FromUserID and IsAgeLess = 1))";
        //End
        
        //ANOMO-11539 - Apply filter and blocking to the Fanpage
        $where .= " AND NOT EXISTS(SELECT TargetUserID from userblock where CurrentUserID = $user_id and TargetUserID = FanPage) ";
        
        // get by ActivityID - fix duplicate issue 
        if ($activity_id > 0){
            $where .= " AND ActivityID < '".$activity_id."'";
        }

                
        $left_join1 = "";
        if ($hashtag != ''){
            $left_join1 = " left join (select ActivityID from hashtag where HashTag = '".$hashtag."') as h ON h.ActivityID = a.ActivityID ";
            $where .= " AND h.ActivityID is not null";
        }
        
        // get total result
        // if first call then calculate total record
        if ($total <= 0){
            $totalSql = "SELECT COUNT(a.ActivityID) as Total
                    FROM (
                            SELECT TopicID FROM topicfavorite WHERE UserID = $user_id
                            union (select TopicID from topic where UserID = $user_id and IsDelete = 0) 
                     ) fav1
                    left JOIN activity a ON a.TopicID = fav1.TopicID
                    $left_join1
                    WHERE $where ";
            $totalQuery = $this->db_slave->query($totalSql);
            $row = $totalQuery->row();
            $totalQuery->free_result();
            $total = $row?$row->Total:0;
        }
        $activities = array();
        if ($total > 0){
            $sql = "
                SELECT temp.*,
                IF (u.Photo != '', CONCAT('$this->photoUrl',u.Photo), '') AS Avatar,
                '' AS OwnerAvatar,
                IF (Image != '', CONCAT('$this->photoUrl',Image), '') AS Image,
                '' AS PlaceIcon,
                '' AS PlaceID,
                '' AS PlaceName,
                '' AS PlaceAddress,
                IF (ak.ID is not null, 1, 0) as IsLike,
                if (ub1.CurrentUserID is not null, 1, 0) as IsBlock,
                if (ub2.CurrentUserID is not null, 1, 0) as IsBlockTarget,
                IF (fc.ID is not null, 1, 0) as IsReported,
                IF (u.FacebookID != '', 1, '') as FacebookID,
                IF (u.UserName is not null, u.UserName, '') as FromUserName, 
                '' as OwnerName, 
                '' as GiftReceiverName, 
                IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,
                if (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,
                '' AS CheckinPlaceID,
                '' AS CheckinPlaceName,
                0 AS CheckinStatus,
                u.IsAdmin,
                IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,
                if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
                               if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
               )AS NeighborhoodName
               
                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoUrl', VideoID), IF(VideoSource = 'vine', concat('$this->vineVideoUrl', VideoID), IF(VideoSource = 'upload', concat('$this->videoUrl', VideoID), '')) ) as VideoURL
                , IF(VideoSource = 'youtube', concat('$this->youtubeVideoThumbnailUrl', VideoID, '$this->youtubeImageDefault'), IF(VideoSource = 'upload', concat('$this->photoUrl', VideoThumbnail), VideoThumbnail) ) as VideoThumbnail
                , if (VideoID is not null && VideoSource != 'upload', VideoID, '') as VideoID   
               
               , IF (u2.Photo != '', CONCAT('$this->photoUrl',u2.Photo), '') AS AboutAvatarURL
                , IF (u2.UserName is not null, u2.UserName, '') as AboutName
                , IF (topic.TopicName is not null, topic.TopicName, '') as TopicName 
                , IF (topic.UserID is not null, topic.UserID, '') as ModID
                , IF ((select UserID from topicban where UserID = temp.FromUserID AND TopicID = temp.TopicID) is not null, 1, 0) as IsTopicBan
               FROM (
                    SELECT a.ActivityID, a.FromUserID, a.Message, a.Image, a.RefID, a.Type, a.ActionType, a.PlaceID, a.PlaceName, a.PlaceIcon, a.PlaceAddress, a.Lat, a.Lon, a.CreatedDate, a.IsInvalid, a.LastUpdate, a.Gender, a.BirthDate, a.OwnerID, a.OrginalDate, a.GiftReceiverID, a.CheckinPlaceID, a.CheckinPlaceName, a.NeighborhoodID, a.IsAnonymous, a.FanPage
                        , a.VideoID, a.VideoSource, a.VideoThumbnail, a.TopicID
                    FROM (
                            SELECT TopicID FROM topicfavorite WHERE UserID = $user_id
                            union (select TopicID from topic where UserID = $user_id and IsDelete = 0) 
                    ) fav1
                    LEFT JOIN activity a ON a.TopicID = fav1.TopicID
                    $left_join1
                    WHERE $where
                    ORDER BY CreatedDate DESC LIMIT $offset, $limit
               ) as temp

                LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=$user_id
                left JOIN userblock ub1 ON ub1.TargetUserID=temp.FromUserID AND ub1.CurrentUserID=$user_id
                left JOIN userblock ub2 ON ub2.CurrentUserID=temp.FromUserID AND ub2.TargetUserID=$user_id
                LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=$user_id
                LEFT JOIN `user` u ON u.UserID=temp.FromUserID
                LEFT JOIN `user` u2 ON u2.UserID = temp.FanPage 
                LEFT JOIN userconnect cc ON cc.TargetUserID=temp.FromUserID AND cc.CurrentUserID = $user_id
                LEFT JOIN stopreceivenotify srn ON srn.UserID = $user_id AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType
                LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID
                left join topic ON topic.TopicID = temp.TopicID
                where u.IsBanned = 0
                ORDER BY temp.ActivityID desc
            ";
            
            $query = $this->db_slave->query($sql);
            $activities = $query->result();
            $query->free_result();
        }
        return $activities;
    }
    
}

?>