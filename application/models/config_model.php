<?php

class Config_model extends CI_Model {
    public $cached = false;

    function __construct()
    {
        parent::__construct();
        $CI =& get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
    }
    
    function add($data) {
        foreach ($data as $value) {
            if (self::getConfig($value['Key'])) {
                $this->db->update('config', array('Value' => $value['Value']), array('Key' => $value['Key']));
            } else {
                $this->db->delete('config', array('Key' => $value['Key']));
                $this->db->insert('config', $value);
            }
        }
        if ($this->cached) {
            $this->cached->clean();
        }
    }

    function update($data, $cond) {
        $this->db->update('config', $data, $cond);
        if ($this->cached) {
            $this->cached->clean();
        }
    }

    function getAllConfig() {
        if ($this->cached) {            
            $cache = $this->cached->get('allconfig');  
            if ($cache) {               
                return $cache;
            } else {
                $this->db->select('*');
                $this->db->from('config');
                $query = $this->db->get();
                $results = $query->result();
                $data = array();
                if ($results) {
                    foreach ($results as $re) {
                        $data[$re->Key] = $re->Value;
                    }
                }
                $query->free_result();
                $this->cached->set('allconfig', $data);
                return $data;
            }
        }else{
            $this->db->select('*');
            $this->db->from('config');
            $query = $this->db->get();
            $results = $query->result();
            $data = array();
            if ($results) {
                foreach ($results as $re) {
                    $data[$re->Key] = $re->Value;
                }
            }
            return $data;
        }
    }

    function getConfig($key) {
        $config = self::getAllConfig();
        if (isset($config[$key])) {
            return $config[$key];
        } else {
            return false;
        }
    }

    function getNearbySearchConfig() {
        if ($this->cached) {            
            $cache = $this->cached->get('nearbysearchconfig');  
            if ($cache) {               
                return $cache;
            } else {
                $this->db->select('*');
                $this->db->from('config');
                $this->db->where('`Key` LIKE "SEARCH_RADIUS_SET" OR `Key` LIKE "SEARCH_CUTOFF_VAL" OR `Key` LIKE "SEARCH_TIME_LIMIT"');
                $query = $this->db->get();
                $results = $query->result();
                $data = array();
                if ($results) {
                    foreach ($results as $re) {
                        $data[$re->Key] = $re->Value;
                    }
                }
                $query->free_result();
                $this->cached->set('nearbysearchconfig', $data);
                return $data;
            }
        }else{
            $this->db->select('*');
            $this->db->from('config');
            $this->db->where('`Key` LIKE "SEARCH_RADIUS_SET" OR `Key` LIKE "SEARCH_CUTOFF_VAL" OR `Key` LIKE "SEARCH_TIME_LIMIT"');
            $query = $this->db->get();
            $results = $query->result();
            $data = array();
            if ($results) {
                foreach ($results as $re) {
                    $data[$re->Key] = $re->Value;
                }
            }
            return $data;
        }
    }

}

?>