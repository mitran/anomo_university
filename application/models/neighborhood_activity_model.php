<?php

class Neighborhood_Activity_model extends CI_Model {

    function add($data) {
        $this->db->insert('neighborhoodactivity', $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        $this->db->update('neighborhoodactivity', $data, $cond);
    }

    function checkPlaceActivity($place_id) {
        $this->db->select('ID');
        $this->db->from('neighborhoodactivity');
        $this->db->where('NeighborhoodID', $place_id);
        $this->db->where('Type', 0);
        $query = $this->db->get();
        return $query->row();
    }

}

?>