<?php

class Sms_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $CI =& get_instance();
        $CI->load->library('Cache');
        $this->cached = $CI->cache->getCache();
    }
    
    function add($data){
        $this->db->insert('sms', $data);
        return $this->db->insert_id();
    }
    
    function update($ids, $result = 'success'){
        if ($result == 'success'){
            $sql = "update sms_message set IsSent = 1 where ID IN (".implode(',', $ids).")";
        }else{
            $sql = "update sms_message set `FailCount` = `FailCount` + 1 where ID IN (".implode(',', $ids).")";
        }
        return $this->db->query($sql);
    }
    
    function addMulti($numbers, $user_id, $message){
        $aNumber = explode(';', $numbers);
        if (sizeof($aNumber) > 0){
            $created_date = gmdate('Y-m-d H:i:s');
            $value = array();
            foreach($aNumber as $number){
                if (!empty($number)){
                    $value[] = "('$number', '$user_id', 0, '$created_date')";
                }
            }
            if (sizeof($value) > 0){
                $sql = "insert into sms_message (`PhoneNumber`, `UserID`, `IsSent`, `CreatedDate`)
                    value ". implode(',', $value);
                return $this->db->query($sql);
            }
        }
        return false;
    }
    
    function getList($limit = 50){
        $this->db->select('*');
        $this->db->from('sms_message');
        $this->db->where('IsSent', 0);
        $this->db->where('FailCount <', 4);
        $this->db->limit($limit);
        $query = $this->db->get();
        return $query->result();
    }
    
    function plusPoint($data){
        foreach($data as $row){
            $cond[] = "(UserID = ".$row['UserID'] . " AND PhoneNumber = '". $row['PhoneNumber']."' AND IsSent = 1)";
        }
        if (isset($cond)){
            $sql = "select * from sms_message where ". implode('OR', $cond);
            $query = $this->db->query($sql);
            $result = $query->result();
            $aPlusPoint = array();
            if ($result){
                foreach($result as $re){
                    foreach($data as $value){
                        if ($re->UserID != $value['UserID'] && $re->PhoneNumber != $value['PhoneNumber']){
                            $aPlusPoint[] = array(
                              'UserID'  => $re->UserID,
                              'PhoneNumber' => $re->PhoneNumber
                            );
                        }
                    }
                }
            }else{
                $aPlusPoint = $data;
            }
            
            if (sizeof($aPlusPoint) > 0){
                foreach($aPlusPoint as $user){
                    $aUserPoint = array(
                        'UserID'    => $user['UserID'],
                        'Point'     =>25,
                        'Type'  => 3
                    );
                    User_model::updateUserPoint($aUserPoint, 'plus');
                }
            }
        }
    }
    
    function SmsInvite($user_id, $phone_number){
        $aNumber = explode(';', $phone_number);
        $temp = array();
        $whereCond  = array();
        foreach ($aNumber as $number){
            if(!empty($number)){
                $temp[] = $number;
                $whereCond[] = "PhoneNumber = '".$number."'";
            }
        }
        if (sizeof($temp) > 0){
            $sCond = implode(' OR ', $whereCond);
            if ($sCond != ''){
                $sCond = "AND ($sCond)";
            }
            $sql = "select PhoneNumber from sms_message where UserID = $user_id $sCond";
            $query = $this->db->query($sql);
            $result = $query->result();
            $existedNumber = array();
            if ($result){
                foreach($result as $row){
                    $existedNumber[] = $row->PhoneNumber;
                }
            }
            
            $plusNumber = array_diff($temp, $existedNumber);
            $numberOfPhone = sizeof($plusNumber);
            if ($numberOfPhone > 0){
                $aUserPoint = array(
                    'UserID' => $user_id,
                    'Type'  => INF_POINT_INVITE_SMS, // 3
                    'Point' => $numberOfPhone * 25,
                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                );
                $this->db->insert('influence_point', $aUserPoint);
                $aUserPoint['NumberFriendInvite'] = $numberOfPhone;
                User_model::updateUserPoint($aUserPoint, 'plus');
                
                // save phone number as sent number
                $created_date = gmdate('Y-m-d H:i:s');
                $value = array();
                foreach($plusNumber as $number){
                    if (!empty($number)){
                        $value[] = "('$number', '$user_id', '$created_date')";
                    }
                }
                if (sizeof($value) > 0){
                    $sql = "insert into sms_message (`PhoneNumber`, `UserID`, `CreatedDate`)
                        value ". implode(',', $value);
                    $this->db->query($sql);
                }
            }
        }
        return true;
    }

}

?>
