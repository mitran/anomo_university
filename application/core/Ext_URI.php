<?php

class Ext_URI extends CI_URI {

    public $vn_param = Array();

    function __construct() {
        parent::__construct();
    }

    /**
     * Explode the URI Segments. The individual segments will
     * be stored in the $this->segments array.
     *
     * @access	private
     * @return	void
     */
    function _explode_segments() {

        //Separate URI in array by '/' mark
        $_segment_arr = explode("/", preg_replace("|/*(.+?)/*$|", "\\1", $this->uri_string));
        $end_method_param = false; // if fine P_SIGN in start of array value, start get parameter in URL
        // Loop throuth segment array to get Method parameter and then get URL parameter
        for ($i = 0; $i < sizeof($_segment_arr); $i++) {
            // Filter segments for security
            $_segment_arr[$i] = trim($this->_filter_uri($_segment_arr[$i]));
            //echo '<pre>';var_dump( strpos($_segment_arr[$i], P_SIGN));

            if ($end_method_param == false && strpos($_segment_arr[$i], P_SIGN) !== 0) { // If not found P_SIGN, it is Method parameter
                $this->segments[] = $_segment_arr[$i];
            } elseif (strpos($_segment_arr[$i], P_SIGN) === 0) { // If found P_SIGN, this is URL parameter
                $end_method_param = true; // Now, not accept Method parameter from here to end
                $key = str_replace(P_SIGN, '', $_segment_arr[$i]); // Get variable of URL parameter
                $value = '';

                if (isset($_segment_arr[$i + 1]) && strpos($_segment_arr[$i + 1], P_SIGN) !== 0) {

                    $value = $_segment_arr[$i + 1]; // Get value of URL parameter
                    $i = $i + 1;
                }

                $this->vn_param[$key] = $value; // Set value to this parameter
            }
        }

        unset($_segment_arr, $i);
    }

    /**
     * Filter segments for malicious characters
     *
     * @access	private
     * @param	string
     * @return	string
     */
    function _filter_uri($str) {
        if ($str != '' AND $this->config->item('permitted_uri_chars') != '') {
            if (!preg_match("|^[" . ($this->config->item('permitted_uri_chars')) . "]+$|i", $str)) {
                exit('The URI you submitted has disallowed characters.');
            }
        }

        return $str;
    }

}

?>