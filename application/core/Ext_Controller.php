<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ext_Controller extends CI_Controller {

    function Ext_Controller() {
        parent::__construct();
        // redirect to login page.
        $this->need_login();
    }

    function need_login() {
        if (($this->uri->segment(1) == 'admin') && ($this->uri->segment(3) != 'login' && $this->uri->segment(3) != 'forgot_password' && $this->uri->segment(3) != 'confirm' && $this->uri->segment(3) != 'reset_password') && $this->session->userdata('role') != 'admin') {
            redirect('admin/index/login');
        }
    }

}

?>
