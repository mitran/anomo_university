<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

//error_reporting(E_ALL);


if (isset($_SERVER['CI_ENV']) && file_exists(__DIR__."/anomo-university-config/{$_SERVER['CI_ENV']}/config-{$_SERVER['CI_ENV']}.php")){
    require(__DIR__."/anomo-university-config/{$_SERVER['CI_ENV']}/config-{$_SERVER['CI_ENV']}.php");
}else{
    require_once(__DIR__."/constants.php");

    /*
    |--------------------------------------------------------------------------
    | Base Site URL
    |--------------------------------------------------------------------------
    |
    | URL to your CodeIgniter root. Typically this will be your base URL,
    | WITH a trailing slash:
    |
    |	http://example.com/
    |
    */

    $config['base_url'] = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
    $config['base_url'] .= "://" . $_SERVER['HTTP_HOST'];
    $config['base_url'] .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);
    $config['email_from'] = EMAIL_FROM; //'admin@anomo.com';
    $config['email_from_name'] = EMAIL_FROM_NAME;
    $config['file_upload'] = 'public/admin/uploads/';
    $config['xml_file'] = 'public/xml/';
    $config['photo_url'] = 'public/upload/';
    $config['share_url'] = 'public/share/';
    $config['s3_share'] = 'Share/';
    $config['s3_photo'] = 'Share/';
    $config['category_icon_url'] = 'public/icons/';
    $config['report_user_to_email'] = REPORT_USER_TO_EMAIL;
    $config['report_match_result_to_email'] = 'luongluu@vinasource.com,minhle@vinasource.com,vuduong@vinasource.com';
    $http = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
    $http .= "://";
    $config['s3_photo_url'] = $http . BUCKET_NAME . '.s3.amazonaws.com/Upload/';
    $config['s3_share_url'] = $http . BUCKET_NAME . '.s3.amazonaws.com/Share/';
    $config['s3_chat_url'] = $http . BUCKET_NAME . '.s3.amazonaws.com/Chat/';
    $config['s3_stream_url'] = $http . BUCKET_NAME . '.s3.amazonaws.com/Stream/';
    $config['s3_sticker_url'] = $http . BUCKET_NAME . '.s3.amazonaws.com/Sticker/';
    $config['s3_video_url'] = $http . BUCKET_NAME . '.s3.amazonaws.com/Video/';

    $config['index_page'] = "index.php";
    $config['uri_protocol'] = "AUTO";
    $config['url_suffix'] = "";
    $config['language'] = "english";
    $config['charset'] = "UTF-8";
    $config['enable_hooks'] = FALSE;
    $config['subclass_prefix'] = 'Ext_';
    $config['permitted_uri_chars'] = '';
    $config['enable_query_strings'] = FALSE;
    $config['controller_trigger'] = 'c';
    $config['function_trigger'] = 'm';
    $config['directory_trigger'] = 'd'; // experimental not currently in use
    $config['log_threshold'] = 1;
    $config['log_path'] = 'public/logs/';
    $config['log_date_format'] = 'Y-m-d H:i:s';
    $config['cache_path'] = '';
    $config['encryption_key'] = "ss";
    $config['sess_cookie_name'] = 'ci_session';
    $config['sess_expiration'] = 7200;
    $config['sess_encrypt_cookie'] = FALSE;
    $config['sess_use_database'] = FALSE;
    $config['sess_table_name'] = 'ci_sessions';
    $config['sess_match_ip'] = FALSE;
    $config['sess_match_useragent'] = TRUE;
    $config['sess_time_to_update'] = 300;
    $config['cookie_prefix'] = "";
    $config['cookie_domain'] = "";
    $config['cookie_path'] = "/";
    $config['global_xss_filtering'] = TRUE;
    $config['compress_output'] = FALSE;
    $config['time_reference'] = 'local';
    $config['rewrite_short_tags'] = FALSE;
    $config['proxy_ips'] = '';
    $config['expired'] = '-7 days';
    $config['reset_password_url'] = RESET_PASSWORD_URL;

}