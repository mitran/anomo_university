<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
/**
 * This is the cronjob for pushing notifications of new chat messages to offline users.
 * Settings: $iLimit = 50
 * Scheduled period: 1 minute
 */

class CronChat extends Ext_Controller
{

    function __construct()
    {
        parent::__construct();

        //Load library
        $this->load->library('AC2DM');
        $this->load->library('APNS');

        //Load model
        $this->load->model('Offline_model');
        $this->load->model('Config_model');
        $this->load->model('User_model');
        $this->load->model('Notification_History_model');
        $this->lang->load('ws');
    }

    function index()
    {
        ob_start();
        ob_clean();

        set_time_limit(0);

        $iLimit = 50;

        $oDataOffline = $this->Offline_model->getMsgOffline($iLimit);
        if ($oDataOffline) {
            $messageQueue = array();
            $ids = array();
            $curAPNSMsgCounter = 0;
            $curPushCounter = 0;

            $apnsConfig = $this->Config_model->getConfig('PUSH_NOTIFICATION_MODE');

            foreach ($oDataOffline as $obj) {

                $logData['UserID'] = $obj->UserID;
                $logData['CreatedDate'] = gmdate('Y-m-d H:i:s');
                $userInfo = $this->User_model->getUserInfo($obj->UserID);
                $total = $this->Notification_History_model->getBadge($userInfo);
                $fromUserObj = $this->User_model->getUserInfo($obj->FromUserID);
                $content = $obj->Message;
                $avatarlink = '';
                if (isset($fromUserObj->Avatar) && !empty($fromUserObj->Avatar)) {
                    $avatarlink = $this->config->item('s3_photo_url') . $fromUserObj->Avatar;
                }
                // check if message is Photo
                if (substr($content, 0, 16) == 'filter_image_tag') {
                    $content = $this->lang->line('send_image_notification');
                }

                $pushStatus = 0;
                if ($obj->DeviceType == 'ios') {
                    $msg = $fromUserObj->UserName . ': ' . $content;

                    if (APN_MSG_LENGTH < strlen($msg)) {
                        $msg = substr($msg, 0, APN_MSG_LENGTH) . '...';
                    }
                    $apnMsg['aps']['alert'] = $msg;
                    $sound = 'abc.aiff';
                    $apnMsg['aps']['sound'] = (string)$sound;
                    $apnMsg['t'] = 1;

                    $apnMsg['f'] = $obj->FromUserID;
                    $apnMsg['u'] = $obj->UserID;
                    $apnMsg['n'] = isset($fromUserObj->UserName) ? $fromUserObj->UserName : '';
                    $apnMsg['aps']['badge'] = (int)$total;
                    $msgToPush = json_encode($apnMsg);
                    $logData['Platform'] = 'ios';
                    $logData['Message'] = $msgToPush;

                    $messageQueue[$curAPNSMsgCounter] = array('push_data' => $logData, 'body' => $msgToPush, 'token' => $obj->DeviceID);
                    $curAPNSMsgCounter += 1;

                    // For iOS, temporarily update IsPush to 1... for now.
                    // Hung Pham - 23/10/2013
                    // $this->Offline_model->update(array('IsPush' => 1), array('ID' => $obj->ID));
                    $ids[$curPushCounter] = $obj->ID;
                    $curPushCounter++;
                    // $pushStatus = $this->apns->_pushMessage($logData, $msgToPush, $obj->DeviceID, $this->Config_model->getConfig('PUSH_NOTIFICATION_MODE'));
                }

                if ($obj->DeviceType == 'android') {
                    $json = array();
                    $json[] = 1;
                    $json[] = isset($fromUserObj->UserName) ? $fromUserObj->UserName : '';
                    $json[] = (strlen($content) > AC2DM_MSG_LENGTH) ? substr($content, 0, AC2DM_MSG_LENGTH) . '...' : $content;
                    $json[] = $total;
                    $json[] = $avatarlink;
                    $json[] = $obj->FromUserID;
                    $json[] = "";
                    $json[] = "";
					$json[] = "";
                    $json[] = "";
                    $c2dmMsg = json_encode($json);
//                    $logData['Platform'] = 'android';
//                    $logData['Message'] = $c2dmMsg;

                    $pushStatus = $this->ac2dm->send($obj->DeviceID, 'text', $c2dmMsg, null);

                    if ($pushStatus == 1)
                    {
                        // $this->Offline_model->update(array('IsPush' => 1), array('ID' => $obj->ID));
                        $ids[$curPushCounter] = $obj->ID;
                        $curPushCounter++;
                    }
                    else
                        $this->Offline_model->updateNumberFail($obj->ID);
                }

                // check if push successful then update IsPush=1
            }

            $this->apns->_pushMessageQueue($messageQueue, $apnsConfig);
            $this->Offline_model->updateStatus($ids);
        }
    }

}
