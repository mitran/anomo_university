<?php

class CronActivityArchive extends Ext_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        set_time_limit(0);
        $limit = 200;
        $time = "-1 month";
        $sql1 = "select ActivityID from activity where CreatedDate <'". date('Y-m-d H:i:s', strtotime($time, strtotime(gmdate('Y-m-d H:i:s'))))."' limit $limit";
        $query = $this->db->query($sql1);
        $result = $query->result();
        if ($result) {
            $ids = array();
            foreach ($result as $re) {
                $ids[] = $re->ActivityID;
            }
            if (sizeof($ids) > 0) {
                $sId = implode(',', $ids);
                // copy to activity_archive
                $insertSql = "insert into activity_archive
                                select * from activity where ActivityID IN (" .$sId . ")";
                $insert = $this->db->query($insertSql);

                // delete activity table
                if ($insert) {
                    $deleteSql = "delete from activity where ActivityID IN (" . $sId . ")";
                    $this->db->query($deleteSql);
                }
            }
        }
    }

}

