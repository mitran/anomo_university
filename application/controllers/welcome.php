<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends Ext_Controller {

	function __construct()
	{
		parent::__construct();
                $this->load->model("Wstoken_model");
	}

	function index()
	{ 
	$env = isset($_SERVER['CI_ENV']) ? $_SERVER['CI_ENV'] : 'development';
	var_dump($env);
		if ($this->session->userdata('role') !== 'admin'){
			redirect('admin/index/login/welcome');
		}
		$this->load->helper('url');
		$this->load->view('welcome_message');
	}

    function getCacheInfo()
    {
        $this->Wstoken_model->getCacheInfo();
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */