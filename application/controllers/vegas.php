<?php
class Vegas extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Vegas_model');
    }
    
    function day1 (){
        require_once APPPATH.'libraries/Mobile_Detect.php';
        $detect = new Mobile_Detect;
        $url = 'http://www.anomo.com';
        if ($detect->isIOS()) {
            $url = "https://itunes.apple.com/us/app/anomo-meet-new-people/id529027583?mt=8";
        }
        if ($detect->isAndroidOS()) {
            $url = "https://play.google.com/store/apps/details?id=com.vinasource.anomoinc.anomo";
        }
        // ANOMO-10771 - Vegas.com - Track when user uses redirect links
        $this->Vegas_model->increaseView();
        redirect($url);
    }
    
    function plan(){
        require_once APPPATH.'libraries/Mobile_Detect.php';
        $detect = new Mobile_Detect;
        $url = 'http://www.anomo.com';
        if ($detect->isIOS()) {
            $url = "https://itunes.apple.com/us/app/anomo-meet-new-people/id529027583?mt=8";
        }
        if ($detect->isAndroidOS()) {
            $url = "https://play.google.com/store/apps/details?id=com.vinasource.anomoinc.anomo";
        }
        // ANOMO-10771 - Vegas.com - Track when user uses redirect links
        $this->Vegas_model->increaseView();
        redirect($url);
    }

}
?>
