<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron_Notify_Daily_Match extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('MyPushNotify');
        $this->lang->load('ws');
        $this->load->model('Config_model');
    }

    function index() {
        ob_start();
        ob_clean();
        set_time_limit(0);

        $iLimit = 20;
        $match_repeat_limit = $this->Config_model->getConfig('MATCH_REPEAT_LIMITATION'); // hour
        if (!$match_repeat_limit){
            $match_repeat_limit = 12;
        }
        $match_repeat_limit = $match_repeat_limit * 3600; // second
        $sql = "select t.CurrentUserID 
                from(
                        select CurrentUserID, max(CreatedDate) as maxDate from dailymatch 
                        where DATE_ADD(CreatedDate, INTERVAL $match_repeat_limit SECOND) < NOW()
                        group by CurrentUserID  
                    ) as t
                inner join dailymatch d ON d.CurrentUserID = t.CurrentUserID AND d.CreatedDate = t.maxDate AND d.IsNotify = 0
                limit $iLimit
                ";
        $query = $this->db->query($sql);
        $result = $query->result();
        $success = array();
        $sender_id = -1;
        if ($result){
            foreach($result as $row){
                $is_push = $this->mypushnotify->send($row->CurrentUserID, $sender_id, NOTIFY_NEW_MATCH, $this->lang->line('msg_new_match'));
                if ($is_push){
                    $success[] = $row->CurrentUserID;
                }
            }
            
            if (sizeof($success) > 0){
                $update_sql = "update dailymatch set IsNotify = 1 where CurrentUserID IN (".implode(',', $success).") AND IsNotify = 0";
                $this->db->query($update_sql);
            }
        }
    }

}
