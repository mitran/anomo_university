<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron_AgeLess_Follower extends Ext_Controller {

    function __construct() {
        parent::__construct();
        //Load model
        $this->load->model('Cron_Mention_User_model');
        $this->load->model('Notification_History_model');
    }

    function index() {
        ob_start();
        ob_clean();
        set_time_limit(0);

        $iLimit = 20;

        $objNotifyFollower = $this->Cron_Mention_User_model->getListNotifyFollower($iLimit);
        if ($objNotifyFollower) {
            $aHistory = array();
            $aPushNotify = array();
            $time = gmdate('Y-m-d H:i:s');
            $idUpdated = array();
            foreach ($objNotifyFollower as $obj) {
                $idUpdated[] = $obj->ID;
                $user_id = $obj->UserID;
                // get all follower of the ageless user
                $sql = "select CurrentUserID from userconnect where TargetUserID = $user_id AND IsFavorite = 1";
                $query = $this->db->query($sql);
                $followerObj = $query->result();
                if ($followerObj) {
                    $notify_ageless_post_type = NOTIFY_AGELESS_POST;
                    foreach ($followerObj as $user) {
                        if ($user->CurrentUserID != $user_id) {
                            $aHistory = array(
                                'UserID' => $user->CurrentUserID,
                                'SendUserID' => $user_id,
                                'Type' => HISTORY_AGELESS_POST, // 19
                                'RefID' => $obj->ContentID,
                                'ContentType' => $obj->ContentType,
                                'ContentID' => $obj->ContentID,
                                'IsAnonymous' => 0,
                                'IsRead' => 1,
                                'UnView' => 1,
                                'CreatedDate' => $time
                            );
                            $history_id = $this->Notification_History_model->add($aHistory);
                            if ($history_id) {
                                $aPushNotify [] = "($user->CurrentUserID, $user_id, $notify_ageless_post_type, $obj->ContentID, $obj->ContentType, 0, 0, '$time', '$history_id', '0')";
                            }
                        }
                    }
                }
            }
            if (sizeof($idUpdated) > 0){
                $this->Cron_Mention_User_model->updateNotifyFollower($idUpdated);
            }
            
            // add to queue to push notify
            if (sizeof($aPushNotify) > 0) {
                $this->Cron_Mention_User_model->addMulti(array_unique($aPushNotify));
            }
        }
    }

}
