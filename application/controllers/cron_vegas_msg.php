<?php
class Cron_Vegas_Msg extends Ext_Controller {

    function __construct() {
        parent::__construct();

        //Load library
        $this->load->library('Pubnub');
        $this->load->library('APNS');

        //Load model
        $this->load->model('Vegas_Msg_model');
        $this->load->model('User_model');
        $this->load->model('Chat_Count_model');
        $this->load->model('Message_model');
    }

    function index() {
        error_reporting(E_ALL & E_STRICT);
        ob_start();
        ob_clean();
        set_time_limit(0);
        $iLimit = 200;
        
        $aUserID = array();
        $oDataTracking = $this->Vegas_Msg_model->getMessageTracking($iLimit);
        if ($oDataTracking){
            // get sender infor
            $senderObj = $this->User_model->getUserInfo(MASS_MESSAGE_SENDER);
            if (!$senderObj){
                exit();
            }else{
                $sender_id = $senderObj->UserID;
                $sender_name = $senderObj->UserName;
                $sender_avatar = ($senderObj->Avatar)?$this->config->item('s3_photo_url').$senderObj->Avatar:'';
            }
            $statusTracks = array();
            // ANOMO-9859 - use correct sandbox/production channel name
            // on sandbox the channel name start with 'dbg_'
            $prefix = '';
            $is_production = PRODUCTION;
            if (!$is_production) {
                $prefix = 'dbg_';
            }
            foreach($oDataTracking as $obj){
                $time = round(microtime(true) * 1000); 
                $receiver_id = $obj->UserID;
                $msg_id = $sender_id.'_'.$receiver_id.'_'.$time;
                $msg = array(
                    'time' => (string)$time,
                    'text' => ($obj->Photo != '') ? '[image]' : $obj->Message,
                    'receiver_id'   => (string)$receiver_id,
                    'receiver_avatar' => $obj->Avatar,
                    'receiver_name' => $obj->UserName,
                    'sender_id' => (string)$sender_id,
                    'sender_name'   => $sender_name,
                    'sender_avatar' => $sender_avatar,
                    'type' => 'MESSAGE',
                    'photo' => $obj->Photo,
                    'message_id'    => (string)$msg_id,
                    'eatReceipts' => '1',
                    'fullscreen' => ($obj->Photo != '')?1:"",
                    'tap_action'    => ($obj->Photo != '') ? $obj->TapAction : '',
                    'tap_param' => ($obj->Photo != '') ? $obj->TapParam:''
                );
                
                $args['channel'] = $prefix.$obj->UserID;
                $args['message'] = $msg;
                // send msg
                $response = $this->pubnub->publish($args);
                if ($response && $response[0] == 1){//success
                    $statusTracks[] = array('MessageID' => $obj->MessageID, 'UserID' => $obj->UserID);
                    // log UserID & msg if device is IOS
                    if ($obj->Type == 'ios'){
                        $aUserID[] = $obj->UserID;
                        $aMsg[$obj->UserID] = $obj->Message;
                    }
                }
                
            }
            // push notify to IOS devices
            if (sizeof($aUserID) > 0){
                $messageQueue = array();
                $aUpdateChatCount = array();
                $aInsertChatCount = array();
                $pushObj = $this->Message_model->getPushReceiverInfo($aUserID);
                if ($pushObj){
                    foreach($pushObj as $deviceObj){
                        if($deviceObj->Type == 'ios' && $deviceObj->AllowChatNotice == 1){
                            $msg = $aMsg[$deviceObj->UserID];
                            if (APN_MSG_LENGTH < strlen($msg)) {
                                $msg = substr($msg, 0, APN_MSG_LENGTH) . '...';
                            }
                            // other badge count
                            $total = $deviceObj->Number;
                            // chat badge count
                            $total += (int)$deviceObj->ChatCount;
                            // increase #chat
                            $total += 1;
                            // update #ChatCount
                            if (is_null($deviceObj->ChatCount)){
                                $aInsertChatCount[$deviceObj->UserID] = $deviceObj->UserID;
                            }else{
                                $aUpdateChatCount[$deviceObj->UserID] = $deviceObj->UserID;
                            }
                            
                            $apnMsg = array();
                            $logData = array();
                            
                            $sound = 'receiveText.aiff';
                            $apnMsg['aps']['alert'] = $msg;
                            $apnMsg['aps']['badge'] = (int) $total;
                            $apnMsg['aps']['sound'] = (string) $sound;
                            $apnMsg['AIM'] = "YES";
                            $msgToPush = json_encode($apnMsg);
                            
                            $messageQueue[] = array('push_data' => $logData, 'body' => $msgToPush, 'token' => $obj->DeviceID);
                        }
                    }
                    // push msg notify
                    if (sizeof($messageQueue) > 0){
                        $this->apns->_pushMessageQueue($messageQueue);
                    }
                    
                    // update #ChatCount
                    if (sizeof($aUpdateChatCount) > 0){
                        $this->Chat_Count_model->updateMulti($aUpdateChatCount);
                    }
                    if (sizeof($aInsertChatCount) > 0){
                        $this->Chat_Count_model->insertMulti($aInsertChatCount);
                    }
                }
            }
            
            // update status for msg (1: sent / 0: waiting)
            if (sizeof($statusTracks) > 0){
                $this->Vegas_Msg_model->updateStatusTrackings(array('Status' => 1), $statusTracks);
            }
            $this->Vegas_Msg_model->updateMessageStatus();
        }
    }
}
