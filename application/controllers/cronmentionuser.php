<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CronMentionUser extends Ext_Controller
{

    function __construct()
    {
        parent::__construct();

        //Load library
        $this->load->library('MyPushNotify');

        //Load model
        $this->load->model('Cron_Mention_User_model');
        $this->load->model('Config_model');
        $this->load->model('User_model');
        $this->load->model('Notification_History_model');
        $this->lang->load('ws');
    }

    function index()
    {
        ob_start();
        ob_clean();
        set_time_limit(0);

        $iLimit = 50;
        
        $objMentionUser = $this->Cron_Mention_User_model->getList($iLimit);
        if ($objMentionUser){
            $successIds = array();
            $failedIds = array();
            foreach ($objMentionUser as $obj){
                $msg = $this->lang->line('msg_mention_user');
                if ($obj->Type == NOTIFY_AGELESS_POST){// Notification to ALL FOLLOWERS of the ageless user
                    $msg = $this->lang->line('msg_ageless_post');
                }
                $push_status = $this->mypushnotify->send($obj->UserID, $obj->FromUserID, $obj->Type, $msg, array('ContentID' => $obj->ContentID, 'ContentType' => $obj->ContentType, 'HistoryID' => $obj->HistoryID, 'IsAnonymous' => $obj->IsAnonymous));
                
                if ($push_status == 1){
                    $successIds[] = $obj->ID;
                }else{
                    $failedIds[] = $obj->ID;
                }
                
            }
            if (sizeof($successIds) > 0){
                $this->Cron_Mention_User_model->updateStatus($successIds, 'success');
            }
            
            if (sizeof($failedIds) > 0){
                $this->Cron_Mention_User_model->updateStatus($failedIds, 'failed');
            }
        }     
    }

}
