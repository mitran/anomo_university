<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron_Has_FanPage extends Ext_Controller {

    function __construct() {
        parent::__construct();        
        $this->load->model('User_model');
    }
    
    function index(){
        set_time_limit(0);        
        $this->User_model->updateHasFanPage(130000);        
    }

}
