<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'libraries/SolrPHPClient/Service.php');

class CronIndexing extends Ext_Controller{
    var $solr;

    function __construct()
    {
        parent::__construct();
        $this->solr = new Apache_Solr_Service(SOLR_HOSTNAME, SOLR_PORT, SOLR_PATH);
    }

    function index() {
        set_time_limit(0);
        $limit = 200;

        if (!$this->solr->ping()) {
            echo 'Solr service not responding.';
            exit;
        }

        //1. Add new column
        // ALTER TABLE  `userconnect` ADD  `IsIndexing` INT( 11 ) NULL DEFAULT  '0' ;

        /*
         April_17_2014 - add cron job to index
         - IsIndexing value:
            + 2: Need indexing (follow)
            + 3: Need remove indexing (unfollow)
            + 4: Need remove indexing and remove contact (for block user function)
         - Reindexing data when user change username
        */
        
        $documents = array();
        $updateCond = array();
        $deleteCond = array();
        $deleteIds = array();
        $userIds = array();

        // 1. Indexing (Follow)
        $sql = "select  CurrentUserID as `name`,
                        (select UserName from `user` where UserID = CurrentUserID) as description,
			            TargetUserID as `resourcename`,
			            (select UserName from `user` where UserID = TargetUserID) as `keywords`
                from userconnect where IsFavorite = 1 AND IsIndexing = 2 limit $limit";
        $query = $this->db->query($sql);
        $result = $query->result();
        if ($result) {
            foreach ($result as $fields) {
                $updateCond[] = "( CurrentUserID =" . $fields->name . " AND TargetUserID = " . $fields->resourcename . " )";

                $part = new Apache_Solr_Document();
                $part->addField('id', $fields->name . '-' . $fields->resourcename);

                foreach ($fields as $key => $value) {
                    if (is_array($value)) {
                        foreach ($value as $data) {
                            $part->addField($key, $data);
                        }
                    } else {
                        $part->$key = $value;
                    }
                }
                $documents[] = $part;
            }
        }

        // 2. Delete indexing (Unfollow / Block)
        $sql1 = "select CurrentUserID, TargetUserID, IsIndexing  from userconnect where IsFavorite = 0 AND (IsIndexing = 3  OR IsIndexing = 4) limit $limit";
        $query1 = $this->db->query($sql1);
        $result1 = $query1->result();
        if ($result1) {
            foreach ($result1 as $re) {
                $deleteIds [] = $re->CurrentUserID . '-' . $re->TargetUserID;
                if ($re->IsIndexing == 3){
                    $updateCond[] = "(CurrentUserID =" . $re->CurrentUserID . " AND TargetUserID = " . $re->TargetUserID .")";
                }else{
                    $deleteCond[] = "(CurrentUserID =" . $re->CurrentUserID . " AND TargetUserID = " . $re->TargetUserID .")";
                }
            }
        }
        
        // 3. Reindex when user change username
        $userSql = " select UserID from user where IsUpdateUserName = 1 limit $limit";
        $userQuery = $this->db->query($userSql);
        $userObj = $userQuery->result();
        if ($userObj){
            $userIds = array();
            foreach($userObj as $obj){
                $userIds[] = $obj->UserID;
            }
            $sUserID = implode(',', $userIds);
            // delete old indexing
            $delSql = "select CurrentUserID, TargetUserID from userconnect where IsFavorite = 1 AND (CurrentUserID IN ($sUserID) OR TargetUserID IN ($sUserID))";
            $delQuery = $this->db->query($delSql);
            $delResult = $delQuery->result();
            if ($delResult){
                foreach ($delResult as $re){
                    $deleteIds [] = $re->CurrentUserID.'-'.$re->TargetUserID;
                }
            }
            
            //Re-indexing with new UserName
            $reSql = "select  CurrentUserID as `name`,
                            (select UserName from `user` where UserID = CurrentUserID) as description,
                                        TargetUserID as `resourcename`,
                                        (select UserName from `user` where UserID = TargetUserID) as `keywords`
                    from userconnect where IsFavorite = 1 AND (CurrentUserID IN ($sUserID) OR TargetUserID IN ($sUserID))";
            $reQuery = $this->db->query($reSql);
            $result = $reQuery->result();
            if ($result){
                foreach ($result as $fields) {
                    $part = new Apache_Solr_Document();
                    $part->addField('id', $fields->name . '-' . $fields->resourcename);
                    foreach ($fields as $key => $value) {
                        if (is_array($value)) {
                            foreach ($value as $data) {
                                $part->addField($key, $data);
                            }
                        }
                        else {
                            $part->$key = $value;
                        }
                    }
                    $documents[] = $part;
                }
            }
        }
        
        // connect to solr and commit
        if (sizeof($documents) > 0 || sizeof($deleteIds) > 0){
            try {
                
                if (sizeof($deleteIds) > 0){
                    $id = "(" . implode('OR', $deleteIds) . ")";
                    $rawPost = "<delete><query>id:$id</query></delete>";
                    $this->solr->delete($rawPost);
                }
                
                if (sizeof($documents) > 0){
                    $this->solr->addDocuments($documents);
                }
                
                $this->solr->commit();
                $this->solr->optimize();
                
                // update db
                if (sizeof($updateCond) > 0) {
                    $sql = " update userconnect set IsIndexing = 1 where " . implode('OR', $updateCond);
                    $this->db->query($sql);
                }
                     
                if (sizeof($deleteCond) > 0){
                    $sql = "delete from userconnect where ". implode('OR', $deleteCond);
                    $this->db->query($sql);
                }
                
                if (sizeof($userIds) > 0){
                    $sql = "update user set IsUpdateUserName = 0 where UserID IN (".  implode(',', $userIds).")";
                    $this->db->query($sql);
                }
                
            } catch (Exception $e) {
                echo $e->getMessage() . '<br/>';
            }
        }   
    }

}



