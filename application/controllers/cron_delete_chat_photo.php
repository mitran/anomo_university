<?php
class Cron_Delete_Chat_Photo extends Ext_Controller {

    function __construct() {
        parent::__construct();

        //Load library
       $this->load->library('S3Upload');
    }

    function index() {
        error_reporting(E_ALL & E_STRICT);
        ob_start();
        ob_clean();
        set_time_limit(0);
        
        
        $today = date('Y-m-d');
        echo "Today: ".$today."<br/>";
        
        $day = date('Y-m-d',strtotime("-30 days"));                          
        $timestamp = strtotime($day);        
        
        echo "Delete all chat's photos before ".$day." ($timestamp)<br/>";
        
        $this->load->library('S3Upload');        
        $result = $this->s3upload->listFileByFolder(BUCKET_NAME_CHAT);
                
        foreach($result as $file){
            if(strlen($file['name']) > (strlen(BUCKET_NAME_CHAT) + 1)){
                if($file['time'] < $timestamp){
                    print_r ("Delete: " .$file['name']." - ");
                    print_r (date('Y-m-d H:i:s',$file['time'])."<br/>");
                    $this->s3upload->deleteObjectS3($file['name']);
                } 
            }
        }
        
    }
}
