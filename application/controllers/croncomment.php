<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CronComment extends Ext_Controller
{

    function __construct()
    {
        parent::__construct();

        //Load library
        $this->load->library('MyPushNotify');

        //Load model
        $this->load->model('Cron_Push_Notification_model');
    }

    function index()
    {
        ob_start();
        ob_clean();
        set_time_limit(0);

        $iLimit = 100;

        $objComment = $this->Cron_Push_Notification_model->getCommentForPushNotify($iLimit);
        if ($objComment && NULL != $objComment) {
            $success = array();
            $failed = array();
            foreach ($objComment as $obj) {
                $pushStatus = 0;
                if ($obj->AllowCommentActivityNotice == 1){
                    $pushStatus = $this->mypushnotify->send($obj->UserID, $obj->FromUserID, 7, $obj->Message, array('ContentID' => $obj->ContentID, 'ContentType' => $obj->Type, 'HistoryID' => $obj->HistoryID, 'IsAnonymousComment' => $obj->IsAnonymousComment));    
                }
                
                // upate stattus for cronpushnotification
                if ($pushStatus == 1) {
                    $success[] = $obj->ID;
                }else{
                    $failed[] = $obj->ID;
                }
            }
            if (sizeof($success) > 0){
                $this->Cron_Push_Notification_model->updateCron($success, 'success');
            }
            
            if (sizeof($failed) > 0){
                $this->Cron_Push_Notification_model->updateCron($failed, 'failed');
            }
        }
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */