<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class qanda extends Ext_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->model('qanda_model');
        $this->load->model('Question_model');
    }

    function index()
    {
        $this->load->view('admin/qanda');
    }

    //function to handle callbacks
    function datatable()
    {
        $this->datatables->select('q.QuestionID QID, q.QuestionGroupID QGroupID, g.Category QCategory, g.ExpireDate QExpireDate, q.Content Questions, q.QuestionType QType, q.Order QOrder, q.Weight QWeight,
        group_concat(a.AnswerID) AnswerID, group_concat(a.Content) Answers')
            ->unset_column('QID') //only one - ID column
            ->unset_column('QGroupID')
            ->unset_column('AnswerID') //One more ID column for a join
            ->from('question q')
            ->join('answer a', 'q.QuestionID=a.QuestionID', $type = LEFT)
            ->join('questiongroup g', 'q.QuestionGroupID = g.QuestionGroupID', $type = LEFT)
            ->group_by('q.QuestionID');

        echo $this->datatables->generate();
    }

    function edit()
    {
        $ids = $this->input->post('id');
        $attr = $this->input->post('columnname');
        $newval = $this->input->post('value');
        $attr = preg_replace('/\s+/', '', $attr);
        $this->qanda_model->update($ids, $attr, $newval);

    }

    function delete()
    {
        /*$id = $this->input->post('id');
        $this->qanda_model->delete($id);*/
    }

    function add()
    {
        // get params and verification
        $QGroupID = ($this->input->post('QGroupID')) ? $this->input->post('QGroupID') : NULL;
        $QContent = ($this->input->post('QContent')) ? $this->input->post('QContent') : NULL;
        $QType = 2;
        // $QOrder = ($this->input->post('QOrder')) ? $this->input->post('QOrder') : NULL;
        $AContent = ($this->input->post('AContent')) ? $this->input->post('AContent') : NULL;
        $QGroupCategory = ($this->input->post('QGroupCategory')) ? $this->input->post('QGroupCategory') : NULL;
        $QGroupExpireDate = ($this->input->post('QGroupExpireDate')) ? $this->input->post('QGroupExpireDate') : NULL;
        $QWeight = ($this->input->post('QWeight')) ? $this->input->post('QWeight') : NULL;
        // if any params are null... just pop a message... hardcoding this for the time being...
        if ($QContent == NULL || $AContent == NULL) {
            echo 'Input fields cannot be empty';
            return;
        }

        // if the QGroupID is empty, create one new QGroup
        if ($QGroupID == NULL || $QGroupID == "") {
            $QGroupID = $this->add_question_group($QGroupCategory, $QGroupExpireDate);
        }

        $this->qanda_model->insert($QGroupID, $QContent, $QType, $AContent, $QWeight);

        echo 'OK';
    }

    function add_question_group($category, $expire_date = null)
    {
        $data['category'] = $category;
        $data['createdDate'] = gmdate('Y-m-d H:i:s');
        if ($expire_date)
            $data['expireDate'] = $expire_date;
        return $this->Question_model->addQuestionGroup($data);
    }
}

?>
