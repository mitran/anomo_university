<?php
class Neighborhood extends Ext_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper('Form');
		$this->load->model('Neighborhood_model');
	}
	
	function index(){
		$data = array();
		if (isset($_POST['lat']) && isset($_POST['lng'])) {
			$data['results'] = $this->Neighborhood_model->find_by_coordinates($_POST['lat'], $_POST['lng']);
			$data += $_POST;
		}
		$this->load->view('admin/neighborhood', $data);
	}
}
?>