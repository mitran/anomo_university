<?php

class User extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Activity_model');
        $this->load->helper('Form');
        $this->load->library('Pagination');
        $this->load->library('S3Upload');
        $this->load->helper('Ext_URI');
        $this->load->model('Image_model');
        $this->load->model('Wstoken_model');
        $this->load->model('reported_content_model');
    }

    function index() {
        $cond = array();
        if (isset($this->uri->vn_param['status'])) {
            $cond['status'] = $this->uri->vn_param['status'];
        }

        if (isset($this->uri->vn_param['shared'])) {
            $cond['shared'] = $this->uri->vn_param['shared'];
        }

        if (isset($this->uri->vn_param['share_to_wall'])) {
            $cond['share_to_wall'] = $this->uri->vn_param['share_to_wall'];
        }
        
        if (isset($this->uri->vn_param['block'])) {
            $cond['block'] = $this->uri->vn_param['block'];
        }
        
        if (isset($this->uri->vn_param['flag'])) {
            $cond['flag'] = $this->uri->vn_param['flag'];
        }

        if (isset($this->uri->vn_param['sort'])) {
            $cond['sort'] = $this->uri->vn_param['sort'];
        } else {
            $cond['sort'] = 'id-desc';
        }

        if (isset($_POST['btn_submit'])){
            $keyword = isset($_POST['keyword'])?$_POST['keyword']:'';
            if ($keyword != ''){
                $this->session->set_userdata('s_user_keyword', $keyword);
                $data['keyword'] = $keyword;
                $cond['keyword'] = $keyword;
            }
        }elseif ((int) $this->uri->segment(4) > 0){
            if ($this->session->userdata('s_user_keyword')){
                $keyword = $this->session->userdata('s_user_keyword');
                $data['keyword'] = $keyword;
                $cond['keyword'] = $keyword;
            }
        }else{
            $this->session->unset_userdata('s_user_keyword');
        }
        
        $config['base_url'] = base_url() . 'admin/user/index';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $config['total_rows'] = $this->User_model->getListByCondition($cond, $config['per_page'], (int)$this->uri->segment(4), true);
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['results'] = $this->User_model->getListByCondition($cond, $config['per_page'], (int)$this->uri->segment(4), false);
        $this->load->view('admin/user', $data);
    }

    function change_email_status($user_id, $status_id) {
        $data['EmailAccountStatus'] = $status_id;
        if ($status_id == 0) { // change to not active => update SignUpDate
            $data['SignUpDate'] = gmdate('Y-m-d H:i:s');
        }
        log_message('error', '----- log update ----- Change Email Status...');
        $logMsg = var_export($data, TRUE);
        log_message('error', $logMsg);
        log_message('error', date('l jS \of F Y h:i:s A'));
        $this->User_model->updateUser($data, array('UserID' => $user_id));
    }

    function post($user_id = 0) {
        $data['user_id'] = $user_id;
        if ($user_id > 0) {
            $info = $this->User_model->getUserInfo($user_id, 'full');
            $data['info'] = $info;
        }
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'UserName', 'trim|required'); 
            $this->form_validation->set_rules('credits', 'Credits', 'trim|required|integer'); 
            $upload['error'] = false;
            if ($user_id == 0 || ($user_id > 0 && $_FILES['photo']['name'] != '')) {
                $isUpload = $this->s3upload->upload('photo');
                if (!$isUpload) {
                    $upload['error'] = "Error upload image";
                } else {
                    $upload['file_name'] = $isUpload;
                }
            }

            $upload_full['error'] = false;
            if ($user_id == 0 || ($user_id > 0 && $_FILES['full_photo']['name'] != '')) {
                $isfull_photo = $this->s3upload->upload('full_photo');
                if (!$isfull_photo) {
                    $upload_full['error'] = "Error upload image";
                } else {
                    $upload_full['file_name'] = $isfull_photo;
                }
            }
            
            $fbid_error = false;
            if (isset($_POST['fbid']) && trim($_POST['fbid']) != ''){
                $fbExistObj = $this->User_model->getFbExist($_POST['fbid'], $user_id);
                if ($fbExistObj) {
                    $fbid_error = true;
                    $data['fbid_msg_error'] = 'The FacebookID field is invalid.';
                }
            }
            
            if ($this->form_validation->run() == FALSE || $upload['error'] || $upload_full['error'] || $fbid_error) {
                $this->load->view('admin/user_form', $data);
            } else {
                if (isset($upload['file_name']) && $upload['file_name'] != '') {
                    $photo = $upload['file_name'];
                    $isUpdatePhoto = true;
                } else {
                    $photo = $info->Photo;
                }
                if (isset($upload_full['file_name']) && $upload_full['file_name'] != '') {
                    $full_photo = $upload_full['file_name'];
                } else {
                    $full_photo = $info->FullPhoto;
                }
                $value = array(
                    'UserName' => $_POST['name'],
                    'Photo' => $photo,
                    'FullPhoto' => $full_photo,
                    'FacebookID'    => $_POST['fbid'],
                    'Credits' => $_POST['credits']
                );
                log_message('error', '----- log update ----- Method Post...');
                $logMsg = var_export($value, TRUE);
                log_message('error', $logMsg);
                log_message('error', date('l jS \of F Y h:i:s A'));
                $ret = $this->User_model->updateUser($value, array('UserID' => $user_id));
                redirect('admin/user');
            }
        } else {
            $this->load->view('admin/user_form', $data);
        }
    }

    function delete($user_id) {
        $info = $this->User_model->getUserInfo($user_id);
        if (count($info) > 0) {
            $re = $this->User_model->deleteAccount($user_id);
           if ($re){
               $data['msg'] = 'Delete user successful';
           }else{
               $data['msg'] = 'Error. Please try again!';
           }
            
        } else {
            $data['msg'] = "User not exists";
        }
        redirect('admin/user/index/p_delete/' . $data['msg']);
    }

    function export_csv() {
        header('Content-Encoding: UTF-8');
        header('Content-Description: File Transfer');
        header("Content-Type: application/csv; charset=UTF-8");
        header("Content-Disposition: attachment; filename=Report.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        $cond['shared'] = 'all';
        $cond['sort'] = 'fb-share-desc';
        $csvData[] = "UserID, UserName, RealName, Gender, BirthDate, FaceboookID, FacebookUrl, College/University, Major, SignUpDate\n";
        $results = $this->User_model->getListByCondition($cond);
        if ($results) {
            foreach ($results as $re) {
                $csvData[] = $re->UserID . ',' . $re->UserName . ',' . $re->RealName . ',' . $re->Gender . ',' . $re->BirthDate . ',' . $re->FacebookID . ',' . $re->FbEmail . ',' . $re->College . ',' . $re->Major . ',' . $re->SignUpDate . "\n";
            }
        }

        foreach ($csvData as $row) {
            echo $row;
        }
    }

    function ban_user($user_id, $status) {
        $data = array();
        $isBanned = ($status == 0) ? 1 : 0;
        $data['IsBanned'] = $isBanned;
        $data['BannedDate'] = gmdate('Y-m-d H:i:s');
        $tokenInfo = $this->Wstoken_model->getTokenOfUser($user_id);
        $token = ($tokenInfo)?$tokenInfo->Token:'';
        $this->db->trans_begin();
            $this->User_model->updateUser($data, array('UserID' => $user_id), $token);
            // then update all activty's user
            $this->Activity_model->update(array('IsInvalid' => $isBanned), array('FromUserID' => $user_id));
            $this->reported_content_model->updateActivityArchive(array('IsInvalid' => $isBanned), array('FromUserID' => $user_id));
            $this->Wstoken_model->delete(array('UserID' => $user_id));
            
            // topic 
            $sql = "select TopicID from topic where UserID = $user_id";
            $query = $this->db->query($sql);
            $result = $query->result();
            if ($result){
                $aTopicID = array();
                foreach($result as $re){
                    $aTopicID[] = $re->TopicID;
                }
                if (sizeof($aTopicID) > 0){
                    $this->db->query("update activity set IsInvalid = $isBanned where TopicID IN (".  implode(',', $aTopicID) . ")");
                    $this->db->query("update topic set IsDelete = $isBanned where TopicID IN (".  implode(',', $aTopicID) . ")");
                    // remove topic favorite
                    if ($isBanned == 1)
                    $this->db->query("delete from topicfavorite where TopicID IN (".implode(',', $aTopicID).")");
                }
            }
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
        redirect('admin/user/index/');
    }
    
    function interaction(){
        $data = array();
        if (isset($_POST['date'])){
            $date= isset($_POST['date'])?$_POST['date']:'';
            if ($date != ''){
                $this->session->set_userdata('s_interaction_date', $date);
                $data['date'] = $date;
                $cond['SignUpDate'] = $date;
            }
        }elseif ((int) $this->uri->segment(4) > 0){
            if ($this->session->userdata('s_interaction_date')){
                $date = $this->session->userdata('s_interaction_date');
                $data['date'] = $date;
                $cond['SignUpDate'] = $date;
            }
        }else{
            $this->session->unset_userdata('s_interaction_date');
        }
        if (isset($cond['SignUpDate'])) {
            $config['base_url'] = base_url() . 'admin/user/interaction';
            $config['per_page'] = 20;
            $config['uri_segment'] = 4;
            $total = 0;
            $result = $this->User_model->listUserBy($cond, $total, $config['per_page'], (int) $this->uri->segment(4));
            $config['total_rows'] = $total;
            $this->pagination->initialize($config);
            $data['total'] = $total;
            $data['result'] = $result;
        }
        $this->load->view('admin/interaction_form', $data);
    }
    
    function add_to_watch_list($user_id){
        $ret = $this->User_model->update($user_id, array('IsAddToWatchList' => 1));
        if ($ret){
            $status = 'ok';
        }else{
            $status = 'failed';
        }
        echo json_encode(array('status' => $status));
    }
    
    function watch_list() {
        $config['base_url'] = base_url() . 'admin/user/watch_list';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $total = 0;
        $cond['IsAddToWatchList'] = 1;
        $result = $this->User_model->listUserBy($cond, $total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $total;
        $data['result'] = $result;
        $this->load->view('admin/watch_list', $data);
    }
    
    function detail_user($user_id){
        $data['row'] = $this->User_model->detail_user($user_id);
        $this->load->view('admin/_detail_user', $data);
    }
    
    function save_notes($user_id){
        if (isset($_POST['newvalue']) && $_POST['newvalue'] != ''){
            // update
            $this->User_model->update($user_id, array('Notes' => $_POST['newvalue']));
            echo $_POST['newvalue'];
        }
    }
	
	function verify_account($user_id, $status_id) {
        $data['IsEmailVerify'] = $status_id;
        $this->User_model->updateUser($data, array('UserID' => $user_id));
    }
    
    function new_user(){
        $cond = array();
       
        if (isset($_POST['btn_submit'])){
            $keyword = isset($_POST['keyword'])?$_POST['keyword']:'';
            if ($keyword != ''){
                $this->session->set_userdata('s_user_keyword', $keyword);
                $data['keyword'] = $keyword;
                $cond['keyword'] = $keyword;
            }
        }elseif ((int) $this->uri->segment(4) > 0){
            if ($this->session->userdata('s_user_keyword')){
                $keyword = $this->session->userdata('s_user_keyword');
                $data['keyword'] = $keyword;
                $cond['keyword'] = $keyword;
            }
        }else{
            $this->session->unset_userdata('s_user_keyword');
        }
        $cond['new_user_date'] = '2014-11-26'; // for testing
        
        $config['base_url'] = base_url() . 'admin/user/new_user';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $total = 0;
        $data['results'] = $this->User_model->getListNewUser($cond, $total, (int)$this->uri->segment(4), $config['per_page']);
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $total;
        $this->load->view('admin/new_user', $data);
    }
    
    function daily_match($user_id = 0) {
        $config['base_url'] = base_url() . 'admin/user/daily_match/'.$user_id;
        $config['per_page'] = 10;
        $config['uri_segment'] = 5;
        $total = 0;
        $result = $this->User_model->getListDailyMatch($user_id, $total,  (int) $this->uri->segment(5), $config['per_page']);
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $total;
        $data['result'] = $result;
        $data['current_user_id'] = $user_id;
        $this->load->view('admin/daily_match', $data);
    }
    
    function daily_match_detail($match_user_id, $current_user_id){
        $data = array();
        $row = $this->User_model->detail_user($match_user_id, $current_user_id);
        if ($row){
            $data['row'] = $row;
            $data['interest'] = $this->User_model->getListTagOfMatchUser($match_user_id,$current_user_id, 1);
            $data['icebreakers'] = $this->User_model->getListQuestionOfMatchUser($match_user_id,$current_user_id);
        }
        
        $this->load->view('admin/_daily_match_detail', $data);
    }

}

?>