<?php

class NeighborhoodPost extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('Form');
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $this->load->model('Image_model');
        $this->load->model('Neighborhood_Post_model');
    }

    function index() {

        $cond = array();
        if (isset($this->uri->vn_param['keyword'])) {
            $cond['keyword'] = $this->input->xss_clean($this->uri->vn_param['keyword']);
        }
        if (isset($this->uri->vn_param['status'])) {
            $cond['status'] = $this->uri->vn_param['status'];
        }
        if (isset($this->uri->vn_param['sort'])) {
            $cond['sort'] = $this->uri->vn_param['sort'];
        }

        //pagination configuration
        $config['base_url'] = base_url() . 'admin/neighborhoodpost/index';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;


        $config['total_rows'] = $this->Neighborhood_Post_model->getListByCondition($cond, $config['per_page'], $this->uri->segment(4), true);
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['results'] = $this->Neighborhood_Post_model->getListByCondition($cond, $config['per_page'], $this->uri->segment(4), false);

        $this->load->view('admin/neighborhoodpost_list', $data);
    }

    //use for ajax submit from Neighborhoodpost list
    //update post content by popover
    function post($id = 0, $nocache) {
        $data['ID'] = $id;
        if ($id > 0) {
            $info = $this->Neighborhood_Post_model->findById($id);
            $data['info'] = $info;
        }
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $value = array(
                'Content' => $_POST['content']
            );
            $this->Neighborhood_Post_model->update($value, array('ID' => $id));
            echo json_encode(TRUE);
            return json_encode(TRUE);
        }
        echo json_encode(FALSE);
        return json_encode(FALSE);
    }

    //just inactive the selected Neighporhood post
    function updatestatus($id = 0, $status = 0) {
        $info = $this->Neighborhood_Post_model->findById($id);
        if ($info) {
            $this->Neighborhood_Post_model->update(array('Status' => $status), array('ID' => $id));
        }
        redirect('admin/neighborhoodpost');
    }

}

?>