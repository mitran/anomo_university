<?php
    
class Weekly_Comparison extends Ext_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('Dashboard_model');
        $this->load->library("Flurry");
    }
        
    function index() {
        set_time_limit(0);
        $data = array();
        $z = date("Y-m-d", strtotime("last Saturday"));
        $z = (date('W', strtotime($z)) == date('W')) ? (strtotime($z) - 7 * 86400 + 7200) : strtotime($z);
        $lastSaturday = date("Y-m-d", $z);
        $lastSunday = date("Y-m-d", strtotime('+1 days', $z));
        $from = date("Y-m-d", strtotime('- 6 days', $z ));
        //phule
        $from1 = $from;
        $from11 = date("Y-m-d", strtotime('- 10 weeks', strtotime($from1)));
        $from10 = date("Y-m-d", strtotime('- 9 weeks', strtotime($from1)));
        $from9 = date("Y-m-d", strtotime('- 8 weeks', strtotime($from1)));
        $from8 = date("Y-m-d", strtotime('- 7 weeks', strtotime($from1)));
        $from7 = date("Y-m-d", strtotime('- 6 weeks', strtotime($from1)));
        $from6 = date("Y-m-d", strtotime('- 5 weeks', strtotime($from1)));
        $from5 = date("Y-m-d", strtotime('- 4 weeks', strtotime($from1)));
        $from4 = date("Y-m-d", strtotime('- 3 weeks', strtotime($from1)));
        $from3 = date("Y-m-d", strtotime('- 2 weeks', strtotime($from1)));
        $from2 = date("Y-m-d", strtotime('- 1 weeks', strtotime($from1)));
        
        $lastSaturday1 = $lastSaturday;
        $lastSaturday11 = date("Y-m-d", strtotime('- 10 weeks', strtotime($lastSaturday1)));
        $lastSaturday10 = date("Y-m-d", strtotime('- 9 weeks', strtotime($lastSaturday1)));
        $lastSaturday9 = date("Y-m-d", strtotime('- 8 weeks', strtotime($lastSaturday1)));
        $lastSaturday8 = date("Y-m-d", strtotime('- 7 weeks', strtotime($lastSaturday1)));
        $lastSaturday7 = date("Y-m-d", strtotime('- 6 weeks', strtotime($lastSaturday1)));
        $lastSaturday6 = date("Y-m-d", strtotime('- 5 weeks', strtotime($lastSaturday1)));
        $lastSaturday5 = date("Y-m-d", strtotime('- 4 weeks', strtotime($lastSaturday1)));
        $lastSaturday4 = date("Y-m-d", strtotime('- 3 weeks', strtotime($lastSaturday1)));
        $lastSaturday3 = date("Y-m-d", strtotime('- 2 weeks', strtotime($lastSaturday1)));
        $lastSaturday2 = date("Y-m-d", strtotime('- 1 weeks', strtotime($lastSaturday1)));
        //end phule

        $data['register_last'] = $this->Dashboard_model->totalUserRegister($from, $lastSaturday);
        $data['register_last2'] = $this->Dashboard_model->totalUserRegister($from2, $lastSaturday2);
        $data['register_last3'] = $this->Dashboard_model->totalUserRegister($from3, $lastSaturday3);
        $data['register_last4'] = $this->Dashboard_model->totalUserRegister($from4, $lastSaturday4);
        $data['register_last5'] = $this->Dashboard_model->totalUserRegister($from5, $lastSaturday5);
        $data['register_last6'] = $this->Dashboard_model->totalUserRegister($from6, $lastSaturday6);
        $data['register_last7'] = $this->Dashboard_model->totalUserRegister($from7, $lastSaturday7);
        $data['register_last8'] = $this->Dashboard_model->totalUserRegister($from8, $lastSaturday8);
        $data['register_last9'] = $this->Dashboard_model->totalUserRegister($from9, $lastSaturday9);
        $data['register_last10'] = $this->Dashboard_model->totalUserRegister($from10, $lastSaturday10);
        $data['register_last11'] = $this->Dashboard_model->totalUserRegister($from11, $lastSaturday11);
        
        $data['register_current'] = $this->Dashboard_model->totalUserRegister($lastSunday);
        $data['register_compare'] = self::calPercentage($data['register_current'], $data['register_last']);
        
        
        // DAU - ANOMO-6802
        $now = date('Y-m-d');
        $dauIos = $this->flurry->getTotalFlurryActiveUser('iso', 'day', $from11, $now, 'array', 'ActiveUsersByWeek', 'weeks') ;
     
    	sleep(1); // hmmmmmm
    	$dauAndroid = $this->flurry->getTotalFlurryActiveUser('android', 'day', $from11, $now, 'array', 'ActiveUsersByWeek', 'weeks');
    	sleep(1); // hmmmmmm
       
        for($w = 0; $w < 12; $w ++){
            $f = ($w==0)?$lastSunday:${'from'.$w};
            $date = date('Y-m-d', strtotime("+1 day", strtotime($f)));
            $dau1 = isset($dauIos[$date])?$dauIos[$date]:0;
	    $dau2 = isset($dauAndroid[$date])?$dauAndroid[$date]:0;
	    $dau = $dau1 + $dau2;
            $data['dau_'.$w] = $dau1 + $dau2;
        }
       
        $data['dau_last'] = $data['dau_1'];
        $data['dau_last2'] = $data['dau_2'];
        $data['dau_last3'] = $data['dau_3'];
        $data['dau_last4'] = $data['dau_4'];
        $data['dau_last5'] = $data['dau_5'];
        $data['dau_last6'] = $data['dau_6'];
        $data['dau_last7'] = $data['dau_7'];
        $data['dau_last8'] = $data['dau_8'];
        $data['dau_last9'] = $data['dau_9'];
        $data['dau_last10'] = $data['dau_10'];
        $data['dau_last11'] = $data['dau_11'];
        $data['dau_current'] = $data['dau_0'];
        

        $data['verified_last'] = $this->Dashboard_model->totalUserRegister($from, $lastSaturday, 'verified');
        $data['verified_last2'] = $this->Dashboard_model->totalUserRegister($from2, $lastSaturday2, 'verified');
        $data['verified_last3'] = $this->Dashboard_model->totalUserRegister($from3, $lastSaturday3, 'verified');
        $data['verified_last4'] = $this->Dashboard_model->totalUserRegister($from4, $lastSaturday4, 'verified');
        $data['verified_last5'] = $this->Dashboard_model->totalUserRegister($from5, $lastSaturday5, 'verified');
        $data['verified_last6'] = $this->Dashboard_model->totalUserRegister($from6, $lastSaturday6, 'verified');
        $data['verified_last7'] = $this->Dashboard_model->totalUserRegister($from7, $lastSaturday7, 'verified');
        $data['verified_last8'] = $this->Dashboard_model->totalUserRegister($from8, $lastSaturday8, 'verified');
        $data['verified_last9'] = $this->Dashboard_model->totalUserRegister($from9, $lastSaturday9, 'verified');
        $data['verified_last10'] = $this->Dashboard_model->totalUserRegister($from10, $lastSaturday10, 'verified');
        $data['verified_last11'] = $this->Dashboard_model->totalUserRegister($from11, $lastSaturday11, 'verified');
        
        $data['verified_current'] = $this->Dashboard_model->totalUserRegister($lastSunday,'', 'verified');
        $data['verified_compare'] = self::calPercentage($data['verified_current'], $data['verified_last']);
            
        $data['unverified_last'] = $this->Dashboard_model->totalUserRegister($from, $lastSaturday, 'unverified');
        $data['unverified_last2'] = $this->Dashboard_model->totalUserRegister($from2, $lastSaturday2, 'unverified');
        $data['unverified_last3'] = $this->Dashboard_model->totalUserRegister($from3, $lastSaturday3, 'unverified');
        $data['unverified_last4'] = $this->Dashboard_model->totalUserRegister($from4, $lastSaturday4, 'unverified');
        $data['unverified_last5'] = $this->Dashboard_model->totalUserRegister($from5, $lastSaturday5, 'unverified');
        $data['unverified_last6'] = $this->Dashboard_model->totalUserRegister($from6, $lastSaturday6, 'unverified');
        $data['unverified_last7'] = $this->Dashboard_model->totalUserRegister($from7, $lastSaturday7, 'unverified');
        $data['unverified_last8'] = $this->Dashboard_model->totalUserRegister($from8, $lastSaturday8, 'unverified');
        $data['unverified_last9'] = $this->Dashboard_model->totalUserRegister($from9, $lastSaturday9, 'unverified');
        $data['unverified_last10'] = $this->Dashboard_model->totalUserRegister($from10, $lastSaturday10, 'unverified');
        $data['unverified_last11'] = $this->Dashboard_model->totalUserRegister($from11, $lastSaturday11, 'unverified');
        
        $data['unverified_current'] = $this->Dashboard_model->totalUserRegister($lastSunday, '', 'unverified');
        $data['unverified_compare'] = self::calPercentage($data['unverified_current'], $data['unverified_last']);
            
       //  $data['devices'] = $this->Dashboard_model->totalByDevices($from, $to, 'all');
        $data['ios_last'] = $this->Dashboard_model->totalByDevices($from, $lastSaturday, 'ios');
        $data['ios_last2'] = $this->Dashboard_model->totalByDevices($from2, $lastSaturday2, 'ios');
        $data['ios_last3'] = $this->Dashboard_model->totalByDevices($from3, $lastSaturday3, 'ios');
        $data['ios_last4'] = $this->Dashboard_model->totalByDevices($from4, $lastSaturday4, 'ios');
        $data['ios_last5'] = $this->Dashboard_model->totalByDevices($from5, $lastSaturday5, 'ios');
        $data['ios_last6'] = $this->Dashboard_model->totalByDevices($from6, $lastSaturday6, 'ios');
        $data['ios_last7'] = $this->Dashboard_model->totalByDevices($from7, $lastSaturday7, 'ios');
        $data['ios_last8'] = $this->Dashboard_model->totalByDevices($from8, $lastSaturday8, 'ios');
        $data['ios_last9'] = $this->Dashboard_model->totalByDevices($from9, $lastSaturday9, 'ios');
        $data['ios_last10'] = $this->Dashboard_model->totalByDevices($from10, $lastSaturday10, 'ios');
        $data['ios_last11'] = $this->Dashboard_model->totalByDevices($from11, $lastSaturday11, 'ios');
        
        $data['ios_current'] = $this->Dashboard_model->totalByDevices($lastSunday, '', 'ios');
        $data['ios_compare'] = self::calPercentage($data['ios_current'], $data['ios_last']);
            
        $data['android_last'] = $this->Dashboard_model->totalByDevices($from, $lastSaturday, 'android');
        $data['android_last2'] = $this->Dashboard_model->totalByDevices($from2, $lastSaturday2, 'android');
        $data['android_last3'] = $this->Dashboard_model->totalByDevices($from3, $lastSaturday3, 'android');
        $data['android_last4'] = $this->Dashboard_model->totalByDevices($from4, $lastSaturday4, 'android');
        $data['android_last5'] = $this->Dashboard_model->totalByDevices($from5, $lastSaturday5, 'android');
        $data['android_last6'] = $this->Dashboard_model->totalByDevices($from6, $lastSaturday6, 'android');
        $data['android_last7'] = $this->Dashboard_model->totalByDevices($from7, $lastSaturday7, 'android');
        $data['android_last8'] = $this->Dashboard_model->totalByDevices($from8, $lastSaturday8, 'android');
        $data['android_last9'] = $this->Dashboard_model->totalByDevices($from9, $lastSaturday9, 'android');
        $data['android_last10'] = $this->Dashboard_model->totalByDevices($from10, $lastSaturday10, 'android');
        $data['android_last11'] = $this->Dashboard_model->totalByDevices($from11, $lastSaturday11, 'android');
        
        $data['android_current'] = $this->Dashboard_model->totalByDevices($lastSunday, '', 'android');
        $data['android_compare'] = self::calPercentage($data['android_current'], $data['android_last']);
            
        $data['game_last'] = $this->Dashboard_model->totalOther($from, $lastSaturday, 'Anomotion');
        $data['game_last2'] = $this->Dashboard_model->totalOther($from2, $lastSaturday2, 'Anomotion');
        $data['game_last3'] = $this->Dashboard_model->totalOther($from3, $lastSaturday3, 'Anomotion');
        $data['game_last4'] = $this->Dashboard_model->totalOther($from4, $lastSaturday4, 'Anomotion');
        $data['game_last5'] = $this->Dashboard_model->totalOther($from5, $lastSaturday5, 'Anomotion');
        $data['game_last6'] = $this->Dashboard_model->totalOther($from6, $lastSaturday6, 'Anomotion');
        $data['game_last7'] = $this->Dashboard_model->totalOther($from7, $lastSaturday7, 'Anomotion');
        $data['game_last8'] = $this->Dashboard_model->totalOther($from8, $lastSaturday8, 'Anomotion');
        $data['game_last9'] = $this->Dashboard_model->totalOther($from9, $lastSaturday9, 'Anomotion');
        $data['game_last10'] = $this->Dashboard_model->totalOther($from10, $lastSaturday10, 'Anomotion');
        $data['game_last11'] = $this->Dashboard_model->totalOther($from11, $lastSaturday11, 'Anomotion');
        $data['game_current'] = $this->Dashboard_model->totalOther($lastSunday, '', 'Anomotion');
        $data['game_compare'] = self::calPercentage($data['game_current'], $data['game_last']);

            
        $data['like_last'] = $this->Dashboard_model->totalOther($from, $lastSaturday, 'ActivityLike');
        $data['like_last2'] = $this->Dashboard_model->totalOther($from2, $lastSaturday2, 'ActivityLike');
        $data['like_last3'] = $this->Dashboard_model->totalOther($from3, $lastSaturday3, 'ActivityLike');
        $data['like_last4'] = $this->Dashboard_model->totalOther($from4, $lastSaturday4, 'ActivityLike');
        $data['like_last5'] = $this->Dashboard_model->totalOther($from5, $lastSaturday5, 'ActivityLike');
        $data['like_last6'] = $this->Dashboard_model->totalOther($from6, $lastSaturday6, 'ActivityLike');
        $data['like_last7'] = $this->Dashboard_model->totalOther($from7, $lastSaturday7, 'ActivityLike');
        $data['like_last8'] = $this->Dashboard_model->totalOther($from8, $lastSaturday8, 'ActivityLike');
        $data['like_last9'] = $this->Dashboard_model->totalOther($from9, $lastSaturday9, 'ActivityLike');
        $data['like_last10'] = $this->Dashboard_model->totalOther($from10, $lastSaturday10, 'ActivityLike');
        $data['like_last11'] = $this->Dashboard_model->totalOther($from11, $lastSaturday11, 'ActivityLike');
        
        $data['like_current'] = $this->Dashboard_model->totalOther($lastSunday, '', 'ActivityLike');
        $data['like_compare'] = self::calPercentage($data['like_current'], $data['like_last']);
            
        $data['comment_last'] = $this->Dashboard_model->totalOther($from, $lastSaturday, 'ActivityComment');
                                
        $data['comment_last2'] = $this->Dashboard_model->totalOther($from2, $lastSaturday2, 'ActivityComment');
                                
        $data['comment_last3'] = $this->Dashboard_model->totalOther($from3, $lastSaturday3, 'ActivityComment');
                                
        $data['comment_last4'] = $this->Dashboard_model->totalOther($from4, $lastSaturday4, 'ActivityComment');
                                
        $data['comment_last5'] = $this->Dashboard_model->totalOther($from5, $lastSaturday5, 'ActivityComment');
                                
        $data['comment_last6'] = $this->Dashboard_model->totalOther($from6, $lastSaturday6, 'ActivityComment');
                                
        $data['comment_last7'] = $this->Dashboard_model->totalOther($from7, $lastSaturday7, 'ActivityComment');
       
        $data['comment_last8'] = $this->Dashboard_model->totalOther($from8, $lastSaturday8, 'ActivityComment');
                                
        $data['comment_last9'] = $this->Dashboard_model->totalOther($from9, $lastSaturday9, 'ActivityComment');
                                
        $data['comment_last10'] = $this->Dashboard_model->totalOther($from10, $lastSaturday10, 'ActivityComment');
                                
        $data['comment_last11'] = $this->Dashboard_model->totalOther($from11, $lastSaturday11, 'ActivityComment');
                                
        $data['comment_current'] = $this->Dashboard_model->totalOther($lastSunday,'', 'ActivityComment');
        $data['comment_compare'] = self::calPercentage($data['comment_current'], $data['comment_last']);
           
            
        $data['gift_last'] = $this->Dashboard_model->totalOther($from, $lastSaturday, 'GiftSend');
        $data['gift_last2'] = $this->Dashboard_model->totalOther($from2, $lastSaturday2, 'GiftSend');
        $data['gift_last3'] = $this->Dashboard_model->totalOther($from3, $lastSaturday3, 'GiftSend');
        $data['gift_last4'] = $this->Dashboard_model->totalOther($from4, $lastSaturday4, 'GiftSend');
        $data['gift_last5'] = $this->Dashboard_model->totalOther($from5, $lastSaturday5, 'GiftSend');
        $data['gift_last6'] = $this->Dashboard_model->totalOther($from6, $lastSaturday6, 'GiftSend');
        $data['gift_last7'] = $this->Dashboard_model->totalOther($from7, $lastSaturday7, 'GiftSend');
        $data['gift_last8'] = $this->Dashboard_model->totalOther($from8, $lastSaturday8, 'GiftSend');
        $data['gift_last9'] = $this->Dashboard_model->totalOther($from9, $lastSaturday9, 'GiftSend');
        $data['gift_last10'] = $this->Dashboard_model->totalOther($from10, $lastSaturday10, 'GiftSend');
        $data['gift_last11'] = $this->Dashboard_model->totalOther($from11, $lastSaturday11, 'GiftSend');
        
        $data['gift_current'] = $this->Dashboard_model->totalOther($lastSunday, '', 'GiftSend');
        $data['gift_compare'] = self::calPercentage($data['gift_current'], $data['gift_last']);
            
        $data['picture_last'] = $this->Dashboard_model->totalOther($from, $lastSaturday, 'PicturePostActivity');
        $data['picture_last2'] = $this->Dashboard_model->totalOther($from2, $lastSaturday2, 'PicturePostActivity');
        $data['picture_last3'] = $this->Dashboard_model->totalOther($from3, $lastSaturday3, 'PicturePostActivity');
        $data['picture_last4'] = $this->Dashboard_model->totalOther($from4, $lastSaturday4, 'PicturePostActivity');
        $data['picture_last5'] = $this->Dashboard_model->totalOther($from5, $lastSaturday5, 'PicturePostActivity');
        $data['picture_last6'] = $this->Dashboard_model->totalOther($from6, $lastSaturday6, 'PicturePostActivity');
        $data['picture_last7'] = $this->Dashboard_model->totalOther($from7, $lastSaturday7, 'PicturePostActivity');
        $data['picture_last8'] = $this->Dashboard_model->totalOther($from8, $lastSaturday8, 'PicturePostActivity');
        $data['picture_last9'] = $this->Dashboard_model->totalOther($from9, $lastSaturday9, 'PicturePostActivity');
        $data['picture_last10'] = $this->Dashboard_model->totalOther($from10, $lastSaturday10, 'PicturePostActivity');
        $data['picture_last11'] = $this->Dashboard_model->totalOther($from11, $lastSaturday11, 'PicturePostActivity');
        
        $data['picture_current'] = $this->Dashboard_model->totalOther($lastSunday, '', 'PicturePostActivity');
        $data['picture_compare'] = self::calPercentage($data['picture_current'], $data['picture_last']);
            
        $data['activity_last'] = $this->Dashboard_model->totalOther($from, $lastSaturday, 'Activity');
        $data['activity_last2'] = $this->Dashboard_model->totalOther($from2, $lastSaturday2, 'Activity');
        $data['activity_last3'] = $this->Dashboard_model->totalOther($from3, $lastSaturday3, 'Activity');
        $data['activity_last4'] = $this->Dashboard_model->totalOther($from4, $lastSaturday4, 'Activity');
        $data['activity_last5'] = $this->Dashboard_model->totalOther($from5, $lastSaturday5, 'Activity');
        $data['activity_last6'] = $this->Dashboard_model->totalOther($from6, $lastSaturday6, 'Activity');
        $data['activity_last7'] = $this->Dashboard_model->totalOther($from7, $lastSaturday7, 'Activity');
        $data['activity_last8'] = $this->Dashboard_model->totalOther($from8, $lastSaturday8, 'Activity');
        $data['activity_last9'] = $this->Dashboard_model->totalOther($from9, $lastSaturday9, 'Activity');
        $data['activity_last10'] = $this->Dashboard_model->totalOther($from10, $lastSaturday10, 'Activity');
        $data['activity_last11'] = $this->Dashboard_model->totalOther($from11, $lastSaturday11, 'Activity');
        
        $data['activity_current'] = $this->Dashboard_model->totalOther($lastSunday, '', 'Activity');
        $data['activity_compare'] = self::calPercentage($data['activity_current'], $data['activity_last']);
            
        $this->load->view('admin/weekly_comparison', $data);
    }
        
    function calPercentage($num, $total) {
      $total = (0 == $total) ? 1 : $total;
      return  $this->commonlib->formatBigNumber($num/$total*100).' %';
    }
        
}
    
?>