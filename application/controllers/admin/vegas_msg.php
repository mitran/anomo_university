<?php
class Vegas_Msg extends Ext_Controller{
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Vegas_Msg_model');
        $this->load->model('User_model');
        $this->load->library('S3Upload');
    }
    
    function send(){
        $aData = array();
        if (isset($_POST['requestsubmit'])) {
            if (!isset($_POST['message']) && !isset($_FILES['photo']['name'])) {
                $this->load->view('admin/vegas_msg', $aData);
            } else {
                //If pass validate, send the message
                $sMessage = $this->input->post('message');
                $iGender = $this->input->post('gender');
                $sApp_used = $this->input->post('app_used');
                $sAge = $this->input->post('age');
                $sTapAction = $this->input->post('tap_action');
                $sTapParam = $this->input->post('tap_param');

                $from = ($this->input->post('from') != '') ? $this->input->post('from') : '';
                $to = ($this->input->post('to') != '') ? $this->input->post('to') : '';

                if ($from != '' && $to != '') {
                    $fromDate = new DateTime($this->input->post('from'));
                    $toDate = new DateTime($this->input->post('to'));

                    $fromDate = date_sub($fromDate, date_interval_create_from_date_string('7 hours'));
                    $toDate = date_sub($toDate, date_interval_create_from_date_string('7 hours'));
                    $toDate = date_add($toDate, date_interval_create_from_date_string('1 day'));

                    $from = $fromDate->format('Y-m-d H:i:s');
                    $to = $toDate->format('Y-m-d H:i:s');
                }
                $photo = '';
                if ($_FILES['photo']['name'] != ''){
                    $isUpload = $this->s3upload->upload('photo');
                    if ($isUpload) {
                        $photo = $isUpload;
                    }
                }
                
                if ($sMessage || $photo) {
                    //Insert to table message
                    $aData = array(
                        'Message' => $sMessage,
                        'Photo' => $photo,
                        'TapAction' => $sTapAction,
                        'TapParam'  => $sTapParam,
                        'Status' => 0,
                        'CreatedDate' => gmdate('Y-m-d H:i:s')
                    );
                    $iID = $this->Vegas_Msg_model->addVegasMsg($aData);
                    if ($iID) {
                        //Get list user id
                        $aCond = array(
                            'gender' => $iGender,
                            'app_used' => $sApp_used,
                            'from_age'   => isset($_POST['from_age'])?$_POST['from_age']:0,
                            'to_age'   => isset($_POST['to_age'])?$_POST['to_age']:0,
                            'from' => $from,
                            'to' => $to,
                            'lat'   => isset($_POST['lat'])?$_POST['lat']:'',
                            'lon'   => isset($_POST['lon'])?$_POST['lon']:'',
                            'radius'    => isset($_POST['radius'])?$_POST['radius']:''
                        );
                        $iCount = 0;
                        $oUser = $this->User_model->getListUserID($aCond);
                        if ($oUser) {
                            $iCount = count($oUser);
                            $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
                            $sMessage =  mysqli_real_escape_string($con, $sMessage);
                            foreach ($oUser as $obj) {
                                $createdDate = gmdate('Y-m-d H:i:s');
                                $aDataTracking [] = "($iID, $obj->UserID, '$sMessage', '$photo', '$sTapAction', '$sTapParam', 0, '$createdDate')";
                            }
                            $this->Vegas_Msg_model->addMultiMsgTracking($aDataTracking);
                        }

                        //Update total message tracking to message
                        $this->Vegas_Msg_model->updateVegagMsg(array('TotalTracking' => $iCount), array('MessageID' => $iID));

                        $sMsg = "Send Successful";
                    } else {
                        $sMsg = "Send Fail";
                    }
                } else {
                    $sMsg = "Can't get message to send";
                }
                $this->session->set_flashdata('msg', $sMsg);

                redirect('admin/vegas_msg/send');
            }
        } else {
            $this->load->view('admin/vegas_msg', $aData);
        }
    }
    
    function listmsg() {
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $cond = array();
        $config['base_url'] = base_url() . 'admin/vegas_msg/listmsg';
        $config['per_page'] = 50;
        $config['uri_segment'] = 4;
        $total = 0;
        $results = $this->Vegas_Msg_model->getListByCondition($cond, $total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $total;
        $data['results'] = $results;
        $this->load->view('admin/vegas_msg_list', $data);
    }
}
?>
