<?php

class Promos extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Promos_model');
        $this->load->model('Activity_model');
        $this->load->model('Notification_History_model');
        $this->load->model('Cron_Push_Notification_model');
        $this->load->model('Stop_Receive_Notify_model');
        $this->load->library('form_validation');
        $this->load->library('S3Upload');        
        $this->lang->load('ws');
    }

    function index() {
        $data['vendors'] = array('' => 'Please select');
        $data['vendors'] += $this->Promos_model->getListVendor();
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $cond = array();
        if (isset($this->uri->vn_param['vendor'])) {
            $cond['vendor'] = $this->uri->vn_param['vendor'];
        }
        $config['base_url'] = base_url() . 'admin/promos/index';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $total = 0;
        $result = $this->Promos_model->getListPromosWithPaging($cond, $total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['results'] = $result;
        $this->load->view('admin/promos_list', $data);
    }
    
    function post($id = 0) {
        $data['vendors'] = array('' => 'Please select');
        $data['vendors'] += $this->Promos_model->getListVendor();
        $data['id'] = $id;
        if ($id > 0) {
            $info = $this->Promos_model->getInfo($id);
            $data['info'] = $info;
        }
        if (isset($_POST['submit'])) {

            $this->form_validation->set_rules('content', 'Content', "trim|required|xss_clean");
            $this->form_validation->set_rules('user_id', 'Vendor', 'trim|required|numeric');
            $this->form_validation->set_rules('link', 'Link', 'trim|required');
            $photo = '';
            $upload['error'] = false;
            if ($id == 0 || ($id > 0 && $_FILES['photo']['name'] != '')) {
                $isUpload = $this->s3upload->upload('photo');
                if (!$isUpload) {
                    $upload['error_msg'] = 'Error upload image';
                } else {
                    $photo = $isUpload;
                }
            }
            
            if ($this->form_validation->run() == FALSE || $upload['error']) {
                if ($upload['error']) {
                    $data['upload_error'] = $upload['error_msg'];
                }
                $this->load->view('admin/promos_form', $data);
            } else {
                if ($id > 0 && empty($photo)) {
                    $photo = $info->Photo;
                }
                $value = array(
                    'Content' => $_POST['content'],
                    'Photo' => $photo,
                    'Link' => $_POST['link'],
                    'UserID' => $_POST['user_id'],
                    'IsPublished'   => isset($_POST['is_published'])?1:0
                );
                if ($id == 0) {
                    $value['CreatedDate'] = gmdate('Y-m-d H:i:s');
                    $this->Promos_model->add($value);
                } else {
                    $this->Promos_model->update($value, array('ID' => $id));
                }
                redirect('admin/promos');
            }
        } else {
            $this->load->view('admin/promos_form', $data);
        }
    }
    
    function delete($id){
        $this->Promos_model->delete($id);
        $msg = 'Delete successful';
        redirect('admin/promos/index/p_delete/' . $msg);
    }
    
    function comment($promo_id = 0){
        $info = $this->Promos_model->getDetailPromoPost($promo_id);
        if ($info){
            $data['info'] = $info;
            $data['list_comment'] = $this->Activity_model->getListComment($promo_id, 10, $info->UserID);  
        }
        $this->load->view('admin/list_comment', $data);
    }
    
    function post_comment($content_id = 0) {
        $type = 10;

        $content = isset($_POST['txt_comment']) ? trim($_POST['txt_comment']) : '';
        if ($content != '') {
            $contentInfo = $this->Activity_model->getContentInfo($content_id, $type);
            if ($contentInfo) {
                $user_id = $contentInfo->UserID;
                $comment_id = $this->Activity_model->comment($user_id, $content_id, $type, $content);
                if ($comment_id) {
                    $this->Activity_model->increase($content_id, $type, 'comment');
                    $cronPushData = array();
                    $mentionUserIds = array();
                    $exclude = $user_id;

                    $listUser = $this->Activity_model->getListUserIDIsCommented($content_id, $type, $exclude, $mentionUserIds, $user_id, $user_id);

                    if ($listUser) {
                        foreach ($listUser as $re) {
                            $pushData = array(
                                'UserID' => $re->UserID,
                                'SendUserID' => $user_id,
                                'Type' => HISTORY_COMMENT_ACTIVITY, // 14 - comment feed
                                'RefID' => $comment_id,
                                'ContentID' => $content_id,
                                'ContentType' => $type,
                                'IsAnonymous' => 0,
                                'IsRead' => 1,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                            );
                            $history_id = $this->Notification_History_model->add($pushData);
                            // notify

                            if ($history_id) {
                                $owner =  $contentInfo->UserName;
                                $msgPushNotify = str_replace('%owner%', $owner, $this->lang->line('commented_owner_post'));
                                $msgPushNotify = str_replace('%name%', $owner, $msgPushNotify);
                                $cronPushData[] = array(
                                    'UserID' => $re->UserID,
                                    'FromUserID' => $user_id,
                                    'Message' => $msgPushNotify,
                                    'ContentID' => $content_id,
                                    'Type' => $type,
                                    'ActionType' => NOTIFY_COMMENT_ACTIVITY, // 7
                                    'Status' => 0,
                                    'Total' => 0,
                                    'HistoryID' => $history_id,
                                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                                );
                            }
                        }
                    }

                    // 
                    if (sizeof($cronPushData) > 0) {
                        $this->Cron_Push_Notification_model->insertMulti($cronPushData);
                    }
                }
            }
        }
        redirect('admin/promos/comment/'.$content_id);
    }
    
    function fanpage(){
        $data['vendors'] = array('' => 'Please select');
        $data['vendors'] += $this->Promos_model->getListVendor();
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $cond = array();
        if (isset($this->uri->vn_param['vendor'])) {
            $cond['vendor'] = $this->uri->vn_param['vendor'];
        }
        $config['base_url'] = base_url() . 'admin/promos/fanpage';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $total = 0;
        
        $result = $this->Activity_model->getListFanPostOfVendor($cond,$total,(int)$this->uri->segment(4),$config['per_page']);
        
        //var_dump($result);
        /*
        foreach ($result as $act) {
            $numLike = 0;
            $numComment = 0;
            $isComment = 0;
            if (in_array($act->Type, $validTypes)) {
                $filter_type = 0;

                $contentInfo = $this->Activity_model->getActivitiesFeedInfo($act->RefID, $act->Type, $tokenInfo->UserID, $filter_type, $act->FromUserID);
                if ($contentInfo) {
                    $numLike = $contentInfo->Like;
                    $numComment = $contentInfo->Comment;
                    $isComment = $contentInfo->IsComment;

                    $act->IsLike = $contentInfo->IsLike;
                    $act->IsBlock = $contentInfo->IsBlock;
                    $act->IsBlockTarget = $contentInfo->IsBlock;
                    $act->IsReported = $contentInfo->IsBlockTarget;
                    $act->IsStopReceiveNotify = $contentInfo->IsStopReceiveNotify;
                    $act->IsFavorite = $contentInfo->IsFavorite;
                }
            }
            $act->IsComment = $isComment;
            $act->Comment = $numComment;
            $act->Like = $numLike;
        }    
        */    
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['results'] = $result;
        
        $this->load->view('admin/promos_fanpage_list', $data);
    }
    
    
    function fanpage_comment($ActivityID = 0){
        $info = $this->Activity_model->getFanPostToVendorInfo($ActivityID);
        if ($info){            
            $data['info'] = $info;
            $data['list_comment'] = $this->Activity_model->getListComment($info->RefID, $info->Type, $info->FromUserID);  
        }
        $this->load->view('admin/promos_fanpage_comment', $data);
    }
    
    function post_fanpage_comment($ActivityID = 0) {        

        $content = isset($_POST['txt_comment']) ? trim($_POST['txt_comment']) : '';
        if ($content != '') {
            $contentInfo = $this->Activity_model->getFanPostToVendorInfo($ActivityID);            
            if ($contentInfo) {
                $content_id = $contentInfo->RefID;
                $type = $contentInfo->Type;
                $vendor = $contentInfo->FanPage;
                $comment_id = $this->Activity_model->comment($vendor, $content_id, $type, $content);
                if ($comment_id) {
                    $this->Activity_model->increase($content_id, $type, 'comment');
                    
                    //push notification to owner
                    $cronPushData = array();
                    $exclude = $vendor;                                          
                    $isTopReceiveNotify = $this->Stop_Receive_Notify_model->getInfo($contentInfo->FromUserID, $content_id, $type);                    
                    if ($vendor != $contentInfo->FromUserID && empty($isTopReceiveNotify)) {                      
                        $exclude .= "," . $contentInfo->FromUserID;
                        $pushData = array(
                            'UserID' => $contentInfo->FromUserID,
                            'SendUserID' => $vendor,
                            'Type' => HISTORY_COMMENT_ACTIVITY, // 14 - comment feed
                            'RefID' => $comment_id,
                            'ContentType' => $type,
                            'ContentID' => $content_id,
                            'IsAnonymous'   => 0,
                            'IsRead' => 1,
                            'CreatedDate' => gmdate('Y-m-d H:i:s')
                        );
                        $history_id = $this->Notification_History_model->add($pushData);

                        // notify
                        if ($history_id ) {                       
                            $msgPushNotify =  $this->lang->line('commented_post');
                            $msgPushNotify = str_replace('%name%', $contentInfo->VendorName, $msgPushNotify);
                            $cronPushData[] = array(
                                'UserID'    => $contentInfo->FromUserID,                    
                                'FromUserID'  => $vendor,
                                'Message'   => $msgPushNotify,
                                'ContentID' => $content_id,
                                'Type'      => $type,
                                'ActionType'    => NOTIFY_COMMENT_ACTIVITY, // 7
                                'Status' => 0,
                                'Total' => 0,
                                'HistoryID' => $history_id,
                                'CreatedDate'   => gmdate('Y-m-d H:i:s')
                            );
                        }
                    }                    
                    $mentionUserIds = array();                                 
                    $listUser = $this->Activity_model->getListUserIDIsCommented($content_id, $type, $exclude, $mentionUserIds, $vendor, $contentInfo->FromUserID);   
                                    
                    if ($listUser) {
                        foreach ($listUser as $re) {
                            $pushData = array(
                                'UserID' => $re->UserID,
                                'SendUserID' => $vendor,
                                'Type' => HISTORY_COMMENT_ACTIVITY, // 14- comment feed
                                'RefID' => $comment_id,
                                'ContentID' => $content_id,
                                'ContentType' => $type,
                                'IsAnonymous' => 0,
                                'IsRead' => 1,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                            );
                            $history_id = $this->Notification_History_model->add($pushData);
                            // notify

                            if ($history_id) {
                                $owner =  $contentInfo->FromUserName;                                
                                $msgPushNotify = str_replace('%owner%', $owner, $this->lang->line('commented_owner_post'));
                                $msgPushNotify = str_replace('%name%', $contentInfo->VendorName, $msgPushNotify);
                                $cronPushData[] = array(
                                    'UserID' => $re->UserID,
                                    'FromUserID' => $vendor,
                                    'Message' => $msgPushNotify,
                                    'ContentID' => $content_id,
                                    'Type' => $type,
                                    'ActionType' => NOTIFY_COMMENT_ACTIVITY, // 7
                                    'Status' => 0,
                                    'Total' => 0,
                                    'HistoryID' => $history_id,
                                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                                );
                            }
                        }
                    }

                    // 
                    if (sizeof($cronPushData) > 0) {
                        $this->Cron_Push_Notification_model->insertMulti($cronPushData);
                    }
                }
            }
        }
        redirect('admin/promos/fanpage_comment/'.$ActivityID);
    }
    
    function fanpage_delete($ActivitiID = 0){
        if($ActivitiID > 0){
            $info = $this->Activity_model->getFanPostToVendorInfo($ActivitiID);
            if($info){
                if($info && !empty($info->FanPage) && $info->IsInvalid == 0){
                    // just deactive this post
                    $ret = $this->Activity_model->deleteActivity($info->RefID, $info->Type, $info->FromUserID);
                    if (!$ret){
                        $response->code = $this->lang->line('failed');
                    }else{
                        $response->code = $this->lang->line('ok');
                    }
                }else{
                    $response->code = $this->lang->line('invalid_request');    
                }
            }
            
        }
        $msg = 'Delete successful';
        redirect('admin/promos/fanpage/p_delete/' . $msg);
    }

}

?>
