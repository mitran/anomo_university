<?php

class reportban_user extends Ext_Controller {

    function __construct() {
        parent::Ext_Controller();
        $this->load->model('user_model');
        $this->load->model('reported_user_model');
        $this->load->model('reported_content_model');
        $this->load->model('activity_model');
        $this->load->model('Comment_model');
        $this->load->model('chat_model');
        $this->load->helper('form');
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $this->load->model('Image_model');
        $this->load->model('wstoken_model');
        $this->load->model('Dashboard_model');
        $this->load->model('picture_post_activity_model');
        $this->load->model('user_profile_status_model');
    }

    function index() {
        $cond = array();
        if (isset($this->uri->vn_param['flag'])) {
            $cond['flag'] = $this->uri->vn_param['flag'];
        }
        if (isset($this->uri->vn_param['status'])) {
            $cond['status'] = $this->uri->vn_param['status'];
        }

        $config['base_url'] = base_url() . 'admin/reportban_user/index';
        $config['per_page'] = 50;
        $config['uri_segment'] = 4;
        $total = 0;
        $this->pagination->initialize($config);
        if (isset($this->uri->vn_param['contentid'])) {
            $cond['ContentID'] = $this->uri->vn_param['contentid'];
            $data['total'] = $config['total_rows'];
            $data['results'] = $this->reported_content_model->getListByCondition($cond, $total, $config['per_page'], (int) $this->uri->segment(4));
            $config['total_rows'] = $total;
            $this->load->view('admin/reported_content', $data);
        }
        if (isset($this->uri->vn_param['touserid'])) {
            $cond['ToUserID'] = $this->uri->vn_param['touserid'];
            $data['total'] = $config['total_rows'];
            $data['results'] = $this->reported_user_model->getListByCondition($cond, $total, $config['per_page'], (int) $this->uri->segment(4));
            $config['total_rows'] = $total;
            $this->load->view('admin/reported_user', $data);
        }
    }

    function reported_content() {
        $types = array(
            0 => 'Check in',
            1 => 'User change status',
            2 => 'Post picture to place',
            7 => 'Post picture to nbh',
            27 => 'Post picture status',
            6 => 'Post text to place',
            8 => 'Post text to neighborhood'
        );
        $cond = array();
        if (isset($this->uri->vn_param['flag'])) {
            $cond['flag'] = $this->uri->vn_param['flag'];
        }
        if (isset($this->uri->vn_param['contentid'])) {
            $cond['ContentID'] = $this->uri->vn_param['contentid'];
        }

        $config['base_url'] = base_url() . 'admin/reportban_user/reported_content';
        $config['per_page'] = 100;
        $config['uri_segment'] = 4;
        $total = 0;

        $results = $this->reported_content_model->getsql($total, $config['per_page'], (int) $this->uri->segment(4));

        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $total;
        $data['results'] = $results;
        $data['types'] = $types;
        $data['current_offset'] = (int) $this->uri->segment(4);
        $this->load->view('admin/reported_content', $data);
    }

    function reported_user() {

        set_time_limit(0);
        $cond = array();
        if (isset($this->uri->vn_param['flag'])) {
            $cond['flag'] = $this->uri->vn_param['flag'];
        }
        if (isset($this->uri->vn_param['touserid'])) {
            $cond['ToUserID'] = $this->uri->vn_param['touserid'];
        }
        $config['base_url'] = base_url() . 'admin/reportban_user/reported_user';
        $config['per_page'] = 100;
        $config['uri_segment'] = 4;
        $total = 0;

        $results = $this->reported_user_model->getsql($total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $total;
        $data['results'] = $results;
        $data['current_offset'] = (int) $this->uri->segment(4);
        $this->load->view('admin/reported_user', $data);
    }

    function reported_chats_info($user_id, $with_chat_id) {
        $chatInfo = $this->reported_content_model->getListMsgXMPP($user_id, $with_chat_id);
        $data['chatInfo'] = $chatInfo;
        $this->load->view('admin/reported_chat_info', $data);
    }

    function reported_chats($user_id, $with_chat_id = '') {
        $chatUsers = $this->reported_content_model->getListChatUsers($user_id);
        $data['SendUserID'] = $user_id;
        $data['chatUsers'] = $chatUsers;
        $this->load->view('admin/reported_chats', $data);
    }

    function ban_reported_user($user_id, $status, $current_offset = 0) {
        $data = array();
        $isBanned = ($status == 0) ? 1 : 0;
        $data['IsBanned'] = $isBanned;
        $data['BannedDate'] = gmdate('Y-m-d H:i:s');
        $tokenInfo = $this->wstoken_model->getTokenOfUser($user_id);
        $token = ($tokenInfo) ? $tokenInfo->Token : '';
        $this->user_model->updateUser($data, array('UserID' => $user_id), $token);
        // then update all activty's user
        $this->activity_model->update(array('IsInvalid' => $isBanned), array('FromUserID' => $user_id));
        $this->reported_content_model->updateActivityArchive(array('IsInvalid' => $isBanned), array('FromUserID' => $user_id));
        $this->wstoken_model->delete(array('UserID' => $user_id));
        redirect('admin/reportban_user/reported_user/' . $current_offset);
    }

    function forgive_user($user_id, $status = null, $current_offset = 0) {
        $data = array();
        $isForgiven = ($status == 'null') ? 1 : 0;
        $data['IsForgiven'] = $isForgiven;
        $this->reported_user_model->update_reported_user(array('IsForgiven' => $isForgiven), array('ToUserID' => $user_id));
        redirect('admin/reportban_user/reported_user/' . $current_offset);
    }

    function forgive_content($content_id, $type, $status = null, $is_comment = 0, $current_offset = 0) {
        $data = array();
        $isForgiven = ($status == 'null') ? 1 : 0;
        $data['IsForgiven'] = $isForgiven;
        $this->reported_content_model->update_reported_content(array('IsForgiven' => $isForgiven), array('ContentID' => $content_id, 'Type' => $type, 'IsComment' => $is_comment));
//    	redirect('admin/reportban_user/reported_content/'.$current_offset);    	    	
    }

    function delete_content($id, $type, $is_comment = 0, $current_offset = 0) {
        if ($is_comment > 0) {
            $commentInfo = $this->activity_model->getComment($id, $type);
            if ($commentInfo) {
                $this->activity_model->increase($commentInfo->ContentID, $type, 'comment', 'decrease');
                $this->Comment_model->deleteComment($id);
            }
        } else {
            $contentInfo = $this->activity_model->getContentInfo($id, $type);
            if ($contentInfo) {
                // ANOMO-12039 Deleting Content- Trackable
                $this->activity_model->deleteActivity($id, $type, $contentInfo->UserID);
            }
        }
        $this->reported_content_model->deletecontent($id, $type, $is_comment);
    }

    function delete_content_ban_user($id, $type, $contentowner_id, $status, $is_comment = 0, $current_offset = 0) {
        set_time_limit(0);
        if ($is_comment > 0) {
            $commentInfo = $this->activity_model->getComment($id, $type);
            if ($commentInfo) {
                $this->activity_model->increase($commentInfo->ContentID, $type, 'comment', 'decrease');
                $this->Comment_model->deleteComment($id);
            }
        } else {
            $contentInfo = $this->activity_model->getContentInfo($id, $type);
            if ($contentInfo) {
                // ANOMO-12039 Deleting Content- Trackable
                $this->activity_model->deleteActivity($id, $type, $contentowner_id);
            }
        }
        // ban this user
        $data = array();
        $isBanned = ($status == 0) ? 1 : 0;
        $data['IsBanned'] = $isBanned;
        $data['BannedDate'] = gmdate('Y-m-d H:i:s');
        $tokenInfo = $this->wstoken_model->getTokenOfUser($contentowner_id);
        $token = ($tokenInfo) ? $tokenInfo->Token : '';
        $this->user_model->updateUser($data, array('UserID' => $contentowner_id), $token);
        // then update all activty's user
        $this->activity_model->update(array('IsInvalid' => $isBanned), array('FromUserID' => $contentowner_id));
        $this->reported_content_model->updateActivityArchive(array('IsInvalid' => $isBanned), array('FromUserID' => $contentowner_id));
        $this->wstoken_model->delete(array('UserID' => $contentowner_id));
        $this->reported_content_model->deletecontent($id, $type, $is_comment);
    }

    function exclude_reporting_users() {
        $data = array();
        $excludeUser = $this->input->post('excludeUser');
        if ($excludeUser) {
            $this->reported_content_model->add_exclude_reporting_users($excludeUser);
        }
        $usernames = $this->reported_content_model->getExcludedReportingUsers();
        $data['usernames'] = $usernames;

        if ($excludeUser) {
            redirect('admin/reportban_user/exclude_reporting_users');
        } else {
            $this->load->view('admin/exclude_reporting_users', $data);
        }
    }

}

?>