<?php

class LeaderBoard extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('Form');
        $this->load->model('Anonymous_Post_model');
        $this->load->model('Dashboard_model');
    }

    function index() {
        $data = array();
        if (isset($_POST['from']) && isset($_POST['to'])) {
            $data = $_POST;
            if (trim($_POST['from']) != '' && trim($_POST['to']) != '' && strtotime($_POST['to']) >= strtotime($_POST['from'])) {
                $fromDate = new DateTime($this->input->post('from'));
                $toDate = new DateTime($this->input->post('to'));

                $fromDate = date_sub($fromDate, date_interval_create_from_date_string('7 hours'));
                $toDate = date_sub($toDate, date_interval_create_from_date_string('7 hours'));
                $toDate = date_add($toDate, date_interval_create_from_date_string('1 day'));

                $from = $fromDate->format('Y-m-d H:i:s');
                $to = $toDate->format('Y-m-d H:i:s');
                
                $data['result'] = $this->Dashboard_model->getTopPoints($from, $to);
                

            }
        }
        $this->load->view('admin/leaderboard_form', $data);
    }
    
    function top_point(){
        $data['result'] = $this->Dashboard_model->getTopPoints();
        $this->load->view('admin/top_point', $data);
    }
}

?>
