<?php
class Avatar extends Ext_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Avatar_model');
        $this->load->library('S3Upload');
        $this->load->helper('Form');
    }

    /**
     * list gift
     *
     */
    function index()
    {
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $gender = 0;
        if (isset($this->uri->vn_param['gender'])) {
            $gender = $this->uri->vn_param['gender'];
        }
        $config['base_url'] = base_url() . 'admin/avatar/index';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $total = 0;
        $result = $this->Avatar_model->getList($gender, $total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['results'] = $result;
        $this->load->view('admin/avatar_list', $data);
    }

    /**
     * add/edit gift
     *
     * @param int $gift_id
     */
    function post($id = 0)
    {
        $data['id'] = $id;
        $avatar_name = '';
        $full_avatar_name = '';
        if ($id > 0) {
            $info = $this->Avatar_model->getInfo($id);
            $data['info'] = $info;
            $avatar_name = $info?$info->Avatar:'';
            $full_avatar_name = $info?$info->FullAvatar:'';
        }
        if (isset($_POST['submit'])) {
            $upload['error'] = false;
            $upload_full['error'] = false;
            if ($id == 0 || ($id > 0 && $_FILES['photo']['name'] != '')) {              
                $isUpload = $this->s3upload->upload('photo', $avatar_name);
                if (!$isUpload) {
                    $upload['error'] = true;
                } else {
                    $upload['file_name'] = $isUpload;
                }
            }
            if ($id == 0 || ($id > 0 && $_FILES['fullphoto']['name'] != '')) { 
                $isUploadFull = $this->s3upload->upload('fullphoto', $full_avatar_name);
                if (!$isUploadFull) {
                    $upload_full['error'] = true;
                } else {
                    $upload_full['file_name'] = $isUploadFull;
                }
            }

            if ($upload['error'] || $upload_full['error']) {
                $data['upload_error'] = 'Error upload image';
                $this->load->view('admin/avatar_form', $data);
            } else {
                if ($id == 0) {
                    $icon = $upload['file_name'];
                    $full_photo = $upload_full['file_name'];
                } else {
                    if (isset($upload['file_name']) && $upload['file_name'] != '') {
                        $icon = $upload['file_name'];
                    } else {
                        $icon = $info->Avatar;
                    }
                    
                    if (isset($upload_full['file_name']) && $upload_full['file_name'] != '') {
                        $full_photo = $upload_full['file_name'];
                    } else {
                        $full_photo = $info->FullAvatar;
                    }
                    
                }
                $value = array( 
                    'Avatar'        => $icon,
                    'FullAvatar'    => $full_photo,
                    'Gender'        => isset($_POST['gender'])?$_POST['gender']:0
                );
                if ($id == 0) {
                    $this->Avatar_model->add($value);
                } else {
                    $this->Avatar_model->update($value, array('ID' => $id));
                }
                redirect('admin/avatar');
            }
        } else {
            $this->load->view('admin/avatar_form', $data);
        }
    }

}

?>