<?php

class Question extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('Form');
        $this->load->helper('Ext_URI');
        $this->load->model('Question_model');
    }

    function index() {
        $this->load->library('Pagination');
        $data['groups'] = array('' => '---select---');
        $data['groups'] += $this->Question_model->getListDropDownQuestionGroup();
        $cond = array();
        if (isset($this->uri->vn_param['keyword'])) {
            $cond['keyword'] = $this->uri->vn_param['keyword'];
        }
        if (isset($this->uri->vn_param['group'])) {
            $cond['group'] = $this->uri->vn_param['group'];
        }
        if (isset($this->uri->vn_param['sort'])) {
            $cond['sort'] = $this->uri->vn_param['sort'];
        }
        
        $config['base_url'] = base_url() . 'admin/question/index';
        $config['per_page'] = 100;
        $config['uri_segment'] = 4;
        $total = 0;
        $results = $this->Question_model->getListByCondition($cond, $total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['results'] = $results;
        $this->load->view('admin/question_list', $data);
    }

    /**
     * add/edit question
     *
     * @param int $questionID
     */
    function post($questionID = 0) {
        $data['answers'] = array();
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('content', 'Content', 'trim|required|xss_clean|max_length[255]|callback_name_check[$questionID]');
            if ($questionID == 0) {
                $this->form_validation->set_rules('groupID', 'Question Group', 'required'); 
            }
            if ($this->form_validation->run() == TRUE) {
                if ($questionID == 0) {
                    $value = array(
                        'QuestionGroupID' => $_POST['groupID'],
                        'Content' => $_POST['content'],
                        'QuestionType' => $_POST['questionType'],
                        'Order' => $this->Question_model->getNextOrder($_POST['groupID']),
                        'CreatedDate' => gmdate('Y-m-d H:i:s'),
                        'ExpiredDate' => $_POST['isForever'] ? null : $_POST['expiredDate']
                    );
                    $questionID = $this->Question_model->add($value);
                } else {
                    $value = array(
                        'Content' => $_POST['content'],
                        'QuestionType' => $_POST['questionType'],
                        'ExpiredDate' => $_POST['isForever'] ? null : $_POST['expiredDate']
                    );
                    $this->Question_model->update($value, array('QuestionID' => $questionID));
                }
                foreach ($_POST['answer'] as $item) {
                    if ($item['AnswerID'] == '' && $item['Content'] != '') {
                        //add
                        $value = array(
                            'QuestionID' => $questionID,
                            'Content' => $item['Content'],
                            'CreatedDate' => gmdate('Y-m-d H:i:s')
                        );
                        $this->Question_model->addAnswer($value);
                    } else {
                        //update
                        if ($item['Content'] != '') {
                            $value = array(
                                'Content' => $item['Content'],
                            );
                            $this->Question_model->updateAnswer($value, array('AnswerID' => $item['AnswerID']));
                        } else {
                            $this->Question_model->deleteAnswer($item['AnswerID']);
                        }
                    }
                }
                redirect('admin/question');
            } else {
                $data['answers'] = isset($_POST['answer']) ? $_POST['answer'] : array();
            }
        } else {
            if ($questionID > 0) {
                $data['info'] = $this->Question_model->getQuestionInfo($questionID);
                $data['answers'] = $this->Question_model->getAnswerOf($questionID);
            }
        }

        $data['groups'] = $this->Question_model->getListDropDownQuestionGroup();
        $data['questionID'] = $questionID;

        $this->load->view('admin/question_form', $data);
    }

    function add_question_group() {
        $data['createdDate'] = gmdate('Y-m-d H:i:s');
        $this->Question_model->addQuestionGroup($data);
    }

    function import() {
        $msg = array();
        if (isset($_POST['submit'])) {
            if (self::validateImport()) {
                $msg['error'] = 'Please choose valid file.';
            } else {
                require_once 'Excel/reader.php';
                // ExcelFile($filename, $encoding);
                $data = new Spreadsheet_Excel_Reader();
                // Set output Encoding.
                $data->setOutputEncoding('CP1251');
                $data->read($_FILES['excel_file']['tmp_name']);
                error_reporting(E_ALL ^ E_NOTICE);
                for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) {
                    $return = array();
                    $columnName = array(
                        1 => 'ID',
                        2 => 'Question',
                        3 => 'Expiration',
                        4 => 'Answer1',
                        5 => 'Answer2',
                        6 => 'Answer3',
                        7 => 'Answer4',
                    );
                    for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
                        $return[$columnName[$j]] = $data->sheets[0]['cells'][$i][$j];
                    }
                    $value [] = $return;
                }
                if (count($value) > 0) {
                    $startTimely = 0;
                    $startForever = 0;
                    for ($i = 0; $i < count($value); $i++) {
                        if (trim($value[$i]['Question']) == 'Timely') {
                            $startTimely = $i + 2; // shift headline : ID, Question, Expiration, Ans1, Ans2, ...
                        }
                        if (trim($value[$i]['Question']) == 'Forever') {
                            $startForever = $i + 2; // shift headline : ID, Question, Expiration, Ans1, Ans2, ...
                        }
                    }

                    if ($startTimely > 0) {
                        $endTimely = ($startForever > $startTimely) ? $startForever : count($value);
                        $questionGroupData['createdDate'] = gmdate('Y-m-d H:i:s');
                        $groupId = $this->Question_model->addQuestionGroup($questionGroupData);
                        $numberQuestion = 0;
                        for ($j = $startTimely; $j < $endTimely; $j++) {
                            if ($value[$j]['Question'] != NULL && trim($value[$j]['Question']) != 'Question' && trim($value[$j]['Question']) != 'Timely' && trim($value[$j]['Question']) != 'Forever') {
                                $numberQuestion++;
                                if ($numberQuestion > 5) {
                                    $numberQuestion = 1;
                                    $groupId = $this->Question_model->addQuestionGroup($questionGroupData);
                                }
                                self::insertQuestion($value[$j], $groupId);
                            }
                        }
                    }

                    if ($startForever > 0) {
                        $questionGroupData['createdDate'] = gmdate('Y-m-d H:i:s');
                        $groupId = $this->Question_model->addQuestionGroup($questionGroupData);
                        $numberQuestion = 0;
                        for ($j = $startForever; $j < count($value); $j++) {
                            if ($value[$j]['Question'] != NULL && trim($value[$j]['Question']) != 'Question' && trim($value[$j]['Question']) != 'Timely' && trim($value[$j]['Question']) != 'Forever') {
                                $numberQuestion++;
                                if ($numberQuestion > 5) {
                                    $numberQuestion = 1;
                                    $groupId = $this->Question_model->addQuestionGroup($questionGroupData);
                                }
                                self::insertQuestion($value[$j], $groupId);
                            }
                        }
                    }
                    redirect('admin/question');
                }
            }
        }
        $this->load->view('admin/question_import', $msg);
    }

    private function insertQuestion($question, $groupId) {
        $questionValue = array(
            'QuestionGroupID' => $groupId,
            'Content' => $question['Question'],
            'QuestionType' => 2,
            'Order' => $this->Question_model->getNextOrder($groupId),
            'CreatedDate' => gmdate('Y-m-d H:i:s'),
//			'ExpiredDate'		=> ($question['Expiration']!= NULL)?$question['Expiration']:NULL
        );
        $questionID = $this->Question_model->add($questionValue);
        // insert answer
        if ($question['Answer1'] != NULL) {
            $answerValue = array(
                'QuestionID' => $questionID,
                'Content' => trim($question['Answer1']),
                'CreatedDate' => gmdate('Y-m-d H:i:s')
            );
            $this->Question_model->addAnswer($answerValue);
        }
        if ($question['Answer2'] != NULL) {
            $answerValue = array(
                'QuestionID' => $questionID,
                'Content' => trim($question['Answer2']),
                'CreatedDate' => gmdate('Y-m-d H:i:s')
            );
            $this->Question_model->addAnswer($answerValue);
        }
        if ($question['Answer3'] != NULL) {
            $answerValue = array(
                'QuestionID' => $questionID,
                'Content' => trim($question['Answer3']),
                'CreatedDate' => gmdate('Y-m-d H:i:s')
            );
            $this->Question_model->addAnswer($answerValue);
        }
        if ($question['Answer4'] != NULL) {
            $answerValue = array(
                'QuestionID' => $questionID,
                'Content' => trim($question['Answer4']),
                'CreatedDate' => gmdate('Y-m-d H:i:s')
            );
            $this->Question_model->addAnswer($answerValue);
        }
    }

    private function validateImport() {
        $error = false;
        if (!is_uploaded_file($_FILES['excel_file']['tmp_name'])) {
            $error = true;
        }
        $fileName = $_FILES['excel_file']['name'];
        $x = explode('.', $fileName);
        $ext = end($x);
        $allowTypes = array('xls');
        if (!in_array($ext, $allowTypes)) {
            $error = true;
        }

        // check max size ....
        return $error;
    }

    function import_with_category() {
        $msg = array();
        if (isset($_POST['submit'])) {
            if (self::validateImport()) {
                $msg['error'] = 'Please choose valid file.';
            } else {
                require_once 'Excel/reader.php';
                // ExcelFile($filename, $encoding);
                $data = new Spreadsheet_Excel_Reader();
                // Set output Encoding.
                $data->setOutputEncoding('CP1251');
                $data->read($_FILES['excel_file']['tmp_name']);
                error_reporting(E_ALL ^ E_NOTICE);
                $numberSheet = count($data->sheets);
                set_time_limit(0);
                for ($s = 0; $s < $numberSheet; $s++) {
                    for ($i = 1; $i <= $data->sheets[$s]['numRows']; $i++) {
                        $return = array();
                        $columnName = array(
                            1 => 'Group',
                            2 => 'GroupExpiration',
                            3 => 'Question',
                            4 => 'Category',
                            5 => 'Answer1',
                            6 => 'Answer2',
                            7 => 'Answer3',
                            8 => 'Answer4',
                        );
                        for ($j = 1; $j <= $data->sheets[$s]['numCols']; $j++) {
                            $return[$columnName[$j]] = $data->sheets[$s]['cells'][$i][$j];
                        }
                        $value [] = $return;
                    }
                }

                if (isset($value)) {
                    $numberQuestion = 0;
                    $questionGroupData['CreatedDate'] = gmdate('Y-m-d H:i:s');
                    $questionGroupData['ExpireDate'] = '2099-01-01'; // TODO 
                    $questionGroupData['Category'] = $value[2]['Category'];
                    $groupId = $this->Question_model->addQuestionGroup($questionGroupData);
                    for ($j = 2; $j <= count($value); $j++) {
                        if ($value[$j]['Group'] != NULL && trim($value[$j]['Group']) != 'Group') {
                            $numberQuestion++;
                            if ($numberQuestion > 5) {
                                $numberQuestion = 1;
                                if ($value[$j]['GroupExpiration'] != NULL && $value[$j]['GroupExpiration'] != '72686') {
                                    $time = self::ExcelToPHP($value[$j]['GroupExpiration']);
                                    $questionGroupData['ExpireDate'] = date('Y-m-d', $time);
                                } else {
                                    $questionGroupData['ExpireDate'] = '2099-01-01';
                                }
                                $questionGroupData['Category'] = $value[$j]['Category'];
                                $groupId = $this->Question_model->addQuestionGroup($questionGroupData);
                            }
                            self::insertQuestion($value[$j], $groupId);
                        }
                    }
                }

                redirect('admin/question');
            }
        }
        $this->load->view('admin/question_import', $msg);
    }

    private function ExcelToPHP($dateValue = 0, $ExcelBaseDate = 0) {
        if ($ExcelBaseDate == 0) {
            $myExcelBaseDate = 25569;
            //  Adjust for the spurious 29-Feb-1900 (Day 60)
            if ($dateValue < 60) {
                --$myExcelBaseDate;
            }
        } else {
            $myExcelBaseDate = 24107;
        }

        // Perform conversion
        if ($dateValue >= 1) {
            $utcDays = $dateValue - $myExcelBaseDate;
            $returnValue = round($utcDays * 86400);
            if (($returnValue <= PHP_INT_MAX) && ($returnValue >= -PHP_INT_MAX)) {
                $returnValue = (integer) $returnValue;
            }
        } else {
            $hours = round($dateValue * 24);
            $mins = round($dateValue * 1440) - round($hours * 60);
            $secs = round($dateValue * 86400) - round($hours * 3600) - round($mins * 60);
            $returnValue = (integer) gmmktime($hours, $mins, $secs);
        }

        // Return
        return $returnValue;
    }

}

?>