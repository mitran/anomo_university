<?php
class Push extends Ext_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('Log_Push_Notification_model');
	}
	
	function index(){
		$this->load->library('Pagination');
		$this->load->helper('Ext_URI');
		$cond = array();
		if (isset($this->uri->vn_param['sort'])){
			$cond['sort'] = $this->uri->vn_param['sort'];
		}
		$config['base_url'] = base_url().'admin/push/index';
		$config['per_page'] = 20;
		$config['uri_segment'] = 4;
		$config['total_rows'] = $this->Log_Push_Notification_model->getListPushByDate($cond, $config['per_page'], $this->uri->segment(4), true);
		$this->pagination->initialize($config);
		$data['total'] = $config['total_rows'];
		$data['results'] = $this->Log_Push_Notification_model->getListPushByDate($cond, $config['per_page'], $this->uri->segment(4), false);
		$this->load->view('admin/push_list', $data);
	}
}
?>