<?php
class Place extends Ext_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('Form');
		$this->load->helper('Ext_URI');
		$this->load->library('FactualSearchVenue');
		$this->searchvenue = FactualSearchVenue::getInstance();
	}
	
	function index() {
		if (isset($_POST['lat']) && isset($_POST['lng'])) {
			$data['results'] = $this->searchvenue->searchByKeywordAndLocationV2($_POST['lat'], $_POST['lng'], $_POST['category'], $_POST['keyword']);
			$data += $_POST;
		}
		
		$optionCategories = array();
		$optionCategories[''] = '--Select--';
		$categories = $this->searchvenue->loadCategories();//print_r($categories);die;
		foreach ($categories as $value) {
			$optionCategories[$value] = $value;
		}
		$data['optionCategories'] = $optionCategories;
		
		$this->load->view('admin/place_search', $data);
	}
}
?>