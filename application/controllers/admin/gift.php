<?php
class Gift extends Ext_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Gift_model');
        $this->load->model('Gift_Send_model');
        $this->load->model('Gift_Category_model');
        $this->load->model('Image_model');
        $this->load->library('S3Upload');
        $this->load->helper('Form');
    }

    /**
     * list gift
     *
     */
    function index()
    {
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $data['categories'] = array('' => '---select---');
        $data['categories'] += $this->Gift_Category_model->getListDropDown();
        $cond = array();
        if (isset($this->uri->vn_param['keyword'])) {
            $cond['keyword'] = $this->uri->vn_param['keyword'];
        }
        if (isset($this->uri->vn_param['category'])) {
            $cond['category'] = $this->uri->vn_param['category'];
        }
        if (isset($this->uri->vn_param['sort'])) {
            $cond['sort'] = $this->uri->vn_param['sort'];
        }
        $config['base_url'] = base_url() . 'admin/gift/index';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $config['total_rows'] = $this->Gift_model->getListByCondition($cond, $config['per_page'], $this->uri->segment(4), true);
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['results'] = $this->Gift_model->getListByCondition($cond, $config['per_page'], $this->uri->segment(4), false);
        $this->load->view('admin/gift_list', $data);
    }

    /**
     * add/edit gift
     *
     * @param int $gift_id
     */
    function post($gift_id = 0)
    {
        $data['categories'] = $this->Gift_Category_model->getListDropDown();
        $data['gift_id'] = $gift_id;
        if ($gift_id > 0) {
            $info = $this->Gift_Send_model->getGiftInfo($gift_id);
            $data['info'] = $info;
        }
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Name', "trim|required|xss_clean|callback_name_check[$gift_id]");
            $this->form_validation->set_rules('category_id', 'Gift category', 'trim|required|numeric');
            $this->form_validation->set_rules('description', 'Description', 'max_length[140]|xss_clean');
            $this->form_validation->set_rules('credits', 'Credits', 'trim|required|numeric');
            $this->form_validation->set_rules('description_on_buying', 'Description On Buying', 'max_length[140]|xss_clean');

            $upload['error'] = false;
            $upload_price['error'] = false;
            $upload_194['error'] = false;
            $upload_454['error'] = false;
            if ($gift_id == 0 || ($gift_id > 0 && $_FILES['photo']['name'] != '')) {
                $isUpload = $this->s3upload->upload('photo');
                if (!$isUpload) {
                    $upload['error_msg'] = 'Error upload image';
                } else {
                    $upload['file_name'] = $isUpload;
                }
            }
            if ($gift_id == 0 || ($gift_id > 0 && $_FILES['price_photo']['name'] != '')) {
                $isUploadPrice = $this->s3upload->upload('price_photo');
                if (!$isUploadPrice) {
                    $upload_price['error_msg'] = 'Error upload image';
                } else {
                    $upload_price['file_name'] = $isUploadPrice;
                }

            }
            if ($gift_id == 0 || ($gift_id > 0 && $_FILES['photo194']['name'] != '')) {
                $isUpload194 = $this->s3upload->upload('photo194');
                if (!$isUpload194) {
                    $upload_194['error_msg'] = 'Error upload image';
                } else {
                    $upload_194['file_name'] = $isUpload194;
                }

            }
            if ($gift_id == 0 || ($gift_id > 0 && $_FILES['photo454']['name'] != '')) {
                $isUpload454 = $this->s3upload->upload('photo454');
                if (!$isUpload454) {
                    $upload_454['error_msg'] = 'Error upload image';
                } else {
                    $upload_454['file_name'] = $isUpload454;
                }

            }
            if ($this->form_validation->run() == FALSE || $upload['error'] || $upload_price['error'] || $upload_194['error'] || $upload_454['error']) {
                if ($upload['error']) {
                    $data['upload_error'] = $upload['error_msg'];
                }
                if ($upload_price['error']) {
                    $data['upload_price_error'] = $upload_price['error_msg'];
                }

                if ($upload_194['error']) {
                    $data['upload_194_error'] = $upload_194['error_msg'];
                }

                if ($upload_454['error']) {
                    $data['upload_454_error'] = $upload_454['error_msg'];
                }

                $this->load->view('admin/gift_form', $data);
            } else {
                if ($gift_id == 0) {
                    $icon = $upload['file_name'];
                    $price_photo = $upload_price['file_name'];
                    $photo194 = $upload_194['file_name'];
                    $photo454 = $upload_454['file_name'];
                } else {
                    if (isset($upload['file_name']) && $upload['file_name'] != '') {
                        $icon = $upload['file_name'];

                        $this->s3upload->deleteObjectS3($this->config->item('s3_photo_url') . $info->Photo);
                    } else {
                        $icon = $info->Photo;
                    }

                    if (isset($upload_price['file_name']) && $upload_price['file_name'] != '') {
                        $price_photo = $upload_price['file_name'];
                        $this->s3upload->deleteObjectS3($this->config->item('s3_photo_url') . $info->PricePhoto);
                    } else {
                        $price_photo = $info->PricePhoto;
                    }

                    if (isset($upload_194['file_name']) && $upload_194['file_name'] != '') {
                        $photo194 = $upload_194['file_name'];
                        $this->s3upload->deleteObjectS3($this->config->item('s3_photo_url') . $info->Photo194);
                    } else {
                        $photo194 = $info->Photo194;
                    }

                    if (isset($upload_454['file_name']) && $upload_454['file_name'] != '') {
                        $photo454 = $upload_454['file_name'];
                        $this->s3upload->deleteObjectS3($this->config->item('s3_photo_url') . $info->Photo454);
                    } else {
                        $photo454 = $info->Photo454;
                    }
                }
                $value = array(
                    'Name' => $_POST['name'],
                    'Photo' => $icon,
                    'PricePhoto' => $price_photo,
                    'Photo194' => $photo194,
                    'Photo454' => $photo454,
                    'GiftCategoryID' => $_POST['category_id'],
                    'Credits' => $_POST['credits'],
                    'Description' => $_POST['description'],
                    'DescriptionOnBuying' => $_POST['description_on_buying']
                );
                if ($gift_id == 0) {
                    $this->Gift_model->add($value);
                } else {
                    $this->Gift_model->update($value, array('GiftID' => $gift_id));
                }
                redirect('admin/gift');
            }
        } else {
            $this->load->view('admin/gift_form', $data);
        }
    }

    function name_check($name, $gift_id)
    {
        $this->form_validation->set_message('name_check', 'Name is existed');
        return $this->Gift_model->checkExistedName($name, $gift_id);
    }
}

?>