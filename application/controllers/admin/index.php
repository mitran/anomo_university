<?php

class Index extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('Session');
        $this->load->model('Admin_model');
        $this->lang->load('admin');
    }

    function list_user() {
        $this->load->helper('Form');
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $cond = array();
        if (isset($this->uri->vn_param['keyword'])) {
            $cond['keyword'] = $this->input->xss_clean($this->uri->vn_param['keyword']);
        }
        if (isset($this->uri->vn_param['status'])) {
            $cond['status'] = $this->uri->vn_param['status'];
        }
        if (isset($this->uri->vn_param['sort'])) {
            $cond['sort'] = $this->uri->vn_param['sort'];
        }
        $config['base_url'] = base_url() . 'admin/index/list_user';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $config['total_rows'] = $this->Admin_model->getListByCondition($cond, $config['per_page'], $this->uri->segment(4), true);
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['results'] = $this->Admin_model->getListByCondition($cond, $config['per_page'], $this->uri->segment(4), false);
        $this->load->view('admin/list_user', $data);
    }

    function login($redirect_uri = '') {
        if ($this->session->userdata('role') == 'admin') {
            redirect('admin/adminpanel');
        }

        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('user_name', 'User name', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/login');
            } else {
                if ($this->Admin_model->login($_POST['user_name'], $_POST['password'])) {
                    if ($redirect_uri == '') {
                        redirect('admin/adminpanel');
                    } else {
                        redirect($redirect_uri);
                    }
                } else {
                    $info = $this->Admin_model->getInfo($_POST['user_name'], 'user_name');
                    if ($info && $info->Status == 0) {
                        $data['login_error'] = 'Your account is not active';
                    } elseif ($info && $info->Status == 2) {
                        $data['login_error'] = 'Your account is banned';
                    } else {
                        $data['login_error'] = 'Incorrect user name or password';
                    }
                    $this->load->view('admin/login', $data);
                }
            }
        } else {
            $this->load->view('admin/login', array('redirect_uri' => $redirect_uri));
        }
    }

    function logout() {
        $this->Admin_model->logout();
        redirect('admin/index/login');
    }

    function profile() {
        if ((int) $this->session->userdata('admin_id')) {
            $data['admin'] = $this->Admin_model->getInfo($this->session->userdata('admin_id'));
            if (isset($_POST['save'])) {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
                $this->form_validation->set_rules('nick_name', 'Nick name', 'required|xss_clean|max_length[30]');
                if ($_POST['new_password'] != '' || $_POST['re_password'] != '') {
                    $this->form_validation->set_rules('new_password', 'New password', 'trim|required|min_length[6]|max_length[30]|matches[re_password]');
                    $this->form_validation->set_rules('re_password', 'Re-newpassword', 'trim|required');
                }
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('admin/profile', $data);
                } else {
                    if ($_POST['new_password'] != '' && $_POST['re_password'] != '') {
                        $value['Password'] = md5($_POST['new_password']);
                    }
                    $value['NickName'] = $_POST['nick_name'];
                    $value['Email'] = $_POST['email'];
                    $this->Admin_model->update($value, array('AdminID' => $this->session->userdata('admin_id')));
                    redirect('admin/index/profile/p_update/success');
                }
            } else {
                $this->load->view('admin/profile', $data);
            }
        }
    }

    function register() {
        $this->load->library('Validation');
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('user_name', 'User name', 'xss_clean|trim|required|min_length[4]|max_length[30]|callback_username_check');
            $this->form_validation->set_rules('email', 'Password', 'required|valid_email');
            $this->form_validation->set_rules('nick_name', 'User name', 'required|xss_clean|max_length[30]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[30]|matches[re_password]');
            $this->form_validation->set_rules('re_password', 'Password', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/register');
            } else {
                $data = array('UserName' => $_POST['user_name'],
                    'Email' => $_POST['email'],
                    'NickName' => $_POST['nick_name'],
                    'Password' => md5($_POST['password']),
                    'Status' => 0,
                );
                $admin_id = $this->Admin_model->add($data);

                // send mail
                $this->load->library('Email');
                $this->email->from($this->config->item('email_from'), $this->config->item('email_from_name'));
                $this->email->to($_POST['email']);
                $this->email->subject($this->lang->line('register_email_subject'));
                $msg = "<p>" . $this->lang->line('register_email_msg') . "</p>";
                $msg .= '<p>' . base_url() . 'admin/index/confirm/' . md5($admin_id) . '</p>';
                $this->email->message($msg);
                @$this->email->send();

                redirect('admin/index/list_user');
            }
        } else {
            $this->load->view('admin/register');
        }
    }

    function username_check($user_name) {
        $this->form_validation->set_message('username_check', 'Username is exists');
        return $this->Admin_model->check_user_name($user_name);
    }

    function change_status_user($user_id, $status_id) {
        $this->Admin_model->update(array('Status' => $status_id), array('AdminID' => $user_id));
    }

    function confirm($code) {
        $re = $this->db->query("select * from admin where md5(AdminID)='$code'");
        if (count($re) > 0) {
            $this->Admin_model->update(array('Status' => 1), array('md5(AdminID)' => $code));
            $data['msg'] = $this->lang->line('confirm_success');
        } else {
            $data['msg'] = $this->lang->line('confirm_fail');
        }
        $this->load->view('admin/confirm', $data);
    }

    function forgot_password() {
        $this->load->library('form_validation');
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('user_name', 'User name', 'required|callback_check_user_exists');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/forgot_password');
            } else {
                $info = $this->Admin_model->getInfo($_POST['user_name'], 'user_name');

                // update db
                $key = md5(time());
                $this->Admin_model->update(array('ForgotPassKey' => $key), array('UserName' => $_POST['user_name']));

                // send mail
                $this->load->library('email');
                $this->email->from($this->config->item('email_from'), $this->config->item('email_from_name'));
                $this->email->to($info->email);
                $this->email->subject('[' . $this->config->item('email_from_name') . '] ' . $this->lang->line('password_reset'));
                $msg = "<p>" . $this->lang->line('pass_reset1') . "</p>
						<p>" . base_url() . "</p>
						<p>User name: " . $_POST['user_name'] . "</p>
						<p>" . $this->lang->line('pass_reset2') . "</p>
						<p>" . base_url() . "admin/index/reset_password/" . $_POST['user_name'] . "/" . $key . "</p>
				";
                $this->email->message($msg);
                $this->email->send();
                $success = "Check your e-mail for the confirmation link";
                redirect('admin/index/forgot_password/p_do/' . $success);
            }
        } else {
            $this->load->view('admin/forgot_password');
        }
    }

    function check_user_exists($user_name) {
        $this->form_validation->set_message('check_user_exists', 'Username not exists');
        $re = $this->Admin_model->check_user_name($user_name);
        return !($re);
    }

    function reset_password($user_name = '', $key = '') {
        $info = $this->Admin_model->check_user_reset_pass($user_name, $key);
        if (count($info) > 0) {
            $data['user_name'] = $user_name;
            $data['key'] = $key;
            if (isset($_POST['submit'])) {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[30]|matches[re_password]');
                $this->form_validation->set_rules('re_password', 'Re-enter password', 'trim|required');
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('admin/reset_password', $data);
                } else {
                    $this->Admin_model->update(array('Password' => md5($_POST['password']), 'ForgotPassKey' => ''), array('UserName' => $user_name));
                    $msg = "Reset password successful";
                    redirect('admin/index/reset_password/p_do/' . $msg);
                }
            } else {
                $this->load->view('admin/reset_password', $data);
            }
        } else {
            $data['msg'] = 'error link';
            $this->load->view('admin/reset_password', $data);
        }
    }

    function delete($user_id) {
        $info = $this->Admin_model->getInfo($user_id);
        if (count($info) > 0) {
            $this->Admin_model->delete($user_id);
            $data['msg'] = 'Delete user successful';
        } else {
            $data['msg'] = "User not exists";
        }
        if ($this->session->userdata('admin_id') == $user_id) {
            redirect('admin/index/logout');
        } else {
            redirect('admin/index/list_user/p_delete/' . $data['msg']);
        }
    }

}

?>