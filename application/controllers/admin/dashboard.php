<?php
class Dashboard extends Ext_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Dashboard_model');
        $this->load->model('Vegas_model');
        $this->load->library("Flurry");
    }

    function index() {
        set_time_limit(0);
        $data = array();
        $data['resgister_all'] = $this->Dashboard_model->getTotalResgisterUser('all');
        $data['resgister_month'] = $this->Dashboard_model->getTotalResgisterUser('month');
        $data['resgister_day'] = $this->Dashboard_model->getTotalResgisterUser('day');
        $data['resgister_week'] = $this->Dashboard_model->getTotalResgisterUser('week');

        $data['verified_all'] = $this->Dashboard_model->getTotalResgisterUser('all', 'verified');
        $data['verified_month'] = $this->Dashboard_model->getTotalResgisterUser('month', 'verified');
        $data['verified_day'] = $this->Dashboard_model->getTotalResgisterUser('day', 'verified');
        $data['verified_week'] = $this->Dashboard_model->getTotalResgisterUser('week', 'verified');

        $data['unverified_all'] = $this->Dashboard_model->getTotalResgisterUser('all', 'unverified');
        $data['unverified_month'] = $this->Dashboard_model->getTotalResgisterUser('month', 'unverified');
        $data['unverified_day'] = $this->Dashboard_model->getTotalResgisterUser('day', 'unverified');
        $data['unverified_week'] = $this->Dashboard_model->getTotalResgisterUser('week', 'unverified');

        $data['game_all'] = $this->Dashboard_model->getTotalGame('all');
        $data['game_month'] = $this->Dashboard_model->getTotalGame('month');
        $data['game_day'] = $this->Dashboard_model->getTotalGame('day');
        $data['game_week'] = $this->Dashboard_model->getTotalGame('week');

        $data['gift_sent_all'] = $this->Dashboard_model->getTotalGiftSent('all');
        $data['gift_sent_month'] = $this->Dashboard_model->getTotalGiftSent('month');
        $data['gift_sent_day'] = $this->Dashboard_model->getTotalGiftSent('day');
        $data['gift_sent_week'] = $this->Dashboard_model->getTotalGiftSent('week');

        $data['reveal_all'] = $this->Dashboard_model->getTotalReveal('all');
        $data['reveal_month'] = $this->Dashboard_model->getTotalReveal('month');
        $data['reveal_day'] = $this->Dashboard_model->getTotalReveal('day');
        $data['reveal_week'] = $this->Dashboard_model->getTotalReveal('week');

        $data['blocked_all'] = $this->Dashboard_model->getTotalUserBlocked();


        $data['share_all'] = $this->Dashboard_model->getTotalTimeFbShare('all');
        $data['share_month'] = $this->Dashboard_model->getTotalTimeFbShare('month');
        $data['share_day'] = $this->Dashboard_model->getTotalTimeFbShare('day');
        $data['share_week'] = $this->Dashboard_model->getTotalTimeFbShare('week');

        $data['share_pt_all'] = $this->Dashboard_model->getTotalTimeFbShare('all', 'first');
        $data['share_pt_month'] = $this->Dashboard_model->getTotalTimeFbShare('month', 'first');
        $data['share_pt_day'] = $this->Dashboard_model->getTotalTimeFbShare('day', 'first');
        $data['share_pt_week'] = $this->Dashboard_model->getTotalTimeFbShare('week', 'first');

        $data['total_status'] = $this->Dashboard_model->totalOther('', '', 'Status');
        $data['total_status_day'] = $this->Dashboard_model->totalOther('', '', 'Status', 'day');
        $data['total_status_month'] = $this->Dashboard_model->totalOther('', '', 'Status', 'month');
        $data['total_status_week'] = $this->Dashboard_model->totalOther('', '', 'Status', 'week');
        $data['total_comment'] = $this->Dashboard_model->totalOther('', '', 'ActivityComment') + $this->Dashboard_model->totalOther('', '', 'PostComment') + $this->Dashboard_model->totalOther('', '', 'NeighborhoodComment');
        $data['total_comment_day'] = $this->Dashboard_model->totalOther('', '', 'ActivityComment', 'day') + $this->Dashboard_model->totalOther('', '', 'PostComment', 'day') + $this->Dashboard_model->totalOther('', '', 'NeighborhoodComment', 'day');
        $data['total_comment_month'] = $this->Dashboard_model->totalOther('', '', 'ActivityComment', 'month') + $this->Dashboard_model->totalOther('', '', 'PostComment', 'month') + $this->Dashboard_model->totalOther('', '', 'NeighborhoodComment', 'month');
        $data['total_comment_week'] = $this->Dashboard_model->totalOther('', '', 'ActivityComment', 'week') + $this->Dashboard_model->totalOther('', '', 'PostComment', 'week') + $this->Dashboard_model->totalOther('', '', 'NeighborhoodComment', 'week');
        $data['total_likes'] = $this->Dashboard_model->totalOther('', '', 'ActivityLike');
        $data['total_likes_day'] = $this->Dashboard_model->totalOther('', '', 'ActivityLike', 'day');
        $data['total_likes_month'] = $this->Dashboard_model->totalOther('', '', 'ActivityLike', 'month');
        $data['total_likes_week'] = $this->Dashboard_model->totalOther('', '', 'ActivityLike', 'week');

        $data['activity_6h'] = $this->Dashboard_model->countActivity('6 hour');
        $data['activity_12h'] = $this->Dashboard_model->countActivity('12 hour');
        $data['activity_24h'] = $this->Dashboard_model->countActivity('24 hour');

        $data['activity_1w'] = $this->Dashboard_model->countActivity('1 week');

        // Hung added 31/5/2013 - activities without likes and comments count
        $data['activity_feeds_6h'] = $this->Dashboard_model->countActivityNoComments('6 hour');
        $data['activity_feeds_12h'] = $this->Dashboard_model->countActivityNoComments('12 hour');
        $data['activity_feeds_24h'] = $this->Dashboard_model->countActivityNoComments('24 hour');

        // ANOMO-5782
        $android = $ios = 0;
       
        /*$ios =  $this->flurry->getTotalFlurryActiveUser('ios', 'month');
        sleep(1);
        $android =  $this->flurry->getTotalFlurryActiveUser('android', 'month');
        $days = cal_days_in_month(CAL_GREGORIAN, date('n')-1, date('Y'));
        $data['daily_active_users_month'] =  number_format(($android + $ios)/$days);
        
        
        sleep(1);
        $ios =  $this->flurry->getTotalFlurryActiveUser('ios', 'day');
        sleep(1);
        $android =  $this->flurry->getTotalFlurryActiveUser('android', 'day');
        $data['daily_active_users_day'] = $android + $ios;
        
        sleep(1);                                  
        $ios =  $this->flurry->getTotalFlurryActiveUser('ios', 'week');
        sleep(1);
        $android =  $this->flurry->getTotalFlurryActiveUser('android', 'week');
        
        $data['daily_active_users_week'] = number_format(($android + $ios)/7);
        */
        // ANOMO-7004
        /*
         *  - Which users clicked on "Invite Friends" and how many friends did they invite 
            - Which users clicked on "Share to Wall" popup for the 1000 free pts on initial sign up 
            - Which users clicked on "Share to Wall" popup for the 1000 free pts after 3 days
         */
        $data['share_to_wall_first_time'] = $this->Dashboard_model->getTotalUserShareToWall('first_time');
        $data['share_to_wall_3days'] = $this->Dashboard_model->getTotalUserShareToWall('3days');
        $data['number_friend_invite'] = $this->Dashboard_model->getTotalFriendInvited();
        $data['vegas_count'] = $this->Vegas_model->totalVegasView();
        $this->load->view('admin/dashboard', $data);
    }
}
?>