<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Topic extends Ext_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('Datatables');
        $this->load->library('table');
        $this->load->model('Topic_model');
    }

    function index() {
        $this->load->view('admin/topic');
    }

    //function to handle callbacks
    function datatable() {        
        $this->datatables->select('t.TopicID, t.TopicName, t.Desc, t.Photo, t.UserID, u.UserName Moderator, t.TotalPost, t.CreatedDate, t.LastModified')
                ->unset_column('TopicID')
                ->from('topic t')
                ->join('user u', 't.UserID=u.UserID', 'LEFT')
                ->where('IsDelete', 0);
        echo $this->datatables->generate();
    }

    function edit() {
        $ids = $this->input->post('id');
        $ids = explode("_", $ids);
        $id = $ids[0];
        $attr = $this->input->post('columnname');
        $newval = $this->input->post('value');
        $attr = preg_replace('/\s+/', '', $attr);
        if ($id > 0 && $attr != 'TopicID')
        $this->Topic_model->update(array($attr => $newval), $id);
    }

    function delete() {
        $id = $this->input->post('id');
        $this->Topic_model->deleteTopic($id); 
    }

}

?>
