<?php

class Deleted_Content extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Deleted_Content_model');
        $this->load->model('Dashboard_model');
    }

    function index($user_id = 0) {
        $data['total'] = $this->Deleted_Content_model->getTotalDeletedContent($user_id, 0);
        $result = $this->Deleted_Content_model->getListDeletedContent($user_id, 0);
        $data['results'] = $result;
        $deletedDate = '';
        foreach($result as $row){
            $contentInfo = $this->Deleted_Content_model->getContentDetail($row->RefID, $row->Type);
            if ($contentInfo){
                $deletedDate = $contentInfo->DeletedDate;
            }
            $row->DeletedDate = $deletedDate;
        }
        $data['user_id'] = $user_id;
        $this->load->view('admin/deleted_content_list', $data);
    }
    
    function load_more($user_id = 0, $last_activity_id = 0){
        $result = $this->Deleted_Content_model->getListDeletedContent($user_id, $last_activity_id);
        
        $deletedDate = '';
        if ($result){
            foreach($result as $row){
                $contentInfo = $this->Deleted_Content_model->getContentDetail($row->RefID, $row->Type);
                if ($contentInfo){
                    $deletedDate = $contentInfo->DeletedDate;
                }
                $row->DeletedDate = $deletedDate;
            }
        }
        $html = '';
        
        if ($result){
            $html .= "<tr><td colspan=7><script>jQuery(function() {
                    jQuery('img.lazy').lazyload();
                });</script></td></tr>";
            foreach($result as $row){
                $html .= "<tr>
                            <td>$row->UserID</td>
                            <td><a href=".  base_url().'admin/deleted_content/index/'.$row->UserID.">$row->UserName</a></td>
                            <td style='max-width: 400px'>".$this->Dashboard_model->formatMsg($row->Message)."</td>
                            <td>";
                 if ($row->Image){
                       $html.="<a href=' $row->Image ' data-lightbox='$row->Image'  >
                                    <img style='max-width:150px;' class='lazy' data-original='$row->Image'  />
                                </a>";
                 }elseif ($row->VideoID){
                     $src = $row->VideoSource == 'youtube'?'http://www.youtube.com/embed/'. $row->VideoID: VINE_VIDEO_URL. $row->VideoID."/embed/simple";
                     $html .= "<div> <iframe class='play_video' type='text/html'  src='$src'  frameborder='0' ></iframe></div>";
                 }
                $html.=    "</td>
                            <td> $row->CreatedDate</td>
                            <td> $row->DeletedDate </td>
                        </tr>";
            }
            $last_activity_id = $result[count($result) - 1]->ActivityID;
        }

        
        $data['html'] = $html;
        $data['last_activity_id'] = $last_activity_id;
        echo json_encode($data);
    }

}

?>
