<?php

class Credit extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Credit_model');
        $this->load->helper('Form');
    }

    /**
     * list credit page
     *
     */
    function index() {
        $this->load->library('pagination');
        $this->load->helper('ext_uri');
        $cond = array();
        if (isset($this->uri->vn_param['sort'])) {
            $cond['sort'] = $this->uri->vn_param['sort'];
        }
        $config['base_url'] = base_url() . 'admin/credit/index';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $config['total_rows'] = $this->Credit_model->getListByCondition($cond, $config['per_page'], $this->uri->segment(4), true);
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['results'] = $this->Credit_model->getListByCondition($cond, $config['per_page'], $this->uri->segment(4), false);
        $this->load->view('admin/credit_list', $data);
    }

    /**
     * add/edit credit
     *
     * @param int $credit_id
     */
    function post($credit_id = 0) {
        $data['credit_id'] = $credit_id;
        if ($credit_id > 0) {
            $info = $this->Credit_model->getCreditPackageInfo($credit_id);
            $data['info'] = $info;
        }
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('credits', 'Price', 'trim|required|numeric');
            $this->form_validation->set_rules('price', 'Price', 'trim|required|numeric');
            $this->form_validation->set_rules('product_id', 'ProductID', 'trim|required');
            
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/credit_form', $data);
            } else {
                $value = array(
                    'Platform' => $_POST['platform'],
                    'Credits' => $_POST['credits'],
                    'Price' => $_POST['price'],
                    'ProductID' => $_POST['product_id']
                );
                if ($credit_id == 0) {
                    $this->Credit_model->add($value);
                } else {
                    $this->Credit_model->update($value, array('ID' => $credit_id));
                }
                redirect('admin/credit');
            }
        } else {
            $this->load->view('admin/credit_form', $data);
        }
    }

}

?>