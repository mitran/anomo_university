<?php
class Auto_Msg extends Ext_Controller{
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Auto_Msg_model');
        $this->load->model('Promotion_Location_model');
        $this->load->library('S3Upload');
    }
    
    function send($id = 0){
        $aData = array();
        $aData['id'] = $id;
        $promos_location = array(
            '1' => 'vegas'
        );
        $aData['promos_location'] = $this->Promotion_Location_model->getListDropDown();
        if ($id > 0){
            $info = $this->Auto_Msg_model->getInfo($id);
            $aData['info'] = $info;
        }

        if (isset($_POST)) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('promo_location_id', 'Location', 'required'); 
            $this->form_validation->set_rules('expire_date', 'Expire Date', 'required'); 
            $this->form_validation->set_rules('tap_param', 'Tap Param', 'trim'); 
            $photo = '';
            $error = false;
            if (isset($_FILES['photo']['name']) && $_FILES['photo']['name'] != '') {
                $isUpload = $this->s3upload->upload('photo');
                if (!$isUpload) {
                    $error = true;
                } else {
                    $photo = $isUpload;
                }
            }
                
            if ($this->form_validation->run() == FALSE || $error) {
                if ($error) {
                    $aData['upload_error'] = 'Error upload image';
                }
                $this->load->view('admin/auto_msg_form', $aData);
            } else {
                if ($id > 0 && empty($photo)) {
                    $photo = $info->photo;
                }
                 $value = array(
                    'message' => $this->input->post('message'),
                    'photo' => $photo,
                    'tap_action' => $this->input->post('tap_action'),
                    'tap_param' => $this->input->post('tap_param'),
                    'expire_date' => $this->input->post('expire_date'),
                    'status' => isset($_POST['status']) ? 1 : 0,
                );

                $constraints = array();
                if (isset($_POST['promo_location_id']) && $_POST['promo_location_id'] > 0) {
                    $constraints[] = array(
                        'cond_key' => 'promo_location_id',
                        'value' => $_POST['promo_location_id']
                    );
                }
                if (isset($_POST['age_from']) && $_POST['age_from'] > 0) {
                    $constraints[] = array(
                        'cond_key' => 'age_from',
                        'value' => $_POST['age_from']
                    );
                }
                if (isset($_POST['age_to']) && $_POST['age_to'] > 0) {
                    $constraints[] = array(
                        'cond_key' => 'age_to',
                        'value' => $_POST['age_to']
                    );
                }
                if (isset($_POST['gender']) && $_POST['gender'] > 0) {
                    $constraints[] = array(
                        'cond_key' => 'gender',
                        'value' => $_POST['gender']
                    );
                }
                $value['constraints'] = $constraints;
                if ($id > 0){
                    $this->Auto_Msg_model->update($id, $value);
                }else{
                    $this->Auto_Msg_model->add($value);
                }
                
                
                redirect('admin/auto_msg/listmsg');
            }
        } else {
            $this->load->view('admin/auto_msg_form', $aData);
        }
    }
    
    function listmsg() {
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $cond = array();
        $config['base_url'] = base_url() . 'admin/auto_msg/listmsg';
        $config['per_page'] = 50;
        $config['uri_segment'] = 4;
        $total = 0;
        $results = $this->Auto_Msg_model->getListByCondition($cond, $total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $total;
        $data['results'] = $results;
        $this->load->view('admin/auto_msg_list', $data);
    }
    
    function delete($id){
        $this->Auto_Msg_model->delete($id);
        $this->session->set_flashdata('msg', 'Delete successful');
        redirect('admin/auto_msg/listmsg');
    }
}
?>
