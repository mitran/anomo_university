<?php

class LPlaces extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('LPlace_model');
        $this->load->model('Place_Category_model');
        $this->load->helper('Form');
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $this->lang->load('admin');
    }

    function index() {
        $cond = array();
        /*
          if (isset($this->uri->vn_param['keyword'])){
          $cond['keyword'] = $this->input->xss_clean($this->uri->vn_param['keyword']);
          }
          if (isset($this->uri->vn_param['status'])){
          $cond['status'] = $this->uri->vn_param['status'];
          }
          if (isset($this->uri->vn_param['sort'])){
          $cond['sort'] = $this->uri->vn_param['sort'];
          }
         */
        $config['base_url'] = base_url() . 'admin/lplaces/index';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $config['total_rows'] = $this->LPlace_model->getCountTotal();
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['results'] = $this->LPlace_model->getListByCondition($cond, $config['per_page'], $this->uri->segment(4), false);
        $this->load->view('admin/lplace', $data);
    }

    function post($id = 0) {
        $data['id'] = $id;
        $data['optionCategories'] = $this->Place_Category_model->findAllCategoryName();


        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) { //check request method
            //get selected categories
            $category = '';
            if (isset($_POST['Category'])) {
                if (count($_POST['Category']) > 0) {
                    $arrCategories = array_unique($_POST['Category']);
                    $category = implode('|', $arrCategories);
                }
            }

            //var_dump($_POST['Category']);
            //validation               
            $this->load->library('validation');
            $rules['Name'] = 'trim|required|xss_clean';
            $rules['Lat'] = 'trim|required|numeric';
            $rules['Lon'] = 'trim|required|numeric';
            $rules['Address'] = 'trim|required|xss_clean';
            $rules['Category'] = 'required';
            $rules['Provider'] = 'trim|required|xss_clean';


            $fields['Name'] = 'Name';
            $fields['Lat'] = 'Lat';
            $fields['Lon'] = 'Lon';
            $fields['Category'] = 'Category';
            $fields['Address'] = 'Address';
            $fields['Provider'] = 'Provider';

            $this->validation->set_rules($rules);
            $this->validation->set_fields($fields);



            if ($this->validation->run() == FALSE) {
                $this->load->view('admin/lplace_form', $data);
            } else {
                $value = array(
                    'Name' => $_POST['Name'],
                    'CreatedDate' => gmdate('Y-m-d H:i:s'),
                    'Lat' => $_POST['Lat'],
                    'Lon' => $_POST['Lon'],
                    'Address' => $_POST['Address'],
                    'Category' => $category,
                    'Provider' => $_POST['Provider'],
                );
                if ($id == 0) { //insert process
                    //generate random PlaceID
                    $placeid = $this->LPlace_model->generateLocalPlaceID();
                    if (!isset($placeid)) {
                        $data['localplaceid_error'] = $this->lang->line('localplaceid_fail');
                        $this->load->view('admin/lplace_form', $data);
                        return false;
                    } else {
                        $value['PlaceID'] = $placeid;
                        $this->LPlace_model->add($value);
                    }
                } else { //update process
                    $this->LPlace_model->update($id, $value);
                }
                redirect('admin/lplaces');
            }
        } else {
            if ($id > 0) {
                $info = $this->LPlace_model->getLocalPlaceInfo($id);
                $data['info'] = $info;
                $data['arr_category'] = explode("|", $info->Category);
            }
            $this->load->view('admin/lplace_form', $data);
        }
    }

    function delete($id) {
        $info = $this->LPlace_model->getLocalPlaceInfo($id);
        if ($info) {
            // delete	
            $this->LPlace_model->delete($id);
        }
        redirect('admin/lplaces');
    }

}

?>