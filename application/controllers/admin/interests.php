<?php
class Interests extends Ext_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tag_model');
        $this->load->helper('Form');
    }

    /**
     * list gift
     *
     */
    function index()
    {
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $data['categories'] = array('' => '---select---');
        $data['categories'] += $this->Tag_model->getTagCategory();

        $cond = array();
        if (isset($this->uri->vn_param['category'])) {
            $cond['category'] = $this->uri->vn_param['category'];
        }
        $config['base_url'] = base_url() . 'admin/interests/index';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $total = 0;
        $result = $this->Tag_model->getListWithPaging($cond, $total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['results'] = $result;
        $this->load->view('admin/interests_list', $data);
    }

    /**
     * add/edit gift
     *
     * @param int $gift_id
     */
    function post($id = 0)
    {
        $data['categories'] = $this->Tag_model->getTagCategory();
        $data['id'] = $id;
        if ($id > 0) {
            $info = $this->Tag_model->getInfo($id);
            $data['info'] = $info;
        }
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Name', "trim|required|xss_clean|callback_name_check[$id]");
            $this->form_validation->set_rules('category_id', 'Tag category', 'trim|required|numeric');
            if ($this->form_validation->run() == FALSE ) {
                $this->load->view('admin/interests_form', $data);
            } else {
                $value = array(
                    'Name' => $_POST['name'],
                    'TagCategoryID' => $_POST['category_id'],
                );
                if ($id == 0) {
                    $this->Tag_model->add($value);
                } else {
                    $this->Tag_model->update($value, array('TagID' => $id));
                }
                redirect('admin/interests');
            }
        } else {
            $this->load->view('admin/interests_form', $data);
        }
    }

    function name_check($name, $id)
    {
        $this->form_validation->set_message('name_check', 'Name is existed');
        return  $this->Tag_model->checkExistedName($name, $id);
    }
    
    function cats(){
        $data['results'] = $this->Tag_model->getTagCategory();
        $this->load->view('admin/interest_cats_list', $data);
    }
    
    function post_cats($id){
         $data['id'] = $id;
        if ($id > 0) {
            $info = $this->Tag_model->getCatInfo($id);
            $data['info'] = $info;
        }
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/interests_cat_form', $data);
            } else {
                $value = array(
                    'Name' => $_POST['name']
                );
                if ($id == 0) {
                    $this->Tag_model->addCat($value);
                } else {
                    $this->Tag_model->updateCat($value, array('ID' => $id));
                }
                redirect('admin/interests');
            }
        } else {
            $this->load->view('admin/interests_cat_form', $data);
        }
    }
}

?>