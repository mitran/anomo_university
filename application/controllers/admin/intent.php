<?php

class Intent extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Intent_model');
        $this->load->helper('Form');
    }

    /**
     * list credit page
     *
     */
    function index() {
        $this->load->library('Pagination');
        $config['base_url'] = base_url() . 'admin/avatar/index';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $total = 0;
        $result = $this->Intent_model->getListIntentWithPaging($total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['results'] = $result;
        $this->load->view('admin/intent_list', $data);
    }

    /**
     * add/edit intent
     *
     * @param int $id
     */
    function post($id = 0) {
        $data['id'] = $id;
        if ($id > 0) {
            $info = $this->Intent_model->getInfo($id);
            $data['info'] = $info;
        }
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/intent_form', $data);
            } else {
                $value = array(
                    'Name' => $_POST['name'],
                );
                if ($id == 0) {
                    $this->Intent_model->add($value);
                } else {
                    $this->Intent_model->update($value, array('IntentID' => $id));
                }
                redirect('admin/intent');
            }
        } else {
            $this->load->view('admin/intent_form', $data);
        }
    }

}

?>