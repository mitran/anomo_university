<?php

class Campaign extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Campaign_model');
        $this->load->library('form_validation');            
        $this->lang->load('ws');
    }

    function index() {        
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');        
        $config['base_url'] = base_url() . 'admin/promos/index';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $total = 0;
        $result = $this->Campaign_model->getListCampaignWithPaging($total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['results'] = $result;
        $this->load->view('admin/campaign_list', $data);
    }
    
    
    
    function dailyreport($CampaignID = 0){
        
        if($CampaignID){
            if (isset($_POST['from']) && isset($_POST['to'])) {
                $from = $_POST['from'];
                $to = $_POST['to'];
            }else{
                $month = gmdate('Y-m');
                $today = gmdate('Y-m-d');
                $from =  date('Y-m-01',strtotime($month));
                $to =  date('Y-m-t',strtotime($month));
                if(strtotime($to) > strtotime($today)){
                    $to = $today;
                }
            }    
            $info = $this->Campaign_model->getInfo($CampaignID);
            if ($info){
                if(strtotime($info->StartDate)>strtotime($from)){
                    $from = date('Y-m-d',strtotime($info->StartDate));
                }
                $data['info'] = $info;
                $data['results'] = $this->Campaign_model->getDailyReport($CampaignID,$from,$to);  
            }else{                
                redirect('admin/campaign');
            }
            
            $data['from'] = $from;
            $data['to'] = $to;
            $data['CampaignID'] = $CampaignID;     
            $this->load->view('admin/campaign_dailyreport', $data);
        }
        
    }
    
    
}

?>
