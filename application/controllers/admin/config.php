<?php

class Config extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Config_model');
        $this->load->model('Hashtag_model');
        $this->load->helper('Form');
    }

    function index() {
        $data['config'] = $this->Config_model->getAllConfig();
        $data['trending_hashtag'] = $this->Hashtag_model->topTrendingHashTag();
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('radius', 'Radius', 'trim|required|numeric');
            $this->form_validation->set_rules('radius_keyword', 'Radius with search keyword', 'trim|required|numeric');
            $this->form_validation->set_rules('validation_timeline', 'Validation timeline', 'trim|required|numeric');
            $this->form_validation->set_rules('email_validate_template', 'Email validate template', 'trim|required');
            $this->form_validation->set_rules('remind_validation_timeline', 'Remind validation timeline', 'trim|required');
            $this->form_validation->set_rules('remind_email_validate_template', 'Remind email validate template', 'trim|required');
            $this->form_validation->set_rules('notification_history', 'Notification history', 'trim|required|numeric');
            $this->form_validation->set_rules('number_venue', 'Number venue', 'trim|required|numeric');
            $this->form_validation->set_rules('checkin_radius_set', 'Checkin Radius Set', 'trim|required|numeric');
            $this->form_validation->set_rules('cache_time_out', 'Cache time out', 'trim|required|numeric');
            $this->form_validation->set_rules('cache_page_count', 'Cache page count', 'trim|required|numeric');
            $this->form_validation->set_rules('cache_trending_card_time_out', 'Cache trending card time out', 'trim|required|numeric');
            $this->form_validation->set_rules('time_show_trending_card_again', 'Time to show trending card again', 'trim|required|numeric');
            $this->form_validation->set_rules('trending_tag_interval', 'Trending card search interval', 'trim|required|numeric');
            $this->form_validation->set_rules('admin_email', 'Admin email addresses', 'trim|required');
            $this->form_validation->set_rules('onboarding_hashtag', 'Onboarding Hashtag', 'trim|required');
            
            $this->form_validation->set_rules('vegas_lat', 'Vegas Lat', 'trim|required|numeric');
            $this->form_validation->set_rules('vegas_lon', 'Vegas Lon', 'trim|required|numeric');
            $this->form_validation->set_rules('vegas_radius', 'Vegas Radius', 'trim|required|numeric');
            $this->form_validation->set_rules('max_point_per_day', 'Maximum of # pts per day', 'trim|required|numeric');
            $this->form_validation->set_rules('match_user_limit', '(Daily Match) Limit of # matched users', 'trim|required|numeric');
            $this->form_validation->set_rules('match_active_day', '(Daily Match) num of user\'s active days', 'trim|required|numeric');
            $this->form_validation->set_rules('match_icebreaker_require', '(Daily Match) num of user\'s ice breaker required', 'trim|required|numeric');
            $this->form_validation->set_rules('match_repeat_limitation', '(Daily Match) get match limitation', 'trim|required|numeric');
            $this->form_validation->set_rules('match_no_response_limit', '(Daily Match) get match limitation (Users who get no response within limitation hours)', 'trim|required|numeric');
            $this->form_validation->set_rules('match_minimum_interest', '(Daily Match) Minimum of shared interest', 'trim|required|numeric');
            $this->form_validation->set_rules('match_minimum_question', '(Daily Match) Minimum of shared icebreaker question', 'trim|required|numeric');
            $this->form_validation->set_rules('match_minimum_major', '(Daily Match) Minimum of shared major', 'trim|required|numeric');
            
            $this->form_validation->set_rules('user_id_contest_winner', 'UserID of contest winners (list UserID separated with a comma symbol)', 'trim');
            $this->form_validation->set_rules('match_repeat_limitation_contest_winner', '(Daily Match) get match limitation (for contest winners)', 'trim|numeric');
            $this->form_validation->set_rules('start_date_contest_winner', 'Contest time ', 'trim');
            $this->form_validation->set_rules('end_date_contest_winner', 'Contest time ', 'trim');
            $this->form_validation->set_rules('EmailValidationInterval', 'EmailValidationInterval ', 'trim|required|numeric');
            $this->form_validation->set_rules('LIMIT_TOPIC_PER_USER', 'Limit topic per user ', 'trim|required|numeric');
            
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/config_form', $data);
            } else {
                $value[] = array(
                    'Key' => 'SEARCH_RADIUS',
                    'Value' => $_POST['radius']);
                $value[] = array(
                    'Key' => 'SEARCH_RADIUS_KEYWORD',
                    'Value' => $_POST['radius_keyword']);
                $value[] = array(
                    'Key' => 'VALIDATION_TIMELINE',
                    'Value' => $_POST['validation_timeline']);
                $value[] = array(
                    'Key' => 'EMAIL_VALIDATE_TEMPLATE',
                    'Value' => $_POST['email_validate_template']);
                $value[] = array(
                    'Key' => 'REMIND_VALIDATION_TIMELINE',
                    'Value' => $_POST['remind_validation_timeline']);
                $value[] = array(
                    'Key' => 'REMIND_EMAIL_VALIDATE_TEMPLATE',
                    'Value' => $_POST['remind_email_validate_template']);
                $value[] = array(
                    'Key' => 'SEARCH_SERVICE_PROVIDER',
                    'Value' => $_POST['search_service_provider']);
                $value[] = array(
                    'Key' => 'NUMBER_VENUE',
                    'Value' => $_POST['number_venue']);
                $value[] = array(
                    'Key' => 'NOTIFICATION_HISTORY_RETURN_WITHIN',
                    'Value' => $_POST['notification_history']);
                $value[] = array(
                    'Key' => 'PUSH_NOTIFICATION_MODE',
                    'Value' => $_POST['push_notification_mode']);
                $value[] = array(
                    'Key' => 'NUMBER_MARK_LIMIT',
                    'Value' => $_POST['number_mark_limit']);
                $value[] = array(
                    'Key' => 'RANKBY',
                    'Value' => $_POST['rankby']);
                $value[] = array(
                    'Key' => 'NUMBER_ANOMO_PICK',
                    'Value' => $_POST['number_anomo_pick']);
                $value[] = array(
                    'Key' => 'TIME_USE_WITHOUT_FB',
                    'Value' => $_POST['time_use_without_fb']);
                $value[] = array(
                    'Key' => 'ACTIVITY_FEED',
                    'Value' => $_POST['activity_feed']);
                $value[] = array(
                    'Key' => 'SEARCH_RADIUS_SET',
                    'Value' => $_POST['search_radius_set']);
                $value[] = array(
                    'Key' => 'SEARCH_CUTOFF_VAL',
                    'Value' => $_POST['search_cutoff_val']);
                $value[] = array(
                    'Key' => 'SEARCH_TIME_LIMIT',
                    'Value' => $_POST['search_time_limit']);
                 $value[] = array(
                    'Key' => 'CHECKIN_RADIUS_SET',
                    'Value' => $_POST['checkin_radius_set']);
//                  $value[] = array(
//                    'Key' => 'FILTER_DISTANCE',
//                    'Value' => $_POST['filter_distance']);
                $value[] = array(
                    'Key' => 'USERNAME_SEND_MSG',
                    'Value' => $_POST['username_send_msg']);
                $value[] = array(
                    'Key' => 'TIME_POPUP_SHARE_FB',
                    'Value' => $_POST['time_popup_share_fb']);
                $value[] = array(
                    'Key' => 'TIME_POPUP_CONVERT_ACCOUNT',
                    'Value' => $_POST['time_popup_convert_account']);
                $value[] = array(
                    'Key' => 'NEW_USER_ACTIVITY_CARD',
                    'Value' => $_POST['new_user_activity_card']);
                $value[] = array(
                    'Key' => 'CACHE_TIME_OUT',
                    'Value' => $_POST['cache_time_out']);
                $value[] = array(
                    'Key' => 'CACHE_PAGE_COUNT',
                    'Value' => $_POST['cache_page_count']);
                $value[] = array(
                    'Key' => 'CACHE_TRENDING_CARD_TIME_OUT',
                    'Value' => $_POST['cache_trending_card_time_out']);
                
                $value[] = array(
                    'Key' => 'TIME_SHOW_TRENDING_CARD_AGAIN',
                    'Value' => $_POST['time_show_trending_card_again']);

                $value[] = array(
                    'Key' => 'TRENDING_TAG_INTERVAL',
                    'Value' => $_POST['trending_tag_interval']);
                $value[] = array(
                    'Key' => 'ADMIN_EMAIL',
                    'Value' => $_POST['admin_email']);
                $value[] = array(
                    'Key' => 'TURNON_VEGAS_HASHTAG',
                    'Value' => $_POST['turnon_vegas_hashtag']);
                
                $value[] = array(
                    'Key' => 'ONBOARDING_HASHTAG',
                    'Value' => $_POST['onboarding_hashtag']);
                
                $value[] = array(
                    'Key' => 'VEGAS_LAT',
                    'Value' => $_POST['vegas_lat']);
                $value[] = array(
                    'Key' => 'VEGAS_LON',
                    'Value' => $_POST['vegas_lon']);
                $value[] = array(
                    'Key' => 'VEGAS_RADIUS',
                    'Value' => $_POST['vegas_radius']);
                
                $value[] = array(
                    'Key' => 'MINGLE_ACTIVE_DAY',
                    'Value' => $_POST['mingle_active_day']);
                
                $value[] = array(
                    'Key' => 'MAX_POINT_PER_DAY',
                    'Value' => $_POST['max_point_per_day']);
                    
                $value[] = array(
                    'Key' => 'MATCH_USER_LIMIT',
                    'Value' => $_POST['match_user_limit']);
                    
                $value[] = array(
                    'Key' => 'MATCH_ACTIVE_DAY',
                    'Value' => $_POST['match_active_day']);
                    
                $value[] = array(
                    'Key' => 'MATCH_ICEBREAKER_REQUIRE',
                    'Value' => $_POST['match_icebreaker_require']);
                    
                $value[] = array(
                    'Key' => 'MATCH_REPEAT_LIMITATION',
                    'Value' => $_POST['match_repeat_limitation']);
                $value[] = array(
                    'Key' => 'MATCH_NO_RESPONSE_LIMIT',
                    'Value' => $_POST['match_no_response_limit']);
                    
                $value[] = array(
                    'Key' => 'MATCH_MINIMUM_INTEREST',
                    'Value' => $_POST['match_minimum_interest']);
                
                 $value[] = array(
                    'Key' => 'MATCH_MINIMUM_QUESTION',
                    'Value' => $_POST['match_minimum_question']);
                 
                 $value[] = array(
                    'Key' => 'USERID_CONTEST_WINNER',
                    'Value' => $_POST['user_id_contest_winner']);
                 $value[] = array(
                    'Key' => 'MATCH_REPEAT_LIMITATION_CONTEST_WINNER',
                    'Value' => $_POST['match_repeat_limitation_contest_winner']);
                 $value[] = array(
                    'Key' => 'START_DATE_CONTEST_WINNER',
                    'Value' => $_POST['start_date_contest_winner']);
                 $value[] = array(
                    'Key' => 'END_DATE_CONTEST_WINNER',
                    'Value' => $_POST['end_date_contest_winner']);
                 
                 $value[] = array(
                    'Key' => 'UsePromoCode',
                    'Value' => $_POST['use_promo_code']);
                 
                 $value[] = array(
                    'Key' => 'MIN_TOTAL_POST',
                    'Value' => $_POST['min_total_post']);
                 
                 $value[] = array(
                    'Key' => 'MIN_TOTAL_POPULAR',
                    'Value' => $_POST['min_total_popular']);
                 $value[] = array(
                    'Key' => 'EmailValidationInterval',
                    'Value' => $_POST['EmailValidationInterval']);
                 $value[] = array(
                    'Key' => 'LIMIT_TOPIC_PER_USER',
                    'Value' => $_POST['LIMIT_TOPIC_PER_USER']);
                 $value[] = array(
                    'Key' => 'MATCH_MINIMUM_MAJOR',
                    'Value' => $_POST['match_minimum_major']);
                
                $this->Config_model->add($value);
                redirect('admin/config/p_update/Updated!!!');
            }
        } else {
            $this->load->view('admin/config_form', $data);
        }
    }

}

?>