<?php

class Report extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Dashboard_model');
    }

    function index() {
        $data = array();
        if (isset($_POST['from']) && isset($_POST['to'])) {
            $data = $_POST;
            if (trim($_POST['from']) != '' && trim($_POST['to']) != '' && strtotime($_POST['to']) >= strtotime($_POST['from'])) {
                $from = $_POST['from'];
                $to = $_POST['to'];
                               
                $from = date('Y-m-d H:i:s', strtotime($from . ' 00:00:00'));                
                $to = date('Y-m-d H:i:s', strtotime($to . ' 23:59:59'));
                        
                //$result['register'] = $this->Dashboard_model->totalUserRegister($from, $to);
                //$result['verified'] = $this->Dashboard_model->totalUserRegister($from, $to, 'verified');
                //$result['unverified'] = $this->Dashboard_model->totalUserRegister($from, $to, 'unverified');

                $result['devices'] = $this->Dashboard_model->totalByDevices($from, $to, 'all');
                $result['ios'] = $this->Dashboard_model->totalByDevices($from, $to, 'ios');
                $result['android'] = $this->Dashboard_model->totalByDevices($from, $to, 'android');

                $result['game'] = $this->Dashboard_model->totalOther($from, $to, 'Anomotion');
                $result['gift'] = $this->Dashboard_model->totalOther($from, $to, 'GiftSend');
                $result['picture'] = $this->Dashboard_model->totalOther($from, $to, 'PicturePostActivity');
                $result['like'] = $this->Dashboard_model->totalOther($from, $to, 'ActivityLike');
                $result['comment'] = $this->Dashboard_model->totalOther($from, $to, 'ActivityComment');
                $result['total'] = $this->Dashboard_model->totalUserRegister('', $to);
                
                //new user
                $result['newuser'] = $this->Dashboard_model->totalUserRegister($from, $to);
                $result['newuser_sms'] = $this->Dashboard_model->totalNewUserVerify($from, $to,'sms');
                $result['newuser_facebook'] = $this->Dashboard_model->totalNewUserVerify($from, $to,'facebook');
                $result['newuser_manual'] = $this->Dashboard_model->totalNewUserVerify($from, $to,'manual');
                $result['newuser_unverified'] = $this->Dashboard_model->totalNewUserVerify($from, $to,'unverified');
                
                
                
                $result['match'] = $this->Dashboard_model->getTotalMatch($from,$to,0);
                $result['match_success'] = $this->Dashboard_model->getTotalMatch($from,$to,1);

                $data['result'] = $result;
            }
        }
        $this->load->view('admin/report', $data);
    }

    function add_metrics() {
        if (isset($_POST['submit'])) {
            $data = array(
                'From' => $_POST['from'],
                'To' => $_POST['to'],
                'Android' => $_POST['android'],
                'Ios' => $_POST['ios'],
                'CreatedDate' => gmdate('Y-m-d H:i:s')
            );

            $id = $this->Dashboard_model->addMetrics($data);
            redirect('admin/report/metrics');
        }
    }

    function metrics() {
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $config['base_url'] = base_url() . 'admin/report/metrics';
        $config['per_page'] = 50;
        $config['uri_segment'] = 4;
        $total = 0;
        $result = $this->Dashboard_model->getListMetrics($total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['result'] = $result;
        $this->load->view('admin/metrics', $data);
    }
    
    function top_like(){
        $data = array();
        if ((isset($_POST['day']) && trim($_POST['day']) != '') || (isset($_POST['hashtag']) && trim($_POST['hashtag']) != '')) {
            $data = $_POST;
            $result = $this->Dashboard_model->getTopLike($data);
            $data['result'] = $result;
        }
        $this->load->view('admin/top_like', $data);
    }

}

?>