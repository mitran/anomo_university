<?php
class Sticker extends Ext_Controller{
    function __construct() {
        parent::__construct();
        $this->load->library('Pagination');
        $this->load->model('Sticker_model');
        $this->load->library('S3Upload');
        $this->load->library('form_validation');
    }
    
    function list_sticker($cate_id = 0){
        $this->load->library('Pagination');
        $config['base_url'] = base_url() . 'admin/sticker/list_sticker/'.$cate_id;
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $total = 0;
        $data['results'] = $this->Sticker_model->getListStickerByCond($cate_id, $total, (int)$this->uri->segment(5), $config['per_page']);
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['cate_id'] = $cate_id;
        $this->load->view('admin/sticker_list', $data);
    }
    
    function list_sticker_category(){
        $data['results'] = $this->Sticker_model->listCategory();
        $this->load->view('admin/sticker_category_list', $data);
    }

    function add_sticker_category($id = 0) {
        $data['id'] = $id;
        if ($id > 0) {
            $info = $this->Sticker_model->getStickerCategoryInfo($id);
            $data['info'] = $info;
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('name', 'Name', "trim|required|xss_clean");
            $upload['error'] = false;
            if ($id == 0 || ($id > 0 && $_FILES['photo']['name'] != '')) {
                $isUpload = $this->s3upload->upload('photo', '', BUCKET_NAME_STICKER);
                if (!$isUpload) {
                    $upload['error'] = true;
                    $upload['error_msg'] = $this->s3upload->error;
                } else {
                    $upload['file_name'] = $isUpload;
                }
            }

            if ($this->form_validation->run() == FALSE || $upload['error']) {
                if ($upload['error']) {
                    $data['upload_error'] = $upload['error_msg'];
                }
                $this->load->view('admin/sticker_category_form', $data);
            } else {
                if ($id == 0) {
                    $icon = $upload['file_name'];
                } else {
                    if (isset($upload['file_name']) && $upload['file_name'] != '') {
                        $icon = $upload['file_name'];
                    } else {
                        $icon = $info->Photo;
                    }
                }
                $value = array(
                    'Name' => $_POST['name'],
                    'Photo' => $icon,
                    'CreatedDate'   => gmdate('Y-m-d H:i:s')
                );
                if ($id == 0) {
                    $this->Sticker_model->addStickerCategory($value);
                } else {
                    $this->Sticker_model->updateStickerCategory($value, array('CateID' => $id));
                }
                redirect('admin/sticker/list_sticker_category');
            }
        } else {
            $this->load->view('admin/sticker_category_form', $data);
        }
    }
    
    function add_sticker($cate_id = 0, $id = 0) {
        $categories = $this->Sticker_model->getListCategory();
        $cate = array();
        if ($categories){
            foreach($categories as $row){
                $cate[$row->CateID] = $row->Name;
            }
        }
        $data['cate'] = $cate;
        $data['cate_id'] = $cate_id;
        $data['id'] = $id;
        if ($id > 0) {
            $info = $this->Sticker_model->getStickerInfo($id);
            $data['info'] = $info;
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('cate_id', 'Category', "required");
            $upload['error'] = false;
            if ($id == 0 || ($id > 0 && $_FILES['photo']['name'] != '')) {
                $isUpload = $this->s3upload->upload('photo', '', BUCKET_NAME_STICKER);
                if (!$isUpload) {
                    $upload['error'] = true;
                    $upload['error_msg'] = $this->s3upload->error;
                } else {
                    $upload['file_name'] = $isUpload;
                }
            }

            if ($this->form_validation->run() == FALSE || $upload['error']) {
                if ($upload['error']) {
                    $data['upload_error'] = $upload['error_msg'];
                }
                $this->load->view('admin/sticker_form', $data);
            } else {
                if ($id == 0) {
                    $icon = $upload['file_name'];
                } else {
                    if (isset($upload['file_name']) && $upload['file_name'] != '') {
                        $icon = $upload['file_name'];
                    } else {
                        $icon = $info->Photo;
                    }
                }
                $cate_id =  isset($_POST['cate_id']) ? $_POST['cate_id']: $cate_id;
                $value = array(
                    'Photo' => $icon,
                    'CateID' => $cate_id
                );
                if ($id == 0) {
                    $value['CreatedDate'] = gmdate('Y-m-d H:i:s');
                    $this->Sticker_model->addSticker($value);
                } else {
                    $this->Sticker_model->updateSticker($value, array('ID' => $id));
                }
                redirect('admin/sticker/list_sticker/'.$cate_id);
            }
        } else {
            $this->load->view('admin/sticker_form', $data);
        }
    }
    
    function increase_version($cate_id = 0){
        if ($cate_id > 0){
            $this->Sticker_model->increaseVersion($cate_id);
            redirect('admin/sticker/list_sticker_category');
        }
    }
}
?>
