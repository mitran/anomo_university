<?php

class Gift_Category extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Gift_Category_model');
        $this->load->helper('Form');
    }

    /**
     * list gift category page
     *
     */
    function index() {
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $cond = array();
        if (isset($this->uri->vn_param['sort'])) {
            $cond['sort'] = $this->uri->vn_param['sort'];
        }
        $config['base_url'] = base_url() . 'admin/gift_category/index';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $config['total_rows'] = $this->Gift_Category_model->getListByCondition($cond, $config['per_page'], $this->uri->segment(4), true);
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['results'] = $this->Gift_Category_model->getListByCondition($cond, $config['per_page'], $this->uri->segment(4), false);
        $this->load->view('admin/gift_category_list', $data);
    }

    /**
     * add/edit gift category
     *
     * @param int $gift_category_id
     */
    function post($gift_category_id = 0) {
        $data['gift_category_id'] = $gift_category_id;
        if ($gift_category_id > 0) {
            $info = $this->Gift_Category_model->getInfo($gift_category_id);
            $data['info'] = $info;
        }
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/gift_category_form', $data);
            } else {
                $value = array(
                    'Name' => $_POST['name']
                );
                if ($gift_category_id == 0) {
                    $this->Gift_Category_model->add($value);
                } else {
                    $this->Gift_Category_model->update($value, array('ID' => $gift_category_id));
                }
                redirect('admin/gift_category');
            }
        } else {
            $this->load->view('admin/gift_category_form', $data);
        }
    }

}

?>