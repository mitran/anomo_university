<?php
class Flag extends Ext_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('Flag_model');
		$this->load->model('Flag_Content_model');
		$this->load->model('Activity_model');
                $this->load->model('Comment_model');
                $this->load->model('Dashboard_model');
		$this->load->helper('Form');
		$this->load->library('Pagination');
		$this->load->helper('Ext_URI');
	}
	
	/**
	 * report user
	 *
	 */
	function index(){
		$cond = array();
                if (isset($this->uri->vn_param['flag'])) {
                    $cond['flag'] = $this->uri->vn_param['flag'];
                }
		$config['base_url'] = base_url().'admin/flag/index';
		$config['per_page'] = 50;
		$config['uri_segment'] = 4;
		$total = 0;
		$results = $this->Flag_model->getListByCondition($cond, $total, $config['per_page'], (int)$this->uri->segment(4));
		$config['total_rows'] = $total;
		$this->pagination->initialize($config);
		$data['total'] = $total;
		$data['results'] = $results;
		$this->load->view('admin/flag_user', $data);
	}
	
	/**
	 * report content
	 *
	 */
	function content(){
		$types = array(
			0 => 'Check in',
			1 => 'User change status',
			2 => 'Post picture to place',
			7 => 'Post picture to nbh',
			27 => 'Post picture status',
			6 => 'Post text to place',
			8 => 'Post text to neighborhood'
		);
		$cond = array();
                if (isset($this->uri->vn_param['flag'])) {
                    $cond['flag'] = $this->uri->vn_param['flag'];
                }
		$config['base_url'] = base_url().'admin/flag/content';
		$config['per_page'] = 50;
		$config['uri_segment'] = 4;
		$total = 0;
		$results = $this->Flag_Content_model->getListByCondition($cond, $total, $config['per_page'], (int)$this->uri->segment(4));
		$config['total_rows'] = $total;
		$this->pagination->initialize($config);
		$data['total'] = $total;
		$data['results'] = $results;
		$data['types'] = $types;
                $data['current_offset'] = (int)$this->uri->segment(4);
		$this->load->view('admin/flag_content', $data);
	}
	
    function delete_content($id, $type, $is_comment = 0, $current_offset=0) {
        if ($is_comment == 0) {
            $this->load->model('Picture_Post_Activity_model');
            $this->load->model('User_Profile_Status_model');
            $contentInfo = $this->Activity_model->getContentInfo($id, $type);
            if ($contentInfo) {
                switch ($type) {
                    case 1: // change profile status
                        $this->User_Profile_Status_model->deleteStatus($id, $contentInfo->UserID);
                        break;
                    case 27: // picture status
                        $this->Picture_Post_Activity_model->deletePicStatus($id, $contentInfo->UserID);
                        break;
                    default:
                        break;
                }
                $data['msg'] = 'Delete successful';
            }
        } else {
            // delete comment
            $commentInfo = $this->Activity_model->getComment($id, $type);
            if ($commentInfo) {
                $this->Activity_model->increase($commentInfo->ContentID, $type, 'comment', 'decrease');
                $this->Comment_model->deleteComment($id);
                $data['msg'] = 'Delete successful';
            }
        }
        redirect('admin/flag/content/'.$current_offset.'/p_delete/' . $data['msg']);
    }
}
?>