<?php

class UserStatus extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('User_Profile_Status_model');
        $this->load->model('Activity_model');
        $this->load->helper('Form');
        $this->load->library('Pagination');
        $this->load->model('Dashboard_model');
        $this->load->model('Video_model');
        $this->load->helper('Ext_URI');
    }

    function index() {
        $cond = array();
        if (isset($this->uri->vn_param['sort'])) {
            $cond['sort'] = $this->uri->vn_param['sort'];
        }
        
        if (isset($_POST['btn_submit'])){
            $type = isset($_POST['type'])?$_POST['type']:'';
            $keyword = isset($_POST['keyword'])?$_POST['keyword']:'';
            if ($type != '' && $keyword != ''){
                $this->session->set_userdata('txt_type', $type);
                $this->session->set_userdata('txt_keyword', $keyword);
                $data['type'] = $type;
                $data['keyword'] = $keyword;
                if ($type == 'content'){
                    $cond['keyword'] = $keyword;
                }elseif($type == 'user_id'){
                    $cond['user_id'] = $keyword;
                }
            }
        }elseif ((int) $this->uri->segment(4) > 0){
            if ($this->session->userdata('txt_type')){
                $type = $this->session->userdata('txt_type');
                $keyword = $this->session->userdata('txt_keyword');
                $data['type'] = $type;
                $data['keyword'] = $keyword;
                if ($type == 'content'){
                    $cond['keyword'] = $keyword;
                }elseif($type == 'user_id'){
                    $cond['user_id'] = $keyword;
                }
            }
        }else{
            $this->session->unset_userdata('txt_type');
            $this->session->unset_userdata('txt_keyword');
        }

        $config['base_url'] = base_url() . 'admin/userstatus/index';
        $config['per_page'] = 50;
        $config['uri_segment'] = 4;
        $total = 0;
        $results = $this->User_Profile_Status_model->getListStatus($cond, $total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $total;
        $data['results'] = $results;
        $this->load->view('admin/user_status', $data);
    }

    function delete_content($content_id = 0 , $type = 0, $redirect_page = 0) {
        $contentInfo = $this->Activity_model->getContentDetail($content_id, $type);
        $ret = $this->Activity_model->deleteActivity($content_id, $type, 0);
        if ($ret){
            //Decrease Topic's total post
            if ($contentInfo && $contentInfo->TopicID){
                $this->Topic_model->decreasePost($contentInfo->TopicID);
            }
            $url = $redirect_page == 1? 'admin/userstatus/index/': 'admin/picture/status_pics/';
        }
        redirect($url. 'p_delete/1');
    }
    
    function video() {
        $cond = array();
        if (isset($this->uri->vn_param['sort'])) {
            $cond['sort'] = $this->uri->vn_param['sort'];
        }
        
        if (isset($_POST['btn_submit'])){
            $keyword = isset($_POST['keyword'])?$_POST['keyword']:'';
            if ($keyword != ''){
                $this->session->set_userdata('txt_keyword', $keyword);
                $data['keyword'] = $keyword;
                $cond['keyword'] = $keyword;
            }
        }elseif ((int) $this->uri->segment(4) > 0){
            if ($keyword = $this->session->userdata('txt_keyword')){
                $data['keyword'] = $keyword;
                $cond['keyword'] = $keyword;
            }
        }else{
            $this->session->unset_userdata('txt_keyword');
        }

        $config['base_url'] = base_url() . 'admin/userstatus/video';
        $config['per_page'] = 10;
        $config['uri_segment'] = 4;
        $total = 0;
        $results = $this->Video_model->getListVideo($cond, $total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $total;
        $data['results'] = $results;
        $this->load->view('admin/video', $data);
    }

}

?>