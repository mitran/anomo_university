<?php

class Announcement extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('Form');
        $this->load->model('Announcement_model');
    }

    /**
     * list gift category page
     *
     */
    function index() {
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $config['base_url'] = base_url() . 'admin/announcement/index';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $total = 0;
        $data['results'] = $this->Announcement_model->getListByCondition($total, (int)$this->uri->segment(4), $config['per_page']);
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $this->load->view('admin/announcement_list', $data);
    }

    /**
     * add/edit gift category
     *
     * @param int $gift_category_id
     */
    function post($id = 0) {
        $data['id'] = $id;
        if ($id > 0) {
            $info = $this->Announcement_model->getInfo($id);            
            // 2014-02-11
            // for expire field: input as PST time so
            // need convert this time to GMT to save into db
            $expire_date = $info->ExpireDate;
            $expire_date_pst = date('Y-m-d', strtotime('-7 hour', strtotime($expire_date)));
            $info->ExpireDate = $expire_date_pst;
            $data['info'] = $info;

        }
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            //  a date, title, body, and an expiration date.
            $this->form_validation->set_rules('type', 'Type', 'trim|required');
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('content', 'Content', 'trim|required');
            $this->form_validation->set_rules('expire_date', 'Expire Date', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/announcement_form', $data);
            } else {
                // 2014-02-11
                // for expire field: input as PST time so
                // need convert this time to GMT to save into db
                $expire_date = $_POST['expire_date']." 23:59:59";
                $expire_date_gmt = date('Y-m-d H:i:s', strtotime('+7 hour', strtotime($expire_date)));
                $value = array(
                    'Title' => $_POST['title'],
                    'Content'   => $_POST['content'],
                    'Type'  => $_POST['type'],
                    'ExpireDate'    => $expire_date_gmt,
                    'RefType' => $_POST['reftype'],
                    'RefID' => $_POST['refid']
                       
                );
                if ($id == 0) {
                     // Selecting Both will automatically create a New User and Old User card
                    if ($_POST['type'] == 0){
                        $value = array();
                        $created_date = gmdate('Y-m-d H:i:s');
                        $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
                        $title = mysqli_real_escape_string($con, $_POST['title']);
                        $content = mysqli_real_escape_string($con, $_POST['content']);
                        $reftype = mysqli_real_escape_string($con, $_POST['reftype']);
                        $refid = mysqli_real_escape_string($con, $_POST['refid']);
                        
                        
                        $value[] = "('".$title."', '".$content."', '1', '".$expire_date_gmt."', '".$created_date."','".$reftype."','".$refid."')";
                        $value[] = "('".$title."', '".$content."', '2', '".$expire_date_gmt."', '".$created_date."','".$reftype."','".$refid."')";
                        $this->Announcement_model->addMulti($value);
                    }else{
                        $value['CreatedDate'] = gmdate('Y-m-d H:i:s');
                        $this->Announcement_model->add($value);
                    }   
                } else {
                    $this->Announcement_model->update($value, array('ID' => $id));
                }
                redirect('admin/announcement');
            }
        } else {
            $this->load->view('admin/announcement_form', $data);
        }
    }
    
    function delete($id){
        $info = $this->Announcement_model->getInfo($id);
        if (count($info) > 0) {
            $this->Announcement_model->delete($id);
            $data['msg'] = 'Delete announcement successful';
        } else {
            $data['msg'] = "Announcement not exists";
        }
        redirect('admin/announcement/index/p_delete/' . $data['msg']);
    }
    
    function closed(){
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $config['base_url'] = base_url() . 'admin/announcement/closed';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $total = 0;
        $data['results'] = $this->Announcement_model->getListAnnouncementClosed($total, (int)$this->uri->segment(4), $config['per_page']);
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $this->load->view('admin/announcement_closed', $data);
    }

}

?>
