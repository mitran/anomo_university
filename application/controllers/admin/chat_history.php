<?php

class Chat_History extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('pubnub');
    }

    function index($user_id = 0) {
        $prefix = '';
        $is_production = PRODUCTION;
        if (!$is_production) {
            $prefix = 'dbg_';
        }
        
        if (!is_numeric($user_id) || $user_id < 0){
            return false;
        }
        $data = array();
        
        $args = array(
            'channel' => $prefix.$user_id,
            'count' => 100
        );
        $histories = $this->pubnub->detailedHistory($args);

        $details = array();
        $conversations = array();
        $last_time = '';
        if ($histories){
            $messages = isset($histories[0])?$histories[0]:false;
            $last_time = isset($histories[1])?$histories[1]:'';
            if ($messages){
                
                foreach($messages as $msg){
                    $partner_id = $msg['receiver_id'] == $user_id?$msg['sender_id']:$msg['receiver_id'];
                    $partner_name = $msg['receiver_id'] == $user_id?$msg['sender_name']:$msg['receiver_name'];
                    $conversations[$partner_id] = $partner_name;                 
                    $details[] = array(
                        'user_id'   => $user_id,
                        'partner_id'    => $partner_id,
                        'sender_name' => $msg['sender_name'],
                        'text' => $msg['text'],
                        'photo' => isset($msg['photo'])?$msg['photo']:'',
                        'time'  => date('M d, Y g:i:s A', strtotime("-7 hour", $msg['time']/1000 ))
                    );
                }
            }
        }

        $data['last_time'] = $last_time;
        $data['conversations'] = $conversations;
        $data['details'] = $details;
        $data['user_id'] = $user_id;
        $this->load->view('admin/chat_history', $data);
    }
    
    function load_more($user_id, $last_time = ''){

        $prefix = '';
        $is_production = PRODUCTION;
        if (!$is_production) {
            $prefix = 'dbg_';
        }
        
        if (!is_numeric($user_id) || $user_id < 0){
            return false;
        }
        $data = array();
        $args = array(
            'channel' => $prefix.$user_id,
            'count' => 100,
            'start' => $last_time, 
        );

        $histories = $this->pubnub->detailedHistory($args);
        $details = array();
        $conversations = array();
        $last_time = '';
        $html_detail = '';
        if ($histories){
            $messages = isset($histories[0])?$histories[0]:false;
            $last_time = isset($histories[1])?$histories[1]:'';
            if ($messages){
                
                foreach($messages as $msg){
                    $partner_id = $msg['receiver_id'] == $user_id?$msg['sender_id']:$msg['receiver_id'];
                    $partner_name = $msg['receiver_id'] == $user_id?$msg['sender_name']:$msg['receiver_name'];
                    $conversations[$partner_id] = array(
                        'partner_id' => $partner_id,
                        'partner_name' => $partner_name);
                    
                    // generate html
                    $html_detail .= "
                    <div class='".$user_id."_".$partner_id."' style='display:none;'>   
                        <div class='msgs'>   
                            <span id='username' class='username'>". $msg['sender_name']. ":</span>
                            <span class='msg'>
                    "; 
                    if (isset($msg['photo']) && $msg['photo'] != ''){
                        $html_detail .= " <a href='".$msg['photo']."' data-lightbox='". $msg['photo']."'  >";                 
                        $html_detail .= "<img src='".$msg['photo']."' width='50px'>";
                        $html_detail .= "</a>";
                    }else{
                        $html_detail .= $msg['text'];
                    }
                    $html_detail .= "</span>";
                    $html_detail .= "<span class='time'>".date('M d, Y g:i:s A', strtotime("-7 hour", $msg['time']/1000 ))."</span>";
                    $html_detail .= "</div></div>";
                }
            }
        }

        $data['last_time'] = $last_time;
        $data['conversations'] = array_values($conversations);
        $data['details'] = $html_detail;
        echo json_encode($data);
    }
}

?>
