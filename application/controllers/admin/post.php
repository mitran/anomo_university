<?php

class Post extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $this->load->model('Topic_model');
        $this->load->model('Dashboard_model');
        $this->load->model('Activity_model');
    }

    function index($topic_id = 0) {
        $cond['TopicID'] = $topic_id;
        $config['base_url'] = base_url() . 'admin/post/index/'.$topic_id;
        $config['per_page'] = 50;
        $config['uri_segment'] = 5;
        $total = 0;
        $results = $this->Topic_model->getListPostInTopic($cond, $total, $config['per_page'], (int) $this->uri->segment(5));

        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $total;
        $data['results'] = $results;
        $this->load->view('admin/post_list', $data);
    }

    function delete_content($activity_id = 0) {
        $activityInfo = $this->Activity_model->getTopicInfoByActivityID($activity_id);
        if ($activityInfo){
            $ret = $this->Activity_model->deleteActivity($activityInfo->RefID, $activityInfo->Type, 0);
            if ($ret){
                //Decrease Topic's total post
                if ($activityInfo && $activityInfo->TopicID) {
                    $this->Topic_model->decreasePost($activityInfo->TopicID);
                }
                redirect('admin/post/index/'.$activityInfo->TopicID.'/p_delete/1');
            }
        }
        
    }

}

?>