<?php
class blocked_users extends Ext_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('blocked_users_model');
	}
	function index(){
		set_time_limit(0);
		$data = array();
		$data['all']  = $this->blocked_users_model->getBlockedUsers('all');
		$value['data'] = $data;
		$this->load->view('admin/blocked_users', $value);
	}

	function last7days(){
		set_time_limit(0);
		$data = array();
		$data['week']  = $this->blocked_users_model->getBlockedUsers('week');
		$value['data'] = $data;
		$this->load->view('admin/blocked_users_last7days', $value);	
	}
		
	function customperiod(){
		set_time_limit(0);
		if ($_POST) {
			$dates = array(
			'StartDate' =>  $_POST['startdate'],
			'EndDate'  => $_POST['enddate']
			);	
			$data['custom']  = $this->blocked_users_model->getBlockedUsers('all',$dates['StartDate'],$dates['EndDate']);
			$value['data'] = $data;
			$this->load->view('admin/blocked_users_custom_period_table', $value);
		}
		else{
			$this->load->view('admin/blocked_users_custom_period',$value);
		}
	}

}

?>