<?php

class Version extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Version_model');
        $this->load->helper('Form');
    }

    /**
     * list credit page
     *
     */
    function index() {
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $cond = array();
        if (isset($this->uri->vn_param['sort'])) {
            $cond['sort'] = $this->uri->vn_param['sort'];
        }
        $config['base_url'] = base_url() . 'admin/version/index';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $config['total_rows'] = $this->Version_model->getListByCondition($cond, $config['per_page'], $this->uri->segment(4), true);
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['results'] = $this->Version_model->getListByCondition($cond, $config['per_page'], $this->uri->segment(4), false);
        $this->load->view('admin/version_list', $data);
    }

    /**
     * add/edit version
     *
     * @param int $version_id
     */
    function post($version_id = 0) {
        $data['version_id'] = $version_id;
        if ($version_id > 0) {
            $info = $this->Version_model->getInfo($version_id);
            $data['info'] = $info;
        }
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('platform', 'Platform', 'required'); 
            $this->form_validation->set_rules('app_version', 'AppVersion', 'trim|required'); 
            $this->form_validation->set_rules('is_latest', 'Is Latest', 'trim'); 
            $this->form_validation->set_rules('is_support', 'Is Support', 'trim'); 
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/version_form', $data);
            } else {
                $value = array(
                    'Platform' => $_POST['platform'],
                    'AppVersion' => $_POST['app_version'],
                    'IsLatest' => isset($_POST['is_latest']) ? 1 : 0,
                    'IsSupport' => isset($_POST['is_support']) ? 1 : 0
                );
                if ($version_id == 0) {
                    $this->Version_model->add($value);
                } else {
                    $this->Version_model->update($value, array('ID' => $version_id));
                }
                redirect('admin/version');
            }
        } else {
            $this->load->view('admin/version_form', $data);
        }
    }

}

?>