<?php

//include_once('library\Stomp.php');
class Message extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Message_model');
        $this->load->model('User_Profile_Status_model');
        $this->load->model('Activity_model');
        $this->load->model('User_model');
        $this->load->model('Devices_model');
        $this->load->helper('Form');
        $this->load->helper('Ext_URI');
        $this->load->library('Pagination');
    }

    function index() {
        echo get_include_path();
        $aData = array();

        //echo "hello world";
        $this->load->view('admin/message', $aData);
    }

    function getUsersCount() {
        //If pass validate, send the message
        $sMessage = $this->input->post('message');
        $iGender = $this->input->post('gender');
        $sApp_used = $this->input->post('app_used');
        $sAge = $this->input->post('age');

        $from = ($this->input->post('from') != '') ? $this->input->post('from') : '';
        $to = ($this->input->post('to') != '') ? $this->input->post('to') : '';

        if ($from != '' && $to != '') {
            $fromDate = new DateTime($this->input->post('from'));
            $toDate = new DateTime($this->input->post('to'));

            $fromDate = date_sub($fromDate, date_interval_create_from_date_string('7 hours'));
            $toDate = date_sub($toDate, date_interval_create_from_date_string('7 hours'));
            $toDate = date_add($toDate, date_interval_create_from_date_string('1 day'));

            $from = $fromDate->format('Y-m-d H:i:s');
            $to = $toDate->format('Y-m-d H:i:s');
        }

        //Get list user id
        $aCond = array(
            'gender' => $iGender,
            'app_used' => $sApp_used,
            'age'   => isset($_POST['age'])?$_POST['age']:'',
            'from' => $from,
            'to' => $to,
            'from_age'   => isset($_POST['from_age'])?$_POST['from_age']:0,
            'to_age'   => isset($_POST['to_age'])?$_POST['to_age']:0,
            'lat'   => isset($_POST['lat'])?$_POST['lat']:'',
            'lon'   => isset($_POST['lon'])?$_POST['lon']:'',
            'radius'    => isset($_POST['radius'])?$_POST['radius']:'',
        );
        $aCond['getTotal'] = 1;

        $numberUser = $this->User_model->getListUserID($aCond);
        echo $numberUser;
        die();
    }

    function send() {
        $aData = array();

        if (isset($_POST['requestsubmit'])) {
            //Validate form field
            $this->load->library('form_validation');
            $this->form_validation->set_rules('message', 'Message', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('admin/message_form', $aData);
            } else {
                //If pass validate, send the message
                $sMessage = $this->input->post('message');
                $iGender = $this->input->post('gender');
                $sApp_used = $this->input->post('app_used');
                $sAge = $this->input->post('age');

                $from = ($this->input->post('from') != '') ? $this->input->post('from') : '';
                $to = ($this->input->post('to') != '') ? $this->input->post('to') : '';

                if ($from != '' && $to != '') {
                    $fromDate = new DateTime($this->input->post('from'));
                    $toDate = new DateTime($this->input->post('to'));

                    $fromDate = date_sub($fromDate, date_interval_create_from_date_string('7 hours'));
                    $toDate = date_sub($toDate, date_interval_create_from_date_string('7 hours'));
                    $toDate = date_add($toDate, date_interval_create_from_date_string('1 day'));

                    $from = $fromDate->format('Y-m-d H:i:s');
                    $to = $toDate->format('Y-m-d H:i:s');
                }

                if ($sMessage) {
                    //Insert to table message
                    $aData = array(
                        'Message' => $sMessage,
                        'Status' => 0,
                        'CreatedDate' => gmdate('Y-m-d H:i:s')
                    );
                    $iID = $this->Message_model->add($aData);
                    if ($iID) {
                        //Get list user id
                        $aCond = array(
                            'gender' => $iGender,
                            'app_used' => $sApp_used,
                            'age' => $sAge,
                            'from' => $from,
                            'to' => $to
                        );
                        $iCount = 0;
                        $oUser = $this->User_model->getListUserID($aCond);
                        if ($oUser) {
                            set_time_limit(0); // ignore php timeout
                            ignore_user_abort(true); // keep on going even if user pulls the plug*
                            while (ob_get_level())
                                ob_end_clean(); // remove output buffers
                            ob_implicit_flush(true); // output stuff directly

                            $iCount = count($oUser);
                            foreach ($oUser as $key => $obj) {
                                //Get device information store in message tracking table
//                                $oDevice = $this->Devices_model->getDeviceOfUser($obj->UserID);
//                                if ($oDevice) {
                                $aDataTracking = array(
                                    'MessageID' => $iID,
                                    'UserID' => $obj->UserID,
                                    'Message' => $sMessage,
                                    'Status' => 0,
                                    'CreatedDate' => gmdate('Y-m-d H:i:s'),
                                    'DeviceID' => '', //$oDevice->DeviceID,
                                    'DeviceType' => '', //$oDevice->Type
                                );
                                $this->Message_model->addMessageTracking($aDataTracking);
//                                }
                            }
//                            $iTotalMsg = $this->Message_model->totalMsgTracking();
                        }

                        //Update total message tracking to message
                        $this->Message_model->updateStatus(array('TotalTracking' => $iCount), array('MessageID' => $iID));

                        $sMsg = "Send Successful";
                    } else {
                        $sMsg = "Send Fail";
                    }
                } else {
                    $sMsg = "Can't get message to send";
                }
                $this->session->set_flashdata('msg', $sMsg);

                redirect('admin/message/send');
            }
        } else {
            $this->load->view('admin/message_form', $aData);
        }
    }

    function listmsg() {
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $cond = array();
        $config['base_url'] = base_url() . 'admin/message/listmsg';
        $config['per_page'] = 50;
        $config['uri_segment'] = 4;
        $total = 0;
        $results = $this->Message_model->getListByCondition($cond, $total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $total;
        $data['results'] = $results;
        $this->load->view('admin/list_msg', $data);
    }

}

?>