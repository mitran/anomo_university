<?php

class Promo_Code extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Promo_Code_model');
        $this->load->helper('Form');
    }

    /**
     * list credit page
     *
     */
    function index() {
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $cond = array();
        if (isset($this->uri->vn_param['sort'])) {
            $cond['sort'] = $this->uri->vn_param['sort'];
        }
        if (isset($this->uri->vn_param['status'])) {
            $cond['status'] = $this->uri->vn_param['status'];
        }
        $config['base_url'] = base_url() . 'admin/promo_code/index';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $config['total_rows'] = $this->Promo_Code_model->getListByCondition($cond, $config['per_page'], $this->uri->segment(4), true);
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['results'] = $this->Promo_Code_model->getListByCondition($cond, $config['per_page'], $this->uri->segment(4), false);
        $this->load->view('admin/promo_code_list', $data);
    }

    /**
     * add/edit credit
     *
     * @param int $credit_id
     */
    function post($promo_code_id = 0) {
        $data['promo_code_id'] = $promo_code_id;
        if ($promo_code_id > 0) {
            $info = $this->Promo_Code_model->getInfo($promo_code_id);
            $data['info'] = $info;
        }
        if (isset($_POST['submit'])) {
            $this->load->library('validation');
            $rules['code'] = 'trim|required|xss_clean';
            $rules['name'] = 'trim|required|xss_clean';
            $rules['point'] = 'trim|required|numeric';
            $fields['code'] = 'Code';
            $fields['name'] = 'Name';
            $fields['point'] = 'Point';
            $this->validation->set_rules($rules);
            $this->validation->set_fields($fields);
            if ($this->validation->run() == FALSE) {
                $this->load->view('admin/promo_code_form', $data);
            } else {
                $value = array(
                    'Code' => $_POST['code'],
                    'Name' => $_POST['name'],
                    'Point' => $_POST['point'],
                    'Status' => $_POST['status'],
                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                );
                if ($promo_code_id == 0) {
                    $this->Promo_Code_model->add($value);
                } else {
                    $this->Promo_Code_model->update($value, array('ID' => $promo_code_id));
                }
                redirect('admin/promo_code');
            }
        } else {
            $this->load->view('admin/promo_code_form', $data);
        }
    }

    function delete($promo_code_id) {
        $info = $this->Promo_Code_model->getInfo($promo_code_id);
        if ($info) {
            // delete	
            $this->Promo_Code_model->delete($promo_code_id);
            redirect('admin/promo_code/p_delete/Successful!');
        } else {
            redirect('admin/promo_code/p_delete/Not delete!');
        }
    }

}

?>