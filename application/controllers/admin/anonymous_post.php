<?php

class Anonymous_Post extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('Form');
        $this->load->model('Anonymous_Post_model');
        $this->load->model('Dashboard_model');
    }

    /**
     * list gift category page
     *
     */
    function index() {
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $config['base_url'] = base_url() . 'admin/anonymous_post/index';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $total = 0;
        $data['results'] = $this->Anonymous_Post_model->getListByCondition($total, (int)$this->uri->segment(4), $config['per_page']);
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $this->load->view('admin/anonymous_post_list', $data);
    }
}

?>
