<?php

class Picture extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('Form');
        $this->load->library('Pagination');
        $this->load->model('Dashboard_model');
        $this->load->helper('Ext_URI');
        $this->load->model('Place_Post_model');
    }

    function index() {
        $cond = array();
        if (isset($this->uri->vn_param['keyword'])) {
            $cond['keyword'] = $this->input->xss_clean($this->uri->vn_param['keyword']);
        }

        if (isset($this->uri->vn_param['sort'])) {
            $cond['sort'] = $this->uri->vn_param['sort'];
        }

        $config['base_url'] = base_url() . 'admin/picture/index';
        $config['per_page'] = 50;
        $config['uri_segment'] = 4;
        $total = 0;
        $results = $this->Place_Post_model->getListPictureInPlace($cond, $total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $total;
        $data['results'] = $results;
        $this->load->view('admin/picture_place', $data);
    }

    function nbh_pics() {
        $cond = array();
        if (isset($this->uri->vn_param['keyword'])) {
            $cond['keyword'] = $this->input->xss_clean($this->uri->vn_param['keyword']);
        }

        if (isset($this->uri->vn_param['sort'])) {
            $cond['sort'] = $this->uri->vn_param['sort'];
        }

        $config['base_url'] = base_url() . 'admin/picture/nbh_pics';
        $config['per_page'] = 50;
        $config['uri_segment'] = 4;
        $total = 0;
        $results = $this->Place_Post_model->getListPictureInNbh($cond, $total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $total;
        $data['results'] = $results;
        $this->load->view('admin/picture_nbh', $data);
    }

    function status_pics() {
        $cond = array();
        if (isset($this->uri->vn_param['keyword'])) {
            $cond['keyword'] = $this->security->xss_clean($this->uri->vn_param['keyword']);    
            $cond['keyword'] = urldecode($cond['keyword']);        
        }

        if (isset($this->uri->vn_param['sort'])) {
            $cond['sort'] = $this->uri->vn_param['sort'];
        }        

        $config['base_url'] = base_url() . 'admin/picture/status_pics';
        $config['per_page'] = 50;
        $config['uri_segment'] = 4;
        $total = 0;
        $results = $this->Place_Post_model->getListPictureStatus($cond, $total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $total;
        $data['results'] = $results;
        $this->load->view('admin/picture_status', $data);
    }

    function delete_content($id, $type) {
        $this->load->model('Activity_model');
        $this->load->model('Neighborhood_Post_model');
        $this->load->model('Picture_Post_Activity_model');
        $this->load->model('Flag_Content_model');
        $contentInfo = $this->Activity_model->getContentInfo($id, $type);
        if ($contentInfo) {
            switch ($type) {
                case 2:
                    $this->Place_Post_model->deletePost($id);
                    $url = 'admin/picture/index';
                    break;
                case 7:
                    $this->Neighborhood_Post_model->deletePost($id);
                    $url = 'admin/picture/nbh_pics';
                    break;
                case 27:
                    $this->Picture_Post_Activity_model->deletePicStatus($id);
                    $url = 'admin/picture/status_pics';
                    break;
                default:
                    break;
            }
            $data['msg'] = 'Delete successful';
        }
        redirect($url . '/p_delete/' . $data['msg']);
    }

}

?>