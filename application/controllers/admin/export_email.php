<?php 
class Export_Email extends Ext_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Dashboard_model');
    }
    function index(){
        $data = array();
        $from = $to = $type = '';
        if (isset($this->uri->vn_param['from'])) {
            $from = $this->uri->vn_param['from'];
        }
        if (isset($this->uri->vn_param['to'])) {
            $to = $this->uri->vn_param['to'];
        }

        if (isset($this->uri->vn_param['type'])) {
            $type = $this->uri->vn_param['type'];
        }
        $data['from'] = $from;
        $data['to'] = $to;
        $data['type'] = $type;
        
        if ($from != '' && $to != '') {
            $fromDate = new DateTime($from);
            $toDate = new DateTime($to);

            $fromDate = date_sub($fromDate, date_interval_create_from_date_string('7 hours'));
            $toDate = date_sub($toDate, date_interval_create_from_date_string('7 hours'));
            $toDate = date_add($toDate, date_interval_create_from_date_string('1 day'));

            $from = $fromDate->format('Y-m-d H:i:s');
            $to = $toDate->format('Y-m-d H:i:s');
        }
        
        $this->load->library('Pagination');
        $config['base_url'] = base_url() . 'admin/export_email/index';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $total = 0;
        $result = $this->Dashboard_model->getEmailUserToExport($type, $from, $to, $total, (int) $this->uri->segment(4), $config['per_page']);
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['results'] = $result;
        $this->load->view('admin/export_email', $data);
    }
    function export(){  
        header('Content-Encoding: UTF-8');
        header('Content-Description: File Transfer');
        header("Content-Type: application/csv; charset=UTF-8");
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Disposition: attachment; filename=Email.csv");
        
        $from = $to = $type = '';
        if (isset($this->uri->vn_param['from'])) {
            $from = $this->uri->vn_param['from'];
        }
        if (isset($this->uri->vn_param['to'])) {
            $to = $this->uri->vn_param['to'];
        }

        if (isset($this->uri->vn_param['type'])) {
            $type = $this->uri->vn_param['type'];
        }

        if ($from != '' && $to != '') {
            $fromDate = new DateTime($this->input->post('from'));
            $toDate = new DateTime($this->input->post('to'));

            $fromDate = date_sub($fromDate, date_interval_create_from_date_string('7 hours'));
            $toDate = date_sub($toDate, date_interval_create_from_date_string('7 hours'));
            $toDate = date_add($toDate, date_interval_create_from_date_string('1 day'));

            $from = $fromDate->format('Y-m-d H:i:s');
            $to = $toDate->format('Y-m-d H:i:s');
           
        }
        $result = $this->Dashboard_model->getEmailUserToExport($type, $from, $to);
        $csvData[] = "UserID, UserName, Email, SignUpDate \n";
        if (sizeof($result) > 0) {
            foreach ($result as $row) {
                $csvData[] = $row->UserID . ',' . $row->UserName. ',' . $row->Email . ',' . $row->SignUpDate . "\n";
            }
        }

        foreach ($csvData as $row) {
            echo $row;
        }
    }
}
?>