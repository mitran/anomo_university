<?php

class Marketing extends Ext_Controller {

    function __construct() {
        parent::Ext_Controller();
        $this->load->model('Dashboard_model');
        $this->load->model('Marketing_model');
        $this->load->library("Flurry");
        $this->load->library("flurry_chat");
    }

    function value() {
        $data = array();
        $numberDay = 21;
        $today = date('Y-m-d');
        $to = $today;
        $days = $numberDay + 1;
        $from = date('Y-m-d', strtotime("-$days day", strtotime($today)));
        $ios = $this->flurry->getTotalFlurryActiveUser('ios', 'day', $from, $to, 'array');
        sleep(1); // hmmmmmm
        $android = $this->flurry->getTotalFlurryActiveUser('android', 'day', $from, $to, 'array');
        $oValues = $this->Marketing_model->getListValues($from, $to);
        $aValues = array();
        if ($oValues) {
            foreach ($oValues as $obj) {
                $aValues[$obj->Day] = $obj->AdDollar;
            }
        }

        if (count($ios) > 0 && count($android) > 0) {
            for ($i = 0; $i < $days; $i++) {
                $pdate = date('Y-m-d', strtotime("-$i day", strtotime($today)));
                $date = date('Y-m-d', strtotime("+1 day", strtotime($pdate)));
                $iIos = isset($ios[$pdate]) ? $ios[$pdate] : 0;
                $iAndroid = isset($android[$pdate]) ? $android[$pdate] : 0;
                $dau = $iIos + $iAndroid;
                $new_user = $this->Dashboard_model->totalUserRegister($pdate, $date);
                $cost_per_user = 0;

                if (isset($aValues[$pdate]) && $aValues[$pdate] > 0) {
                    if ($new_user > 0) {
                        $cost_per_user = $aValues[$pdate] / $new_user;
                    }
                }
                $data[] = array(
                    'date' => $pdate,
                    'dau' => $dau,
                    'new_user' => $new_user,
                    'true_dau' => $dau - $new_user,
                    'ad_dollar' => isset($aValues[$date]) ? $aValues[$date] : '',
                    'cost_per_user' => round($cost_per_user, 4)
                );
            }
        }
        $value['data'] = $data;
        $value['numberDay'] = $numberDay;
        $this->load->view('admin/value', $value);
    }

    function schedule() {
        $this->load->library('Pagination');
        $this->load->helper('Ext_URI');
        $config['base_url'] = base_url() . 'admin/marketing/schedule';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $total = 0;
        $result = $this->Marketing_model->getListSchedule($total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['result'] = $result;
        $this->load->view('admin/schedule', $data);
    }

    function add_schedule() {
        if (isset($_POST['submit'])) {
            $data = array(
                'Week' => $_POST['week'],
                'FBAds' => $_POST['fb_ads'],
                'Twitter' => $_POST['twitter'],
                'ContentContest' => $_POST['content_contest'],
                'CoreFeatureContest' => $_POST['core_feature_contest'],
                'LiveInfluencer' => $_POST['live_influencer'],
                'InternalContest' => $_POST['internal_contest'],
                'Broadcast' => $_POST['broadcast'],
                'CreatedDate' => gmdate('Y-m-d H:i:s')
            );

            $id = $this->Marketing_model->addSchedule($data);
            redirect('admin/marketing/schedule');
        }
    }

    function core_feature_engagement_new() {
        set_time_limit(0);
        date_default_timezone_set('America/Los_Angeles');
        $today = date('Y-m-d');
        $data = array();

        $sql = 'select Week from corefeatureengagement order by Week desc limit 1';
        $query = $this->db->query($sql);
        $result = $query->row();
        $lastdateofweek = $result ? $result->Week : false;
        
        // the first time 
        // SH**T
        if ($lastdateofweek) {

            $d = date('w', strtotime($today));
            if ($d == 1) {
                $from = date('Y-m-d', strtotime('-2 Monday', strtotime('+1 day', strtotime($today))));
                $to = date('Y-m-d', strtotime('+7 day', strtotime($from)));
            } else {
                $from = date('Y-m-d', strtotime('-2  Monday', strtotime($today)));
                $to = date('Y-m-d', strtotime('+7 day', strtotime($from)));
            }
            $i = 0;

            $daybeforeto = date('Y-m-d', strtotime('-1 day', strtotime($to)));
            while ($daybeforeto != $lastdateofweek) {
                $comment = $this->Dashboard_model->totalOther($from, $to, 'ActivityComment');

                $chats_ios1 = $this->flurry_chat->getTotalFlurryChatMsgs('Chat_SendMsg_Success', 'ios', $from, $daybeforeto);
                sleep(3);
                $chats_android1 = $this->flurry_chat->getTotalFlurryChatMsgs('Chat_SendMsg_Success', 'android', $from, $daybeforeto);
                sleep(3);
                $chats = $chats_ios1 + $chats_android1;

                $data[] = array(
                    'Week' => date("\"Y-m-d\"", strtotime('-1 day', strtotime($to))),
                    'Posts' => $this->Dashboard_model->totalOther($from, $daybeforeto, 'Post'),
                    'Comments' => $comment,
                    'Likes' => $this->Dashboard_model->totalOther($from, $daybeforeto, 'ActivityLike'),
                    'FollowersAdded' => $this->Dashboard_model->getTotalFollowerAdded($from, $daybeforeto),
                    'IceBreakers' => $this->Dashboard_model->totalOther($from, $daybeforeto, 'Anomotion'),
                    'ChatSessions' => 0,
                    'ChatMessages' => $chats,
                    'Reveals' => $this->Dashboard_model->getTotalReveal('all', $from, $daybeforeto),
                    'Checkins' => 0
                );

                $values = implode(",", $data[$i]);
                $insertsql = "insert into corefeatureengagement values($values)";
                $this->db->query($insertsql);

                $to = $from;
                $from = date('Y-m-d', strtotime('-7  day', strtotime($to)));
                $daybeforeto = date('Y-m-d', strtotime('-1 day', strtotime($to)));
                $i++;
            }
        }

        $value['data'] = $data;
        $numofWeeks = 26;
        $sql = "select * from corefeatureengagement order by Week desc limit $numofWeeks";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $value['result'] = $result;

        $ilimit = 7;
        $sql = "select * from core_feature_engagement_current_data where dtype = 'day' order by cdate desc limit $ilimit";
        $query = $this->db->query($sql);
        $daysdata = $query->result_array();
        $value['daysdata'] = $daysdata;


        $ilimit = 1;

        $sql = "select * from core_feature_engagement_current_data where dtype = 'week' order by cdate desc limit $ilimit";
        $query = $this->db->query($sql);
        $weekdata = $query->result_array();
        if ($weekdata) {
            $cweek = self::calculateweek($weekdata[0]['cdate']);
            $weekdata[0]['sunday'] = $cweek['end'];
        }
        $value['weekdata'] = $weekdata;
        $this->load->view('admin/core_feature_engagement_new', $value);
    }

    function core_feature_engagement_days() {
        set_time_limit(0);
        date_default_timezone_set('America/Los_Angeles');
        $lock = $rlock = 0;
        $ilimit = 7;
        $sql = "select cdate from core_feature_engagement_current_data where dtype = 'day' order by cdate desc limit 1";
        $query = $this->db->query($sql);
        $data = $query->row();
        $lastupdated = $data ? $data->cdate : date('Y-m-d', strtotime('-3 day', strtotime('Y-m-d')));


        $tdate = date('Y-m-d', strtotime('-1 days'));
        $date1 = new DateTime($lastupdated);
        $date2 = new DateTime($tdate);
        $interval = $date1->diff($date2);
        $countloop = $interval->d;


        if ($_SERVER['REQUEST_METHOD'] == "GET") {
            if (date('Y-m-d', strtotime('+1  day', strtotime($lastupdated))) == date("Y-m-d")) {
                $sql = "select * from core_feature_engagement_current_data where dtype = 'day' order by cdate desc limit $ilimit";
                $query = $this->db->query($sql);
                $data = $query->result_array();
                $value['data'] = $data;
                $this->load->view('admin/core_feature_engagement_days', $value);
            }
        } else {

            if (date("Y-m-d H:i:s") > $lastupdated) {


                $locksql = "SELECT get_lock('uwtempdays',0) as got_lock";
                $lockquery = $this->db->query($locksql);
                $lockstatus = $lockquery->row();
                $lock = $lockstatus? $lockstatus->got_lock: 0;

                if (!$lock)
                    exit();
                if ($lock = 1) {
                    header("Connection: close");
                    ignore_user_abort(true);
                    flush();
                    
                    for ($i = 0; $i <= $countloop; $i++) {
                        $from = date('Y-m-d H:i:s', strtotime($tdate . ' 00:00:00'));                
                        $to = date('Y-m-d H:i:s', strtotime($tdate . ' 23:59:59'));
                        $chats_ios1 = $this->flurry_chat->getTotalFlurryChatMsgs('Chat_SendMsg_Success', 'ios', $tdate, $tdate);
                        sleep(3);

                        $chats_android1 = $this->flurry_chat->getTotalFlurryChatMsgs('Chat_SendMsg_Success', 'android', $tdate, $tdate);                        sleep(3);

                        $iChat = $chats_ios1 + $chats_android1;
                        $iPost = $this->Dashboard_model->totalOther($from, $to, 'Post');
                        $iComment = $this->Dashboard_model->totalOther($from, $to, 'ActivityComment');
                        $iLike =  $this->Dashboard_model->totalOther($from, $to, 'ActivityLike');
                        $iIceBreaker = $this->Dashboard_model->totalOther($from, $to, 'Anomotion');
                        $iReveal = $this->Dashboard_model->getTotalReveal('all', $from, $to);
                        $iFollowerAdded = $this->Dashboard_model->getTotalFollowerAdded($from, $to);
  
                        $sql = "insert into core_feature_engagement_current_data (`cdate`, `posts`, `chat_messages`, `acomments`, `likes`, `followers_added`, `ice_breakers`, `reveals`, `dtype`)
                                values('$tdate', '$iPost', '$iChat', '$iComment', '$iLike', '$iFollowerAdded', '$iIceBreaker', '$iReveal', 'day')";
                        $this->db->query($sql);

                        $tdate = date('Y-m-d', strtotime('-1 day', strtotime($tdate)));
                    }
                    $discardlsql = "SELECT release_lock('uwtempdays') as discardlock";
                    $this->db->query($discardlsql);
                }
            }
        }
    }

    function core_feature_engagement_currentweek() {
        set_time_limit(0);
        date_default_timezone_set('America/Los_Angeles');
        $ilimit = 1;
        $flag = 0;
        $sql = "select cdate from core_feature_engagement_current_data where dtype = 'week' order by cdate desc limit 1";
        $query = $this->db->query($sql);
        $data = $query->result_array();
        $lastupdated = ($data) ? $data[0]['cdate'] : false;


        if ($_SERVER['REQUEST_METHOD'] == "GET") {
            $sql = "select * from core_feature_engagement_current_data where dtype = 'week' order by cdate desc limit $ilimit";
            $query = $this->db->query($sql);
            $data = $query->result_array();
            $cweek = self::calculateweek($data[0]['cdate']);
            $data[0]['end'] = $cweek['end'];
            $value['data'] = $data;
            $this->load->view('admin/core_feature_engagement_currentweek', $value);
        } else {

            $lock = 0;
            $dateUpdated = ($lastupdated)?$lastupdated:date("Y-m-d H:i:s", strtotime('-1 day', strtotime(date("Y-m-d H:i:s"))));
            $diff = abs(strtotime(date("Y-m-d H:i:s")) - strtotime($dateUpdated));
            $minutes = round(abs($diff) / 60, 2);
            $setminutes = 2;

            if ($minutes > $setminutes) {

                $locksql = "SELECT get_lock('uwtempcurrentweek',0) as got_lock";
                $lockquery = $this->db->query($locksql);
                $lockstatus = $lockquery->row();
                $lock = $lockstatus?$lockstatus->got_lock:0;
                if (!$lock)
                    exit();
                if ($lock = 1) {

                    header("Connection: close");
                    ignore_user_abort(true);
                    flush();
                    
                    $cweek = self::calculateweek(date('Y-m-d'));
                    $from = $cweek['start'];
                    $to = date('Y-m-d', strtotime('-1 day', strtotime($cweek['end'])));

                    $chats_ios1 = $this->flurry_chat->getTotalFlurryChatMsgs('Chat_SendMsg_Success', 'ios', $cweek['start'], $cweek['end']);
                    sleep(3);

                    $chats_android1 = $this->flurry_chat->getTotalFlurryChatMsgs('Chat_SendMsg_Success', 'android', $cweek['start'], $cweek['end']);
                    sleep(3);

                    $iChat = $chats_ios1 + $chats_android1;
                    $iPost = $this->Dashboard_model->totalOther($from, $to, 'Post');
                    $iComment = $this->Dashboard_model->totalOther($from, $to, 'ActivityComment');
                    $iLike =  $this->Dashboard_model->totalOther($from, $to, 'ActivityLike');
                    $iIceBreaker = $this->Dashboard_model->totalOther($from, $to, 'Anomotion');
                    $iReveal = $this->Dashboard_model->getTotalReveal('all', $from, $to);
                    $iFollowerAdded = $this->Dashboard_model->getTotalFollowerAdded($from, $to);
                    $cDate = date('Y-m-d H:i:s');
                    if ($lastupdated){ // update
                        $sql = "UPDATE core_feature_engagement_current_data SET 
                                    cdate = '" . $cDate . "'
                                    ,posts = '" . $iPost . "'
                                    ,chat_messages = '" . $iChat . "'
                                    ,acomments = '" . $iComment . "'
                                    ,likes = '" . $ilimit . "'
                                    ,followers_added = '" . $iFollowerAdded . "'
                                    ,ice_breakers = '" . $iIceBreaker . "'
                                    ,reveals = '" . $iReveal . "'         
    				where dtype = 'week'";
                        $this->db->query($sql);
                    }else{ // insert 
                        $sql = "insert into core_feature_engagement_current_data (`cdate`, `posts`, `chat_messages`, `acomments`, `likes`, `followers_added`, `ice_breakers`, `reveals`, `dtype`)
                                values('$cDate', '$iPost', '$iChat', '$iComment', '$iLike', '$iFollowerAdded', '$iIceBreaker', '$iReveal', 'week')";
                        $this->db->query($sql);
                    }
                    
                    // release lock
                    $discardlsql = "SELECT release_lock('uwtempcurrentweek') as discardlock";
                    $this->db->query($discardlsql);
                }
            }

            if ($minutes < $setminutes) {
                $sql = "select * from core_feature_engagement_current_data where dtype = 'week' order by cdate desc limit $ilimit";
                $query = $this->db->query($sql);
                $data = $query->result_array();
                if ($data)
                    $data [0] ['end'] = $end;
                $value ['data'] = $data;
                $this->load->view('admin/core_feature_engagement_currentweek', $value);
            }
        }
    }

    private function calculateweek($date) {
        $ts = strtotime($date);
        $d = date('w', $ts);
        if ($d != 0) {
            $start = date('Y-m-d', strtotime('monday this week', $ts));
            $end = date('Y-m-d', strtotime('monday next week', $ts));
        } else {
            $start = date('Y-m-d', strtotime('monday last week', $ts));
            $end = date('Y-m-d', strtotime('monday this week', $ts));
        }
        return(array('start' => $start, 'end' => $end));
    }

    function content_engagement() {
        set_time_limit(0);
        $dateWeek = self::x_week_range();
        $start = $dateWeek[0];
        $end = $dateWeek[1];
        $numberWeeks = 7;
        $data = array();
        // in house content
        $userID = 32772; // pandagirl - 32772
        for ($i = 0; $i < $numberWeeks; $i++) {
            $from = date("Y-m-d", strtotime("- $i weeks", strtotime($start)));
            $to = date("Y-m-d", strtotime("- $i weeks", strtotime($end)));
            $comment = $this->Dashboard_model->getTotalContent($userID, $from, $to, 'ActivityComment', 'only');

            $data[] = array(
                'Week' => $from,
                'Post' => $this->Dashboard_model->getTotalContent($userID, $from, $to, 'Post', 'only'),
                'Comment' => $comment,
                'Like' => $this->Dashboard_model->getTotalContent($userID, $from, $to, 'ActivityLike', 'only'),
                'ExtShare' => $this->Dashboard_model->getTotalContent($userID, $from, $to, 'ExtShare', 'only')
            );
        }


        // user content
        $userContent = array();
        for ($i = 0; $i < $numberWeeks; $i++) {
            $from = date("Y-m-d", strtotime("- $i weeks", strtotime($start)));
            $to = date("Y-m-d", strtotime("- $i weeks", strtotime($end)));
            $comment = $this->Dashboard_model->getTotalContent($userID, $from, $to, 'ActivityComment', 'exclude');

            $userContent[] = array(
                'Week' => $from,
                'Post' => $this->Dashboard_model->getTotalContent($userID, $from, $to, 'Post', 'exclude'),
                'Comment' => $comment,
                'Like' => $this->Dashboard_model->getTotalContent($userID, $from, $to, 'ActivityLike', 'exclude'),
                'ExtShare' => $this->Dashboard_model->getTotalContent($userID, $from, $to, 'ExtShare', 'exclude')
            );
        }

        // app invite friend
        $appShare = array();
        for ($i = 0; $i < $numberWeeks; $i++) {
            $from = date("Y-m-d", strtotime("- $i weeks", strtotime($start)));
            $to = date("Y-m-d", strtotime("- $i weeks", strtotime($end)));
            $appShare[] = array(
                'Week' => $from,
                'Share' => $this->Dashboard_model->totalOther($from, $to, 'AppShare')
            );
        }
        $value['data'] = $data;
        $value['userContent'] = $userContent;
        $value['appShare'] = $appShare;
        $this->load->view('admin/content_engagement', $value);
    }

    function life_cycle_tracking() {
        set_time_limit(0);
        $dateWeek = self::x_week_range();
        $start = $dateWeek[0];
        $end = $dateWeek[1];
        $numberWeeks = 7;
        $data = array();
        // first 3 day
        for ($i = 0; $i < $numberWeeks; $i++) {
            $from = date("Y-m-d", strtotime("- $i weeks", strtotime($start)));
            $to = date("Y-m-d", strtotime("- $i weeks", strtotime($end)));

            // get list user register in a week
            $post = 0;
            $like = 0;
            $comment = 0;
            $game = 0;
            $users = $this->Marketing_model->getUserRegister($from, $to);

            $post = $this->Dashboard_model->getAllTotalComment($from, $to, 'Post', 2);
            $like = $this->Dashboard_model->getAllTotalComment($from, $to, 'ActivityLike', 2);
            $game = $this->Dashboard_model->gettoAllTotalGameOf($from, $to, 2);
            $comment = $this->Dashboard_model->getAllTotalComment($from, $to, 'ActivityComment', 2);

            $data[] = array(
                'Week' => $from,
                'AccountCreation' => count($users),
                'Post' => $post,
                'Like' => $like,
                'Comment' => $comment,
                'Game' => $game,
            );

            $post7 = 0;
            $like7 = 0;
            $comment7 = 0;
            $game7 = 0;

            $post7 = $this->Dashboard_model->getAllTotalComment($from, $to, 'Post', 6);
            $like7 = $this->Dashboard_model->getAllTotalComment($from, $to, 'ActivityLike', 6);
            $game7 = $this->Dashboard_model->gettoAllTotalGameOf($from, $to, 6);
            $comment7 = $this->Dashboard_model->getAllTotalComment($from, $to, 'ActivityComment', 6);

            $dataWithIn7Days[] = array(
                'Week' => $from,
                'AccountCreation' => $this->commonlib->formatBigNumber(count($users) / 7),
                'Post' => $this->commonlib->formatBigNumber($post7 / 7),
                'Like' => $this->commonlib->formatBigNumber($like7 / 7),
                'Comment' => $this->commonlib->formatBigNumber($comment7 / 7),
                'Game' => $this->commonlib->formatBigNumber($game7 / 7),
            );
        }

        $value['data'] = $data;
        $value['within7days'] = $dataWithIn7Days;
        $this->load->view('admin/life_cycle_tracking', $value);
    }

    function kpi() {
        set_time_limit(0);
        $now = date('Y-m-d');
        $dateWeek = self::x_week_range();
        $start = $dateWeek[0];
        $end = $dateWeek[1];
        $numberWeeks = 7;
        $to = $start;
        $from = date("Y-m-d", strtotime("- $numberWeeks weeks", strtotime($end)));


        // #download
        $downloadIos = $this->flurry->getTotalFlurryActiveUser('iso', 'day', $from, $to, 'array', 'NewUsers', 'weeks');

        sleep(1); // hmmmmmm
        $downloadAndroid = $this->flurry->getTotalFlurryActiveUser('android', 'day', $from, $to, 'array', 'NewUsers', 'weeks');
        sleep(1); // hmmmmmm
        // #Avg Daily Actives
        $avgDailyActiveIos = $this->flurry->getTotalFlurryActiveUser('iso', 'day', $from, $now, 'array', 'ActiveUsers');
        sleep(1); // hmmmmmm
        $avgDailyActiveAndroid = $this->flurry->getTotalFlurryActiveUser('android', 'day', $from, $now, 'array', 'ActiveUsers');
        sleep(1); // hmmmmmm
        // #Avg Daily Sessions per User
        $avgDailySessionPerUserIos = $this->flurry->getTotalFlurryActiveUser('iso', 'day', $from, $now, 'array', 'Sessions');
        sleep(1); // hmmmmmm
        $avgDailySessionPerUserAndroid = $this->flurry->getTotalFlurryActiveUser('android', 'day', $from, $now, 'array', 'Sessions');

        sleep(1); // hmmmmmm
        $dauIos = $this->flurry->getTotalFlurryActiveUser('iso', 'day', $from, $to, 'array', 'ActiveUsersByWeek', 'weeks');
        sleep(1); // hmmmmmm
        $dauAndroid = $this->flurry->getTotalFlurryActiveUser('android', 'day', $from, $to, 'array', 'ActiveUsersByWeek', 'weeks');

        $data = array();

        for ($i = 0; $i < $numberWeeks; $i++) {
            $date = date("Y-m-d", strtotime("- $i weeks", strtotime($start)));
            $dateTo = date("Y-m-d", strtotime("- $i weeks", strtotime($end)));
            $account = $this->Dashboard_model->totalUserRegister($date, $dateTo);

            $download1 = isset($downloadIos[$date]) ? $downloadIos[$date] : 0;
            $download2 = isset($downloadAndroid[$date]) ? $downloadAndroid[$date] : 0;
            $download = $download1 + $download2;

            $dau1 = isset($dauIos[$date]) ? $dauIos[$date] : 0;
            $dau2 = isset($dauAndroid[$date]) ? $dauAndroid[$date] : 0;
            $dau = $dau1 + $dau2;

            // 
            $avgDailyActive1 = 0;
            $avgDailyActive2 = 0;
            $avgDailySessionPerUser1 = 0;
            $avgDailySessionPerUser2 = 0;
            for ($j = 0; $j < 7; $j++) {
                $sDate = date('Y-m-d', strtotime("+$j day", strtotime($date)));
                $avgDailyActive1 += isset($avgDailyActiveIos[$sDate]) ? $avgDailyActiveIos[$sDate] : 0;
                $avgDailyActive2 += isset($avgDailyActiveAndroid[$sDate]) ? $avgDailyActiveAndroid[$sDate] : 0;

                $avgDailySessionPerUser1 += isset($avgDailySessionPerUserIos[$sDate]) ? $avgDailySessionPerUserIos[$sDate] : 0;
                $avgDailySessionPerUser2 += isset($avgDailySessionPerUserAndroid[$sDate]) ? $avgDailySessionPerUserAndroid[$sDate] : 0;
            }
            $avgDailyActive = $avgDailyActive1 + $avgDailyActive2;
            $avgDailySessionPerUser = ($avgDailySessionPerUser1 + $avgDailySessionPerUser2);
            if ($avgDailySessionPerUser > 0) {
                $avgDailySessionPerUser = ($avgDailySessionPerUser / $avgDailyActive);
                $avgDailySessionPerUser = round($avgDailySessionPerUser, 2);
            } else {
                $avgDailySessionPerUser = 0;
            }


            $data[] = array(
                'Week' => $date,
                'Download' => $download,
                'Account' => $account,
                'AvgDailyActive' => round($avgDailyActive / 7, 2),
                'AvgDailySessionPerUser' => $avgDailySessionPerUser,
                'DAU' => $dau
            );
        }
        $value['data'] = $data;
        $this->load->view('admin/kpi', $value);
    }

    function social_media() {
        $this->load->library('Pagination');
        $this->load->helper('ext_uri');
        $config['base_url'] = base_url() . 'admin/marketing/social_media';
        $config['per_page'] = 20;
        $config['uri_segment'] = 4;
        $total = 0;
        $result = $this->Marketing_model->getListSocialMedia($total, $config['per_page'], (int) $this->uri->segment(4));
        $config['total_rows'] = $total;
        $this->pagination->initialize($config);
        $data['total'] = $config['total_rows'];
        $data['result'] = $result;
        $this->load->view('admin/social_media', $data);
    }

    function add_social_media() {
        if (isset($_POST['submit'])) {
            $data = array(
                'Week' => $_POST['week'],
                'FBAdClicks' => $_POST['fb_ad_click'],
                'FBDownloads' => $_POST['fb_dowloads'],
                'FBLikes' => $_POST['fb_likes'],
                'TwitterFollowers' => $_POST['twitter_followers'],
                'CreatedDate' => gmdate('Y-m-d H:i:s')
            );

            $id = $this->Marketing_model->addSocialMedia($data);
            redirect('admin/marketing/social_media');
        }
    }

    function save_value() {
        $json['status'] = '';
        if (isset($_POST)) {
            $data = array(
                'Day' => $_POST['date'],
                'AdDollar' => $_POST['ad_dollar']
            );
            $return = $this->Marketing_model->addValue($data);
            if ($return) {
                $json['status'] = 'ok';
            }
        }
        echo json_encode($json);
    }

    private function x_week_range($date = '') {
        if ($date == '')
            $date = date('Y-m-d');

        $ts = strtotime($date);
        $d = date('w', strtotime($date));
        if ($d != 0) {
            $start = strtotime('sunday this week -1 week', $ts);
            $end = strtotime('sunday this week', $ts);
        } else {
            $start = strtotime('monday last week -1 week', $ts);
            $end = strtotime('monday last week', $ts);
        }
        return array(date('Y-m-d', $start), date('Y-m-d', $end));
    }

    private function x_week_range2($date) {
        $ts = strtotime($date);
        $start = strtotime('sunday this week -1 week', $ts);
        $end = strtotime('sunday this week', $ts);
        return array(date('Y-m-d', $start), date('Y-m-d', $end));
    }

}

?>