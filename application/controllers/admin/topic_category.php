<?php
class Topic_Category extends Ext_Controller{
    function __construct() {
        parent::__construct();
        $this->load->library('Pagination');
        $this->load->model('Topic_Category_model');
        $this->load->library('S3Upload');
        $this->load->library('form_validation');
    }

    function index(){
        $data['results'] = $this->Topic_Category_model->getListCategory();
        $this->load->view('admin/topic_cate_list', $data);
    }

    function add($id = 0) {
        $data['id'] = $id;
        if ($id > 0) {
            $info = $this->Topic_Category_model->getCateInfo($id);
            $data['info'] = $info;
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('name', 'Name', "trim|required|xss_clean");
            $this->form_validation->set_rules('order', 'Order', "trim|required|int");
            $this->form_validation->set_rules('desc', 'Desc', "trim|required");
            $upload['error'] = false;
            if ($id == 0 || ($id > 0 && $_FILES['photo']['name'] != '')) {
                $isUpload = $this->s3upload->upload('photo', '', BUCKET_NAME_STREAM);
                if (!$isUpload) {
                    $upload['error'] = true;
                    $upload['error_msg'] = $this->s3upload->error;
                } else {
                    $upload['file_name'] = $isUpload;
                }
            }

            if ($this->form_validation->run() == FALSE || $upload['error']) {
                if ($upload['error']) {
                    $data['upload_error'] = $upload['error_msg'];
                }
                $this->load->view('admin/topic_cate_form', $data);
            } else {
                if ($id == 0) {
                    $icon = $upload['file_name'];
                } else {
                    if (isset($upload['file_name']) && $upload['file_name'] != '') {
                        $icon = $upload['file_name'];
                    } else {
                        $icon = $info->Photo;
                    }
                }
                $value = array(
                    'CateName' => $this->input->post('name'),
                    'OrderID' => $this->input->post('order'),
                    'Desc' => $this->input->post('desc'),
                    'Status'    => 1,
                    'AllowCreateTopic'  => $this->input->post('allow_create_topic'),
                    'Photo' => $icon
                    
                );

                if ($id == 0) {
                    $value['CreatedDate'] = gmdate('Y-m-d H:i:s');
                    $value['LastModified'] = gmdate('Y-m-d H:i:s');
                    $this->Topic_Category_model->addTopicCate($value);
                } else {
                    $this->Topic_Category_model->updateTopicCate($value, array('CateID' => $id));
                }
                redirect('admin/topic_category/index');
            }
        } else {
            $this->load->view('admin/topic_cate_form', $data);
        }
    }
}
?>
