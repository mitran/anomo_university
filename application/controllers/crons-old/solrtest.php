<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'libraries/SolrPHPClient/Service.php');

class SolrTest extends Ext_Controller
{
    var $solr_hostname = '54.245.100.207';
    var $solr_port = '8234';
    var $solr_path = '/solr';

    var $solr;


    function __construct()
    {
        parent::__construct();
        $this->solr = new Apache_Solr_Service($this->solr_hostname, $this->solr_port, $this->solr_path);
    }

    function index()
    {
        if (!$this->solr->ping()) {
            echo 'Solr service not responding.';
            exit;
        }

        $sql = "select  CurrentUserID as `name`,
                        (select UserName from `user` where UserID = CurrentUserID) as description,
			            TargetUserID as `resourcename`,
			            (select UserName from `user` where UserID = TargetUserID) as `keywords`
                from userconnect where IsFavorite = 1";
        $query = $this->db->query($sql);

        $documents = array();

        foreach ($query->result() as $fields) {

            $part = new Apache_Solr_Document();
            $part->addField('id', $fields->name . '-' . $fields->resourcename);

            foreach ($fields as $key => $value) {
                if (is_array($value)) {
                    foreach ($value as $data) {
                        $part->addField($key, $data);
                    }
                }
                else {
                    $part->$key = $value;
                }
            }
            $documents[] = $part;
        }

        echo '<pre>';
        var_dump($documents);
        echo '</pre>';

        try {
            $this->solr->addDocuments($documents);
            $this->solr->commit();
            $this->solr->optimize();
        }
        catch (Exception $e) {
            echo $e->getMessage() . '<br/>';
        }
    }

    function search($curUser, $keywords = '')
    {
        $offset = 0;
        $limit = 10;

        if (!isset($curUser))
            die ("ERROR: INVALID REQUEST");

        $queries = array(
            "(name:$curUser AND keywords:*$keywords*) OR (resourcename:$curUser AND description:*$keywords*)"
        );

        echo "Querying added items ...<br/>";
        foreach ($queries as $query) {
            $response = $this->solr->search($query, $offset, $limit);

            if ($response->getHttpStatus() == 200) {
                if ($response->response->numFound > 0) {
                    // echo "$query <br />";

                    foreach ($response->response->docs as $doc) {
                        if ($doc->name == '' . $curUser)
                            echo "id : $doc->resourcename, name : $doc->keywords <br />";
                        else
                            echo "id : $doc->name, name : $doc->description <br />";
                    }
                    // echo '<br />';
                }
            }
            else {
                echo $response->getHttpStatusMessage();
            }
        }
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */

