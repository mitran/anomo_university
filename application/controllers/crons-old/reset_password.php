<?php

class Reset_Password extends Ext_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('User_model');
    }

    function index($key = '') { 
        $this->load->library('form_validation');
        $data['correct'] = 0;
        if ($key != ''){
            $data['key'] = $key;
            $pattern = $this->commonlib->decode($key);
            if ($pattern != ''){
                $value = explode('_', $pattern);
                $user_id = isset($value[0])?$value[0]:'';
                $email = isset($value[1])?$value[1]:'';
                if (!empty($user_id) && !empty($email)){
                    $userObj = $this->User_model->getUserInfoByEmail($email, $user_id);                  
                    if ($userObj){
                        $data['correct'] = 1;
                        $data['user_id'] = $userObj->UserID;
                    }
                }
            }
        }
        
        if (isset($_POST['submit'])) {
            
            $this->form_validation->set_rules('password', 'New password', 'trim|required|alpha_numeric|min_length[4]|matches[re_password]');
            $this->form_validation->set_rules('re_password', 'Re-newpassword', 'trim|required|alpha_numeric');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('reset_password', $data);
            } else {
                $value = array();
                $value['Password'] = md5(trim($_POST['password']));
                $re = $this->User_model->updateUser($value, array('UserID' => $user_id));
                if ($re) {
                    echo 'successful';
                } else {
                    echo 'fail';
                }
            }
        }else{  
            $this->load->view('reset_password',$data);
        }
    }

}

