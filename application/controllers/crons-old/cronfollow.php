<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CronFollow extends Ext_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{ 
		$sql1 = "UPDATE user,
						(
						SELECT CurrentUserID AS UserID, count( * ) AS NUM
						FROM userconnect
						WHERE IsFavorite =1
						GROUP BY CurrentUserID
						) AS temp
				SET user.NumberOfFollowing = temp.NUM WHERE user.UserID = temp.UserID";
		$this->db->query($sql1);
		$sql2 = "UPDATE user,
						(
						SELECT TargetUserID AS UserID, count( * ) AS NUM
						FROM userconnect
						WHERE IsFavorite =1
						GROUP BY TargetUserID
						) AS temp
				SET user.NumberOfFollower = temp.NUM WHERE user.UserID = temp.UserID";
		$this->db->query($sql2);
		
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */