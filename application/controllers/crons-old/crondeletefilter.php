<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CronDeleteFilter extends Ext_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{ 
                set_time_limit(0);
		$sql1 = "delete from ofmessagearchive where body IN ( 
									'filter_message_status_typing',
									'filter_message_status_endtyping',
									'filter_message_status_delivered')";
		$this->db->query($sql1);
		$sql2 = "delete from ofoffline where SUBSTRING(stanza,INSTR(stanza, '<body>')+6, (INSTR(stanza, '</body>') - (INSTR(stanza, '<body>') + 6)))
								IN ( 
									'filter_message_status_typing',
									'filter_message_status_endtyping',
									'filter_message_status_delivered')";
		$this->db->query($sql2);
                
                $sql3 = "delete from ofconversation where conversationID Not in (select conversationID from ofmessagearchive)";
                $this->db->query($sql3);
                
                $sql4 = "delete from ofconparticipant where conversationID Not in (select conversationID from ofmessagearchive)";
                $this->db->query($sql4);
		
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */