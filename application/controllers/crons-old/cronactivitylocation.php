<?php
class CronActivityLocation extends Ext_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $limit = 200;
        $sql1 = "select ActivityID from activity where NeighborhoodID is null limit $limit";
        $query = $this->db->query($sql1);
        $result = $query->result();
        $aActivity = array();
        if ($result) {
            foreach ($result as $re) {
                $aActivity[] = $re->ActivityID;
            }
        }
        if (sizeof($aActivity) > 0) {
            $sql = "update activity, user 
                    set activity.NeighborhoodID = user.NeighborhoodID 
                    where activity.FromUserID = user.UserID 
                        AND activity.ActivityID IN (" . implode(',', $aActivity) . ")";
            $this->db->query($sql);
        }
    }

}

