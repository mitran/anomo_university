<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class coreFeatureEngagementPopulateDB extends Ext_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
/* 
	function index()
	{
		set_time_limit(0);
		$time = "-14 day";
		$sql = "select ID from notificationhistory where CreatedDate <'". date('Y-m-d H:i:s', strtotime($time, strtotime(gmdate('Y-m-d H:i:s'))))."' limit $limit";
		$query = $this->db->query($sql);
		$result = $query->result();
	}
	 */
	
	function totalOther($from = '', $to = '', $type = '') {
		$tables = array(
				'Anomotion' => 'anomotion',
				'UserChat' => 'ofconversation',
				'UserChatMessages' => 'ofmessagearchive',
				'GiftSend' => 'giftsend',
				'ActivityLike' => 'activitylike',
				'PicturePostActivity' => 'picturepostactivity',
				'ActivityComment' => 'activitycomment',
				'PostComment' => 'postcomment',
				'NeighborhoodComment' => 'neighborhoodcomment',
				'Status' => 'userprofilestatus',
				'Activity' => 'activity',
				'Post' => 'activity',
				'AppShare' => 'fbshare'
		);
		if ($type != '' && in_array($type, array_keys($tables))) {
			$createdDate = 'CreatedDate';
	
			$this->db->select('count(*) as Total');
			$this->db->from($tables[$type]);
	
			if ($type == 'UserChat') {
				$createdDate = "FROM_UNIXTIME(lastActivity/1000)";
	
				$exclude_user_id = MASS_MESSAGE_SENDER_IDS;
				$where = "conversationID NOT IN (select conversationID from ofconparticipant where SUBSTRING_INDEX(bareJID,'@',1) IN ( $exclude_user_id)) and lastMessage is not null and lastMessage != '' and lastMessage != 'filter_request_game'";
				$this->db->where($where);
			}
			if ($type == 'UserChatMessages') {
				$createdDate = "FROM_UNIXTIME(sentDate/1000)";
				$exclude_user_id = MASS_MESSAGE_SENDER_IDS;
				$where = "conversationID NOT IN (select conversationID from ofconparticipant where SUBSTRING_INDEX(bareJID,'@',1) IN ( $exclude_user_id)) and body NOT IN ('filter_request_game',
				'filter_block_user',
				'filter_unblock_user',
				'filter_message_status_typing',
				'filter_message_status_endtyping',
				'filter_message_status_delivered')";
				$this->db->where($where);
			}
	
			if ($from != '') {
			$this->db->where("DATE_FORMAT(DATE_SUB($createdDate, INTERVAL 7 HOUR ) , '%Y-%m-%d')>='" . $from . "'");
			// $this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s')>='". $from."'");
			}
			if ($to != '') {
			$this->db->where("DATE_FORMAT(DATE_SUB($createdDate, INTERVAL 7 HOUR ) , '%Y-%m-%d')<='" . $to . "'");
					// $this->db->where("DATE_FORMAT(CreatedDate, '%Y-%m-%d %H:%i:%s')<='". $to."'");
			}

		if ($type == 'Activity') {
		$actionType = '0, 1, 2, 7, 27, 6, 8';
			$this->db->where("ActionType IN (" . $actionType . ")");
		}
		if ($type == 'Post') {
			$actionType = '1, 2, 7, 27, 6, 8';
			$this->db->where("ActionType IN (" . $actionType . ")");
		}
	
		if ($type == 'Anomotion') {
			$this->db->where('Status', 3);
		}
	
	
	
		$query = $this->db->get();
		$row = $query->row();
		return ($row) ? $row->Total : 0;
		echo "$query \n";
	}
	}	
	
	function getTotalFollowerAdded($from = '', $to = '') {
		$this->db->select('count(*) as Total');
		$this->db->from('userconnect');
		if ($from != '') {
			$this->db->where("DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d')>='" . $from . "'");
		}
		if ($to != '') {
			$this->db->where("DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d')<='" . $to . "'");
		}
		$this->db->where('IsFavorite', 1);
		$query = $this->db->get();
		$row = $query->row();
		return ($row) ? $row->Total : 0;
	}
	
	function getTotalReveal($from = '', $to = '') {
		$this->db->select('count(ID) as Total');
		$this->db->from('reveal');
		if ($from != '') {
			$this->db->where("DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d')>='" . $from . "'");
		}
		if ($to != '') {
			$this->db->where("DATE_FORMAT(DATE_SUB(CreatedDate, INTERVAL 7 HOUR ) , '%Y-%m-%d')<='" . $to . "'");
		}
		$this->db->where('Status', 1);
		$query = $this->db->get();
		$row = $query->row();
		return ($row) ? $row->Total : 0;
	}
	
	function getTotalCheckin($from = '', $to = '') {
		$this->db->select('count(ID) as Total');
		$this->db->from('usercheckin');
		if ($from != '') {
			$this->db->where("DATE_FORMAT(DATE_SUB(CheckOutDate, INTERVAL 7 HOUR ) , '%Y-%m-%d')>='" . $from . "'");
		}
		if ($to != '') {
			$this->db->where("DATE_FORMAT(DATE_SUB(CheckOutDate, INTERVAL 7 HOUR ) , '%Y-%m-%d')<='" . $to . "'");
		}
		$this->db->where('Status', 1);
		$query = $this->db->get();
		$row = $query->row();
		return ($row) ? $row->Total : 0;
	}

 	
	function index() {
        set_time_limit(0);
		$tdate = date('Y-m-d');        
        $w = date('W',strtotime($tdate));
        $d = date('Y-m-d',strtotime($tdate));
        $numberWeeks = 26;
        $data = array();
        $i=0;
        if($d == 0){        	
        	$tdate = strtotime('-1 day',strtotime($tdate));
        }
        else{
        while($i < $numberWeeks){    	
        	$week = str_pad(($w), 2, '0', STR_PAD_LEFT);
        	$sWeek = date('Y',strtotime($d)) . '-W' . $week . '-'. 1;
            $from = date('Y-m-d', strtotime($sWeek));            
			$to = date('Y-m-d', strtotime('+6 day', strtotime($from)));
			$tmpd = date('Y-m-d', strtotime('-1 day', strtotime($from)));
			$w = date('W',strtotime($tmpd)); 
			$d = $tmpd;
 			$activityComment = $this->totalOther($from, $to, 'ActivityComment');	
			$placeComment = $this->totalOther($from, $to, 'PostComment');
			$nbhComment = $this->totalOther($from, $to, 'NeighborhoodComment');
			$comment = $activityComment + $placeComment + $nbhComment;
	
			$data[] = array(
					'Week' => date("\"Y-m-d\"",strtotime($from)),
					'Post' => $this->totalOther($from, $to, 'Post'),
					'Comment' => $comment,
					'Like' => $this->totalOther($from, $to, 'ActivityLike'),
					'FollowerAdded' => $this->getTotalFollowerAdded($from, $to),
					'IceBreaker' => $this->totalOther($from, $to, 'Anomotion'),
					'ChatSession' => $this->totalOther($from, $to, 'UserChat'),
					'ChatMessage' => $this->totalOther($from, $to, 'UserChatMessages'),
					'Reveal' => $this->getTotalReveal($from, $to),
					'Checkin' => $this->getTotalCheckin($from, $to)
			);			
			$values=implode(",",$data[$i]);
			$insertsql = "insert into coreFeatureEngagement values($values)";			
			$this->db->query($insertsql);
            $i++; 
        }
        }	
	}
	

}
?>