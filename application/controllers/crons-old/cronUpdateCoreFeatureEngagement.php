<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class cronUpdateCoreFeatureEngagement extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Dashboard_model');
        $this->load->library("flurry_chat");
    }

    function index($numberWeek = 10) {
        set_time_limit(0);
        date_default_timezone_set('America/Los_Angeles');
        $today = date('Y-m-d');
        $d = date('w', strtotime($today));
        if ($d == 1) {
            $from = date('Y-m-d', strtotime('-2 Monday', strtotime('+1 day', strtotime($today))));
            $to = date('Y-m-d', strtotime('+7 day', strtotime($from)));
        } else {
            $from = date('Y-m-d', strtotime('-2  Monday', strtotime($today)));
            $to = date('Y-m-d', strtotime('+7 day', strtotime($from)));
        }
        $i = 0;

        
        $daybeforeto = date('Y-m-d', strtotime('-1 day', strtotime($to)));
        $weekStart = $daybeforeto;



        if (!$numberWeek)
            $numberWeek = 10;
        while ($i < $numberWeek) {
            var_dump($from . '----' . $to . '=======' . $daybeforeto);
            $comment = $this->Dashboard_model->totalOther($from, $to, 'ActivityComment');

            $chats_ios1 = $this->flurry_chat->getTotalFlurryChatMsgs('Chat_SendMsg_Success', 'ios', $from, $daybeforeto);
            sleep(3);
            $chats_android1 = $this->flurry_chat->getTotalFlurryChatMsgs('Chat_SendMsg_Success', 'android', $from, $daybeforeto);
            sleep(3);
            $chats = $chats_ios1 + $chats_android1;

            $data[$i] = array(
                'Week' => date("\"Y-m-d\"", strtotime('-1 day', strtotime($to))),
                'Posts' => $this->Dashboard_model->totalOther($from, $daybeforeto, 'Post'),
                'Comments' => $comment,
                'Likes' => $this->Dashboard_model->totalOther($from, $daybeforeto, 'ActivityLike'),
                'FollowersAdded' => $this->Dashboard_model->getTotalFollowerAdded($from, $daybeforeto),
                'IceBreakers' => $this->Dashboard_model->totalOther($from, $daybeforeto, 'Anomotion'),
                'ChatSessions' => 0,
                'ChatMessages' => $chats,
                'Reveals' => $this->Dashboard_model->getTotalReveal('all', $from, $daybeforeto),
                'Checkins' => 0
            );

            $values = implode(",", $data[$i]);
            $insertsql = "insert into corefeatureengagement values($values)";
            $this->db->query($insertsql);

            $to = $from;
            $from = date('Y-m-d', strtotime('-7  day', strtotime($to)));
            $daybeforeto = date('Y-m-d', strtotime('-1 day', strtotime($to)));
            $i++;
        }

    }

}

