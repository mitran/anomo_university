<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CreateUsers extends Ext_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index($count = 100, $startYear = '1970', $endYear = '2013', $userNamePrefix = 'Tester')
    {
        if ($count > 200)
            die('Users count is too big... please try something smaller (<= 200)');

        $curYear = (int)$startYear;
        $curGender = 1;
        for ($counter = 1; $counter <= $count; $counter++) {
            $data = array(
                'Email' => $userNamePrefix . $counter . '@vinasource.com',
                'Password' => '123456',
                'EmailAccountStatus' => 1,
                'SignUpDate' => '2013-2-4 03:10:00',
                'AllowReceiveMessage' => 1,
                'ALlowDisplayAge' => 1,
                'AllowAddFavoriteNotice' => 1,
                'AllowSendGiftNotice' => 1,
                'AllowRevealNotice' => 1,
                'AllowChatNotice' => 1,
                'AllowCommentTextPostNotice' => 1,
                'AllowCommentPicturePostNotice' => 1,
                'AllowRequestLockedCategoryNotice' => 1,
                'AllowRevealCategoryNotice' => 1,
                'AllowAnomotionNotice' => 1,
                'Credits' => 0,
                'UserName' => $userNamePrefix . $counter,
                'BirthDate' => $curYear . '-1-1',
                'Gender' => $curGender,
                'Photo' => '0a2b1d07d82f950e26e5eafe6f101584.jpg',
                'FullPhoto' => '0b8a4215dc4792851a8bce3f8a7f9847',
                'CoverPicture' => '',
                'FacebookID' => '99805819301',
                'FbEmail' => 'http://www.facebook.com/tester1',
                'Phone' => '',
                'PromoCode' => 0,
                'ProfileStatus' => '',
                'IsRemind' => 0,
                'RealName' => $userNamePrefix . $counter,
                'RealPhoto' => '',
                'SentForgotPassword' => '0000-0-0',
                'SentEmailConfirm' => '0000-0-0',
                'IsSendEmailValidate' => 1,
                'BasicPicture' => '',
                'College' => '',
                'Major' => '',
                'Ethnicity' => '',
                'GoToDrink' => '',
                'WhatAttractsMe' => '',
                'WhatIDoOnTheWeekend' => '',
                'HowIDescribeMyPersonality' => '',
                'HowIDescribeMyBody' => '',
                'Looks' => '',
                'Party' => '',
                'Flirty' => '',
                'Lat' => '10.769041',
                'Lon' => '106.68098',
                'NeighborhoodID' => NULL,
                'LastActivity' => '2013-3-6 10:25:21',
                'Look_description' => '',
                'Party_description' => '',
                'Flirty_description' => '',
                'Major_description' => '',
                'Shared' => 1,
                'SharedLatestDate' => NULL,
                'UserNameUpdateDate' => NULL,
                'Deactive' => 0,
                'AllowCommentActivityNotice' => 0,
                'AllowLikeActivityNotice' => 1,
                'NumberOfFollower' => 0,
                'NumberOfFollowing' => 0,
                'FlagsCount' => 0,
                'AllowFollowNotice' => 0,
                'LastCheckInID' => 2439
            );

            $this->db->insert('user', $data);

            if ($curYear < $endYear)
                $curYear += 1;
            else $curYear = $startYear;

            $curGender = ($curGender == 1) ? 2 : 1;

            // Then update the input var...
        }
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */