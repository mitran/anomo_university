<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CronMessage extends Ext_Controller
{

    function __construct()
    {
        parent::__construct();

        //Load library
        $this->load->library('AC2DM');
        $this->load->library('APNS');

        //Load model
        $this->load->model('Message_model');
        $this->load->model('Config_model');
    }

    function index()
    {
        ob_start();
        ob_clean();

        set_time_limit(0);

        $iLimit = 200;

        $oDataTracking = $this->Message_model->getMessageTracking($iLimit);

        if ($oDataTracking) {
            foreach ($oDataTracking as $key => $obj) {
                $logData['UserID'] = $obj->UserID;
                $logData['CreatedDate'] = gmdate('Y-m-d H:i:s');

                $pushStatus = 0;

                if ($obj->DeviceType == 'ios') {
                    $apnMsg['aps']['alert'] = $obj->Message;
                    $sound = 'abc.aiff';
                    $apnMsg['aps']['sound'] = (string )$sound;
                    $msgToPush = json_encode($apnMsg);

                    $logData['Platform'] = 'ios';
                    $logData['Message'] = $msgToPush;

                    //Send message to iOS device
                    $APNS = new APNS();
                    $pushStatus = $APNS->_pushMessage($logData, $msgToPush, $obj->DeviceID, $this->Config_model->getConfig('PUSH_NOTIFICATION_MODE'));
                }

                if ($obj->DeviceType == 'android') {
                    $apnMsg['aps']['alert'] = $obj->Message;
                    $sound = 'abc.aiff';
                    $apnMsg['aps']['sound'] = (string )$sound;
                    $msgToPush = json_encode($apnMsg);

                    $logData['Platform'] = 'android';
                    $logData['Message'] = $msgToPush;

                    //Send message to Android device
                    $AC2DM = new AC2DM();
                    $pushStatus = $AC2DM->send($logData, $msgToPush, $obj->DeviceID, $this->Config_model->getConfig('PUSH_NOTIFICATION_MODE'));
                }

                $this->Message_model->updateStatusTracking(array('Status' => 1), array('MessageID' => $obj->MessageID, 'UserID' => $obj->UserID));

                //If end of message, update status of message
                $iCountStatus = $this->Message_model->getTotalStatusTracking($obj->MessageID);
                if (empty($iCountStatus)) {
                    $this->Message_model->updateStatus(array('Status' => 1), array('MessageID' => $obj->MessageID));
                }
            }
        }
    }

    function update_content_owner_id()
    {
        $type = array(0, 1, 2, 6, 7, 8, 27);

        $table_name = array(
            0 => 'usercheckin', // check in
            1 => 'userprofilestatus', // change profile status
            2 => 'placepost', // post pic to place
            6 => 'placepost', // post text to place
            7 => 'neighborhoodpost', // post pic to nbh
            8 => 'neighborhoodpost', // post text to nbh
            27 => 'picturepostactivity'
        );

        for ($i = 0; $i < sizeof($type); $i++) {
            $sql = "update flagcontent, " . $table_name[$type[$i]] . " set ContentOwnerID = " . $table_name[$type[$i]] . ".UserID WHERE Type = " . $type[$i] . " AND ContentID = " . $table_name[$type[$i]] . ".ID";
            $this->db->query($sql);
        }
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */