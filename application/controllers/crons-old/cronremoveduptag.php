<?php
Class CronRemoveDupTag extends Ext_Controller{
    function __construct() {
        parent::__construct();
    }
    
    function index(){
        set_time_limit(0);
        $iLimit = 100;
        $sql = "select * from tag where TagCategoryID = 0 AND Name != '' group by Name having count(*) > 1 limit $iLimit";
        $query = $this->db->query($sql);
        $result = $query->result();
        if ($result){
            $con = mysqli_connect($this->db->hostname, $this->db->username, $this->db->password);
            foreach($result as $re){
                $subSql = "select * from tag where Name='".mysqli_real_escape_string($con, $re->Name)."'";
                $subQuery = $this->db->query($subSql);
                $tagObj = $subQuery->result();
                if($tagObj){
                    // default get first record
                    $tagID = $tagObj[0]->TagID;
                    $tagName = strtolower($re->Name);
                    
                    foreach($tagObj as $row){
                        if ($tagName == $row->Name){
                            $tagID = $row->TagID;
                        }
                    }
                    $aTagID = array(); // list of duplicate tag should be deleted
                    // get other tag and delete
                    foreach($tagObj as $obj){
                        if ($tagID != $obj->TagID){
                            $aTagID[] = $obj->TagID;
                        }
                    }
                     if (sizeof($aTagID) > 0){
                        $this->db->trans_begin();
                        $updateSql = "update IGNORE usertag set TagID = $tagID where TagID IN (".implode(',', $aTagID).")";
                        echo $updateSql.'<br />';
                        $this->db->query($updateSql);
                        
                        // 2. delete 
                        $deleteSql = "delete from tag where TagID IN (".implode(',', $aTagID).") AND TagCategoryID = 0 ";
                        echo $deleteSql.'<br />';
                        $this->db->query($deleteSql);

                        if ($this->db->trans_status() === FALSE) {
                            $this->db->trans_rollback();
                        } else {
                            $this->db->trans_commit();
                        }
                    }
                    
                    
                }
            }
        }
    }
}
?>
