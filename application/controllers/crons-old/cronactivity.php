<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CronActivity extends Ext_Controller
{

    function __construct()
    {
        parent::__construct();

        //Load model
        $this->load->model('Activity_model');
        $this->load->model('Config_model');
        $this->load->model('User_model');
        $this->load->model('Place_model');
        $this->load->model('Flag_Content_model');
        $this->lang->load('ws');
    }

    /**
     * This function perform the automated cache for the following filter ONLY:
     * $page = 1 -> 5, $type = 0, $radius = -1, $gender = 0, $from_age = 0, $to_age = 0
     *
     * The cached data will consists of the first 5 pages of the activities stream.
     * However the content for
     *  - Like status
     *  - Flag status
     * must be acquired separately for every user.
     *
     * @Author: Hung Pham
     * @Date: 20/8/2013
     * @Action: index
     */
    function index()
    {
        // run-unstop!
        ob_start();
        ob_clean();
        set_time_limit(20);

        // Initial variables
        $type = 0;
        $radius = -1;
        $gender = 0;
        $from_age = 0;
        $to_age = 0;
        $user_id = 15;
        $config = $this->Config_model->getAllConfig();
        $limit = 12;

        $response = new stdClass();

        $cb = new Couchbase("127.0.0.1:8091", "Administrator", "Bubbagump2013A", "default");

        // loop through page 1 -> 5 for cache data
        for ($page = 1; $page <= 5; $page++) {
            // initialize:
            $offset = ($page - 1) * $limit;
            $total = 0;
            $radiusOut = -1;
            $cacheKey = 'recent_activities_p' . $page;

            // Hung added... setup nearby search configuration
            $more_type = 'text';
            $activities = $this->Activity_model->getActivities($user_id, $config['SEARCH_TIME_LIMIT'], $radius, $config['SEARCH_CUTOFF_VAL'], $type, $radiusOut, $total, $offset, $limit, $config['SEARCH_RADIUS_SET'], $more_type, $gender, $from_age, $to_age);
            if ($activities) {
                $validTypes = array(0, 1, 2, 7, 27, 6, 8);
                foreach ($activities as $act) {
                    $numLike = 0;
                    $numShare = 0;
                    $numComment = 0;
                    $act->IsLike = 0; // need to acquire elsewhere

                    if (in_array($act->Type, $validTypes)) {
                        $contentInfo = $this->Activity_model->getActivitiesFeedInfo($act->RefID, $act->Type, $user_id);
                        if ($contentInfo) {
                            $numLike = $contentInfo->Like;
                            $numShare = $contentInfo->Share;
                            $numComment = $contentInfo->Comment;
                        }
                    }

                    $act->Share = $numShare;
                    $comments = null;
                    $likes = null;

                    $act->Comment = $numComment;
                    $act->Like = $numLike;

                    $act->LatestComment = (object)$comments;
                    $act->LatestLike = (object)$likes;

                    $checkinPlaceId = '';
                    $checkinPlaceName = '';
                    $checkinStatus = 0;
                    $neighborhoodId = '';
                    $neighborhoodName = '';

                    $placeCheckIn = $this->Place_model->getUserCheckinStatus($act->FromUserID);

                    if ($placeCheckIn) {
                        $checkinPlaceId = $placeCheckIn->PlaceID;
                        $checkinPlaceName = $placeCheckIn->Name;
                        $checkinStatus = $placeCheckIn->Status;
                    } else {
                        // check in neighborhood
                        $nbhObj = $this->User_model->getNeighborhoodInfoOfUser($act->FromUserID);
                        if ($nbhObj) {
                            $neighborhoodName = property_exists($nbhObj, 'NAME') ? $nbhObj->NAME : '';
                            $neighborhoodId = property_exists($nbhObj, 'NeighborhoodID') ? $nbhObj->NeighborhoodID : '';
                        }
                    }
                    $act->CheckinPlaceID = $checkinPlaceId;
                    $act->CheckinPlaceName = $checkinPlaceName;
                    $act->CheckinStatus = $checkinStatus;


                    $act->NeighborhoodID = $neighborhoodId;
                    $act->NeighborhoodName = $neighborhoodName;
                    $act->IsReported = 0;
                    $act->IsInternalShared = 0;

                    $act->OwnerCheckinPlaceID = '';
                    $act->OwnerCheckinPlaceName = '';
                    $act->OwnerCheckinStatus = 0;
                    $act->OwnerNeighborhoodID = '';
                    $act->OwnerNeighborhoodName = '';
                }
            }
            $response->Activities = $activities;
            $response->Radius = $radiusOut;
            $response->Page = $page;
            $response->TotalPage = ceil($total / $limit);
            $response->code = $this->lang->line('ok');

            $cb->set($cacheKey, $response);
        }

        exit();
    }

    function testcache()
    {
        $cb = new Couchbase("127.0.0.1:8091", "Administrator", "Bubbagump2013A", "default");
        for ($i = 0; $i <= 5; $i++)
        {
            $cacheKey = 'recent_activities_p' . $i;
            var_dump($cb->get($cacheKey));
        }

        exit();
    }

    function invalidatecache()
    {
        $cb = new Couchbase("127.0.0.1:8091", "Administrator", "Bubbagump2013A", "default");
        for ($i = 0; $i <= 5; $i++)
        {
            $cacheKey = 'recent_activities_p' . $i;
            $cb->set($cacheKey, NULL);
        }

        exit();
    }

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */