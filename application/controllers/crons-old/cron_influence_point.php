<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron_Influence_Point extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Influence_Point_model');
    }
    
    function index(){
        set_time_limit(0);
       // $sql = "update user set Point = NumberOfFollower * 10 where 1;";
        //$this->db->query($sql);
        // truncate table influence_point;
        $created_date = date('Y-m-d H:i:s', strtotime('-1 week', strtotime(gmdate('Y-m-d H:i:s'))));
        $sql = "insert into influence_point (`UserID`, `FromUserID`, `Type`, `Point`, `CreatedDate`)
            select UserID, '0' as FromUserID, '2' as Type, (NumberOfFollower * 10) as Point, '$created_date' as CreatedDate from user where NumberOfFollower > 0";
        $this->db->query($sql);

    }

}
