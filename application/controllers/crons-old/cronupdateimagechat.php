<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CronUpdateImageChat extends Ext_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{ 
                set_time_limit(0);
		$new_url = $this->config->item('s3_photo_url');
                // ofmessagearchive
                $sql = "Update ofmessagearchive SET 
                        body = CONCAT('filter_image_tag$new_url', SUBSTRING_INDEX(SUBSTR(body , 17, LENGTH(body) - 18 - 16),'/',-1), 'filter_image_untag')
                        where SUBSTR(body,1,16) = 'filter_image_tag'";
                
                $this->db->query($sql);
                
               
                // ofoffine
                $sql1 = "select username, stanza, messageID from ofoffline where SUBSTR(SUBSTRING(stanza,INSTR(stanza, '<body>')+6, (INSTR(stanza, '</body>') - (INSTR(stanza, '<body>') + 6))),1,16) = 'filter_image_tag'";
                $query = $this->db->query($sql1);
                $result = $query->result();
                if ($result){
                    foreach ($result as $re){
                        $stanza = $re->stanza;
                        $old_url = base_url().$this->config->item('photo_url');
                        $new_url = $this->config->item('s3_photo_url');
                        $newStanza = str_replace($old_url, $new_url, $stanza);
                        $sSql = "update ofoffline set stanza ='".$newStanza."'
                                where username=$re->username and messageID = $re->messageID
                                ";
                        $this->db->query($sSql);
                    }
                }
	}      
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */