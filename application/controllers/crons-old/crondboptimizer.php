<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CronDBOptimizer extends Ext_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->db_slave = $this->load->database('slave', TRUE);
    }

    function index()
    {
        ob_start();
        ob_clean();
        set_time_limit(0);

        $sql = "select concat('KILL ',id,';') from information_schema.processlist where `host` like 'ip-10-232-11-80.us-west-2.compute.internal%' and `time` > 200";

        $res = $this->db_slave->query($sql);
        $res->free_result();
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */