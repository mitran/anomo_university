<?php

class CronValentine extends Ext_Controller {
    public $cached = false;
    function __construct() {
        parent::__construct();
        $this->load->library('Cache');
        $this->cached = $this->cache->getCache();
    }

    function index() {       
        set_time_limit(0);
        $limit = 2000;
        $credit = 100;
        
        //1. Add new column
        // ALTER TABLE  `user` ADD  `IsRun` TINYINT( 4 ) NULL DEFAULT  '0' ;
        
        $sql3 = "select UserID from user where IsRun = 0 limit $limit";
        $query = $this->db->query($sql3);
        $result = $query->result();
        if ($result){
            $id = array();
            foreach($result as $re){
                $id[] = $re->UserID;
            }
            if (sizeof($id) > 0){
                // update credits for user
                $sql1 = "update user SET `Credits` = `Credits` + $credit , IsRun = 1 where UserID IN (".  implode(',', $id).")" ;
                $updated = $this->db->query($sql1);
                if ($updated){
                    if($this->cached){
                        // delete cache info
                        foreach($id as $user_id){
                            $this->cached->delete('user' . $user_id);
                        }
                    }
                }

            }
        } 
    }

}

