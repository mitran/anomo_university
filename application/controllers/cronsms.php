<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CronSms extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('MyTwilio');
        $this->load->model('Sms_model');
        $this->load->model('User_model');
    }
    
    function index(){
        set_time_limit(0);
        $limit = 50;
        $result = $this->Sms_model->getList($limit);
        $aSuccess = array();
        $aFailed = array();
        if ($result){
            $msg = "You should join me on Anomo.  It' a cool new social app where you only reveal what you want. www.anomo.com";
            foreach($result as $re){
                $status = $this->mytwilio->send($re->PhoneNumber, $msg);
                if ($status == 1){
                    $aSuccess[] = $re->ID;
                    $aPoint[] = array(
                        'UserID' => $re->UserID,
                        'PhoneNumber' => $re->PhoneNumber
                    );
                }else{
                    $aFailed[] = $re->ID;
                }
            }
            // plus point for first invite
            if (sizeof($aPoint) > 0){
                // plus point for first invite
                $this->Sms_model->plusPoint($aPoint);
            }
            
            if (sizeof($aSuccess) > 0){
                $this->Sms_model->update($aSuccess, 'success');
            }
            
            if (sizeof($aFailed) > 0){
                 $this->Sms_model->update($aFailed, 'failed');
            }
        }
        
    }

}
