<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron_Send_Email extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Blocked_User_1day_model');
        $this->load->model('Config_model');
    }
    
    function index(){
        set_time_limit(0);
        $limit = 50;
        $result = $this->Blocked_User_1day_model->getList($limit);
        if($result){
            foreach($result as $re){
                $data[] = "[$re->UserID] [$re->UserName]  has been blocked >= 2 times within 24 hours";
                $idSent[] = $re->ID;
            }
            // send email 
            $this->load->library('email');
            $this->email->from($this->config->item('email_from'), $this->config->item('email_from_name'));
            $to = $this->Config_model->getConfig('ADMIN_EMAIL');
            $this->email->to($to);
            $this->email->subject("[Anomo] Blocked User");
            $msg = implode('<br />', $data);     
            $this->email->message($msg);
            $ret = $this->email->send();
            if ($ret){
                // update 
                $this->Blocked_User_1day_model->update($idSent);
            }
        }

    }

}
