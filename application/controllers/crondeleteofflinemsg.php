<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CronDeleteOfflineMsg extends Ext_Controller
{
    var $_deleteOldMsg = 0; // 0: sleep; 1: enabled; 2: completed

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        // Remove already pushed messages OR messages that always failed
//        $sql2 = "delete from offlinemessagenotification where IsPush = 1 or (IsPush = 0 and FailCount >= 3)";
//        $this->db->query($sql2);

        // Remove messages that will never get pushed every once in a day... (at 7:00AM GMT)
        if (date("H:i:s") >= '07:00:00' && date("H:i:s") < '07:30:00')
            if ($this->_deleteOldMsg == 0) $this->_deleteOldMsg = 1;
            else
                $this->_deleteOldMsg = 0;

        if ($this->_deleteOldMsg == 1)
        {
            // delete from chat message push queue
//            $sql3 = "delete from offlinemessagenotification where DATEDIFF(NOW(),CreatedDate) > 2";
//            $this->db->query($sql3);

            // delete from comments push notifications push queue
            $sql4 = "delete from cronpushnotification where DATEDIFF(NOW(),CreatedDate) > 2";
            $this->db->query($sql4);

            // delete old notification history since they are no longer be used
            $time = "-7 day";
            $sql5 = "delete from notificationhistory where CreatedDate <'". date('Y-m-d', strtotime($time, strtotime(gmdate('Y-m-d')))) . "'";
            $this->db->query($sql5);

            // run table optimzation for notification
            $sql6 = "optimize table notificationhistory";
            $this->db->query($sql6);

            $this->_deleteOldMsg = 2;
        }

        //2013-12-30 - Mass msg Admin
        // Remove already sent msg
        $sql = "delete from message_tracking where Status = 1";
        $this->db->query($sql);

        //2014-01-17
        // cron mention user
        $sql = "delete from cronmentionuser where IsPush = 1 or (IsPush = 0 and FailCount >= 3)";
        $this->db->query($sql);
    }
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */