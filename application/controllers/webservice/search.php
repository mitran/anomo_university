<?php

require_once(APPPATH . 'libraries/SolrPHPClient/Service.php');

class Search extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->solr = new Apache_Solr_Service(SOLR_HOSTNAME, SOLR_PORT, SOLR_PATH);
        $this->load->model('Wstoken_model');
        $this->lang->load('ws');
    }

    function user($token = '', $page = 1) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!isset($_POST['Keyword']) || empty($_POST['Keyword'])) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $keywords = $_POST['Keyword'];
                $limit = 10;
                $offset = ($page - 1) * $limit;
                $total = 0;
                $user_id = $tokenInfo->UserID;
                $query = "(name:$user_id AND keywords:$keywords*) OR (resourcename:$user_id AND description:$keywords*)";
                $result = array();
                $responseSearch = $this->solr->search($query, $offset, $limit);
                if ($responseSearch->getHttpStatus() == 200) {
                    if ($responseSearch->response->numFound > 0) {
                        $total = $responseSearch->response->numFound;
                        foreach ($responseSearch->response->docs as $doc) {
                            if ($doc->name == $user_id) {
                                $result[$doc->resourcename] = array(
                                    'UserID' => $doc->resourcename,
                                    'UserName' => $doc->keywords
                                );
                            } else {
                                $result[$doc->name] = array(
                                    'UserID' => $doc->name,
                                    'UserName' => $doc->description
                                );
                            }
                        }
                    }
                    $response->code = $this->lang->line('ok');
                } else {
//                    echo $responseSearch->getHttpStatusMessage();
                    $response->code = $this->lang->line('failed');
                }
                $response->ListUser = array_values($result);
                $response->Page = $page;
                $response->TotalPage = ceil($total / $limit);
            }
        }
        echo json_encode($response);
    }

}

?>
