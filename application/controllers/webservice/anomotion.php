<?php
class Anomotion extends Ext_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Anomotion_model');
        $this->load->model('Notification_History_model');
        $this->load->model('Config_model');
        $this->load->model('Question_model');
        $this->load->model('Result_model');
        $this->load->model('Wstoken_model');
        $this->load->model('Neighborhood_model');
        $this->lang->load('ws');
        $this->load->library('MyPushNotify');
    }

    private function _generateQuestion($userObj, $targetUserObj) {
        // ANOMO-7089 - teens group
        $category = '';
        $birthdate = $userObj->BirthDate ? $userObj->BirthDate : '';
        $age_range = $this->User_model->returnAge($birthdate);
        $birthdate1 = $targetUserObj->BirthDate ? $targetUserObj->BirthDate : '';
        $age_range1 = $this->User_model->returnAge($birthdate1);
        // if logged in user is teenager, should return question in teen group
        if ($age_range["to"] == 17 || $age_range1["to"] == 17) {
            $category = "TEEN";
        }

        if ($category == '') {
            // ANOMO-4999 - play game choosen by gender - default is MF
            $userGender = $userObj->Gender;
            $targetGender = $targetUserObj->Gender;
            $genders = array(
                1 => 'M',
                2 => 'F'
            );
            $category = 'MF';
            if (in_array($userGender, array_keys($genders)) && in_array($targetGender, array_keys($genders))) {
                $category = $genders[$userGender] . $genders[$targetGender];
                if ($category == 'FM') {
                    $category = 'MF';
                }
            }
        }
        // ANOMO-5107 - avoid question repeat
        $userPlayed = $this->Question_model->getQuestionPlayed($userObj->UserID, $category);
        $targetPlayed = $this->Question_model->getQuestionPlayed($targetUserObj->UserID, $category);
        $gamePlayed = array();
        if ($userPlayed != '') {
            $gamePlayed = explode(',', $userPlayed);
        }
        if ($targetPlayed != '') {
            $gamePlayed = array_merge($gamePlayed, explode(',', $targetPlayed));
        }
        $gamePlayedStr = '';
        if (!empty($gamePlayed)) {
            $gamePlayedStr = implode(',', $gamePlayed);
        }

        $generateQuestion = $this->Question_model->randomQuestion($category, $gamePlayedStr);
        if (!$generateQuestion) {
            $generateQuestion = $this->Question_model->randomQuestion($category, $userPlayed);
            if (!$generateQuestion) {
                $generateQuestion = $this->Question_model->randomQuestion($category);
                // reset question played
                if ($generateQuestion) {
                    $this->Question_model->updateQuestionPlayed(array('QuestionGroupIDs' => $generateQuestion[0]->QuestionGroupID), array('UserID' => $userObj->UserID, 'Category' => $category));
                }
            }
        }

        if ($generateQuestion) {
            // ANOMO-5107 log this question-group is played
            // 1. for request user
            $question_group_id = isset($generateQuestion[0]->QuestionGroupID)?$generateQuestion[0]->QuestionGroupID:0;
            if ($userPlayed != '') {
                $questionPlayed = explode(',', $userPlayed);
                if (!in_array($question_group_id, array_values($questionPlayed))) {
                    $questionPlayed[] = $question_group_id;
                    $newQuestionGroupIDs = implode(',', $questionPlayed);
                    // if existed then update
                    $this->Question_model->updateQuestionPlayed(array('QuestionGroupIDs' => $newQuestionGroupIDs), array('UserID' => $userObj->UserID, 'Category' => $category));
                }
            } else {
                // else insert new
                $newQuestionGroupIDs = $question_group_id;
                $this->Question_model->addQuestionPlayed(array('UserID' => $userObj->UserID, 'QuestionGroupIDs' => $newQuestionGroupIDs, 'Category' => $category));
            }

            // 2. for parner
            if ($targetPlayed != '') {
                $questionPlayed = explode(',', $targetPlayed);
                if (!in_array($question_group_id, array_values($questionPlayed))) {
                    $questionPlayed[] = $question_group_id;
                    $newQuestionGroupIDs = implode(',', $questionPlayed);
                    // if existed then update
                    $this->Question_model->updateQuestionPlayed(array('QuestionGroupIDs' => $newQuestionGroupIDs), array('UserID' => $targetUserObj->UserID, 'Category' => $category));
                }
            } else {
                // else insert new
                $newQuestionGroupIDs = $question_group_id;
                $this->Question_model->addQuestionPlayed(array('UserID' => $targetUserObj->UserID, 'QuestionGroupIDs' => $newQuestionGroupIDs, 'Category' => $category));
            }
            // end ANOMO-5107
        }
        return $generateQuestion;
    }
    

    /**
     * get all anomotions
     *
     * @param string $token
     * @param int $userID
     * @return json
     */
    function get_all($token = '') {
        $response = new stdClass();
        $userID = $this->commonlib->checkValidToken($token);
        if ($userID == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $results = $this->Anomotion_model->getAnomotionsByUserID($userID);
            foreach ($results as &$entry) {
                //modify status
                $status = '';
                switch ($entry->Status) {
                    case 0:
                        $status = ($userID == $entry->UserID) ? $this->lang->line('waiting') : $this->lang->line('request_waiting');
                        break;
                    case 1:
                        $status = $this->lang->line('inprogress');
                        break;
                    case 2:
                        $status = $this->lang->line('cancel');
                        break;
                    case 3:
                        $status = $this->lang->line('finished');
                        break;
                }
                $entry->Status = $status;

                //add full image path
                if ($entry->TargetAvatar) {
                    $entry->TargetAvatar = $this->config->item('s3_photo_url') . $entry->TargetAvatar;
                }

                //get ConnectMeter
                $entry->ConnectMeter = 1;
            }
            $response->results = $results;
            $response->code = $this->lang->line('ok');
        }
        echo json_encode($response);
    }

    /**
     * get_all_anomotion_between_2_user
     *
     * @param string $token
     * @param int $userID
     * @param int $targetUserID
     * @return json
     */
    function get_all_anomotion_between_2_user($token = '', $targetUserID = 0)
    {
        $response = new stdClass();
        $userID = $this->commonlib->checkValidToken($token);
        if ($userID == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $response->code = $this->lang->line('invalid_request');
                $results = $this->Anomotion_model->getAnomotionsBy2User($userID, $targetUserID);
                foreach ($results as &$entry) {
                    //modify status
                    $status = '';
                    switch ($entry->Status) {
                        case 0:
                            $status = ($userID == $entry->UserID) ? $this->lang->line('waiting') : $this->lang->line('request_waiting');
                            break;
                        case 1:
                            $status = $this->lang->line('inprogress');
                            break;
                        case 2:
                            $status = $this->lang->line('cancel');
                            break;
                        case 3:
                            $status = $this->lang->line('finished');
                            break;
                    }
                    $entry->Status = $status;

                    //add full image path
                    if (isset($entry->Avatar) && !empty($entry->Avatar)) {
                        $entry->Avatar = $this->config->item('s3_photo_url') . $entry->Avatar;
                    }
                    if (isset($entry->TargetAvatar) && !empty($entry->TargetAvatar)) {
                        $entry->TargetAvatar = $this->config->item('s3_photo_url') . $entry->TargetAvatar;
                    }
                    //get ConnectMeter
                    $entry->ConnectMeter = 1; 
                }
                $response->results = $results;
                $response->code = $this->lang->line('ok');
           
        }
        echo json_encode($response);
    }

    /**
     * get_finish_anomotion
     *
     * @param string $token
     * @param int $anomotion_id
     * @return json
     */
    function get_finish_anomotion($token = '', $anomotion_id = 0) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $isGame = $this->Anomotion_model->checkAnomotion($tokenInfo->UserID, $anomotion_id);
            if ($isGame && $isGame->Status == 3) {
                $gameObj = $this->Anomotion_model->getFinishedGame($anomotion_id);
                $target_user_id = ($isGame->UserID == $tokenInfo->UserID) ? $isGame->TargetUserID : $isGame->UserID;
                $targetUserInfo = $this->User_model->getUserDetail($target_user_id);
                if ($gameObj && $targetUserInfo) {
                    $results = array();
                    $image = '';
                    $questions = array();
                    foreach ($gameObj as $re) {
                        $questions[$re->QuestionID] = $re->Question;
                    }

                    foreach ($questions as $key => $value) {
                        $answers = array();
                        foreach ($gameObj as $row) {
                            if ($key == $row->QuestionID) {
                                $answers[] = array(
                                    'UserID' => $row->UserID,
                                    'Answer' => $row->Answer
                                );
                            }
                        }
                        $results[] = array(
                            'Question' => $value,
                            'Answer' => $answers
                        );
                    }

                    // return fb share img
                    $image_fb = FCPATH . 'public/share/' . $anomotion_id . '_' . $tokenInfo->UserID . '.jpg';
                    if (file_exists($image_fb)) {
                        $image = base_url() . 'public/share/' . $anomotion_id . '_' . $tokenInfo->UserID . '.jpg';
                    }
                    $response->CreatedDate = $isGame->CreatedDate;
                    $response->Score = $isGame->Score;
                    $response->Image = $image;
                    $response->Result = $results;
                    $response->code = $this->lang->line('ok');

                    // should return information of partner
                    // userid, username, avatar, birthdate, credits, neighborhoodid, nbhname
                    $response->TargetUserID = $targetUserInfo->UserID;
                    $response->TargetUserName = $targetUserInfo->UserName;
                    $response->Avatar = $targetUserInfo->Avatar;
                    $response->BirthDate = $targetUserInfo->BirthDate;
                    $response->FacebookID = $targetUserInfo->FacebookID;
                    $response->CoverPicture = $targetUserInfo->CoverPicture;
                    $response->Credits = $targetUserInfo->Credits;
                    $response->NeighborhoodID = $targetUserInfo->NeighborhoodID;
                    $response->NeighborhoodName = $targetUserInfo->NeighborhoodName;
                    $response->IsAdmin = $targetUserInfo->IsAdmin;
                } else {
                    $response->code = $this->lang->line('invalid_request');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }

    /**
     * delete anomotion
     *
     * @param string $token
     * @param int $anomotion_id
     * @return json
     */
    function delete_anomotion($token = '', $anomotion_id = 0){
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
             $checkAnomotion = $this->Anomotion_model->checkAnomotion($user_id, $anomotion_id);
             if (is_numeric($anomotion_id) && $checkAnomotion) {
                if ($checkAnomotion->Status == 3) { // finished
                    $userDelete = ($checkAnomotion->UserID == $user_id) ? 'IsUserDelete' : 'IsTargetUserDelete';
                    $this->Anomotion_model->update(array($userDelete => 1), array('AnomotionID' => $anomotion_id));
                } else {
                    // change status => CANCEL
                    $this->Anomotion_model->update(array('Status' => 2), array('AnomotionID' => $anomotion_id));
                }
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }

    /**
    age <= 17 : teen
    18 >= age <= 22: college
    23 >= age < 30 : twenties
    30>= age <40 : thirties
    40>= age < 50 : forties
    50 >= age : fifties
     *
     * @param unknown_type $token
     */
    function random_anomo($token = '')
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            /* ANOMO-8462
            For males: 
            - Use "EVERYONE" question set 
            - Choose 1 male active from past 2 hours (same age group) 
            - Choose 1 female active from past 2-24 hours (same age group) 
            - Choose 1 female active between 24-72 hours ago (same age group) 
            - Choose 1 female active between 3-7 days ago (same age group) 

            For females: 
            - Use "EVERYONE" question set 
            - Choose 1 male active from past 2 hours (same age group) 
            - Choose 1 female active from past 2-24 hours (same age group) 
            - Choose 1 male active between 24-72 hours ago (same age group) 
            - Choose 1 female active between 3-7 days ago (same age group)
             * 
             */
            $dateOnline = date('Y-m-d H:i:s', strtotime('-2 hour', strtotime(gmdate('Y-m-d H:i:s'))));
            $date24h = date('Y-m-d H:i:s', strtotime('-24 hour', strtotime(gmdate('Y-m-d H:i:s'))));
            $date72h = date('Y-m-d H:i:s', strtotime('-72 hour', strtotime(gmdate('Y-m-d H:i:s'))));
            $date3d = date('Y-m-d H:i:s', strtotime('-3 day', strtotime(gmdate('Y-m-d H:i:s'))));
            $date7d = date('Y-m-d H:i:s', strtotime('-7 day', strtotime(gmdate('Y-m-d H:i:s'))));
            $date1month = date('Y-m-d H:i:s', strtotime('-1 month', strtotime(gmdate('Y-m-d H:i:s'))));
            $user_id = $tokenInfo->UserID;
            $birth_date = ($tokenInfo->BirthDate) ? $tokenInfo->BirthDate : '';
            $age_from = '';
            $age_to = '';
            if ($birth_date != '') {
                $age = $this->User_model->returnAge($birth_date);
                $age_from = isset($age['from']) ? $age['from'] : '';
                $age_to = isset($age['to']) ? $age['to'] : '';
            }

            $gender = ($tokenInfo->Gender) ? $tokenInfo->Gender : '';
            if ($gender != '') {
                $genderR = $gender;
                $gender = ($gender == 1) ? 2 : 1;
            }

            $userOnline = $this->User_model->returnUserForGame4People($user_id, $dateOnline, '', 1, $age_from, $age_to, $gender);

            $number = 1;
            if (!$userOnline) {
                $number = 2;
            }
            $user24h = $this->User_model->returnUserForGame4People($user_id, $date24h, $dateOnline, $number, $age_from, $age_to, 2);

            $number = 1;
            if (!$user24h) {
                $number = 2;
                if (!$userOnline) {
                    $number = 3;
                }
            }
            $user1month = $this->User_model->returnUserForGame4People($user_id, $date72h, $date24h, $number, $age_from, $age_to, 1);

            $number = 1;
            if (!$user1month) {
                $number = 3;
                if (!$user24h) {
                    $number = 4;
                }
            }
            $userOther = $this->User_model->returnUserForGame4People($user_id, $date7d, $date3d, $number, $age_from, $age_to, 2);

            $users = array_merge($userOnline, $user24h, $user1month, $userOther);

            $iTotal = sizeof($users);
            if ($iTotal < 4) {
                $i = 4 - $iTotal;
                // And then… failing that… use anyone active in last 30 days
                $userOther = $this->User_model->returnUserForGame4People($user_id, '', $date7d, $i, '', '', 0, 1);
                if ($userOther) {
                    $users = array_merge($users, $userOther);
                }
            }
            
            // ANOMO-12516 - fix if don't find out anyone ... random anyone active in 30 days
            if (!$users || sizeof($users) < 4){
                $users = $this->User_model->returnUserForGame4People($user_id, '', '', 4, '', '', 0, 1);
            }

            if ($users) {
                foreach ($users as $user) {
                    $user->Tags = $this->User_model->getListTagOf($user->UserID);

                    $checkinPlaceId = '';
                    $checkinPlaceName = '';
                    $checkinStatus = 0;
                    $neighborhoodId = '';
                    $neighborhoodName = '';

                    if (isset($user->NeighborhoodID) && !empty($user->NeighborhoodID)) {
                        $neighborhoodId = $user->NeighborhoodID;
                        $neighborhoodInfo = (object)$this->Neighborhood_model->getInfo($neighborhoodId);
                        $neighborhoodName = property_exists($neighborhoodInfo, 'NAME') ? $neighborhoodInfo->NAME : '';
                    }

                    $user->CheckinPlaceID = $checkinPlaceId;
                    $user->CheckinPlaceName = $checkinPlaceName;
                    $user->CheckinStatus = $checkinStatus;
                    $user->NeighborhoodID = $neighborhoodId;
                    $user->NeighborhoodName = $neighborhoodName;
                }
                $response->Users = $users;
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('failed');
            }
        }
        echo json_encode($response);
    }

   

    /**
        delete all game bwt 2 users
     * 
     * 
     */
    function delete_all_game($token = '', $target_user_id = 0)
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($target_user_id)) {
                // get all game of 2 users
                $allAnomotion = $this->Anomotion_model->getAllAnomotionBy2User($tokenInfo->UserID, $target_user_id);
                if ($allAnomotion) {
                    foreach ($allAnomotion as $anomotion) {
                        if ($anomotion->Status == 3) { // finished
                            $userDelete = ($anomotion->UserID == $tokenInfo->UserID) ? 'IsUserDelete' : 'IsTargetUserDelete';
                            $this->Anomotion_model->update(array($userDelete => 1), array('AnomotionID' => $anomotion->AnomotionID));
                        } else {
                            $aCancel[] = $anomotion->AnomotionID;
                        }
                    }
                    if (isset($aCancel)){
                         $this->Anomotion_model->update(array('Status' => 2), "AnomotionID IN (".  implode(',', $aCancel).")");
                    }
                }
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }

    /**Random question for game 4 peoples
     * 
     * @param type $token
     */
    function random_question($token = '')
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            // ANOMO-8462 Use "EVERYONE" question set 
            $category = 'EVERYONE';
            $user_id = $tokenInfo->UserID;

            // ANOMO-5107 - avoid question repeat
            $userPlayed = $this->Question_model->getQuestionPlayed($user_id, $category);
            $generateQuestion = $this->Question_model->randomQuestion($category, $userPlayed);
            if (!$generateQuestion) {
                $generateQuestion = $this->Question_model->randomQuestion($category);
                if (!$generateQuestion) {
                    $generateQuestion = $this->Question_model->randomQuestion();
                }
            }

            if (!$generateQuestion) {
                $response->code = $this->lang->line('failed');
            } else {
                foreach ($generateQuestion as $re) {
                    $re->Answer = $this->Question_model->getAnswerOf($re->QuestionID);
                }
                $response->Question = $generateQuestion;
                $response->code = $this->lang->line('ok');
            }
        }
        echo json_encode($response);
    }

    /**create new game or answer game
     * 
     * @param type $token
     * GameResult as Json format:
     * 
     *  {
            "UserID":[1,2,3,4],
            "Questions":[
                {"QuestionID":1,"AnswerID":2,"Order":1},
                {"QuestionID":2,"AnswerID":3,"Order":2},
                {"QuestionID":3,"AnswerID":4,"Order":3},
                {"QuestionID":4,"AnswerID":5,"Order":4},
                {"QuestionID":5,"AnswerID":6,"Order":5}
            ],
            "QuestionGroupID":100,
            "AnomotionID": -1
        }
     */
    function create_game_4people($token = '') {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!isset($_POST['GameResult'])) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $sJson = $_POST['GameResult'];
                $oData = json_decode($sJson);
                if (!isset($oData->UserID) || !isset($oData->Questions) || !isset($oData->QuestionGroupID) || in_array($tokenInfo->UserID, $oData->UserID)) {
                    $response->code = $this->lang->line('invalid_parameter');
                } else {
                    $validateData = $this->Question_model->validateGameResult($oData);
                    if (!$validateData) {
                        $response->code = $this->lang->line('invalid_parameter');
                    } else {
                        $oData = $validateData;
                        $anomotion_id = (isset($oData->AnomotionID)) ? $oData->AnomotionID : -1;
                        $oData->AnomotionID = $anomotion_id;
						/*
                        if ($oData->AnomotionID == -1) {
                            $aUserIds = self::validateGamePlaying($tokenInfo->UserID, $oData->UserID);
                            if (sizeof($aUserIds) > 0) {
                                $oData->UserID = $aUserIds;
                            }
                        }*/
                        if (sizeof($oData->UserID) > 0) {
                            $return = $this->Question_model->createGame4People($tokenInfo->UserID, $oData);
                            if (!$return) {
                                $response->code = $this->lang->line('failed');
                            } else {
                                $ids = $return['ids'];
                                $is_finished = isset($return['is_finished']) ? $return['is_finished'] : 0;
                                $historyType = $is_finished == 1 ? 4 : 3;
                                $pushType = $is_finished == 1 ? 11 : 10;
                                $msg = $is_finished == 1 ? str_replace('%username%', $tokenInfo->UserName, $this->lang->line('msg_anomotion_completed')) : $this->lang->line('msg_anomotion_request');
                                $historyObj = $this->Anomotion_model->getHistoryIdOfGame($tokenInfo->UserID, $historyType, $ids);
                                if ($historyObj) {
                                    foreach ($historyObj as $obj) {
                                        if ($obj->AllowAnomotionNotice == 1){
                                            $this->mypushnotify->send($obj->UserID, $tokenInfo->UserID, $pushType, $msg, array('HistoryID' => $obj->ID, 'AnomotionID' => $obj->AnomotionID));
                                        }
                                    }
                                }
                                
                                // return game id
                                $response->AnomotionID = sizeof($ids)>1?-1:$ids[0];
                                
                                if ($is_finished == 1) {
                                    $response->code = $this->lang->line('finished');
                                } else {
                                    $response->code = $this->lang->line('ok');
                                }
                            }
                        } else {
                            $response->code = $this->lang->line('invalid_request');
                        }
                    }
                }
            }
        }
        echo json_encode($response);
    }

    /**check is pending game btw 2 users
     * 
     * @param type $user_id
     * @param type $target_user_ids
     * @param type $is_facebook_game
     * @return type
     */
    private function validateGamePlaying($user_id, $target_user_ids)
    {
        $data = array();
        foreach ($target_user_ids as $target_user_id) {
            $existGame = $this->Anomotion_model->checkRequest($user_id, $target_user_id);
            if (!$existGame) {
                $data[] = $target_user_id;
            }
        }
        return $data;
    }
    
    /**Request single game btw 2 users
     * 
     * @param type $token
     * @param type $target_user_id
     */
    function request_single_game($token = '', $target_user_id = '', $anomotion_id = 0) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $user_id = $tokenInfo->UserID;
            if (is_numeric($target_user_id) && $user_id != $target_user_id) {
                $block_status = $this->User_model->returnBlockStatus($user_id, $target_user_id);
                if ($block_status != '') {
                    $response->code = $block_status;
                } else {
                    $targetUserInfo = $this->User_model->getUserDetail($target_user_id);
                    if ($targetUserInfo) {
                        $is_new_game = 0;
                        $gameObj = $this->Anomotion_model->isHavePendingGame($user_id, $target_user_id, $anomotion_id);

                        if ($gameObj) { // incase have a pending game
                            if ($gameObj->Status == 3){ // this game is finished
                                $response->code = $this->lang->line('finished');
                            }elseif ($gameObj->Status == 1 && $gameObj->CreatedDate > gmdate("Y-m-d H:i:s", strtotime($this->config->item('expired')))){
                                $response->AnomotionID = $gameObj->AnomotionID;
                                // check is complete game
                                $objIsComplete = $this->Anomotion_model->isCompleteGame($user_id, $gameObj->AnomotionID, $gameObj->QuestionGroupID);
                                if ($objIsComplete){
                                    if ((int)$objIsComplete->TotalAnswer == (int)$objIsComplete->TotalQuestion){
                                        $response->code = $this->lang->line('completed');
                                    }else{ // return all question
                                        $response->code = $this->lang->line('ok');
                                        // return all question of game
                                        $generateQuestion = $this->Question_model->getQuestionOfGroup($gameObj->QuestionGroupID);
                                        foreach ($generateQuestion as $re) {
                                            $re->Answer = $this->Question_model->getAnswerOf($re->QuestionID);
                                        }
                                        $response->Question = $generateQuestion;
                                    }
                                }
                            }else{
                                $is_new_game = 1;
                            }
                        }else{
                            $is_new_game = 1;
                        }
                        
                        if ($is_new_game) { // incase new game
                            // generate new question 
                            $generateQuestion = self::_generateQuestion($tokenInfo, $targetUserInfo);
                        
                            // return list question of game
                            if (!$generateQuestion) {
                                $response->code = $this->lang->line('failed');
                            } else {
                                foreach ($generateQuestion as $re) {
                                    $re->Answer = $this->Question_model->getAnswerOf($re->QuestionID);
                                }
                                $response->Question = $generateQuestion;
                                $response->code = $this->lang->line('ok');
                            }
                        }
                        
                        // should return information of partner
                        // userid, username, avatar, birthdate, credits, neighborhoodid, nbhname
                        $response->TargetUserID = $targetUserInfo->UserID;
                        $response->TargetUserName = $targetUserInfo->UserName;
                        $response->Avatar = $targetUserInfo->Avatar;
                        $response->BirthDate = $targetUserInfo->BirthDate;
                        $response->FacebookID = $targetUserInfo->FacebookID;
                        $response->CoverPicture = $targetUserInfo->CoverPicture;
                        $response->Credits = $targetUserInfo->Credits;
                        $response->NeighborhoodID = $targetUserInfo->NeighborhoodID;
                        $response->NeighborhoodName = $targetUserInfo->NeighborhoodName;
                        $response->IsAdmin = $targetUserInfo->IsAdmin;
                        
                    } else {
                        $response->code = $this->lang->line('invalid_request');
                    }
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }
}

?>