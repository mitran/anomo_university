<?php

class Activity extends Ext_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Activity_model');
        $this->load->model('Wstoken_model');
        $this->load->model('Place_model');
        $this->load->model('Neighborhood_model');
        $this->load->model('User_model');
        $this->load->model('Flag_Content_model');
        $this->load->model('Place_Activity_model');
        $this->load->model('Notification_History_model');
        $this->load->model('Cron_Push_Notification_model');
        $this->load->model('Config_model');
        $this->load->model('Content_Share_model');
        $this->load->model('Hashtag_model');
        $this->load->model('Announcement_model');
        $this->load->model('Stop_Receive_Notify_model');
        $this->load->model('Tag_model');
        $this->load->model('Influence_Point_model');
        $this->load->model('Promos_model');
        $this->load->model('Topic_model');
        $this->load->model('Topic_Category_model');
        $this->load->library('MyPushNotify');
        $this->lang->load('ws');
    }

    /**
     * - Age
    0: All
    1: Teen - age < 18 && age > 0
    2: College - age >= 18 && age <= 22
    3: Twenties - age >= 23 && age < 30
    4: Thirties - age >= 30 && age < 40
    5: Forties - age >= 40 && age < 50
    6: Fifties - age >= 50

    - Gender (0: both, 1: male, 2: female)
    - Type
    0: Recent
    1: Nearby
    2: Popular
    3: Following
    4: Search hashtag (anywhere)
    5: Search nearby hashtag
    6: Search following hashtag
     *
     * @param string $token
     * @param int $page
     * @param int $type
     * @param int $radius
     * @param int $gender
     * @param int $age
     */
    
    function get_activities($token = '', $page = 1, $type = 0, $radius = -1, $gender = 0, $from_age = 0, $to_age = 0, $activity_id = 0, $total = 0) {
        // log input parameters
       // $this->commonlib->myLog('v200 activity.php get_activities', array('token' => $token, 'page' => $page, 'type' => $type, 'radius' => $radius, 'gender' => $gender, 'from_age' => $from_age, 'to_age' => $to_age, 'activity_id' => $activity_id, 'total' => $total));

        // Hung's temporary fix to db lock issue: set time limit to (20s)
        set_time_limit(20);
        
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);

        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            
            // if filter by topic
            // first checking this topic is valid
            if (isset($_POST['TopicID'])){
                if (is_numeric($_POST['TopicID']) && $_POST['TopicID'] > 0){
                    $topicInfo = $this->Topic_model->checkValidTopic($_POST['TopicID'], $tokenInfo->UserID);
                    if (!$topicInfo){
                        $response->code = $this->lang->line('invalid_request');
                        echo json_encode($response);
                        exit();
                    }else{
                        if ($topicInfo->IsTopicBan == 1){
                            $response->code = $this->lang->line('topic_banned');
                            echo json_encode($response);
                            exit();
                        }
                    }
                }
            }
            
            // if filter by category
            // first checking this category is valid
            if (isset($_POST['CateID'])){
                $iValid = 0;
                if (is_numeric($_POST['CateID']) && $_POST['CateID'] > 0){
                    $cateInfo = $this->Topic_Category_model->getCateInfo($_POST['CateID']);
                    if ($cateInfo){
                        $iValid = 1;
                    }
                }
                if (!$iValid){
                    $response->code = $this->lang->line('invalid_request');
                    echo json_encode($response);
                    exit();
                }
            }
            
            // this will do update last activity when pull down to refresh
            if ($page == 1)
                $this->User_model->updateLastActivity($tokenInfo->UserID);

            // 2014-06-18 - Eric's feedback
            if (($gender < 0) || ($gender > 2)) {
                $gender = 0;
            }

            if ($from_age < 0)
                $from_age = 0;

            if ($to_age < 0)
                $to_age = 0;

            if ($to_age < $from_age)
                $to_age = $from_age;

            // ANOMO-5958
            $limit = 24;
            $offset = ($page - 1) * $limit;
            $radiusOut = -1;
            $total = $total * $limit;

            // ANOMO-7852 - search hashtag
            $hashtag = isset($_POST['HashTag']) ? trim($_POST['HashTag']) : '';
            if ($hashtag != '') {
                $hashtag = $this->Hashtag_model->clean($hashtag);
            }
            // if user in vegas then Nearby tab will be show the post have #vegas hashtag
            // if user in vegas then the promos post will be show as the fourth post on the first screen on Recent, Following, Popular
            // need to log the current type for showing the promos post as the fourth post on the first screen
            $current_type = $type;
            $isVegas = 0;
            /*
            if ($type == FEED_NEARBY || (in_array($type, array(FEED_RECENT, FEED_POPULAR, FEED_FOLLOWING)) && $activity_id == 0 && $page == 1)) {
                $lat = isset($_POST['Lat'])?$_POST['Lat']:'';
                $lon = isset($_POST['Lon'])?$_POST['Lon']:'';
                if ($lat != '' && $lon != ''){
                    $isVegas = $this->commonlib->isVegas($lat, $lon); 
                    if ($isVegas == 1 && $type == FEED_NEARBY && empty($hashtag)){
                        $type = FEED_RECENT; // search hashtag
                        $hashtag = VEGAS_HASHTAG;
                    }
                }
            }*/
            
            // ANOMO-12123 - new stream flow
            // enable to search by topic :D
            $topic_id = isset($_POST['TopicID'])?$_POST['TopicID']:0;
            $cate_id = isset($_POST['CateID'])?$_POST['CateID']:0;
            
            // Hung added... setup nearby search configuration
            $config = $this->Config_model->getNearbySearchConfig();
            $activities = $this->Activity_model->getActivities($tokenInfo->UserID, $config['SEARCH_TIME_LIMIT'], $radius, $config['SEARCH_CUTOFF_VAL'], $type, $radiusOut, $total, $offset, $limit, $config['SEARCH_RADIUS_SET'], $gender, $from_age, $to_age, $activity_id, $hashtag, $tokenInfo->Lat, $tokenInfo->Lon, $topic_id, $cate_id);
            
            // ANOMO-12424 - disable popular tab
            $disable_popular = 0;
            if ($topic_id > 0 && $page == 1 && $activity_id == 0 && $type == FEED_POPULAR){
                if ($total < $this->Config_model->getConfig('MIN_TOTAL_POPULAR')){
                    $disable_popular = 1;
                }else{
                    $total_topic = $this->Activity_model->getTotalTopicByCond($tokenInfo->UserID, $gender, $from_age, $to_age, $topic_id);
                    if ($total_topic < $this->Config_model->getConfig('MIN_TOTAL_POST')){
                        $disable_popular = 1;
                    }
                }
                
            }
            if ($disable_popular){
                $response->code = $this->lang->line('ok');
                $response->DisablePopular = $disable_popular;
            }
            else{
                
          
            
            if ($activities) {
                $validTypes = array(ACTIVITY_POST_STATUS, ACTIVITY_POST_PICTURE, ACTIVITY_POST_VIDEO, ACTIVITY_NEW_USER);
                foreach ($activities as $key => $act) {
                    $act->IsReported = (int) $act->IsReported;
                    $act->IsBlock = (int) $act->IsBlock;
                    $act->IsBlockTarget = (int) $act->IsBlockTarget;
                    $numLike = "0";
                    $numShare = "0";
                    $numComment = "0";
                    $isComment = 0;

                    if (in_array($act->Type, $validTypes)) {
                        $contentInfo = $this->Activity_model->getActivitiesFeedInfo($act->RefID, $act->Type, $tokenInfo->UserID, $type, $act->FromUserID);
                        if ($contentInfo) {
                            $numLike = $contentInfo->Like;
                            $numComment = $contentInfo->Comment;
                            $isComment = $contentInfo->IsComment;
                            if ($type == FEED_RECENT) {
                                $act->IsLike = $contentInfo->IsLike;
                                $act->IsBlock = $contentInfo->IsBlock;
                                $act->IsBlockTarget = $contentInfo->IsBlockTarget;
                                $act->IsReported = $contentInfo->IsReported;
                                $act->IsStopReceiveNotify = $contentInfo->IsStopReceiveNotify;
                                $act->IsFavorite = $contentInfo->IsFavorite;
                            }
                        }
                    }

                    // tags
                    // ANOMO-7067
                    $tags = array();
                    
                    if ($act->Type == ACTIVITY_NEW_USER && $act->FromUserID != $tokenInfo->UserID) { // new register user
                        $tags = $this->User_model->getListTagOf($act->FromUserID);
                    }
                    
               
                    $act->Tags = $tags;
                    $comments = null;
                    $likes = null;

                    $act->IsComment = $isComment;
                    $act->Comment = $numComment;
                    $act->Like = $numLike;

                    //$act->LatestComment = (object) $comments;
                    //$act->LatestLike = (object) $likes;

                    // for owner - checkin
                    /* ANOMO-11220 : Turn off all these keys from activity feed dict
                    $act->OwnerCheckinPlaceID = '';
                    $act->OwnerCheckinPlaceName = '';
                    $act->OwnerCheckinStatus = 0;
                    $act->OwnerNeighborhoodID = '';
                    $act->OwnerNeighborhoodName = '';
                    */
                    
                    // Jan_22_2014
                    // temporary fix:  return current user location
                    // waiting the cron job update data
                    if (empty($act->NeighborhoodID)) {
                        $nbhObj = $this->User_model->getNeighborhoodInfoOfUser($act->FromUserID);
                        if ($nbhObj) {
                            $act->NeighborhoodName = property_exists($nbhObj, 'NAME') ? $nbhObj->NAME : '';
                            $act->NeighborhoodID = property_exists($nbhObj, 'NeighborhoodID') ? $nbhObj->NeighborhoodID : '';
                        }
                    }
                    
                    // ANOMO-10957  Anonymous posts - do not send user information to the client
                    if ($act->IsAnonymous == 1){
                        //ANOMO-11071 - Only return UserID to anonymous owner
                        if($tokenInfo->UserID != $act->FromUserID){                          
                            $act->FromUserID = "";    
                        }  
                        $act->FromUserName = "";
                        $act->Avatar = "";
                        $act->BirthDate = "";
                        $act->Gender = "";
                        $act->NeighborhoodID = "";
                        $act->NeighborhoodName = "";
                        $act->FacebookID = "";
                        $act->Lat = "";
                        $act->Lon = "";
                        $act->IsFavorite = "";
                        $act->IsAdmin = "";
                    }
                    
                    // Sep-11-2014
                    // Remove blocked content
                    if ($act->IsBlock == 1 || $act->IsBlockTarget == 1 || ($act->Type == ACTIVITY_NEW_USER && $act->FromUserID == $tokenInfo->UserID)){
                        unset($activities[$key]);
                    }

                    
                    //Remove Null/Zero/Blank properties
//                    if(empty($act->FromUserID)) unset($activities[$key]->FromUserID);
                    if(empty($act->FromUserName)) unset($activities[$key]->FromUserName);
                    if(empty($act->Image)) unset($activities[$key]->Image);
                    if(empty($act->Lat)) unset($activities[$key]->Lat);
                    if(empty($act->Lon)) unset($activities[$key]->Lon);
                    if(empty($act->Gender)) unset($activities[$key]->Gender);
                    if(empty($act->BirthDate)) unset($activities[$key]->BirthDate);
                    if(empty($act->PlaceID)) unset($activities[$key]->PlaceID);
                    if(empty($act->PlaceName)) unset($activities[$key]->PlaceName);
                    if(empty($act->PlaceIcon)) unset($activities[$key]->PlaceIcon);
                    if(empty($act->PlaceAddress)) unset($activities[$key]->PlaceAddress);
                    if(empty($act->IsInvalid)) unset($activities[$key]->IsInvalid);
                    if(empty($act->LastUpdate)) unset($activities[$key]->LastUpdate);
                    if(empty($act->OwnerID)) unset($activities[$key]->OwnerID);
                    if(empty($act->OrginalDate)) unset($activities[$key]->OrginalDate);
                    if(empty($act->GiftReceiverID)) unset($activities[$key]->GiftReceiverID);
                    if(empty($act->CheckinPlaceID)) unset($activities[$key]->CheckinPlaceID);
                    if(empty($act->CheckinPlaceName)) unset($activities[$key]->CheckinPlaceName);
                    if(empty($act->NeighborhoodID)) unset($activities[$key]->NeighborhoodID);
                    if(empty($act->NeighborhoodName)) unset($activities[$key]->NeighborhoodName);
                    if(empty($act->IsAnonymous)) unset($activities[$key]->IsAnonymous);
//                    if(empty($act->FanPage)) unset($activities[$key]->FanPage);
                    if(empty($act->VideoID)) unset($activities[$key]->VideoID);
                    if(empty($act->Avatar)) unset($activities[$key]->Avatar);
                    if(empty($act->OwnerAvatar)) unset($activities[$key]->OwnerAvatar);
                    if(empty($act->IsBlock)) unset($activities[$key]->IsBlock);
                    if(empty($act->IsBlockTarget)) unset($activities[$key]->IsBlockTarget);
                    if(empty($act->IsReported)) unset($activities[$key]->IsReported);
                    if(empty($act->IsFavorite)) unset($activities[$key]->IsFavorite);
                    if(empty($act->FacebookID)) unset($activities[$key]->FacebookID);
                    if(empty($act->OwnerName)) unset($activities[$key]->OwnerName);
                    if(empty($act->GiftReceiverName)) unset($activities[$key]->GiftReceiverName);
                    if(empty($act->IsStopReceiveNotify)) unset($activities[$key]->IsStopReceiveNotify);
                    if(empty($act->CheckinStatus)) unset($activities[$key]->CheckinStatus);
                    if(empty($act->VideoURL)) unset($activities[$key]->VideoURL);
                    if(empty($act->VideoThumbnail)) unset($activities[$key]->VideoThumbnail);
                    if(empty($act->AboutAvatarURL)) unset($activities[$key]->AboutAvatarURL);
                    if(empty($act->AboutName)) unset($activities[$key]->AboutName);
                    if(empty($act->IsLike)) unset($activities[$key]->IsLike);
                    if(empty($act->IsComment)) unset($activities[$key]->IsComment);                                               if(empty($act->Tags)) unset($activities[$key]->Tags);
                    //if(empty($act->LatestComment)) unset($activities[$key]->LatestComment);             
                    //if(empty($act->LatestLike)) unset($activities[$key]->LatestLike);
                    if(empty($act->IsAdmin)) unset($activities[$key]->IsAdmin); 
                    if(empty($act->VideoSource)) unset($activities[$key]->VideoSource); 
                    
                    //If a celebrity creates a post, do not display the "banner"
                    if($act->FromUserID == $act->FanPage){
                        if(isset($act->AboutAvatarURL))unset($activities[$key]->AboutAvatarURL);
                        if(isset($act->AboutName))unset($activities[$key]->AboutName);
                    }
                    if(empty($act->FromUserID)) unset($activities[$key]->FromUserID);
                    if(empty($act->FanPage)) unset($activities[$key]->FanPage);
                    
                    if(empty($act->TopicID) ) unset($activities[$key]->TopicID);
                    if(empty($act->TopicName) ) unset($activities[$key]->TopicName);
                    if(empty($act->ModID)) unset($activities[$key]->ModID);
                    if(empty($act->IsTopicBan)) unset($activities[$key]->IsTopicBan);
                    
                }
            }

            if ($topic_id > 0){
                if ($activity_id == 0 && $page == 1){
                    // show description of topic
                    // type = 32
                    $isClose = $this->Topic_model->isCloseTopic($tokenInfo->UserID, $topic_id);
                    if (!$isClose){
                        $topicInfo = $this->Topic_model->getTopicDescription($topic_id);
                        if ($topicInfo){
                             array_unshift($activities, $topicInfo);
                        }
                    }
                }
            }else{
                // ANOMO-7979 - add announcement card as first record
                // only show on the first page

                if (in_array($current_type, array(FEED_RECENT, FEED_NEARBY, FEED_POPULAR, FEED_FOLLOWING)) && $activity_id == 0 && $page == 1) {                              
                    // if user in vegas then the promos post will be show as the fourth post on the first screen
                    $promoPost = false;
                    if ($isVegas == 1 || strtolower($hashtag) == VEGAS_HASHTAG ){
                        $promoPost = $this->Promos_model->randomAds($tokenInfo->UserID);
                    } else {
                        if ($current_type == 3) {
                            $promoPost = $this->Promos_model->randomAds($tokenInfo->UserID, 0, 1);
                        }
                    }
                    if ($promoPost) {
                        array_splice($activities, 3, 0, array($promoPost));
                    }

                    // Announcement card will be shown on Recent/Nearby/Popular/Following tab
                    if (in_array($current_type, array(FEED_RECENT, FEED_NEARBY, FEED_POPULAR, FEED_FOLLOWING))) {
                        $cards = $this->Announcement_model->showAnnouncement($tokenInfo->UserID, $tokenInfo->SignUpDate, $current_type);
                        if (sizeof($cards) > 0) {
                            if (isset($cards['announcement_card'])) {
                                //Remove if null
                                if(empty($cards['announcement_card']->RefType)) 
                                    unset($cards['announcement_card']->RefType);
                                array_unshift($activities, $cards['announcement_card']);
                            }
                        }
                    }


                }
            } // end if topic > 0
            
            
            $response->Activities = array_values($activities);
            $response->Radius = $radiusOut;
            $response->Page = $page;
            $response->TotalPage = ceil($total / $limit);
            $response->code = $this->lang->line('ok');
        } // end if diable popular
        }

        echo json_encode($response);
    }
    
    function get_activities_v1($token = '', $page = 1, $type = 0, $radius = -1, $gender = 0, $from_age = 0, $to_age = 0, $activity_id = 0, $total = 0) {
        // log input parameters
       // $this->commonlib->myLog('v200 activity.php get_activities', array('token' => $token, 'page' => $page, 'type' => $type, 'radius' => $radius, 'gender' => $gender, 'from_age' => $from_age, 'to_age' => $to_age, 'activity_id' => $activity_id, 'total' => $total));

        // Hung's temporary fix to db lock issue: set time limit to (20s)
        set_time_limit(20);
        
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);

        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            
            // if filter by topic
            // first checking this topic is valid
            if (isset($_POST['TopicID'])){
                if (is_numeric($_POST['TopicID']) && $_POST['TopicID'] > 0){
                    $topicInfo = $this->Topic_model->checkValidTopic($_POST['TopicID'], $tokenInfo->UserID);
                    if (!$topicInfo){
                        $response->code = $this->lang->line('invalid_request');
                        echo json_encode($response);
                        exit();
                    }else{
                        if ($topicInfo->IsTopicBan == 1){
                            $response->code = $this->lang->line('topic_banned');
                            echo json_encode($response);
                            exit();
                        }
                    }
                }
            }
            
            // if filter by category
            // first checking this category is valid
            if (isset($_POST['CateID'])){
                $iValid = 0;
                if (is_numeric($_POST['CateID']) && $_POST['CateID'] > 0){
                    $cateInfo = $this->Topic_Category_model->getCateInfo($_POST['CateID']);
                    if ($cateInfo){
                        $iValid = 1;
                    }
                }
                if (!$iValid){
                    $response->code = $this->lang->line('invalid_request');
                    echo json_encode($response);
                    exit();
                }
            }
            
            // this will do update last activity when pull down to refresh
            if ($page == 1)
                $this->User_model->updateLastActivity($tokenInfo->UserID);

            // 2014-06-18 - Eric's feedback
            if (($gender < 0) || ($gender > 2)) {
                $gender = 0;
            }

            if ($from_age < 0)
                $from_age = 0;

            if ($to_age < 0)
                $to_age = 0;

            if ($to_age < $from_age)
                $to_age = $from_age;

            // ANOMO-5958
            $limit = 24;
            $offset = ($page - 1) * $limit;
            $radiusOut = -1;
            $total = $total * $limit;

            // ANOMO-7852 - search hashtag
            $hashtag = isset($_POST['HashTag']) ? trim($_POST['HashTag']) : '';
            if ($hashtag != '') {
                $hashtag = $this->Hashtag_model->clean($hashtag);
            }
            // if user in vegas then Nearby tab will be show the post have #vegas hashtag
            // if user in vegas then the promos post will be show as the fourth post on the first screen on Recent, Following, Popular
            // need to log the current type for showing the promos post as the fourth post on the first screen
            $current_type = $type;
            $isVegas = 0;
            /*
            if ($type == FEED_NEARBY || (in_array($type, array(FEED_RECENT, FEED_POPULAR, FEED_FOLLOWING)) && $activity_id == 0 && $page == 1)) {
                $lat = isset($_POST['Lat'])?$_POST['Lat']:'';
                $lon = isset($_POST['Lon'])?$_POST['Lon']:'';
                if ($lat != '' && $lon != ''){
                    $isVegas = $this->commonlib->isVegas($lat, $lon); 
                    if ($isVegas == 1 && $type == FEED_NEARBY && empty($hashtag)){
                        $type = FEED_RECENT; // search hashtag
                        $hashtag = VEGAS_HASHTAG;
                    }
                }
            }*/
            
            // ANOMO-12123 - new stream flow
            // enable to search by topic :D
            $topic_id = isset($_POST['TopicID'])?$_POST['TopicID']:0;
            $cate_id = isset($_POST['CateID'])?$_POST['CateID']:0;
            
            // Hung added... setup nearby search configuration
            $config = $this->Config_model->getNearbySearchConfig();
            $activities = $this->Activity_model->getActivities($tokenInfo->UserID, $config['SEARCH_TIME_LIMIT'], $radius, $config['SEARCH_CUTOFF_VAL'], $type, $radiusOut, $total, $offset, $limit, $config['SEARCH_RADIUS_SET'], $gender, $from_age, $to_age, $activity_id, $hashtag, $tokenInfo->Lat, $tokenInfo->Lon, $topic_id, $cate_id);
            
            // ANOMO-12424 - disable popular tab
            $disable_popular = 0;
            if ($topic_id > 0 && $page == 1 && $activity_id == 0){
                $total_popular = $total;
                if ($type != FEED_POPULAR){
                    // get total record on popular
                    $total_popular = $this->Activity_model->getTotalPopular($tokenInfo->UserID, $gender, $from_age, $to_age, $hashtag, $topic_id);
                }
                if ($total_popular < $this->Config_model->getConfig('MIN_TOTAL_POPULAR')){
                    $disable_popular = 1;
                }else{
                    $total_topic = $this->Activity_model->getTotalTopicByCond($tokenInfo->UserID, $gender, $from_age, $to_age, $topic_id);
                    if ($total_topic < $this->Config_model->getConfig('MIN_TOTAL_POST')){
                        $disable_popular = 1;
                    }
                }
                $response->DisablePopular = $disable_popular;
            }

            if ($activities) {
                $validTypes = array(ACTIVITY_POST_STATUS, ACTIVITY_POST_PICTURE, ACTIVITY_POST_VIDEO, ACTIVITY_NEW_USER);
                foreach ($activities as $key => $act) {
                    $act->IsReported = (int) $act->IsReported;
                    $act->IsBlock = (int) $act->IsBlock;
                    $act->IsBlockTarget = (int) $act->IsBlockTarget;
                    $numLike = "0";
                    $numShare = "0";
                    $numComment = "0";
                    $isComment = 0;

                    if (in_array($act->Type, $validTypes)) {
                        $contentInfo = $this->Activity_model->getActivitiesFeedInfo($act->RefID, $act->Type, $tokenInfo->UserID, $type, $act->FromUserID);
                        if ($contentInfo) {
                            $numLike = $contentInfo->Like;
                            $numComment = $contentInfo->Comment;
                            $isComment = $contentInfo->IsComment;
                            if ($type == FEED_RECENT) {
                                $act->IsLike = $contentInfo->IsLike;
                                $act->IsBlock = $contentInfo->IsBlock;
                                $act->IsBlockTarget = $contentInfo->IsBlockTarget;
                                $act->IsReported = $contentInfo->IsReported;
                                $act->IsStopReceiveNotify = $contentInfo->IsStopReceiveNotify;
                                $act->IsFavorite = $contentInfo->IsFavorite;
                            }
                        }
                    }

                    // tags
                    // ANOMO-7067
                    $tags = array();
                    
                    if ($act->Type == ACTIVITY_NEW_USER && $act->FromUserID != $tokenInfo->UserID) { // new register user
                        $tags = $this->User_model->getListTagOf($act->FromUserID);
                    }
                    
               
                    $act->Tags = $tags;
                    $comments = null;
                    $likes = null;

                    $act->IsComment = $isComment;
                    $act->Comment = $numComment;
                    $act->Like = $numLike;

                    //$act->LatestComment = (object) $comments;
                    //$act->LatestLike = (object) $likes;

                    // for owner - checkin
                    /* ANOMO-11220 : Turn off all these keys from activity feed dict
                    $act->OwnerCheckinPlaceID = '';
                    $act->OwnerCheckinPlaceName = '';
                    $act->OwnerCheckinStatus = 0;
                    $act->OwnerNeighborhoodID = '';
                    $act->OwnerNeighborhoodName = '';
                    */
                    
                    // Jan_22_2014
                    // temporary fix:  return current user location
                    // waiting the cron job update data
                    if (empty($act->NeighborhoodID)) {
                        $nbhObj = $this->User_model->getNeighborhoodInfoOfUser($act->FromUserID);
                        if ($nbhObj) {
                            $act->NeighborhoodName = property_exists($nbhObj, 'NAME') ? $nbhObj->NAME : '';
                            $act->NeighborhoodID = property_exists($nbhObj, 'NeighborhoodID') ? $nbhObj->NeighborhoodID : '';
                        }
                    }
                    
                    // ANOMO-10957  Anonymous posts - do not send user information to the client
                    if ($act->IsAnonymous == 1){
                        //ANOMO-11071 - Only return UserID to anonymous owner
                        if($tokenInfo->UserID != $act->FromUserID){                          
                            $act->FromUserID = "";    
                        }  
                        $act->FromUserName = "";
                        $act->Avatar = "";
                        $act->BirthDate = "";
                        $act->Gender = "";
                        $act->NeighborhoodID = "";
                        $act->NeighborhoodName = "";
                        $act->FacebookID = "";
                        $act->Lat = "";
                        $act->Lon = "";
                        $act->IsFavorite = "";
                        $act->IsAdmin = "";
                    }
                    
                    // Sep-11-2014
                    // Remove blocked content
                    if ($act->IsBlock == 1 || $act->IsBlockTarget == 1 || ($act->Type == ACTIVITY_NEW_USER && $act->FromUserID == $tokenInfo->UserID)){
                        unset($activities[$key]);
                    }

                    
                    //Remove Null/Zero/Blank properties
//                    if(empty($act->FromUserID)) unset($activities[$key]->FromUserID);
                    if(empty($act->FromUserName)) unset($activities[$key]->FromUserName);
                    if(empty($act->Image)) unset($activities[$key]->Image);
                    if(empty($act->Lat)) unset($activities[$key]->Lat);
                    if(empty($act->Lon)) unset($activities[$key]->Lon);
                    if(empty($act->Gender)) unset($activities[$key]->Gender);
                    if(empty($act->BirthDate)) unset($activities[$key]->BirthDate);
                    if(empty($act->PlaceID)) unset($activities[$key]->PlaceID);
                    if(empty($act->PlaceName)) unset($activities[$key]->PlaceName);
                    if(empty($act->PlaceIcon)) unset($activities[$key]->PlaceIcon);
                    if(empty($act->PlaceAddress)) unset($activities[$key]->PlaceAddress);
                    if(empty($act->IsInvalid)) unset($activities[$key]->IsInvalid);
                    if(empty($act->LastUpdate)) unset($activities[$key]->LastUpdate);
                    if(empty($act->OwnerID)) unset($activities[$key]->OwnerID);
                    if(empty($act->OrginalDate)) unset($activities[$key]->OrginalDate);
                    if(empty($act->GiftReceiverID)) unset($activities[$key]->GiftReceiverID);
                    if(empty($act->CheckinPlaceID)) unset($activities[$key]->CheckinPlaceID);
                    if(empty($act->CheckinPlaceName)) unset($activities[$key]->CheckinPlaceName);
                    if(empty($act->NeighborhoodID)) unset($activities[$key]->NeighborhoodID);
                    if(empty($act->NeighborhoodName)) unset($activities[$key]->NeighborhoodName);
                    if(empty($act->IsAnonymous)) unset($activities[$key]->IsAnonymous);
//                    if(empty($act->FanPage)) unset($activities[$key]->FanPage);
                    if(empty($act->VideoID)) unset($activities[$key]->VideoID);
                    if(empty($act->Avatar)) unset($activities[$key]->Avatar);
                    if(empty($act->OwnerAvatar)) unset($activities[$key]->OwnerAvatar);
                    if(empty($act->IsBlock)) unset($activities[$key]->IsBlock);
                    if(empty($act->IsBlockTarget)) unset($activities[$key]->IsBlockTarget);
                    if(empty($act->IsReported)) unset($activities[$key]->IsReported);
                    if(empty($act->IsFavorite)) unset($activities[$key]->IsFavorite);
                    if(empty($act->FacebookID)) unset($activities[$key]->FacebookID);
                    if(empty($act->OwnerName)) unset($activities[$key]->OwnerName);
                    if(empty($act->GiftReceiverName)) unset($activities[$key]->GiftReceiverName);
                    if(empty($act->IsStopReceiveNotify)) unset($activities[$key]->IsStopReceiveNotify);
                    if(empty($act->CheckinStatus)) unset($activities[$key]->CheckinStatus);
                    if(empty($act->VideoURL)) unset($activities[$key]->VideoURL);
                    if(empty($act->VideoThumbnail)) unset($activities[$key]->VideoThumbnail);
                    if(empty($act->AboutAvatarURL)) unset($activities[$key]->AboutAvatarURL);
                    if(empty($act->AboutName)) unset($activities[$key]->AboutName);
                    if(empty($act->IsLike)) unset($activities[$key]->IsLike);
                    if(empty($act->IsComment)) unset($activities[$key]->IsComment);                                               if(empty($act->Tags)) unset($activities[$key]->Tags);
                    //if(empty($act->LatestComment)) unset($activities[$key]->LatestComment);             
                    //if(empty($act->LatestLike)) unset($activities[$key]->LatestLike);
                    if(empty($act->IsAdmin)) unset($activities[$key]->IsAdmin); 
                    if(empty($act->VideoSource)) unset($activities[$key]->VideoSource); 
                    
                    //If a celebrity creates a post, do not display the "banner"
                    if($act->FromUserID == $act->FanPage){
                        if(isset($act->AboutAvatarURL))unset($activities[$key]->AboutAvatarURL);
                        if(isset($act->AboutName))unset($activities[$key]->AboutName);
                    }
                    if(empty($act->FromUserID)) unset($activities[$key]->FromUserID);
                    if(empty($act->FanPage)) unset($activities[$key]->FanPage);
                    
                    if(empty($act->TopicID) ) unset($activities[$key]->TopicID);
                    if(empty($act->TopicName) ) unset($activities[$key]->TopicName);
                    if(empty($act->ModID)) unset($activities[$key]->ModID);
                    if(empty($act->IsTopicBan)) unset($activities[$key]->IsTopicBan);
                    
                }
            }

            if ($topic_id > 0){
                if ($activity_id == 0 && $page == 1){
                    // show description of topic
                    // type = 32
                    $isClose = $this->Topic_model->isCloseTopic($tokenInfo->UserID, $topic_id);
                    if (!$isClose){
                        $topicInfo = $this->Topic_model->getTopicDescription($topic_id);
                        if ($topicInfo){
                             array_unshift($activities, $topicInfo);
                        }
                    }
                }
            }else{
                // ANOMO-7979 - add announcement card as first record
                // only show on the first page

                if (in_array($current_type, array(FEED_RECENT, FEED_NEARBY, FEED_POPULAR, FEED_FOLLOWING)) && $activity_id == 0 && $page == 1) {                              
                    // if user in vegas then the promos post will be show as the fourth post on the first screen
                    $promoPost = false;
                    if ($isVegas == 1 || strtolower($hashtag) == VEGAS_HASHTAG ){
                        $promoPost = $this->Promos_model->randomAds($tokenInfo->UserID);
                    } else {
                        if ($current_type == 3) {
                            $promoPost = $this->Promos_model->randomAds($tokenInfo->UserID, 0, 1);
                        }
                    }
                    if ($promoPost) {
                        array_splice($activities, 3, 0, array($promoPost));
                    }

                    // Announcement card will be shown on Recent/Nearby/Popular/Following tab
                    if (in_array($current_type, array(FEED_RECENT, FEED_NEARBY, FEED_POPULAR, FEED_FOLLOWING))) {
                        $cards = $this->Announcement_model->showAnnouncement($tokenInfo->UserID, $tokenInfo->SignUpDate, $current_type);
                        if (sizeof($cards) > 0) {
                            if (isset($cards['announcement_card'])) {
                                //Remove if null
                                if(empty($cards['announcement_card']->RefType)) 
                                    unset($cards['announcement_card']->RefType);
                                array_unshift($activities, $cards['announcement_card']);
                            }
                        }
                    }


                }
            } // end if topic > 0
            
            
            $response->Activities = array_values($activities);
            $response->Radius = $radiusOut;
            $response->Page = $page;
            $response->TotalPage = ceil($total / $limit);
            $response->code = $this->lang->line('ok');
        
        }

        echo json_encode($response);
    }
    
    /**
     *    share function
     *
     * @param string $token
     * @param int $content_id
     * @param int $type
     * @param int $share_type
     *     0: Facebook
     *  1: Internal
     *     2: Both
     */
    function share($token = '', $content_id = 0, $type = '', $share_type = 0)
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $validTypes = array(0, 1, 2, 7, 27, 6, 8);
            // validate
            if (!is_numeric($content_id) || !is_numeric($type) || !in_array($type, $validTypes) || !in_array($share_type, array(0, 1, 2))) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                // only share once time.
                $isShare = false;
                if ($share_type == 1 || $share_type == 2) {
                    $isShare = $this->Content_Share_model->isShare($tokenInfo->UserID, $content_id, $type, 1);
                }
                if ($isShare) {
                    $response->code = $this->lang->line('shared');
                } else {
                    // this content is existed
                    $contentInfo = $this->Activity_model->getContentShareDetail($content_id, $type);
                    if (!$contentInfo) {
                        $response->code = $this->lang->line('invalid_request');
                    } else {
                        // not share your's content
                        // if ($tokenInfo->UserID == $contentInfo->FromUserID){
                        // 	$response->code = $this->lang->line('invalid_request');
                        // }else{
                        // 1. insert into ContentShare table
                        if ($share_type == 0 || $share_type == 2) { // share facebook
                            $data = array(
                                'UserID' => $tokenInfo->UserID,
                                'ContentID' => $content_id,
                                'ContentType' => $type,
                                'ShareType' => 0,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                            );
                            $content_share_id = $this->Content_Share_model->add($data);
                        }

                        if ($share_type == 1 || $share_type == 2) { // share internal
                            $data = array(
                                'UserID' => $tokenInfo->UserID,
                                'ContentID' => $content_id,
                                'ContentType' => $type,
                                'ShareType' => 1,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                            );
                            $content_share_id = $this->Content_Share_model->add($data);
                        }
                        if (!$content_share_id) {
                            $response->code = $this->lang->line('failed');
                        } else {
                            if ($share_type == 1 || $share_type == 2) {
                                // internal share
                                // 2. If success => insert new activity stream
                                // a. share activity
                                $data = array(
                                    'FromUserID' => $tokenInfo->UserID,
                                    'FromUserName' => $tokenInfo->UserName,
                                    'Avatar' => $tokenInfo->Avatar,
                                    'RefID' => $content_id,
                                    'Type' => $type,
                                    'ActionType' => 5,
                                    'Message' => $contentInfo->Message,
                                    'Image' => $contentInfo->Image,
                                    'PlaceID' => $contentInfo->PlaceID,
                                    'PlaceName' => $contentInfo->PlaceName,
                                    'PlaceIcon' => $contentInfo->PlaceIcon,
                                    'PlaceAddress' => $contentInfo->PlaceAddress,
                                    'Lat' => $tokenInfo->Lat,
                                    'Lon' => $tokenInfo->Lon,
                                    'BirthDate' => $tokenInfo->BirthDate,
                                    'Gender' => $tokenInfo->Gender,
                                    'OwnerID' => $contentInfo->FromUserID,
                                    'OwnerName' => $contentInfo->FromUserName,
                                    'OwnerAvatar' => $contentInfo->Avatar,
                                    'OrginalDate' => $contentInfo->CreatedDate,
                                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                                );
                                $id = $this->Activity_model->add($data);

                                // b. will show in his activity
                                $feedTypes = array(
                                    0 => 0, // check in place
                                    1 => 5, // change profile status
                                    2 => 3, // post picture to place
                                    7 => 10, // post picture to neighborhood
                                    27 => 12, // post picture status
                                    6 => 7, // post text to place
                                    8 => 9, // post text to neighborhood
                                );
                                if ($type == 0) {
                                    $activityUser = $this->Place_Activity_model->getActivityBy($content_id, 0);
                                    if ($activityUser) {
                                        $postID = $activityUser->PostID;
                                    }
                                }
                                $dataUserActivity = array(
                                    'UserID' => $tokenInfo->UserID,
                                    'Type' => $feedTypes[$type],
                                    'RefID' => $content_id,
                                    'PlaceID' => $contentInfo->PlaceID,
                                    'IsShare' => 1,
                                    'PostID' => isset($postID) ? $postID : NULL,
                                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                                );
                                $user_activity_id = $this->Place_Activity_model->add($dataUserActivity);

                                if ($id && $user_activity_id) {
                                    $response->code = $this->lang->line('ok');
                                } else {
                                    $response->code = $this->lang->line('failed');
                                }
                            } else {
                                $response->code = $this->lang->line('ok');
                            }
                        }
                    }
                }
            }
        }

        echo json_encode($response);
        exit();
    }

    /*
     * comment checkin / change profile status / send gift / change tag => insert into ActivityComment table
     * comment picture post to place / post text to place / post to nbh => using old table (PostComment / NeighborhoodComment)
     */
    function comment($token = '', $content_id = 0, $type = '')
    {
        $isCommentAnonymous = isset($_POST['IsAnonymous'])?$_POST['IsAnonymous']:0;
        $validTypes = array(ACTIVITY_POST_STATUS, ACTIVITY_POST_PICTURE, ACTIVITY_PROMOS, ACTIVITY_POST_VIDEO);
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!is_numeric($content_id) || !in_array($type, $validTypes) || !isset($_POST['Content']) || (isset($_POST['Content']) && trim($_POST['Content']) == "")) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                // update user's last activity
                $this->User_model->updateLastActivity($tokenInfo->UserID);
                $contentInfo = $this->Activity_model->getContentInfo($content_id, $type);
                //validate anonymous commment - post must be anonymous and from the owner user
                if($isCommentAnonymous == 1 && ($contentInfo->IsAnonymous != 1  || $contentInfo->UserID != $tokenInfo->UserID  )){
                    $response->code = $this->lang->line('invalid_request');
                }else{                                    
                    // validate this conntent
                    if (!$contentInfo) {
                        $response->code = $this->lang->line('invalid_request');
                    } else {
                        // dont allow blocked user comment this post
                        $isBlock = $this->User_model->checkBlock($contentInfo->UserID, $tokenInfo->UserID);
                        if ($isBlock == 1) {
                            $response->code = $this->lang->line('failed');
                        } else {
                            $comment_id = $this->Activity_model->comment($tokenInfo->UserID, $content_id, $type, trim($_POST['Content']), $isCommentAnonymous);
                            if (!$comment_id) { // fail
                                $response->code = $this->lang->line('failed');
                            } else {
                                // increase # of comment of content
                                $this->Activity_model->increase($content_id, $type, 'comment');
    
                                // return this comment
                                $response->code = $this->lang->line('ok');
                                $commentObj = new stdClass();
                                $commentObj->ID = $comment_id;
                                $commentObj->UserID = $tokenInfo->UserID;
                                $commentObj->UserName = $tokenInfo->UserName;
                                $commentObj->Avatar = ($tokenInfo->Avatar) ? $this->config->item('s3_photo_url') . $tokenInfo->Avatar : "";
                                $commentObj->Content = trim($_POST['Content']);
                                $commentObj->CreatedDate = gmdate('Y-m-d H:i:s');
                                $commentObj->FacebookID = ($tokenInfo->FacebookID) ? $tokenInfo->FacebookID : "";
    			    $commentObj->BirthDate = ($tokenInfo->BirthDate) ? $tokenInfo->BirthDate : "";
                                $placeId = '';
                                $placeName = '';
                                $checkinStatus = 0;
    
                                $neighborhoodId = '';
                                $neighborhoodName = '';
                                // check in neighborhood
                                $nbhObj = $this->User_model->getNeighborhoodInfoOfUser($tokenInfo->UserID);
                                if ($nbhObj) {
                                    $neighborhoodName = property_exists($nbhObj, 'NAME') ? $nbhObj->NAME : '';
                                    $neighborhoodId = property_exists($nbhObj, 'NeighborhoodID') ? $nbhObj->NeighborhoodID : '';
                                }
                               
                                $commentObj->PlaceID = $placeId;
                                $commentObj->PlaceName = $placeName;
                                $commentObj->CheckinStatus = $checkinStatus;
    
                                $commentObj->NeighborhoodID = $neighborhoodId;
                                $commentObj->NeighborhoodName = $neighborhoodName;
                                $commentObj->IsAnonymous = $isCommentAnonymous;
                                $commentObj->IsAdmin = $tokenInfo->IsAdmin;
    
                                if($isCommentAnonymous){
                                    $commentObj->UserName = "";
                                    $commentObj->Avatar = "";
                                    $commentObj->FacebookID = "";
                                    $commentObj->BirthDate = "";
                                    $commentObj->NeighborhoodID = "";
                                    $commentObj->NeighborhoodName = "";
                                }
                                $response->comment = $commentObj;
    
                                $is_anonymous = isset($contentInfo->IsAnonymous)?$contentInfo->IsAnonymous:0;
                                // push notify
                                // 1. owner
                                $cronPushData = array();
                                $exclude = $tokenInfo->UserID;
                                if ($tokenInfo->UserID != $contentInfo->UserID && !$this->Stop_Receive_Notify_model->getInfo($contentInfo->UserID, $content_id, $type)) {
                                    $exclude .= "," . $contentInfo->UserID;
                                    $pushData = array(
                                        'UserID' => $contentInfo->UserID,
                                        'SendUserID' => $tokenInfo->UserID,
                                        'Type' => HISTORY_COMMENT_ACTIVITY, // 14 - comment feed
                                        'RefID' => $comment_id,
                                        'ContentType' => $type,
                                        'ContentID' => $content_id,
                                        'IsAnonymous'   => $is_anonymous,
                                        'IsRead' => 1,
                                        'CreatedDate' => gmdate('Y-m-d H:i:s')
                                    );
                                    $history_id = $this->Notification_History_model->add($pushData);
    
                                    // notify
                                    if ($history_id ) {
                                        $msgPushNotify =  $this->lang->line('commented_post');
                                        $msgPushNotify = str_replace('%name%', $tokenInfo->UserName, $msgPushNotify);
                                        $cronPushData[] = array(
                                            'UserID'    => $contentInfo->UserID,                    
                                            'FromUserID'  => $tokenInfo->UserID,
                                            'Message'   => $msgPushNotify,
                                            'ContentID' => $content_id,
                                            'Type'      => $type,
                                            'ActionType'    => NOTIFY_COMMENT_ACTIVITY, // 7
                                            'Status' => 0,
                                            'Total' => 0,
                                            'HistoryID' => $history_id,
                                            'CreatedDate'   => gmdate('Y-m-d H:i:s')
                                        );
                                    }
                                }
                                // 2. users used to comment this content
                                // ANOMO-8198 Comment on activity: should push notify to mention user
                                // only for change profile status
                                $mentionUserIds = array();
                                $content = isset($contentInfo->Content)?$contentInfo->Content:'';
                                if (empty($content) && isset($contentInfo->PictureCaption)){
                                    $content = $contentInfo->PictureCaption;
                                }
                                if (in_array($type, array(ACTIVITY_POST_STATUS, ACTIVITY_POST_PICTURE, ACTIVITY_POST_VIDEO)) && $content != ''){
                                    $mentionUserIds = $this->Activity_model->returnMentionUserID($content);
                                }
                                
                                // ANOMO-8213 It should display follow/unfollow post when user receives a gift
                                if ($type == 9 && $contentInfo->ReceiveUserID != NULL){
                                    $mentionUserIds[] =$contentInfo->ReceiveUserID;
                                }
                                
                                $listUser = $this->Activity_model->getListUserIDIsCommented($content_id, $type, $exclude, $mentionUserIds, $tokenInfo->UserID, $contentInfo->UserID);
    
                                if ($listUser) {
                                    foreach ($listUser as $re) {
                                        $pushData = array(
                                            'UserID' => $re->UserID,
                                            'SendUserID' => $tokenInfo->UserID,
                                            'Type' => HISTORY_COMMENT_ACTIVITY, // 14 - comment feed
                                            'RefID' => $comment_id,
                                            'ContentID' => $content_id,
                                            'ContentType' => $type,
                                            'IsAnonymous'   => $is_anonymous,
                                            'IsAnonymousComment' => $isCommentAnonymous,
                                            'IsRead' => 1,
                                            'CreatedDate' => gmdate('Y-m-d H:i:s')
                                        );
                                        $history_id = $this->Notification_History_model->add($pushData);
                                        // notify
         
                                        if ($history_id) {
                                            $onwer = $is_anonymous != 1?$contentInfo->UserName: 'Anonymous';
                                            $msgPushNotify = str_replace('%owner%', $onwer, $this->lang->line('commented_owner_post'));
                                            //anonymous comment message
                                            if($isCommentAnonymous == 1){
                                                $msgPushNotify = $this->lang->line('commented_owner_anonymous');
                                            }else{
                                                $msgPushNotify = str_replace('%name%', $tokenInfo->UserName, $msgPushNotify);
                                            }
    
                                            
                                            $cronPushData[] = array(
                                                'UserID'    => $re->UserID,                    
                                                'FromUserID'  => $tokenInfo->UserID,
                                                'Message'   => $msgPushNotify,
                                                'ContentID' => $content_id,
                                                'Type'      => $type,
                                                'ActionType'    => NOTIFY_COMMENT_ACTIVITY, // 7
                                                'Status' => 0,
                                                'Total' => 0,
                                                'HistoryID' => $history_id,
                                                'IsAnonymousComment' => $isCommentAnonymous,
                                                'CreatedDate'   => gmdate('Y-m-d H:i:s')
                                            );
                                        }
                                    }
                                }
    
                                // 
                                if (sizeof($cronPushData) > 0){
                                    $this->Cron_Push_Notification_model->insertMulti($cronPushData);
                                }
                            }
                        } // end block
    
                    }
                }
            }
        }

        echo json_encode($response);
    }

    function likelist($token, $content_id = 0, $type = "")
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!is_numeric($content_id) || !is_numeric($type)) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $listlike = $this->Activity_model->getListLike($content_id, $type, $tokenInfo->UserID);
                $response->likes = $listlike;
                $response->code = $this->lang->line('ok');
            }
        }
        echo json_encode($response);
    }

    function detail($token = '', $content_id = 0, $type = '', $activity_id = 0)
    {
        $validTypes = array(ACTIVITY_POST_STATUS, ACTIVITY_POST_PICTURE, ACTIVITY_PROMOS, ACTIVITY_POST_VIDEO);
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!is_numeric($content_id) || !is_numeric($type) || !in_array($type, $validTypes)) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                if ($type == ACTIVITY_PROMOS){
                    $activityInfo = $this->Promos_model->randomAds($tokenInfo->UserID, $content_id);
                    if ($activityInfo){
                        $listComment = $this->Activity_model->getListComment($content_id, $type, $tokenInfo->UserID);                       
                        $activityInfo->ListComment = $listComment;
                        $response->Activity = $activityInfo;
                        $response->code = $this->lang->line('ok');
                    }else{
                        $response->code = $this->lang->line('invalid_request');
                    }
                    
                }else{
                    $activityInfo = $this->Activity_model->getContentDetail($content_id, $type, $activity_id, $tokenInfo->UserID);
                    if (!$activityInfo) {
                        $response->code = $this->lang->line('invalid_request');
                    } else {
                        if ($activityInfo->TopicBanStatus == 1){
                            $response->code = $this->lang->line('topic_banned');
                        }else{
                            
                        $numLike = "0";
                        $numShare = "0";
                        $numComment = "0";
                        $activityInfo->FacebookID = '';
                        if (in_array($type, $validTypes)) {
                            $contentInfo = $this->Activity_model->getContentInfo($content_id, $type);
                            if ($contentInfo) {
                                $numLike = $contentInfo->Like;
                                $numComment = $contentInfo->Comment;
                                $activityInfo->FacebookID = ($contentInfo->FacebookID) ? $contentInfo->FacebookID : '';
                            }
                        }


                        $activityInfo->IsLike = (int) $activityInfo->IsLike;
                        $activityInfo->Share = $numShare;
                        $activityInfo->Comment = $numComment;
                        $activityInfo->Like = $numLike;   

                        $listComment = $this->Activity_model->getListComment($content_id, $type, $tokenInfo->UserID, (int)$activityInfo->TopicID);     
                        //filter Anonymous Comment
                        foreach ($listComment as $comment){
                            if($comment->IsAnonymous == 1){
                                if ($comment->UserID != $tokenInfo->UserID){
                                    $comment->UserID = "";
                                }
                                $comment->UserName = "";
                                $comment->Avatar = "";
                                $comment->FacebookID = "";
                                $comment->BirthDate = "";
                                $comment->NeighborhoodID = "";
                                $comment->NeighborhoodName = "";
                                $comment->IsAdmin = "";
                            }                            
                        }                   
                        $activityInfo->ListComment = $listComment;

                        $checkinStatus = 0;
                        $placeId = $activityInfo->CheckinPlaceID != NULL ? $activityInfo->CheckinPlaceID:'';
                        $placeName = $activityInfo->CheckinPlaceName != NULL ? $activityInfo->CheckinPlaceName:'';
                        $neighborhoodName = $activityInfo->NeighborhoodName != NULL ? $activityInfo->NeighborhoodName:'';
                        $neighborhoodId = $activityInfo->NeighborhoodID != NULL ? $activityInfo->NeighborhoodID:'';

                        $activityInfo->CheckinPlaceID = $placeId;
                        $activityInfo->CheckinPlaceName = $placeName;
                        $activityInfo->CheckinStatus = $checkinStatus;
                        $activityInfo->NeighborhoodID = $neighborhoodId;
                        $activityInfo->NeighborhoodName = $neighborhoodName;

                        // Jan_22_2014
                        // temporary fix:  return current user location
                        // waiting the cron job update data
                        if (is_null($activityInfo->NeighborhoodID) || empty($activityInfo->NeighborhoodID)) {
                            $nbhObj = $this->User_model->getNeighborhoodInfoOfUser($activityInfo->FromUserID);
                            if ($nbhObj) {
                                $activityInfo->NeighborhoodName = property_exists($nbhObj, 'NAME') ? $nbhObj->NAME : '';
                                $activityInfo->NeighborhoodID = property_exists($nbhObj, 'NeighborhoodID') ? $nbhObj->NeighborhoodID : '';
                            }
                        }
                        // ANOMO-10957  Anonymous posts - do not send user information to the client
                        if ($activityInfo->IsAnonymous == 1){                            
                            //ANOMO-11071 - Only return UserID to anonymous owner
                            if($tokenInfo->UserID == $activityInfo->FromUserID){
                                $activityInfo->AllowCommentAnonymous = "1";    
                            }else{
                                $activityInfo->FromUserID = "";    
                            }                            
                            $activityInfo->FromUserName = "";
                            $activityInfo->Avatar = "";
                            $activityInfo->BirthDate = "";
                            $activityInfo->Gender = "";
                            $activityInfo->NeighborhoodID = "";
                            $activityInfo->NeighborhoodName = "";
                            $activityInfo->FacebookID = "";
                            $activityInfo->Lat = "";
                            $activityInfo->Lon = "";
                            $activityInfo->IsAdmin = "";
                        }
                    
                        unset($activityInfo->TopicBanStatus);
                        
                        $response->Activity = $activityInfo;
                        $response->code = $this->lang->line('ok');
                    }
                    } // end check banned topic
                }
            }
        }

        echo json_encode($response);
    }

    /**
     * Save comment into queue for cron job
     * @param int $userID
     * @param int $type
     * @param int $fromUserID
     * @param string $message
     * @param string $placename
     * @param int $contentType
     * @param int $contentID
     */
    private function _addCronPushNotify($userID, $type = '', $fromUserID = 0, $message = '', $contentType = '', $contentID = '', $history_id = '')
    {
        $data['UserID'] = $userID;
        $data['FromUserID'] = $fromUserID;
        $data['FromUserName'] =  '';
        $data['Avatar'] = '';
        $data['Message'] = $message;
        $data['ContentID'] = $contentID;
        $data['Type'] = $contentType;
        $data['ActionType'] = $type;
        $data['Status'] = 0;
        $data['Total'] = 0;
        $data['HistoryID'] = $history_id;
        $data['CreatedDate'] = gmdate('Y-m-d H:i:s');
        $this->Cron_Push_Notification_model->add($data);
    }

    // util function:
    private function invalidatecache()
    {
        $cb = new Couchbase("127.0.0.1:8091", "Administrator", "Bubbagump2013A", "default");
        for ($page = 1; $page <= 5; $page++) {
            $cacheKey = 'recent_activities_p' . $page;
            $cb->set($cacheKey, NULL);
        }
    }


    function like($token = '', $content_id = 0, $type = ''){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            // update last activity
            $this->User_model->updateLastActivity($tokenInfo->UserID);
            $validTypes = array(ACTIVITY_POST_STATUS, ACTIVITY_POST_PICTURE, ACTIVITY_PROMOS, ACTIVITY_POST_VIDEO);
            // validate
            if (!is_numeric($content_id) || !is_numeric($type) || !in_array($type, $validTypes)) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $contentInfo = $this->Activity_model->getContentInfo($content_id, $type);
                if (!$contentInfo) {
                    $response->code = $this->lang->line('invalid_request');
                } else {
                    $isAnonymous = isset($contentInfo->IsAnonymous)?$contentInfo->IsAnonymous: 0;
                    $isLikeObj = $this->Activity_model->isLike($tokenInfo->UserID, $content_id, $type);
                    if ($isLikeObj){ // incase unlike
                        $isUnLike = $this->Activity_model->unLike($tokenInfo->UserID, $content_id, $type);
                        if ($isUnLike) {
                            $this->Activity_model->increase($content_id, $type, 'like', 'decrease');
                            $response->ContentID = $content_id;
                            $response->NumberOfLike = (int)$contentInfo->Like - 1;

                            $response->LatestLikeUserID = '';
                            $response->LatestLikeUserName = '';

                            $response->code = $this->lang->line('ok');

                            // remove notification history
                            $this->Notification_History_model->delete(array('RefID' => $isLikeObj->ID, 'Type' => 13));
                            
                            // remove influence point if have
                            // ANOMO-10327 - influence point
                            // No points or reputation screen for Vendors
                            if ($isAnonymous != 1 && $type != ACTIVITY_PROMOS){
                                // ANOMO-10327 - influence point
                                $aPoint = array(
                                    'UserID' => $contentInfo->UserID,
                                    'FromUserID'    => $tokenInfo->UserID,
                                    'Type'   => INF_POINT_LIKE, // 1
                                    'Point'   => 3,
                                    'ContentID' => $content_id,
                                    'ContentType'  => $type,
                                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                                );
                                $this->Influence_Point_model->minusPoint($aPoint);
                            }
                        } else {
                            $response->code = $this->lang->line('failed');
                        }   
                    }else{ // incase like
                        // insert new record into ActivityLike
                        $likeData = array(
                            'UserID' => $tokenInfo->UserID,
                            'ContentID' => $content_id,
                            'Type' => $type,
                            'CreatedDate' => gmdate('Y-m-d H:i:s')
                        );
                        $likeId = $this->Activity_model->addLike($likeData);
                        if (!$likeId) {
                            $response->code = $this->lang->line('failed');
                        } else {
                            $response->ContentID = $content_id;
                            $response->NumberOfLike = (int)$contentInfo->Like + 1;
                            $response->LatestLikeUserID = $tokenInfo->UserID;
                            $response->LatestLikeUserName = $tokenInfo->UserName;
                            $response->code = $this->lang->line('ok');
                                    
                            // increase #like
                            $this->Activity_model->increase($content_id, $type, 'like');
                            // No points or reputation screen for Vendors
                            if ($isAnonymous != 1 && $type != ACTIVITY_PROMOS){
                                // ANOMO-10327 - influence point
                                $aPoint = array(
                                    'UserID' => $contentInfo->UserID,
                                    'FromUserID'    => $tokenInfo->UserID,
                                    'Type'   => INF_POINT_LIKE, // 1
                                    'Point'   => 3,
                                    'ContentID' => $content_id,
                                    'ContentType'  => $type,
                                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                                );
                                $this->Influence_Point_model->add($aPoint);
                            }
                            // log history & push notify
                            if ($tokenInfo->UserID != $contentInfo->UserID && !$this->Stop_Receive_Notify_model->getInfo($contentInfo->UserID, $content_id, $type)) {
                                $pushData = array(
                                    'UserID' => $contentInfo->UserID,
                                    'SendUserID' => $tokenInfo->UserID,
                                    'Type' => HISTORY_LIKE_ACTIVITY, // 13 - like content in feed
                                    'RefID' => $likeId,
                                    'ContentID' => $content_id,
                                    'IsAnonymous'   => $isAnonymous,
                                    'ContentType' => $type,
                                    'IsRead' => 1,
                                    'UnView'    => 1,
                                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                                );
                                $history_id = $this->Notification_History_model->add($pushData);
                                if ($history_id){
                                    $userSetting = $this->Activity_model->returnUserSetting($contentInfo->UserID);
                                    if ($userSetting && $userSetting->AllowLikeActivityNotice == 1) {
                                        $msgPushNotify = $this->lang->line('like_post');
                                        $msgPushNotify = str_replace('%name%', $tokenInfo->UserName, $msgPushNotify);
                                        $this->mypushnotify->send($contentInfo->UserID, $tokenInfo->UserID, NOTIFY_LIKE_ACTIVITY, $msgPushNotify, array('ContentID' => $content_id, 'ContentType' => $type, 'HistoryID' => $history_id));
                                    }
                                }
                            }
                        }    
                    }
                }
            }
        }
        echo json_encode($response);
    }
    
    /**
     * Close Announcement
     * @param type $token
     * @param type $announcement_id
     */
    function close_announcement($token = '', $announcement_id = '') {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if ($announcement_id == -1) { // hashtag card - ANOMO-8598
                $updated = $this->User_model->updateUser(array('CloseTrendingCardDate' => gmdate('Y-m-d H:i:s')), array('UserID' => $tokenInfo->UserID));
                if (!$updated) {
                    $response->code = $this->lang->line('failed');
                } else {
                    $response->code = $this->lang->line('ok');
                }
            } else {
                $announcementInfo = $this->Announcement_model->getInfo($announcement_id);
                if (!$announcementInfo) {
                    $response->code = $this->lang->line('invalid_request');
                } else {
                    $isClosed = $this->Announcement_model->getAnnouncementCloseInfo($tokenInfo->UserID, $announcement_id);
                    if (!$isClosed) {
                        $data = array(
                            'UserID' => $tokenInfo->UserID,
                            'AnnouncementID' => $announcement_id,
                            'CreatedDate' => gmdate('Y-m-d H:i:s')
                        );
                        $id = $this->Announcement_model->addAnnouncementClose($data);
                        if (!$id) {
                            $response->code = $this->lang->line('failed');
                        } else {
                            $response->code = $this->lang->line('ok');
                        }
                    } else {
                        $response->code = $this->lang->line('announcement_closed');
                    }
                }
            }
        }

        echo json_encode($response);
    }
    
    function trending_hashtag($token = '', $cate_id = 0, $topic_id = 0){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $trending = $this->Hashtag_model->topTrendingHashTag($cate_id, $topic_id);
            
            if ($trending){
                if ($this->Config_model->getConfig('TURNON_VEGAS_HASHTAG') == 'on'){
                    foreach($trending as $key => $obj){
                        if ($obj->HashTag == VEGAS_HASHTAG){
                            unset($trending[$key]);
                        }
                    }
                    array_unshift($trending, array('HashTag' => VEGAS_HASHTAG));
                }
            }
            $response->ListTrending = $trending;
            $response->code = $this->lang->line('ok');
        }
        echo json_encode($response);
    }
    
    /**search in activity feed screen
     * 
     * 
     * @return 
     *      ListUser - return 3 users (maximum) match with keyword  
     *      ListHashTag - return 3 hashtag (maximum) match with keyword
     *      IsHaveTag - 0 or 1: 0: don't have any tag match with keyword/ 1: else
     */
    
    /**search user
     * 
     * @param type $token
     */
    function search_user($token = ''){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (isset($_POST['Keyword']) && !empty($_POST['Keyword'])){
                $keyword = strtolower(trim($_POST['Keyword']));
                $response->ListUser = $this->Activity_model->searchUser($keyword, $tokenInfo->UserID);
                $response->code = $this->lang->line('ok');
            }else{
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }
    
    /**search hashtag
     * 
     * @param type $token
     */
    function search_hashtag($token = ''){
        $response = new stdClass();
        $keyword = isset($_POST['Keyword'])?trim($_POST['Keyword']):'';
        $cate_id = isset($_POST['CateID'])?$_POST['CateID']:0;
        $topic_id = isset($_POST['TopicID'])?$_POST['TopicID']:0;
        if ($keyword == ''){ 
            $response->code = $this->lang->line('invalid_request');
        }  else {
            if (!is_numeric($cate_id) || !is_numeric($topic_id)){
                $response->code = $this->lang->line('invalid_request');
            }else{
                $tokenInfo = $this->Wstoken_model->getToken($token);
                if (!$tokenInfo) {
                    $response->code = $this->lang->line('invalid_token');
                } else {
                    $response->ListHashTag = $this->Activity_model->findHashTag($keyword, $cate_id, $topic_id);
                    $response->code = $this->lang->line('ok');
                }
            }
        }
        echo json_encode($response);
    }
    
    /**search tag
     * 
     * @param type $token
     */
    function search_tag($token = ''){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (isset($_POST['Keyword']) && !empty($_POST['Keyword'])){
                $keyword = strtolower(trim($_POST['Keyword']));
                $response->IsHaveTag = $this->Tag_model->searchTag($keyword, $tokenInfo->UserID);
                $response->code = $this->lang->line('ok');
            }else{
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }
    
    function onboarding_hashtag($token = ''){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $data = array();
            $hashTag = $this->Config_model->getConfig('ONBOARDING_HASHTAG');
            if ($hashTag){
                $hashTag = explode(',', $hashTag);
                foreach($hashTag as $tag){
                    if (!empty($tag)){
                        $data[] = array('HashTag' => $tag);
                    }
                }
            }

            $response->ListTrending = $data;
            $response->code = $this->lang->line('ok');
        }
        echo json_encode($response);
    }
    
    /**search hashtag
     * 
     * @param type $token
     */
    function search_topic($token = ''){
        $response = new stdClass();
        $keyword = isset($_POST['Keyword'])?trim($_POST['Keyword']):'';
        $cate_id = isset($_POST['CateID'])?$_POST['CateID']:0;
        if ($keyword == ''){ 
            $response->code = $this->lang->line('invalid_request');
        }  else {
            if (!is_numeric($cate_id)){
                $response->code = $this->lang->line('invalid_request');
            }else{
                $tokenInfo = $this->Wstoken_model->getToken($token);
                if (!$tokenInfo) {
                    $response->code = $this->lang->line('invalid_token');
                } else {
                    $response->results = $this->Topic_model->findTopic($tokenInfo->UserID, $keyword, $cate_id);
                    $response->code = $this->lang->line('ok');
                }
            }
        }
        echo json_encode($response);
    }
    
}

?>