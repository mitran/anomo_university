<?php

class Comment extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Activity_model');
        $this->load->model('Comment_model');
        $this->load->model('Wstoken_model');
        $this->load->model('Stop_Receive_Notify_model');
        $this->load->model('Notification_History_model');
        $this->load->model('User_model');
        $this->load->model('Devices_model');
        $this->load->model('Config_model');     
        $this->load->model('Influence_Point_model');
        $this->lang->load('ws');
        $this->load->library('MyPushNotify');
    }

    /**
     * 
     * @param type $token
     * @param type $comment_id
     * @param type $content_type
     */
    function like($token = '', $comment_id = 0, $content_type = '') {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $validTypes = array(ACTIVITY_POST_STATUS, ACTIVITY_POST_PICTURE, ACTIVITY_PROMOS, ACTIVITY_POST_VIDEO);
            // validate
            if (!is_numeric($comment_id) || !is_numeric($content_type) || !in_array($content_type, $validTypes)) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $commentInfo = $this->Activity_model->getComment($comment_id, $content_type);
                if (!$commentInfo) {
                    $response->code = $this->lang->line('invalid_request');
                } else {
                    $contentInfo = $this->Activity_model->getContentInfo($commentInfo->ContentID, $content_type);
                    if (!$contentInfo) {
                        $response->code = $this->lang->line('invalid_request');
                    } else {
                        $isAnonymousComment = isset($commentInfo->IsAnonymous)?$commentInfo->IsAnonymous: 0;
                        $isLike = $this->Comment_model->isLike($tokenInfo->UserID, $comment_id, $content_type);
                        $numberLike = $commentInfo->NumberOfLike;
                        if ($isLike == 0) { // add like
                            $data = array(
                                'UserID' => $tokenInfo->UserID,
                                'CommentID' => $comment_id,
                                'ContentType' => $content_type,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                            );
                            $likeId = $this->Comment_model->addLike($data);
                            if ($likeId) {
                                // increase #like
                                $this->Comment_model->updateNumberLikeOfComment($comment_id, $content_type, 'increase');

                                //ANOMO-11399
                                // no point or reputation when like anonymous comment
                                if($isAnonymousComment != 1){
                                    // ANOMO-10327 - influence point
                                    $aPoint = array(
                                        'UserID' => $commentInfo->UserID,
                                        'FromUserID' => $tokenInfo->UserID,
                                        'Type' => INF_POINT_LIKE, // 1
                                        'Point' => 3,
                                        'ContentID' => $comment_id,
                                        'ContentType' => 17,
                                        'CreatedDate' => gmdate('Y-m-d H:i:s')
                                    );
                                    $this->Influence_Point_model->add($aPoint);
                                }
                                // push notify to comment's owner
                                if ($commentInfo->UserID != $tokenInfo->UserID) {
                                    // don't push to unfollow user
                                    if (!$this->Stop_Receive_Notify_model->getInfo($commentInfo->UserID, $commentInfo->ContentID, $content_type)) {
                                        // Stupid case - ANOMO-9583
                                        // if A block B
                                        // then C like A's comment on B's post
                                        // should not push notify to A
                                        // don't push to blocked user
                                        if (!$this->Comment_model->checkBlockUser($commentInfo->UserID, $commentInfo->ContentID, $content_type)) {
                                            $isAnonymous = isset($contentInfo->IsAnonymous)?$contentInfo->IsAnonymous: 0;
                                            // add to notification history
                                            $historyData = array(
                                                'UserID' => $commentInfo->UserID,
                                                'SendUserID' => $tokenInfo->UserID,
                                                'Type' => HISTORY_LIKE_COMMENT, // 17
                                                'RefID' => $commentInfo->ContentID,
                                                'ContentID' => $commentInfo->ContentID,
                                                'IsAnonymous'   => $isAnonymous,
                                                'ContentType' => $content_type,
                                                'IsRead' => 1,
                                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                                            );
                                            $history_id = $this->Notification_History_model->add($historyData);
                                            if ($history_id) {
                                                $msgPushNotify = str_replace('%name%', $tokenInfo->UserName, $this->lang->line('like_on_comment'));
                                                $this->mypushnotify->send($commentInfo->UserID, $tokenInfo->UserID, NOTIFY_LIKE_COMMENT, $msgPushNotify, array('ContentID' => $commentInfo->ContentID, 'ContentType' => $content_type, 'HistoryID' => $history_id, 'IsAnonymous' => $isAnonymous));
                                            }
                                        }
                                    }
                                }
                                // return
                                $response->CommentID = $comment_id;
                                $response->NumberOfLike = $numberLike + 1;
                                $response->code = $this->lang->line('ok');
                            } else {
                                $response->code = $this->lang->line('failed');
                            }
                        } else { // unlike
                            $unLike = $this->Comment_model->unLike($tokenInfo->UserID, $comment_id, $content_type);
                            if ($unLike) {
                                // decrease #like
                                $this->Comment_model->updateNumberLikeOfComment($comment_id, $content_type, 'decrease');
                                if($isAnonymousComment != 1){
                                    // ANOMO-10327 - influence point
                                    $aPoint = array(
                                        'UserID' => $commentInfo->UserID,
                                        'FromUserID' => $tokenInfo->UserID,
                                        'Type' => INF_POINT_LIKE, // 1
                                        'Point' => 3,
                                        'ContentID' => $comment_id,
                                        'ContentType' => 17,
                                        'CreatedDate' => gmdate('Y-m-d H:i:s')
                                    );
                                    $this->Influence_Point_model->minusPoint($aPoint);
                                }
                                // return
                                $response->CommentID = $comment_id;
                                $response->NumberOfLike = $numberLike - 1;
                                $response->code = $this->lang->line('ok');
                            } else {
                                $response->code = $this->lang->line('failed');
                            }
                        }
                    }
                }
            }
        }

        echo json_encode($response);
    }

    function likelist($token, $comment_id = 0, $type = "") {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!is_numeric($comment_id) || !is_numeric($type)) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $listlike = $this->Comment_model->getListLike($comment_id, $type, $tokenInfo->UserID);
                $response->likes = $listlike;
                $response->code = $this->lang->line('ok');
            }
        }
        echo json_encode($response);
    }

    /*     * ANOMO-7868
     * Ability to stop receiving notifications on specific threads.
     * 
     * @param type $token
     * @param type $content_id
     * @param type $type
     */

    function stop_receive_notify($token = '', $content_id = 0, $type = -1) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $validTypes = array(ACTIVITY_POST_STATUS, ACTIVITY_POST_PICTURE, ACTIVITY_POST_VIDEO, ACTIVITY_PROMOS);
            if (!is_numeric($content_id) || !is_numeric($type) || !in_array($type, $validTypes)) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $isStopObj = $this->Stop_Receive_Notify_model->getInfo($tokenInfo->UserID, $content_id, $type);
                if (!$isStopObj) {
                    // should STOP receiving notify
                    $data = array(
                        'UserID' => $tokenInfo->UserID,
                        'ContentID' => $content_id,
                        'Type' => $type,
                        'CreatedDate' => gmdate('Y-m-d H:i:s')
                    );
                    $re = $this->Stop_Receive_Notify_model->add($data);
                    if (!$re) {
                        $response->code = $this->lang->line('failed');
                    } else {
                        $response->code = $this->lang->line('ok');
                    }
                } else {
                    // update become continues receiving this notify
                    $re = $this->Stop_Receive_Notify_model->delete(array('ID' => $isStopObj->ID));
                    if (!$re) {
                        $response->code = $this->lang->line('failed');
                    } else {
                        $response->code = $this->lang->line('ok');
                    }
                }
            }
        }

        echo json_encode($response);
        exit();
    }
    
    /**delete comment
     * 
     * @param type $token
     * @param type $comment_id
     */
    function delete($token = '', $comment_id = 0) {
        $response = new stdClass();
        if (!is_numeric($comment_id)) {
            $response->code = $this->lang->line('invalid_request');
        } else {
            $tokenInfo = $this->Wstoken_model->getToken($token);
            if (!$tokenInfo) {
                $response->code = $this->lang->line('invalid_token');
            } else {
                $is_valid = 0;
                $commentInfo = $this->Activity_model->getComment($comment_id);
                if ($commentInfo) {
                    // The owner can delete post's comment
                    if ($commentInfo->UserID == $tokenInfo->UserID) {
                        $is_valid = 1;
                    } else {
                        // Mod can delete post's comment
                        $contentInfo = $this->Activity_model->getTopicInfoOfContent($commentInfo->ContentID, $commentInfo->Type);
                        if ($contentInfo && $contentInfo->ModID == $tokenInfo->UserID) {
                            $is_valid = 1;
                        }
                    }
                }
                if ($is_valid) {
                    // delete comment
                    $this->Activity_model->increase($commentInfo->ContentID, $commentInfo->Type, 'comment', 'decrease');
                    $this->Comment_model->deleteComment($comment_id);
                    $response->code = $this->lang->line('ok');
                } else {
                    $response->code = $this->lang->line('invalid_request');
                }
            }
        }
        echo json_encode($response);
    }

}

?>
