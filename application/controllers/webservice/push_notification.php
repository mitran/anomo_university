<?php
class Push_Notification extends Ext_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Notification_History_model');
        $this->load->model('User_model');
        $this->load->model('Wstoken_model');
        $this->load->model('Config_model');
        $this->load->model('Anomotion_model');
        $this->load->model('Place_post_model');
        $this->load->model('Post_Comment_model');
        $this->load->model('Gift_Send_model');
        $this->load->model('Chat_model');
        $this->load->model('Neighborhood_Post_model');
        $this->load->model('Neighborhood_Comment_model');
        $this->load->model('Activity_model');
        $this->lang->load('ws');
    }

    /**
     * return list notification history
     *
     * @param string $token
     * @return json
     */

    function get_notification_history($token = '', $is_get_new = 0, $page = 1) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $limit = 10;
            $offset = ($page - 1)*$limit;
            $total = 0;
            $new_notification_number = 0;
            $user_id = $tokenInfo->UserID;
            $notificationHistory = $this->Notification_History_model->getListNotificationHistory($user_id, $is_get_new, $total, $offset, $limit, $new_notification_number);
            if ($notificationHistory) {
                foreach ($notificationHistory as $key => $re) {
                    $re->Content = "";
                    $re->Photo = "";
                    $re->GiftName = "";
                    $re->PostID = "";
                    $re->PostCreatedDate = "";
                    $re->PostContent = "";
                   // $re->ContentID = ($re->RefID != NUll) ? $re->RefID : '';
                    $re->PostOwnerID = '';
                    $re->PostOwnerName = '';

                    if ($re->RefID) {
                        /*
                        if ($re->Type == 1) {
                            //2014-01-08
                            //hmmm
                            // if ContenType is null => it meant the old gift send
                            // when show gift on activity stream we need save the ContentID instead of GiftID
                            if ($re->ContentType == 9) {
                                $re->ContentID = $re->RefID;
                                $giftSendObj = $this->Gift_Send_model->findById($re->RefID);
                                if ($giftSendObj) {
                                    $giftInfo = $this->Gift_Send_model->getGiftInfo($giftSendObj->GiftID);
                                    $re->GiftName = isset($giftInfo->Name) ? $giftInfo->Name : '';
                                }
                            } else {
                                $giftInfo = $this->Gift_Send_model->getGiftInfo($re->RefID);
                                $re->GiftName = isset($giftInfo->Name) ? $giftInfo->Name : '';
                            }
                        }
                        */

                       if ($re->Type == 14) { // comment activity
                            if ($re->ContentID == '') {
                                $comment = $this->Activity_model->getComment($re->RefID, $re->ContentType);
                                $re->ContentID = $comment ? $comment->ContentID : '';
                            }
                            if ($re->ContentID != '') {
                                $contentInfo = $this->Activity_model->getContentInfo($re->ContentID, $re->ContentType);
                                if ($contentInfo) {
                                    $re->PostOwnerID = $contentInfo->UserID;
                                    $re->PostOwnerName = $contentInfo->UserName;
                                }
                            }
                            //anonymous comment
                            if($re->IsAnonymousComment == 1){
                                $re->SendUserID = "";
                                $re->UserName = "Anonymous";
                                $re->Avatar = $this->config->item('s3_photo_url').'avatar_anonymous.png';
                                //$re->PostOwnerID = "";
                                $re->PostOwnerName = "Anonymous";
                                $re->FacebookID = "";
                            }
                        }

                        if ($re->Type == 13) { // like activity
                            if ($re->ContentID == ''){ // old data
                                $like = $this->Activity_model->getLike($re->RefID, $re->ContentType);
                                $re->ContentID = $like ? $like->ContentID : '';
                            }
                            $re->PostOwnerID = $tokenInfo->UserID;
                            $re->PostOwnerName = $tokenInfo->UserName;
                        }

                        // mention to @username
                        if ($re->Type == 16) {
                            if ($re->ContentID == ''){
                                $re->ContentID = $re->RefID;
                            }
                            $re->PostOwnerID = $re->SendUserID;
                            $re->PostOwnerName = $re->UserName;
                        }
                        
                        //
                        if (empty($re->ContentID)){
                            $re->ContentID = $re->RefID ;
                        }
                    }
                }
                //update is_read
                if ($is_get_new == 0){
                    $this->Notification_History_model->updateIsReadWithoutMessage($user_id);
                }
            }
            $response->NotificationHistory = $notificationHistory;

            
            $response->NewNotificationsNumber = $new_notification_number;
            $response->Page = $page;
            $response->TotalPage = ceil($total/$limit);
            $response->code = $this->lang->line('ok');
        }
        echo json_encode($response);
    }

    /**decrease #badge and mark as read a history
     * 
     * @param type $token
     * @param type $history_id
     */
    function read($token = '', $history_id = -1,$content_id = -1, $content_type = -1) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $ret = $this->Notification_History_model->read($history_id, $content_id, $content_type, $tokenInfo->UserID);
            if (!$ret) {
                $response->code = $this->lang->line('failed');
            } else {
                $response->code = $this->lang->line('ok');
            }
        }
        echo json_encode($response);
    }
}

?>