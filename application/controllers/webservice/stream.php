<?php

class Stream extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Wstoken_model');
        $this->load->model('Topic_model');
        $this->load->model('Topic_Category_model');
        $this->load->model('Activity_model');
        $this->load->model('User_model');
        $this->load->model('User_Profile_Status_model');
        $this->load->model('Picture_Post_Activity_model');
        $this->load->model('Hashtag_model');
        $this->load->model('Video_model');
        $this->load->model('Cron_Mention_User_model');
        $this->load->model('Notification_History_model');
        $this->lang->load('ws');
        $this->load->library('S3Upload');
    }

    function get_list_category($token = ''){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $is_admin = isset($tokenInfo->IsAdmin)?$tokenInfo->IsAdmin:0;
            $response->results = $this->Topic_Category_model->getListCategory($is_admin);
            $response->code = $this->lang->line('ok');
        }
        echo json_encode($response);
    }
    
    function get_list_category_v1($token = ''){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $is_admin = isset($tokenInfo->IsAdmin)?$tokenInfo->IsAdmin:0;
            $results = array(
                'ListCategory' => $this->Topic_Category_model->getListCategory($is_admin),
                'ListTopic' => $this->Topic_Category_model->showTopicOnCateScreen()
            );
            $response->results = $results;
            $response->code = $this->lang->line('ok');
        }
        echo json_encode($response);
    }
    
    function get_list_topic($token = '',$cate_id = 0, $page = 1){
        $response = new stdClass();
        if(empty($token) || !is_numeric($cate_id)){
            $response->code = $this->lang->line('invalid_parameter');
        }else{
            $tokenInfo = $this->Wstoken_model->getToken($token);
            if (!$tokenInfo) {
                $response->code = $this->lang->line('invalid_token');
            } else {
                $limit = 15;
                $offset = ($page - 1) * $limit;
                $total = 0;
                $result = $this->Topic_model->getListTopicByCategory($tokenInfo->UserID,$cate_id, $total, $offset, $limit);
                $response->results = $result;
                $response->TotalPage = ceil($total/$limit);
                $response->CurrentPage = $page;
                $response->code = $this->lang->line('ok');
            }
        }
        
        echo json_encode($response);
    }
    
    function get_list_favorite($token = '', $page = 1){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $limit = 10;
            $offset = ($page - 1) * $limit;
            $total = 0;
            $response->results = $this->Topic_model->getListFavorite($tokenInfo->UserID, $total, $offset, $limit);
            $response->TotalPage = ceil($total/$limit);
            $response->CurrentPage = $page;
            $response->code = $this->lang->line('ok');
        }
        echo json_encode($response);
    }
    
    function add_favorite($token = '', $topic_id = 0){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!is_numeric($topic_id)){
                $response->code = $this->lang->line('invalid_parameter');
            }else{
                $topicInfo = $this->Topic_model->getTopicInfo($topic_id);
                if (!$topicInfo){
                    $response->code = $this->lang->line('invalid_request');
                }else{
                    $isFavorite = $this->Topic_model->isFavorite($tokenInfo->UserID, $topic_id);
                    if ($isFavorite){ // remove favorite
                         $ret = $this->Topic_model->removeFavorite($tokenInfo->UserID, $topic_id);
                         if ($ret) {
                            $response->code = $this->lang->line('ok');
                        } else {
                            $response->code = $this->lang->line('failed');
                        }
                    }else{ // add favorite
                            $this->Topic_model->addFavorite($tokenInfo->UserID, $topic_id);
                            $response->code = $this->lang->line('ok');
                    }

                }
            }
        }
        echo json_encode($response);
    }
    
    function search_topic($token = '', $page = 1){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $keyword = isset($_POST['Keyword'])?trim($_POST['Keyword']):'';
            if ($keyword != ''){
                $cate_id = isset($_POST['CateID'])?$_POST['CateID']: 0;
                $limit = 10;
                $offset = ($page - 1) * $limit;
                $total = 0;
                $response->results = $this->Topic_model->searchTopic($tokenInfo->UserID, $keyword, $cate_id, $total, $offset, $limit);
                $response->TotalPage = ceil($total/$limit);
                $response->CurrentPage = $page;
                $response->code = $this->lang->line('ok');
            }else{
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }
    
    function add_topic($token) {
        $response = new stdClass();
        $success = 0;
        if (!isset($_POST)) {
            $response->code = $this->lang->line('invalid_parameter');
        } else {
            if (!isset($_FILES['Photo']) || empty($_POST['TopicName']) || empty($_POST['Desc']) || !is_numeric($_POST['CateID'])) {
                $response->code = $this->lang->line('invalid_parameter');
            } else {
                $tokenInfo = $this->Wstoken_model->getToken($token);
                if (!$tokenInfo) {
                    $response->code = $this->lang->line('invalid_token');
                } else {
                    $user_id = $tokenInfo->UserID;
                    // allow create topic
                    $catInfo = $this->Topic_Category_model->getCateInfo($_POST['CateID']);
                    if (!$catInfo) {
                        $response->code = $this->lang->line('invalid_request');
                    } else {
                        if ($catInfo->AllowCreateTopic == 0 && $tokenInfo->IsAdmin != 1) {
                            $response->code = $this->lang->line('not_allow_create_topic');
                        } else {
                            // ANOMO-12595 Allow maximum of 3 streams created per user
                            $error_limit_topic = false;
                            $limitTopicPerUser = $this->Config_model->getConfig('LIMIT_TOPIC_PER_USER');
                            if (!$limitTopicPerUser) $limitTopicPerUser = 0;
                            if ($limitTopicPerUser > 0){
                                $numberTopicOfUser = $this->Topic_model->getTotalTopicCreatedByUser($tokenInfo->UserID);
                                if ($numberTopicOfUser >= $limitTopicPerUser){
                                    $error_limit_topic = true;
                                }
                            }
                            
                            if ($error_limit_topic){
                                $response->code = $this->lang->line('maximum_topic_allowed');
                            }else{
                                // checking existed topic name
                                $existed_topic = $this->Topic_model->checkExistTopic(trim($_POST['CateID']), trim($_POST['TopicName']));
                                if ($existed_topic) {
                                    $response->TopicID = $existed_topic->TopicID;
                                    $response->code = $this->lang->line('topic_exist');
                                } else {
                                    // processing add new topic
                                    // user must add new post 
                                    //upload photo            
                                    $isPhoto = $this->s3upload->upload('Photo', '', BUCKET_NAME_STREAM);
                                    if (!$isPhoto) {
                                        $response->code = $this->lang->line('invalid_image');
                                    } else {
                                        $photo = $isPhoto;
                                        $photo100 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 100, 100, BUCKET_NAME_STREAM);
                                    }
                                    
                                    $isAnonymous = isset($_POST['IsAnonymous'])?$_POST['IsAnonymous']:0; 
                                    $content = isset($_POST['Content'])?trim($_POST['Content']):'';
                                    $isPostValid = 0;
                                    $postType = 0;
                                    $pPhoto = '';
                                    $pPhoto100 = '';
                                    $photo200 = '';
                                    $photo300 = '';
                                    $video = '';
                                    $photoError = 0;
                                    $videoError = 0;
                                    
                                    if ($content != ''){
                                        // post text
                                        $isPostValid = 1;
                                        $postType = ACTIVITY_POST_STATUS;
                                    }
                                    
                                    if (isset($_FILES['PostPhoto']) && $_FILES['PostPhoto']['size'] > 0) {
                                        // post picture
                                        $isPPhoto = $this->s3upload->upload('PostPhoto');
                                        if (!$isPPhoto) {
                                            $photoError = 1;
                                            $response->description = $this->s3upload->display_errors('', '');
                                        } else {
                                            $pPhoto = $isPPhoto;
                                            $pPhoto100 = $this->s3upload->create_thumbnail($_FILES['PostPhoto']['tmp_name'], $photo, 100, 150);
                                            $isPostValid = 1;
                                            $postType = ACTIVITY_POST_PICTURE;
                                        }
                                        
                                    }
                                    
                                    if (isset($_FILES['Video']) && $_FILES['Video']['size'] > 0) {
                                        $isVideo = $this->s3upload->upload('Video', '', BUCKET_NAME_VIDEO, 'video');
                                        if (!$isVideo) {
                                            $videoError = 1;
                                            $response->description = $this->s3upload->display_errors();
                                        } else {
                                            $video = $isVideo;
                                            $isPostValid = 1;
                                            $postType = ACTIVITY_POST_VIDEO;
                                        }
                                    }
                    
                                    //Create Topic
                                    if (empty($photo) || empty($photo100) || !$isPostValid || $videoError || $photoError) {
                                        $response->code = $this->lang->line('failed');
                                    } else {
                                        
                                        $this->db->trans_begin();
                                        
                                        $aData = array(
                                            'UserID' => $tokenInfo->UserID,
                                            'TopicName' => trim($_POST['TopicName']),
                                            'Desc' => trim($_POST['Desc']),
                                            'Photo' => $photo,
                                            'Photo100' => $photo100,
                                            'TotalPost' => 1,
                                            'CateID' => trim($_POST['CateID']),
                                            'CreatedDate' => gmdate('Y-m-d H:i:s'),
                                            'LastModified' => gmdate('Y-m-d H:i:s')
                                        );
                                        $topic_id = $this->Topic_model->add($aData);
                                        $ups_id = false;
                                        if ($postType == ACTIVITY_POST_VIDEO){
                                            $data = array(
                                                'UserID' => $user_id,
                                                'Content'   => $content,
                                                'VideoSource' => 'upload',
                                                'VideoID' => $video,
                                                'VideoThumbnail' => $pPhoto,
                                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                                            );

                                            $video_id = $video;
                                            $video_thumbnail = $pPhoto;
                                            $video_source = 'upload';
                                            $ups_id = $this->Video_model->add($data);
                                            
                                        }elseif ($postType == ACTIVITY_POST_PICTURE){
                                            // add to PicturePostActivity tbl
                                            $data = array(
                                                'UserID' => $user_id,
                                                'PictureCaption' => $content,
                                                'Photo' => $pPhoto,
                                                'Photo100' => $pPhoto100,
                                                'Photo200' => $photo200,
                                                'Photo300' => $photo300,
                                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                                            );
                                            $ups_id = $this->Picture_Post_Activity_model->add($data);
                                        }elseif ($postType == ACTIVITY_POST_STATUS){
                                            $aResponse = $this->User_Profile_Status_model->parseData($content, $tokenInfo->UserID, 0, ACTIVITY_POST_STATUS, $isAnonymous);
                                            $content = $aResponse['content'];
                                            if($isAnonymous != 1){
                                                $aVideo = isset($aResponse['video'])?$aResponse['video']:false;
                                            }
                                            
                                            
                                            // ANOMO-11390 - if have video
                                            if (isset($aVideo) && $aVideo != false){
                                                $video_id = isset($aVideo['VideoID'])?$aVideo['VideoID']:false;
                                                $video_source = isset($aVideo['VideoSource'])?$aVideo['VideoSource']:false;
                                                $video_thumbnail = isset($aVideo['VideoThumbnail'])?$aVideo['VideoThumbnail']:null;
                                                if ($video_id && $video_source){
                                                    $ups_data = array(
                                                        'UserID' => $user_id,
                                                        'Content' => $content,
                                                        'VideoID'   => $video_id,
                                                        'VideoSource'   => $video_source,
                                                        'VideoThumbnail'    => $video_thumbnail,
                                                        'CreatedDate' => gmdate('Y-m-d H:i:s')
                                                    );
                                                    $ups_id = $this->Video_model->add($ups_data);
                                                    $postType = ACTIVITY_POST_VIDEO;
                                                }
                                            }else{
                                                $ups_data = array(
                                                    'UserID' => $user_id,
                                                    'Content' => $content,
                                                    'CreatedDate' => gmdate('Y-m-d H:i:s'),
                                                    'IsAnonymous' => $isAnonymous
                                                );
                                                $ups_id = $this->User_Profile_Status_model->add($ups_data);
                                            }
                                        }
                                        
                                        if ($ups_id){
                                            $feed = array(
                                                'FromUserID' => $user_id,
                                                'FromUserName' => $tokenInfo->UserName,
                                                'Avatar' => $tokenInfo->Avatar,
                                                'Type' => $postType,
                                                'ActionType' => $postType,
                                                'Message' => $content,
                                                'Image' => isset($video_id)?'':$pPhoto,
                                                'RefID' => $ups_id,
                                                'Lat' => $tokenInfo->Lat,
                                                'Lon' => $tokenInfo->Lon,
                                                'BirthDate' => $tokenInfo->BirthDate,
                                                'Gender' => $tokenInfo->Gender,
                                                'CreatedDate' => gmdate('Y-m-d H:i:s'),
                                                'NeighborhoodID' => $tokenInfo->NeighborhoodID,
                                                'FanPage' => 0,
                                                'TopicID' => $topic_id,
                                                'VideoID'   => isset($video_id)?$video_id:null,
                                                'VideoSource'   => isset($video_source)?$video_source:null,
                                                'VideoThumbnail'    => isset($video_thumbnail)?$video_thumbnail:null,
                                                'IsAnonymous' => $postType == ACTIVITY_POST_PICTURE ? 0 : $isAnonymous,
                                            );
                                            $feed_id = $this->Activity_model->add($feed);
                                            if ($feed_id){
                                                $this->Cron_Mention_User_model->saveHistoryAndPushNotify($content, $user_id, $ups_id, $postType);
    
                                                // ANOMO-7852 add hashtag
                                                $this->Hashtag_model->add($content, $feed_id, $_POST['CateID'], $topic_id);
                                            }
                                        }
                                        
                                        if ($this->db->trans_status() === FALSE) {
                                            $this->db->trans_rollback();
                                        } else {
                                            $this->db->trans_commit();
                                            $success = 1;
                                        }
                                        
                                        if ($success){
                                            //get info
                                            $topicInfo = $this->Topic_model->getTopicInfo($topic_id);
                                            if (!$topicInfo) {
                                                $response->code = $this->lang->line('invalid_request');
                                            } else {
                                                if (!empty($topicInfo->Photo) && !empty($topicInfo->Photo100)) {
                                                    $topicInfo->Photo = $this->config->item('s3_stream_url') . $topicInfo->Photo;
                                                    $topicInfo->Photo100 = $this->config->item('s3_stream_url') . $topicInfo->Photo100;
                                                }
                                                $response->results = $topicInfo;
                                                $response->code = $this->lang->line('ok');
                                            }
                                        }else{
                                            $response->code = $this->lang->line('failed');
                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        echo json_encode($response);
    }

    function edit_topic($token){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
             if(!isset($_POST)){
                $response->code = $this->lang->line('invalid_parameter');
             }else{
                if(empty($_POST['TopicName']) || empty($_POST['Desc']) || empty($_POST['TopicID'])){
                    $response->code = $this->lang->line('invalid_parameter');
                }else{
                    $topic_id = trim($_POST['TopicID']); 
                    $topicinfo = $this->Topic_model->getTopicInfo($topic_id);
                    if(!$topicinfo){ // Topic Exist validation
                        $response->code = $this->lang->line('invalid_request');
                    }else{
                        if($topicinfo->UserID != $tokenInfo->UserID){ //Author Validation
                            $response->code = $this->lang->line('invalid_request');
                        }else{
                            if($this->Topic_model->checkExistTopic($topicinfo->CateID,trim($_POST['TopicName']),$topic_id)){
                                $response->code = $this->lang->line('topic_exist');
                            }else{
                                //upload photo  
                                if(isset($_FILES['Photo']) && !empty($_FILES['Photo'])){
                                    $isPhoto = $this->s3upload->upload('Photo','',BUCKET_NAME_STREAM);
                                    if (!$isPhoto) {
                                        $response->code = $this->lang->line('invalid_image');                        
                                    } else {                                        
                                        $photo = $isPhoto;                        
                                        $photo100 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 100, 100,BUCKET_NAME_STREAM);                    
                                    }
                                }          
                                $aData = array(
                                    'UserID' => $tokenInfo->UserID,
                                    'TopicName' => trim($_POST['TopicName']),
                                    'Desc' => trim($_POST['Desc']),                                                                
                                    'LastModified' => gmdate('Y-m-d H:i:s')
                                );
                                //Create Topic
                                if (!empty($photo) && !empty($photo100)) {
                                    $aData['Photo'] = $photo;
                                    $aData['Photo100'] = $photo100;                          
                                }
                                $updateresult = $this->Topic_model->update($aData,$topic_id);
                                //get info
                                $topicInfo = $this->Topic_model->getTopicInfo($topic_id);
                                if (!$topicInfo){
                                    $response->code = $this->lang->line('invalid_request');
                                }else{
                                    if(!empty($topicInfo->Photo) && !empty($topicInfo->Photo100)){
                                        $topicInfo->Photo = $this->config->item('s3_stream_url').$topicInfo->Photo;
                                        $topicInfo->Photo100 = $this->config->item('s3_stream_url').$topicInfo->Photo100;
                                    }
                                    $response->results = $topicInfo;
                                    $response->code = $this->lang->line('ok');
                                } 
                            }    
                        }
                        
                    }
                    
                                    
                } 
             }
            
        }
        echo json_encode($response);
    }
    
    function get_topic($token = '', $topic_id = 0){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $topicDetail = $this->Topic_model->getTopicDetail($tokenInfo->UserID, $topic_id);
            if (!$topicDetail){
                $response->code = $this->lang->line('invalid_request');
            }else{
                $response->results = $topicDetail;
                $response->code = $this->lang->line('ok');
            }
        }
        echo json_encode($response);
    }
    
    function delete_topic($token = '', $topic_id = 0){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!is_numeric($topic_id)){
                $response->code = $this->lang->line('invalid_parameter');
            }else{
                $topicInfo = $this->Topic_model->getTopicInfo($topic_id);
                if (!$topicInfo){
                    $response->code = $this->lang->line('invalid_request');
                }else{
                    if ($tokenInfo->UserID == $topicInfo->UserID){
                        // delete topic
                        $ret = $this->Topic_model->deleteTopic($topic_id, $tokenInfo->UserID);
                        if (!$ret){
                            $response->code = $this->lang->line('failed');
                        }else{
                            $response->code = $this->lang->line('ok');
                        }
                    }else{
                        $response->code = $this->lang->line('invalid_request');
                    }
                }
            }
        }
        echo json_encode($response);
    }
    
     function close_topic($token = '', $topic_id = 0){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!is_numeric($topic_id)){
                $response->code = $this->lang->line('invalid_parameter');
            }else{
                $topicInfo = $this->Topic_model->getTopicInfo($topic_id);
                if (!$topicInfo){
                    $response->code = $this->lang->line('invalid_request');
                }else{
                    $isClose = $this->Topic_model->isCloseTopic($tokenInfo->UserID, $topic_id);
                    if (!$isClose){
                        $this->Topic_model->closeTopic($tokenInfo->UserID, $topic_id);
                    }
                    $response->code = $this->lang->line('ok');
                }
            }
        }
        echo json_encode($response);
    }
    
    function ban_user($token = '', $user_id = 0, $topic_id = 0){
        $response = new stdClass();
        if (empty($token) || !is_numeric($user_id) || !is_numeric($topic_id)){
            $response->code = $this->lang->line('invalid_parameter');
        }else{
            $tokenInfo = $this->Wstoken_model->getToken($token);
            if (!$tokenInfo) {
                $response->code = $this->lang->line('invalid_token');
            } else {
                $topicInfo = $this->Topic_model->getTopicInfo($topic_id);
                if ($topicInfo && $topicInfo->UserID == $tokenInfo->UserID){
                    $targetUserInfo = $this->User_model->getUserInfo($user_id);
                    if (!$targetUserInfo){
                        $response->code = $this->lang->line('invalid_request');
                    }else{
                        $isBanObj = $this->Topic_model->isBan($topic_id, $user_id);
                        if ($isBanObj){ //This user was banned by mod
                            // incase - unban
                            $ret = $this->Topic_model->unBanUser($topic_id, $user_id);
                            if ($ret){
                                $response->code = $this->lang->line('ok');
                            }else{
                                $response->code = $this->lang->line('failed');
                            }
                        }else{
                            // incase - ban
                            $this->Topic_model->banUser($topic_id, $user_id);
                            $response->code = $this->lang->line('ok');
                        }
                        
                    }
                }else{
                    $response->code = $this->lang->line('invalid_request');
                } 
                
            }
        }
        echo json_encode($response);
    }
}

?>
