<?php
class FbAnomotion extends Ext_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('FbAnomotion_model');
        $this->load->model('Notification_History_model');
        $this->load->model('Devices_model');
        $this->load->model('Config_model');
        $this->load->model('Question_model');
        $this->load->model('FbResult_model');
        $this->load->model('Push_model');
        $this->load->model('Chat_model');
        $this->load->model('Wstoken_model');
        $this->load->model('Place_model');
        $this->load->model('Neighborhood_model');
        $this->lang->load('ws');
        $this->load->library('AC2DM');
        $this->load->library('APNS');
    }

    /**
     * push to target user when user sent anomotion request
     *
     * @param int $user_id
     */
    private function _pushToTargetUser($user_id, $type = 'request', $from_user_id = 0, $anomotion_id = 0, $status = 0, $is_fb_game = 0)
    {
        $pushStatus = 0;
        $gameTypes = array(
            'request' => 1,
            'accept' => 2,
            'result' => 3,
            'completed' => 4,
            'finished' => 5,
        );

        $anomotionInfo = $this->FbAnomotion_model->getAnomotion($anomotion_id);

        // $userInfo = $this->User_model->getUserInfo($from_user_id);
        // $targetUserInfo = $this->User_model->getUserInfo($user_id);

        $avatarlink = '';
        $msg = '';
        $username = ($anomotionInfo->UserID==$from_user_id)?$anomotionInfo->UserName:$anomotionInfo->TargetUserName;
        if ($type == 'accept') {
            $msg = $username . $this->lang->line('msg_anomotion_accept');
        } elseif ($type == 'request') {
            $msg = $this->lang->line('msg_anomotion_request');
        } elseif ($type == 'finished') {
            $msg = str_replace('%username%', $username, $this->lang->line('msg_anomotion_finished'));
        } elseif ($type == 'completed') {
            $msg = str_replace('%username%', $username, $this->lang->line('msg_anomotion_completed'));
        }
        $device = $this->Devices_model->getDeviceOfFbUser($user_id);
        if ($device) {
            // $pushInfo = $this->Push_model->getInfo($user_id);
            //getBadge
            $userInfo = $this->User_model->getUserInfo($device->UserID);
            $total = $this->Notification_History_model->getBadge($userInfo);
            $logData['UserID'] = $device->UserID;
            $logData['CreatedDate'] = gmdate('Y-m-d H:i:s');
            // for ios
            if ($device->Type == 'ios') {
                if (APN_MSG_LENGTH < strlen($msg)) {
                    $msg = substr($msg, 0, APN_MSG_LENGTH) . '...';
                }
                $apnMsg['aps']['alert'] = $msg;
                $sound = 'abc.aiff';
                $apnMsg['aps']['sound'] = (string)$sound;
                $apnMsg['t'] = $status; // anomotion

                $apnMsg['f'] = $from_user_id; // TODO:  get user from fb
                
                $apnMsg['u'] = $device->UserID;
                $apnMsg['n'] = $username;
                $apnMsg['a'] = $anomotion_id;
                $apnMsg['aps']['badge'] = (int)$total;
                
                $apnMsg['fi'] = $from_user_id;
                $apnMsg['fn'] = $username;

                
                $msgToPush = json_encode($apnMsg);
                $logData['Platform'] = 'ios';
                $logData['Message'] = $msgToPush;
                $pushStatus = $this->apns->_pushMessage($logData, $msgToPush, $device->DeviceID, $this->Config_model->getConfig('PUSH_NOTIFICATION_MODE'));
            }

            // for android
            if ($device->Type == 'android') {

                $json[] = 3; // anomotion
                $json[] = $username;
                if (APN_MSG_LENGTH < strlen($msg)) {
                    $msg = substr($msg, 0, APN_MSG_LENGTH) . '...';
                }

                $json[] = $msg;
                $json[] = $total;
                $json[] = isset($avatarlink) ? $avatarlink : '';
                
                $json[] = $from_user_id;  // TODO:  get user from fb
                $json[] = $gameTypes[$type];
                $json[] = $anomotion_id;

                $json[] = $from_user_id;
                $json[] = $username;


                $c2dmMsg = json_encode($json);
                $logData['Platform'] = 'android';
                $logData['Message'] = $c2dmMsg;
               
                    
                $pushStatus = $this->ac2dm->send($device->DeviceID, 'text', $c2dmMsg, $logData);
            }
        }

        return $pushStatus;
    }

    private function _calculateScore($anomotion_id, $user_id, $target_user_id)
    {
        $anomotionInfo = $this->FbAnomotion_model->getAnomotion($anomotion_id);
        $result1 = $this->FbResult_model->getListResultOf($user_id, $anomotion_id);
        $result2 = $this->FbResult_model->getListResultOf($target_user_id, $anomotion_id);
        $sameResult = array_intersect_key($result1, $result2);
        $numberQuestion = $this->Question_model->getNumberQuestionOfGroup($anomotionInfo->QuestionGroupID);
        $percentPerQuestion = 100 / $numberQuestion;
        $percent = 0;
        if ($sameResult) {
            foreach ($sameResult as $key => $value) {
                if ($result1[$key] == $result2[$key]) {
                    $percent += $percentPerQuestion;
                } else {
                    $numberAnswer = $this->Question_model->getNumberAnswerOfQuestion($key);
                    $answer1 = $result1[$key];
                    $answer2 = $result2[$key];
                    $answer1Arr = explode('-', $answer1);
                    $answer2Arr = explode('-', $answer2);
                    $re = array_intersect($answer1Arr, $answer2Arr);
                    $percent += (count($re) * $percentPerQuestion) / $numberAnswer;
                }
            }
        }

        return $percent;
    }

    /**
     * send anomotion request version v2
     *
     * @param string $token
     * @param int $user_id
     * @param int $target_user_id
     * @return json
     */
    function request_v2($token = '', $user_id = 0, $target_user_id = 0, $is_facebook_game = 1) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $user_id = $tokenInfo->FacebookID;
            if (is_numeric($target_user_id) && $user_id != $target_user_id) {

                $this->User_model->updateLastActivity($tokenInfo->UserID, 1);
                $error = 0;
                $checkRequest = $this->FbAnomotion_model->checkRequest($user_id, $target_user_id, $is_facebook_game);

                if ($checkRequest) {
                    if (strtotime($checkRequest->CreatedDate) < strtotime(gmdate("Y-m-d H:i:s", strtotime($this->config->item('expired'))))) {
                        $error = 0;
                    } elseif ($checkRequest->Status == 1) {
                        $response->AnomotionID = $checkRequest->AnomotionID;

                        //get next question of this user
                        $lastQuestion = $this->FbResult_model->getProgressOfUser($user_id, $response->AnomotionID);
                        if ($lastQuestion) {
                            if ($lastQuestion->AnswerID) {
                                $order = isset($lastQuestion->Order) ? $lastQuestion->Order : '';
                            } else {
                                $order = $lastQuestion->Order - 1;
                            }
                        } else {
                            $order = 0;
                        }
                        $nextQuestion = $this->Question_model->getNextQuestion($checkRequest->QuestionGroupID, $order);
                        if ($nextQuestion) {
                            // check is final question - ANOMO-4379
                            $isFinalQuestion = 0;
                            if ($nextQuestion->Order == $this->Question_model->getNumberQuestionOfGroup($nextQuestion->QuestionGroupID)) {
                                $isFinalQuestion = 1;
                            }
                            $nextQuestion->IsFinalQuestion = $isFinalQuestion;

                            $nextQuestion->Answer = $this->Question_model->getAnswerOf($nextQuestion->QuestionID);
                        } else {
                            $nextQuestion = $this->lang->line('no_more');
                        }
                        $response->NextQuestion = $nextQuestion;

                        // get target progress
                        $targetUserID = ($user_id == $checkRequest->UserID) ? $checkRequest->TargetUserID : $checkRequest->UserID;
                        $targetProgress = $this->FbResult_model->getProgressOfUser($targetUserID, $checkRequest->AnomotionID);
                        if ($targetProgress) {
                            if ($targetProgress->AnswerID) {
                                $progress = $targetProgress->Order;
                            } else {
                                $progress = $targetProgress->Order - 1;
                            }
                        } else {
                            $progress = 0;
                        }
                        $response->TargetUserProgress = $progress;
                        $response->code = $this->lang->line('inprogress');
                        $error = 1;
                    }
                }

                if (!$error) {
                    //2013-12-09
                    // dont allow create new game
                    $response->code = 'NO_PENDING_GAME';
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * get anomotion status
     *
     * @param string $token
     * @param int $user_id
     * @param int $anomotion_id
     * @return json
     */
    function get_anomotion_status($token = '', $user_id = 0, $anomotion_id = 0)
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($anomotion_id)) {
                $checkAnomotion = $this->FbAnomotion_model->checkAnomotion(0, $anomotion_id);
                if ($checkAnomotion) {
                    $status = '';
                    switch ($checkAnomotion->Status) {
                        case 1:
                            $status = $this->lang->line('accept');
                            break;
                        case 2:
                            $status = $this->lang->line('cancel');
                            break;
                        case 3:
                            $status = $this->lang->line('finished');
                            break;
                        case 0:
                            $status = $this->lang->line('waiting');
                            break;
                    }
                    $response->Status = $status;
                    if ($checkAnomotion->Status == 1) {
                        $question = $this->FbResult_model->getCurrentQuestionOf($user_id, $anomotion_id);
                        $question->Answer = $this->Question_model->getAnswerOf($question->QuestionID);
                        $response->Question = $question;
                        // return question number of partner
                        $target_user_id = ($checkAnomotion->UserID == $user_id) ? $checkAnomotion->TargetUserID : $checkAnomotion->UserID;
                        $targetUserQuestion = $this->FbResult_model->getProgressOfUser($target_user_id, $anomotion_id);
                        $numberOfGroupQuestion = $this->Question_model->getNumberQuestionOfGroup($targetUserQuestion->QuestionGroupID);
                        if ($targetUserQuestion->AnswerID == NULL) {
                            $targetQuestionNumber = $targetUserQuestion->Order;
                        } else {
                            if ($targetUserQuestion->Order == $numberOfGroupQuestion) {
                                $targetQuestionNumber = 0;
                            }
                        }
                        $response->TargetQuestionNumber = (int)$targetQuestionNumber;
                    }
                    $response->code = $this->lang->line('ok');
                } else {
                    $response->code = $this->lang->line('invalid_request');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }


    function answer($token = '', $user_id = 0, $anomotion_id = 0, $question_id = 0, $answer_id = '')
    {
        $response = new stdClass();
        // verify token...
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else { // Token verified...
            if (is_numeric($user_id) && is_numeric($anomotion_id) && is_numeric($question_id) && trim($answer_id) != '') {
                // get game info... (q & a infos)
                // and check this question belong with this game 
                // and check this answer belong with this question
                $anomotion = $this->FbAnomotion_model->getAnomotion($anomotion_id);
                $groupQuestionID = ($anomotion) ? $anomotion->QuestionGroupID : 0;
                $questionInfo = $this->Question_model->getQuestionInfo($question_id, $groupQuestionID);
                $answerInfo = $this->Question_model->getAnswerInfo($answer_id, $question_id);

                // update last activity
                $this->User_model->updateLastActivity($tokenInfo->UserID, 1);

                // check validity of the requested game
                if (!$anomotion || !$questionInfo || !$answerInfo) {
                    $response->code = $this->lang->line('invalid_request');
                } else { // game validated
                    // get the question details
                    $anomotionQuestion = $this->FbResult_model->checkAnomotionQuestion($user_id, $anomotion_id, $question_id);

                    // update the answer to that question... (2 cases check! If entry exist, and entry not exist - wonder why?)
                    if ($anomotionQuestion) {
                        $updated = $this->FbResult_model->update(array('AnswerID' => $answer_id), array('ID' => $anomotionQuestion->ID));
                    } else {
                        $dataAnswer = array(
                            'AnomotionID' => $anomotion_id,
                            'UserID' => $user_id,
                            'QuestionID' => $question_id,
                            'AnswerID' => $answer_id,
                            'CreatedDate' => gmdate('Y-m-d H:i:s')
                        );
                        $updated = $this->FbResult_model->add($dataAnswer);
                    }

                    if (!$updated) { //if update failed => return
                        $response->code = $this->lang->line('failed');
                    } else { // update result successful

                        // the clutch is here...
                        // get the current user and the target player (who's playing with the current player...
                        $currentUserId = $user_id;
                        $targetUserId = ($anomotion->UserID == $currentUserId) ? $anomotion->TargetUserID : $anomotion->UserID;

                        // get target progress
                        $targetProgress = $this->FbResult_model->getProgressOfUser($targetUserId, $anomotion_id);

                        if ($targetProgress) {
                            if ($targetProgress->AnswerID) {
                                $progress = $targetProgress->Order;
                            } else {
                                $progress = $targetProgress->Order - 1;
                            }
                        } else {
                            $progress = 0;
                        }
                        $response->TargetUserProgress = $progress;

                        //update score
                        $score = $this->_calculateScore($anomotion_id, $anomotion->UserID, $anomotion->TargetUserID);
                        $this->FbAnomotion_model->update(array('Score' => $score), array('AnomotionID' => $anomotion_id));

                        // cases - if current user's no more question to answer:
                        // get next question
                        $nextQuestion = $this->Question_model->getNextQuestion($anomotion->QuestionGroupID, $questionInfo->Order);
                        $targetQuestion = $this->FbResult_model->getQuestionStatusOf($targetUserId, $anomotion_id);

                        // get target user status
                        if ($anomotion->Status == 2) {
                            $targetUserStatus = $this->lang->line('cancel');
                        } else {
                            if ($targetQuestion->AnswerID)
                                $targetUserStatus = $this->lang->line('finished');
                            else { // if target user's also finished...
                                $targetUserStatus = $this->lang->line('inprogress');
                            }
                        }

                        $response->TargetUserStatus = $targetUserStatus;
                        $response->NextQuestion = isset($nextQuestion) ? $nextQuestion : '';
                        $response->code = $this->lang->line('ok');

                        // handle push notification
                        if ($nextQuestion) { // if there's still next question
                            // update next question for this user
                            $nextQuestion->Answer = $this->Question_model->getAnswerOf($nextQuestion->QuestionID);

                            // check is final question - ANOMO-4379
                            $isFinalQuestion = 0;
                            if ($nextQuestion->Order == $this->Question_model->getNumberQuestionOfGroup($nextQuestion->QuestionGroupID)) {
                                $isFinalQuestion = 1;
                            }
                            $nextQuestion->IsFinalQuestion = $isFinalQuestion;

                            $toUser = array(
                                'AnomotionID' => $anomotion_id,
                                'UserID' => $currentUserId,
                                'QuestionID' => $nextQuestion->QuestionID,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                            );
                            $this->FbResult_model->add($toUser);
                        } else { // if no more question... aka completed his / her game
                            // first: check if the target user's also completed all their questions
                            if ($anomotion->Status != 2) {
                                $targetUserInfo = $this->User_model->getByFbID($targetUserId);

                                // if current user still has question... let him / her know that the current user's finished the game
                                if (!$targetQuestion->AnswerID) {

                                    if ($targetUserInfo && $targetUserInfo->AllowAnomotionNotice == 1) {
                                        self::_pushToTargetUser($targetUserId, 'finished', $currentUserId, $anomotion_id, 12, 1);
                                    }
                                } else { // if target user's also finished...
                                    if ($progress == $this->Question_model->getNumberQuestionOfGroup($anomotion->QuestionGroupID)) {
                                        // update Anomotion status become finished

                                        $this->FbAnomotion_model->update(array('Status' => 3, 'CreatedDate' => gmdate('Y-m-d H:i:s')), array('AnomotionID' => $anomotion_id));
                                        
                                        $targetUserName = $anomotion->UserID == $user_id?$anomotion->TargetUserName:$anomotion->UserName;
                                        $currentUserName = $anomotion->UserID == $user_id?$anomotion->UserName:$anomotion->TargetUserName;
                                        // add to push notification historys
                                        $pushDataToUser = array(
                                            'UserID' => $tokenInfo->UserID,
                                            'SendUserID' => $targetUserInfo?$targetUserInfo->UserID:'',
                                            'Type' => 4, // anomotion result
                                            'Total' => 1,
                                            'AnomotionID' => $anomotion_id,
                                            'IsRead' => 1,
                                            'Score' => $score,
                                            'CreatedDate' => gmdate('Y-m-d H:i:s'),
                                            'IsFacebookGame'    => $anomotion->IsFacebookGame,
                                            'FacebookID'    => $currentUserId,
                                            'SendFacebookID' => $targetUserId,
                                            'SendFacebookName' => $targetUserName
                                        );

                                        $this->Notification_History_model->add($pushDataToUser);

                                        $pushDataToTargetUser = array(
                                            'UserID' => $targetUserInfo?$targetUserInfo->UserID:$targetUserId,
                                            'SendUserID' => $tokenInfo->UserID,
                                            'Type' => 4, // anomotion result
                                            'Total' => 1,
                                            'AnomotionID' => $anomotion_id,
                                            'IsRead' => 1,
                                            'Score' => $score,
                                            'CreatedDate' => gmdate('Y-m-d H:i:s'),
                                            'IsFacebookGame'    => $anomotion->IsFacebookGame,
                                            'FacebookID'    => $targetUserId,
                                            'SendFacebookID'    => $currentUserId,
                                            'SendFacebookName' => $currentUserName
                                        );
                                        $this->Notification_History_model->add($pushDataToTargetUser);
                                        
                                        
                                        // ANOMO-4877 - just push notify to the last person who finished game
                                        if ($targetUserInfo && $targetUserInfo->AllowAnomotionNotice == 1) {

                                            // push notification to user / target user
                                            self::_pushToTargetUser($targetUserId, 'completed', $currentUserId, $anomotion_id, 11, $anomotion->IsFacebookGame);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // end validate
                } // end update answer successful

            }
            else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * get all anomotions
     *
     * @param string $token
     * @param int $userID
     * @return json
     */
    function get_all($token = '', $userID = 0)
    {
        $response = new stdClass();
        if (!$this->commonlib->checkToken($token)) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $response->code = $this->lang->line('invalid_request');
            if (is_numeric($userID)) {
                $results = $this->FbAnomotion_model->getAnomotionsByUserID($userID);
                foreach ($results as &$entry) {
                    //modify status
                    $status = '';
                    switch ($entry->Status) {
                        case 0:
                            $status = ($userID == $entry->UserID) ? $this->lang->line('waiting') : $this->lang->line('request_waiting');
                            break;
                        case 1:
                            $status = $this->lang->line('inprogress');
                            break;
                        case 2:
                            $status = $this->lang->line('cancel');
                            break;
                        case 3:
                            $status = $this->lang->line('finished');
                            break;
                    }
                    $entry->Status = $status;

                    //add full image path
                    if ($entry->TargetAvatar) {
                        $entry->TargetAvatar = $this->config->item('s3_photo_url') . $entry->TargetAvatar;
                    }

                    //get ConnectMeter
                    $entry->ConnectMeter = $this->User_model->getConnectMeter($userID, $entry->TargetUserID);
                }
                $response->results = $results;
                $response->code = $this->lang->line('ok');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * result anomotion
     *
     * @param string $token
     * @param int $user_id
     * @param int $anomotion_id
     * @return json
     */
    function result($token = '', $user_id = 0, $anomotion_id = 0)
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if ( is_numeric($anomotion_id)) {
               
                $anomotionInfo = $this->FbAnomotion_model->getAnomotion($anomotion_id);
                if ($anomotionInfo && ($anomotionInfo->UserID == $user_id || $anomotionInfo->TargetUserID == $user_id) && $anomotionInfo->Status == 3) {
                    $targetUserID = $anomotionInfo->UserID == $user_id ? $anomotionInfo->TargetUserID : $anomotionInfo->UserID;

                    $response->TargetUserID = $targetUserID;
                    $response->TargetUserName = $anomotionInfo->UserID == $user_id ? $anomotionInfo->TargetUserName : $anomotionInfo->UserName;
                    $response->Score = $anomotionInfo->Score . '%';
                    $response->code = $this->lang->line('ok');
                } else {
                    $response->code = $this->lang->line('no_result');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * get_all_anomotion_between_2_user
     *
     * @param string $token
     * @param int $userID
     * @param int $targetUserID
     * @return json
     */
    function get_all_anomotion_between_2_user($token = '', $userID = 0, $targetUserID = 0, $is_facebook_game = 1)
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $response->code = $this->lang->line('invalid_request');
                $userID = $tokenInfo->FacebookID;
                $this->User_model->updateLastActivity($tokenInfo->UserID, 1);
                $results = $this->FbAnomotion_model->getAnomotionsBy2User($userID, $targetUserID, $is_facebook_game);
                foreach ($results as &$entry) {
                    //modify status
                    $status = '';
                    switch ($entry->Status) {
                        case 0:
                            $status = ($userID == $entry->UserID) ? $this->lang->line('waiting') : $this->lang->line('request_waiting');
                            break;
                        case 1:
                            $status = $this->lang->line('inprogress');
                            break;
                        case 2:
                            $status = $this->lang->line('cancel');
                            break;
                        case 3:
                            $status = $this->lang->line('finished');
                            break;
                    }
                    $entry->Status = $status;
                    $entry->Avatar = "";
                    $entry->TargetAvatar = "";
                    //get ConnectMeter
                    $entry->ConnectMeter = 1; //$this->User_model->getConnectMeter($userID, $entry->TargetUserID);
                }
                $response->results = $results;
                $response->code = $this->lang->line('ok');
           
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * get_finish_anomotion
     *
     * @param string $token
     * @param int $userID
     * @param int $anomotionID
     * @return json
     */
    function get_finish_anomotion($token = '', $anomotionID = 0)
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {

            $anomotion = $this->FbAnomotion_model->getAnomotion($anomotionID);
            if (is_numeric($anomotionID) && $anomotion) {

                // update last activity
                $this->User_model->updateLastActivity($tokenInfo->UserID, 1);

                $results = array();
                $image = '';
                if ($anomotion->Status == 3) {
                    $questions = $this->Question_model->getQuestionOfGroup($anomotion->QuestionGroupID);
                    foreach ($questions as &$entry) {
                        $tmp = new stdClass();
                        $tmp->Question = $entry->Content;

                        $optionAnswer = array();
                        $answers = $this->Question_model->getAnswerOf($entry->QuestionID);
                        foreach ($answers as $item) {
                            $optionAnswer[$item->AnswerID] = $item->Content;
                        }

                        $result = $this->FbResult_model->getListResultOfQuestion($anomotionID, $entry->QuestionID);
                        foreach ($result as $item) {
                            $_answerArray = explode('-', $item->AnswerID);
                            foreach ($_answerArray as $value) {
                                $tmp->Answer[$item->UserID][] = isset($optionAnswer[$value]) ? $optionAnswer[$value] : '';
                            }
                        }
                        $results[] = $tmp;
                    }
                    $response->Score = $anomotion->Score;

                    // return fb share img
                    $user_id = $tokenInfo->FacebookID;
                    $image_fb = FCPATH . 'public/share/' . $anomotionID . '_' . $user_id . '_1.jpg';
                    if (file_exists($image_fb)) {
                        $image = base_url() . 'public/share/' . $anomotionID . '_' . $user_id . '_1.jpg';
                    }
                }
                $response->Image = $image;
                $response->results = $results;
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * get anomotion info
     *
     * @param string $token
     * @param int $user_id
     * @param int $anomotion_id
     * @return json
     */
    function get_anomotion_info($token = '', $user_id = 0, $anomotion_id = 0)
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($anomotion_id)) {
                $user_id = $tokenInfo->FacebookID;
                $checkAnomotion = $this->FbAnomotion_model->checkAnomotion(0, $anomotion_id);
                if ($checkAnomotion) {
                    $status = ''; // this string will return
                    $statusGame = $checkAnomotion->Status;
                    switch ($statusGame) {
                        case 1:
                            $status = $this->lang->line('accept');
                            break;
                        case 2:
                            $status = $this->lang->line('cancel');
                            break;
                        case 3:
                            $status = $this->lang->line('finished');
                            break;
                        case 0:
                            if ($checkAnomotion->UserID == $user_id) {
                                $status = $this->lang->line('request_sent');
                            } elseif ($checkAnomotion->TargetUserID == $user_id) {
                                $status = $this->lang->line('request_waiting');
                            }
                            break;
                    }

                    // recheck if this game is finished
                    // get number question of group
                    $numberQuestion = $this->Question_model->getNumberQuestionOfGroup($checkAnomotion->QuestionGroupID);
                    $target_user_id = ($checkAnomotion->UserID == $user_id) ? $checkAnomotion->TargetUserID : $checkAnomotion->UserID;

                    // get progress of this game
                    $targetProgress = $this->FbResult_model->getProgressOfUser($target_user_id, $anomotion_id);
                    $progress1 = 0;
                    if ($targetProgress) {
                        if ($targetProgress->AnswerID) {
                            $progress1 = $targetProgress->Order;
                        } else {
                            $progress1 = $targetProgress->Order - 1;
                        }
                    }else{
                        $progress1 = 0;
                    }

                    $userProgress = $this->FbResult_model->getProgressOfUser($user_id, $anomotion_id);
                    $progress2 = 0;
                    if ($userProgress) {
                        if ($userProgress->AnswerID) {
                            $progress2 = $userProgress->Order;
                        } else {
                            $progress2 = $userProgress->Order - 1;
                        }
                    }else{
                        $progress2 = 0;
                    }
                    if ($numberQuestion == $progress1 && $numberQuestion == $progress2) {
                        // update status this game is finished
                        $updated = $this->FbAnomotion_model->update(array('Status' => 3, 'CreatedDate' => gmdate('Y-m-d H:i:s')), array('AnomotionID' => $anomotion_id));
                        if ($updated) {
                            $status = $this->lang->line('finished');
                            $statusGame = 3;
                        }

                    }


                    if (isset($checkAnomotion->CreatedDate) && (strtotime($checkAnomotion->CreatedDate) < strtotime(gmdate('Y-m-d H:i:s', strtotime($this->config->item('expired')))))) {
                        $status = 'EXPIRED';
                    }
                    $response->Status = $status;
                    if ($statusGame == 1) {

                        //get next question of this user
                        $nextQuestion = $this->Question_model->getNextQuestion($checkAnomotion->QuestionGroupID, $progress2);
                        if ($nextQuestion) {
                            $nextQuestion->Answer = $this->Question_model->getAnswerOf($nextQuestion->QuestionID);
                            // check is final question - ANOMO-4379
                            $isFinalQuestion = 0;
                            if ($nextQuestion->Order == $this->Question_model->getNumberQuestionOfGroup($nextQuestion->QuestionGroupID)) {
                                $isFinalQuestion = 1;
                            }
                            $nextQuestion->IsFinalQuestion = $isFinalQuestion;
                        } else {
                            $nextQuestion = $this->lang->line('no_more');
                        }
                        $response->Question = $nextQuestion;


                        // return question number of partner
                        // fix bug - return wrong status when play game again
                        if ($targetProgress) {
                            $numberOfGroupQuestion = $this->Question_model->getNumberQuestionOfGroup($targetProgress->QuestionGroupID);
                            if ($targetProgress->AnswerID == NULL) {
                                $targetQuestionNumber = $targetProgress->Order;
                            } else {
                                if ($targetProgress->Order == $numberOfGroupQuestion) {
                                    $targetQuestionNumber = 0;
                                }
                                else
                                    //  $targetQuestionNumber = 0; // Hung's temporary fix to the issue with null targetQuestionNumber...
                                    $targetQuestionNumber = $targetProgress->Order + 1;
                            }
                        } else {
                            $targetQuestionNumber = 0;
                        }
                        $response->TargetQuestionNumber = (int)$targetQuestionNumber;
                    }
                    
                    $response->TargetUserID = $target_user_id;
                    $response->TargetUserName = $checkAnomotion->UserID == $user_id ? $checkAnomotion->TargetUserName : $checkAnomotion->UserName;                    
                    $response->TargetAvatar = '';
                    if ($statusGame == 3) {

                        $response->Score = $checkAnomotion->Score . '%';
                    }
                    $response->code = $this->lang->line('ok');
                } else {
                    $response->code = $this->lang->line('invalid_request');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * delete anomotion
     *
     * @param string $token
     * @param int $user_id
     * @param int $anomotion_id
     * @return json
     */
    function delete_anomotion($token = '', $user_id = 0, $anomotion_id = 0)
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($anomotion_id)) {
                $checkAnomotion = $this->FbAnomotion_model->checkAnomotion(0, $anomotion_id);
                if ($checkAnomotion->Status == 3) { // finished
                    $userDelete = ($checkAnomotion->UserID == $user_id) ? 'IsUserDelete' : 'IsTargetUserDelete';
                    $this->FbAnomotion_model->update(array($userDelete => 1), array('AnomotionID' => $anomotion_id));
                } else {
                    // change status => CANCEL
                    $this->FbAnomotion_model->update(array('Status' => 2), array('AnomotionID' => $anomotion_id));
                }
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    function _validateRequestTo4User($user_id, $target_user_ids)
    {
        $count = 0;
        foreach ($target_user_ids as $target_user_id) {
            $existGame = $this->FbAnomotion_model->checkRequest($user_id, $target_user_id);
            if ($existGame) {
                $count++;
            }
        }
        return ($count > 0) ? true : false;
    }

 

    /**
    delete all game
     */
    function delete_all_game($token = '', $target_user_id = 0)
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($target_user_id)) {
                // get all game of 2 users
                $allAnomotion = $this->FbAnomotion_model->getAllAnomotionBy2User($tokenInfo->UserID, $target_user_id);
                if ($allAnomotion) {
                    foreach ($allAnomotion as $anomotion) {
                        if ($anomotion->Status == 3) { // finished
                            $userDelete = ($anomotion->UserID == $tokenInfo->UserID) ? 'IsUserDelete' : 'IsTargetUserDelete';
                            $this->FbAnomotion_model->update(array($userDelete => 1), array('AnomotionID' => $anomotion->AnomotionID));
                        } else {
                            // change status => CANCEL
                            $this->FbAnomotion_model->update(array('Status' => 2), array('AnomotionID' => $anomotion->AnomotionID));
                        }
                    }
                }
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    function validateGamePlaying($user_id, $target_user_ids, $is_facebook_game = 0)
    {
        $data = array();
        foreach ($target_user_ids as $target_user_id) {
            $existGame = $this->FbAnomotion_model->checkRequest($user_id, $target_user_id, $is_facebook_game);
            if (!$existGame) {
                $data[] = $target_user_id;
            }
        }
        return $data;
    }
    
    /** FB GAME
     * return list game is invited
     * 
     * @param type $token
     */
    function get_list_fb_invited($token = '', $fb_id, $page = 1){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $limit = 100;
            $offset = ($page - 1) * $limit;
            $total = 0;
            $response->ListGame = $this->FbAnomotion_model->getFbGame($fb_id, $total, $offset, $limit);
            $response->CurrentPage = $page;
            $response->TotalPage = ceil($total / $limit);
            $response->code = $this->lang->line('ok');
        }
        echo json_encode($response);

    }

    /** FB GAME
     *  check this fb account is already had playing game with request user
     * 
     * @param string $token
     */
    function check_is_game_playing($token = ''){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!isset($_POST['FacebookIDs']) || trim($_POST['FacebookIDs']) == ''){
                $response->code = $this->lang->line('invalid_request');
            }else{
                $aFbId = explode(',', trim($_POST['FacebookIDs']));
                foreach($aFbId as $id){
                    if (!is_numeric($id)){
                        $error = 1;
                    }
                }
                if (isset($error)){
                    $response->code = $this->lang->line('invalid_request');
                }else{
                    $response->result = $this->FbAnomotion_model->isFBGamePlaying($tokenInfo->FacebookID, $_POST['FacebookIDs']);
                    $response->code = $this->lang->line('ok');
                }
            }
        }
        echo json_encode($response);

    }
    
    /**
     *  [FB GAME] create fb game
     * @param string $token
     */
    function create_fbgame($token = '') {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!isset($_POST['GameResult'])) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $sJson = $_POST['GameResult'];
                $oData = json_decode($sJson);
                if (!isset($oData->FBs) || !isset($oData->Questions) || !isset($oData->QuestionGroupID)) {
                    $response->code = $this->lang->line('invalid_parameter');
                } else {
                    if (!$this->Question_model->validateGameResult($oData)) {
                        $response->code = $this->lang->line('invalid_parameter');
                    } else {
                        
                        $ids = $this->FbAnomotion_model->createGameFb($tokenInfo->UserID, $oData, 1);
                        if (!$ids) {
                            $response->code = $this->lang->line('failed');
                        } else {
                            // push notify to user have anomo account
                            $oAnomotions = $this->FbAnomotion_model->getFbGamesBy($ids);
                            foreach ($oAnomotions as $row) {
                                self::_pushToTargetUser($row->TargetUserID, 'request', $row->UserID, $row->AnomotionID, 10, 1);
                            }
                            $response->code = $this->lang->line('ok');
                        }
                    }
                }
            }
        }
        echo json_encode($response);
    }
    
    /**
     * [FB GAME]
     * 
     * @param string $token
     * @param int $page
     */
    function list_user_play_fbgame($token = '', $page = 1){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            // hotfix - 2013-12-31
            // should handle if trial account call this ws
            if ($tokenInfo->FacebookID != null && $tokenInfo->FacebookID != ''){
                $limit = 10;
                $offset = ($page - 1) * $limit;
                $total = 0;
                $listGame = $this->FbAnomotion_model->getListFbGame($tokenInfo->FacebookID, $total, $offset, $limit);
                $response->CurrentPage = $page;
                $response->TotalPage = ceil($total / $limit);
                $response->ListConversation = $listGame;
                $response->code = $this->lang->line('ok');
            }else{
                $response->code = $this->lang->line('invalid_request');
            }
            
        }
        echo json_encode($response);
    }
}

?>