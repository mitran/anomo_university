<?php

class Influence_Point extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('ws');
        $this->load->model('Influence_Point_model');
        $this->load->model('User_model');
    }

    function week_chart($token = '', $target_user_id = 0) {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $response->result = $this->Influence_Point_model->weekPoint($target_user_id);
            $response->code = $this->lang->line('ok');
        }
        echo json_encode($response);
    }
    
    function total_point($token = '', $target_user_id = 0) {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $userInfo = $this->User_model->getUserInfo($target_user_id);
            if (!$userInfo) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $data[] = array(
                    'Type' => (string) INF_POINT_LIKE, //1
                    'Name' => 'Likes +3pts',
                    'Point' => (string) ($userInfo->NumberPostLike * 3),
                    'Message'   => 'Any time a user likes your status or comment, you earn 3 reputation pts.'
                );

                $data[] = array(
                    'Type' => (string) INF_POINT_FOLLOW, //2
                    'Name' => 'Follower +10pts',
                    'Point' => (string) ($userInfo->NumberOfFollower * 10),
                    'Message'   => 'Attracting followers earns 10 points per follower and –10 pts for being un-followed.'
                );
   
                $data[] = array(
                    'Type' => (string) INF_POINT_INVITE_SMS, //3
                    'Name' => 'Friend Invite +25pts',
                    'Point' => (string) ($userInfo->NumberFriendInvite * 25),
                    'Message'   => 'Sending an SMS invitation to friends earns 25 reputation points per invite.'
                );
                
                $data[] = array(
                    'Type' => (string) INF_POINT_SHARE_FB, // 4
                    'Name' => 'Facebook Shares +10pts',
                    'Point' => (string) ($userInfo->NumberShareFbWall * 10),
                    'Message'   => 'Sharing something to your Facebook wall earns you 10 reputation points.'
                );
                $response->result = $data;
                $response->code = $this->lang->line('ok');
            }
        }
        echo json_encode($response);
    }

}

?>
