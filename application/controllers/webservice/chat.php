<?php

class Chat extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Config_model');
        $this->load->model('Chat_Count_model');
        $this->load->model('Wstoken_model');
        $this->load->model('User_match_model');
        $this->load->library('APNS');
        $this->lang->load('ws');
    }

    function send_ios_chat_push($token = '', $receiver_id = -1) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $msg = isset($_POST['Message']) ? $_POST['Message'] : '';
            if (!empty($msg)) {
                // ANOMO-12005 - How many chat messages were sent between the two users after matching
                $this->User_match_model->increaseNumberChatMsg($tokenInfo->UserID, $receiver_id);
                
                //ANOMO-12073 - Allow users who play daily match and get no response to play another daily match within 3 hours
                $this->User_match_model->updateFirstChatAndReplyDate($tokenInfo->UserID,$receiver_id);
                // get device's user
                $deviceObj = $this->User_model->getPushNotificationReceiverInfo($receiver_id);
                if ($deviceObj) {
                    // only push to IOS device
                    if ($deviceObj->Type == 'ios' && $deviceObj->AllowChatNotice == 1) {
                        if (APN_MSG_LENGTH < strlen($msg)) {
                            $msg = substr($msg, 0, APN_MSG_LENGTH) . '...';    
                        }
  
                        // other badge count
                        $total = $deviceObj->Number;
                        // chat badge count
                        $total += $this->Chat_Count_model->getChatCountOfUser($receiver_id);
                        // increase #chat
                        $total += 1;

                        $sound = 'receiveText.aiff';
                        $apnMsg['aps']['alert'] = $msg;
                        $apnMsg['aps']['badge'] = (int) $total;
                        $apnMsg['aps']['sound'] = (string) $sound;
                        $apnMsg['AIM'] = "YES";
                        $apnMsg['t'] = 1;
                        $apnMsg['f'] = $tokenInfo->UserID; // sender id
                        $apnMsg['n'] = $tokenInfo->UserName ; // sender name
                        $apnMsg['u'] = $receiver_id ; // receive id
						
						$msgToPush = str_replace("\\\\","\\",json_encode($apnMsg));
						
                        $logData['UserID'] = $receiver_id;
                        $logData['CreatedDate'] = gmdate('Y-m-d H:i:s');
                        $logData['Platform'] = 'ios';
                        $logData['Message'] = $msgToPush;
                        $pushStatus = $this->apns->_pushMessage($logData, $msgToPush, $deviceObj->DeviceID, $this->Config_model->getConfig('PUSH_NOTIFICATION_MODE'));
                    }
                }
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }

    function update_client_notification_badge_count($token = '', $chat_count = 0) {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        $this->commonlib->myLog('v200 chat.php update_client_notification_badge_count', array('token' => $token, 'user_id' => $user_id, 'chat_count' => $chat_count));
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!is_numeric($chat_count) || $chat_count < 0){
                $response->code = $this->lang->line('invalid_request');
            }else{
                // reset #ChatCount = 0
                $ret = $this->Chat_Count_model->updateChatCount($user_id, $chat_count);
                if (!$ret){
                    $response->code = $this->lang->line('failed');
                }else{
                    $response->code = $this->lang->line('ok');
                }
            }
        }
        echo json_encode($response);
    }

}

?>
