<?php
class Wstest extends Ext_Controller {
	
	function __construct() {
		parent :: Ext_Controller();

		//$this->load->library("FormatterFactory_lib");
                $this->load->model('User_model');
        $this->load->model('Place_model');
        $this->load->model('Neighborhood_model');
        $this->load->model('Place_Activity_model');
        $this->load->model('Place_Category_model');
        $this->load->model('Wstoken_model');
//        $this->load->model('Profile_model');
        $this->load->model('Tag_model');
        $this->load->model('Gift_Send_model');
        $this->load->model('Chat_model');
        $this->load->model('Post_Comment_model');
        $this->load->model('Credit_model');
        $this->load->model('Config_model');
        $this->load->model('Devices_model');
        $this->load->model('Notification_History_model');
        $this->load->model('Place_Post_model');
        $this->load->model('Mark_Post_Comment_model');
//        $this->load->model('Push_model');
        $this->load->model('Secret_Picture_model');
        $this->load->model('Reveal_model');
        $this->load->model('Anomotion_model');
        $this->load->model('Neighborhood_Checkin_model');
        $this->load->model('Neighborhood_Post_model');
        $this->load->model('Neighborhood_Comment_model');
        $this->load->model('Image_model');
        $this->lang->load('ws');
        $this->load->library('email');
        $this->load->library('Form_validation');
        $this->load->library('AC2DM');
        $this->load->library('APNS');
        $this->load->library('VerifyingAppReceipts');
        $this->load->library('S3Upload');

        $this->load->library('SearchVenue');
        $this->searchvenue = SearchVenue::getInstance();

        $config['upload_path'] = './public/upload/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = PHOTO_SIZE_MAX;
        $config['encrypt_name'] = TRUE;
        $config['remove_spaces'] = TRUE;
        $this->load->library('upload', $config);

	}

	function index() {
	}
	
	function test($token) {
        
		if(!$this->commonlib->checkToken($token))
		{
			$arr_err = array("code"=>1, "description"=>"Invalid Token");
			$result = array("error"=>$arr_err);
		}
		else
		{
	        $result = array("result"=>"hello world!");			
		}				
		$formatter = FormatterFactory::createFormatter("json");
		header('Content-type: ' . $formatter->getContentType());		
		$output = $formatter->formatData($result);
		echo($output); 
        return $result;
	}
	
	public function test_time(){
		$this->load->helper('form');
		$this->benchmark->mark('code_start');
		if (isset($_POST['url']) && $_POST['url'] != ''){
			$urlToCall = $_POST['url'];
//			$urlToCall = "http://localhost/anomo/index.php/webservice/place/get_nearby_places/IWPMXYTFVCVXVWYAPRB9/34/-118";
			$curl = curl_init();  
			curl_setopt($curl, CURLOPT_URL, $urlToCall);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  
			curl_setopt($curl, CURLOPT_FAILONERROR, true);  
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  
			$response = curl_exec($curl);  
			if (!$response) {  
			    $response = curl_error($curl);  
			}  
			curl_close($curl);  
			var_dump($response);
			$this->benchmark->mark('code_end');
			echo $this->benchmark->elapsed_time('code_start', 'code_end') . "(s)";
		}
		$this->load->view('test_time');
	}
	
	function test_mail(){
		$this->load->library('email');
		$this->email->from('support@anomo.com', 'Anomo report');
		$this->email->to('mitran@vinasource.com'); // pandagirl@anomo.com
//                $this->email->reply_to('mitran@vinasource.com', 'Anomo report');
		$this->email->subject('Send from app');
		//$msg = str_replace('%s', base_url().'webservice/user/confirm/'.md5($user_id), $template );
		$this->email->message('Test test test');
		$a = $this->email->send();
                $this->email->print_debugger();
                var_dump($a);
	}
	
	function test_pass($password){
		echo $this->commonlib->encrypt($password);
	}
	
	function test_place_category(){
		$this->load->model('place_category_model');
		$cat = $this->place_category_model->findAll();
		foreach($cat as $ca){
			$url = base_url() . $this->config->item('category_icon_url') . $ca->Name . '.png';
			$file_headers = @get_headers($url);
			if($file_headers[0] == 'HTTP/1.0 404 Not Found') {
				echo $ca->Name . '<br/>';
			}
		}
	}
	
	function testS3Upload(){
		$this->load->library('S3Upload');
		if(isset($_POST['Submit'])){
//			var_dump($_FILES['theFile']);
		  $result = $this->s3upload->upload('theFile');
		  var_dump($result);
		  if ($result){
		  	$resize = $this->s3upload->create_thumbnail($_FILES['theFile']['tmp_name'], $result ,100, 200);
		  	var_dump($resize);
		  }else{
		  		echo $this->s3upload->error;
		  }
		  
		  
		}
		
		$this->load->view('s3upload_form');
	}
	
	function s3list_file(){
		$this->load->library('S3Upload');
//	$file = 'Chrysanthemum.jpg';
		$this->s3upload->listFile();
	//	$a = $this->s3upload->getInfo($file);
	//	var_dump($a);
//	//	$resize = $this->s3upload->create_thumbnail('Chrysanthemum.jpg',100, 200);
//	//	var_dump($resize);
	}
	
	function s3upload_multi(){
	$this->load->library('S3Upload');
		if(isset($_POST['Submit'])){
			var_dump($_FILES['SecretPicture']);
		  $result = $this->s3upload->upload_multi('SecretPicture');
		  var_dump($result);
		  if(!$result){
		 echo $this->s3upload->error;
		  }
		  
		}
		$this->load->view('s3upload_multi_form');
	}
	
	function s3Copy(){
	   $this->load->library('S3Upload');
       $sUrl = 'Upload/3dd1d5c57783f2adf37b77e4af23cf12.jpg';
       $result = $this->s3upload->copyObjectS3($sUrl,'upload');
       var_dump($result);
	}
    
    function s3GetObject(){
        $this->load->library('S3Upload');
        $url = 'upload/3dd1d5c57783f2adf37b77e4af23cf12.jpg';
        $this->s3upload->create_thumbnail('upload/3dd1d5c57783f2adf37b77e4af23cf12.jpg',100, 200);
        return $this->s3upload->getS3Object($url,FCPATH. $this->config->item('photo_url').'3dd1d5c57783f2adf37b77e4af23cf12.jpg');
    }
    
    public function save_image()
    {
        $this->load->library('S3Upload');
        $url='https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQRWd_G4tNG22bmyGMUKtGssPNrG746Wr4JntCPco2yyh2LdpD_ug';
        if ($url != '') {
            $content = @file_get_contents($url);
            if ($content) {
                $file_name = md5(uniqid(mt_rand())) . '.jpg';
                $file_location = FCPATH . $this->config->item('photo_url') . $file_name;
                $re = @file_put_contents($file_location, $content);
                if ($re) {
                    $return= $this->s3upload->_uploadS3($file_location, $file_name);
                }
            }
        }
        echo $return;
        return $return;
    }
    
    
    function gcm_test(){
        $this->load->library('AC2DM');
          
        $id = "APA91bEdfXGyKfscnR9r4ww1bp3Cm1WhVOrhHUFa8LDwbvCpBO4ZxlEzmlTCdz7EEPCwhY_2hJB4kV1UuguXpPg3Olo36wQCLFfc8hmYbO5dMpZDbFJ7YXphOVVNTvJAkdQZ1VWE00L3Wc8F5jz9WPv_QlE8iae7mFsT5_aLyKBPSIn8PJY2DPg";
        $msg = array();
                
        $msg[]=1;
        $msg[]='Duc';
        $msg[]='this mssmss ';
        $msg[]=1;
        $msg[]='http://sandbox240.vinasource.com/public/upload/8f257806aec4ec53da985b579a2ba01c.jpg';
        $msg[]=33118;
        $msg[]=1;
        $msg[]=1687;
        
        $a = json_encode($msg);

//    if ($devicesID){
        $re = $this->ac2dm->send($id, null, $a);
        var_dump($re);
//    }else{
//        echo 'not found';
//    }
//    
        
    }
    
     function fb_anomotion($token = '', $anomotion_id = 0, $type = 0)
    {
        
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($anomotion_id)) {
                $user_id = $tokenInfo->UserID;
                // check this anomotion is finished
                $check = $this->Anomotion_model->checkAnomotionIsFinished($anomotion_id, $user_id);
                if ($check) {
                    // get image if exist
//                    $image = FCPATH . $this->config->item('share_url') . $anomotion_id . '_' . $user_id . '.jpg';
//                    if (file_exists($image)) {
//                        // Hung fixed: never return local url / replace it with S3 url
//                        $response->Image = $this->config->item('s3_photo_url') . $anomotion_id . '_' . $user_id . '.jpg';
//                        $response->code = $this->lang->line('ok');
//                        // ANOMO-5957
//                        if ($type == 1) {
//                            if (!self::shareGame($user_id, $anomotion_id)) {
//                                $response->code = $this->lang->line('error');
//                            }
//                        }
//                    } else {
                        // generate image
                        $image = self::generate_img($anomotion_id, $user_id);
                        if ($image != '') {
                            $response->Image = $image;
                            $response->code = $this->lang->line('ok');
                            // ANOMO-5957
                            if ($type == 1) {
                                if (!self::shareGame($user_id, $anomotion_id)) {
                                    $response->code = $this->lang->line('error');
                                }
                            }
                        } else {
                            $response->code = $this->lang->line('error');
                        }
//                    }
                } else {
                    $response->code = $this->lang->line('invalid');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }

        header("Content-type: text/html;"); 
        echo json_encode($response);
        exit();

    }
    
    function generate_html($anomotion_id, $user_id)
    {
        $this->load->model('Question_model');
        // get info
        $questions = $this->Anomotion_model->getResult($anomotion_id, $user_id);
        if ($questions) {
            foreach ($questions as $question) {
                $question->Answers = $this->Question_model->getAnswerOf($question->QuestionID);
            }
        }
        $data['questions'] = $questions;
        $this->load->view('anomotion_html', $data);
    }
    
    function generate_img($anomotion_id, $user_id)
    {
        // 1. convert html -> pdf
        require_once(APPPATH . "libraries/dompdf/dompdf_config.inc.php");
        $dompdf = new DOMPDF();
        // get html
        $html = base_url() . 'webservice/share/generate_html/' . $anomotion_id . '/' . $user_id;
        $dompdf->load_html_file($html);
        $dompdf->set_paper('640x960');
        $dompdf->render();
        // save to pdf file
        // Format file name: AnomotionID_UserID.pdf
        $pdf = FCPATH . $this->config->item('share_url') . $anomotion_id . '_' . $user_id . '.pdf';
        file_put_contents($pdf, $dompdf->output());

        // 2. convert pdf -> image
        $output = FCPATH . $this->config->item('share_url') . $anomotion_id . '_' . $user_id . '.jpg';
        try {
            $img = new Imagick($pdf . "[0]");

            $img->setImageCompression(Imagick::COMPRESSION_JPEG);
            $img->setImageCompressionQuality(50);

            $img->setImageFormat("jpeg");
            $img->writeImage($output);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        if (file_exists($output)) {
            @unlink($pdf);
            $path_source = FCPATH . $this->config->item('share_url');
            $file_name = $anomotion_id . '_' . $user_id . '.jpg';
            $isUpload = $this->s3upload->_uploadS3($path_source . $file_name, $file_name, 'image/jpeg');
            return $this->config->item('s3_photo_url') . (($isUpload) ? $isUpload : "");
        }
        return "";
    }
    
    function test_image(){
        $pdf = FCPATH . $this->config->item('photo_url') . '1_1.pdf';
      
        $response = new stdClass();
        // 2. convert pdf -> image
        $output = FCPATH . $this->config->item('share_url') .  '1_test.jpg';
        try {
            $img = new Imagick($pdf . "[0]");

            $img->setImageCompression(Imagick::COMPRESSION_JPEG);
            $img->setImageCompressionQuality(50);

            $img->setImageFormat("jpeg");
            $img->writeImage($output);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        $response->Image = $output;
        echo json_encode($response);
    }
    
    function test_cache(){
        
//        $this->load->library('Cache');
//        $this->cached = $this->cache->getCache();
//        
//        var_dump($this->cached);
        
        $host = 'anomo-cache-sb.flbxs5.cfg.usw2.cache.amazonaws.com'; //
        $port  = '11211';
        $memcache = new Memcache;
	$memcache->connect($host, $port) or die ("Could not connect");
        var_dump($memcache);
    }
    
    function clear_cache(){
        $this->load->library('Cache');
        $this->cached = $this->cache->getCache();
        $this->cached->clean();
    }
    
    
    function img_s3(){
//        $f = "http://anomo-sandbox.s3.amazonaws.com/Upload/full_afr_fem_441.png";
//        $a = $this->s3upload->getInfo('Upload/crop_afr_fem_16.png');
//        
//        var_dump($a);die;
////        
//        
//        $f = "http://anomo-sandbox.s3.amazonaws.com/Upload/full_afr_fem_441.png";
//        $b = file_exists($a);
//        var_dump($b);die;
        
        set_time_limit(0);
        $sql = "select * from avatar where gender = 2";
        $query = $this->db->query($sql);
        $result = $query->result();
        $photoUrl = $this->config->item('s3_photo_url');
        if ($result){
            $ids = array();
            foreach($result as $re){
                if ($this->s3upload->getInfo('Upload/'.$re->Avatar) == false || $this->s3upload->getInfo('Upload/'.$re->FullAvatar) == false){
                    $ids[] = $re->ID;
                }
            }
            if (sizeof($ids) > 0){
                $deletesql = "delete from avatar where ID IN (".implode(',', $ids).")";
                $this->db->query($deletesql);
            }
            var_dump($ids);
        }
    }
    
    function avatar_s3(){
        set_time_limit(0);
        $dir = "D:/projects/anomo/AnomoAvatarAndroid/Test/";
        $files = scandir($dir);
//        var_dump($files);
        $content_type = "image/png";
        foreach($files as $file){
            if ($file != '.' && $file != '..'){
                $this->s3upload->_uploadS3($dir.$file, $file, $content_type);
            }
        }
    }
    
    function delete_reveal($user_id, $target_user_id){
        $sql = "delete from reveal where (CurrentUserID = $user_id AND TargetUserID = $target_user_id) or (CurrentUserID = $target_user_id AND TargetUserID = $user_id)";
        $this->db->query($sql);
    }
}
