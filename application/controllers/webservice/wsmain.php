<?php
class Wsmain extends Ext_Controller {
	
	function __construct() {
		parent :: Ext_Controller();

		$this->load->library("FormatterFactory_lib");		
		$this->load->model('Wstoken_model', 'obj_model');
	}

	function index() {
	}
	
	function login($username, $password)
	{	
		$token = false;
		
		$ws_username = $this->config->item('ws_username');
		$ws_password = $this->config->item('ws_password');

		if($ws_username == $username && $ws_password == $password)
		{
	        $data = array();
	        $token = $this->commonlib->gen_uuid();
	        $data["token"] = $token;
	        $this->obj_model->create($data);
	        
	        $result = array("token"=>$token);	        			
		}
		else
		{
			$arr_err = array("code"=>4, "description"=>"Invalid Username and Password.");
			$result = array("error"=>$arr_err);		
		}
		$formatter = FormatterFactory::createFormatter("json");
		header('Content-type: ' . $formatter->getContentType());
		$output = $formatter->formatData($result);
		echo($output);		
		return $result;		
	}		
}