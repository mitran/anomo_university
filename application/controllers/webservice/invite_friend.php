<?php

class Invite_Friend extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Wstoken_model');
        $this->load->model('Sms_model');
        $this->load->model('User_model');
        $this->lang->load('ws');
    }
    
    function sms($token = ''){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $phone_numbers = isset($_POST['PhoneNumbers'])?$_POST['PhoneNumbers']:'';
            if (!empty($phone_numbers)){
                $id = $this->Sms_model->SmsInvite($tokenInfo->UserID, $phone_numbers);
                if(!$id){
                    $response->code = $this->lang->line('invalid_request');
                }else{
                    $response->code = $this->lang->line('ok');
                }
            }else{
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }
}

?>
