<?php
class Sticker extends Ext_Controller{
    function __construct() {
        parent::__construct();
        $this->lang->load('ws');
        $this->load->model('Wstoken_model');
        $this->load->model('Sticker_model');
    }
    
    function get_list_category($token = ''){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $response->results = $this->Sticker_model->getListCategory();
            $response->code = $this->lang->line('ok');
        }
        echo json_encode($response);
    }
    
    function get_list_sticker($token = '', $cate_id = 0) {
        $response = new stdClass();
        if (empty($token) || !is_numeric($cate_id)) {
            $response->code = $this->lang->line('invalid_parameter');
        } else {
            $tokenInfo = $this->Wstoken_model->getToken($token);
            if (!$tokenInfo) {
                $response->code = $this->lang->line('invalid_token');
            } else {
                $response->results = $this->Sticker_model->getListStickerByCategory($cate_id);
                $response->code = $this->lang->line('ok');
            }
        }
        echo json_encode($response);
    }
    
    function check_version($token = ''){
        $response = new stdClass();
        $sticker = isset($_POST['Sticker'])?$_POST['Sticker']:'';
        if (empty($token) || empty($sticker)) {
            $response->code = $this->lang->line('invalid_parameter');
        } else {
            $tokenInfo = $this->Wstoken_model->getToken($token);
            if (!$tokenInfo) {
                $response->code = $this->lang->line('invalid_token');
            } else {
                $response->results = $this->Sticker_model->check_version($sticker);
                $response->code = $this->lang->line('ok');
            }
        }
        echo json_encode($response);
    }
}
?>
