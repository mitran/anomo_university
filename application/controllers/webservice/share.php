<?php

class Share extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Wstoken_model');
        $this->load->model('Config_model');
        $this->load->model('Anomotion_model');
        $this->load->model('FbAnomotion_model');
        $this->load->model('Hashtag_model');
        $this->load->model('Cron_Mention_User_model');
        $this->load->model('User_Profile_Status_model');
        $this->load->model('Influence_Point_model');
        $this->lang->load('ws');
        $this->load->library('S3Upload');
    }

    public function facebook($token = '') {
        $userid = $this->commonlib->checkValidToken($token);
        $response = "";

        if ($userid == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            // track all fb share
            $data = array(
                'UserID' => $userid,
                'IsFirst' => 0,
                'CreatedDate' => gmdate('Y-m-d H:i:s')
            );
            // check if user shared on FB before
            $shareInfo = $this->User_model->isShare($userid);
            //if not, plus 1000 point
            if ($shareInfo == 0) {
                $result = $this->User_model->share($userid);
                $data['IsFirst'] = 1;
            }
            $this->User_model->logShare($data);
            // update SharedLatestDate
            $this->User_model->updateUser(array('SharedLatestDate' => gmdate('Y-m-d H:i:s')), array('UserID' => $userid));
            $response->code = $this->lang->line('ok');
        }
        echo json_encode($response);
    }

    /**
     *  Share Anomo Game
     * @param type $token
     * @param type $anomotion_id
     * @param type $type
     */
    function fb_anomotion($token = '', $anomotion_id = 0, $type = 0) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($anomotion_id)) {
                $user_id = $tokenInfo->UserID;
                // check this anomotion is finished
                $check = $this->Anomotion_model->checkAnomotionIsFinished($anomotion_id, $user_id);
                if ($check) {
                    // get image if exist
                    $image = FCPATH . $this->config->item('share_url') . $anomotion_id . '_' . $user_id . '.jpg';
                    if (file_exists($image)) {
                        // Hung fixed: never return local url / replace it with S3 url
                        $response->Image = $this->config->item('s3_photo_url') . $anomotion_id . '_' . $user_id . '.jpg';
                        $response->code = $this->lang->line('ok');
                        // ANOMO-5957
                        if ($type == 1) {
                            if (!self::shareGame($user_id, $anomotion_id)) {
                                $response->code = $this->lang->line('error');
                            }
                        }
                    } else {
                        // generate image
                        $image = self::generate_img($anomotion_id, $user_id);
                        if ($image != '') {
                            $response->Image = $image;
                            $response->code = $this->lang->line('ok');
                            // ANOMO-5957
                            if ($type == 1) {
                                if (!self::shareGame($user_id, $anomotion_id)) {
                                    $response->code = $this->lang->line('error');
                                }
                            }
                        } else {
                            $response->code = $this->lang->line('error');
                        }
                    }
                } else {
                    $response->code = $this->lang->line('invalid');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }

        echo json_encode($response);
    }

    /** Share FB Game
     * 
     * @param type $token
     * @param type $anomotion_id
     * @param type $type
     */

    function fb_anomotionshare($token = '', $anomotion_id = 0, $type = 0) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($anomotion_id)) {
                $user_id = $tokenInfo->FacebookID;
                // check this anomotion is finished
                $check = $this->FbAnomotion_model->checkAnomotionIsFinished($anomotion_id, $user_id);
                if ($check) {
                    // get image if exist
                    $image = FCPATH . $this->config->item('share_url') . $anomotion_id . '_' . $user_id . '_1.jpg';
                    if (file_exists($image)) {
                        // Hung fixed: never return local url / replace it with S3 url
                        $response->Image = $this->config->item('s3_photo_url') . $anomotion_id . '_' . $user_id . '_1.jpg';
                        $response->code = $this->lang->line('ok');
                        // ANOMO-5957
                        if ($type == 1) {
                            if (!self::shareGame($tokenInfo->UserID, $anomotion_id, 1, $user_id)) {
                                $response->code = $this->lang->line('error');
                            }
                        }
                    } else {
                        // generate image
                        $image = self::generate_img($anomotion_id, $user_id, 1);
                        if ($image != '') {
                            $response->Image = $image;
                            $response->code = $this->lang->line('ok');
                            // ANOMO-5957
                            if ($type == 1) {
                                if (!self::shareGame($tokenInfo->UserID, $anomotion_id, 1, $user_id)) {
                                    $response->code = $this->lang->line('error');
                                }
                            }
                        } else {
                            $response->code = $this->lang->line('error');
                        }
                    }
                } else {
                    $response->code = $this->lang->line('invalid');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }

        echo json_encode($response);
    }

    /**
     * generate html
     *
     * @param unknown_type $anomotion_id
     * @param unknown_type $user_id
     */
    function generate_html($anomotion_id, $user_id, $is_facebook_game = 0) {
        $this->load->model('Question_model');
        // get info
        if ($is_facebook_game == 1) {
            $questions = $this->FbAnomotion_model->getResult($anomotion_id, $user_id);
        } else {
            $questions = $this->Anomotion_model->getResult($anomotion_id, $user_id);
        }
        if ($questions) {
            foreach ($questions as $question) {
                $question->Answers = $this->Question_model->getAnswerOf($question->QuestionID);
            }
        }
        $data['questions'] = $questions;
        $this->load->view('anomotion_html', $data);
    }

    function generate_img($anomotion_id, $user_id, $is_facebook_game = 0) {
        $file_name = $anomotion_id . '_' . $user_id . '.jpg';
        $pdf_file_name = $anomotion_id . '_' . $user_id . '.pdf';
        if ($is_facebook_game == 1) {
            $file_name = $anomotion_id . '_' . $user_id . '_1.jpg';
            $pdf_file_name = $anomotion_id . '_' . $user_id . '_1.pdf';
        }
        // 1. convert html -> pdf
        require_once(APPPATH . "libraries/dompdf/dompdf_config.inc.php");
        $dompdf = new DOMPDF();
        // get html
        $html = base_url() . 'webservice/share/generate_html/' . $anomotion_id . '/' . $user_id . '/' . $is_facebook_game;
        $dompdf->load_html_file($html);
        $dompdf->set_paper('640x960');
        $dompdf->render();
        // save to pdf file
        // Format file name: AnomotionID_UserID.pdf
        $pdf = FCPATH . $this->config->item('share_url') . $pdf_file_name;
        file_put_contents($pdf, $dompdf->output());

        // 2. convert pdf -> image
        $output = FCPATH . $this->config->item('share_url') . $file_name;
        try {
            $img = new Imagick($pdf . "[0]");

            $img->setImageCompression(Imagick::COMPRESSION_JPEG);
            $img->setImageCompressionQuality(50);

            $img->setImageFormat("jpeg");
            $img->writeImage($output);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        if (file_exists($output)) {
            @unlink($pdf);
            $path_source = FCPATH . $this->config->item('share_url');

            $isUpload = $this->s3upload->_uploadS3($path_source . $file_name, $file_name, 'image/jpeg');
            return $this->config->item('s3_photo_url') . (($isUpload) ? $isUpload : "");
        }
        return "";
    }

    function shareGame($user_id, $anomotion_id, $is_facebook_game = 0, $fb_id = 0) {
        $this->load->model('Picture_Post_Activity_model');
        $this->load->model('Activity_model');
        $file_name = $anomotion_id . '_' . $user_id . '.jpg';
        if ($is_facebook_game == 1) {
            $file_name = $anomotion_id . '_' . $fb_id . '_1.jpg';
        }
        $path_source = FCPATH . $this->config->item('share_url');
        $isUpload = $this->s3upload->_uploadS3($path_source . $file_name, $file_name, 'image/jpeg');
        if ($isUpload) {
            $userInfo = $this->User_model->getUserInfo($user_id);
            if ($userInfo) {
                // publish feed
                $photo = $isUpload;
                $p100 = $this->s3upload->create_thumbnail($path_source . $file_name, $photo, 100, 150);
                $photo100 = ($p100) ? $p100 : "";
                $p200 = $this->s3upload->create_thumbnail($path_source . $file_name, $photo, 200, 300);
                $photo200 = ($p200) ? $p200 : "";
                $p300 = $this->s3upload->create_thumbnail($path_source . $file_name, $photo, 300, 450);
                $photo300 = ($p300) ? $p300 : "";

                $content = '';
                if (isset($_POST['Content']) && $_POST['Content'] != '') {
                    $aResponse = $this->User_Profile_Status_model->parseData($_POST['Content'], $user_id);
                    $content = $aResponse['content'];
                }
                // add to PicturePostActivity tbl
                $data = array(
                    'UserID' => $user_id,
                    'PictureCaption' => $content,
                    'Photo' => $photo,
                    'Photo100' => $photo100,
                    'Photo200' => $photo200,
                    'Photo300' => $photo300,
                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                );
                $id = $this->Picture_Post_Activity_model->add($data);
                if ($id) {
                    // publishing a feed
                    if ($this->Config_model->getConfig('ACTIVITY_FEED') == 'on') {
                        // ANOMO-7288 - Show the location of the activity post
                        $feed = array(
                            'FromUserID' => $user_id,
                            'FromUserName' => $userInfo->UserName,
                            'Avatar' => $userInfo->Avatar,
                            'Type' => 27,
                            'ActionType' => 27,
                            'Message' => $content,
                            'Image' => $photo,
                            'RefID' => $id,
                            'Lat' => $userInfo->Lat,
                            'Lon' => $userInfo->Lon,
                            'BirthDate' => $userInfo->BirthDate,
                            'Gender' => $userInfo->Gender,
                            'CreatedDate' => gmdate('Y-m-d H:i:s')
                            , 'CheckinPlaceID' => ''
                            , 'CheckinPlaceName' => ''
                            , 'NeighborhoodID' => $userInfo->NeighborhoodID
                        );
                        $feed_id = $this->Activity_model->add($feed);
                        if ($feed_id) {
                            if ($content != '') {
                                $this->Cron_Mention_User_model->saveHistoryAndPushNotify($content, $user_id, $id, 27);

                                // ANOMO-7852 add hashtag
                                $this->Hashtag_model->add($content, $feed_id);
                            }
                        }
                    }
                    // end publishing a feed
                    return true;
                }
            }
        }
        return false;
    }

    // ANOMO-6916
    // ANOMO-7004
    /*
     *  - Which users clicked on "Invite Friends" and how many friends did they invite
      - Which users clicked on "Share to Wall" popup for the 1000 free pts on initial sign up
      - Which users clicked on "Share to Wall" popup for the 1000 free pts after 3 days
     */
    function invite($token = '', $type = 0, $number = 0) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!in_array($type, array(0, 1)) || $number < 0) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $this->User_model->plusInvite($tokenInfo->UserID, $type, $number);
                $response->code = $this->lang->line('ok');
            }
        }
        echo json_encode($response);
    }
    
    /**ANOMO-11049
     * 
     * @param type $token
     */
    function share_content_to_fb($token = ''){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            // A user can only earn a maximum of # pts per day
            $maxPointPerDay = $this->Config_model->getConfig('MAX_POINT_PER_DAY');
            if (!$maxPointPerDay){
                $maxPointPerDay = 100; // default
            }
            $isReachMaxPointPerDay = $this->Influence_Point_model->maxPointPerDay($tokenInfo->UserID, $maxPointPerDay);
            if (!$isReachMaxPointPerDay){
                // plus 10 points
                $aPoint = array(
                    'UserID' => $tokenInfo->UserID,
                    'Type' => INF_POINT_SHARE_FB, // 4
                    'Point' => 10,
                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                );
                $this->Influence_Point_model->add($aPoint);
            }
            $response->code = $this->lang->line('ok');
            
        }
        echo json_encode($response);
    }
}

?>