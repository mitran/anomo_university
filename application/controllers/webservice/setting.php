<?php

class Setting extends Ext_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Wstoken_model');
        $this->load->model('Config_model');
        $this->load->model('Promo_Code_model');
        $this->load->model('User_model');
        $this->load->model('Promo_Code_model');
        $this->lang->load('ws');
    }
    
    function validate_promo_code(){
        $response = new stdClass();
        $promo_code = isset($_POST['PromoCode'])?trim($_POST['PromoCode']):'';
        if(empty($promo_code)){
            $response->code = $this->lang->line('invalid_parameter');
        }else{
            $promoCodeObj = $this->Promo_Code_model->validatePromoCode($promo_code);
            if ($promoCodeObj){
                $response->ActionType = $promoCodeObj->ActionType?$promoCodeObj->ActionType:"";
                $response->ActionValue = $promoCodeObj->ActionValue?$promoCodeObj->ActionValue:"";
                $response->code = $this->lang->line('ok');
                
            }else{
                $response->code = $this->lang->line('invalid_code');
            }
        }
        echo json_encode($response);
    }
    
    function get_setting(){
        $response = new stdClass();
        $data = array(
            'UsePromoCode' => $this->Config_model->getConfig('UsePromoCode')
        );
        $response->results = $data;
        $response->code = $this->lang->line('ok');
        echo json_encode($response);
    }
    
    function send_email_code($token = '') {
        $response = new stdClass();
        $email = isset($_POST['Email']) ? trim(strtolower($_POST['Email'])) : '';
        $is_resend_code = isset($_POST['IsResendCode']) ? $_POST['IsResendCode'] : 0;
        if (empty($email) || !is_numeric($is_resend_code)) {
            $response->code = $this->lang->line('invalid_email');
        } else {
            // ANOMO-12783 Allow user to type in "skip" for the verification email address
            if ($email == SKIP) {
                $response->code = $this->lang->line('ok');
            } else {
                if (!$this->commonlib->validateEmail($email)) {
                    $response->code = $this->lang->line('invalid_email');
                } else {
                    $user_id = 0;
                    $error = false;
                    if (!empty($token)) {
                        $tokenInfo = $this->Wstoken_model->getToken($token);
                        if (!$tokenInfo) {
                            $response->code = $this->lang->line('invalid_token');
                            $error = true;
                        } else {
                            $user_id = $tokenInfo->UserID;
                        }
                    }
                    if (!$error) {
                        if ($this->User_model->checkExistedEmail($email, $user_id, 1) > 0) {
                            $response->code = $this->lang->line('email_existed');
                        } else {
                            $code = false;
                            $codeExistObj = $this->Promo_Code_model->getEmailCode($email);
                            if ($codeExistObj) {
                                if ($is_resend_code == 1) {
                                    // generate new code
                                    $code = rand(10000, 99999);
                                    // update new code
                                    $this->Promo_Code_model->updateEmailCode(array('Code' => $code), array('Email' => $email));
                                } else {
                                    $code = $codeExistObj->Code;
                                }
                            } else {
                                // generate code
                                $code = rand(10000, 99999);

                                // insert into email_verification
                                $value = array(
                                    'Email' => $email,
                                    'Code' => $code,
                                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                                );
                                $this->Promo_Code_model->addEmailCode($value);
                            }
                            if ($code) {
                                // send email 
                                $this->load->library('email');
                                $this->email->from($this->config->item('email_from'), $this->config->item('email_from_name'));
                                $this->email->to($email);
                                $this->email->subject("[" . NAME_UNIVERSITY . " Social] Verification Email");

                                $msg = "$code is your verification code.";
                                $this->email->message($msg);
                                $sent = $this->email->send();
                                if (!$sent) {
                                    // try again
                                    $sent = $this->email->send();
                                }
                            }

                            if ($sent) {
                                $response->code = $this->lang->line('ok');
                            } else {
                                $response->code = $this->lang->line('failed');
                            }
                        }
                    }
                }
            }
        }
        echo json_encode($response);
    }
    
    function validate_email_code() {
        $response = new stdClass();
        $code = isset($_POST['EmailCode']) ? trim(strtolower($_POST['EmailCode'])) : '';
        $email = isset($_POST['Email']) ? trim(strtolower($_POST['Email'])) : '';
        if (empty($code) || empty($email)) {
            $response->code = $this->lang->line('invalid_code');
        } else {
            // ANOMO-12783 Allow user to type in "skip" for the verification email address
            if ($email == SKIP && $code == SKIP) {
                $response->code = $this->lang->line('ok');
            } else {
                if (!$this->commonlib->validateEmail($email)) {
                    $response->code = $this->lang->line('invalid_code');
                } else {
                    if ($this->User_model->checkExistedEmail($email, 0) > 0) {
                        $response->code = $this->lang->line('email_existed');
                    } else {
                        $emailCodeObj = $this->Promo_Code_model->validateEmailCode($email, $code);
                        if (!$emailCodeObj) {
                            $response->code = $this->lang->line('invalid_code');
                        } else {
                            $response->code = $this->lang->line('ok');
                        }
                    }
                }
            }
        }
        echo json_encode($response);
    }
}

?>
