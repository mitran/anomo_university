<?php

class User extends Ext_Controller {

    function __construct() {
        parent::Ext_Controller();
        $this->load->model('User_model');
        $this->load->model('Place_Category_model');
        $this->load->model('Wstoken_model');
        $this->load->model('Tag_model');
        $this->load->model('Gift_Send_model');
        $this->load->model('Chat_model');
        $this->load->model('Credit_model');
        $this->load->model('Config_model');
        $this->load->model('Devices_model');
        $this->load->model('Notification_History_model');
        $this->load->model('Secret_Picture_model');
        $this->load->model('Reveal_model');
        $this->load->model('Picture_Post_Activity_model');
        $this->load->model('Image_model');
        $this->load->model('User_Profile_Status_model');
        $this->load->model('Activity_model');
        $this->load->model('Hashtag_model');
        $this->load->model('Cron_Mention_User_model');
        $this->load->model('Avatar_model');
        $this->load->model('Intent_model');
        $this->load->model('Neighborhood_model');
        $this->load->model('Influence_Point_model');
        $this->load->model('Blocked_User_1day_model');
        $this->load->model('Promos_model');
        $this->load->model('Blocked_Anonymous_model');
        $this->load->model('Video_model');
        $this->load->model('Campaign_model');
        $this->load->model('Campaign_CAU_model');
        $this->load->model('Campaign_CAU_Daily_model');
        $this->load->model('User_Verification_model');
        $this->load->model('Anomotion_model');
        $this->load->model('User_Match_model');
        $this->load->model('Chat_Count_model');
        $this->load->model('Topic_model');
        $this->load->model('Promo_Code_model');
        $this->load->model('Major_model');

        $this->lang->load('ws');
        $this->load->library('Email');
        $this->load->library('Form_validation');
        $this->load->library('MyPushNotify');
        $this->load->library('VerifyingAppReceipts');
        $this->load->library('S3Upload');
        $this->load->library('MySolr');
        $this->load->library('SearchVenue');
        $this->load->library('Promotionlib');
        $this->load->library('MyTwilio');
        $this->load->library('Pubnub');
        $this->load->library('APNS');
        $this->searchvenue = SearchVenue::getInstance();

		$config['upload_path'] = './public/upload/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = PHOTO_SIZE_MAX;
        $config['encrypt_name'] = TRUE;
        $config['remove_spaces'] = TRUE;
        $this->load->library('upload', $config);
    }

    private function registerDevice($user_id, $device_id, $type) {
        $deviceInfo = $this->Devices_model->getDevice($user_id, $device_id);
        if ($deviceInfo) {
            if ($deviceInfo->Status != 'active') {
                $this->Devices_model->update(array('Status' => 'active'), array('UserID' => $user_id, 'DeviceID' => $device_id));
            }
        } else {
            $deviceValue = array(
                'UserID' => $user_id,
                'DeviceID' => $device_id,
                'Type' => $type,
                'Status' => 'active',
                'Development' => $this->Config_model->getConfig('PUSH_NOTIFICATION_MODE'),
                'CreatedDate' => gmdate('Y-m-d H:i:s')
            );
            $this->Devices_model->add($deviceValue);
        }
        // set other device of user INACTIVE
        $this->Devices_model->update(array('Status' => 'inactive'), "(`UserID` = " . $user_id . " AND `DeviceID` != '" . $device_id . "')");
        $this->Devices_model->update(array('Status' => 'inactive'), "(`UserID` != " . $user_id . " AND `DeviceID` = '" . $device_id . "')");
    }

    private function validateReceipt($receipt, $platform, $signature = '') {
        if ($platform == 'ios') {
            return $this->verifyingappreceipts->verify($receipt);
        } else {
            return $this->verifyingappreceipts->verify_android_market($receipt, $signature);
        }
    }

    private function validateRegister() {
        if (!isset($_POST['UserName']) || (isset($_POST['UserName']) && trim($_POST['UserName']) == '') || !preg_match('/^[a-zA-Z0-9]+$/', $_POST['UserName'])) {
            return true;
        }
        
        if (isset($_POST['Email']) && (trim($_POST['Email']) == '' || !$this->commonlib->validateEmail($_POST['Email']))) {
            // ANOMO-12783 Allow user to type in "skip" for the verification email address
            $email = isset($_POST['Email'])?$_POST['Email']:'';
            $email_code = isset($_POST['EmailCode'])?$_POST['EmailCode']:'';
            if (strtolower($email) != SKIP && strtolower($email_code) != SKIP)
            return true;
        }

        if (!isset($_POST['Gender']) || (isset($_POST['Gender']) && trim($_POST['Gender']) == '')) {
            return true;
        }
        return false;
    }

    /**
     * register function
     * @return json user info
     */
    function register() {
        $response = new stdClass();
        $error = false;
        if (isset($_POST)) {
            // field required
            if ($this->validateRegister()) {
                $error = true;
                $response->code = $this->lang->line('invalid_request');
            }
            if (!$error){
            // check username or email is existed
            $email = isset($_POST['Email'])?trim(strtolower($_POST['Email'])):'';
            $user_name = isset($_POST['UserName'])?trim($_POST['UserName']):'';
            if (!empty($user_name) || !empty($email)){
                $objUser = $this->User_model->checkExistedUserNameOrEmail($user_name, $email);
                if ($objUser){
                    if (strtolower($user_name) == strtolower($objUser->UserName)){
                        $response->code = $this->lang->line('username_existed');
                    }else{
                        $response->code = $this->lang->line('email_existed');
                    }
                    $error = true;
                }
            }
            }

            $photo = isset($_POST['Photo'])?$_POST['Photo']:'';
            $full_photo = isset($_POST['FullPhoto'])?$_POST['FullPhoto']:'';
            
            if (isset($_FILES['Photo']) && $_FILES['Photo']['size'] > 0) {
                $upPhoto = $this->s3upload->upload('Photo');
                if (!$upPhoto) {
                    $response->code = $this->lang->line('invalid_image');
                    $error = true;
                } else {
                    $photo = $upPhoto;
                    $full_photo = '';
                }
            }
            
            // ANOMO-11738 If a new user signs up w/ FB… and they are <18… they are automatically verified. 
            $facebook_id = isset($_POST['FacebookID']) ? $_POST['FacebookID'] : "";
            $birth_date = isset($_POST['BirthDate']) ? $_POST['BirthDate'] : "";
            
            //Temporary set all user is verified
            $is_verify = "1";
            
            if ($facebook_id > 0 ){
                $objFb = $this->User_model->getFbExist($facebook_id, 0);
                if (!$objFb){
                    // ANOMO-11768 ALL users who have a FB account are “automatically” verified
                    $is_verify = "1";
                }else{
                    $error = true;
                    $response->code = $this->lang->line('fb_id_existed');
                }
            }
            
            // ANOMO-12453 - use promo code
            $promo_code = null;
            if ($this->Config_model->getConfig('UsePromoCode')){
                $promo_code = isset($_POST['PromoCode'])?trim($_POST['PromoCode']):'';
                if (empty($promo_code)){
                    // allow skip promo code
                    //$response->code = $this->lang->line('invalid_promo_code');
                }else{
                    $promoCodeObj = $this->Promo_Code_model->validatePromoCode($promo_code);
                    if (!$promoCodeObj){
                        $error = true;
                        $response->code = $this->lang->line('invalid_promo_code');
                    }
                }
            }
            
            $is_email_verify = "0";
            // verify by email code
            $email_code = isset($_POST['EmailCode'])?trim($_POST['EmailCode']):'';
            if (!empty($email_code) && !empty($email)){
                // ANOMO-12783 Allow user to type in "skip" for the verification email address
                if ($email == SKIP && strtolower($email_code) == SKIP){
                    $is_email_verify = "1";
                }else{
                    $emailCodeObj = $this->Promo_Code_model->validateEmailCode($email, $email_code);
                    if (!$emailCodeObj){
                        $error = true;
                        $response->code = $this->lang->line('invalid_email_code');
                    }else{
                        $is_email_verify = "1";
                    }
                }
            }
            
            

            if (!$error) {
                $data = array(
                    'Email' => $email == SKIP? '': $email,
                    'UserName' => $user_name,
                    'Password' => isset($_POST['Password'])?trim($_POST['Password']):"",
                    'Gender' => $_POST['Gender'],
                    'BirthDate' => $birth_date,
                    'FbEmail' => isset($_POST['FbEmail']) ? $_POST['FbEmail'] : "",
                    'FacebookID' => $facebook_id,
                    'Photo' => $photo,
                    'FullPhoto' => $full_photo,
                    'GenderDating' => isset($_POST['GenderDating'])?$_POST['GenderDating']:0,
                    'SignUpDate'    => gmdate('Y-m-d H:i:s'),
                    'IsVerify'      => $is_verify,
                    'NeighborhoodID'    => NBH_EARTH,
                    'PromoCode'     => $promo_code,
                    'IsEmailVerify' => $is_email_verify
                );

                $user_id = $this->User_model->add($data);
                if ($user_id > 0){
                    
                    // ANOMO-12465
                    if ($is_email_verify == 1 && $email != SKIP){
                        $this->Promo_Code_model->updateEmailCode(array('Status' => 1, 'VerifiedDate' => gmdate('Y-m-d H:i:s')), array('Email' => $email, 'Code' => $email_code));
                    }
                    //-- ANOMO-7067
                    $isUserCardOn = $this->Config_model->getConfig('NEW_USER_ACTIVITY_CARD');
                    if ($isUserCardOn && $isUserCardOn == 'on') {
                        $newUserCard = array(
                            'FromUserID'    =>  $user_id,
                            'FromUserName' =>  $user_name,
                            'Avatar' => $photo,
                            'Type'  => ACTIVITY_NEW_USER,
                            'ActionType'    => ACTIVITY_NEW_USER,
                            'Message'   => '',
                            'RefID' => $user_id,
                            'BirthDate' => isset($_POST['BirthDate']) ? $_POST['BirthDate'] : "",
                            'Gender'    => $_POST['Gender'],
                            'NeighborhoodID' => NBH_EARTH,
                            'CreatedDate'   => gmdate('Y-m-d H:i:s')
                        );
                        $this->Activity_model->add($newUserCard);
                    }
                    
                    $aTagIds = array();
                    $sTagIds = '';
                    if (isset($_POST['Tags']) && $_POST['Tags'] != '') {
                        $post_tag = explode(',', $_POST['Tags']);
                        $ids = array();
                        // remove tag duplicate
                        $aTempPostTag = array();
                        foreach ($post_tag as $k => $v) {
                            $v = mb_strtolower(trim($v));
                            $aTempPostTag[$v] = $v;
                        }
                        $tags = array_unique(array_map('trim', $aTempPostTag));
                        $aExistsTag = $this->Tag_model->returnListTagId($tags);
                        $aNewTags = array_diff(array_values($tags), array_values($aExistsTag));
                        if (count($aNewTags) > 0) {
                            $tagId = $this->Tag_model->insertMulti($aNewTags);
                            if ($tagId) {
                                for ($i = 0; $i < count($aNewTags); $i++) {
                                    $ids[] = $tagId + $i;
                                }
                            }
                        }
                        // remove tag duplicate
                        if (sizeof($aExistsTag) > 0) {
                            $aTemp1 = array_unique(array_map('mb_strtolower', $aExistsTag));
                            $aTemp2 = array_intersect_key($aExistsTag, $aTemp1);
                            $aExistsTag = $aTemp2;
                        }

                        $aTagIds = array_merge(array_keys($aExistsTag), $ids);
                        $this->Tag_model->insertUserTag($user_id, $aTagIds);
                    }

                    // intent
                    $sIntent = isset($_POST['IntentIDs'])?$_POST['IntentIDs']:'';
                    if ($sIntent != ''){
                        $aIntent = explode(',', $sIntent);
                        $aIntent = array_unique($aIntent);
                        $this->Intent_model->insertUserIntent($user_id, $aIntent);
                    }
                    
                    // ANOMO-12622 - Select Major
                    $sMajor = isset($_POST['MajorIDs'])?$_POST['MajorIDs']:'';
                    if ($sMajor != ''){
                        $aMajor = explode(',', $sMajor);
                        $aMajor = array_unique($aMajor);
                        $this->Major_model->insertUserMajor($user_id, $aMajor);
                    }

                    // generate token
                    $token = $this->commonlib->gen_uuid();
                    $token_data["Token"] = $token;
                    $token_data["UserID"] = $user_id;
                    $this->Wstoken_model->create($token_data);

                    // return
                    $data['Avatar'] = '';
                    if ($photo) {
                        $data['Avatar'] = $this->config->item('s3_photo_url') . $photo;
                    }
                    if ($full_photo) {
                        $data['FullPhoto'] = $this->config->item('s3_photo_url') . $full_photo;
                    }
                    unset($data['Tags']);
                    unset($data['Password']);
                    unset($data['Photo']);
                    $data['UserID'] = (string)$user_id;
                    $data['Credits'] = 0;

                    // 2014-08-12 - S*** request from android :|
                    $data['OnlyShowCountry'] = "0";
                    $data['AllowSendGiftNotice'] = "1";
                    $data['AllowRevealNotice'] = "1";
                    $data['AllowChatNotice'] = "1";
                    $data['AllowAnomotionNotice'] = "1";
                    $data['AllowCommentActivityNotice'] = "1";
                    $data['AllowLikeActivityNotice'] = "1";
                    $data['AllowFollowNotice'] = "1";

                    $response = (object) $data;
                    $response->token = $token;
                    $response->code = $this->lang->line('ok');
                } else {
                    $response->code = $this->lang->line('db_error');
                }
            }
        } else {
            $response->code = $this->lang->line('invalid_request');
        }
        echo json_encode($response);
    }

    /**
     * register device for pushing notification
     *
     * @param string $token
     * @return json
     */
    function register_device($token = '') {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (isset($_POST['DeviceID']) && trim($_POST['DeviceID']) != '' && isset($_POST['Platform']) && in_array($_POST['Platform'], array('ios', 'android'))) {
                $this->registerDevice($tokenInfo->UserID, trim($_POST['DeviceID']), trim($_POST['Platform']));
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }

    /**
     * Update user information
     *
     * @param string $token
     * @param int $user_id
     * @return json array
     */
    function update($token = '', $is_update_fb = 0) {
        // Hung's temporary fix to db lock issue: set time limit to (20s)
        set_time_limit(20);

        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {            
                $this->User_model->updateLastActivity($user_id);
                $userInfo = $this->User_model->getUserInfo($user_id);
                if (isset($_POST) && $userInfo) {
                    //Topic validation
                    $topic_id = isset($_POST['TopicID'])?$_POST['TopicID']:null;
                    $topic_cate_id = null;
                    $isDeletedTopic = false;                            
                    if($topic_id){
                        $topic = $this->Topic_model->getTopicInfo($topic_id);
                        if(!$topic){
                            $isDeletedTopic = true;    
                        }else{
                            $topic_cate_id = $topic->CateID;
                        }
                    }
                    if($isDeletedTopic){
                        $response->code = $this->lang->line('invalid_request');
                    }else{ 
                    
                    $error = false;
                    if (isset($_POST['UserName']) && $_POST['UserName'] != '' && $is_update_fb == 0) {
                        // check previous change
                        if (gmdate('Y-m-d') < date('Y-m-d', strtotime('+1 month', strtotime($userInfo->UserNameUpdateDate)))) {
                            $error = true;
                            $response->code = $this->lang->line('block_change_user_name');
                            $response->UserNameUpdateDate = date('Y-m-d', strtotime("+ 1 month", strtotime($userInfo->UserNameUpdateDate)));
                        } else {
                            if (!preg_match('/^[a-zA-Z0-9]+$/', $_POST['UserName'])) {
                                $error = true;
                                $response->code = $this->lang->line('invalid_username');
                            }else{
                                //check bad word
                                $isValidUsername = true;
                                $banwords = $this->User_model->getListBanWords();                            
                                if($banwords && count($banwords)>0){
                                    $username = $_POST['UserName'];
                                    foreach ( $banwords as $item ) {
                                        if (stripos(strtolower(trim($username)), strtolower($item->word)) !== FALSE ) {
                                            $isValidUsername = false;
                                            break;
                                        }
                                    }
                                }                                          
                                if($isValidUsername == false){
                                    $error = true;
                                    $response->code = $this->lang->line('invalid_username');
                                }else{
                                    // check existed username
                                    if ($this->User_model->checkExistedUserName($_POST['UserName'], $user_id)) {
                                        $error = true;
                                        $response->code = $this->lang->line('username_existed');
                                    }
                                } 
                            }
                        }
                    }
    
                    if (isset($_POST['FacebookID']) && trim($_POST['FacebookID']) != '') {
                        $fbExistObj = $this->User_model->getFbExist($_POST['FacebookID'], $user_id);
                        if ($fbExistObj) {
                            $error = true;
                            $response->UserName = $fbExistObj->UserName;
                            $response->code = $this->lang->line('fb_id_existed');
                        }else{
                            //ANOMO-11738 If a <17 trial user connects to FB… and their age is still < 17, then they are verified. 
                            // ANOMO-11768 ALL users who have a FB account are “automatically” verified
                            $data['IsVerify'] = 1;
                        }
                    }
    
                    if (isset($_FILES['CoverPicture']) && $_FILES['CoverPicture']['size'] > 0) {
                        $upCoverPhoto = $this->s3upload->upload('CoverPicture');
                        if (!$upCoverPhoto) {
                            $response->code = $this->lang->line('invalid_image');
                            $response->description = $this->s3upload->error;
                            $error = true;
                        } else {
                            $data['CoverPicture'] = $upCoverPhoto;
                        }
                    }
                    if ($error == false) {
    
                        if (isset($data['CoverPicture']) && $userInfo->CoverPicture) {
                            @unlink($this->config->item('s3_photo-url') . $userInfo->CoverPicture);
                        }
    
                        if (isset($_POST['UserName'])) {
                            $data['UserName'] = $_POST['UserName'];
                            if ($is_update_fb == 0){
                                $data['UserNameUpdateDate'] = gmdate('Y-m-d H:i:s');
                            }
                            $data['IsUpdateUserName'] = 1;
                        }
                        if (isset($_POST['BirthDate'])) {
                            $data['BirthDate'] = $_POST['BirthDate'];
                        }
                        // hotfix - 2014-06-20 - for IOS don't allow change gender when upgrade
                        if ($is_update_fb == 0){
                            if (isset($_POST['Gender'])) {
                                $data['Gender'] = $_POST['Gender'];
                                if ($userInfo->Gender && $userInfo->Gender != $data['Gender']) {
                                    if ($data['Gender'] == 1) {
                                        $data['Photo'] = MALE_AVATAR_DEFAULT;
                                        $data['FullPhoto'] = MALE_FULLPHOTO_DEFAULT;
                                    } elseif ($data['Gender'] == 2) {
                                        $data['Photo'] = FEMALE_AVATAR_DEFAULT;
                                        $data['FullPhoto'] = FEMALE_FULLPHOTO_DEFAULT;
                                    }
                                }
                            }
                        }
                        if (isset($_POST['AboutMe'])) {
                            $data['AboutMe'] = $_POST['AboutMe'];
                        }
                        if (isset($_POST['Email']) && $this->commonlib->validateEmail($_POST['Email'])) {                
                            $data['Email'] = $_POST['Email'];
                        }
                        if (isset($_POST['FbEmail'])) {
                            $data['FbEmail'] = $_POST['FbEmail'];
                        }
                        if (isset($_POST['RealName'])) {
                            $data['RealName'] = $_POST['RealName'];
                        }
                        if (isset($_POST['College'])) {
                            $data['College'] = $_POST['College'];
                        }
    
                        if (isset($_POST['FacebookID'])) {
                            $data['FacebookID'] = $_POST['FacebookID'];
                        }
    
                        if (isset($_POST['Password'])) {
                            $data['Password'] = trim($_POST['Password']);
                        }
    
                        if (isset($_POST['Photo'])){
                            $data['Photo'] = $_POST['Photo'];
                        }
    
                        if (isset($_POST['FullPhoto'])){
                            $data['FullPhoto'] = $_POST['FullPhoto'];
                        }
                        
                        if (isset($_FILES['Photo']) && $_FILES['Photo']['size'] > 0) {

                            $upPhoto = $this->s3upload->upload('Photo');
                            if (!$upPhoto) {
                                $error = true;
                            } else {
                                $data['Photo'] = $upPhoto;
                                $data['FullPhoto'] = '';
                            }
                        }
                
                        $isAnonymous = isset($_POST['IsAnonymous'])?$_POST['IsAnonymous']:0;        
                        if (isset($_POST['ProfileStatus']) && trim($_POST['ProfileStatus']) != '') {
                            $lat = isset($_POST['Lat'])?$_POST['Lat']:'';
                            $lon = isset($_POST['Lon'])?$_POST['Lon']:'';
                            $is_vegas = $this->commonlib->isVegas($lat, $lon);
                            $aResponse = $this->User_Profile_Status_model->parseData($_POST['ProfileStatus'], $user_id, $is_vegas, 1, $isAnonymous);
                            $ProfileStatus = $aResponse['content'];
                            if($isAnonymous != 1){
                                $aVideo = isset($aResponse['video'])?$aResponse['video']:false;
                                $data['ProfileStatus'] = $ProfileStatus;
                            }  
                        }
                        //ANOMO-10188
                        if (isset($_POST['GenderDating'])){
                            $data['GenderDating'] = $_POST['GenderDating'];
                        }
                        if (isset($data) || isset($ProfileStatus)) {                          
                           
                            if(isset($data)){
                                // ANOMO-11355  Create ability to track username history
                                if (isset($data['UserName']) && $data['UserName'] != ''){
                                    $this->User_model->trackUserName(array(
                                                'UserID' => $user_id
                                                , 'UserName' => $userInfo->UserName
                                                , 'CreatedDate' => gmdate('Y-m-d H:i:s'))
                                            );
                                }
                                
                                $updated = $this->User_model->updateUser($data, array('UserID' => $user_id), $token);    
                            }                                           
                            // insert into user profile status table
                            if (isset($ProfileStatus)) {                            
                                $ups_id = false;
                                // ANOMO-11390 - if have video
                                if (isset($aVideo) && $aVideo != false){
                                    $video_id = isset($aVideo['VideoID'])?$aVideo['VideoID']:false;
                                    $video_source = isset($aVideo['VideoSource'])?$aVideo['VideoSource']:false;
                                    $video_thumbnail = isset($aVideo['VideoThumbnail'])?$aVideo['VideoThumbnail']:null;
                                    if ($video_id && $video_source){
                                        $ups_data = array(
                                            'UserID' => $user_id,
                                            'Content' => $ProfileStatus,
                                            'VideoID'   => $video_id,
                                            'VideoSource'   => $video_source,
                                            'VideoThumbnail'    => $video_thumbnail,
                                            'CreatedDate' => gmdate('Y-m-d H:i:s')
                                        );
                                        $ups_id = $this->Video_model->add($ups_data);
                                    }
                                }else{
                                    $ups_data = array(
                                        'UserID' => $user_id,
                                        'Content' => $ProfileStatus,
                                        'CreatedDate' => gmdate('Y-m-d H:i:s'),
                                        'IsAnonymous' => $isAnonymous
                                    );
                                    $ups_id = $this->User_Profile_Status_model->add($ups_data);
                                }
                                
                                if ($ups_id) {                                    
                                    $fanpage = isset($_POST['FanPage'])?$_POST['FanPage']:null;
                                    //ANOMO-11510 - When a Celebrity makes a post it will show on both Recent and Fan tabs
                                    //ANOMO-11717 - user HasFanPage
                                    if(empty($fanpage) && ($userInfo->IsAgeLess == 1 || $userInfo->IsAdmin ==1 || $userInfo->HasFanPage == 1) && $isAnonymous !=1){
                                        $fanpage = $user_id;
                                    }
                                    // don't allow Anon post on fanpage
                                    if ($isAnonymous == 1){
                                        $fanpage = null;
                                    }
                                    
                                    
                                    
                                    $activity_type = isset($video_id)?ACTIVITY_POST_VIDEO:ACTIVITY_POST_STATUS;
                                    $feed = array(
                                        'FromUserID' => $user_id,
                                        'FromUserName' => $userInfo->UserName,
                                        'Avatar' => $userInfo->Avatar,
                                        'Type' => $activity_type,
                                        'ActionType' => $activity_type,
                                        'Message' => $ProfileStatus,
                                        'RefID' => $ups_id,
                                        'Lat' => $userInfo->Lat,
                                        'Lon' => $userInfo->Lon,
                                        'BirthDate' => $userInfo->BirthDate,
                                        'Gender' => $userInfo->Gender,
                                        'CreatedDate' => gmdate('Y-m-d H:i:s'),
                                        'NeighborhoodID' => $userInfo->NeighborhoodID,
                                        'IsAnonymous' => $isAnonymous,
                                        'FanPage' => $fanpage,
                                        'VideoID'   => isset($video_id)?$video_id:null,
                                        'VideoSource'   => isset($video_source)?$video_source:null,
                                        'VideoThumbnail'    => isset($video_thumbnail)?$video_thumbnail:null,
                                        'TopicID' => $topic_id
                                    );
                                    $feed_id = $this->Activity_model->add($feed);
                                    // end feed
                                    if ($feed_id) {
                                        // update Topic LastModified
                                        if($topic_id){
                                            $this->Topic_model->increasePost($topic_id);
                                        }                                    
                                        // ANOMO-11749  Notification to ALL FOLLOWERS of the ageless user
                                        if ($userInfo->IsAgeLess == 1 && $isAnonymous != 1){
                                             $this->Cron_Mention_User_model->addNotifyFollowerToQueue(array('UserID' => $user_id,
                                                                                                        'ContentID' => $ups_id,
                                                                                                        'ContentType'   => $activity_type,
                                                                                                        'CreatedDate'   => gmdate('Y-m-d H:i:s')  ));
                                        }
    
                                         // save history & push notify to @mentionUser
                                         $this->Cron_Mention_User_model->saveHistoryAndPushNotify($ProfileStatus, $user_id, $ups_id, $activity_type, $isAnonymous);
    
                                        // ANOMO-7852 add hashtag
                                        $text = $ProfileStatus;
                                        $this->Hashtag_model->add($text, $feed_id, $topic_cate_id, $topic_id);
                                        
                                        
                                        // ANOMO-11242 - Fan page notifications
                                        // add to push notification historys
                                        if(!empty($fanpage) && $user_id != $fanpage){
                                            $pushData = array(
                                                'UserID' => $fanpage,
                                                'SendUserID' => $user_id,
                                                'Type' => HISTORY_FANPAGE_POST,  //18                                          
                                                'ContentType' => $activity_type,
                                                'ContentID' => $ups_id,
                                                'IsAnonymous'   => '',
                                                'IsRead' => 1,
                                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                                            );
                                            $history_id = $this->Notification_History_model->add($pushData);
                                            $targetUserInfo = $this->User_model->getUserInfo($fanpage);
                                            if ($history_id && $targetUserInfo->AllowFollowNotice == 1) {
                                                // push notification to target user
                                                $msg_push = str_replace('%name%', $userInfo->UserName, $this->lang->line('post_on_fanpage'));
                                                $this->mypushnotify->send($fanpage, $user_id, NOTIFY_FANPAGE_POST, $msg_push, 
                                                    array(
                                                        'HistoryID' => $history_id,
                                                        'ContentID' => $ups_id,
                                                        'ContentType' => $activity_type
                                                    )
                                                );
                                            }
                                        }
                                        //End push notification
                                    }
                                }
                            }
                        }
                        // add tag
                        if (isset($_POST['Tags']) && !empty($_POST['Tags'])) {
                            $aTag = array();
                            $post_tag = explode(',', $_POST['Tags']);
                            foreach ($post_tag as $key => $value) {
                                $value = mb_strtolower(trim($value));
                                $aTag[$value] = $value;
                            }
    
                            $this->User_model->addTags($user_id, array_unique(array_map('trim', $aTag)));
                        }
    
                        // update intents
                        $sIntent = isset($_POST['IntentIDs'])?$_POST['IntentIDs']:'';
                        if ($sIntent != ''){
                            $aIntent = explode(',', $sIntent);
                            $this->Intent_model->addMulti($user_id, array_unique($aIntent));
                        }
                        
                        // ANOMO-12622 - update major
                        $sMajor = isset($_POST['MajorIDs'])?$_POST['MajorIDs']:'';
                        if ($sMajor != ''){
                            $aMajor = explode(',', $sMajor);
                            $this->Major_model->addMulti($user_id, array_unique($aMajor));
                        }
    
    
    
                        // return user info
                        $info = $this->User_model->getUserDetail($user_id);
                        $response = $info;
                        $response->Tags = $this->User_model->getListTagOf($info->UserID);
                        $response->ListIntent = $this->Intent_model->getListIntentOfUser($info->UserID);
                        $response->code = $this->lang->line('ok');
                    }
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
            
        }
        echo json_encode($response);
    }

    /**
     * validate privacy
     *
     * @return bool
     */
    private function validateUpdatePrivacy() {
        $error = false;
        $value = array(0, 1);

        $settings = array(
            'AllowSendGiftNotice',
            'AllowRevealNotice',
            'AllowChatNotice',
            'AllowAnomotionNotice',
            'AllowCommentActivityNotice',
            'AllowLikeActivityNotice',
            'AllowFollowNotice',
            'OnlyShowCountry'
        );

        foreach ($_POST as $key => $val) {
            if (!in_array($key, $settings)) {
                $error = true;
            }
            if (!in_array($val, $value)) {
                $error = true;
            }
        }
        return $error;
    }

    /**
     * update privacy
     *
     * @param string $token
     * @param int $user_id
     * @return json
     */
    function update_privacy($token = '') {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $userInfo = $this->User_model->getUserInfo($user_id);
            if (sizeof($_POST) > 0 && $userInfo && !$this->validateUpdatePrivacy()) {
                $ret = $this->User_model->updateUser($_POST, array('UserID' => $user_id));
                if (!$ret){
                    $response->code = $this->lang->line('failed');
                }else{
                    // return user's location if they update OnlyShowCountry setting.
                    if (isset($_POST['OnlyShowCountry'])){
                        $nbhName = "";
                        $nbhId = "";
                        $nbhObj = $this->User_model->getNeighborhoodInfoOfUser($user_id);
                        if($nbhObj){
                            $nbhName = $nbhObj->NAME;
                            $nbhId = $nbhObj->NeighborhoodID;
                        }
                        $response->NeighborhoodID = $nbhId;
                        $response->NeighborhoodName = $nbhName;
                    }
                    $response->code = $this->lang->line('ok');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }

    /**
     * forgot password function
     *    - send a email with the reset password link to user
     *
     * @param string $email
     * @return json
     */
    function forgot_password() {
        $response = new stdClass();
        $email = isset($_POST['Email']) ? $_POST['Email'] : '';
        if ($email != '') {
            $objUser = $this->User_model->getUserInfoByEmail($email);
            if ($objUser) {
                if (!empty($objUser->FacebookID) && empty($objUser->Password)){
                    $response->code = $this->lang->line('fb_account');
                }else{
                    // generate the link to reset password
                    $pattern = $objUser->UserID . '_' . $objUser->Email;
                    $key = $this->commonlib->encode($pattern);
//                    $link = base_url() . 'reset_password/index/' .$key;
                    $link = $this->config->item('reset_password_url').'?key='.$key;
                    $url = "<a href='" . $link . "'>Reset now</a>";
                    $this->email->from($this->config->item('email_from'), $this->config->item('email_from_name'));
                    $this->email->to($email);
                    $this->email->subject($this->lang->line('forgotten_password_subject'));
                    $msg = str_replace('%name%', $objUser->UserName, $this->lang->line('forgotten_password_title'));
                    $msg .= str_replace('%link%', $url, $this->lang->line('forgotten_password_body'));
                    $msg .= $this->lang->line('forgotten_password_footer');
                    $this->email->message($msg);
                    if (@$this->email->send()) {
                        $response->code = $this->lang->line('ok');
                    } else {
                        $response->code = $this->lang->line('failed');
                    }
                }
            } else {
                $response->code = $this->lang->line('invalid_email');
            }
        } else {
            $response->code = $this->lang->line('invalid_request');
        }
        echo json_encode($response);
    }

    /**Follow / UnFollow user
     *
     * @param type $token
     * @param type $target_user_id
     */
    function follow($token = '', $target_user_id = 0) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $current_user_id = $tokenInfo->UserID;
            $targetUserInfo = $this->User_model->getUserInfo($target_user_id);
            if ($current_user_id == $target_user_id || !$targetUserInfo) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $connectObj = $this->User_model->getConnectMeter($current_user_id, $target_user_id, 'object');
                if ($connectObj && $connectObj->IsFavorite == 1) { // incase unfollow
                    $ret = $this->User_model->removeFavorite($current_user_id, $target_user_id);
                    if (!$ret) {
                        $response->code = $this->lang->line('failed');
                    } else {
                        $response->code = $this->lang->line('ok');

                        // decrease #Follower, #Following
                        $this->User_model->updateNumberFollow($current_user_id, 'NumberOfFollowing', 'sub');
                        $this->User_model->updateNumberFollow($target_user_id, 'NumberOfFollower', 'sub');

                        // ANOMO-10327 - influence point
                        $aPoint = array(
                            'UserID' => $target_user_id,
                            'FromUserID' => $current_user_id,
                            'Type' => INF_POINT_FOLLOW, // 2
                            'Point' => 10,
                            'CreatedDate' => gmdate('Y-m-d H:i:s')
                        );
                        $this->Influence_Point_model->minusPoint($aPoint);                        
                        
                        //ANOMO-11422 Celebrity campaign - unfollow                           
                        if($targetUserInfo->IsAgeLess == 1){                       
                            $CAU = $this->Campaign_CAU_model->getCampaignCAUByUser($current_user_id);
                            if($CAU){
                                // if CAU e
                                if($CAU->CelebrityID != -1){     
                                    $data = array(                                                            
                                        'CelebrityID' => -1
                                    );  
                                    $cond = array(
                                        'UserID' => $current_user_id
                                    );
                                    $this->Campaign_CAU_model->update($data,$cond);
                                }
                            }
                        }
                    }
                } else { // incase follow
                    $block_status = $this->User_model->returnBlockStatus($current_user_id, $target_user_id);
                    if ($block_status != '') {
                        $response->code = $block_status;
                    } else {
                        if (!$connectObj) {
                            // set connect meter
                            $ret = $this->User_model->follow($current_user_id, $target_user_id);
                        }else{
                            // add favorite 
                            $ret = $this->User_model->addFavorite($current_user_id, $target_user_id);
                        }
                        if (!$ret) {
                            $response->code = $this->lang->line('failed');
                        } else {
                            $response->code = $this->lang->line('ok');

                            // ANOMO-10327 - influence point
                            $aPoint = array(
                                'UserID' => $target_user_id,
                                'FromUserID'    => $current_user_id,
                                'Type'   => INF_POINT_FOLLOW, // 2
                                'Point'     => 10,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                            );
                            $this->Influence_Point_model->add($aPoint);

                            // ANOMO-5840 - push notify
                            // add to push notification historys
                            $pushData = array(
                                'UserID' => $target_user_id,
                                'SendUserID' => $current_user_id,
                                'Type' => HISTORY_FOLLOW, // 15 - follow
                                'Total' => 1,
                                'IsRead' => 1,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                            );
                            $history_id = $this->Notification_History_model->add($pushData);
                            if ($history_id && $targetUserInfo->AllowFollowNotice == 1) {
                                // push notification to target user
                                $msg_push = str_replace('%name%', $tokenInfo->UserName, $this->lang->line('push_notify_follow'));
                                $this->mypushnotify->send($target_user_id, $current_user_id, NOTIFY_FOLLOW, $msg_push, array('HistoryID' => $history_id));
                            }
                            // increase #Follower, #Following
                            $this->User_model->updateNumberFollow($current_user_id, 'NumberOfFollowing', 'add');
                            $this->User_model->updateNumberFollow($target_user_id, 'NumberOfFollower', 'add');
                            
                            //ANOMO-11422 Celebrity campaign                            
                            if($targetUserInfo->IsAgeLess == 1){                                
                                $campaign = $this->Campaign_model->getActiveCampaignByCelebrity($target_user_id,$tokenInfo->SignUpDate);                                
                                if($campaign){
                                    $CAU = $this->Campaign_CAU_model->getCampaignCAUByUser($current_user_id);
                                    if($CAU){
                                        // if CAU exist - update if the same Campaign and has been unfollowed before
                                        if($campaign->CampaignID == $CAU->CampaignID && $CAU->CelebrityID == -1){    
                                            $data = array(
                                                'UserID' => $current_user_id,                                        
                                                'CelebrityID' => $target_user_id
                                            );  
                                            $cond = array(
                                                'UserID' => $current_user_id
                                            );
                                            $this->Campaign_CAU_model->update($data,$cond);
                                        }
                                    }else{
                                        //if not existed - insert into CAU table
                                        $data = array(
                                            'UserID' => $current_user_id,
                                            'CampaignID' => $campaign->CampaignID,
                                            'CelebrityID' => $target_user_id,
                                            'CreatedDate' => gmdate('Y-m-d H:i:s')
                                        );      
                                        $this->Campaign_CAU_model->add($data);                      
                                    }
                                    //track daily active
                                    $CAU_daily = $this->Campaign_CAU_Daily_model->getCurrentDateInfo($tokenInfo->UserID);    
                                    if(!$CAU_daily){                        
                                        $data = array(
                                            'UserID' => $tokenInfo->UserID,
                                            'CreatedDate' => gmdate('Y-m-d') 
                                        );
                                        $this->Campaign_CAU_Daily_model->add($data);
                                    }  
                                }
                            }
                        }
                    }
                }
            }
        }
        echo json_encode($response);
    }


    /**
     * remove contact
     *
     * @param string $token
     * @param int $current_user_id
     * @param int $target_user_id
     * @return json
     */
    function remove_contact($token = '', $current_user_id = 0, $target_user_id = 0) {
        $response = new stdClass();
        $current_user_id = $this->commonlib->checkValidToken($token);
        if ($current_user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $targetUserInfo = $this->User_model->getUserInfo($target_user_id);
            $connectMeter = $this->User_model->getConnectMeter($current_user_id, $target_user_id, 'object');
            if ($targetUserInfo && is_numeric($target_user_id) && $connectMeter) {
                // update db
                $this->User_model->removeContact($current_user_id, $target_user_id);
                $response->code = $this->lang->line('ok');

                // update last activity
                // $this->User_model->updateLastActivity($current_user_id);
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * logout function
     *
     * @param string $token
     * @param int $user_id
     * @return json
     */
    function logout($token = '', $user_id = 0) {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $userInfo = $this->User_model->getUserInfo($user_id);
            if ($userInfo && is_numeric($user_id)) {
                $this->User_model->updateLastActivity($user_id);
                $this->User_model->logout($user_id, $token);
                $response->code = $this->lang->line('ok');

                // disable push notification
                $this->Devices_model->update(array('Status' => 'inactive'), array('UserID' => $user_id));
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * get user detail
     *
     * @param string $token
     * @param int $current_user_id
     * @param int $target_user_id
     * @return json array
     */
    function get_user_info($token = '', $target_user_id = '') {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!is_numeric($target_user_id)) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $targetUserInfo = $this->User_model->getUserDetail($target_user_id);
                if (!$targetUserInfo) {
                    $response->code = $this->lang->line('invalid_parameter');
                } else {
                    if ($targetUserInfo->IsBanned == 1 || $targetUserInfo->Deactive == 1){
                        $response->code = $this->lang->line('banned');
                    }else{

                    $user_id = $tokenInfo->UserID;
                    $block_status = '';
                    if ($user_id != $target_user_id){
                        $block_status = $this->User_model->returnBlockStatus($user_id, $target_user_id);
                    }
                    //ANOMO-11482
                    //Revert valiation if BLOCK_TARGET
                    if ($block_status != '') {
                        $response->code = $block_status;                    
                    //ANOMO-10486
                    //if($block_status == 'BLOCK'){
                    //$response->code = $block_status;
                    } else {
                        $is_block = 0;
                        $is_block_target = 0;
                        if($block_status == 'BLOCK'){
                            $is_block = 1;
                        }else if($block_status == 'BLOCK_TARGET'){
                            $is_block_target = 1;
                        }
                        $targetUserInfo->IsBlock = $is_block;
                        $targetUserInfo->IsBlockTarget = $is_block_target;
                        
                        $is_favorite = 0;
                        $is_reported = 0;
                        if ($user_id != $target_user_id){
                            $statusObj = $this->User_model->getStatusBtw2User($user_id, $target_user_id);
                            if ($statusObj){
                                $is_favorite = (int)$statusObj->IsFavorite;
                                $is_reported = (int)$statusObj->IsReported;
                            }
                        }
                        $targetUserInfo->IsFavorite = $is_favorite;
                        $targetUserInfo->IsReported = $is_reported;
                        // tag list
                        $targetUserInfo->Tags = $this->User_model->getListTagOf($target_user_id);
                        $targetUserInfo->ListIntent = $this->Intent_model->getListIntentOfUser($target_user_id);
                        $targetUserInfo->ListMajor = $this->Major_model->getListMajorOfUser($target_user_id);
                        
                        unset($targetUserInfo->IsBanned);
                        unset($targetUserInfo->Deactive);
                        $results = $targetUserInfo;
                        $response->results = $results;
                        $response->code = $this->lang->line('ok');
                    }
                }
                }
            }
        }
        echo json_encode($response);
    }

    /**
     * block user
     *
     * @param string $token
     * @param int $target_user_id
     * @return json
     */
    function block_user($token = '', $target_user_id = '') {
        $response = new stdClass();
        $current_user_id = $this->commonlib->checkValidToken($token);
        if ($current_user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $targetUserInfo = $this->User_model->getUserInfo($target_user_id);
            if ($current_user_id == $target_user_id || !$targetUserInfo){
                $response->code = $this->lang->line('invalid_request');
            }else{
                $blockObj = $this->User_model->checkBlockMsg($current_user_id, $target_user_id);
                if (!$blockObj){ // incase block user
                    $ret = $this->User_model->blockUser($current_user_id, $target_user_id);
                    if (!$ret) {
                        $response->code = $this->lang->line('failed');
                    } else {
                        // ANOMO-10294 - if a user has been blocked >= 2 times within 24 hours, 
                        // notify admin by sending email to a list of admin email addresses 
                        $numberBlocked = $this->User_model->numberBlock1Day($target_user_id);
                        if ($numberBlocked >= 2){
                            // insert to queue and will send later
                            $this->Blocked_User_1day_model->add($target_user_id);
                        }
                        $response->code = $this->lang->line('ok');
                    }
                }else{ // incase un-block user
                    $this->User_model->unBlockMsg($current_user_id, $target_user_id);
                    $response->code = $this->lang->line('ok');
                }
            }
        }
        echo json_encode($response);
    }


    /**
     * ws check app version
     *
     * @param string $token
     * @param string $app_version
     * @param string $platform
     * @return json
     */
    function check_app_version($app_version = '', $platform = '') {
        $response = new stdClass();
        $version = $this->User_model->checkVersion($app_version, $platform);
        if ($version) {
            if ($version->IsLatest == 1 && $version->IsSupport == 1) {
                $response->code = $this->lang->line('ok');
            } elseif ($version->IsSupport == 1 && $version->IsLatest == 0) {
                $response->code = $this->lang->line('support');
            } elseif ($version->IsSupport == 0 && $version->IsLatest == 0) {
                $response->code = $this->lang->line('unsupport');
            }
        } else {
            $response->code = $this->lang->line('invalid_request');
        }

        echo json_encode($response);
    }

    /**
     * send gift function
     *
     * @param string $token
     * @param int $current_user_id
     * @param int $target_user_id
     * @param int $gift_id
     * @return json
     */
    function send_gift($token = '', $target_user_id = 0, $gift_id = 0) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $current_user_id = $tokenInfo->UserID;
            $currentUserInfo = $this->User_model->getUserInfo($current_user_id);
            $targetUserInfo = $this->User_model->getUserInfo($target_user_id);
            $giftInfo = $this->Gift_Send_model->getGiftInfo($gift_id);
            if ($currentUserInfo && $targetUserInfo && $giftInfo && is_numeric($target_user_id) && is_numeric($gift_id)) {
                if ($this->User_model->checkBlock($current_user_id, $target_user_id)) {
                    $response->code = $this->lang->line('target_user_blocked');
                } elseif ($this->User_model->checkBlock($target_user_id, $current_user_id)) {
                    $response->code = $this->lang->line('blocked');
                } elseif ($currentUserInfo->Credits >= $giftInfo->Credits) {
                    $data = array(
                        'SendUserID' => $current_user_id,
                        'ReceiveUserID' => $target_user_id,
                        'GiftID' => $gift_id,
                        'CreatedDate' => gmdate('Y-m-d H:i:s')
                    );
                    $sent = $this->Gift_Send_model->sendGift($data);
                    if ($sent) {

                        // publishing a feed
                        if ($this->Config_model->getConfig('ACTIVITY_FEED') == 'on') {
                            $feed = array(
                                'FromUserID' => $current_user_id,
                                'FromUserName' => $currentUserInfo->UserName,
                                'Avatar' => $currentUserInfo->Avatar,
                                'Type' => 9,
                                'ActionType' => 9,
                                'Message' => $giftInfo->Name,
                                'Image' => $giftInfo->Photo,
                                'RefID' => $sent,
                                'Lat' => $currentUserInfo->Lat,
                                'Lon' => $currentUserInfo->Lon,
                                'BirthDate' => $currentUserInfo->BirthDate,
                                'Gender' => $currentUserInfo->Gender,
                                'CreatedDate' => gmdate('Y-m-d H:i:s'),
                                'GiftReceiverID' => $target_user_id,
                                'GiftReceiverName' => $targetUserInfo->UserName
                            );
                            $this->Activity_model->add($feed);
                        }
                        // end publishing a feed

                        // update credits
                        $currentCredit = $currentUserInfo->Credits - $giftInfo->Credits;
                        $this->User_model->updateUser(array('Credits' => $currentCredit), array('UserID' => $current_user_id));

                        //add push
                        // link to activity stream
                        $pushData = array(
                            'UserID' => $target_user_id,
                            'SendUserID' => $current_user_id,
                            'RefID' => $sent,
                            'ContentType' => 9,
                            'Type' => 1,
                            'Total' => 1,
                            'IsRead' => 1,
                            'CreatedDate' => gmdate('Y-m-d H:i:s')
                        );
                        $history_id = $this->Notification_History_model->add($pushData);

                        if ($history_id && $targetUserInfo->AllowSendGiftNotice == 1) {
                            $msg_push = str_replace('%name%', $tokenInfo->UserName, $this->lang->line('msg_send_gift'));
                            $this->mypushnotify->send($target_user_id, $current_user_id, 2, $msg_push, array('HistoryID' => $history_id, 'ContentID' => $sent, 'ContentType' => 9));
                        }

                        $response->Credits = $currentCredit;
                        $response->code = $this->lang->line('ok');
                    } else {
                        $response->code = $this->lang->line('failed');
                    }
                } else {
                    $response->Credits = (float) $currentUserInfo->Credits;
                    $response->code = $this->lang->line('not_enough_credits');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }

    /**
     * purchase credit function
     *
     * @param string $token
     * @param int $user_id
     * @param int $credit_package_id
     * @return json
     */
    function purchase_credit($token = '') {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {

            $user_id = $tokenInfo->UserID;
            $userInfo = $this->User_model->getUserInfo($user_id);

            // update last activity
            // $this->User_model->updateLastActivity($user_id);
            $error = false;
            if (isset($_POST['Platform']) && $_POST['Platform'] == 'android' && !isset($_POST['Signature'])) {
                $error = true;
            }
            if (!isset($_POST['CreditPackageID']) || !isset($_POST['Platform']) || !isset($_POST['Receipt']) || $error) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $credit_package_id = trim($_POST['CreditPackageID']);
                $platform = trim($_POST['Platform']);
                $receipt = trim($_POST['Receipt']);
                $signature = isset($_POST['Signature']) ? trim($_POST['Signature']) : "";
                $creditInfo = $this->Credit_model->getCreditPackageInfo($credit_package_id);
//                $checkReceipt = $this->validateReceipt($receipt, $platform, $signature);
                if ($userInfo && $creditInfo && is_numeric($user_id) && is_numeric($credit_package_id) && $receipt != '' && in_array($platform, array('ios', 'android'))) {
                    $receiptIsUse = $this->Credit_model->checkReceipt($receipt, $platform);
                    if (!$receiptIsUse) {
                        // check receipt
                        $checkReceipt = $this->validateReceipt($receipt, $platform, $signature);
                        if ($checkReceipt) {
                            // increase point for user
                            $newCredits = (int) $userInfo->Credits + (int) $creditInfo->Credits;
                            $this->User_model->updateUser(array('Credits' => $newCredits), array('UserID' => $user_id));
                            // add credit transaction
                            $data = array(
                                'UserID' => $user_id,
                                'CreditPackageID' => $credit_package_id,
                                'Receipt' => $receipt,
                                'Signature' => $signature,
                                'Platform' => $platform,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                            );
                            $id = $this->Credit_model->addCreditTransaction($data);
                            if ($id) {
                                $response->UserCredits = $newCredits;
                                $response->code = $this->lang->line('ok');
                            } else {
                                $response->code = $this->lang->line('failed');
                            }
                        } else {
                            $response->code = $this->lang->line('invalid_receipt');
                        }
                    } else {
                        $response->code = $this->lang->line('receipt_existed');
                    }
                } else {
                    $response->code = $this->lang->line('invalid_request');
                }
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * get all stuff of user
     *
     * @param string $token
     * @param int $user_id
     * @return json
     */
    function get_all_stuff($token = '', $user_id = 0) {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $userInfo = $this->User_model->getUserInfo($user_id);
            if ($userInfo && is_numeric($user_id)) {
                $response->MyStuff = $this->Gift_Send_model->getMyReceivedGift($user_id);
                $response->GiftHistory = $this->Gift_Send_model->getMyGiftHistory($user_id);
                $response->ListCredit = $this->Credit_model->getListCredit();
                $response->ListGift = $this->Gift_Send_model->getListGift();
                $response->CurrentPoint = $userInfo->Credits;
                $response->code = $this->lang->line('ok');

                // update last activity
                // $this->User_model->updateLastActivity($user_id);
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * get stuffs of user
     *
     * @param string $token
     * @return json
     */
    function get_stuffs($token = '') {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            // get user id from token

            $user_id = $tokenInfo->UserID;
            $userInfo = $this->User_model->getUserInfo($user_id);
            if ($userInfo) {
                $response->ListStuff = $this->Gift_Send_model->getMyReceivedGift($user_id);
                $this->Push_model->update(array('Gift' => 0), array('UserID' => $user_id));
                $response->code = $this->lang->line('ok');
                // update last activity
                // $this->User_model->updateLastActivity($user_id);
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * get all system gift
     *
     * @param string $token
     * @return json
     */
    function get_all_gift($token = '') {
        $response = new stdClass();
        if (!$this->commonlib->checkToken($token)) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $response->Gifts = $this->Gift_Send_model->getAllGiftDividedByCategory();
            $response->code = $this->lang->line('ok');
        }
        echo json_encode($response);
        return json_encode($response);
    }


    /**
     * get stuff history of user
     *
     * @param string $token
     * @return json
     */
    function get_stuff_history($token = '') {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {

            $userInfo = $this->User_model->getUserInfo($tokenInfo->UserID);
            if ($userInfo) {
                $response->StuffHistory = $this->Gift_Send_model->getMyGiftHistory($tokenInfo->UserID);
                $response->code = $this->lang->line('ok');
                // update last activity
                // $this->User_model->updateLastActivity($tokenInfo->UserID);
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * get all package
     *
     * @param string $token
     * @return json
     */
    function get_all_package($token = '') {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $userInfo = $this->User_model->getUserInfo($tokenInfo->UserID);
            if ($userInfo) {
                $response->UserCredits = $userInfo->Credits;
                $response->ListPackage = $this->Credit_model->getListCredit();
                $response->code = $this->lang->line('ok');
                // update last activity
                // $this->User_model->updateLastActivity($tokenInfo->UserID);
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }


    /**
     * return #badge and some config for Popup
     *
     * @param string $token
     * @return json
     */
    function get_total_push($token = '') {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $response->Badge = $this->Notification_History_model->getBadge($tokenInfo);;
            $response->TimePopupShareFB = $this->Config_model->getConfig('TIME_POPUP_SHARE_FB');
            $response->TimePopupConvertAccount = $this->Config_model->getConfig('TIME_POPUP_CONVERT_ACCOUNT');
            $response->code = $this->lang->line('ok');
        }
        echo json_encode($response);
    }


    /**
     * update user location
     *
     * @param string $token
     * @param int $user_id
     * @param float $lat
     * @param float $lon
     * @return json
     */
    function update_user_location($token = '', $lat = '', $lon = '') {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $userInfo = $this->User_model->getUserInfo($tokenInfo->UserID);
            if ((is_numeric($lat) && is_numeric($lon)) || ($lat == 'null' && $lon = 'null')) {
                // Hung's hack to force neighborhood earth update:
                if ($lat == 0 && $lon == 0) {
                    $lat = 'null';
                    $lon = 'null';
                }

                // POST method
                $country = isset($_POST['Country']) ? trim($_POST['Country']) : '';
                $city = isset($_POST['City']) ? trim($_POST['City']) : '';
                $state = isset($_POST['State']) ? trim($_POST['State']) : '';
                $error = 0;

                // ANOMO-8070 - if we cant get location will show Earth
                if ($lat == 'null' && $lon == 'null') {
                    $country = 'Earth';
                    $city = '';
                    $state = '';
                }

                // client doesn't send Country, City, State
                // then get info from google
                if ($lat != 'null' && $lon != 'null' && empty($country) && empty($city) && empty($state)) {
                    $geoInfo = $this->Neighborhood_model->getCountryName($lat, $lon);
                    if ($geoInfo) {
                        $country = $geoInfo['country'];
                        $city = $geoInfo['city'];
                        $state = $geoInfo['state'];
                    } else {
                        // can not get location
                        // return fail
                        $error = 1;
                    }
                }

                if (!$error) {
                    $data = array(
                        'UserID' => $userInfo->UserID,
                        'Lat' => $lat,
                        'Lon' => $lon,
                        'CountryName' => $country,
                        'City' => $city,
                        'State' => $state
                    );

                    $result = $this->Neighborhood_model->sp_location($data, $userInfo->IsAgeLess);
                    // do our automated stuffs here...
                    $this->promotionlib->requestAutoPromoMessage($lat, $lon, $userInfo);
                    if (sizeof($result) > 0) {
                        if (isset($result['Return']) && $result['Return'] > 0) {
                            // return location of user
                            $nbh_id = '';
                            $nbh_name = '';
                            $nbhObj = $this->User_model->getNeighborhoodInfoOfUser($userInfo->UserID);
                            if ($nbhObj){
                                $nbh_id = $nbhObj->NeighborhoodID;
                                $nbh_name = $nbhObj->NAME;
                            }
                            $response->NeighborhoodID = $nbh_id;
                            $response->NeighborhoodName = $nbh_name;
                            $response->code = $this->lang->line('ok');
                        } else {
                            $response->code = $this->lang->line('failed');
                        }
                    } else {
                        $response->code = $this->lang->line('invalid_request');
                    }
                } else {
                    $response->code = $this->lang->line('invalid_request');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }


    public function save_image($url = '') {
        $return = '';
        if ($url != '') {
            $content = @file_get_contents($url);
            if ($content) {
                $file_name = md5(uniqid(mt_rand())) . '.jpg';
                $file_location = FCPATH . $this->config->item('photo_url') . $file_name;
                $re = @file_put_contents($file_location, $content);
                if ($re) {
                    return $this->s3upload->_uploadS3($file_location, $file_name, 'image/jpeg');
                }
            }
        }
        return $return;
    }

    /**delete recent post on profile
     *
     * @param type $token
     * @param type $id - activity id
     */
    function delete_activity($token = '', $id = 0) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $activityInfo = $this->Activity_model->getTopicInfoByActivityID($id);
            $is_valid = 0;
            if ($activityInfo){
                if ($activityInfo->IsInvalid == 0 && 
                        ($activityInfo->FromUserID == $tokenInfo->UserID
                        || $activityInfo->FanPage == $tokenInfo->UserID
                        || $activityInfo->ModID == $tokenInfo->UserID))
                   {
                        $is_valid = 1;
                   }
            }
            
            if ($is_valid) {
                // just deactive this post
                $ret = $this->Activity_model->deleteActivity($activityInfo->RefID, $activityInfo->Type, $tokenInfo->UserID);
                if (!$ret) {
                    $response->code = $this->lang->line('failed');
                } else {
                    //Decrease Topic's total post
                    if($activityInfo && $activityInfo->TopicID){
                        $this->Topic_model->decreasePost($activityInfo->TopicID);
                    }
                    $response->code = $this->lang->line('ok');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }


    function get_blocked_users($token = '') {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $blockUsers = $this->User_model->getBlockUserOf($tokenInfo->UserID);
            foreach ($blockUsers as $user) {
                if ($user->Avatar) {
                    $user->Avatar = $this->config->item('s3_photo_url') . $user->Avatar;
                }
                if ($user->RealPhoto) {
                    $user->RealPhoto = $this->config->item('s3_photo_url') . $user->RealPhoto;
                }
            }
            $response->results = $blockUsers;
            $response->code = $this->lang->line('ok');
        }
        echo json_encode($response);
    }

    /** post picture status
     *
     * @param type $token
     * @param type $user_id
     * @return type
     */

    function post_picture_activity($token = '') {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            //Topic validation
            $topic_id = isset($_POST['TopicID'])?$_POST['TopicID']:null;
            $isDeletedTopic = false;    
            $topic_cate_id = null;
            if($topic_id){
                $topic = $this->Topic_model->getTopicInfo($topic_id);
                if(!$topic){
                    $isDeletedTopic = true;    
                }else{
                    $topic_cate_id = $topic->CateID;
                }
            }
            if(!$isDeletedTopic){            
                $user_id = $tokenInfo->UserID;
                $this->User_model->updateLastActivity($user_id);
                if ((isset($_FILES['Video']) && $_FILES['Video']['size'] > 0) || (isset($_FILES['Photo']) && $_FILES['Photo']['size'] > 0)) {
    
                    // post picture
                    $error = false;
                    $video = '';
                    if (isset($_FILES['Video']) && $_FILES['Video']['size'] > 0) {
                        $isVideo = $this->s3upload->upload('Video', '', BUCKET_NAME_VIDEO, 'video');
                        if (!$isVideo) {
                            $response->description = 'Video: '.$this->s3upload->display_errors();
                            $error = true;
                        } else {
                            $video = $isVideo;
                        }
                    }
                    
                    // post picture
                    $photo = '';
                    $photo100 = '';
                    $photo200 = '';
                    $photo300 = '';
                    if (isset($_FILES['Photo']) && $_FILES['Photo']['size'] > 0) {
                        $isPhoto = $this->s3upload->upload('Photo');
                        if (!$isPhoto) {
                            $response->code = $this->lang->line('invalid_image');
                            $response->description = $this->s3upload->display_errors();
                            $error = true;
                        } else {
    
                            $photo = $isPhoto;
                            if (empty($video))
                            $photo100 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 100, 150);
                            $photo200 = ''; //$this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 200, 300);
                            $photo300 = ''; //$this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 300, 450);
                        }
                    }
                    
                    
                    if (!$error && ($photo != '' || $video != '')) {
                        // Enable tag user
                        $content = '';
                        if (isset($_POST['PictureCaption']) && $_POST['PictureCaption'] != '') {
                            $lat = isset($_POST['Lat'])?$_POST['Lat']:'';
                            $lon = isset($_POST['Lon'])?$_POST['Lon']:'';
                            $is_vegas = $this->commonlib->isVegas($lat, $lon);
                            $aResponse = $this->User_Profile_Status_model->parseData($_POST['PictureCaption'], $tokenInfo->UserID, $is_vegas);
                            $content = $aResponse['content'];
                        }
                        $activityType = ACTIVITY_POST_PICTURE;
                        if (!empty($video)){
                            // add to video tbl
                            $data = array(
                                'UserID' => $user_id,
                                'Content'   => $content,
                                'VideoSource' => 'upload',
                                'VideoID' => $video,
                                'VideoThumbnail' => $photo,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                            );

                            $id = $this->Video_model->add($data);
                            $activityType = ACTIVITY_POST_VIDEO;
                            
                        }else{
                            // add to PicturePostActivity tbl
                            $data = array(
                                'UserID' => $user_id,
                                'PictureCaption' => $content,
                                'Photo' => $photo,
                                'Photo100' => $photo100,
                                'Photo200' => $photo200,
                                'Photo300' => $photo300,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                            );
                            $id = $this->Picture_Post_Activity_model->add($data);
                        }
                        $fanpage = isset($_POST['FanPage'])?$_POST['FanPage']:null;
                        //ANOMO-11510 - When a Celebrity makes a post it will show on both Recent and Fan tabs
                        //ANOMO-11717 - user HasFanPage
                        if(empty($fanpage) && ($tokenInfo->IsAgeLess == 1 || $tokenInfo->IsAdmin == 1 || $tokenInfo->HasFanPage == 1)){
                            $fanpage = $user_id;
                        }   
                                 
                        $activity = new stdClass();
                        if ($id) {
                            // publishing a feed
                            $feed = array(
                                'FromUserID' => $user_id,
                                'FromUserName' => $tokenInfo->UserName,
                                'Avatar' => $tokenInfo->Avatar,
                                'Type' => $activityType,
                                'ActionType' => $activityType,
                                'Message' => $content,
                                'Image' => empty($video)?$photo:'',
                                'RefID' => $id,
                                'Lat' => $tokenInfo->Lat,
                                'Lon' => $tokenInfo->Lon,
                                'BirthDate' => $tokenInfo->BirthDate,
                                'Gender' => $tokenInfo->Gender,
                                'CreatedDate' => gmdate('Y-m-d H:i:s'),
                                'NeighborhoodID' => $tokenInfo->NeighborhoodID,
                                'FanPage' => $fanpage,
                                'TopicID' => $topic_id,
                                'VideoID'   => $video,
                                'VideoSource' => !empty($video)?'upload':'',
                                'VideoThumbnail'    => !empty($video)?$photo:'',
                            );
                            $feed_id = $this->Activity_model->add($feed);
                            if ($feed_id) {
                                // update Topic LastModified
                                if($topic_id){
                                    $this->Topic_model->increasePost($topic_id);                                 
                                }
                                
                                // ANOMO-11749  Notification to ALL FOLLOWERS of the ageless user
                                if ($tokenInfo->IsAgeLess == 1 && !empty($fanpage)) {
                                    $this->Cron_Mention_User_model->addNotifyFollowerToQueue(array('UserID' => $user_id,
                                        'ContentID' => $id,
                                        'ContentType' => $activityType,
                                        'CreatedDate' => gmdate('Y-m-d H:i:s')));
                                }
                                    
                                if ($content != '') {
                                    // should add to a queue
                                    $this->Cron_Mention_User_model->saveHistoryAndPushNotify($content, $user_id, $id, $activityType);
    
                                    // ANOMO-7852 add hashtag
                                    $this->Hashtag_model->add($content, $feed_id, $topic_cate_id, $topic_id);
                                    
                                    // ANOMO-11242 - Fan page notifications
                                    // add to push notification historys
                                    if(!empty($fanpage) && $user_id != $fanpage){
                                        $pushData = array(
                                            'UserID' => $fanpage,
                                            'SendUserID' => $user_id,
                                            'Type' => HISTORY_FANPAGE_POST,  // 18                                    
                                            'ContentType' => $activityType,
                                            'ContentID' => $id,
                                            'IsAnonymous'   => '',
                                            'IsRead' => 1,
                                            'CreatedDate' => gmdate('Y-m-d H:i:s')
                                        );
                                        $history_id = $this->Notification_History_model->add($pushData);
                                        $targetUserInfo = $this->User_model->getUserInfo($fanpage);
                                        if ($history_id && $targetUserInfo->AllowFollowNotice == 1) {
                                            // push notification to target user
                                            $msg_push = str_replace('%name%', $tokenInfo->UserName, $this->lang->line('post_on_fanpage'));
                                            $this->mypushnotify->send($fanpage, $user_id, NOTIFY_FANPAGE_POST, $msg_push, 
                                                array(
                                                    'HistoryID' => $history_id,
                                                    'ContentID' => $id,
                                                    'ContentType' => $activityType                                        
                                                )
                                            );
                                        }
                                    }
                                    //End push notification
                                }
    
                                $activity->ID = $feed_id;
                                $activity->PlaceID = '';
                                $activity->Name = '';
                                $activity->CreatedDate = gmdate('Y-m-d H:i:s');
                                $activity->Content = $content;
                                $activity->Type = $activityType; // post picture activity
                                $activity->Photo = $this->config->item('s3_photo_url') . $photo;
                                
                                $activity->Photo100 = !empty($photo100)?$this->config->item('s3_photo_url') . $photo100:'';
                                
                                $activity->Video = !empty($video)?$this->config->item('s3_video_url') . $video:'';
                            }
    
                            $response->Activities = $activity;
                            $response->code = $this->lang->line('ok');
    
                        } else {
                            $response->code = $this->lang->line('db_error');
                        }
                    }else{
                        $response->code = $this->lang->line('invalid_request');
                    }
                } else {
                    $response->code = $this->lang->line('invalid_request');
                }
            }else{
                $response->code = $this->lang->line('invalid_request');
            }
            
        }
        echo json_encode($response);
    }


    /**
     * check username or email is existed
     *
     *
     */
    function check_username_or_email_is_existed() {
        $response = new stdClass();
        $user_name = isset($_POST['UserName'])?$_POST['UserName']:'';
        $email = isset($_POST['Email'])?$_POST['Email']:'';
        if (empty($user_name) && empty($email)) {
            $response->code = $this->lang->line('invalid_request');
        } else {
            $invalid = 0;
            if (!empty($user_name)){
                if (!preg_match('/^[a-zA-Z0-9]+$/', $user_name)){
                    $invalid = 1;
                    $response->code = $this->lang->line('invalid_username');
                }else{
                    //check bad word                    
                    $banwords = $this->User_model->getListBanWords();                            
                    if($banwords && count($banwords)>0){                        
                        foreach ( $banwords as $item ) {
                            if (stripos(strtolower(trim($user_name)), strtolower($item->word)) !== FALSE ) {
                                $invalid = 1;
                                $response->code = $this->lang->line('invalid_username');
                                break;
                            }
                        }
                    } 
                }
            }
            if (!$invalid){
                $objUser = $this->User_model->checkExistedUserNameOrEmail($user_name, $email);
                if ($objUser){
                    if (strtolower($user_name) == strtolower($objUser->UserName)){
                        $response->code = $this->lang->line('username_existed');
                    }else{
                        $response->code = $this->lang->line('email_existed');
                    }
                }else{
                    $response->code = $this->lang->line('ok');
                }
            }
        }
        echo json_encode($response);
    }


    function get_list_follower($token = '', $target_user_id = 0, $page = 1) {
        $tokenInfo = $this->Wstoken_model->getToken($token);
        $response = new stdClass();
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!is_numeric($target_user_id) || !is_numeric($page)) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $targetUserInfo = $this->User_model->getUserInfo($target_user_id);
                if (!$targetUserInfo) {
                    $response->code = $this->lang->line('invalid_request');
                } else {
                    $limit = 15;
                    $offset = ($page - 1) * $limit;
                    $total = 0;
                    $keyword = '';
                    if (isset($_POST['Keyword']) && !empty($_POST['Keyword'])) {
                        $keyword = strtolower(trim($_POST['Keyword']));
                        $limit = 0;
                        $offset = '';
                    }

                    $listFollower = $this->User_model->getListFollower($target_user_id, $total, $keyword, $offset, $limit, $tokenInfo->UserID);
                    if ($listFollower) {
                        foreach ($listFollower as $user) {
                            $user->IsFavorite = (int) $user->IsFavorite;
                            $user->CheckinStatus = (int) $user->CheckinStatus;
                        }
                    }
                    $response->TotalPage = $limit > 0 ? ceil($total / $limit) : 1;
                    $response->CurrentPage = $page;
                    $response->ListFollower = $listFollower;
                    $response->NumberOfFollower = (int)$total;
                    $response->code = $this->lang->line('ok');
                }
            }
        }
        echo json_encode($response);
    }

    function get_list_following($token = '', $target_user_id = 0, $page = 1) {
        $tokenInfo = $this->Wstoken_model->getToken($token);
        $response = new stdClass();
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!is_numeric($target_user_id) || !is_numeric($page)) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $targetUserInfo = $this->User_model->getUserInfo($target_user_id);
                if (!$targetUserInfo) {
                    $response->code = $this->lang->line('invalid_request');
                } else {
                    $limit = 15;
                    $offset = ($page - 1) * $limit;
                    $total = 0;
                    $keyword = '';
                    if (isset($_POST['Keyword']) && !empty($_POST['Keyword'])) {
                        $keyword = strtolower(trim($_POST['Keyword']));
                        $limit = 0;
                        $offset = '';
                    }
                    $listFollowing = $this->User_model->getListFollowing($target_user_id, $total, $keyword, $offset, $limit, $tokenInfo->UserID);
                    if ($listFollowing) {
                        foreach ($listFollowing as $user) {
                            $user->IsFavorite = (int) $user->IsFavorite;
                            $user->CheckinStatus = (int) $user->CheckinStatus;
                        }
                    }
                    $response->TotalPage = $limit > 0 ? ceil($total / $limit) : 1;
                    $response->CurrentPage = $page;
                    $response->ListFollowing = $listFollowing;
                    $response->NumberOfFollowing = (int)$total;
                    $response->code = $this->lang->line('ok');
                }
            }
        }
        echo json_encode($response);
    }

    /**login function (username & password)
     *
     * @param type $user_name
     * @param type $password
     */
    function login() {
        $response = new stdClass();
        $user_name = isset($_POST['UserName'])?trim($_POST['UserName']):'';
        $password = isset($_POST['Password'])?trim($_POST['Password']):'';
        if ($user_name != '' && $password != '') {
            $login = $this->User_model->login($user_name, $password);
            if (!$login) {
                $response->code = $this->lang->line('failed');
            } else {
                if ($login->Deactive == 1) {
                    $response->code = $this->lang->line('deactive');
                } elseif ($login->IsBanned == 1) {
                    $response->code = $this->lang->line('banned');
                } else {
                    // generate token
                    $token = $this->commonlib->gen_uuid();
                    if ($token){
                        // update last activity
                        $this->User_model->updateLastActivity($login->UserID, 1);

                        $userId = $login->UserID;
                        $tokenObj = $this->Wstoken_model->checkTokenOfUser($userId);
                        if ($tokenObj) {
                            $this->Wstoken_model->update(array('UserID' => $userId), array('Token' => $token), $tokenObj->Token);
                        } else {
                            $data["Token"] = $token;
                            $data["UserID"] = $userId;
                            $this->Wstoken_model->create($data);
                        }

                        $response->token = $token;
                        unset($login->IsBanned);
                        unset($login->Deactive);
                        $response = $login;
                        $response->ListIntent = $this->Intent_model->getListIntentOfUser($login->UserID);
                        $response->Tags = $this->User_model->getListTagOf($login->UserID);
                        $response->token = $token;
                        $response->code = $this->lang->line('ok');
                    }else{
                        $response->code = $this->lang->line('failed');
                    }
                }
            }
        } else {
            $response->code = $this->lang->line('invalid_request');
        }
        echo json_encode($response);
    }

    /**
     * login function for trial account
     *
     * @param int $user_id
     * @return json
     */
    function login_trial_account($user_id = '') {
        $response = new stdClass();
        $login = $this->User_model->getUserInfo($user_id);
        if ($login) {
            if ($login->Deactive == 1) {
                $response->code = $this->lang->line('deactive');
            } elseif ($login->IsBanned == 1) {
                $response->code = $this->lang->line('banned');
            } else {
                // generate token
                $token = $this->commonlib->gen_uuid();
                if ($token){
                    $userId = $login->UserID;
                    $tokenObj = $this->Wstoken_model->checkTokenOfUser($userId);
                    if ($tokenObj) {
                        $this->Wstoken_model->update(array('UserID' => $userId), array('Token' => $token), $tokenObj->Token);
                    } else {
                        $data["Token"] = $token;
                        $data["UserID"] = $userId;
                        $this->Wstoken_model->create($data);
                    }

                    // update last activity
                    $this->User_model->updateLastActivity($login->UserID, 1);
                    $userObj = new stdClass();
                    $userObj->UserID = $login->UserID;
                    $userObj->UserName = $login->UserName;
                    $userObj->Gender = $login->Gender;
                    $userObj->BirthDate = $login->BirthDate;
                    $userObj->Credits = (int)$login->Credits;
                    $userObj->Email = !is_null($login->Email)?$login->Email:"";
                    $userObj->FacebookID = !is_null($login->FacebookID)?$login->FacebookID:"";
                    $userObj->FbEmail = !is_null($login->FbEmail)?$login->FbEmail:"";
                    $userObj->ProfileStatus = !is_null($login->ProfileStatus)?$login->ProfileStatus:"";
                    $userObj->AboutMe = !is_null($login->AboutMe)?$login->AboutMe:"";
                    $userObj->Avatar = !is_null($login->Avatar)?$this->config->item('s3_photo_url') . $login->Avatar:"";
                    $userObj->FullPhoto = !is_null($login->FullPhoto)?$this->config->item('s3_photo_url') . $login->FullPhoto:"";
                    $userObj->CoverPicture = !is_null($login->CoverPicture)?$this->config->item('s3_photo_url') . $login->CoverPicture:$this->config->item('s3_photo_url') . COVER_PICTURE_DEFAULT;;
                    $userObj->AllowSendGiftNotice = $login->AllowSendGiftNotice;
                    $userObj->AllowRevealNotice = $login->AllowRevealNotice;
                    $userObj->AllowChatNotice = $login->AllowChatNotice;
                    $userObj->AllowAnomotionNotice = $login->AllowAnomotionNotice;
                    $userObj->AllowCommentActivityNotice = $login->AllowCommentActivityNotice;
                    $userObj->AllowLikeActivityNotice = $login->AllowLikeActivityNotice;
                    $userObj->AllowFollowNotice = $login->AllowFollowNotice;
                    $userObj->OnlyShowCountry = $login->OnlyShowCountry;
                    $userObj->Tags = $this->User_model->getListTagOf($login->UserID);
                    $userObj->ListIntent = $this->Intent_model->getListIntentOfUser($login->UserID);
                    $userObj->GenderDating = $login->GenderDating;
                    $userObj->IsVendor = $login->IsVendor;

                    $response = $userObj;
                    $response->token = $token;
                    $response->code = $this->lang->line('ok'); // actived
                }else{
                    $response->code = $this->lang->line('failed');
                }
            }
        }else {
            $response->code = $this->lang->line('not_register');
        }
        echo json_encode($response);
    }

    /**
     * Login with facebok
     * @param type $fb_id
     * @param type $access_token
     */
    function login_with_fb() {
        $response = new stdClass();
        $fb_id = isset($_POST['FacebookID'])?$_POST['FacebookID']:'';
        $access_token = isset($_POST['FbAccessToken'])?$_POST['FbAccessToken']:'';
        $email = isset($_POST['Email'])?$_POST['Email']:'';
        if (!self::_checkFb($fb_id, $access_token)) {
            $response->code = $this->lang->line('failed');
        } else {
            $login = $this->User_model->loginWithFb($fb_id, $email);
            if ($login) {
                if ($login->Deactive == 1) {
                    $response->code = $this->lang->line('deactive');
                } elseif ($login->IsBanned == 1) {
                    $response->code = $this->lang->line('banned');
                } else {
                    // generate token
                    $token = $this->commonlib->gen_uuid();
                    if ($token){
                        $userId = $login->UserID;
                        $tokenObj = $this->Wstoken_model->checkTokenOfUser($userId);
                        if ($tokenObj) {
                            $this->Wstoken_model->update(array('UserID' => $userId), array('Token' => $token), $tokenObj->Token);
                        } else {
                            $data["Token"] = $token;
                            $data["UserID"] = $userId;
                            $this->Wstoken_model->create($data);
                        }

                        if ($login->FacebookID != $fb_id && $login->Email == $email){
                            // should update fbid for this user
                            $this->User_model->updateUser(array('FacebookID' => $fb_id, 'LastActivity' => gmdate('Y-m-d H:i:s')), array('UserID' => $login->UserID));
                        }else{
                            // update last activity
                            $this->User_model->updateLastActivity($login->UserID, 1);
                        }
                        unset($login->IsBanned);
                        unset($login->Deactive);

                        $response = $login;
                        $response->ListIntent = $this->Intent_model->getListIntentOfUser($login->UserID);
                        $response->Tags = $this->User_model->getListTagOf($login->UserID);
                        $response->token = $token;
                        $response->code = $this->lang->line('ok');
                    }else{
                        $response->code = $this->lang->line('failed');
                    }
                }
            } else {
                $response->code = $this->lang->line('not_register');
            }
        }
        echo json_encode($response);
    }

    private function _checkFb($fb_id = '', $access_token = '') {
        if (trim($fb_id) == '' || trim($access_token) == '') {
            return false;
        }
        $url = "https://graph.facebook.com/me?field=id&access_token=" . $access_token;
        $result = @file_get_contents($url);
        if (!$result) {
            return false;
        } else {
            $data = json_decode($result);
            if ($data) {
                $id = $data->id;
                if ($id == $fb_id) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * XMPP - upload image
     * @param string $token
     */
    function upload_image($token = '') {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (isset($_FILES['Photo']) && $_FILES['Photo']['size'] > 0) {
                $isPhoto = $this->s3upload->upload('Photo','',BUCKET_NAME_CHAT);
                if (!$isPhoto) {
                    $response->code = $this->lang->line('invalid_image');
                    $response->description = $this->s3upload->display_errors('', '');
                } else {
                    $upload_data = $this->upload->data();
                    //$photo = $upload_data['file_name'];
                    //$photo100 = $this->Image_model->create_thumbnail($photo, 100, 150);
                    //$photo200 = $this->Image_model->create_thumbnail($photo, 200, 300);
                    //$photo300 = $this->Image_model->create_thumbnail($photo, 300, 450);
                    $photo = $isPhoto;
                    $photo100 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 100, 150,BUCKET_NAME_CHAT);
                    $photo200 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 200, 300,BUCKET_NAME_CHAT);
                    $photo300 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 300, 450,BUCKET_NAME_CHAT);
                    if ($photo != '' && $photo100 != '' && $photo200 != '' && $photo300 != '') {
                        $response->code = $this->lang->line('ok');
                        //$url = base_url().$this->config->item('photo_url');
                        $url = $this->config->item('s3_chat_url');
                        $response->Photo = $url . $photo;
                        $response->Photo100 = $url . $photo100;
                        $response->Photo200 = $url . $photo200;
                        $response->Photo300 = $url . $photo300;
                    } else {
                        $response->code = $this->lang->line('failed');
                    }
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }


    /**Search user function
     *
     * @param type $token
     * @param type $user_id
     * @param type $lat
     * @param type $lon
     * @param type $page
     * @param type $gender
     * @param type $from_age
     * @param type $to_age
     * @return type
     */
    function search_user($token = '', $user_id = 0, $lat = '', $lon = '', $page = 1, $gender = 0, $from_age = 0, $to_age = 0) {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $this->User_model->updateLastActivity($user_id);
            $userInfo = $this->User_model->getUserInfo($user_id);
            $genderArr = array(0, 1, 2);
            // ANOMO-8070 - if we cant get location will show Earth
            if ((!is_numeric($lat) && $lat != 'null') || (!is_numeric($lon) && $lon != 'null') || !in_array($gender, $genderArr) || $from_age < 0 || $to_age < 0 || $to_age < $from_age) {
                $response->code = $this->lang->line('invalid_request');
            } else {

                // POST method
                $country = isset($_POST['Country']) ? trim($_POST['Country']) : '';
                $city = isset($_POST['City']) ? trim($_POST['City']) : '';
                $state = isset($_POST['State']) ? trim($_POST['State']) : '';
                $error = 0;

                // ANOMO-8070 - if we cant get location will show Earth
                if ($lat == 'null' && $lon == 'null') {
                    $country = 'Earth';
                    $city = '';
                    $state = '';
                }

                // ANOMO-8116
                // client doesn't send Country, City, State
                // then get info from google
                if ($lat != 'null' && $lon != 'null' && empty($country) && empty($city) && empty($state)) {
                    $geoInfo = $this->Neighborhood_model->getCountryName($lat, $lon);
                    if ($geoInfo) {
                        $country = $geoInfo['country'];
                        $city = $geoInfo['city'];
                        $state = $geoInfo['state'];
                    } else {
                        // can not get location
                        // return fail
                        $error = 1;
                    }
                }

                if (!$error) {
                    $data = array(
                        'UserID' => $user_id,
                        'Lat' => $lat,
                        'Lon' => $lon,
                        'CountryName' => $country,
                        'City' => $city,
                        'State' => $state
                    );


                    $this->Neighborhood_model->sp_location($data, $userInfo->IsAgeLess);
                    // do our automated stuffs here...
                    $this->promotionlib->requestAutoPromoMessage($lat, $lon, $userInfo);
                }
                // end ANOMO-8116

                $results = new stdClass();
                $limit = 10;
                $offset = ($page - 1) * $limit;
                $total = 0;
                $keyword = isset($_POST['Keyword'])?trim(strtolower($_POST['Keyword'])):'';
                $is_search_tag = isset($_POST['IsSearchTag']) ? $_POST['IsSearchTag'] : 0;
                
                if ($is_search_tag == 2){ // search following
                    $users = $this->User_model->searchFollowing($keyword, $user_id, $limit, $offset, $total);
                }else{
                    if ($keyword != '') {                    
                        if ($is_search_tag == 1){
                            $users = $this->User_model->searchUserbyTag($keyword, $user_id, $limit, $offset, $total);
                        }else{
                            $users = $this->User_model->searchUserbyKeyword($keyword, $user_id, $limit, $offset, $total);
                        }
                        
                    } else {
                        // ANOMO-5958 - filter by age, gender
                        $users = $this->User_model->searchUserbyCoordinate($total, $user_id, $lat, $lon, $limit, $offset, $gender, $from_age, $to_age);

                    }
                }
                $totalPage = ceil($total / $limit);
                
                if ($users) {
                    foreach ($users as $user) {
                        if ($user->Avatar) {
                            $user->Avatar = $this->config->item('s3_photo_url') . $user->Avatar;
                        }
                        
                        if ($user->CoverPicture) {
                            $user->CoverPicture = $this->config->item('s3_photo_url') . $user->CoverPicture;
                        }else{
                            $user->CoverPicture = $this->config->item('s3_photo_url') . COVER_PICTURE_DEFAULT;
                        }
                        //ANOMO-11218 - Hide Email and Facebook
                        if($user->Email){
                            $user->Email = "";
                        }
                        if($user->ProfileStatus){
                            $user->ProfileStatus = "";
                        }
                        
                        $user->Tags = $this->User_model->getListTagOf($user->UserID);
                        $user->RecentPicture = array(); //$this->Activity_model->getRecentPictureOfUser($user->UserID);
                    }
                }

                $results->ListUser = $users;
                $results->CurrentPage = $page;
                $results->TotalPage = $totalPage;
                $response->results = $results;
                $response->code = $this->lang->line('ok');
            }
        }
        echo json_encode($response);
    }

    /**
     * return list interests
     *
     */
    function list_interests(){
        $response = new stdClass();
        $response->ListInterests = $this->Tag_model->getListInterests();
        $response->code = $this->lang->line('ok');
        echo json_encode($response);
    }

    /**return recent user's post
     *
     * @param type $token
     * @param type $target_user_id
     * @param type $page
     */
    function get_user_post($token = '', $target_user_id = 0, $page = 1) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $targetUserInfo = $this->User_model->getUserInfo($target_user_id);
            if (!$targetUserInfo) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $limit = 10;
                $offset = ($page - 1) * $limit;
                $total = 0;
                $validTypes = array(0, 1, 2, 7, 27, 6, 8, 9);
                $userActivity = $this->Activity_model->getActivityOfUser($target_user_id, $total, $offset, $limit);
                foreach ($userActivity as $act) {
                    $numLike = 0;
                    $numComment = 0;
                    $isComment = 0;
                    if (in_array($act->Type, $validTypes)) {
                        $filter_type = 0;

                        $contentInfo = $this->Activity_model->getActivitiesFeedInfo($act->RefID, $act->Type, $tokenInfo->UserID, $filter_type, $act->FromUserID);
                        if ($contentInfo) {
                            $numLike = $contentInfo->Like;
                            $numComment = $contentInfo->Comment;
                            $isComment = $contentInfo->IsComment;

                            $act->IsLike = $contentInfo->IsLike;
                            $act->IsBlock = $contentInfo->IsBlock;
                            $act->IsBlockTarget = $contentInfo->IsBlock;
                            $act->IsReported = $contentInfo->IsBlockTarget;
                            $act->IsStopReceiveNotify = $contentInfo->IsStopReceiveNotify;
                        }
                    }
                    $act->IsComment = $isComment;
                    $act->Comment = $numComment;
                    $act->Like = $numLike;
                }
                $response->Activities = $userActivity;
                $response->Page = $page;
                $response->TotalPage = ceil($total / $limit);
                $response->code = $this->lang->line('ok');
            }
        }
        echo json_encode($response);
    }


    function get_all_user_post($token = '', $target_user_id = 0, $is_fan_page = 0, $activity_id = -1, $from_age = 0, $to_age = 0) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $targetUserInfo = $this->User_model->getUserInfo($target_user_id);
            if (!$targetUserInfo) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                // ANOMO-10878 - return list promos post
                if ($targetUserInfo->IsVendor == 1 && $is_fan_page == 0){
                    $userActivity = $this->Promos_model->listPromoPostOfVendor($tokenInfo->UserID, $target_user_id, $activity_id);
                }else{
                    //ANOMO-11844 fan page post notifications
                    if ($activity_id <= 0){ // first page 
                        $this->User_model->clearFanpageNofify($tokenInfo->UserID, $targetUserInfo);
                    }
                    
                    $validTypes = array(ACTIVITY_POST_STATUS, ACTIVITY_POST_PICTURE, ACTIVITY_POST_VIDEO);
                    $userActivity = $this->Activity_model->getAllActivityOfUser($tokenInfo->UserID, $target_user_id, $activity_id, $is_fan_page,$from_age,$to_age);
                    foreach ($userActivity as $act) {
                        $numLike = 0;
                        $numComment = 0;
                        $isComment = 0;
                        if (in_array($act->Type, $validTypes)) {
                            $filter_type = 0;

                            $contentInfo = $this->Activity_model->getActivitiesFeedInfo($act->RefID, $act->Type, $tokenInfo->UserID, $filter_type, $act->FromUserID);
                            if ($contentInfo) {
                                $numLike = $contentInfo->Like;
                                $numComment = $contentInfo->Comment;
                                $isComment = $contentInfo->IsComment;

                                $act->IsLike = $contentInfo->IsLike;
                                $act->IsBlock = $contentInfo->IsBlock;
                                $act->IsBlockTarget = $contentInfo->IsBlock;
                                $act->IsReported = $contentInfo->IsBlockTarget;
                                $act->IsStopReceiveNotify = $contentInfo->IsStopReceiveNotify;
                                $act->IsFavorite = $contentInfo->IsFavorite;
                            }
                        }
                        $act->IsComment = $isComment;
                        $act->Comment = $numComment;
                        $act->Like = $numLike;
                    }
                }
                $response->Activities = $userActivity;
                $response->code = $this->lang->line('ok');
            }
        }
        echo json_encode($response);
    }

    /**
     * return list avatar
     */
    function list_avatar($gender = 0){
        $response = new stdClass();
        if ($gender > 0){
            $response->ListAvatar = $this->Avatar_model->listAvatar($gender);
            $response->code = $this->lang->line('ok');
        }else{
            $response->code = $this->lang->line('invalid_request');
        }
        echo json_encode($response);
    }

    /**
     * return list intent
     */
    function list_intent(){
        $response = new stdClass();
        $response->ListIntent = $this->Intent_model->listIntent();
        $response->code = $this->lang->line('ok');
        echo json_encode($response);
    }

    /**return list gift received of user
     *
     * @param type $token
     * @param type $target_user_id
     * @param type $page
     */
    function get_gift_received($token = '', $target_user_id = 0, $page = 1){
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $targetUserInfo = $this->User_model->getUserInfo($target_user_id);
            if (!$targetUserInfo){
                $response->code = $this->lang->line('invalid_request');
            }else{
                $total = 0;
                $limit = 10;
                $offset = ($page - 1)*$limit;
                $giftReceived = $this->Gift_Send_model->getReceivedGift($target_user_id, $total, $offset, $limit);
                $response->ListGiftReceived = $giftReceived;
                $response->Page = $page;
                $response->TotalPage = ceil($total/$limit);
                $response->code = $this->lang->line('ok');
            }
        }
        echo json_encode($response);
    }


    /*Reveal WS
     * =======================================================================
     */

    /**Get reveal info
     *
     * @param type $token
     * @param type $target_user_id
     * @return
     *      UserRevealInfo:
     *          + information of current user
     *          + isRequest: a flag to know target user requested reveal to you
     *          + isReveal: a flag to know you revealed to target user
     *      TargetRevealInfo:
     *          + information of target user
     *          + isRequest: a flag to know you requested reveal to target user
     *          + isReveal: a flag to know target user revealed to you
     */
    function get_reveal_info($token = '', $target_user_id = 0) {
        $response = new stdClass();
        $current_user_id = $this->commonlib->checkValidToken($token);
        if ($current_user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($target_user_id)) {
                $userInfo = $this->User_model->getUserInfo($current_user_id);
                $targetUserInfo = $this->User_model->getUserInfo($target_user_id);
                if ($userInfo && $targetUserInfo) {
                    // 1. UserRevealInfo
                    $userReveal = new stdClass();
                    $requesting = array();
                    $revealTypes = array();
                    $pictureReveal = array();
                    $pictureRequest = array();
                    // get all reveal info btw 2 users
                    $objReveal = $this->Reveal_model->getRevealInfoBtw2User($target_user_id, $current_user_id);
                    if ($objReveal){
                        foreach($objReveal as $obj){
                            if ($obj->Type == 0){
                                $pictureRequest[] = $obj->PictureOrder;
                            }
                            if ($obj->Status == 0){
                                $requesting[] = $obj->Type;

                            }
                            if ($obj->Status == 1){
                                $revealTypes[] = $obj->Type;
                                if ($obj->Type == 0){
                                    $pictureReveal = array_merge($pictureReveal, explode(',', $obj->PictureIDs));
                                }
                            }
                        }
                    }

                    $userReveal->ListSecretPicture = (object)array(
                        'isRequest' => sizeof($pictureRequest)> 0 ?1:0,
                        'isReveal'  => sizeof($pictureReveal)> 0 ?1:0
                    );
                    $userReveal->FbEmail = (object) array('content' => isset($userInfo->FbEmail) ? $userInfo->FbEmail : '', 'isReveal' => (int) in_array(1, $revealTypes), 'isRequest' => (int) in_array(1, $requesting));
                    $userReveal->RealName = (object) array('content' => isset($userInfo->RealName) ? $userInfo->RealName : '', 'isReveal' => (int) in_array(2, $revealTypes), 'isRequest' => (int) in_array(2, $requesting));
                    $userReveal->College = (object) array('content' => isset($userInfo->College) ? $userInfo->College : '', 'isReveal' => (int) in_array(3, $revealTypes), 'isRequest' => (int) in_array(3, $requesting));
                    $userReveal->Major = (object) array('content' => isset($userInfo->Major) ? $userInfo->Major : '', 'description' => isset($userInfo->Major_description) ? $userInfo->Major_description : '', 'isReveal' => (int) in_array(7, $revealTypes), 'isRequest' => (int) in_array(7, $requesting));

                    $response->UserRevealInfo = $userReveal;

                    // 2. TargetRevealInfo
                    $targetUserReveal = new stdClass();
                    $targetRequesting = array();
                    $targetRevealTypes = array();
                    $targetPictureReveal = array();
                    $targetPictureRequest = array();
                    // get all reveal info btw 2 users
                    $objRevealTarget = $this->Reveal_model->getRevealInfoBtw2User($current_user_id, $target_user_id);
                    if ($objRevealTarget){
                        foreach($objRevealTarget as $obj){
                            if ($obj->Type == 0){
                                $targetPictureRequest[] = $obj->PictureOrder;
                            }
                            if ($obj->Status == 0){
                                $targetRequesting[] = $obj->Type;
                            }
                            if ($obj->Status == 1){
                                $targetRevealTypes[] = $obj->Type;
                                if ($obj->Type == 0){
                                    $targetPictureReveal = array_merge($targetPictureReveal, explode(',', $obj->PictureIDs));
                                }
                            }
                        }
                    }

                    $targetUserReveal->ListSecretPicture = (object)array(
                        'isRequest' => sizeof($targetPictureRequest)> 0 ?1:0,
                        'isReveal'  => sizeof($targetPictureReveal)> 0 ?1:0
                    );

                    $targetUserReveal->FbEmail = (object) array(
                        'content' => (isset($targetUserInfo->FbEmail) && in_array(1, $targetRevealTypes)) ? $targetUserInfo->FbEmail : '',
                        'isReveal' => (int) in_array(1, $targetRevealTypes),
                        'isRequest' => (int) in_array(1, $targetRequesting));
                    $targetUserReveal->RealName = (object) array(
                        'content' => (isset($targetUserInfo->RealName) && in_array(2, $targetRevealTypes)) ? $targetUserInfo->RealName : '',
                        'isReveal' => (int) in_array(2, $targetRevealTypes),
                        'isRequest' => (int) in_array(2, $targetRequesting));
                    $targetUserReveal->College = (object) array(
                        'content' => (isset($targetUserInfo->College) && in_array(3, $targetRevealTypes)) ? $targetUserInfo->College : '',
                        'isReveal' => (int) in_array(3, $targetRevealTypes),
                        'isRequest' => (int) in_array(3, $targetRequesting));
                    $targetUserReveal->Major = (object) array(
                        'content' => (isset($targetUserInfo->Major) && in_array(7, $targetRevealTypes)) ? $targetUserInfo->Major : '',
                        'description' => (isset($targetUserInfo->Major_description) && in_array(7, $targetRevealTypes)) ? $targetUserInfo->Major_description : '',
                        'isReveal' => (int) in_array(7, $targetRevealTypes),
                        'isRequest' => (int) in_array(7, $targetRequesting));

                    $response->TargetRevealInfo = $targetUserReveal;

                    $response->code = $this->lang->line('ok');
                } else {
                    $response->code = $this->lang->line('invalid_request');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }

    /**delete secret picture
     *
     * @param type $token
     * @param type $picture_id
     */
    function delete_secret_picture($token = '', $picture_id = 0) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($picture_id)) {
                $pictureInfo = $this->Secret_Picture_model->getSecretPictureInfo($picture_id);
                if ($pictureInfo && $pictureInfo->UserID == $tokenInfo->UserID) {
                    // delete reveal info
                    $this->Reveal_model->deleteSecretPicture($tokenInfo->UserID, $picture_id);
                    $this->Secret_Picture_model->delete(array('PictureID' => $picture_id, 'UserID' => $tokenInfo->UserID));
                    $response->code = $this->lang->line('ok');
                } else {
                    $response->code = $this->lang->line('invalid_request');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }

    /**
     * send request reveal v3
     *
     * @param string $token
     * @param int $target_user_id
     * @param int $type
     * @return json
     */
    function request_reveal($token = '', $target_user_id = 0, $type = '', $pictureId = '') {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $revealTypes = array(0, 1, 2, 3, 4, 5, 6, 7);
            if (!is_numeric($target_user_id) || !in_array($type, $revealTypes) || $user_id == $target_user_id) {
                $response->code = $this->lang->line('invalid_request');
            } else {

                $targetUserInfo = $this->User_model->getUserInfo($target_user_id);
                if (!$targetUserInfo) {
                    $response->code = $this->lang->line('invalid_parameter');
                } else {

                    $block_status = $this->User_model->returnBlockStatus($user_id, $target_user_id);
                    if ($block_status != '') {
                        $response->code = $block_status;
                    } else {

                        $isPush = false;
                        $reveal = $this->Reveal_model->getRequestReveal($user_id, $target_user_id, $type);
                        if ($reveal) {
                            if (isset($reveal->PictureOrder) && ($reveal->PictureOrder != $pictureId)) {
                                // send reveal
                                $data = array(
                                    'CurrentUserID' => $user_id,
                                    'TargetUserID' => $target_user_id,
                                    'Type' => $type,
                                    'Status' => 0,
                                    'PictureOrder' => $pictureId,
                                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                                );
                                $this->Reveal_model->add($data);
                                $response->code = $this->lang->line('request_sent');
                                $isPush = true;
                            } else {
                                if (isset($reveal->Status) && ($reveal->Status == 1)) {
                                    if ($type == 0) {
                                        $this->Reveal_model->update(array('Status' => 0), array('ID' => $reveal->ID));
                                        $response->code = $this->lang->line('ok');
                                        $isPush = true;
                                    } else {
                                        $response->code = $this->lang->line('revealed');
                                    }
                                } elseif (isset($reveal->Status) && ($reveal->Status == 0)) {
                                    $response->code = $this->lang->line('request_sent');
                                    $isPush = true;
                                }
                            }
                        } else {
                            // send reveal
                            $data = array(
                                'CurrentUserID' => $user_id,
                                'TargetUserID' => $target_user_id,
                                'Type' => $type,
                                'Status' => 0,
                                'PictureOrder' => $pictureId,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                            );
                            $this->Reveal_model->add($data);
                            $response->code = $this->lang->line('ok');
                            $isPush = true;
                        }
                        if ($isPush) {
                            // add to push notification historys
                            $pushData = array(
                                'UserID' => $target_user_id,
                                'SendUserID' => $user_id,
                                'RevealType' => $type,
                                'PictureOrder' => $pictureId,
                                'Type' => HISTORY_REQUEST_REVEAL, // 8 - request reveal
                                'Total' => 1,
                                'IsRead' => 1,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                            );
                            $this->Notification_History_model->add($pushData);
                            if ($targetUserInfo->AllowRevealNotice == 1) {
                                $pictureId = $pictureId > 0?$pictureId:-1;
                                $this->mypushnotify->send($target_user_id, $user_id, NOTIFY_REQUEST_REVEAL, $this->lang->line('msg_reveal_request'), array('RevealType' => $type, 'RevealPictureID' => $pictureId));
                            }
                        }
                    }
                }
            }
        }
        echo json_encode($response);
    }



    /**
     * response reveal v2
     *
     * @param string $token
     * @param int $target_user_id
     * @param int $type
     * @param string $is_accept
     * @param int $picture_id
     * @return json
     */
    function response_reveal($token = '', $target_user_id = 0, $type = '', $is_accept = '', $picture_ids = '') {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $revealTypes = array(0, 1, 2, 3, 4, 5, 6, 7);
            $acceptValues = array('YES', 'NO');
            if (!is_numeric($target_user_id) || !in_array($type, $revealTypes) || !in_array($is_accept, $acceptValues) || $user_id == $target_user_id) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $targetUserInfo = $this->User_model->getUserInfo($target_user_id);
                if (!$targetUserInfo) {
                    $response->code = $this->lang->line('invalid_parameter');
                } else {
                    $reveal_picture_id = $picture_ids > 0?$picture_ids:-1;
                    $reveal = $this->Reveal_model->getRequestReveal($target_user_id, $user_id, $type);
                    if ($type == 0) {
                        $ids = array();
                        if ($reveal) {
                            if ($is_accept == 'YES') {
                                $ids = array_merge(explode(',', $reveal->PictureIDs), explode(',', $picture_ids));
                            } else {
                                $ids = array_diff(explode(',', $reveal->PictureIDs), explode(',', $picture_ids));
                            }
                        } else {
                            if ($is_accept == 'YES') {
                                $ids = explode(',', $picture_ids);
                            }
                        }
                        $picture_ids = implode(",", array_unique(array_filter($ids)));
                    }
                    if ($reveal) {
                        if ($reveal->Status == 0) {
                            // if accept = yes  => update status = 1
                            if ($is_accept == 'YES') {
                                $this->Reveal_model->update(array('Status' => 1, 'PictureIDs' => $picture_ids), array('ID' => $reveal->ID));

                                // update notification history with type = 2 (reveal)
                                $notificationHistoryData = array(
                                    'UserID' => $target_user_id,
                                    'SendUserID' => $user_id,
                                    'Type' => HISTORY_REVEAL, //2 - accepted reveal
                                    'Total' => 1,
                                    'IsRead' => 1,
                                    'RevealType' => $type,
                                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                                );
                                $history_id = $this->Notification_History_model->add($notificationHistoryData);
                                if ($history_id && $targetUserInfo->AllowRevealNotice == 1) {
                                    $this->mypushnotify->send($target_user_id, $user_id, NOTIFY_ACCEPT_REVEAL, '', array('RevealType' => $type, 'HistoryID' => $history_id, 'RevealPictureID' => $reveal_picture_id));

                                }
                            } elseif ($is_accept == 'NO') {
                                if ($type == 0 && !empty($picture_ids)) {
                                    $this->Reveal_model->update(array('Status' => 1, 'PictureIDs' => $picture_ids), array('ID' => $reveal->ID));
                                } else {
                                    // else accept = no => remove request
                                    $this->Reveal_model->delete(array('ID' => $reveal->ID));
                                }
                            }
                            $response->code = $this->lang->line('ok');
                        } elseif ($reveal->Status == 1) {
                            if ($is_accept == 'YES') {
                                if ($type == 0) {
                                    $this->Reveal_model->update(array('Status' => 1, 'PictureIDs' => $picture_ids), array('ID' => $reveal->ID));

                                    // update notification history with type = 2 (reveal)
                                    $notificationHistoryData = array(
                                        'UserID' => $target_user_id,
                                        'SendUserID' => $user_id,
                                        'Type' => HISTORY_REVEAL, //2 - accepted reveal
                                        'Total' => 1,
                                        'IsRead' => 1,
                                        'RevealType' => $type,
                                        'CreatedDate' => gmdate('Y-m-d H:i:s')
                                    );
                                    $history_id = $this->Notification_History_model->add($notificationHistoryData);
                                    if ($history_id && $targetUserInfo->AllowRevealNotice == 1) {
                                        $this->mypushnotify->send($target_user_id, $user_id, NOTIFY_ACCEPT_REVEAL, '', array('RevealType' => $type, 'HistoryID' => $history_id, 'RevealPictureID' => $reveal_picture_id));
                                    }
                                    $response->code = $this->lang->line('ok');
                                } else {
                                    $response->code = $this->lang->line('revealed');
                                }
                            } elseif ($is_accept == 'NO') { // unreveal
                                if ($type == 0 && !empty($picture_ids)) {
                                    $this->Reveal_model->update(array('Status' => 1, 'PictureIDs' => $picture_ids), array('ID' => $reveal->ID));
                                } else {
                                    $this->Reveal_model->delete(array('ID' => $reveal->ID));
                                }

                                $response->code = $this->lang->line('ok');
                            }
                        }
                    } else {
                        if ($is_accept == 'YES') {
                            // current user is reveal to target user
                            $revealData = array(
                                'CurrentUserID' => $target_user_id,
                                'TargetUserID' => $user_id,
                                'Type' => $type,
                                'PictureOrder' => $picture_ids, //add pictureorder
                                'PictureIDs' => $picture_ids,
                                'Status' => 1,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                            );
                            $id = $this->Reveal_model->add($revealData);
                            if ($id) {
                                $notificationHistoryData = array(
                                    'UserID' => $target_user_id,
                                    'SendUserID' => $user_id,
                                    'Type' => HISTORY_REVEAL, // reveal
                                    'Total' => 1,
                                    'IsRead' => 1,
                                    'RevealType' => $type,
                                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                                );
                                $history_id = $this->Notification_History_model->add($notificationHistoryData);
                                if ($history_id && $targetUserInfo->AllowRevealNotice == 1) {
                                    $this->mypushnotify->send($target_user_id, $user_id, NOTIFY_ACCEPT_REVEAL, '', array('RevealType' => $type, 'HistoryID' => $history_id, 'RevealPictureID' => $reveal_picture_id));
                                }
                                $response->code = $this->lang->line('ok');
                            }
                        } else {
                            $response->code = $this->lang->line('ok');
                        }
                    }
                }
            }
        }
        echo json_encode($response);
    }



    /**
     * update reveal v4
     *
     * @param string $token
     * @return json
     */
    function update_reveal($token = '') {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (isset($_POST['RealName'])) {
                $revealData['RealName'] = $_POST['RealName'];
            }
            if (isset($_POST['FbEmail'])) {
                $revealData['FbEmail'] = $_POST['FbEmail'];
            }
            if (isset($_POST['College'])) {
                $revealData['College'] = $_POST['College'];
            }
            if (isset($_POST['Major'])) {
                $revealData['Major'] = $_POST['Major'];
            }
            if (isset($_POST['Major_description'])) {
                $revealData['Major_description'] = $_POST['Major_description'];
            }
            if (isset($revealData)) {
                $re = $this->User_model->updateUser($revealData, array('UserID' => $user_id));
                if ($re) {
                    $response->code = $this->lang->line('ok');
                } else {
                    $response->code = $this->lang->line('failed');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }

    /**
     * update secret picture v2
     *
     * @param string $token
     * @param int $user_id
     * @param int $picture_id
     * @return json
     */
    function update_secret_picture($token = '',  $picture_id = 0) {
        $this->load->model('Secret_Picture_model');
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!is_numeric($picture_id)) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                if ((isset($_FILES['SecretPicture']) || isset($_POST['SecretPicture']) || isset($_POST['ImageLink']))) {

                    $error = false;
                    if (isset($_FILES['SecretPicture']) && $_FILES['SecretPicture']['size'] > 0) {
                        $isPhoto = $this->s3upload->upload('SecretPicture');
                        if (!$isPhoto) {
                            $response->code = $this->lang->line('invalid_image');
                            $response->description = $this->s3upload->display_errors('', '');
                            $error = true;
                        } else {
                            $photo = $isPhoto;
                        }
                    }

                    if (isset($_POST['ImageLink']) && trim($_POST['ImageLink']) != '') {
                        // get image from link
                        $photo = $this->save_image($_POST['ImageLink']);
                        if ($photo == '') {
                            $error = true;
                            $response->code = $this->lang->line('unavailable_link');
                        }
                    }

                    if (!$error) {
                        if (isset($_POST['SecretPicture']) && trim($_POST['SecretPicture']) == 'delete') {
                            // remove picture with UserID & Order
                            $this->Secret_Picture_model->delete(array('PictureID' => $picture_id, 'UserID' => $user_id));
                            $response->code = $this->lang->line('ok');
                        } else {
                            // update secret picture
                            $photo100 = ''; //$this->s3upload->create_thumbnail($_FILES['SecretPicture']['tmp_name'], $photo, 100, 150);
                            $photo200 = ''; //$this->s3upload->create_thumbnail($_FILES['SecretPicture']['tmp_name'], $photo, 200, 300);
                            $photo300 = ''; //$this->s3upload->create_thumbnail($_FILES['SecretPicture']['tmp_name'], $photo, 300, 450);
                            $pictureData = array(
                                'FileName' => $photo,
                                'Photo100' => $photo100,
                                'Photo200' => $photo200,
                                'Photo300' => $photo300,
                                'UserID' => $user_id,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                            );
                            $pictureID = $this->Secret_Picture_model->add($pictureData);
                            if ($pictureID) {
                                $response->PictureID = (string) $pictureID;
                                $response->PictureUrl = $this->config->item('s3_photo_url') . $photo;
                                $response->Photo100 = ''; //$this->config->item('s3_photo_url') . $photo100;
                                $response->Photo200 = ''; //$this->config->item('s3_photo_url') . $photo200;
                                $response->Photo300 = ''; //$this->config->item('s3_photo_url') . $photo300;
                                $response->code = $this->lang->line('ok');
                            } else {
                                $response->code = $this->lang->line('db_error');
                            }
                        }
                    }
                } else {
                    $response->code = $this->lang->line('invalid_parameter');
                }
            }
        }
        echo json_encode($response);
    }

    /**
     * return list secret picture
     *
     * @param string $token
     * @param int $target_user_id
     */
    function get_secret_picture($token = '', $target_user_id = 0) {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!is_numeric($target_user_id)) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $user_id = $tokenInfo->UserID;

                // 1. return secret picture of current user 
                $userReveal = new stdClass();

                $secretPictureReveal = $this->Secret_Picture_model->getListSecretPictureOf($user_id);
                if ($secretPictureReveal && count($secretPictureReveal) > 0) {
                    $pictureReveal = array();
                    $pictureRequest = array();
                    // get all reveal info btw 2 users
                    $objReveal = $this->Reveal_model->getRevealInfoBtw2User($target_user_id, $user_id, 0);
                    if ($objReveal) {
                        foreach ($objReveal as $obj) {
                            $pictureRequest[] = $obj->PictureOrder;
                            if ($obj->Status == 1) {
                                $pictureReveal = array_merge($pictureReveal, explode(',', $obj->PictureIDs));
                            }
                        }
                    }
                    foreach ($secretPictureReveal as $sp) {
                        $sp->isReveal = (int) in_array($sp->PictureID, $pictureReveal);
                        $sp->isRequest = (int) in_array($sp->PictureID, $pictureRequest);
                    }
                }
                $userReveal->ListSecretPicture = $secretPictureReveal;

                // 2. return secret picture of target user
                $targetUserReveal = new stdClass();
                $secretPictureRevealTarget = $this->Secret_Picture_model->getListSecretPictureOf($target_user_id);
                if ($secretPictureRevealTarget && count($secretPictureRevealTarget) > 0) {
                    $pictureReveal = array();
                    $pictureRequest = array();
                    // get all reveal info btw 2 users
                    $objReveal = $this->Reveal_model->getRevealInfoBtw2User($user_id, $target_user_id, 0);
                    if ($objReveal) {
                        foreach ($objReveal as $obj) {
                            $pictureRequest[] = $obj->PictureOrder;
                            if ($obj->Status == 1) {
                                $pictureReveal = array_merge($pictureReveal, explode(',', $obj->PictureIDs));
                            }
                        }
                    }
                    foreach ($secretPictureRevealTarget as $key=> $sp) {
                        $sp->isReveal = (int) in_array($sp->PictureID, $pictureReveal);
                        $sp->isRequest = (int) in_array($sp->PictureID, $pictureRequest);
                        if ($sp->isReveal == 0 && $user_id != $target_user_id) { // if not review do not show them the real image!
                            $secretPictureRevealTarget[$key]->FileName = ANOMO_ICON;
                            $secretPictureRevealTarget[$key]->Photo100 = ANOMO_ICON;
                            $secretPictureRevealTarget[$key]->Photo200 = ANOMO_ICON;
                            $secretPictureRevealTarget[$key]->Photo300 = ANOMO_ICON;
                        }
                    }
                }
                $targetUserReveal->ListSecretPicture = $secretPictureRevealTarget;

                $response->UserRevealInfo = $userReveal;
                $response->TargetRevealInfo = $targetUserReveal;

                $response->code = $this->lang->line('ok');
            }
        }
        echo json_encode($response);
    }
    
    
    /**
     * Allow user block all anonymous post from anonymous owner
     * 
     */
    function block_anonymous($token = '', $activity_id = 0) {
        $response = new stdClass();
        $current_user_id = $this->commonlib->checkValidToken($token);
        if ($current_user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $activityInfo = $this->Activity_model->getActivityInfo($activity_id);   
            if (!$activityInfo){                
                $response->code = $this->lang->line('invalid_request');
            }else{                
                $target_user_id = $activityInfo->FromUserID;
                $IsAnonymous = $activityInfo->IsAnonymous;          
                if($target_user_id == $current_user_id || $IsAnonymous != 1){
                    $response->code = $this->lang->line('invalid_request');
                }else{                    
                    $blockObj = $this->Blocked_Anonymous_model->checkBlockAnonymous($current_user_id, $target_user_id);
                    if (!$blockObj){ // incase block user                        
                        $ret = $this->Blocked_Anonymous_model->blockAnonymous($current_user_id, $target_user_id);
                        if (!$ret) {
                            $response->code = $this->lang->line('failed');
                        } else {                        
                            $response->code = $this->lang->line('ok');
                        }
                    }else{                     
                        $response->code = $this->lang->line('ok');
                    }
                }         
            }
        }
        echo json_encode($response);
    }
    
    function send_verification_code($token = ''){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        $phone = isset($_POST['Phone'])?$_POST['Phone']:"";
        if (!$tokenInfo) {        
            $response->code = $this->lang->line('invalid_token');
        } else {
            if(empty($phone)){
                $response->code = $this->lang->line('invalid_parameter');
            }else{  
                if($tokenInfo->IsVerify == 1){
                    $response->code = $this->lang->line('invalid_request');
                }else{
                    //pre-format phone number
                    $phone = preg_replace('/[^0-9\+\']/', '', $phone);                        
                    $pos = strpos($phone, '+');
                    if($pos === false){
                        if (!empty($tokenInfo->Lat) && !empty($tokenInfo->Lon)){
                            $geoInfo = $this->Neighborhood_model->getCountryName($tokenInfo->Lat, $tokenInfo->Lon);
                            if ($geoInfo) {
                                $country_code = $geoInfo['code'];
                                $prefix = $this->User_Verification_model->getPhonePrefixByCountry($country_code);
                                if($prefix){
                                    $phone = $prefix.$phone;
                                }else{
                                    $phone = SMS_DEFAULT_PREFIX.$phone;
                                }
                            } else {
                                $phone = SMS_DEFAULT_PREFIX.$phone;
                            }
                        }else{
                            $phone = SMS_DEFAULT_PREFIX.$phone;
                        }
                    }
                    //check phone existed
                    $checkPhone = $this->User_model->checkExistedPhone($phone,$tokenInfo->UserID);
                    if($checkPhone > 0 ){
                        $response->code = $this->lang->line('phone_existed');
                    }else{                        
                        //Generate code and message
                        $code = rand(10000, 99999);
                        $CreatedDate = gmdate('Y-m-d H:i:s');
                        $msg = $this->lang->line('sms_verify_msg');
                        $msg = str_replace('%code%',$code,$msg);
                        
                        $verification = $this->User_Verification_model->getVerification($tokenInfo->UserID);
                        if($verification){                            
                            $DateForm = new DateTime($verification->CreatedDate);                      
                            $DateTo = new DateTime($CreatedDate);                            
                            $interval = $DateTo->diff($DateForm);
                            $minute = (float)$interval->format('%i');                                             
                            //Verification limit
                            if($minute < SMS_VERIFICATION_LIMIT){
                                $response->code = $this->lang->line('ok');
                            }else{
                                //send SMS
                                $status = $this->mytwilio->send($phone, $msg);
                                if ($status == 1){
                                    $response->code = $this->lang->line('ok');    
                                }else{
                                    $response->code = $this->lang->line('undelivered');    
                                }
                                $data = array(
                                    'Code' => $code,
                                    'Phone' => $phone,
                                    'CreatedDate' => $CreatedDate,
                                    'Attempt' => 0,
                                    'SendStatus' => ($status==1)?1:0
                                );
                                $this->User_Verification_model->update($data,array('UserID' => $tokenInfo->UserID));
                            }                            
                        }else{
                            //send SMS
                            $status = $this->mytwilio->send($phone, $msg);
                            if ($status == 1){                               
                                $response->code = $this->lang->line('ok');    
                            }else{
                                $response->code = $this->lang->line('undelivered');    
                            }
                            $data = array(
                                'UserID' => $tokenInfo->UserID, 
                                'Code' => $code,
                                'Phone' => $phone,
                                'CreatedDate' => $CreatedDate,
                                'Attempt' => 0,
                                'SendStatus' => ($status==1)?1:0
                            );
                            $this->User_Verification_model->add($data);
                        }                        
                    }
                }   
            } 
        }          
        echo json_encode($response);
    }
    
    function verify_code($token = ''){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        $code = isset($_POST['Code'])?$_POST['Code']:"";
        if (!$tokenInfo) {        
            $response->code = $this->lang->line('invalid_token');
        } else {
            if(empty($code)){
                $response->code = $this->lang->line('invalid_parameter');
            }else{  
                if($tokenInfo->IsVerify == 1){
                    $response->code = $this->lang->line('invalid_request');
                }else{
                    $verification = $this->User_Verification_model->getVerification($tokenInfo->UserID);
                    if($verification){                            
                        if($verification->Attempt >= SMS_ATTEMPT_LIMIT){
                            $response->code = $this->lang->line('invalid_request');
                        }else{
                            if(trim($verification->Code) != trim($code)){
                                //update Attempt number
                                $this->User_Verification_model->updateNumberAttempt($tokenInfo->UserID);
                                $response->code = $this->lang->line('invalid_code');
                            }else{
                                $checkPhone = $this->User_model->checkExistedPhone($verification->Phone,$tokenInfo->UserID);
                                if($checkPhone > 0 ){
                                    $response->code = $this->lang->line('phone_existed');
                                }else{
                                    //update user IsVerify status
                                    $userData = array(
                                        'IsVerify' => 1,
                                        'Phone' => $verification->Phone
                                    );
                                    $this->User_model->update($tokenInfo->UserID,$userData);
                                    //update code status
                                    $data = array(                                                                    
                                        'Attempt' => (int)$verification->Attempt + 1,
                                        'VerifiedDate' =>  gmdate('Y-m-d H:i:s')
                                    );
                                    $this->User_Verification_model->update($data,array('UserID' => $tokenInfo->UserID));
                                    $response->code = $this->lang->line('ok');
                                }
                            }
                        }                                                                                         
                    }else{
                         $response->code = $this->lang->line('invalid_request');
                    } 
                }   
            } 
        }          
        echo json_encode($response);
    }
    
    function check_verification_status($token = ''){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);        
        if (!$tokenInfo) {        
            $response->code = $this->lang->line('invalid_token');
        } else {
           if($tokenInfo->IsVerify == 1){
                $response->code = $this->lang->line('verified'); 
           }else{
                $response->code = $this->lang->line('unverified');
           }                      
        }          
        echo json_encode($response);
    }
    
    /**Get Match    
     */
    function get_match($token = '',$gender = 0, $from_age = 0, $to_age = 0) {
	set_time_limit(0);
        $iError = 0;
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $this->User_model->updateLastActivity($tokenInfo->UserID);
            $genderArr = array(0,1,2,3);            
            if (!in_array($gender, $genderArr) || $from_age < 0 || $to_age < 0 || $to_age < $from_age) {
                $response->code = $this->lang->line('invalid_parameter');
            } else {
                $Previous = $this->User_Match_model->checkPreviousMatch($tokenInfo->UserID);
                if($Previous){  
                    $time_left = self::check_time_left($tokenInfo->UserID,$Previous->CreatedDate,$Previous->FirstChatDate,$Previous->FirstReplyDate);                     
                    if($time_left>0){                                                        
                        $response->code = $this->lang->line('wait_for_next_match');
                        $response->timeleft = $time_left; 
                        $iError = 1;    
                    }
                }
                if($iError == 0){
                    $results = new stdClass();                          
                    $total_question  = $this->User_model->countUniqueQuestionByUser($tokenInfo->UserID);          
                    $total_interest  = $this->User_model->countCommonTagByUser($tokenInfo->UserID);  
                    $total_major = $this->User_model->countCommonMajorByUser($tokenInfo->UserID);
                    //running match    
                    if (USE_MATCH_VERSION == 1){
                        $users = $this->User_model->searchUserMatched($tokenInfo->UserID,$tokenInfo->BirthDate,$tokenInfo->Gender,$tokenInfo->GenderDating, $gender, $from_age, $to_age,$total_interest,$total_question, $total_major,0);
                        //expand filter
                        $loop = 1;
                        while(empty($users) && $loop<=2){                            
                            $users = $this->User_model->searchUserMatched($tokenInfo->UserID,$tokenInfo->BirthDate,$tokenInfo->Gender,$tokenInfo->GenderDating, $gender, $from_age, $to_age,$total_interest,$total_question, $total_major,$loop);    
                            $loop++;
                        }
                    } 
                    elseif (USE_MATCH_VERSION == 2){
                        $users = $this->User_model->searchUserMatched2($tokenInfo->UserID,$tokenInfo->BirthDate,$tokenInfo->Gender,$tokenInfo->GenderDating, $gender, $from_age, $to_age,$total_interest,$total_question,0);
                        //expand filter
                        $loop = 1;
                        while(empty($users) && $loop<=2){                            
                            $users = $this->User_model->searchUserMatched2($tokenInfo->UserID,$tokenInfo->BirthDate,$tokenInfo->Gender,$tokenInfo->GenderDating, $gender, $from_age, $to_age,$total_interest,$total_question,$loop);    
                            $loop++;
                        }
                    }         
                    if (!$users){
                        // Change match algorithm to just pick a random person if it fails
                        $users = $this->User_model->matchRandom($tokenInfo->UserID);
                        
                    }
                    if ($users) {
                        foreach ($users as $index=>$user) {
                            if($index == 0){
                                $matchData = array(
                                    'CurrentUserID' => $tokenInfo->UserID,
                                    'TargetUserID' => $user->UserID,
                                    'Interest' => $user->SharedInterest,
                                    'IceBreaker' => $user->SharedQuestion,
                                    'Major' => $user->SharedMajor,
                                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                                );
                                $this->User_Match_model->add($matchData);
                                
                                if ($user->Avatar) {
                                    $user->Avatar = $this->config->item('s3_photo_url') . $user->Avatar;
                                } 
                                if($user->FullPhoto){
                                    $user->FullPhoto = $this->config->item('s3_photo_url') . $user->FullPhoto;
                                }                           
                                if($user->Email){
                                    $user->Email = "";
                                }
                                if($user->ProfileStatus){
                                    $user->ProfileStatus = "";
                                }
                                $user->Tags = $this->User_model->getListTagOfMatchUser($tokenInfo->UserID,$user->UserID,0);            
                               
                                break;
                            }                        
                        }
                        //send message                    
                        if($tokenInfo->Avatar){
                            $tokenInfo->Avatar = $this->config->item('s3_photo_url') . $tokenInfo->Avatar;
                        }
                        //Result                         
                        $current_photo = "";
                        $target_photo = "";
                        $MATCH_REAL_THUMB = $this->Config_model->getConfig('MATCH_REAL_THUMB_IMAGE');
                        if($MATCH_REAL_THUMB && $MATCH_REAL_THUMB == 1){
                            //gernate image   
                            $current_photo = self::generate_match_img($users[0]->UserID,$tokenInfo->UserID);
                            $target_photo = self::generate_match_img($tokenInfo->UserID,$users[0]->UserID);
                        }
                        self::send_match_result($tokenInfo,$users[0],$current_photo);
                        self::send_match_result($users[0],$tokenInfo,$target_photo);                  
                        //Message
                        self::send_match_message($tokenInfo,$users[0]);
                        self::send_match_message($users[0],$tokenInfo);
                    }else{
                        //Fail Tracking
                        $aFail = array(
                            'UserID' => $tokenInfo->UserID,
                            'UserName' => $tokenInfo->UserName,
                            'BirthDate' => $tokenInfo->BirthDate,
                            'NumOfInterest' => $total_interest,
                            'NumOfQuestion' => $total_question,
                            'NumOfMajor'    => $total_major,
                            'Gender' => $tokenInfo->Gender,
                            'GenderDating' => $tokenInfo->GenderDating,
                            'GenderMatch' => $gender,
                            'FromAge' => $from_age,
                            'ToAge' => $to_age,
                            'CreatedDate' => gmdate('Y-m-d H:i:s')
                        );
                        $this->User_Match_model->addFailTracking($aFail);
                        //send notification email
                        $this->load->library('email');
                        $this->email->from($this->config->item('email_from'), $this->config->item('email_from_name'));
                        $this->email->to($this->config->item('report_match_result_to_email'));
                        $subject = $this->lang->line('canot_find_match');
                        if (!PRODUCTION){
                             $subject = $this->lang->line('staging_canot_find_match');
                        }
                        $this->email->subject($subject);                        
                        $msg = gmdate('Y-m-d H:i:s')." (GMT) ". "- There is no match result for ".$tokenInfo->UserName."<br/>";
                        $msg .= "<code class='prettyprint'>";
                        $aFail['Gender'] = ($aFail['Gender']==1)?"Male":(($aFail['Gender']==2)?"Female":"Both");
                        $aFail['GenderDating'] = ($aFail['GenderDating']==1)?"Male":(($aFail['GenderDating']==2)?"Female":"Both");
                        $aFail['GenderMatch'] = ($aFail['GenderMatch']==1)?"Male":(($aFail['GenderMatch']==2)?"Female":"Both");
                        foreach($aFail as $key=>$value){
                            $msg .= "<p>$key: $value</p>";
                        }
                        $msg .= "</code>";
                        $this->email->message($msg);
                        $sent = $this->email->send();
                    }
                    
                                        
                    $results->ListUser = $users;         
                    $response->results = $results;  
                    $response->code = $this->lang->line('ok');
                }                
            }
        }
        echo json_encode($response);
    }
    
    function send_match_result($sender, $receiver, $photo = ""){
        if($sender && $receiver){
            $msg =  $this->lang->line('anomo_matched_us');
            if(empty($photo)){
                $photo  = $this->config->item('s3_photo_url').ANOMO_MATCHED_US_PHOTO;   
            }
             
            
            $prefix = '';
            $is_production = PRODUCTION;
            if (!$is_production) {
                $prefix = 'dbg_';
            }
    
            $time = round(microtime(true) * 1000); 
            $receiver_id = $receiver->UserID;
            $sender_id = $sender->UserID;
            $msg_id = $sender_id.'_'.$receiver_id.'_'.$time;
            $aMsg = array(
                'time' => (string)$time,
                'text' => ($photo != '') ? '[image]' : $msg,
                'receiver_id'   => (string)$receiver_id,
                'receiver_avatar' => $receiver->Avatar,
                'receiver_name' => $receiver->UserName,
                'sender_id' => (string)$sender_id,
                'sender_name'   => $sender->UserName,
                'sender_avatar' => $sender->Avatar,
                'type' => 'MESSAGE',
                'photo' => $photo,
                'message_id'    => (string)$msg_id,
                'eatReceipts' => '1',
                'fullscreen' => 0,
                'tap_action'    => 2,
                'tap_param' => (string)$sender_id
            );        
            $args['channel'] = $prefix.$receiver_id;
            $args['message'] = $aMsg;
            // send msg
            $response = $this->pubnub->publish($args);
            if($response && $response[0] == 1){//send success
               //push IOS notification        
                // get device's user
                $deviceObj = $this->User_model->getPushNotificationReceiverInfo($receiver_id);
                if ($deviceObj) {
                    // only push to IOS device
                    if ($deviceObj->Type == 'ios' && $deviceObj->AllowChatNotice == 1) {
                        if (APN_MSG_LENGTH < strlen($msg)) {
                            $msg = substr($msg, 0, APN_MSG_LENGTH) . '...';
                        }
                        // other badge count
                        $total = $deviceObj->Number;
                        // chat badge count
                        $total += $this->Chat_Count_model->getChatCountOfUser($receiver_id);
                        // increase #chat
                        $total += 1;
        
                        $sound = 'receiveText.aiff';
                        $apnMsg['aps']['alert'] = $msg;
                        $apnMsg['aps']['badge'] = (int) $total;
                        $apnMsg['aps']['sound'] = (string) $sound;
                        $apnMsg['AIM'] = "YES";
                        $apnMsg['t'] = 1;
                        $apnMsg['f'] = $sender_id; // sender id
                        $apnMsg['n'] = $sender->UserName ; // sender name
                        $apnMsg['u'] = $receiver_id ; // receive id
                        $msgToPush = json_encode($apnMsg);
                        
                        $logData['UserID'] = $receiver_id;
                        $logData['CreatedDate'] = gmdate('Y-m-d H:i:s');
                        $logData['Platform'] = 'ios';
                        $logData['Message'] = $msgToPush;
                        $pushStatus = $this->apns->_pushMessage($logData, $msgToPush, $deviceObj->DeviceID, $this->Config_model->getConfig('PUSH_NOTIFICATION_MODE'));
                    }
                } 
            }
       }   
    }
    
    function send_match_message($sender, $receiver){
        if($sender && $receiver){
            $msg =  $this->lang->line('anomo_matched_us_msg');             
            
            $prefix = '';
            $is_production = PRODUCTION;
            if (!$is_production) {
                $prefix = 'dbg_';
            }
            $time = round(microtime(true) * 1000); 
            $receiver_id = $receiver->UserID;
            $sender_id = $sender->UserID;
            $msg_id = $sender_id.'_'.$receiver_id.'_'.$time;
            $aMsg = array(
                'time' => (string)$time,
                'text' => $msg,
                'receiver_id'   => (string)$receiver_id,
                'receiver_avatar' => $receiver->Avatar,
                'receiver_name' => $receiver->UserName,
                'sender_id' => (string)$sender_id,
                'sender_name'   => $sender->UserName,
                'sender_avatar' => $sender->Avatar,
                'type' => 'MESSAGE',
                'photo' => "",
                'message_id'    => (string)$msg_id,
                'eatReceipts' => '1',
                'fullscreen' => 0,
                'tap_action'    => 0,
                'tap_param' => (string)$sender_id
            );        
            $args['channel'] = $prefix.$receiver_id;
            $args['message'] = $aMsg;
            // send msg
            $response = $this->pubnub->publish($args);
            if($response && $response[0] == 1){//send success
               //push IOS notification        
                // get device's user
                $deviceObj = $this->User_model->getPushNotificationReceiverInfo($receiver_id);
                if ($deviceObj) {
                    // only push to IOS device
                    if ($deviceObj->Type == 'ios' && $deviceObj->AllowChatNotice == 1) {
                        if (APN_MSG_LENGTH < strlen($msg)) {
                            $msg = substr($msg, 0, APN_MSG_LENGTH) . '...';
                        }
                        // other badge count
                        $total = $deviceObj->Number;
                        // chat badge count
                        $total += $this->Chat_Count_model->getChatCountOfUser($receiver_id);
                        // increase #chat
                        $total += 1;
        
                        $sound = 'receiveText.aiff';
                        $apnMsg['aps']['alert'] = $msg;
                        $apnMsg['aps']['badge'] = (int) $total;
                        $apnMsg['aps']['sound'] = (string) $sound;
                        $apnMsg['AIM'] = "YES";
                        $apnMsg['t'] = 1;
                        $apnMsg['f'] = $sender_id; // sender id
                        $apnMsg['n'] = $sender->UserName ; // sender name
                        $apnMsg['u'] = $receiver_id ; // receive id
                        $msgToPush = json_encode($apnMsg);
                        
                        $logData['UserID'] = $receiver_id;
                        $logData['CreatedDate'] = gmdate('Y-m-d H:i:s');
                        $logData['Platform'] = 'ios';
                        $logData['Message'] = $msgToPush;
                        $pushStatus = $this->apns->_pushMessage($logData, $msgToPush, $deviceObj->DeviceID, $this->Config_model->getConfig('PUSH_NOTIFICATION_MODE'));
                    }
                } 
            }
       }   
    }
    
    function check_match($token = ''){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {                    
               $Previous = $this->User_Match_model->checkPreviousMatch($tokenInfo->UserID);
                if($Previous){                            
                    $time_left = self::check_time_left($tokenInfo->UserID,$Previous->CreatedDate,$Previous->FirstChatDate,$Previous->FirstReplyDate);                   
                    if($time_left>0){
                        //Get Previous User Info
                        $target_user_id = $Previous->TargetUserID;
                        $previous_user = $this->User_model->getUserDetail($target_user_id);
                        if ($previous_user){ 
                            $user_id = $tokenInfo->UserID;
                            $block_status = '';
                            if ($user_id != $target_user_id){
                                $block_status = $this->User_model->returnBlockStatus($user_id, $target_user_id);
                            }
                            //if ($block_status != '') {
                                //$response->code = $block_status;                    
                            //} else {
                            $is_block = 0;
                            $is_block_target = 0;
                            if($block_status == 'BLOCK'){
                                $is_block = 1;
                            }else if($block_status == 'BLOCK_TARGET'){
                                $is_block_target = 1;
                            }
                            $previous_user->IsBlock = $is_block;
                            $previous_user->IsBlockTarget = $is_block_target;
                            
                            $is_favorite = 0;
                            $is_reported = 0;
                            if ($user_id != $target_user_id){
                                $statusObj = $this->User_model->getStatusBtw2User($user_id, $target_user_id);
                                if ($statusObj){
                                    $is_favorite = (int)$statusObj->IsFavorite;
                                    $is_reported = (int)$statusObj->IsReported;
                                }
                            }
                            $previous_user->IsFavorite = $is_favorite;
                            $previous_user->IsReported = $is_reported;
                            // tag list
                            $previous_user->Tags = $this->User_model->getListTagOfMatchUser($tokenInfo->UserID,$target_user_id,0);                  
                            $response->previous = $previous_user;                         
                            //}
                        }
                        //return previous match
                        //Time left
                        $response->code = $this->lang->line('wait_for_next_match');
                        $response->timeleft = $time_left;                            
                    }else{                        
                        //If previous match > 12 hours
                        $ib_require = $this->Config_model->getConfig('MATCH_ICEBREAKER_REQUIRE');
                        if (!$ib_require){
                            $ib_require = 15;
                        }            
                        $total_ib  = $this->Anomotion_model->countAnomotionByUser($tokenInfo->UserID);            
                        if($total_ib < $ib_require){
                            //User must play Ice Breaker
                            $response->code = $this->lang->line('play_icebreaker');
                        }else{
                            //OK
                            $response->code = $this->lang->line('ok');
                        }
                    }
                }else{                    
                    //If there's no previous match
                    $ib_require = $this->Config_model->getConfig('MATCH_ICEBREAKER_REQUIRE');
                    if (!$ib_require){
                        $ib_require = 15;
                    }            
                    $total_ib  = $this->Anomotion_model->countAnomotionByUser($tokenInfo->UserID);            
                    if($total_ib < $ib_require){
                        //User must play Ice Breaker
                        $response->code = $this->lang->line('play_icebreaker');
                    }else{
                        //OK
                        $response->code = $this->lang->line('ok');
                    }
                }        
        }
        echo json_encode($response);
    }
    
    function get_match_info($token,$target_user_id){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {                    
           $Previous = $this->User_Match_model->getMatch($tokenInfo->UserID,$target_user_id);
            if($Previous){   
                //Get Previous User Info                
                $previous_user = $this->User_model->getUserDetail($target_user_id);
                if ($previous_user){ 
                    $user_id = $tokenInfo->UserID;
                    $block_status = '';
                    if ($user_id != $target_user_id){
                        $block_status = $this->User_model->returnBlockStatus($user_id, $target_user_id);
                    }
                    if ($block_status != '') {
                        $response->code = $block_status;                    
                    } else {
                        $is_block = 0;
                        $is_block_target = 0;
                        if($block_status == 'BLOCK'){
                            $is_block = 1;
                        }else if($block_status == 'BLOCK_TARGET'){
                            $is_block_target = 1;
                        }
                        $previous_user->IsBlock = $is_block;
                        $previous_user->IsBlockTarget = $is_block_target;
                        
                        $is_favorite = 0;
                        $is_reported = 0;
                        if ($user_id != $target_user_id){
                            $statusObj = $this->User_model->getStatusBtw2User($user_id, $target_user_id);
                            if ($statusObj){
                                $is_favorite = (int)$statusObj->IsFavorite;
                                $is_reported = (int)$statusObj->IsReported;
                            }
                        }
                        $previous_user->IsFavorite = $is_favorite;
                        $previous_user->IsReported = $is_reported;
                        // tag list
                        $aTags = $this->User_model->getListTagOfMatchUser($tokenInfo->UserID,$target_user_id,1);       
                        
                        // merge tag & major
                        // client will not handle to show common major
                        $aMajors = $this->User_model->getListMajorOfMatchUser($tokenInfo->UserID, $target_user_id, 1);
                        $previous_user->Tags  = array_merge($aTags, $aMajors);
                        $response->previous = $previous_user; 
                        $response->code = $this->lang->line('ok');
                    }
                }else{
                    $response->code = $this->lang->line('invalid_request');
                }
            }else{
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }
    
    function get_match_question($token,$target_user_id, $last_id = 0){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {                    
           $Previous = $this->User_Match_model->getMatch($tokenInfo->UserID,$target_user_id);
            if($Previous){                   
                if(!$last_id){
                    $last_id = 0;
                }
                $limit = 25;
                $questions = $this->User_model->getListQuestionOfMatchUser($tokenInfo->UserID,$target_user_id,$limit,$last_id);                
                $response->results = $questions; 
                $response->code = $this->lang->line('ok');
                
            }else{
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }
   
    function upload_match(){
        $path_source = FCPATH . $this->config->item('share_url');
        $isUpload = $this->s3upload->_uploadS3($path_source . 'anomo_matched_us.jpg', 'anomo_matched_us.jpg', 'image/jpeg');
        $isUpload_thumb = $this->s3upload->_uploadS3($path_source . 'anomo_matched_us.jpg', 'anomo_matched_us_thumb_100x150.jpg', 'image/jpeg');        
        echo $this->config->item('s3_photo_url') . (($isUpload) ? $isUpload : "")."<br/>";
        echo $this->config->item('s3_photo_url') . (($isUpload_thumb) ? $isUpload_thumb : "");         
       
    }
    
    function match_html($target_user_id) {
        if($target_user_id){
            $user = $this->User_model->getUserDetail($target_user_id);
            if ($user){  
                //Age
                $searcher_birthday = new DateTime($user->BirthDate);                
                $now = new DateTime('now');                
                $interval = $searcher_birthday->diff($now);                
                $age = $interval->y; 
                $user->Age = $age; 
                
                //Location
                if(!$user->NeighborhoodName){
                    $user->NeighborhoodName = "Earth";
                }                 
                $data['info'] = $user;                
            }
            $this->load->view('match_html', $data);            
        }        
    }
    
    function generate_match_img($user_id, $target_user_id, $isdebug = 0) {
        $time = time();
        $pdf_file_name      = $user_id . '_' . $target_user_id .'_'.$time.'.pdf';       
        $file_name          = $user_id . '_' . $target_user_id .'_'.$time.'.jpg';
        $file_name_thumb    = $user_id . '_' . $target_user_id .'_'.$time.'_thumb_100x150.jpg';         
        
        // 1. convert html -> pdf
        require_once(APPPATH . "libraries/dompdf/dompdf_config.inc.php");
        $dompdf = new DOMPDF();        
        // get html
        $html = base_url() . 'webservice/user/match_html/' . $target_user_id ;
        $dompdf->load_html_file($html);
        $dompdf->set_paper('640x960');
        $dompdf->render();        
        // save to pdf file
        // Format file name: AnomotionID_UserID.pdf
        $pdf = FCPATH . $this->config->item('share_url') . $pdf_file_name;
        file_put_contents($pdf, $dompdf->output());        
        // 2. convert pdf -> image
        $output = FCPATH . $this->config->item('share_url') . $file_name;
        try {
            $img = new Imagick($pdf . "[0]");

            $img->setImageCompression(Imagick::COMPRESSION_JPEG);
            $img->setImageCompressionQuality(50);            
            $img->setImageFormat("jpeg");         
            $img->cropImage(640,640,0,0);
            $img->writeImage($output);
        } catch (Exception $e) {
            //echo $e->getMessage();
        }        
        if (file_exists($output)) {            
            @unlink($pdf);                        
            $path_source = FCPATH . $this->config->item('share_url');

            $isUpload = $this->s3upload->_uploadS3($path_source . $file_name, $file_name, 'image/jpeg');
            $isThumb = $this->s3upload->_uploadS3($path_source . $file_name, $file_name_thumb, 'image/jpeg');
            if($isdebug == 1){
                echo $this->config->item('s3_photo_url') . (($isUpload) ? $isUpload : "");    
            }else{
                return $this->config->item('s3_photo_url') . (($isUpload) ? $isUpload : "");
            }
        }
        return "";
    }
    
    function check_time_left($user_id,$previous_date,$first_chat_date,$first_reply_date){
        $NUM_OF_FIRST_MATCH = $this->Config_model->getConfig('MATCH_NUM_OF_FIRST_MATCH');
        if (!$NUM_OF_FIRST_MATCH){
            $NUM_OF_FIRST_MATCH = 3;
        }
        
        $user_first_match = $this->User_Match_model->countUserMatch($user_id);        
        if($user_first_match < $NUM_OF_FIRST_MATCH){
            $FIRST_MATCH_LIMIT = $this->Config_model->getConfig('MATCH_FIRST_MATCH_LIMIT');
            if (!$FIRST_MATCH_LIMIT){
                $FIRST_MATCH_LIMIT = 0.16;
            }
            $CreatedDate = gmdate('Y-m-d H:i:s');                    
            $DateForm = strtotime($previous_date);                      
            $DateTo = strtotime($CreatedDate);                                                
            $second = $DateTo - $DateForm;  
            $timeleft = $FIRST_MATCH_LIMIT*3600-$second;
            return $timeleft;
        }else{
            $contest_time_left = self::check_contest_winner($user_id,$previous_date);               
            $no_response_time_left = self::check_no_response($user_id,$previous_date,$first_chat_date,$first_reply_date);        
            $NORMAL_LIMIT = $this->Config_model->getConfig('MATCH_REPEAT_LIMITATION');
            if (!$NORMAL_LIMIT){
                $NORMAL_LIMIT = 12;
            }
            $CreatedDate = gmdate('Y-m-d H:i:s');                    
            $DateForm = strtotime($previous_date);                      
            $DateTo = strtotime($CreatedDate);                                                
            $second = $DateTo - $DateForm;  
            $timeleft = $NORMAL_LIMIT*3600-$second;
            
            //prepare time left array
            $aTimeleft = array($timeleft);
            if($contest_time_left != -1){
                $aTimeleft[] = $contest_time_left;
            }
            if($no_response_time_left != -1){
                $aTimeleft[] = $no_response_time_left;
            }
            //return min
            return min($aTimeleft); 
        }       
    }         
    
    function check_contest_winner($user_id,$previous_date){        
        $WINNER_LIST = $this->Config_model->getConfig('USERID_CONTEST_WINNER');        
        if ($user_id && !empty($WINNER_LIST)){            
            $WINNER_LIST = trim($WINNER_LIST,',');            
            $WINNER_LIST = explode(",",$WINNER_LIST);            
            if(in_array($user_id,$WINNER_LIST)){                
                $START_DATE = $this->Config_model->getConfig('START_DATE_CONTEST_WINNER');
                $END_DATE = $this->Config_model->getConfig('END_DATE_CONTEST_WINNER');
                $TIME_LIMIT = $this->Config_model->getConfig('MATCH_REPEAT_LIMITATION_CONTEST_WINNER');
                if($START_DATE && $END_DATE && $TIME_LIMIT){
                    $START_DATE = strtotime($START_DATE." 00:00:00");                    
                    $END_DATE = strtotime($END_DATE. " 23:59:59");                    
                    $CURRENT_DATE = strtotime(gmdate('Y-m-d H:i:s', strtotime("- 7 hour")));                    
                     if($START_DATE <= $CURRENT_DATE && $CURRENT_DATE <= $END_DATE){
                        $CreatedDate = gmdate('Y-m-d H:i:s');                    
                        $DateForm = strtotime($previous_date);                      
                        $DateTo = strtotime($CreatedDate);                                                
                        $second = $DateTo - $DateForm;
                        $timeleft = $TIME_LIMIT*3600-$second;                                        
                        if($timeleft > 0){
                            return (int)$timeleft;
                        }else{
                            return 0;
                        }   
                    }
                }                
            }            
        }
        return -1;
    }
    
    function check_no_response($user_id,$previous_date,$first_chat_date,$first_reply_date){
        $NO_RESPONSE_LIMIT = $this->Config_model->getConfig('MATCH_NO_RESPONSE_LIMIT');
        if (!$NO_RESPONSE_LIMIT){
            $NO_RESPONSE_LIMIT = 3;
        }
        
        if($first_chat_date && ($first_reply_date == null || strtotime($first_reply_date) == 0)){
            $CreatedDate = gmdate('Y-m-d H:i:s');                    
            $DateForm = strtotime($first_chat_date);                      
            $DateTo = strtotime($CreatedDate);                                                
            $second = $DateTo - $DateForm;  
            $timeleft = $NO_RESPONSE_LIMIT*3600-$second;                                                 
            if($timeleft > 0){
                return  (int)$timeleft;
            }else{
                return 0;
            }  
        }else if($first_chat_date && $first_reply_date){                         
            $DateForm = strtotime($first_chat_date);                      
            $DateTo = strtotime($first_reply_date);                                                
            $timer = $DateTo - $DateForm;  
            $limit = $NO_RESPONSE_LIMIT*3600;                                                 
            if($timer > $limit ){
                return  0;
            }else{
                return -1;
            }  
        }
        return -1;
        
    }
    
    function get_user_tag($token = ''){
        $response = new stdClass();
        if (empty($token)) {
            $response->code = $this->lang->line('invalid_parameter');
        } else {
            $tokenInfo = $this->Wstoken_model->getToken($token);
            if (!$tokenInfo) {
                $response->code = $this->lang->line('invalid_token');
            } else {
                $response->Tags = $this->User_model->getListTagOf($tokenInfo->UserID);
                $response->code = $this->lang->line('ok');
            }
        }
        echo json_encode($response);
    }
    
    function verify_email_code ($token = ''){
        $response = new stdClass();
        $email_code = isset($_POST['EmailCode'])?trim(strtolower($_POST['EmailCode'])):'';
        $email = isset($_POST['Email'])?trim(strtolower($_POST['Email'])):'';
        if (empty($token) || empty($email_code)) {
            $response->code = $this->lang->line('invalid_code');
        } else {
            $tokenInfo = $this->Wstoken_model->getToken($token);
            if (!$tokenInfo) {
                $response->code = $this->lang->line('invalid_token');
            } else {
                // ANOMO-12783 Allow user to type in "skip" for the verification email address
                if ($email_code == SKIP && $email == SKIP){
                    // verify this account
                    $updated = $this->User_model->updateUser(array('IsEmailVerify' => 1), array('UserID' => $tokenInfo->UserID), $token);
                    if ($updated){
                        $response->code = $this->lang->line('ok');
                    }else{
                        $response->code = $this->lang->line('failed');
                    }
                }else{
                    $email = $tokenInfo->Email;
                    $emailCodeObj = $this->Promo_Code_model->validateEmailCode($email, $email_code);
                    if (!$emailCodeObj){
                        $response->code = $this->lang->line('invalid_code');
                    }else{
                        $this->User_model->updateUser(array('IsEmailVerify' => 1), array('UserID' => $tokenInfo->UserID));
                        $this->Promo_Code_model->updateEmailCode(array('Status' => 1, 'VerifiedDate' => gmdate('Y-m-d H:i:s')), array('Email' => $email, 'Code' => $email_code));
                        $response->code = $this->lang->line('ok');
                    }
                }
            }
        }
        echo json_encode($response);
    }
    
    function check_email_verification_status($token = ''){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);        
        if (!$tokenInfo) {        
            $response->code = $this->lang->line('invalid_token');
        } else {
           $needVerify = "0";
           if ($tokenInfo->IsEmailVerify == 0){
               $emailValidationInterval = $this->Config_model->getConfig('EmailValidationInterval'); // hours
               if (!$emailValidationInterval){
                   $emailValidationInterval = 72; // 3days
               }
               $emailValidationInterval = $emailValidationInterval * 3600;
               if (strtotime("+ $emailValidationInterval second", strtotime($tokenInfo->SignUpDate)) < strtotime(gmdate('Y-m-d H:i:s'))){
                   $needVerify = "1";
               }
           }
           $response->code = $this->lang->line('ok');
           $response->NeedVerify = $needVerify;
           $response->IsEmailVerify = $tokenInfo->IsEmailVerify;
        }          
        echo json_encode($response);
    }
    
    /**
     * return list interests
     *
     */
    function list_major(){
        $response = new stdClass();
        $response->ListMajor = $this->Major_model->getListMajors();
        $response->code = $this->lang->line('ok');
        echo json_encode($response);
    }
    
    /*
     * Update user's email
     */
    function update_email($token = '') {
        $response = new stdClass();
        $email = isset($_POST['Email']) ? trim(strtolower($_POST['Email'])) : '';
        if (empty($email)) {
            $response->code = $this->lang->line('invalid_email');
        } else {
            $tokenInfo = $this->Wstoken_model->getToken($token);
            if (!$tokenInfo) {
                $response->code = $this->lang->line('invalid_token');
            } else {
                $user_id = $tokenInfo->UserID;

                // ANOMO-12783 Allow user to type in "skip" for the verification email address
                if ($email == SKIP) {
                    // ignore this step
                    $response->code = $this->lang->line('ok');
                } else {
                    if (!$this->commonlib->validateEmail($email)) {
                        $response->code = $this->lang->line('invalid_email');
                    } else {
                        // if this email is existed in db
                        if ($this->User_model->checkExistedEmail($email, $user_id) > 0) {
                            $response->code = $this->lang->line('email_existed');
                        } else {
                            // processing....
                            // 1. update user's email
                            $updated = $this->User_model->updateUser(array('Email' => $email), array('UserID' => $user_id), $token);
                            if ($updated) { // successful 
                                $code = false;
                                // 2. send EmailCode to user's email
                                $codeExistObj = $this->Promo_Code_model->getEmailCode($email);
                                if ($codeExistObj) {
                                    $code = $codeExistObj->Code;
                                } else {
                                    // generate code
                                    $code = rand(10000, 99999);

                                    // insert into email_verification
                                    $value = array(
                                        'Email' => $email,
                                        'Code' => $code,
                                        'CreatedDate' => gmdate('Y-m-d H:i:s')
                                    );
                                    $this->Promo_Code_model->addEmailCode($value);
                                }
                                if ($code) {
                                    // send email 
                                    $this->load->library('email');
                                    $this->email->from($this->config->item('email_from'), $this->config->item('email_from_name'));
                                    $this->email->to($email);
                                    $this->email->subject("[UW Social] Verification Email");

                                    $msg = "$code is your verification code.";
                                    $this->email->message($msg);
                                    $sent = $this->email->send();
                                    if (!$sent) {
                                        // try again
                                        $sent = $this->email->send();
                                    }
                                }
                                $response->code = $this->lang->line('ok');
                            } else {
                                // incase error 
                                $response->code = $this->lang->line('failed');
                            }
                        }
                    }
                }
            }
        }

        echo json_encode($response);
    }
}
?>