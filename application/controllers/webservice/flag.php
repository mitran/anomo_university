<?php
class Flag extends Ext_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Flag_model');
        $this->load->model('User_model');
        $this->load->model('Wstoken_model');
        $this->load->model('Activity_model');
        $this->load->model('Flag_Content_model');
        $this->load->model('Comment_model');
        $this->lang->load('ws');
    }

    /**
     * report user
     *
     * @param string $token
     * @param int $to_user_id
     */
    function add($token = '', $to_user_id = 0){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!is_numeric($to_user_id)){
                $response->code = $this->lang->line('invalid_request');
            }else{
                $toUserInfo = $this->User_model->getUserInfo($to_user_id);
                if (!$toUserInfo){
                    $response->code = $this->lang->line('invalid_request');
                }else{
                    $isAddFlag = $this->Flag_model->isAddFlag($tokenInfo->UserID, $to_user_id);
                    if ($isAddFlag){
                        $response->code = $this->lang->line('flag_added');
                    }else{
                        $createdDate = gmdate('Y-m-d H:i:s');
                        $data = array(
                            'FromUserID'	=> $tokenInfo->UserID,
                            'ToUserID'		=> $to_user_id,
                            'Content'		=> isset($_POST['Content'])?trim($_POST['Content']):"",
                            'CreatedDate'	=> $createdDate
                        );
                        $add = $this->Flag_model->add($data);
                        if (!$add){
                            $response->code = $this->lang->line('failed');
                        }else{

                            // send email to support@anomo.com
                            $this->load->library('email');
                            $this->email->from($this->config->item('email_from'), $this->config->item('email_from_name'));
                            $this->email->to($this->config->item('report_user_to_email'));
                            $this->email->subject("[UW Social] Report user");
                            $reason = isset($_POST['Content'])?trim($_POST['Content']):"";
                            $msg = "$toUserInfo->UserName was reported with reason: $reason at $createdDate by $tokenInfo->UserName";
                            $this->email->message($msg);
                            $sent = $this->email->send();
                            $this->commonlib->dynaLog('flag', 'flag user', $sent);
                            $response->code = $this->lang->line('ok');
                        }
                    }
                }
            }
        }
        echo json_encode($response);exit();
    }

    /**
     * report content / comment of post
     * 
     *
     * @param string $token
     * @param int $content_id
     * @param int $type
     * @param int $is_comment [0: content/ 1: comment]
     */
    function content($token = '', $content_id = 0, $type = '', $is_comment = 0){
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $validTypes = array(ACTIVITY_POST_STATUS, ACTIVITY_POST_PICTURE, ACTIVITY_PROMOS, ACTIVITY_POST_VIDEO);
            if (!is_numeric($content_id) || !in_array($type, $validTypes)){
                $response->code = $this->lang->line('invalid_request');
            }else{
                if ($is_comment == 0){
                    $contentInfo = $this->Activity_model->getContentInfo($content_id, $type);
                }else{
                    $contentInfo = $this->Comment_model->getCommentDetail($content_id, $type);
                }
                if (!$contentInfo){
                    $response->code = $this->lang->line('invalid_request');
                }else{
                    // 2014-July-31 ANOMO-10294 
                    $isInsertDb = 1;
                    if ($is_comment == 0 && $type != ACTIVITY_PROMOS){ // don't hidden if is Promos post
                        $isReport = $this->Flag_Content_model->isReport($content_id, $type, 0);
                        if ($isReport){
                            // if admin already forgive - do nothing
                            if ($isReport[0]->IsForgiven != 1){
                                $numberReport = sizeof($isReport);
                                if ($numberReport >= 2){
                                    // auto hidden this content on feed
                                    $this->Activity_model->update(array('IsInvalid' => 1), array('RefID' => $content_id, 'Type' => $type), $contentInfo->UserID);
                                    $isInsertDb = 1;
                                }
                            }else{
                                $isInsertDb = 0;
                            }
                        }
                    }
                    if (!$isInsertDb){
                        $response->code = $this->lang->line('ok');
                    }else{
                        $isAddFlag = $this->Flag_Content_model->isAddFlag($tokenInfo->UserID, $content_id, $type, $is_comment);
                        if ($isAddFlag){
                            $response->code = $this->lang->line('flag_added');
                        }else{
                            $createdDate = gmdate('Y-m-d H:i:s');
                            $content = isset($_POST['Content'])?trim($_POST['Content']):"";
                            $data = array(
                                'UserID'		=> $tokenInfo->UserID,
                                'ContentID'		=> $content_id,
                                'ContentOwnerID'    => $contentInfo->UserID,
                                'Type'		=> $type,
                                'Content'		=> $content,
                                'CreatedDate'	=> $createdDate,
                                'IsComment'         => $is_comment
                            );
                            $add = $this->Flag_Content_model->add($data);
                            if (!$add){
                                $response->code = $this->lang->line('failed');
                            }else{
                                // send email to support@anomo.com
                                $this->load->library('email');
                                $this->email->from($this->config->item('email_from'), $this->config->item('email_from_name'));
                                $this->email->to($this->config->item('report_user_to_email'));
                                $this->email->subject("[UW Social] Report content");
                                $reason = isset($_POST['Content'])?trim($_POST['Content']):"";
                                $msg = "$contentInfo->UserName 's activity feed item <$content_id> was reported with reason: $reason at $createdDate by {$tokenInfo->UserName}";
                                $this->email->message($msg);
                                $sent = $this->email->send();
                                $this->commonlib->dynaLog('flag', 'flag content', $sent);
                                $response->code = $this->lang->line('ok');
                            }
                        }
                    }
                }
            }
        }
        echo json_encode($response);exit();
    }
}
?>