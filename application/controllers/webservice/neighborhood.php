<?php
class Neighborhood extends Ext_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Neighborhood_model');
        $this->load->model('Neighborhood_Comment_model');
        $this->load->model('Neighborhood_Post_model');
        $this->load->model('Neighborhood_Activity_model');
        $this->load->model('Place_Activity_model');
        $this->load->model('Config_model');
        $this->load->model('Notification_History_model');
        $this->load->model('Neighborhood_User_Chat_Messages_model');
        $this->load->model('Place_Category_model');
        $this->load->model('Place_model');
        $this->load->model('Push_model');
        $this->load->model('Image_model');
        $this->load->model('Activity_model');
        $this->load->model('Wstoken_model');
        $this->load->model('Hashtag_model');
        $this->lang->load('ws');
        $this->load->library('S3Upload');

        $config['upload_path'] = './public/upload/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = PHOTO_SIZE_MAX;
        $config['encrypt_name'] = TRUE;
        $config['remove_spaces'] = TRUE;
        $this->load->library('upload', $config);
    }

    function get_neighborhood_by_coordinates($token = '', $lat = 0, $lon = 0)
    {
        $response = new stdClass();
        if (!$this->commonlib->checkToken($token)) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($lat) && is_numeric($lon)) {
                $neighborhood = $this->Neighborhood_model->find_by_coordinates($lat, $lon);
                if ($neighborhood) {
                    $results = get_object_vars($neighborhood);
                    $response->results = $results;
                    $response->code = $this->lang->line('ok');
                } else {
                    $response->code = $this->lang->line('zero_results');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

   
    /**
     * V2
     *
     * @param unknown_type $token
     * @param unknown_type $user_id
     * @param unknown_type $neighborhood_id
     */
    function get_detail($token = '', $user_id = 0, $neighborhood_id = 0)
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($neighborhood_id)) {

                $neighborhood = $this->Neighborhood_model->getInfo($neighborhood_id);
                if ( $neighborhood) {
                    // list tags
                    // ANOMO-5917 - limit tag
                    $tagList = $this->Neighborhood_model->getListTagOf($neighborhood_id, 'current', 100);
                    $number_text_post = $this->Neighborhood_Post_model->getTotalPostInNbh($neighborhood_id, 'text');
                    $number_picture_post = $this->Neighborhood_Post_model->getTotalPostInNbh($neighborhood_id, 'picture');

                    $ret = array(
                        'PlaceID' => $neighborhood->OGR_FID,
                        'Name' => $neighborhood->NAME,
                        'Address' => $neighborhood->COUNTY,
                        'Lat' => '',
                        'Lon' => '',
                        'Category' => $this->Place_Category_model->returnListCategoryByName('neighborhood'),
                        'Phone' => '',
                        'CheckInStatus' => $this->Neighborhood_model->getCheckInStatus($user_id, $neighborhood_id),
                        'ListTag' => $tagList,
                        'ListAnomo' => array(), // not use
                        'NumberAnomo' => 0, // not use
                        'NumberAnomoPast' => 0, // not use
                        'ListTextPost' => array(), // not use
                        'NumberTextPost' => $number_text_post,
                        'ListPicturePost' => array(), // not use
                        'NumberPicturePost' => $number_picture_post,
                        'NumberTagPast' => 0, // not use
                        'NumberOfUserCheckedIn' => $this->Neighborhood_model->getNumberUserCheckedIn($neighborhood_id)

                    );
                    $response->results = $ret;
                    $response->code = $this->lang->line('ok');
                } else {
                    $response->code = $this->lang->line('invalid_request');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }

    /**
     * post msg/picture
     *
     * @param string $token
     * @param int $user_id
     * @param int $nbh_id
     * @return json
     */
    function post_to_neighborhood($token = '', $user_id = '', $nbh_id = '')
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            // check PlaceID and UserID
            $user_id = $tokenInfo->UserID;
            $placeInfo = $this->Neighborhood_model->getInfo($nbh_id);

            if (((isset($_POST['Content']) && trim($_POST['Content']) != '') || (isset($_FILES['Photo']) && $_FILES['Photo']['size'] > 0)) && $placeInfo) {
                // post picture
                $error = false;
                $photo = '';
                $photo100 = '';
                $photo200 = '';
                $photo300 = '';
                if (isset($_FILES['Photo']) && $_FILES['Photo']['size'] > 0) {
                    $isUpload = $this->s3upload->upload('Photo');
                    if (!$isUpload) {
                        $response->code = $this->lang->line('invalid_image');
                        $response->description = $this->s3upload->error;
                        $error = true;
                    } else {
                        $photo = $isUpload;
                        $p100 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 100, 150);
                        $photo100 = ($p100) ? $p100 : "";
                        $p200 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 200, 300);
                        $photo200 = ($p200) ? $p200 : "";
                        $p300 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 300, 450);
                        $photo300 = ($p300) ? $p300 : "";
                    }
                }

                if (!$error) {
                    // add to PlacePost tbl
                    $data = array(
                        'UserID' => $user_id,
                        'NeighborhoodID' => $nbh_id,
                        'Content' => $_POST['Content'],
                        'Photo' => $photo,
                        'Photo100' => $photo100,
                        'Photo200' => $photo200,
                        'Photo300' => $photo300,
                        'CreatedDate' => gmdate('Y-m-d H:i:s')
                    );
                    $id = $this->Neighborhood_Post_model->add($data);
                    if ($id) {
                        $data = array(
                            'UserID' => $user_id,
                            'Type' => ($photo === '') ? 9 : 10,
                            'PlaceID' => $nbh_id,
                            'RefID' => $id,
                            'CreatedDate' => gmdate('Y-m-d H:i:s')
                        );
                        $this->Place_Activity_model->add($data);

                        $postInfo = $this->Neighborhood_Post_model->getInfo($id);
                        if ($postInfo->Photo) {
                            $postInfo->Photo = $this->config->item('s3_photo_url') . $postInfo->Photo;
                        }

                        if ($postInfo->Photo100) {
                            $postInfo->Photo100 = $this->config->item('s3_photo_url') . $postInfo->Photo100;
                        }
                        if ($postInfo->Photo200) {
                            $postInfo->Photo200 = $this->config->item('s3_photo_url') . $postInfo->Photo200;
                        }
                        if ($postInfo->Photo300) {
                            $postInfo->Photo300 = $this->config->item('s3_photo_url') . $postInfo->Photo300;
                        }

                        if ($postInfo->Avatar) {
                            $postInfo->Avatar = $this->config->item('s3_photo_url') . $postInfo->Avatar;
                        }
                        $postInfo->PlaceID = $postInfo->NeighborhoodID;
                        unset($postInfo->NeighborhoodID);
                        $response->post = $postInfo;

                        // publishing a feed
                        // ANOMO-7288 - Show the location of the activity post
                        $userLocationObj = $this->User_model->getCurrentLocationOfUser($user_id);
                            
                        if ($this->Config_model->getConfig('ACTIVITY_FEED') == 'on') {
                            $feed = array(
                                'FromUserID' => $user_id,
                                'FromUserName' => $tokenInfo->UserName,
                                'Avatar' => $tokenInfo->Avatar,
                                'Type' => ($photo == '') ? 8 : 7,
                                'ActionType' => ($photo == '') ? 8 : 7,
                                'Message' => isset($_POST['Content']) ? $_POST['Content'] : "",
                                'Image' => $photo,
                                'RefID' => $id,
                                'PlaceID' => $nbh_id,
                                'PlaceName' => $placeInfo->NAME,
                                'PlaceIcon' => $this->Place_Category_model->getIconByName('neighborhood'),
                                'PlaceAddress' => $placeInfo->NAME,
                                'Lat' => $tokenInfo->Lat,
                                'Lon' => $tokenInfo->Lon,
                                'BirthDate' => $tokenInfo->BirthDate,
                                'Gender' => $tokenInfo->Gender,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                                ,'CheckinPlaceID'  => $userLocationObj?$userLocationObj->PlaceID:''
                                ,'CheckinPlaceName'    => $userLocationObj?$userLocationObj->PlaceName:''
                                ,'NeighborhoodID' => $userLocationObj?$userLocationObj->NeighborhoodID:''
                            );
                            $feed_id = $this->Activity_model->add($feed);
                            if ($feed_id){
                                // ANOMO-7852 add hashtag
                                $text = isset($_POST['Content']) ? $_POST['Content'] : '';
                                $this->Hashtag_model->add($text, $feed_id);
                            }
                        }
                        // end publishing a feed

                        $response->code = $this->lang->line('ok');
                    } else {
                        $response->code = $this->lang->line('db_error');
                    }
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * comment function
     *
     * @param string $token
     * @param int $user_id
     * @param int $post_id
     * @return  json
     */
    function post_comment($token = '', $user_id = '', $post_id = '')
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!isset($_POST['Content']) || !trim($_POST['Content']) != '') {
                $response->code = $this->lang->line('invalid_request');
            } else {
                // check PostID and UserID
                $user_id = $tokenInfo->UserID;
                $postInfo = $this->Neighborhood_Post_model->getInfo($post_id);
                if ( !$postInfo) {
                    $response->code = $this->lang->line('invalid_parameter');
                } else {
                    $target_user_id = $postInfo->UserID;
                    //check block
                    if ($this->User_model->checkBlock($user_id, $target_user_id)) {
                        $response->code = $this->lang->line('target_user_blocked');
                    } elseif ($this->User_model->checkBlock($target_user_id, $user_id)) {
                        $response->code = $this->lang->line('blocked');
                    } else {
                        $data = array(
                            'UserID' => $user_id,
                            'NeighborhoodPostID' => $post_id,
                            'Content' => $_POST['Content'],
                            'CreatedDate' => gmdate('Y-m-d H:i:s')
                        );

                        $id = $this->Neighborhood_Comment_model->add($data);
                        if ($id) {
                            $contentType = ($postInfo->Photo != '') ? 7 : 8;
                            $this->Activity_model->increase($post_id, $contentType, 'comment');

                            $data = array(
                                'UserID' => $user_id,
                                'PlaceID' => $postInfo->NeighborhoodID,
                                'Type' => 11,
                                'RefID' => $id,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                            );
                            $this->Place_Activity_model->add($data);
                           
                            if ($target_user_id !== $user_id) {
                                $placeInfo = $this->Neighborhood_model->getInfo($postInfo->NeighborhoodID);
                                $placeName = $placeInfo->NAME;
                                
                                //add push notification
                                $pushData = array(
                                    'UserID' => $target_user_id,
                                    'SendUserID' => $user_id,
                                    'Type' => ($postInfo->Photo) ? 11 : 12, // comment photo to neighborhood
                                    'RefID' => $id,
                                    'IsRead' => 1,
                                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                                );
                                $this->Notification_History_model->add($pushData);

                                $targetUserInfo = $this->User_model->getUserInfo($target_user_id);

                                if (isset($postInfo->Photo) && !empty($postInfo->Photo)) {
                                    if ($targetUserInfo->AllowCommentPicturePostNotice) {
                                        self::_pushNotify($target_user_id, 1, $user_id, $this->lang->line('comment_post'), $placeName);
                                    }
                                } else {
                                    if ($targetUserInfo->AllowCommentTextPostNotice) {
                                        self::_pushNotify($target_user_id, 2, $user_id, $this->lang->line('comment_post'), $placeName);

                                    }
                                }
                            }

                            // output
                            $commentInfo = new stdClass();
                            $commentInfo->ID = $id;
                            $commentInfo->Content = $_POST['Content'];
                            $commentInfo->UserID = $tokenInfo->UserID;
                            $commentInfo->UserName = $tokenInfo->UserName;
                            $commentInfo->Gender = $tokenInfo->Gender;
                            $commentInfo->CreatedDate = gmdate('Y-m-d H:i:s');
                            $commentInfo->Avatar = ($tokenInfo->Avatar)?$this->config->item('s3_photo_url') . $tokenInfo->Avatar:"";
                            
                            $response->comment = $commentInfo;
                            $response->code = $this->lang->line('ok');
                        } else {
                            $response->code = $this->lang->line('db_error');
                        }
                    }
                }
            }
        }

        echo json_encode($response);
    }

    /**
     * get list comment of post
     *
     * @param string $token
     * @param int $post_id
     * @return json
     */
    function get_comment($token = '', $post_id = 0)
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $post = $this->Neighborhood_Post_model->findById($post_id);
            if ($post ) {
                // list comment of post
                $comments = array();
                $list_comment = $this->Neighborhood_Comment_model->getListCommentOf($post_id);
                foreach ($list_comment as $comment) {
                    if ($comment->Avatar) {
                        $comment->Avatar = $this->config->item('s3_photo_url') . $comment->Avatar;
                    }
//                    $comment->ConnectMeter = $this->User_model->getConnectMeter($comment->UserID, $tokenInfo->UserID);
                    $comment->ConnectMeter = 0; // not use
                    $comments[] = get_object_vars($comment);
                }
                $response->Total = (int)count($list_comment);
                $response->result = $comments;
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }

        echo json_encode($response);
    }

    /**
     * get picture wall
     *
     * @param unknown_type $token
     * @param unknown_type $nbh_id
     * @return json
     */
    function get_picture_wall_v2($token = '', $nbh_id = 0, $page = 1)
    {
        $response = new stdClass();
        if (!$this->commonlib->checkToken($token)) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $placeInfo = $this->Neighborhood_model->getInfo($nbh_id);
            if ($placeInfo && is_numeric($page)) {
                // list text post
                $limit = 18;
                $offset = ($page - 1) * $limit;
                $total = 0;
                $list_text_post = $this->Neighborhood_Post_model->getListPostOf($nbh_id, 'picture', $total, $offset, $limit);
                $text_posts = array();
                foreach ($list_text_post as $post) {
                    if ($post->Photo) {
                        $post->Photo = $this->config->item('s3_photo_url') . $post->Photo;
                    }
                    if ($post->Photo100) {
                        $post->Photo100 = $this->config->item('s3_photo_url') . $post->Photo100;
                    }
                    if ($post->Photo200) {
                        $post->Photo200 = $this->config->item('s3_photo_url') . $post->Photo200;
                    }
                    if ($post->Photo300) {
                        $post->Photo300 = $this->config->item('s3_photo_url') . $post->Photo300;
                    }
                    if ($post->Avatar) {
                        $post->Avatar = $this->config->item('s3_photo_url') . $post->Avatar;
                    }
//                    $post->TotalComment = $this->Neighborhood_Comment_model->countCommentOf($post->ID);
//                    unset($post->Gender);
                    $text_posts[] = get_object_vars($post);
                }
                $results = new stdClass();

                $results->TotalPage = ceil($total / $limit);
                $results->CurrentPage = $page;

                $results->Posts = $text_posts;
                $response->results = $results;
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }

        echo json_encode($response);
    }

    /**
     * get wall post
     *
     * @param string $token
     * @param int $nbh_id
     * @return json
     */
    function get_wall_post_v2($token = '', $nbh_id = 0, $page = 1)
    {
        $response = new stdClass();
        if (!$this->commonlib->checkToken($token)) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $placeInfo = $this->Neighborhood_model->getInfo($nbh_id);
            if ($placeInfo && is_numeric($page)) {
                // list text post
                $limit = 12;
                $offset = ($page - 1) * $limit;
                $total = 0;
                $list_text_post = $this->Neighborhood_Post_model->getListPostOf($nbh_id, 'all', $total, $offset, $limit);
                $text_posts = array();
                foreach ($list_text_post as $post) {
                    if ($post->Photo) {
                        $post->Photo = $this->config->item('s3_photo_url') . $post->Photo;
                    }
                    if ($post->Photo100) {
                        $post->Photo100 = $this->config->item('s3_photo_url') . $post->Photo100;
                    }
                    if ($post->Photo200) {
                        $post->Photo200 = $this->config->item('s3_photo_url') . $post->Photo200;
                    }
                    if ($post->Photo300) {
                        $post->Photo300 = $this->config->item('s3_photo_url') . $post->Photo300;
                    }
                    if ($post->Avatar) {
                        $post->Avatar = $this->config->item('s3_photo_url') . $post->Avatar;
                    }

                    // list comment of post
//                    $post->TotalComment = $this->Neighborhood_Comment_model->countCommentOf($post->ID);
//                    unset($post->Gender);
                    $text_posts[] = get_object_vars($post);
                }

                $response->TotalPage = ceil($total / $limit);
                $response->CurrentPage = $page;

                $response->WallPostList = $text_posts;
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }

        echo json_encode($response);
    }

    /**
     * get anomo
     *
     * @param string $token
     * @param int $user_id
     * @param int $nbh_id
     * @return json
     */
    function get_anomos_v2($token = '', $user_id = 0, $nbh_id = 0, $page = 1)
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if ( !is_numeric($page)) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $placeInfo = $this->Neighborhood_model->getInfo($nbh_id);
                if ( !$placeInfo) {
                    $response->code = $this->lang->line('invalid_parameter');
                } else {
                    // list user checked in / spectate
                    $limit = 12;
                    $offset = ($page - 1) * $limit;
                    $total = 0;
                    $user_checkedin = array();
                    $list_checkedin = $this->Neighborhood_model->getListUserOf($nbh_id, 0, 'current', $total, $offset, $limit);
                    if ($list_checkedin) {
                        foreach ($list_checkedin as $entry) {
                            if ($entry->Avatar) {
                                $entry->Avatar = $this->config->item('s3_photo_url') . $entry->Avatar;
                            }
                            $status = 0;
                            $latestCheckin = $this->User_model->getLatestCheckin($entry->UserID);
                            if ($latestCheckin && $latestCheckin[0]->Status == 1) {
                                $status = 1;
                            }
                            $entry->Status = $status;
                            $entry->Tags = $this->User_model->getListTagOf($entry->UserID);
                            //
                            $datesearch = date('Y-m-d H:i:s', strtotime(US_USER_ACTIVE_TIME . ' hour', strtotime(gmdate('Y-m-d H:i:s'))));
                            $entry->IsOnline = ($entry->LastActivity >= $datesearch) ? 1 : 0;

                            unset($entry->Email);
                            unset($entry->CheckInDate);
                            $facebokID = ($entry->FacebookID) ? $entry->FacebookID : '';
                            $entry->FacebookID = $facebokID;

                            // return place id, place name
                            $checkinPlaceId = '';
                            $checkinPlaceName = '';
                            $checkinStatus = 0;
                            $placeCheckIn = $this->Place_model->getUserCheckinStatus($entry->UserID);

                            if ($placeCheckIn) {
                                $checkinPlaceId = $placeCheckIn->PlaceID;
                                $checkinPlaceName = $placeCheckIn->Name;
                                $checkinStatus = $placeCheckIn->Status;
                            }
                            $entry->CheckinPlaceID = $checkinPlaceId;
                            $entry->CheckinPlaceName = $checkinPlaceName;
                            $entry->CheckinStatus = $checkinStatus;
                            //
                            $user_checkedin[] = get_object_vars($entry);
                        }
                    }
                    $response->TotalPage = ceil($total / $limit);
                    $response->CurrentPage = $page;
                    $response->ListUser = $user_checkedin;
                    $response->code = $this->lang->line('ok');
                }
            }
        }

        echo json_encode($response);
    }

    /**
     * send msg
     *
     * @param string $token
     * @param int $user_id
     * @param int $nbh_id
     * @return json
     */
    function send_message($token = '', $user_id = 0, $nbh_id = '')
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $placeInfo = $this->Neighborhood_model->getInfo($nbh_id);

            $isUpPhoto = isset($_FILES['Photo']) && $_FILES['Photo']['size'] > 0;
            $errorContent = $isUpPhoto || (isset($_POST['Content']) && trim($_POST['Content']) != '');

            if ($errorContent && $placeInfo) {
                $photo = '';
                $photo100 = '';
                $photo200 = '';
                $photo300 = '';
                $error = false;
                if ($isUpPhoto) {
                    $isUpload = $this->s3upload->upload('Photo');
                    if (!$isUpload) {
                        $response->code = $this->lang->line('invalid_image');
                        $response->description = $this->s3upload->error;
                        $error = true;
                    } else {
                        $photo = $isUpload;
                        $p100 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 100, 150);
                        $photo100 = ($p100) ? $p100 : "";
                        $p200 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 200, 300);
                        $photo200 = ($p200) ? $p200 : "";
                        $p300 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 300, 450);
                        $photo300 = ($p300) ? $p300 : "";
                    }
                }

                if (!$error) {
                    $createdDate = gmdate('Y-m-d H:i:s');
                    $data = array(
                        'NeighborhoodID' => $nbh_id,
                        'UserID' => $user_id,
                        'Content' => isset($_POST['Content']) ? $_POST['Content'] : '',
                        'Photo' => $photo,
                        'Photo100' => $photo100,
                        'Photo200' => $photo200,
                        'Photo300' => $photo300,
                        'CreatedDate' => $createdDate
                    );
                    $id = $this->Neighborhood_User_Chat_Messages_model->add($data);
                    if ($id) {
                        $data['ID'] = $id;
                        if ($isUpPhoto) {
                            $data['Photo'] = $this->config->item('s3_photo_url') . $data['Photo'];
                            $data['Photo100'] = $this->config->item('s3_photo_url') . $data['Photo100'];
                            $data['Photo200'] = $this->config->item('s3_photo_url') . $data['Photo200'];
                            $data['Photo300'] = $this->config->item('s3_photo_url') . $data['Photo300'];
                        } else {
                            $data['Photo'] = "";
                            $data['Photo100'] = "";
                            $data['Photo200'] = "";
                            $data['Photo300'] = "";
                        }

                        $response->results = $data;
                        $response->code = $this->lang->line('ok');
                    }
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }

        echo json_encode($response);
    }

    /**
     * get msg
     *
     * @param string $token
     * @param int $user_id
     * @param int $nbh_id
     * @param int $message_id
     * @param int $position_flag
     * @return json
     */
    function get_messages($token = '', $user_id = 0, $nbh_id = '', $message_id = -1, $position_flag = 2)
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {

            $placeInfo = $this->Neighborhood_model->getInfo($nbh_id);
            if ( $placeInfo) {
                $reverse = false;
                if ($position_flag == 1) {
                    $reverse = true;
                }
                $messages = $this->Neighborhood_User_Chat_Messages_model->find50MessagesbyMessageID($nbh_id, $message_id, $reverse, $user_id);
                if ($messages) {
                    foreach ($messages as $msg) {
                        $msg->Avatar = $this->config->item('s3_photo_url') . $msg->Avatar;
                        if ($msg->Photo) {
                            $msg->Photo = $this->config->item('s3_photo_url') . $msg->Photo;
                        }
                        if ($msg->Photo100) {
                            $msg->Photo100 = $this->config->item('s3_photo_url') . $msg->Photo100;
                        }
                        if ($msg->Photo200) {
                            $msg->Photo200 = $this->config->item('s3_photo_url') . $msg->Photo200;
                        }
                        if ($msg->Photo300) {
                            $msg->Photo300 = $this->config->item('s3_photo_url') . $msg->Photo300;
                        }
                    }
                }
                $numOfChattingUser = $this->Neighborhood_User_Chat_Messages_model->countChattingUser($nbh_id);
                $results = new stdClass();
                $results->Messages = $messages;
                $results->NumOfChattingUser = $numOfChattingUser;
                $response->results = $results;
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    private function _pushNotify($user_id, $type = '', $from_user_id = 0, $content = '', $placename)
    {
        $pushStatus = 0;
        $this->load->model('devices_model');
        $this->load->library('AC2DM');
        $this->load->library('APNS');
        $userInfo = $this->User_model->getUserInfo($from_user_id);

        if ($type == 1) { // msg
            $msg = $userInfo->UserName . ': ' . substr($content, 0, 30);
        } elseif ($type == 2) { // gift
            $msg = str_replace('%username%', $userInfo->UserName, $this->lang->line('msg_send_gift'));
        }
        if ($content == $this->lang->line('comment_post')) {
            $msg = str_replace('%name%', $userInfo->UserName, $this->lang->line('comment_post'));
            $msg = str_replace('%placename%', $placename, $msg);
        }

        $device = $this->devices_model->getDeviceOfUser($user_id);
        //$noti = $this->Push_model->getListPushNotification($user_id);
        $targetUserInfo = $this->User_model->getUserInfo($user_id);
        $total = $this->Notification_History_model->getBadge($targetUserInfo);
        if ($device) {
            $logData['UserID'] = $user_id;
            $logData['CreatedDate'] = gmdate('Y-m-d H:i:s');
            // for ios
            if ($device->Type == 'ios') {
                $apnMsg['aps']['alert'] = $msg;
                $apnMsg['aps']['badge'] = (int)$total;
                $sound = 'abc.aiff';
                $apnMsg['aps']['sound'] = (string)$sound;
                $apnMsg['f'] = $from_user_id;
                $apnMsg['t'] = 20;
                $apnMsg['u'] = $user_id;
                $apnMsg['n'] = $userInfo->UserName;
                $msgToPush = json_encode($apnMsg);
                $logData['Platform'] = 'ios';
                $logData['Message'] = $msgToPush;
                $pushStatus = $this->apns->_pushMessage($logData, $msgToPush, $device->DeviceID, $this->Config_model->getConfig('PUSH_NOTIFICATION_MODE'));
            }

            // for android
            if ($device->Type == 'android') {
                $c2dmMsg = $msg;
                $logData['Platform'] = 'android';
                $logData['Message'] = $c2dmMsg;
                $pushStatus = $this->ac2dm->send($device->DeviceID, 'text', $c2dmMsg, $logData);
            }
        }

        return $pushStatus;
    }

    function delete_post($token = '', $post_id = 0)
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($post_id)) {
                $postInfo = $this->Neighborhood_Post_model->findById($post_id);
                if ($postInfo && $postInfo->UserID == $tokenInfo->UserID) {
                    // delete this post & reference
                    $this->Neighborhood_Post_model->deletePost($post_id);
                    $response->code = $this->lang->line('ok');
                } else {
                    $response->code = $this->lang->line('invalid_request');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }

    function post_to_neighborhood_sp($token = '', $user_id = '', $nbh_id = '')
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (((isset($_POST['Content']) && trim($_POST['Content']) != '') || (isset($_FILES['Photo']) && $_FILES['Photo']['size'] > 0))) {
                // post picture
                $error = false;
                $photo = '';
                $photo100 = '';
                $photo200 = '';
                $photo300 = '';
                if (isset($_FILES['Photo']) && $_FILES['Photo']['size'] > 0) {
                    $isUpload = $this->s3upload->upload('Photo');
                    if (!$isUpload) {
                        $response->code = $this->lang->line('invalid_image');
                        $response->description = $this->s3upload->error;
                        $error = true;
                    } else {
                        $photo = $isUpload;
                        $p100 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 100, 150);
                        $photo100 = ($p100) ? $p100 : "";
                        $p200 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 200, 300);
                        $photo200 = ($p200) ? $p200 : "";
                        $p300 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 300, 450);
                        $photo300 = ($p300) ? $p300 : "";
                    }
                }

                if (!$error) {
                    $placeInfo = $this->Neighborhood_model->getInfo($nbh_id);
                    $created_date = gmdate('Y-m-d H:i:s');
                    $data = array(
                        'UserID' => $user_id,
                        'UserName' => $tokenInfo->UserName,
                        'Avatar' => $tokenInfo->Avatar,
                        'BirthDate' => $tokenInfo->BirthDate,
                        'Gender' => $tokenInfo->Gender,
                        'Lat' => $tokenInfo->Lat,
                        'Lon' => $tokenInfo->Lon,
                        'PlaceID' => $nbh_id,
                        'PlaceName' => $placeInfo->NAME,
                        'PlaceAddress' => $placeInfo->NAME,
                        'PlaceIcon' => $this->Place_Category_model->getIconByName('neighborhood'),
                        'Content' => isset($_POST['Content']) ? trim($_POST['Content']) : "",
                        'Photo' => $photo,
                        'Photo100' => $photo100,
                        'Photo200' => $photo200,
                        'Photo300' => $photo300,
                        'CreatedDate' => $created_date,
                        'Type' => 2
                    );

                    $result = $this->Place_model->postToPlaceSP($data);
                    if (sizeof($result) > 0) {
                        if (isset($result['Return']) && $result['Return'] == 0) {
                            $response->code = $this->lang->line('invalid_request');
                        } else {
                            // return this post
                            $post = new stdClass();
                            $post->ID = $result['PostID'];
                            $post->Photo = $photo != '' ? $this->config->item('s3_photo_url') . $photo : '';
                            $post->Photo100 = $photo100 != '' ? $this->config->item('s3_photo_url') . $photo100 : '';
                            $post->Photo200 = $photo200 != '' ? $this->config->item('s3_photo_url') . $photo200 : '';
                            $post->Photo300 = $photo300 != '' ? $this->config->item('s3_photo_url') . $photo300 : '';
                            $post->PlaceID = $nbh_id;
                            $post->Content = isset($_POST['Content']) ? trim($_POST['Content']) : "";
                            $post->CreatedDate = $created_date;
                            $post->UserID = $user_id;
                            $post->UserName = $tokenInfo->UserName;
                            $post->Avatar = $tokenInfo->UserName ? $this->config->item('s3_photo_url') . $tokenInfo->Avatar : '';
                            $post->Gender = $tokenInfo->Gender;
                            $response->post = $post;
                            $response->code = $this->lang->line('ok');
                        }
                    } else {
                        $response->code = $this->lang->line('invalid_request');
                    }
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

}