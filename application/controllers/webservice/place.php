<?php

class Place extends Ext_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Place_model');
        $this->load->model('Post_Comment_model');
        $this->load->model('Place_Post_model');
        $this->load->model('User_Checkin_model');
        $this->load->model('Place_Activity_model');
        $this->load->model('Tag_model');
        $this->load->model('Config_model');
        $this->load->model('Place_Category_model');
        $this->load->model('Place_User_Chat_Messages_model');
        $this->load->model('Notification_History_model');
        $this->load->model('Push_model');
        $this->load->model('Devices_model');
        $this->load->model('Image_model');
        $this->load->model('Wstoken_model');
        $this->load->model('Neighborhood_model');
        $this->load->model('Activity_model');
        $this->load->model('Hashtag_model');
        $this->load->model('Cron_Mention_User_model');
        $this->load->model('User_Profile_Status_model');
        $this->lang->load('ws');
        $this->load->library('SearchVenue');
        $this->load->library('AC2DM');
        $this->load->library('APNS');
        $this->searchvenue = SearchVenue::getInstance();

        $this->load->library('S3Upload');

        $config['upload_path'] = './public/upload/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = PHOTO_SIZE_MAX;
        $config['encrypt_name'] = TRUE;
        $config['remove_spaces'] = TRUE;
        $this->load->library('upload', $config);
    }

    private function sortPlaceByDistance($array)
    {
        $new_array = array();
        $sortable_array = array();
        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                $sortable_array[$k] = $v['Distance'];
            }
            asort($sortable_array);
            foreach ($sortable_array as $k => $v) {
                $new_array[] = $array[$k];
            }
        }
        return $new_array;
    }

    private function sortBy($array, $key)
    {
        $new_array = array();
        $sortable_array = array();
        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                $sortable_array[$k] = $v->$key;
            }
            arsort($sortable_array);
            foreach ($sortable_array as $k => $v) {
                $new_array[] = $array[$k];
            }
        }
        return $new_array;
    }


    /**
     * get nearby place with keyword (use post method)
     *
     * @param string $token
     * @param float $lat
     * @param float $lon
     * @param int $page
     * @return json array
     */
    function search_by_keyword_v2($token = '', $lat = '', $lon = '', $page = 1, $page_token = '')
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!is_numeric($page)) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                // POST method
                $country = isset($_POST['Country']) ? trim($_POST['Country']) : '';
                $city = isset($_POST['City']) ? trim($_POST['City']) : '';
                $state = isset($_POST['State']) ? trim($_POST['State']) : '';
                $error = 0;

                // ANOMO-8070 - if we cant get location will show Earth
                if ($lat == 'null' && $lon == 'null') {
                    $country = 'Earth';
                    $city = '';
                    $state = '';
                }
                
                // ANOMO-8116
                // client doesn't send Country, City, State
                // then get info from google
                if ($lat != 'null' && $lon != 'null' && empty($country) && empty($city) && empty($state)) {
                    $geoInfo = $this->Neighborhood_model->getCountryName($lat, $lon);
                    if ($geoInfo) {
                        $country = $geoInfo['country'];
                        $city = $geoInfo['city'];
                        $state = $geoInfo['state'];
                    } else {
                        // can not get location
                        // return fail
                        $error = 1;
                    }
                }
                
                if (!$error) {
                    $data = array(
                        'UserID' => $tokenInfo->UserID,
                        'Lat' => $lat,
                        'Lon' => $lon,
                        'CountryName' => $country,
                        'City' => $city,
                        'State' => $state
                    );
                    $this->Neighborhood_model->sp_location($data);
                }
                // end ANOMO-8116

                if (isset($_POST['keyword']) && trim($_POST['keyword']) != '') {
                    $results = $this->searchvenue->searchByKeywordAndLocation($_POST['keyword'], $lat, $lon, $page, $page_token);
                } else {
                    $results = $this->searchvenue->searchByLocation($lat, $lon, $page, $page_token);
                }
                if ($results) {
                    if (isset($results['results']) && count($results['results']) > 0) {
                        foreach ($results['results'] as $entry) {
                            $address = $entry['Address'];
                            if (preg_match('/^{{Province/', $entry['Address'])) {
                                $address = '';
                            }
                            $address = str_replace('{', '', $address);
                            $place_value = array(
                                'PlaceID' => $entry['ID'],
                                'Name' => $entry['Name'],
                                'Address' => $address,
                                'Lat' => $entry['Lat'],
                                'Lon' => $entry['Lon'],
                                'Reference' => $entry['Reference'],
                                'Category' => implode('|', $entry['Category'])
                            );

                            // insert new place
                            if ($this->Place_model->checkExistPlace($entry['ID']) == 0) {
                                $this->Place_model->addPlace($place_value);
                            }

                            $places[] = array(
                                'PlaceID' => $entry['ID'],
                                'Name' => $entry['Name'],
                                'Address' => $address,
                                'Phone' => $entry['Phone'],
                                'Email' => $entry['Email'],
                                'Website' => $entry['Website'],
                                'Lat' => $entry['Lat'],
                                'Lon' => $entry['Lon'],
                                'Category' => $this->Place_Category_model->returnListCategoryByName($entry['Category']),
                                'Distance' => $this->Place_model->distance($lat, $lon, $entry['Lat'], $entry['Lon']),
                                'NumberOfUserCheckedIn' => $this->Place_model->getNumberUserCheckedIn($entry['ID']),
                                'IsCheckedIn' => $this->Place_model->checkCheckIn($tokenInfo->UserID, $entry['ID']),
                                'Photo' => $this->Place_Category_model->returnOnePhotoByName($entry['Category'], $this->Config_model->getConfig('SEARCH_SERVICE_PROVIDER'))
                            );
                        }

                        //updated by Luong add Local Places
                        //identify the lat and lon of the last place
                        $places = $this->sortPlaceByDistance($places);

                        $index = count($places) - 1;
                        //if there are no page token, we will get local place between user's location and last place's location
                        if (isset($page_token) && empty($page_token) == false) {
                            $firstlat = $places[0]['Lat'];
                            $firstlon = $places[0]['Lon'];
                        } else {
                            $firstlat = $lat;
                            $firstlon = $lon;
                        }
                        $lastLat = $places[$index]['Lat'];
                        $lastLon = $places[$index]['Lon'];
                        $keyword = '';
                        if (isset($_POST['keyword']) && trim($_POST['keyword']) != '') {
                            $keyword = $_POST['keyword'];
                        }
                        $extPlace = $this->getLocalPlace_SearchByKeyword($places, $lat, $lon, $firstlat, $firstlon, $lastLat, $lastLon, 2, $keyword, $page, 2);
                        if ($extPlace) {
                            $places = array_merge($places, $extPlace);
                        }
                        //End updated by Luong		

                        $response->pagetoken = $results['pagetoken'];
                        $response->TotalPage = $results['total'];
                        $response->results = $places;
                        $response->code = $this->lang->line('ok');
                    } else {
                        $response->code = $this->lang->line('zero_results');
                    }
                } else {
                    //updated by Luong add Local Places
                    //if there are no Places is found from the search provider 
                    //Search Local Place depending on lat and lon of the user's input.
                    $keyword = '';
                    if (isset($_POST['keyword']) && trim($_POST['keyword']) != '') {
                        $keyword = $_POST['keyword'];
                    }
                    $extPlace = $this->getLocalPlace_SearchByKeyword(null, $lat, $lon, 0, 0, 0, 0, 2, $keyword, $page, 2);
                    if ($extPlace) {
                        $places = $extPlace;
                        $places = $extPlace;
                        $totalplaces = $this->Place_model->getLocalPlace($lat, $lon, 0, 0, 0, 0, 2, $keyword, $page, true);
                        //response
                        $response->pagetoken = "";
                        $response->TotalPage = ceil($totalplaces / 10);
                        $response->results = $places;
                        $response->code = $this->lang->line('ok');
                    } else {
                        $response->code = $this->lang->line('zero_results');
                    }
                    //End updated by Luong	 
                }
            }
        }

        echo json_encode($response);
        return json_encode($response);
    }

   

    /**
     * post text OR picture to place
     *
     * @param string $token
     * @param ing $user_id
     * @param string $place_id
     * @return json array
     */
    function post_to_place($token = '', $user_id = '', $place_id = '')
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            // check PlaceID and UserID
            $userInfo = $this->User_model->getUserInfo($user_id);
            $placeInfo = $this->Place_model->getPlace($place_id);

            if (((isset($_POST['Content']) && trim($_POST['Content']) != '') || (isset($_FILES['Photo']) && $_FILES['Photo']['size'] > 0)) && $placeInfo && $userInfo) {
                // update last activity
                // $this->User_model->updateLastActivity($user_id);

                // post picture
                $error = false;
                $photo = '';
                $photo100 = '';
                $photo200 = '';
                $photo300 = '';
                if (isset($_FILES['Photo']) && $_FILES['Photo']['size'] > 0) {
                    $isUpload = $this->s3upload->upload('Photo');
                    if (!$isUpload) {
                        $response->code = $this->lang->line('invalid_image');
                        $response->description = $this->s3upload->error;
                        $error = true;
                    } else {
                        $photo = $isUpload;
                        $p100 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 100, 150);
                        $photo100 = ($p100) ? $p100 : "";
                        $p200 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 200, 300);
                        $photo200 = ($p200) ? $p200 : "";
                        $p300 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 300, 450);
                        $photo300 = ($p300) ? $p300 : "";
                    }
                }

                if (!$error) {
                    // Enable tag user
                    $content = '';
                    if (isset($_POST['Content']) && $_POST['Content'] != ''){
                        $content = $this->User_Profile_Status_model->parseData($_POST['Content']);  
                    }
                        
                    // add to PlacePost tbl
                    $data = array(
                        'UserID' => $user_id,
                        'PlaceID' => $place_id,
                        'Content' => $content,
                        'Photo' => $photo,
                        'Photo100' => $photo100,
                        'Photo200' => $photo200,
                        'Photo300' => $photo300,
                        'CreatedDate' => gmdate('Y-m-d H:i:s')
                    );
                    $id = $this->Place_Post_model->add($data);
                    if ($id) {
                        $data = array(
                            'UserID' => $user_id,
                            'Type' => ($photo === '') ? 7 : 3,
                            'PlaceID' => $place_id,
                            'RefID' => $id,
                            'CreatedDate' => gmdate('Y-m-d H:i:s')
                        );
                        $this->Place_Activity_model->add($data);

                        $postInfo = $this->Place_Post_model->getInfo($id);
                        if ($postInfo->Photo) {
                            $postInfo->Photo = $this->config->item('s3_photo_url') . $postInfo->Photo;
                        }

                        if ($postInfo->Photo100) {
                            $postInfo->Photo100 = $this->config->item('s3_photo_url') . $postInfo->Photo100;
                        }
                        if ($postInfo->Photo200) {
                            $postInfo->Photo200 = $this->config->item('s3_photo_url') . $postInfo->Photo200;
                        }
                        if ($postInfo->Photo300) {
                            $postInfo->Photo300 = $this->config->item('s3_photo_url') . $postInfo->Photo300;
                        }

                        if ($postInfo->Avatar) {
                            $postInfo->Avatar = $this->config->item('s3_photo_url') . $postInfo->Avatar;
                        }
                        $response->post = $postInfo;

                        // publishing a feed
                        if ($this->Config_model->getConfig('ACTIVITY_FEED') == 'on') {
                            $place_icon = '';
                            $place_cats = explode('|', $placeInfo->Category);
                            if (!empty($place_cats)) {
                                $place_icon = $place_cats[0];
                            }
                            
                            // ANOMO-7288 - Show the location of the activity post
                            $userLocationObj = $this->User_model->getCurrentLocationOfUser($user_id);
                            $activityType =  ($photo == '') ? 6 : 2  ;   
                            $feed = array(
                                'FromUserID' => $user_id,
                                'FromUserName' => $userInfo->UserName,
                                'Avatar' => $userInfo->Avatar,
                                'Type' => $activityType,
                                'ActionType' => $activityType,
                                'Message' => $content,
                                'Image' => $photo,
                                'RefID' => $id,
                                'PlaceID' => $place_id,
                                'PlaceName' => $placeInfo->Name,
                                'PlaceIcon' => $this->Place_Category_model->getIconByName($place_icon),
                                'PlaceAddress' => $placeInfo->Address,
                                'Lat' => $userInfo->Lat,
                                'Lon' => $userInfo->Lon,
                                'BirthDate' => $userInfo->BirthDate,
                                'Gender' => $userInfo->Gender,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                                ,'CheckinPlaceID'  => $userLocationObj?$userLocationObj->PlaceID:''
                                ,'CheckinPlaceName'    => $userLocationObj?$userLocationObj->PlaceName:''
                                ,'NeighborhoodID' => $userLocationObj?$userLocationObj->NeighborhoodID:''
                            );
                            $feed_id = $this->Activity_model->add($feed);
                            if ($feed_id){
                                if ($content != ''){
                                        // push notify to @User
                                        $json = json_decode($content);
                                        if ($json){
                                            $mentionUserObj = isset($json->message_tags)?$json->message_tags:null;
                                            if ($mentionUserObj){
                                                $aHistory = array();
                                                $aPushNotify = array();
                                                foreach($mentionUserObj as $userObj){
                                                    // push notify    
                                                    $time = gmdate('Y-m-d H:i:s');
                                                    // dont push to myself
                                                    if (isset($userObj->id) && $userObj->id != $user_id){
                                                        // should add to queue
                                                        $aPushNotify [] = "($userObj->id, $user_id, 13, $id, $activityType, 0, 0, '$time')";
                                                        $aHistory [] = "($userObj->id, $user_id, 16, $id, $activityType, 1, '$time')";
                                                    }
                                                }
                                                // add to queue to push notify
                                                if (sizeof($aPushNotify) > 0){
                                                    $this->Cron_Mention_User_model->addMulti(array_unique($aPushNotify));
                                                }
                                                
                                                // add to notification history center
                                                if (sizeof($aHistory) > 0){
                                                    $this->Notification_History_model->addMulti(array_unique($aHistory));
                                                }
                                            }
                                        }
                                        // ANOMO-7852 add hashtag
                                        $this->Hashtag_model->add($content, $feed_id);
                                    } 
                            }
                        }
                        // end publishing a feed

                        $response->code = $this->lang->line('ok');
                    } else {
                        $response->code = $this->lang->line('db_error');
                    }
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * post comment to a post of place
     *
     * @param string $token
     * @param int $user_id
     * @param int $post_id
     * @return json array
     */
    function post_comment($token = '', $user_id = '', $post_id = '')
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!isset($_POST['Content']) || !trim($_POST['Content']) != '') {
                $response->code = $this->lang->line('invalid_request');
            } else {
                // check PostID and UserID
                $postInfo = $this->Place_Post_model->getInfo($post_id);
                if (!$postInfo) {
                    $response->code = $this->lang->line('invalid_parameter');
                } else {
                    $user_id = $tokenInfo->UserID;
                    $target_user_id = $postInfo->UserID;
                    //check block
                    if ($this->User_model->checkBlock($user_id, $target_user_id)) {
                        $response->code = $this->lang->line('target_user_blocked');
                    } elseif ($this->User_model->checkBlock($target_user_id, $user_id)) {
                        $response->code = $this->lang->line('blocked');
                    } else {
                        $data = array(
                            'UserID' => $user_id,
                            'PlacePostID' => $post_id,
                            'Content' => $_POST['Content'],
                            'CreatedDate' => gmdate('Y-m-d H:i:s')
                        );

                        $id = $this->Post_Comment_model->add($data);
                        if ($id) {
                            $contentType = ($postInfo->Photo != '') ? 2 : 6;
                            $this->Activity_model->increase($post_id, $contentType, 'comment');    
                            $data = array(
                                'UserID' => $user_id,
                                'PlaceID' => $postInfo->PlaceID,
                                'Type' => 1,
                                'RefID' => $id,
                                'CreatedDate' => gmdate('Y-m-d H:i:s')
                            );
                            $this->Place_Activity_model->add($data);
                            
                            if ($target_user_id !== $user_id) {
                                $placeInfo = $this->Place_model->getPlace($postInfo->PlaceID);
                                $placeName = $placeInfo->Name;
                                //add push notification

                                $pushData = array(
                                    'UserID' => $target_user_id,
                                    'SendUserID' => $user_id,
                                    'Type' => ($postInfo->Photo) ? 10 : 9,
                                    'RefID' => $id,
                                    'IsRead' => 1,
                                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                                );
                                $this->Notification_History_model->add($pushData);

                                $targetUserInfo = $this->User_model->getUserInfo($target_user_id);
                                if ($postInfo->Photo) {
                                    if ($targetUserInfo->AllowCommentPicturePostNotice) {
                                        self::_pushNotify($target_user_id, 5, $user_id, $this->lang->line('comment_post'), $placeName, 2, $postInfo->ID);
                                    }
                                } else {
                                    if ($targetUserInfo->AllowCommentTextPostNotice) {
                                        self::_pushNotify($target_user_id, 6, $user_id, $this->lang->line('comment_post'), $placeName);
                                    }
                                }
                            }

                            // output
                            $commentInfo = new stdClass();
                            $commentInfo->ID = $id;
                            $commentInfo->Content = $_POST['Content'];
                            $commentInfo->UserID = $tokenInfo->UserID;
                            $commentInfo->UserName = $tokenInfo->UserName;
                            $commentInfo->Gender = $tokenInfo->Gender;
                            $commentInfo->CreatedDate = gmdate('Y-m-d H:i:s');
                            $commentInfo->Avatar = ($tokenInfo->Avatar)?$this->config->item('s3_photo_url') . $tokenInfo->Avatar:"";
                            
                            $response->comment = $commentInfo;
                            $response->code = $this->lang->line('ok');
                        } else {
                            $response->code = $this->lang->line('db_error');
                        }
                    }
                }
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * Check in / Spectate
     *
     * @param string $token
     * @param int $user_id
     * @param string $place_id
     * @param int status (1: checked in / 2: check out / 3: spectator)
     * @return json array
     */
    function check_in($token = '', $user_id = '', $place_id = '', $status = 0)
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            // check PlaceID and UserID
            if (!in_array($status, array(1, 3))) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                // $userInfo = $this->User_model->getUserInfo($user_id);
                $placeInfo = $this->Place_model->getPlace($place_id);

                if (!$placeInfo) {
                    $response->code = $this->lang->line('invalid_parameter');
                } else {
                    // update last activity
                    // $this->User_model->updateLastActivity($user_id);
                    // ANOMO-4981 -attach picture when check-in
                    $error = false;
                    $photo = '';
                    $photo100 = '';
                    $photo200 = '';
                    $photo300 = '';
                    if (isset($_FILES['Photo']) && $_FILES['Photo']['size'] > 0) {
                        $isPhoto = $this->s3upload->upload('Photo');
                        if (!$isPhoto) {
                            $response->code = $this->lang->line('invalid_image');
                            $response->description = $this->s3upload->display_errors('', '');
                            $error = true;
                        } else {
                            //$upload_data = $this->upload->data();
                            //$photo = $upload_data['file_name'];
                            $attachPhoto = true;
                            $photo = $isPhoto;
                            $photo100 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 100, 150);
                            $photo200 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 200, 300);
                            $photo300 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 300, 450);
                        }
                    }

                    if ($error) {
                        $response->code = $this->lang->line('invalid_request');
                    } else {
                        // end ANOMO-4981 
                        $checkin = $this->_check_in($user_id, $place_id, $status);
                        if ($checkin) {
                            $post_id = '';
                            $place_icon = '';
                            $place_cats = explode('|', $placeInfo->Category);
                            if (!empty($place_cats)) {
                                $place_icon = $place_cats[0];
                            }
                            $icon = $this->Place_Category_model->getIconByName($place_icon);
                            // ANOMO-4981 - attach picture when check-in
                            // post picture to place
                            // Enable tag user
                            $content = '';
                            if (isset($_POST['Content']) && $_POST['Content'] != ''){
                                $content = $this->User_Profile_Status_model->parseData($_POST['Content']);  
                            }
                            if (isset($attachPhoto) && $photo != '') {
                                // add to PlacePost tbl
                                $data = array(
                                    'UserID' => $user_id,
                                    'PlaceID' => $place_id,
                                    'Content' => $content,
                                    'Photo' => $photo,
                                    'Photo100' => $photo100,
                                    'Photo200' => $photo200,
                                    'Photo300' => $photo300,
                                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                                );
                                $id = $this->Place_Post_model->add($data);
                                if ($id) {
                                    $post_id = $id;
                                    $postInfo = $this->Place_Post_model->getInfo($id);
                                    if ($postInfo->Photo) {
                                        $postInfo->Photo = $this->config->item('s3_photo_url') . $postInfo->Photo;
                                    }

                                    if ($postInfo->Photo100) {
                                        $postInfo->Photo100 = $this->config->item('s3_photo_url') . $postInfo->Photo100;
                                    }
                                    if ($postInfo->Photo200) {
                                        $postInfo->Photo200 = $this->config->item('s3_photo_url') . $postInfo->Photo200;
                                    }
                                    if ($postInfo->Photo300) {
                                        $postInfo->Photo300 = $this->config->item('s3_photo_url') . $postInfo->Photo300;
                                    }

                                    if ($postInfo->Avatar) {
                                        $postInfo->Avatar = $this->config->item('s3_photo_url') . $postInfo->Avatar;
                                    }
                                    $response->post = $postInfo;
                                }
                            }
                            // end ANOMO-4981
                            // add feed

                            if ($status == 1 && $this->Config_model->getConfig('ACTIVITY_FEED') == 'on') {
                                $msg = '';
                                $feed = array(
                                    'FromUserID' => $user_id,
                                    'FromUserName' => $tokenInfo->UserName,
                                    'Avatar' => $tokenInfo->Avatar,
                                    'Type' => 0,
                                    'ActionType' => 0,
                                    'Message' => $content,
                                    'RefID' => $checkin,
                                    'Image' => $photo,
                                    'PlaceID' => $place_id,
                                    'PlaceName' => $placeInfo->Name,
                                    'PlaceIcon' => $icon,
                                    'PlaceAddress' => $placeInfo->Address,
                                    'Lat' => $tokenInfo->Lat,
                                    'Lon' => $tokenInfo->Lon,
                                    'BirthDate' => $tokenInfo->BirthDate,
                                    'Gender' => $tokenInfo->Gender,
                                    'CreatedDate' => gmdate('Y-m-d H:i:s')
                                    ,'CheckinPlaceID'  => $place_id
                                    ,'CheckinPlaceName'    => $placeInfo->Name
                                    ,'NeighborhoodID' => $tokenInfo->NeighborhoodID != null?$tokenInfo->NeighborhoodID:''
                                );
                                $feed_id = $this->Activity_model->add($feed);
                                // end feed
                                if ($feed_id){
                                    if ($content != ''){
                                        // push notify to @User
                                        $json = json_decode($content);
                                        if ($json){
                                            $mentionUserObj = isset($json->message_tags)?$json->message_tags:null;
                                            if ($mentionUserObj){
                                                $aHistory = array();
                                                $aPushNotify = array();
                                                foreach($mentionUserObj as $userObj){
                                                    // push notify    
                                                    $time = gmdate('Y-m-d H:i:s');
                                                    // dont push to myself
                                                    if (isset($userObj->id) && $userObj->id != $user_id){
                                                        // should add to queue
                                                        $aPushNotify [] = "($userObj->id, $user_id, 13, $checkin, 0, 0, 0, '$time')";
                                                        $aHistory [] = "($userObj->id, $user_id, 16, $checkin, 0, 1, '$time')";
                                                    }
                                                }
                                                // add to queue to push notify
                                                if (sizeof($aPushNotify) > 0){
                                                    $this->Cron_Mention_User_model->addMulti(array_unique($aPushNotify));
                                                }
                                                
                                                // add to notification history center
                                                if (sizeof($aHistory) > 0){
                                                    $this->Notification_History_model->addMulti(array_unique($aHistory));
                                                }
                                            }
                                        }
                                        // ANOMO-7852 add hashtag
                                        $this->Hashtag_model->add($content, $feed_id);
                                    } 
                                }
                            }

                            $data = array(
                                'UserID' => $user_id,
                                'PlaceID' => $place_id,
                                'RefID' => $checkin,
                                'Type' => ($status == 1) ? 0 : 4,
                                'CreatedDate' => gmdate('Y-m-d H:i:s'),
                                'Content' => $content,
                                'PostID' => $post_id
                            );
                            $this->Place_Activity_model->add($data);

                            $response->code = $this->lang->line('ok');
                        } else {
                            $response->code = $this->lang->line('db_error');
                        }
                    }
                }
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * check out function
     *
     * @param string $token
     * @param int $user_id
     * @param string $place_id
     * @return json array
     */
    function check_out($token = '', $user_id = '', $place_id = '', $status = 1)
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            // check PlaceID and UserID
            if (!is_numeric($user_id) || !in_array($status, array(1, 3))) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $placeInfo = $this->Place_model->checkExistPlace($place_id);
                if (!$placeInfo) {
                    $response->code = $this->lang->line('invalid_parameter');
                } else {
                    // update last activity
                    // $this->User_model->updateLastActivity($user_id);
                    if ($this->Place_model->checkExistCheckin($place_id, $user_id, $status)) {
                        // update status = check out
                        $this->User_Checkin_model->update(array('Status' => 2, 'CheckOutDate' => gmdate('Y-m-d H:i:s')), array('UserID' => $user_id, 'PlaceID' => $place_id));
                        $response->code = $this->lang->line('ok');
                    } else {
                        $response->code = $this->lang->line('invalid_request');
                    }
                }
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * ws get list of activity
     *
     * @param string $token
     * @param int $user_id
     * @param float $lat
     * @param float $lon
     * @param int $page
     * @return json
     */
    function get_activity($token = '', $user_id = 0, $lat = 0, $lon = 0, $page = 1)
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            // $user = $this->User_model->getUserInfo($user_id);
            if (is_numeric($lat) && is_numeric($lon) && is_numeric($page)) {
                // update last activity
                // $this->User_model->updateLastActivity($user_id);

                $limit = 10;
                $offset = ($page - 1) * $limit;
                $results = $this->Place_model->getPlaceActivity($lat, $lon, $limit, $offset);
                $results = self::sortBy($results, 'CreatedDate');
                $data = array();
                if ($results) {
                    foreach ($results as $re) {
                        unset($re->Distance);
                        //$re->Tags = array();
                        $re->Content = "";
                        $re->Photo = "";
                        if ($re->UserID != 0) {
                            $userInfo = $this->User_model->getUserInfo($re->UserID);
                            $re->UserName = $userInfo->UserName;
                            $re->Avatar = ($userInfo->Avatar) ? $this->config->item('s3_photo_url') . $userInfo->Avatar : "";
                            $re->CheckInStatus = $this->Place_model->getCheckInStatus($userInfo->UserID, $re->PlaceID);
                            // show list tag
                            //$re->Tags = $this->Place_model->getListTagOf($re->PlaceID);
                        }

                        if ($re->PostID != 0) {
                            // post info
                            $info = $this->Place_Post_model->getInfo($re->PostID);
                            $re->UserName = $info->UserName;
                            $re->Avatar = ($info->Avatar) ? $this->config->item('s3_photo_url') . $info->Avatar : "";
                            $re->Content = $info->Content;
                            $re->Photo = ($info->Photo) ? $this->config->item('s3_photo_url') . $info->Photo : "";
                            ;
                            $re->CheckInStatus = $this->Place_model->getCheckInStatus($info->UserID, $re->PlaceID);
                        }

                        $data[] = get_object_vars($re);
                    }
                    $total = $this->Place_model->getPlaceActivity($lat, $lon, $limit, $offset, true);
                    $response->TotalPage = ceil($total / 10);
                }
                $response->Result = $data;
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }


    /**
     * ws get anomo pick
     *
     * @param string $token
     * @param float $lat
     * @param float $lon
     * @param int $page
     * @return json
     */
    function get_anomo_pick_v2($token = '', $lat = 0, $lon = 0, $page = 1)
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($lat) && is_numeric($lon) && is_numeric($page) && $page >= 1) {

                $limit = $this->Config_model->getConfig('NUMBER_ANOMO_PICK');
                $offset = ($page - 1) * $limit;
                $results = $this->Place_model->getAnomoPickV2($lat, $lon, $limit, $offset);
                if ($results) {

                    foreach ($results as $re) {
                        $re->Distance = number_format($re->Distance, 3, '.', '');
                        $re->PictureNum = $this->Place_Post_model->getNumberPostOf($re->PlaceID, 'picture');
                        $re->TextNum = $this->Place_Post_model->getNumberPostOf($re->PlaceID, 'text');
                        $numberCheckIn = $this->Place_model->getTotalCheckin($re->PlaceID);
                        $re->CheckinNum = $numberCheckIn;
                        $re->NumberAnomo = $numberCheckIn;
                        $category = self::getCategoryOfPlace($re->PlaceID);
                        $re->Image = ($category != '') ? $this->Place_Category_model->returnOnePhotoByName($category) : '';
                    }
                    $response->Result = $results;
                } else {
                    $response->Result = array();
                }
                $total = $this->Place_model->getAnomoPickV2($lat, $lon, $limit, $offset, true);
                $response->TotalPage = ceil($total / 10);
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    function get_detail_place_v2($token = '', $user_id = 0, $place_id = '')
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            // update last activity
            // $this->User_model->updateLastActivity($user_id);
            self::set_as_spectate_user($place_id, $user_id);

            $data = $this->_get_detail_place($user_id, $place_id);
            if ($data) {
                $response->results = $data;
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('not_found');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    

    function prepare_chat_on_place($token = '', $user_id = 0, $place_id = '', $lat = 0, $lon = 0)
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            // update last activity
            // $this->User_model->updateLastActivity($user_id);

            $place = $this->_get_detail_place($user_id, $place_id);
            if ($place) {
                $distance = $this->Place_model->distance($lat, $lon, $place['Lat'], $place['Lon']);
                $status = 1;
                if ($distance > 2) {
                    $status = 3;
                }
                $checkin = $this->_check_in($user_id, $place_id, $status);
                if ($checkin) {
                    $place = $this->_get_detail_place($user_id, $place_id);
                    $response->place = $place;
                    $response->code = $this->lang->line('ok');
                } else {
                    $response->code = $this->lang->line('db_error');
                }
            } else {
                $response->code = $this->lang->line('not_found');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

   

    function get_picture_wall_v2($token = '', $place_id = 0, $page = 1)
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $placeInfo = $this->Place_model->getPlace($place_id);
            if ($placeInfo && is_numeric($page)) {

                // update last activity
                // $this->User_model->updateLastActivity($user_id);

                // list text post
                $limit = 18;
                $offset = ($page - 1) * $limit;
                $total = 0;
                $list_text_post = $this->Place_Post_model->getListPostOf($place_id, 'picture', $total, $offset, $limit);
                $text_posts = array();
                foreach ($list_text_post as $post) {
                    if ($post->Photo) {
                        $post->Photo = $this->config->item('s3_photo_url') . $post->Photo;
                    }
                    if ($post->Photo100) {
                        $post->Photo100 = $this->config->item('s3_photo_url') . $post->Photo100;
                    }
                    if ($post->Photo200) {
                        $post->Photo200 = $this->config->item('s3_photo_url') . $post->Photo200;
                    }
                    if ($post->Photo300) {
                        $post->Photo300 = $this->config->item('s3_photo_url') . $post->Photo300;
                    }
                    if ($post->Avatar) {
                        $post->Avatar = $this->config->item('s3_photo_url') . $post->Avatar;
                    }
//                    $post->TotalComment = $this->Post_Comment_model->countCommentOf($post->ID);
//                    unset($post->Gender);
                    $text_posts[] = get_object_vars($post);
                }
                $results = new stdClass();
                $results->Posts = $text_posts;
                $results->TotalPage = ceil($total / $limit);
                $results->CurrentPage = $page;
                $response->results = $results;
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

   

    function get_wall_post_v2($token = '', $place_id = 0, $page = 1)
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $placeInfo = $this->Place_model->getPlace($place_id);
            if ($placeInfo && is_numeric($page)) {


                // update last activity
                // $this->User_model->updateLastActivity($user_id);

                // list text post
                $limit = 12;
                $offset = ($page - 1) * $limit;
                $total = 0;

                $list_text_post = $this->Place_Post_model->getListPostOf($place_id, 'all', $total, $offset, $limit);
                $text_posts = array();
                foreach ($list_text_post as $post) {
                    if ($post->Photo) {
                        $post->Photo = $this->config->item('s3_photo_url') . $post->Photo;
                    }
                    if ($post->Photo100) {
                        $post->Photo100 = $this->config->item('s3_photo_url') . $post->Photo100;
                    }
                    if ($post->Photo200) {
                        $post->Photo200 = $this->config->item('s3_photo_url') . $post->Photo200;
                    }
                    if ($post->Photo300) {
                        $post->Photo300 = $this->config->item('s3_photo_url') . $post->Photo300;
                    }
                    if ($post->Avatar) {
                        $post->Avatar = $this->config->item('s3_photo_url') . $post->Avatar;
                    }

                    // list comment of post
//                    $post->TotalComment = $this->Post_Comment_model->countCommentOf($post->ID);
//                    unset($post->Gender);
                    $text_posts[] = get_object_vars($post);
                }
                $response->TotalPage = ceil($total / $limit);
                $response->CurrentPage = $page;

                $response->WallPostList = $text_posts;
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }


    function get_anomos_v2($token = '', $user_id = 0, $place_id = 0, $page = 1)
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (!is_numeric($page)) {
                $response->code = $this->lang->line('invalid_request');
            } else {
                $placeInfo = $this->Place_model->getPlace($place_id);
                if (!$placeInfo) {
                    $response->code = $this->lang->line('invalid_parameter');
                } else {

                    // update last activity
                    // $this->User_model->updateLastActivity($user_id);

                    // list user checked in / spectate // offline in around 24hrs
                    $limit = 12;
                    $offset = ($page - 1) * $limit;
                    $total = 0;
                    $user_checkedin = array();
                    $list_checkedin = $this->Place_model->getListUserOf($place_id, 0, 'current', $total, $offset, $limit);
                    if ($list_checkedin) {
                        foreach ($list_checkedin as $entry) {
                            if ($entry->Avatar) {
                                $entry->Avatar = $this->config->item('s3_photo_url') . $entry->Avatar;
                            }
                            $facebokID = ($entry->FacebookID) ? $entry->FacebookID : '';
                            $entry->FacebookID = $facebokID;
                            $entry->Tags = $this->User_model->getListTagOf($entry->UserID);
                            //
                            $datesearch = date('Y-m-d H:i:s', strtotime(US_USER_ACTIVE_TIME . ' hour', strtotime(gmdate('Y-m-d H:i:s'))));
                            $entry->IsOnline = ($entry->LastActivity >= $datesearch) ? 1 : 0;

                            unset($entry->Email);
                            unset($entry->CheckInDate);

                            $user_checkedin[] = get_object_vars($entry);
                        }
                    }
                    $response->TotalPage = ceil($total / $limit);
                    $response->CurrentPage = $page;
                    $response->ListUser = $user_checkedin;
                    $response->code = $this->lang->line('ok');
                }
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    function get_anomo_by_tag($token = '', $place_id = '', $tag_id = 0)
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($tag_id)) {
                $tagInfo = $this->Tag_model->getTagInfo($tag_id);
                $placeInfo = $this->Place_model->getPlace($place_id);
                if ($placeInfo && $tagInfo) {

                    // update last activity
                    // $this->User_model->updateLastActivity($user_id);

                    // user check in
                    $user_checkedin = array();
                    $list_checkin = $this->Place_model->getListUserBelongTagOf($place_id, $tag_id, 'current');
                    foreach ($list_checkin as $entry) {
                        if ($entry->Avatar) {
                            $entry->Avatar = $this->config->item('s3_photo_url') . $entry->Avatar;
                        }
                        $entry->ConnectMeter = $this->User_model->getConnectMeter($entry->UserID, $user_id);
                        $entry->Tags = $this->User_model->getListTagOf($entry->UserID);
                        $user_checkedin[] = get_object_vars($entry);
                    }
                    // list user logout
                    $user_logout = array();
                    $list_logout = $this->Place_model->getListUserBelongTagOf($place_id, $tag_id, 'past');
                    if ($list_logout) {
                        foreach ($list_logout as $entry) {
                            if ($entry->Avatar) {
                                $entry->Avatar = $this->config->item('s3_photo_url') . $entry->Avatar;
                            }
                            $entry->ConnectMeter = $this->User_model->getConnectMeter($entry->UserID, $user_id);
                            $entry->Tags = $this->User_model->getListTagOf($entry->UserID);
                            $user_logout[] = get_object_vars($entry);
                        }
                    }
                    $response->ListUserCheckIn = $user_checkedin;
                    $response->ListLogout = $user_logout;
                    $response->code = $this->lang->line('ok');
                } else {
                    $response->code = $this->lang->line('invalid_request');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    function get_tags($token = '', $place_id = '')
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $place_id = trim($place_id);
            $placeInfo = $this->Place_model->getPlace($place_id);
            if ($placeInfo) {
                // update last activity
                // $this->User_model->updateLastActivity($user_id);

                $response->ListTag = $this->Place_model->getListTagOf($place_id);
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

   

    function get_random_v2($token = '', $user_id = '', $lat = '', $lon = '', $page = 1)
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($lat) && is_numeric($lon) && is_numeric($page) && $page >= 1) {

                // update last activity
                // $this->User_model->updateLastActivity($user_id);

                //$response->PlaceList = $this->Place_model->searchPlaceNearby($lat, $lon);
                $limit = 12;
                $offset = ($page - 1) * $limit;
                $userList = $this->Place_model->searchUserNearbyV2($user_id, $lat, $lon, $limit, $offset);
                if ($userList) {
                    foreach ($userList as &$user) {
                        unset($user->Distance);
                        if ($user->Avatar) {
                            $user->Avatar = $this->config->item('s3_photo_url') . $user->Avatar;
                        }
                        if ($user->FullAvatar) {
                            $user->FullAvatar = $this->config->item('s3_photo_url') . $user->FullAvatar;
                        }
                        $placeCheckIn = $this->Place_model->getCheckInPlaceOfUser($user->UserID);
                        $placeID = NULL;
                        $placeName = NULL;
                        if ($placeCheckIn) {
                            $placeID = $placeCheckIn->PlaceID;
                            $placeName = $placeCheckIn->Name;
                        }
                        $user->PlaceID = $placeID;
                        $user->Name = $placeName;
                        $user->IsBlock = $this->User_model->checkBlock($user_id, $user->UserID);
                        $user->IsBlockTarget = $this->User_model->checkBlock($user->UserID, $user_id);
                    }
                } else {
                    $userList = array();
                }
                $response->UserList = $userList;
                $total = $this->Place_model->searchUserNearbyV2($user_id, $lat, $lon, $limit, $offset, true);
                $response->TotalPage = ceil($total / 12);
                $response->CurrentPage = $page;
                $response->code = $this->lang->line('ok');

            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }

        echo json_encode($response);
        return json_encode($response);
    }

    private function _check_in($user_id = '', $place_id = '', $status = 0)
    {
        $checkin = 0;

        // check out other location
        if ($status == 1) {
            $this->User_Checkin_model->update(array('Status' => 2, 'CheckOutDate' => gmdate('Y-m-d H:i:s')), array('UserID' => $user_id));
        }
        // checkout time = current time + 3 hours
        $checkout_date = date('Y-m-d H:i:s', strtotime("+" . CHECKEDIN_DURATION . " hours", strtotime(gmdate('Y-m-d H:i:s'))));

        //updated by Luong : insert new check-in record instead of update to the existed check-in place
        //$checkin_exist = $this->Place_model->checkExistCheckin($place_id, $user_id, 0);
        //if (!$checkin_exist) {
        $data = array(
            'UserID' => $user_id,
            'PlaceID' => $place_id,
            'Status' => $status,
            'CheckInDate' => gmdate('Y-m-d H:i:s'),
            'CheckOutDate' => $checkout_date
        );
        $checkin = $this->User_Checkin_model->add($data);
        //update last checkin id to user table
        if ($status == 1) {
            $this->User_Checkin_model->updateUserLastCheckInID($user_id, $checkin);
        }
        /*
          } else {
          // update status
          $updated = $this->User_Checkin_model->update(array('Status' => $status, 'CheckInDate' => gmdate('Y-m-d H:i:s'), 'CheckOutDate' => $checkout_date), array('UserID' => $user_id, 'PlaceID' => $place_id));
          $checkin = ($updated)?$checkin_exist->ID:0;
          }
         */
        return $checkin;
    }

    private function _get_detail_place($user_id = 0, $place_id = '')
    {
        $ret = array();

        // check service provider
        $venue = false;
        $serviceProvider = $this->Config_model->getConfig('SEARCH_SERVICE_PROVIDER');
        $placeDetail = $this->Place_model->getPlace($place_id);
        if ($placeDetail->Provider != 'local') {
            $local = false;
            $serviceProvider = $this->Config_model->getConfig('SEARCH_SERVICE_PROVIDER');
            if ($serviceProvider == 'google') {
                $placeInfo = $this->Place_model->getPlace($place_id);
                $venue = false;
                if ($placeInfo) {
                    $venue = $this->searchvenue->getDetail($placeInfo->Reference);
                }
            } else {
                $venue = $this->searchvenue->getDetail($place_id);
            }
        } else {
            $local = true;
            $venue = array('ID' => $placeDetail->ID,
                'Address' => $placeDetail->Address,
                'Name' => $placeDetail->Name,
                'Lat' => $placeDetail->Lat,
                'Lon' => $placeDetail->Lon,
                'Reference' => $placeDetail->Reference,
                'Category' => explode("|", $placeDetail->Category)
            );
        }

        $userInfo = $this->User_model->getUserInfo($user_id);
        if ($venue && $userInfo) {
            $address = $venue['Address'];
            if (preg_match('/^{{Province/', $venue['Address'])) {
                $address = '';
            }
            $address = str_replace('{', '', $address);
            if ($local == false) {
                $place_value = array(
                    'PlaceID' => $venue['ID'],
                    'Name' => $venue['Name'],
                    'Address' => $address,
                    'Lat' => $venue['Lat'],
                    'Lon' => $venue['Lon'],
                    'Reference' => $venue['Reference'],
                    'Category' => implode('|', $venue['Category'])
                );

                // insert new place
                if ($this->Place_model->checkExistPlace($venue['ID']) == 0) {
                    $this->Place_model->addPlace($place_value);
                }
            }

            // tag list
            $tagList = $this->Place_model->getListTagOf($place_id, 'current');            
            $number_text_post = $this->Place_Post_model->getTotalPostInPlace($place_id, 'text');           
            $number_picture_post = $this->Place_Post_model->getTotalPostInPlace($place_id, 'picture');

            // Checkin radius -  1 : can checkin, 0: don't allow 
            $canCheckIn = $checkinRadius = 0;
            if (isset($userInfo->Lat) && !empty($userInfo->Lat) && isset($userInfo->Lon) && !empty($userInfo->Lon)) {
                $distance = $this->Place_model->distance($userInfo->Lat, $userInfo->Lon, $venue['Lat'], $venue['Lon']);
                $checkinRadius = $this->Config_model->getConfig('CHECKIN_RADIUS_SET');
                if (0 != $checkinRadius && ($distance <= $checkinRadius)) {
                    $canCheckIn = 1;
                }
            }
            $ret = array(
                'Name' => $venue['Name'],
                'Address' => $address,
                'Lat' => $venue['Lat'],
                'Lon' => $venue['Lon'],
                'Phone' => isset($venue['Phone']) ? $venue['Phone'] : "",
                'Email' => isset($venue['Email']) ? $venue['Email'] : "",
                'Website' => isset($venue['Website']) ? $venue['Website'] : "",
                'Category' => $this->Place_Category_model->returnListCategoryByName($venue['Category']),
                'CheckInStatus' => $this->Place_model->getCheckInStatus($user_id, $place_id),
                'ListAnomo' => array(), // not use
                'NumberAnomo' => 0, // not use
                'NumberAnomoPast' => 0, // not use
                'ListTextPost' => array(), // not use
                'NumberTextPost' => $number_text_post,
                'ListPicturePost' => array(), // not use
                'NumberPicturePost' => $number_picture_post,
                'ListTag' => $tagList,
                'NumberTagPast' => 0, // not use
                'NumberOfUserCheckedIn' => $this->Place_model->getNumberUserCheckedIn($place_id),
                'CanCheckIn' => $canCheckIn
            );
        }
        return $ret;
    }

    /**
     * ws get all place that user is spectating
     *
     * @param string $token
     * @param int $user_id
     * @param float $lat
     * @param float $lon
     * @return json
     */
    function get_spectate_place($token = '', $user_id = 0, $lat = '', $lon = '')
    {
        $response = new stdClass();

        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($user_id)) {
                // update last activity
                // $this->User_model->updateLastActivity($user_id);

                $places = $this->Place_model->getListSpectatePlaceOf($user_id);
                $data = array();
                if ($places) {
                    // check service provider
                    $serviceProvider = $this->Config_model->getConfig('SEARCH_SERVICE_PROVIDER');
                    if ($serviceProvider == 'google') {
                        $field = 'Reference';
                    } else {
                        $field = 'PlaceID';
                    }

                    foreach ($places as $place) {
                        $entry = $this->searchvenue->getDetail($place->$field);
                        $address = $entry['Address'];
                        if (preg_match('/^{{Province/', $entry['Address'])) {
                            $address = '';
                        }
                        $address = str_replace('{', '', $address);
                        $dataPlace[] = array(
                            'PlaceID' => $place->PlaceID,
                            'Name' => $entry['Name'],
                            'Address' => $address,
                            'Phone' => $entry['Phone'],
                            'Email' => $entry['Email'],
                            'Website' => $entry['Website'],
                            'Lat' => $entry['Lat'],
                            'Lon' => $entry['Lon'],
                            'Category' => $this->Place_Category_model->returnListCategoryByName($entry['Category']),
                            'Distance' => $this->Place_model->distance($lat, $lon, $entry['Lat'], $entry['Lon']),
                            'NumberOfUserCheckedIn' => $this->Place_model->getNumberUserCheckedIn($entry['ID'])
                        );
                    }
                    $data = $dataPlace; //$this->sortPlaceByDistance($dataPlace);
                }
                $response->results = $data;
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    /**
     * ws update place status
     *
     * @param string $token
     * @param int $user_id
     * @param string $place_id
     * @param int $status
     * @return json
     */
    function update_place_status($token = '', $user_id = 0, $place_id = '')
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $placeInfo = $this->Place_model->getPlace($place_id);
            if (isset($_POST) && count($_POST) > 0 && is_numeric($user_id) && is_numeric($_POST['Status']) && $placeInfo) {

                // update last activity
                // $this->User_model->updateLastActivity($user_id);

                $status = $_POST['Status'];
                $checkout_date = date('Y-m-d H:i:s', strtotime("+" . CHECKEDIN_DURATION . " hours", strtotime(gmdate('Y-m-d H:i:s'))));
                $check = $this->Place_model->getStatusOf($user_id, $place_id);

                if ($status == 1) {
                    $this->User_Checkin_model->update(array('Status' => $status, 'CheckInDate' => gmdate('Y-m-d H:i:s')), array('UserID' => $user_id, 'PlaceID' => $place_id));
                } elseif ($status == 2) {
                    $this->User_Checkin_model->update(array('Status' => $status, 'CheckOutDate' => gmdate('Y-m-d H:i:s')), array('UserID' => $user_id, 'PlaceID' => $place_id));
                }
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    function send_message($token = '', $user_id = 0, $place_id = '')
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $placeInfo = $this->Place_model->getPlace($place_id);

            $isUpPhoto = isset($_FILES['Photo']) && $_FILES['Photo']['size'] > 0;
            $errorContent = $isUpPhoto || (isset($_POST['Content']) && trim($_POST['Content']) != '');

            if ($errorContent && $placeInfo) {
                $photo = '';
                $photo100 = '';
                $photo200 = '';
                $photo300 = '';
                $error = false;
                if ($isUpPhoto) {
                    $isUpload = $this->s3upload->upload('Photo');
                    if (!$isUpload) {
                        $response->code = $this->lang->line('invalid_image');
                        $response->description = $this->s3upload->error;
                        $error = true;
                    } else {
                        $photo = $isUpload;
                        $p100 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 100, 150);
                        $photo100 = ($p100) ? $p100 : "";
                        $p200 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 200, 300);
                        $photo200 = ($p200) ? $p200 : "";
                        $p300 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 300, 450);
                        $photo300 = ($p300) ? $p300 : "";
                    }

                }

                if (!$error) {
                    // update last activity
                    // $this->User_model->updateLastActivity($user_id);
                    $createdDate = gmdate('Y-m-d H:i:s');
                    $data = array(
                        'PlaceID' => $place_id,
                        'UserID' => $user_id,
                        'Content' => isset($_POST['Content']) ? $_POST['Content'] : '',
                        'Photo' => $photo,
                        'Photo100' => $photo100,
                        'Photo200' => $photo200,
                        'Photo300' => $photo300,
                        'CreatedDate' => $createdDate
                    );

                    $id = $this->Place_User_Chat_Messages_model->add($data);
                    if ($id) {
                        $data['ID'] = $id;
                        if ($isUpPhoto) {
                            $data['Photo'] = $this->config->item('s3_photo_url') . $data['Photo'];
                            $data['Photo100'] = $this->config->item('s3_photo_url') . $data['Photo100'];
                            $data['Photo200'] = $this->config->item('s3_photo_url') . $data['Photo200'];
                            $data['Photo300'] = $this->config->item('s3_photo_url') . $data['Photo300'];
                        } else {
                            $data['Photo'] = "";
                            $data['Photo100'] = "";
                            $data['Photo200'] = "";
                            $data['Photo300'] = "";
                        }

                        $response->results = $data;
                        $response->code = $this->lang->line('ok');
                    }
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    function get_messages($token = '', $user_id = 0, $place_id = '', $message_id = -1, $position_flag = 2)
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $placeInfo = $this->Place_model->getPlace($place_id);
            if ($placeInfo) {
                // update last activity
                // $this->User_model->updateLastActivity($user_id);
                $reverse = false;
                if ($position_flag == 1) {
                    $reverse = true;
                }
                $messages = $this->Place_User_Chat_Messages_model->find50MessagesbyMessageID($place_id, $message_id, $reverse, $user_id);
                if ($messages) {
                    sort($messages);
                }
                $numOfChattingUser = $this->Place_User_Chat_Messages_model->countChattingUser($place_id);
                $results = new stdClass();
                $results->Messages = $messages;
                $results->NumOfChattingUser = $numOfChattingUser;
                $response->results = $results;
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    private function set_as_spectate_user($place_id, $user_id)
    {
        $checkout_date = date('Y-m-d H:i:s', strtotime("+" . CHECKEDIN_DURATION . " hours", strtotime(gmdate('Y-m-d H:i:s'))));
        $check = $this->Place_model->getStatusOf($user_id, $place_id);
        // add user request as spectate user 
        if (!$check) {
            $data = array(
                'UserID' => $user_id,
                'PlaceID' => $place_id,
                'Status' => 3, // spectate
                'CheckInDate' => gmdate('Y-m-d H:i:s'),
                'CheckOutDate' => $checkout_date
            );
            $this->User_Checkin_model->add($data);
        }
        /* else{
          if ($check->Status == 2 || ($check->Status == 1 && strtotime(gmdate('Y-m-d H:i:s')) > strtotime($check->CheckOutDate)) ){
          $this->User_Checkin_model->update(array('Status' => 3, 'CheckInDate' => gmdate('Y-m-d H:i:s'), 'CheckOutDate' => $checkout_date), array('UserID' => $user_id, 'PlaceID' => $place_id));
          }elseif ($check->Status == 3 && strtotime(gmdate('Y-m-d H:i:s')) > strtotime($check->CheckOutDate)){
          $this->User_Checkin_model->update(array('CheckInDate' => gmdate('Y-m-d H:i:s'), 'CheckOutDate' => $checkout_date), array('UserID' => $user_id, 'PlaceID' => $place_id));
          }
          } */
    }

    private function _pushNotify($user_id, $type = '', $from_user_id = 0, $content = '', $placename = "", $content_type = '', $content_id = '')
    {
        $pushStatus = 0;
        $userInfo = $this->User_model->getUserInfo($from_user_id);
        $msg = str_replace('%name%', $userInfo->UserName, $content);
        $msg = str_replace('%placename%', $placename, $msg);
        $avatarlink = '';
        if (isset($userInfo->Avatar) && !empty($userInfo->Avatar)) {
            $avatarlink = $this->config->item('s3_photo_url') . $userInfo->Avatar;
        }
        $device = $this->Devices_model->getDeviceOfUser($user_id);
        if ($device) {
            $targetUserInfo = $this->User_model->getUserInfo($user_id);
            $total = $this->Notification_History_model->getBadge($targetUserInfo);
            $logData['UserID'] = $user_id;
            $logData['CreatedDate'] = gmdate('Y-m-d H:i:s');
            // for ios
            if ($device->Type == 'ios') {
                $apnMsg['aps']['alert'] = $msg;
                $sound = 'abc.aiff';
                $apnMsg['aps']['sound'] = (string)$sound;

                $apnMsg['t'] = 20;
                $apnMsg['f'] = $from_user_id;
                $apnMsg['u'] = $user_id;
                $apnMsg['n'] = isset($userInfo->UserName) ? $userInfo->UserName : '';
                // $apnMsg['avatarURL'] = isset($avatarlink)?$avatarlink:'';
                //$apnMsg['message'] = $msg;
                //$apnMsg['gameType']	= "";
                //$apnMsg['anomotionID'] = "";
                $apnMsg['aps']['badge'] = (int)$total;
                $msgToPush = json_encode($apnMsg);
                $logData['Platform'] = 'ios';
                $logData['Message'] = $msgToPush;
                $pushStatus = $this->apns->_pushMessage($logData, $msgToPush, $device->DeviceID, $this->Config_model->getConfig('PUSH_NOTIFICATION_MODE'));
            }

            // for android
            if ($device->Type == 'android') {
                $types = array(
                    1 => 'Msg',
                    2 => 'Gift',
                    3 => 'Anomotion',
                    4 => 'Reveal',
                    5 => 'CommentPicturePost',
                    6 => 'CommentTextPost'
                );

                $json[] = $type;
                $json[] = isset($userInfo->UserName) ? $userInfo->UserName : '';
                $json[] = $msg;
                $json[] = $total;
                $json[] = isset($avatarlink) ? $avatarlink : '';
                $json[] = $from_user_id;
                $json[] = $content_type;
                $json[] = $content_id;
				$json[] = "";
                $json[] = "";
                $c2dmMsg = json_encode($json);

                $logData['Platform'] = 'android';
                $logData['Message'] = $c2dmMsg;
                $pushStatus = $this->ac2dm->send($device->DeviceID, 'text', $c2dmMsg, $logData);
            }
        }

        return $pushStatus;
    }

    function getCategoryOfPlace($place_id)
    {
        $serviceProvider = $this->Config_model->getConfig('SEARCH_SERVICE_PROVIDER');
        if ($serviceProvider == 'google') {
            $placeInfo = $this->Place_model->getPlace($place_id);
            if ($placeInfo) {
                if ($placeInfo->Provider == 'local') {
                    $category = explode("|", $placeInfo->Category);
                    return $category;
                }
                $venue = $this->searchvenue->getDetail($placeInfo->Reference);
            }
        } else {
            $venue = $this->searchvenue->getDetail($place_id);
        }
        $category = '';
        if ($venue) {
            $category = $venue['Category'];
        }

        // already check in GooglePlaceSearchProvider / getDetail
//		for ($i = 0; $i < count($category); $i++) {
//			if ($category[$i] == 'bus_station' || $category[$i] == 'doctor' || $category[$i] == 'dentist')
//				unset($category[$i]);
//		}
        return $category;
    }

    function get_comment($token = '', $post_id = 0)
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            $post = $this->Place_Post_model->findById($post_id);
            if ($post) {
                // update last activity
                // $this->User_model->updateLastActivity($user_id);

                // list comment of post
                $comments = array();
                $list_comment = $this->Post_Comment_model->getListCommentOf($post_id);
                foreach ($list_comment as $comment) {
                    if ($comment->Avatar) {
                        $comment->Avatar = $this->config->item('s3_photo_url') . $comment->Avatar;
                    }
//                    $comment->ConnectMeter = $this->User_model->getConnectMeter($comment->UserID, $user_id);
                    $comment->ConnectMeter = 0; // not use
                    $comments[] = get_object_vars($comment);
                }
                $response->Total = (int)count($list_comment);
                $response->result = $comments;
                $response->code = $this->lang->line('ok');
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    function delete_post($token = '', $post_id = 0)
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($post_id)) {
                $postInfo = $this->Place_Post_model->findById($post_id);
                if ($postInfo && $postInfo->UserID == $user_id) {
                    // delete this post & reference
                    $this->Place_Post_model->deletePost($post_id);
                    $response->code = $this->lang->line('ok');
                } else {
                    $response->code = $this->lang->line('invalid_request');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }

    function add_place($token = '')
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (isset($_POST) && !self::validateAddPlace()) {
                $data = array(
                    'Name' => $_POST['Name'],
                    'Address' => $_POST['Address'],
                    'Lat' => $_POST['Lat'],
                    'Lon' => $_POST['Lon'],
                    'Category' => isset($_POST['Category']) ? $_POST['Category'] : '',
                    'CreatedDate' => gmdate('Y-m-d H:i:s'),
                    'Provider' => 'local'
                );
                $this->load->model('lPlace_model');
                //genereate PlaceID for Local Place
                $placeid = $this->lPlace_model->generateLocalPlaceID();
                if (!isset($placeid)) {
                    $response->code = $this->lang->line('error');
                } else {
                    $data['PlaceID'] = $placeid;
                    $place_id = $this->lPlace_model->add($data);
                    if ($place_id) {
                        $response->PlaceID = $place_id;
                        $response->code = $this->lang->line('ok');
                    } else {
                        $response->code = $this->lang->line('error');
                    }
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
    }

    function validateAddPlace()
    {
        $error = false;
        if (!isset($_POST['Name']) || (isset($_POST['Name']) && trim($_POST['Name']) == '')) {
            $error = true;
        }
        if (!isset($_POST['Address']) || (isset($_POST['Address']) && trim($_POST['Address']) == '')) {
            $error = true;
        }
        if (!isset($_POST['Lat']) || (isset($_POST['Lat']) && trim($_POST['Lat']) == '')) {
            $error = true;
        }
        if (!isset($_POST['Lon']) || (isset($_POST['Lon']) && trim($_POST['Lon']) == '')) {
            $error = true;
        }
        return $error;
    }

    /**
     * for Find place ws (Add your neighborhood as the first spot on anomo pics)
     *
     * @param int $user_id
     * @return unknown
     */
    private function getNeighborhoodOfUser($user_id)
    {
        $data = array();
        $userInfo = $this->User_model->getUserInfo($user_id);
        if ($userInfo) {
            if ($userInfo->NeighborhoodID != NULL) {
                $nbhInfo = $this->Neighborhood_model->getInfo($userInfo->NeighborhoodID);
                if ($nbhInfo) {
                    $data['PlaceID'] = $nbhInfo->OGR_FID;
                    $data['Name'] = $nbhInfo->NAME;
                    $data['NumberOfUserCheckedIn'] = $this->Neighborhood_model->getNumberUserCheckedIn($nbhInfo->OGR_FID);
                    $data['NumberText'] = $this->Neighborhood_model->getTotalPostOf($nbhInfo->OGR_FID, 'text');
                    $data['NumberPicture'] = $this->Neighborhood_model->getTotalPostOf($nbhInfo->OGR_FID, 'photo');
                    $data['Address'] = '';
                    $data['Phone'] = '';
                    $data['Email'] = '';
                    $data['Website'] = '';
                    $data['Lat'] = '';
                    $data['Lon'] = '';
                    $data['Category'] = '';
                    $data['Distance'] = '';
                    $data['Type'] = 'neighborhood';
                    $data['IsCheckedIn'] = '1';
                    $data['Photo'] = base_url() . '/public/icons/neighborhood.png';
                }
            }
        }
        return $data;
    }

    /**
     * get Extend Local Place
     * use for
     * get_nearby_places,get_nearby_places_v2
     *
     * @param array Places: use for check existed places
     * @param lat,lon: user for get places near by
     * @param distance: search for distance
     * @param version: version of web service
     * */
    private function getLocalPlace_GetNearbyPlaces($places = array(), $lat, $lon, $fromlat, $fromlon, $tolat, $tolon, $distance, $page, $version)
    {
        $extPlace = array();
        $arrExt = array();
        if (isset($lat) && is_numeric($lat) && isset($lon) && is_numeric($lon)) {
            $extPlace = $this->Place_model->getLocalPlace($lat, $lon, $fromlat, $fromlon, $tolat, $tolon, $distance, '', $page, false);
            //get Local Place Detail
            if (count($extPlace) > 0) {
                $arrExt = array();
                foreach ($extPlace as $entry) {
                    $exists = 0;
                    if (isset($places) && count($places) > 0) {
                        for ($j = 0, $exists = 0; $j < count($places); $j++) {
                            if ($places[$j]['PlaceID'] == $entry->PlaceID) {
                                $exists = 1;
                                break;
                            }
                        }
                    }
                    if ($exists == 0) {
                        switch ($version) {
                            case 1:
                                $arrExt[] = array(
                                    'PlaceID' => isset($entry->PlaceID) ? $entry->PlaceID : "",
                                    'Name' => $entry->Name,
                                    'Address' => $entry->Address,
                                    'Phone' => isset($entry->Phone) ? $entry->Phone : "",
                                    'Email' => isset($entry->Email) ? $entry->Email : "",
                                    'Website' => isset($entry->Website) ? $entry->Website : "",
                                    'Lat' => $entry->Lat,
                                    'Lon' => $entry->Lon,
                                    'Category' => $this->Place_Category_model->returnListCategoryByName(explode("|", $entry->Category)),
                                    'Distance' => $this->Place_model->distance($lat, $lon, $entry->Lat, $entry->Lon),
                                    'NumberOfUserCheckedIn' => $this->Place_model->getNumberUserCheckedIn($entry->PlaceID)
                                );
                                break;
                            case 2:
                                $arrExt[] = array(
                                    'PlaceID' => isset($entry->PlaceID) ? $entry->PlaceID : "",
                                    'Name' => $entry->Name,
                                    'Address' => $entry->Address,
                                    'Phone' => isset($entry->Phone) ? $entry->Phone : "",
                                    'Email' => isset($entry->Email) ? $entry->Email : "",
                                    'Website' => isset($entry->Website) ? $entry->Website : "",
                                    'Lat' => $entry->Lat,
                                    'Lon' => $entry->Lon,
                                    'Category' => $this->Place_Category_model->returnListCategoryByName(explode("|", $entry->Category)),
                                    'Distance' => $this->Place_model->distance($lat, $lon, $entry->Lat, $entry->Lon),
                                    'NumberOfUserCheckedIn' => $this->Place_model->getNumberUserCheckedIn($entry->PlaceID),
                                    'Type' => 'place'
                                );
                                break;
                        }
                    }
                }
            }
        }
        return $arrExt;
    }

    /**
     * get Extend Local Place
     * use for
     * search_by_keyword,search_by_keyword_v2,
     *
     * @param array Places: use for check existed places
     * @param lat,lon: user for get places near by
     * @param distance: search for distance
     * @param version: version of web service
     * */
    private function getLocalPlace_SearchByKeyword($places = array(), $lat, $lon, $fromlat, $fromlon, $tolat, $tolon, $distance, $keyword, $page, $version)
    {
        $extPlace = array();
        $arrExt = array();
        if (isset($lat) && is_numeric($lat) && isset($lon) && is_numeric($lon)) {
            $extPlace = $this->Place_model->getLocalPlace($lat, $lon, $fromlat, $fromlon, $tolat, $tolon, $distance, $keyword, $page, false);
            //get Local Place Detail
            if (count($extPlace) > 0) {
                $arrExt = array();
                foreach ($extPlace as $entry) {
                    $exists = 0;
                    if (isset($places) && count($places) > 0) {
                        for ($j = 0, $exists = 0; $j < count($places); $j++) {
                            if ($places[$j]['PlaceID'] == $entry->PlaceID) {
                                $exists = 1;
                                break;
                            }
                        }
                    }
                    if ($exists == 0) {
                        switch ($version) {
                            case 1:
                                $arrExt[] = array(
                                    'PlaceID' => isset($entry->PlaceID) ? $entry->PlaceID : "",
                                    'Name' => $entry->Name,
                                    'Address' => $entry->Address,
                                    'Phone' => isset($entry->Phone) ? $entry->Phone : "",
                                    'Email' => isset($entry->Email) ? $entry->Email : "",
                                    'Website' => isset($entry->Website) ? $entry->Website : "",
                                    'Lat' => $entry->Lat,
                                    'Lon' => $entry->Lon,
                                    'Category' => $this->Place_Category_model->returnListCategoryByName(explode("|", $entry->Category)),
                                    'Distance' => $this->Place_model->distance($lat, $lon, $entry->Lat, $entry->Lon),
                                    'NumberOfUserCheckedIn' => $this->Place_model->getNumberUserCheckedIn($entry->PlaceID)
                                );
                                break;
                            case 2:
                                $arrExt[] = array(
                                    'PlaceID' => isset($entry->PlaceID) ? $entry->PlaceID : "",
                                    'Name' => $entry->Name,
                                    'Address' => $entry->Address,
                                    'Phone' => isset($entry->Phone) ? $entry->Phone : "",
                                    'Email' => isset($entry->Email) ? $entry->Email : "",
                                    'Website' => isset($entry->Website) ? $entry->Website : "",
                                    'Lat' => $entry->Lat,
                                    'Lon' => $entry->Lon,
                                    'Category' => $this->Place_Category_model->returnListCategoryByName(explode("|", $entry->Category)),
                                    'Distance' => $this->Place_model->distance($lat, $lon, $entry->Lat, $entry->Lon),
                                    'NumberOfUserCheckedIn' => $this->Place_model->getNumberUserCheckedIn($entry->PlaceID),
                                    'Photo' => $this->Place_Category_model->returnOnePhotoByName(explode("|", $entry->Category), $this->Config_model->getConfig('SEARCH_SERVICE_PROVIDER')),
                                );
                                break;
                        }
                    }
                }
            }
        }
        return $arrExt;
    }

    function post_to_place_sp($token = '', $user_id = '', $place_id = '')
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (((isset($_POST['Content']) && trim($_POST['Content']) != '') || (isset($_FILES['Photo']) && $_FILES['Photo']['size'] > 0))) {
                // post picture
                $error = false;
                $photo = '';
                $photo100 = '';
                $photo200 = '';
                $photo300 = '';
                if (isset($_FILES['Photo']) && $_FILES['Photo']['size'] > 0) {
                    $isUpload = $this->s3upload->upload('Photo');
                    if (!$isUpload) {
                        $response->code = $this->lang->line('invalid_image');
                        $response->description = $this->s3upload->error;
                        $error = true;
                    } else {
                        $photo = $isUpload;
                        $p100 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 100, 150);
                        $photo100 = ($p100) ? $p100 : "";
                        $p200 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 200, 300);
                        $photo200 = ($p200) ? $p200 : "";
                        $p300 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 300, 450);
                        $photo300 = ($p300) ? $p300 : "";
                    }
                }

                if (!$error) {
                    $placeInfo = $this->Place_model->getPlace($place_id);
                    $place_icon = '';
                    $place_cats = explode('|', $placeInfo->Category);
                    if (!empty($place_cats)) {
                        $place_icon = $place_cats[0];
                    }
                    $created_date = gmdate('Y-m-d H:i:s');
                    $data = array(
                        'UserID' => $user_id,
                        'UserName' => $tokenInfo->UserName,
                        'Avatar' => $tokenInfo->Avatar,
                        'BirthDate' => $tokenInfo->BirthDate,
                        'Gender' => $tokenInfo->Gender,
                        'Lat' => $tokenInfo->Lat,
                        'Lon' => $tokenInfo->Lon,
                        'PlaceID' => $place_id,
                        'PlaceName' => $placeInfo->Name,
                        'PlaceAddress' => $placeInfo->Address,
                        'PlaceIcon' => $this->Place_Category_model->getIconByName($place_icon),
                        'Content' => isset($_POST['Content']) ? trim($_POST['Content']) : "",
                        'Photo' => $photo,
                        'Photo100' => $photo100,
                        'Photo200' => $photo200,
                        'Photo300' => $photo300,
                        'CreatedDate' => $created_date,
                        'Type' => 1
                    );

                    $result = $this->Place_model->postToPlaceSP($data);
                    if (sizeof($result) > 0) {
                        if (isset($result['Return']) && $result['Return'] == 0) {
                            $response->code = $this->lang->line('invalid_request');
                        } else {
                            // return this post
                            $post = new stdClass();
                            $post->ID = $result['PostID'];
                            $post->Photo = $photo != '' ? $this->config->item('s3_photo_url') . $photo : '';
                            $post->Photo100 = $photo100 != '' ? $this->config->item('s3_photo_url') . $photo100 : '';
                            $post->Photo200 = $photo200 != '' ? $this->config->item('s3_photo_url') . $photo200 : '';
                            $post->Photo300 = $photo300 != '' ? $this->config->item('s3_photo_url') . $photo300 : '';
                            $post->PlaceID = $place_id;
                            $post->Content = isset($_POST['Content']) ? trim($_POST['Content']) : "";
                            $post->CreatedDate = $created_date;
                            $post->UserID = $user_id;
                            $post->UserName = $tokenInfo->UserName;
                            $post->Avatar = $tokenInfo->UserName ? $this->config->item('s3_photo_url') . $tokenInfo->Avatar : '';
                            $post->Gender = $tokenInfo->Gender;
                            $response->post = $post;
                            $response->code = $this->lang->line('ok');
                        }
                    } else {
                        $response->code = $this->lang->line('invalid_request');
                    }
                } else {
                    $response->code = $this->lang->line('invalid_request');
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    function check_in_sp($token = '', $user_id = '', $place_id = '', $status = 0)
    {
        $response = new stdClass();
        $tokenInfo = $this->Wstoken_model->getToken($token);
        if (!$tokenInfo) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            // check PlaceID and UserID
            if (!is_numeric($user_id) || !in_array($status, array(1, 3))) {
                $response->code = $this->lang->line('invalid_request');
            } else {

                // ANOMO-4981 -attach picture when check-in
                $error = false;
                $photo = '';
                $photo100 = '';
                $photo200 = '';
                $photo300 = '';
                if (isset($_FILES['Photo']) && $_FILES['Photo']['size'] > 0) {
                    $isPhoto = $this->s3upload->upload('Photo');
                    if (!$isPhoto) {
                        $response->code = $this->lang->line('invalid_image');
                        $response->description = $this->s3upload->display_errors('', '');
                        $error = true;
                    } else {
                        //$upload_data = $this->upload->data();
                        //$photo = $upload_data['file_name'];
                        $attachPhoto = true;
                        $photo = $isPhoto;
                        $photo100 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 100, 150);
                        $photo200 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 200, 300);
                        $photo300 = $this->s3upload->create_thumbnail($_FILES['Photo']['tmp_name'], $photo, 300, 450);
                    }
                }

                if ($error) {
                    $response->code = $this->lang->line('invalid_request');
                } else {
                    $placeInfo = $this->Place_model->getPlace($place_id);
                    $place_icon = '';
                    $place_cats = explode('|', $placeInfo->Category);
                    if (!empty($place_cats)) {
                        $place_icon = $place_cats[0];
                    }
                    $created_date = gmdate('Y-m-d H:i:s');
                    $checkout_date = date('Y-m-d H:i:s', strtotime("+" . CHECKEDIN_DURATION . " hours", strtotime(gmdate('Y-m-d H:i:s'))));

                    $data = array(
                        'UserID' => $user_id,
                        'UserName' => $tokenInfo->UserName,
                        'Avatar' => $tokenInfo->Avatar,
                        'BirthDate' => $tokenInfo->BirthDate,
                        'Gender' => $tokenInfo->Gender,
                        'Lat' => $tokenInfo->Lat,
                        'Lon' => $tokenInfo->Lon,
                        'PlaceID' => $place_id,
                        'PlaceName' => addslashes($placeInfo->Name),
                        'PlaceAddress' => addslashes($placeInfo->Address),
                        'PlaceIcon' => $this->Place_Category_model->getIconByName($place_icon),
                        'Content' => isset($_POST['Content']) ? addslashes(trim($_POST['Content'])) : "",
                        'Photo' => $photo,
                        'Photo100' => $photo100,
                        'Photo200' => $photo200,
                        'Photo300' => $photo300,
                        'CreatedDate' => $created_date,
                        'CheckOutDate' => $checkout_date,
                        'Status' => $status
                    );

                    $result = $this->User_Checkin_model->addSP($data);
                    if (sizeof($result) > 0) {
                        if (isset($result['Return']) && $result['Return'] == 0) {
                            $response->code = $this->lang->line('invalid_request');
                        } else {
                            if ($result['ID'] > 0) {
                                // return this post
                                $post = new stdClass();
                                $post->ID = $result['ID'];
                                $post->Photo = $photo != '' ? $this->config->item('s3_photo_url') . $photo : '';
                                $post->Photo100 = $photo100 != '' ? $this->config->item('s3_photo_url') . $photo100 : '';
                                $post->Photo200 = $photo200 != '' ? $this->config->item('s3_photo_url') . $photo200 : '';
                                $post->Photo300 = $photo300 != '' ? $this->config->item('s3_photo_url') . $photo300 : '';
                                $post->PlaceID = $place_id;
                                $post->Content = isset($_POST['Content']) ? trim($_POST['Content']) : "";
                                $post->CreatedDate = $created_date;
                                $post->UserID = $user_id;
                                $post->UserName = $tokenInfo->UserName;
                                $post->Avatar = $tokenInfo->UserName ? $this->config->item('s3_photo_url') . $tokenInfo->Avatar : '';
                                $post->Gender = $tokenInfo->Gender;
                                $response->post = $post;
                            }
                            $response->code = $this->lang->line('ok');
                        }
                    } else {
                        $response->code = $this->lang->line('invalid_request');
                    }
                }
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

    // ANOMO-6901 - add CountryName as new params
    function get_nearby_places_v3($token = '', $lat = '', $lon = '', $page = 1, $page_token = '')
    {
        $response = new stdClass();
        $user_id = $this->commonlib->checkValidToken($token);
        if ($user_id == -1) {
            $response->code = $this->lang->line('invalid_token');
        } else {
            if (is_numeric($page) && (is_numeric($lat) && is_numeric($lon) && $lat != 0 && $lon != 0) || ($lat == 'null' && $lon = 'null')) {
                
                 // POST method
                $country = isset($_POST['Country']) ? trim($_POST['Country']) : '';
                $city = isset($_POST['City']) ? trim($_POST['City']) : '';
                $state = isset($_POST['State']) ? trim($_POST['State']) : '';
                $error = 0;

                // ANOMO-8070 - if we cant get location will show Earth
                if ($lat == 'null' && $lon == 'null') {
                    $country = 'Earth';
                    $city = '';
                    $state = '';
                }
                
                // ANOMO-8116
                // client doesn't send Country, City, State
                // then get info from google
                if ($lat != 'null' && $lon != 'null' && empty($country) && empty($city) && empty($state)) {
                    $geoInfo = $this->Neighborhood_model->getCountryName($lat, $lon);
                    if ($geoInfo) {
                        $country = $geoInfo['country'];
                        $city = $geoInfo['city'];
                        $state = $geoInfo['state'];
                    } else {
                        // can not get location
                        // return fail
                        $error = 1;
                    }
                }
                
                if (!$error) {
                    $data = array(
                        'UserID' => $user_id,
                        'Lat' => $lat,
                        'Lon' => $lon,
                        'CountryName' => $country,
                        'City' => $city,
                        'State' => $state
                    );

                    $this->Neighborhood_model->sp_location($data);
                }
                // end ANOMO-8116
               

                $results = $this->searchvenue->searchByLocation($lat, $lon, $page, $page_token);
                if ($results) {
                    $response->pagetoken = $results['pagetoken'];
                    foreach ($results['results'] as $entry) {
                        $address = $entry['Address'];
                        if (preg_match('/^{{Province/', $entry['Address'])) {
                            $address = '';
                        }
                        $address = str_replace('{', '', $address);
                        $place_value = array(
                            'PlaceID' => $entry['ID'],
                            'Name' => $entry['Name'],
                            'Address' => $address,
                            'Lat' => $entry['Lat'],
                            'Lon' => $entry['Lon'],
                            'Reference' => $entry['Reference'],
                            'Category' => implode('|', $entry['Category'])
                        );

                        // insert new place
                        if ($this->Place_model->checkExistPlace($entry['ID']) == 0) {
                            $this->Place_model->addPlace($place_value);
                        }
                        $places[] = array(
                            'PlaceID' => $entry['ID'],
                            'Name' => $entry['Name'],
                            'Address' => $address,
                            'Phone' => $entry['Phone'],
                            'Email' => $entry['Email'],
                            'Website' => $entry['Website'],
                            'Lat' => $entry['Lat'],
                            'Lon' => $entry['Lon'],
                            'Category' => $this->Place_Category_model->returnListCategoryByName($entry['Category']),
                            'Distance' => $this->Place_model->distance($lat, $lon, $entry['Lat'], $entry['Lon']),
                            'NumberOfUserCheckedIn' => $this->Place_model->getNumberUserCheckedIn($entry['ID']),
                            'Type' => 'place'
                        );
                    }

                    //updated by Luong add Local Places
                    //identify the lat and lon of the last place
                    $places = $this->sortPlaceByDistance($places);

                    $index = count($places) - 1;
                    //if there are no page token, we will get local place between user's location and last place's location
                    if (isset($page_token) && empty($page_token) == false) {
                        $firstlat = $places[0]['Lat'];
                        $firstlon = $places[0]['Lon'];
                    } else {
                        $firstlat = $lat;
                        $firstlon = $lon;
                    }
                    $lastLat = $places[$index]['Lat'];
                    $lastLon = $places[$index]['Lon'];
                    $extPlace = $this->getLocalPlace_GetNearbyPlaces($places, $lat, $lon, $firstlat, $firstlon, $lastLat, $lastLon, 2, $page, 1);
                    if ($extPlace) {
                        $places = array_merge($places, $extPlace);
                    }
                    //End updated by Luong					

                    $response->TotalPage = $results['total'];

                    $placeSorts = $this->sortPlaceByDistance($places);
                    if (!isset($page_token) || (isset($page_token) && $page_token == '')) {
                        // add your neigborhood as first record
                        $neighborhood = self::getNeighborhoodOfUser($user_id);
                        if (count($neighborhood) > 0) {
                            array_unshift($placeSorts, $neighborhood);
                        }
                    }
                    $response->results = $placeSorts;

                    $response->code = $this->lang->line('ok');
                } else {
                    //updated by Luong add Local Places
                    //if there are no Places was found from the search provider 
                    //get Local Place depending on lat and lon of the user's input.
                    $extPlace = $this->getLocalPlace_GetNearbyPlaces(null, $lat, $lon, 0, 0, 0, 0, 2, $page, 1);
                    if ($extPlace) {
                        $places = $extPlace;
                        $totalplaces = $this->Place_model->getLocalPlace($lat, $lon, 0, 0, 0, 0, 2, '', $page, true);
                        $response->pagetoken = "";
                        $response->TotalPage = ceil($totalplaces / 10);

                        $placeSorts = $this->sortPlaceByDistance($places);
                        if ($page == 1) {
                            // add your neigborhood as first record
                            $neighborhood = self::getNeighborhoodOfUser($user_id);
                            if (count($neighborhood) > 0) {
                                array_unshift($placeSorts, $neighborhood);
                            }
                        }
                        $response->results = $placeSorts;

                        $response->code = $this->lang->line('ok');
                    } else {
                        $response->code = $this->lang->line('zero_results');
                    }
                    //End updated by Luong	 
                }
            } else {
                $response->code = $this->lang->line('invalid_request');
            }
        }
        echo json_encode($response);
        return json_encode($response);
    }

}

?>