<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['ok']					= 'OK';
$lang['invalid_token']		= 'INVALID_TOKEN';
$lang['user_existed']		= 'USER_EXISTED';
$lang['email_existed']		= 'EMAIL_EXISTED';
$lang['phone_existed']		= 'PHONE_EXISTED';
$lang['invalid_phone']		= 'INVALID_PHONE';
$lang['invalid_code']		= 'INVALID_CODE';
$lang['username_existed']       = 'USERNAME_EXISTED';
$lang['db_error']		= 'DB_ERROR';
$lang['zero_results']		= 'ZERO_RESULTS';
$lang['over_query_limit']	= 'OVER_QUERY_LIMIT';
$lang['request_denied']		= 'REQUEST_DENIED';
$lang['invalid_request']	= 'INVALID_REQUEST';
$lang['invalid_parameter']	= 'INVALID_PARAMETER';
$lang['timeout_request']	= 'TIMEOUT_REQUEST';
$lang['invalid_image']		= 'INVALID_IMAGE';
$lang['failed'] 			= 'FAIL';
$lang['undelivered'] 			= 'UNDELIVERED';
$lang['support'] 			= 'SUPPORT';
$lang['unsupport'] 			= 'UNSUPPORT';
$lang['verified'] 			= 'VERIFIED';
$lang['unverified'] 		= 'UNVERIFIED';
$lang['invalid_promo_code'] = 'INVALID_PROMO_CODE';
$lang['invalid_email_code'] = 'INVALID_EMAIL_CODE';
$lang['not_found']		    = 'NOT_FOUND';
$lang['email_unactive']		= 'EMAIL_NOT_VALIDATE';
$lang['ok_not_validate']	= 'OK_BUT_EMAIL_NOT_VALIDATE';
$lang['invalid_email'] 		= 'INVALID_EMAIL';
$lang['block']				= 'BLOCK';
$lang['password_sent']		= 'PASSWORD_SENT';
$lang['request_sent'] 		= 'REQUEST_SENT';
$lang['not_enough_credits']	= 'NOT_ENOUGH_CREDITS';
$lang['invalid_receipt']	= 'INVALID_RECEIPT';
$lang['mark_limit']			= 'MARK_LIMIT';
$lang['receipt_existed'] 	= 'RECEIPT_EXISTED';
$lang['request_waiting']	= 'REQUEST_WAITING';
$lang['no_question']		= 'NO_QUESTION';
$lang['accept']				= 'ACCEPT';
$lang['cancel']				= 'CANCEL';
$lang['timeout']			= 'TIMEOUT';
$lang['waiting']			= 'WAITING';
$lang['no_more']			= 'NO_MORE';
$lang['inprogress']			= 'IN_PROGRESS';
$lang['finished']			= 'FINISHED';
$lang['answer_sent']		= 'ANSWER_SENT';
$lang['blocked']			= 'BLOCK_TARGET';
$lang['target_user_blocked']= 'BLOCK';
$lang['no_result']			= 'NO_RESULT';
$lang['revealed']			= 'REVEALED';
$lang['unavailable_link'] 	= 'UNAVAILABLE_LINK';
$lang['not_register']		= 'NOT_REGISTER';
$lang['ok_but_no_email'] 	= 'OK_BUT_NO_EMAIL';
$lang['no_email']			= 'NO_EMAIL';
$lang['not_activate'] 		= 'NOT_ACTIVATE';
$lang['ok_but_not_activate'] = 'OK_BUT_NOT_ACTIVATE';
$lang['invalid']			= 'INVALID';
$lang['fb_id_existed']		= 'FACEBOOK_ID_EXISTED';
$lang['error'] = 'ERROR';
$lang['flag_added']	= 'FLAG_ADDED';
$lang['shared']	= "SHARED";
$lang['invalid_birthdate'] = 'INVALID_BIRTHDATE';

$lang['fb_account'] = 'FB_ACCOUNT';

$lang['completed']  = 'COMPLETED';

$lang['banned']		= 'BANNED';
$lang['deactive']	= 'DEACTIVE';
$lang['announcement_closed'] = 'ANNOUNCEMENT_CLOSED';
$lang['invalid_username'] = 'INVALID_USERNAME';

$lang['block_change_user_name']	= 'NOT_ALLOW_CHANGE_USER_NAME';

$lang['play_icebreaker'] = 'PLAY_ICEBREAKER';
$lang['wait_for_next_match'] = 'WAIT_FOR_NEXT_MATCH';

$lang['comment_feed'] = '%name% just comment on your activity';
$lang['like_feed'] = '%name% just like on your activity';

$lang['commented_post'] = '%name% commented on your post';
$lang['commented_checkin'] = '%name% commented on your check-in ';

$lang['commented_owner_post'] = "%name% commented on %owner%'s post";
$lang['commented_owner_anonymous'] = "Anonymous commented on his/her post";
$lang['commented_owner_checkin'] = "%name% commented on %owner%'s check-in";

$lang['like_post'] = '%name% likes your post';
$lang['like_checkin'] = '%name% likes your check-in';

$lang['push_notify_follow'] = '%name% is now following you on UW Social';

$lang['msg_mention_user'] = '%username% mentioned you in a post. Check it out';
$lang['msg_ageless_post'] = '%username% just made a fan page post';

$lang['like_on_comment'] = '%name% just liked your comment';

$lang['post_on_fanpage'] = '%name% posted on your fan page';

// activity msgs 
$lang['feed_checkin'] = "%username% has checked in in %placename%";
$lang['feed_post_picture_place'] = "%username% has posted a picture in %placename%";

$lang['msg_revealed'] = '%username% has revealed "%revealinfo%" to you';
$lang['msg_reveal_request']	= '%username% wants you to reveal something to them';
$lang['msg_reveal_accept']	= '%username% just revealed info about %revealinfo% to you ';

$lang['msg_send_gift'] = '%name% has sent you a gift';

$lang['msg_anomotion_accept']	= ' agreed your game request';
$lang['msg_anomotion_result']	= 'You have a new Ice Breaker result from ';
$lang['msg_anomotion_request']	= 'You have a new Ice Breaker request';
$lang['msg_anomotion_finished']	= '%username% finished playing Ice Breaker';
$lang['msg_anomotion_completed']= '%username% and you have completed game Ice Breaker';

$lang['msg_push']			= 'You have %1$@ messages, %2$@ gifts, %3$@ reveals';
$lang['msg_push0']			= 'You have ';
$lang['msg_push1']			= '%1$@ messages ';
$lang['msg_push2']			= '%2$@ gifts ';
$lang['msg_push3']			= '%3$@ reveals';

$lang['msg_reveal'] 		= "Hey %profile_name%, I am ready to come clean!  My name is %real_name%. ";
$lang['msg_reveal1'] = "I'd love to talk to you more so feel free to call me at %phone%. ";
$lang['msg_reveal2'] = "You can also check me out on Facebook, look me up with my email address %fbemail%. ";
$lang['msg_reveal3'] = " Looking forward to chatting with you soon.
No Longer Anomo ...
%real_name%";


// confirm email
$lang['text_confirm_success'] = 'Thank you for completing your Anomo registration.';
$lang['text_confirm_fail'] = 'The invalid request. Please try again';
$lang['text_confirmed'] = 'This email is confirmed. Thanks';

// send email when register
$lang['register_email_subject'] = '[Anomo] Validation Email';
$lang['register_title'] = '<p> Hello, </p> <p>Please click <a href="%s">here</a> to validate your email address.</p>';
$lang['register_body'] = '<p>It�s important for us to know that you are a human! Anomo is completely anonomys, you have our word on
that. Your email will be kept strictly confidential and is only used for login purposes.</p>';
$lang['register_footer'] = '<p>Thanks,</p><p>Team Anomo</p><p><i>Anomate Your Experiences, Place by Place</i></p>';

// remind the user to validate their email
$lang['remind_email_subject'] = '[Anomo] Remind validation email';
$lang['remind_title'] = '<p>Hey %name%</p>';
$lang['remind_body'] = '<p>This is your last chance to validate your email address. Please validate in order to continue using Anomo.</p>
					<p>There is a chance the valdation email went to your spam folder. Please make sure you add
					friends@anomo.com to your safe senders list.</p>
					<p>Click <a href="%resend%">here</a> if you need us to resend the validation to you.</p>';
$lang['remind_footer'] = '<p>Team Anomo</p><p><i>Anomate Your Experiences, Place by Place</i></p>';

// forgotten password
$lang['forgotten_password_subject'] = '[UW Social] Forgot password';
$lang['forgotten_password_title'] = '<p>Hello %name%</p>';
$lang['forgotten_password_body'] = '<p>You recently initiated a password reset for your UW Social account.
                                    To complete the process, click the link below.</p>
                                    <p>%link%</p>';
$lang['forgotten_password_footer'] = '<p>Team UW Social</p>';
$lang['send_image_notification'] = 'sent you a photo'; // ANOMO-6823
$lang['comment_post'] = '%name% just comment on your post at %placename%';


$lang['invite_subject'] = '[UW Social] - Invite friend';
$lang['invite_msg'] = "This is msg invite";
$lang['sms_verify_msg'] = "UW Social unlock code: %code%";
$lang['anomo_matched_us'] = "UW Social matched us";
$lang['anomo_matched_us_msg'] = "A friend match was found. Check out what both of you have in common.";
$lang['msg_new_match'] = 'You have an UW Social match. See who it is!';
$lang['topic_exist'] = 'TOPIC_EXISTED';
$lang['topic_banned'] = 'TOPIC_BANNED';
$lang['not_allow_create_topic'] = 'NOT_ALLOW_CREATE_TOPIC';
$lang['maximum_topic_allowed'] = 'MAXIMUM_TOPIC_ALLOWED';

$lang['canot_find_match'] = '[UW Social] Cannot find match result';
$lang['staging_canot_find_match'] = '[UW Social Staging] Cannot find match result';

?>