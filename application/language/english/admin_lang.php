<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['title'] = 'Title';
$lang['description'] = 'Description';
$lang['icon'] = 'Icon';
$lang['category'] = 'Category';
$lang['num_of_min_item'] = 'Num of min item';
$lang['num_of_max_item'] = 'Num of max item';
$lang['is_actived'] = "is active";
$lang['is_featured'] = 'is featured';
$lang['register_email_subject'] = 'Please confirm your account';
$lang['register_email_msg'] = "Please click the following link to confirm your account";
$lang['confirm_success'] =  "Your account has been successfully confirmed" ;
$lang['confirm_fail']= "The Account associated with your Confirmation Link was not found";
$lang['password_reset'] = "Password reset";
$lang['pass_reset1'] = "Someone has asked to reset the password for the following site and username.";
$lang['pass_reset2'] = "To reset your password visit the following address, otherwise just ignore this email and nothing will happen.";
$lang['localplaceid_fail'] = "Local PlaceID generation has failed. Please try again";
?>