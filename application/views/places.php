<h1 id="Places" style="margin:1em 0 0 -10px;">Places</h1>
<p>Description for Places.</p>

<ol class="toc">
	<li><a href="#SearchingPlaces">Get nearby place list from a user with user location</a></li>
	<li><a href="#SearchingPlacesV2">Get nearby place list from a user with user location V2(add Neighborhood as first record)</a></li>
	<li><a href="#SearchByKeyword">Search place with keyword from a user with user location</a></li>
	<li><a href="#SearchByKeywordV2">(UPDATE) Search place with keyword from a user with user location V2 (with post method)</a></li>
	<li><a href="#SearchByKeywordV3">Search place with keyword from a user with user location V3 (with post method & add Neighborhood as first record</a></li>
	<li><a href="#SearchPeople">Search (checkin/spectate)people nearby from a location </a></li>
	<li><a href="#GetPlaceDetail">Get A Place Detail</a></li>
	<li><a href="#GetPlaceDetailV2">Get A Place Detail V2</a></li>
	<li><a href="#GetPlaceDetailV3">(NEW)Get A Place Detail V3</a></li>
	<li><a href="#CheckIns">(UPDATE) Check In / Spectate</a></li>
	<li><a href="#CheckOut">Check Out / Unspectate</a></li>
	<li><a href="#SendMsgPlace">(UPDATE) Post message/picture to place</a></li>
	<li><a href="#PostComment">(UPDATE) Post a comment to a post of place</a></li>
	<li><a href="#ListActivity">Get list activity</a></li>
	<li><a href="#AnomoPick">getAnomoPick</a></li>
	<li><a href="#AnomoPickV2">(UPDATE) Get Anomo Pick V2</a></li>
	<li><a href="#GetTextWall">Get Text Wall</a></li>
	<li><a href="#GetPictureWall">Get Picture Wall</a></li>
	<li><a href="#GetPictureWallV2">(NEW) Get Picture Wall v2</a></li>
	<li><a href="#GetWallPost">Get wall post</a></li>
	<li><a href="#GetWallPostV2">(NEW) Get wall post v2</a></li>
	<li><a href="#GetAnomos">Get Anomos</a></li>
	<li><a href="#GetAnomosV2">(NEW) Get Anomos v2</a></li>
	<li><a href="#GetAnomoByTag">Get list anomo base on tag web service</a></li>
	<li><a href="#GetUserStatus">Get user status</a></li>
	<li><a href="#GetTags">Get tags</a></li>
	<li><a href="#GetRandom">Get random</a></li>
	<li><a href="#GetRandomV2">Get random V2</a></li>
	<li><a href="#GetSpectatePlace">Get spectate place</a></li>
	<li><a href="#PrepareChatOnPlace">Prepare Chat On Place</a></li>
	<li><a href="#UpdatePlaceStatus">(NEW) Update place status</a></li>
	<li><a href="#SendMessageInPlace">(NEW) Send message in place</a></li>
	<li><a href="#GetMessagesInPlace">(NEW) Get messages in place</a></li>
	<li><a href="#CommentOfPost">Get comment of post</a></li>
	<li><a href="#AddLocalPlace">Add local place</a></li>
	<li><a href="#DeletePost">Delete post</a></li>
</ol>

<h2 id="SearchingPlaces">Get nearby place list from a user with user location</h2>
<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_nearby_places');?>/#token/#lat/#lon/#page/#pageToken</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>

<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>lat</code> (<em>required</em>) - The latitude/longitude around which to retrieve Place information. This must be specified as <em>latitude</em>,<em>longitude</em></li>
  <li><code>lon</code> (<em>required</em>) - ...</li>
  <li><code>page</code> (<em>optional</em>) - default page is 1</li>
  <li><code>pageToken</code> (<em>optional</em>) - (just for Google search -  Returns the next 20 results from a previously run search)</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_nearby_places');?>/IWPMXYTFVCVXVWYAPRB9/34/-118</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint">
<span class="str">{"pagetoken":"ClREAAAAd-_Y69fJEXNRFHt5xW3eXhasAbdYWOcr7eC2ekl0pUnDrhcEO6uptgy9lkfUuZifCfd-6iXdxjq87_9PvgzXWoTtl0qzMhOb_iC3DoXKJAgSEMj_23_G_nqYEKBuF5ptwUMaFK2azi9RCa0VFi74RrOWtxy2c_Ml","TotalPage":4,"results":[{"PlaceID":"8b41bf70-ad3e-4562-9b68-ebf36e8d66ac","Name":"Lasser Realty","Address":"14161 Skyline Dr","Phone":"(626) 330-3000","Email":null,"Website":null,"Lat":33.999063,"Lon":-118.00504,"Category":[{"Name":"Real Estate & Home Improvement","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.296","NumberOfUserCheckedIn":0},{"PlaceID":"c829191f-02b2-45eb-9e4a-31e4fd37ea7b","Name":"Emerald Mountain View Investment Incorporation","Address":"14545 Blue Sky Rd","Phone":"(562) 693-7888","Email":null,"Website":null,"Lat":33.995439,"Lon":-117.993826,"Category":[{"Name":"Legal & Financial > Financial Planning & Investments","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.474","NumberOfUserCheckedIn":0},{"PlaceID":"460a72eb-076d-4056-afd5-16f60305c912","Name":"Artistic Ceramics","Address":"14816 Edgeridge Dr","Phone":"(562) 201-2344","Email":null,"Website":null,"Lat":33.999902,"Lon":-117.991681,"Category":[{"Name":"Travel & Tourism > Monuments","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.477","NumberOfUserCheckedIn":0},{"PlaceID":"6a399a44-2a33-49d7-ab65-276390604c19","Name":"Diversified Disposal Service","Address":"1947 Old Canyon Dr","Phone":"(562) 698-8799","Email":null,"Website":null,"Lat":34.007569,"Lon":-117.998611,"Category":[{"Name":"Community & Government > Environmental Conservation & Ecological Organizations","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.529","NumberOfUserCheckedIn":0},{"PlaceID":"26b897e4-2f14-4094-8cba-f67565910237","Name":"Go Motors","Address":"14725 Finisterra Pl","Phone":"(562) 693-5090","Email":null,"Website":null,"Lat":33.994249,"Lon":-117.993329,"Category":[{"Name":"Automotive > Car Parts & Accessories","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.551","NumberOfUserCheckedIn":0},{"PlaceID":"ca493ea0-656a-4540-89fc-8370cea7ca4b","Name":"Nic","Address":"14725 Finisterra Pl","Phone":"(714) 237-7733","Email":null,"Website":null,"Lat":33.994219,"Lon":-117.993329,"Category":[{"Name":"Automotive > Car Parts & Accessories","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.553","NumberOfUserCheckedIn":0},{"PlaceID":"e871a794-50c4-4c14-83fa-d00d263d8796","Name":"M and M Motors","Address":"14457 Eadbrook Dr","Phone":"(626) 968-3353","Email":null,"Website":null,"Lat":34.007747,"Lon":-117.996207,"Category":[{"Name":"Automotive > Car Dealers","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.578","NumberOfUserCheckedIn":0},{"PlaceID":"c7f22ea6-1520-49bd-84cb-462a51a8f8f0","Name":"Bejarano Family Partnership","Address":"14533 Langhill Dr","Phone":"(626) 330-9627","Email":null,"Website":null,"Lat":34.009266,"Lon":-117.997235,"Category":[{"Name":"Real Estate & Home Improvement > Real Estate Agents","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.659","NumberOfUserCheckedIn":0},{"PlaceID":"4987a4ec-27ee-4865-a3be-f701feb6859a","Name":"Putnam International","Address":"2250 Turnbull Canyon Rd","Phone":"(626) 336-0932","Email":null,"Website":null,"Lat":34.00191,"Lon":-117.98708,"Category":[{"Name":null,"Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.752","NumberOfUserCheckedIn":0},{"PlaceID":"6bcbd92d-7bdb-41a9-8125-f14888195d7c","Name":"Orange Grove Middle School","Address":"14505 Orange Grove Ave","Phone":"(626) 933-7001","Email":null,"Website":"http:\/\/www.orange-grove.hlpusd.k12.ca.us\/","Lat":34.010235,"Lon":-117.99524,"Category":[{"Name":"Education > Schools","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.758","NumberOfUserCheckedIn":0},{"PlaceID":"07218730-c0a8-4922-8c4e-4f208e36ee7c","Name":"Orange Grove Elementary School","Address":"10626 Orange Grove Ave","Phone":"(562) 789-3200","Email":null,"Website":null,"Lat":34.0094,"Lon":-117.9927,"Category":[{"Name":"Education > Schools","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.772","NumberOfUserCheckedIn":0},{"PlaceID":"473ad4f0-b40f-4482-83d6-e5059bc190b4","Name":"Hacienda Pipeline","Address":"14621 Orange Grove Ave","Phone":"(626) 333-8386","Email":null,"Website":null,"Lat":34.00974,"Lon":-117.99307,"Category":[{"Name":"Real Estate & Home Improvement > Plumbing","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.781","NumberOfUserCheckedIn":0},{"PlaceID":"ad13e314-49dc-43ad-a7d7-4ffde5aac1a1","Name":"Pacific Grading","Address":"15163 Los Altos Dr","Phone":"(562) 694-2288","Email":null,"Website":null,"Lat":33.993233,"Lon":-117.988004,"Category":[{"Name":"Real Estate & Home Improvement","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.831","NumberOfUserCheckedIn":0},{"PlaceID":"752fab78-73bc-4f23-8fd3-1ac165e3d49d","Name":"Pacific Grating","Address":"15163 Los Altos Dr","Phone":"(626) 446-0809","Email":null,"Website":null,"Lat":33.99324,"Lon":-117.98798,"Category":[{"Name":null,"Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.832","NumberOfUserCheckedIn":0},{"PlaceID":"cef1c527-e8f8-4bc5-ae90-a53b191e4779","Name":"Warehouse Equipment Erectors","Address":"1631 Doverfield Ave","Phone":"(626) 333-5165","Email":null,"Website":null,"Lat":34.010559,"Lon":-117.992241,"Category":[{"Name":"Business & Professional Services > Equipment, Supplies & Services > Industrial Machinery & Vehicles","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.854","NumberOfUserCheckedIn":0},{"PlaceID":"4eb7e90e-4d5f-4e0d-95df-dfb1bb95a762","Name":"Friends of Senator Alarcon","Address":"14704 Lujon St","Phone":"(626) 369-5923","Email":null,"Website":null,"Lat":34.010423,"Lon":-117.990687,"Category":[{"Name":"Community & Government > Organizations & Associations","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.896","NumberOfUserCheckedIn":0},{"PlaceID":"06e3fdc5-5d86-4d93-b36f-b18e314c8a6b","Name":"White Dove Releases","Address":"1549 7th Ave","Phone":"(626) 330-2129","Email":null,"Website":"http:\/\/whitedoverelease.com\/","Lat":34.011558,"Lon":-117.9928,"Category":[{"Name":"Shopping > Pet Shops & Pet Supplies","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.899","NumberOfUserCheckedIn":0},{"PlaceID":"2c49ae72-716d-4c5b-a85b-6d2ac8f666f4","Name":"Allview Image of America","Address":"2578 Daytona Ave","Phone":"(626) 968-3798","Email":null,"Website":"http:\/\/www.allviewmirror.com\/","Lat":33.996429,"Lon":-117.9847,"Category":[{"Name":"Automotive > Car Parts & Accessories","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.910","NumberOfUserCheckedIn":0},{"PlaceID":"ca41f99e-062b-4e1d-bdb8-bd7014112621","Name":"Ambience Sound and Video","Address":"2588 Daytona Ave","Phone":"(626) 968-4446","Email":null,"Website":null,"Lat":33.996524,"Lon":-117.984649,"Category":[{"Name":"Business & Professional Services > Equipment, Supplies & Services > Audiovisual","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.912","NumberOfUserCheckedIn":0},{"PlaceID":"0fdab88d-8db3-4be8-992d-6d7c0652227d","Name":"Lotus Arts and Gifts","Address":"14428 Crystal Lantern Dr","Phone":"(626) 968-7875","Email":null,"Website":null,"Lat":34.013171,"Lon":-117.996217,"Category":[{"Name":"Shopping > Gift & Novelty Stores","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.935","NumberOfUserCheckedIn":0}],"code":"OK"}
</span></pre>

<p>A JSON response contains three root elements:</p>
<ul>
  <li><code>"pagetoken"</code> contains a token that can be used to return up to 20 additional results.</li>
  <li><code>"TotalPage"</code> total page</li>
  <li><code>"results"</code> contains an array of Places, each place include (PlaceID, Name, Address, Distance (miles), NumberOfUserCheckedIn)</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#PlaceSearchStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PlaceSearchStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> indicates that no errors occurred; the place was successfully detected and at least one result was returned.</li>
  <li><code>ZERO_RESULTS</code> indicates that the search was successful but returned no results. This may occur if the search was passed a <code>latlng</code> in a remote location.</li>
  <li><code>DB_ERROR</code> - indicates that database error.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>

<h2 id="SearchingPlacesV2">Get nearby place list from a user with user location V2(add Neighborhood as first record)</h2>
<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_nearby_places_v2');?>/#token/#lat/#lon/#page/#pageToken</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>

<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>lat</code> (<em>required</em>) - The latitude/longitude around which to retrieve Place information. This must be specified as <em>latitude</em>,<em>longitude</em></li>
  <li><code>lon</code> (<em>required</em>) - ...</li>
  <li><code>page</code> (<em>optional</em>) - default page is 1</li>
  <li><code>pageToken</code> (<em>optional</em>) - (just for Google search -  Returns the next 20 results from a previously run search)</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_nearby_places_v2');?>/IWPMXYTFVCVXVWYAPRB9/34/-118</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint">
<span class="str">{"pagetoken":"CqQEGwIAAKhmBbwYDNwf0-_R4q1EuVposXsFCAy903GiChJF39f4sXA6TYvHilxKE_MXAIQ0MRMJ_0lpRUX_PRRsLeKG1FI-1eV3efc0Z_48mklyK_iWP29s1f7vkzJPjXr854lK6b9TJRmLPwZh6D1RI-ZuI2qmTQNkBULFN9h8jL0XMpbheWRHFy5CFyMkPOJCuq_CieYMzjQCr87jE9dunGcIYsZIXCUORKiSa_BDyWt1rtxO1Nlw2NKLa7gmoD-AhA-n9XTImFY-1o_-A86lgjeeNwmFtL4SZUdhw8r-v2R8J7pyPNfiFVv8n_ASm7URpnjDjOdU0zmINOlAxTBvoMWKXB3qSmw_wvOAJE4Il9bWepL_rinCP4r4ftvGgczk9cmhyAjlkXTkhSxagbkjuf_6yGIFFnWSq7RZzd-lOfOGlXXQgOykTR5tqiIUaFpzsTE_0tMp7ov634X2QD---nemCnJpBHq8J9ZkxkWFjl1Y7e2EOgrx5INtvAt2ZxQXAbb3TX4BiMP3bWs2_NYWgvLGoXVM1Gt6vxtC1Fk-a72UpdBAvMk0-z2vhQ9tbggY2GZuG9GHZKSyB9KJTlbdEIsbokPcGUUbtYItTCGsJprnVu_5Oz-I-Apym6osHpzLfT3kmAFuaJpnAzEdfYxjPa0jGSP7zApiJIBlXCCcEAfdTQ7M9Aul0Oeo1tDzbKRfV_rHqyXE2zPwMoLhaX23gpoCve4SEHXwvX4h-Jffk21LsGl_GykaFA2aRC9MLDEl0H0rPUJrI2A3mgf8","TotalPage":1,"results":[{"PlaceID":"2296","Name":"Industry","NumberOfUserCheckedIn":3,"Address":"","Phone":"","Email":"","Website":"","Lat":"","Lon":"","Category":"","Distance":"","Type":"neighborhood"},{"PlaceID":"b1faeb773f084c2f919030100a945a0d92fd4e0f","Name":"Steinmetz Park","Address":"1545 S Stimson Ave, Hacienda Heights","Phone":"","Email":null,"Website":"","Lat":34.003008,"Lon":-118.000975,"Category":[{"Name":"park","Icon":"http:\/\/localhost\/anomo\/public\/icons\/park.png"},{"Name":"establishment","Icon":"http:\/\/localhost\/anomo\/public\/icons\/establishment.png"}],"Distance":"0.215","NumberOfUserCheckedIn":0,"Type":"place"}, ... ],"code":"OK"}
</span></pre>

<p>A JSON response contains three root elements:</p>
<ul>
  <li><code>"pagetoken"</code> contains a token that can be used to return up to 20 additional results.</li>
  <li><code>"TotalPage"</code> total page</li>
  <li><code>"results"</code> contains an array of Places, each place include (PlaceID, Name, Address, Distance (miles), NumberOfUserCheckedIn, Type (place / neighborhood))</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#PlaceSearchStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PlaceSearchStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> indicates that no errors occurred; the place was successfully detected and at least one result was returned.</li>
  <li><code>ZERO_RESULTS</code> indicates that the search was successful but returned no results. This may occur if the search was passed a <code>latlng</code> in a remote location.</li>
  <li><code>DB_ERROR</code> - indicates that database error.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="SearchByKeyword">Search place with keyword from a user with user location</h2>
<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/search_by_keyword');?>/#token/#lat/#lon/#keyword/#page/#pageToken</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>

<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>lat</code> (<em>required</em>) - The latitude/longitude around which to retrieve Place information. This must be specified as <em>latitude</em>,<em>longitude</em></li>
  <li><code>lon</code> (<em>required</em>) - ...</li>
  <li><code>keyword</code> (<em>required</em>) - ...</li>
  <li><code>page</code> (<em>optional</em>) - default page is 1</li>
  <li><code>pageToken</code> (<em>optional</em>) - (just for Google search -  Returns the next 20 results from a previously run search)</li>
  
</ul>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/search_by_keyword');?>/IWPMXYTFVCVXVWYAPRB9/34/-118/sushi</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint">
<span class="str">{"pagetoken":"","TotalPage":2,"results":[{"PlaceID":"1ea69649-c722-4ce4-a506-01e4aa13dd15","Name":"Sushi Land","Address":"2129 S Hacienda Blvd","Phone":"(626) 369-1143","Email":null,"Website":null,"Lat":33.995064,"Lon":-117.96853,"Category":[{"Name":"Food & Beverage > Restaurants > Sushi","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"1.835","NumberOfUserCheckedIn":0},{"PlaceID":"3566f3f3-adda-4784-a871-2ad935478cf3","Name":"Sushi Umi","Address":"3135 S Hacienda Blvd","Phone":"(626) 934-7731","Email":null,"Website":null,"Lat":33.980381,"Lon":-117.973,"Category":[{"Name":"Food & Beverage > Restaurants","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"2.057","NumberOfUserCheckedIn":0},{"PlaceID":"3fd3a80c-3798-45a5-8871-ecc445f4de28","Name":"Sushimax","Address":"7007 Greenleaf Ave","Phone":"(562) 698-4047","Email":null,"Website":null,"Lat":33.978725,"Lon":-118.037366,"Category":[{"Name":"Food & Beverage > Restaurants","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"2.597","NumberOfUserCheckedIn":0},{"PlaceID":"3270cba8-752c-48d3-addc-23c41b4a7cc8","Name":"Sushi from East Restaurant","Address":"15334 Whittier Blvd","Phone":"(562) 943-8997","Email":null,"Website":null,"Lat":33.94655,"Lon":-117.99996,"Category":[{"Name":"Food & Beverage > Restaurants","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"3.693","NumberOfUserCheckedIn":0},{"PlaceID":"c66e080d-420c-4fce-945a-57623f1594d8","Name":"Sushi Iwa","Address":"15334 Whittier Blvd","Phone":"(562) 943-8997","Email":null,"Website":null,"Lat":33.946548,"Lon":-117.999962,"Category":[{"Name":null,"Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"3.693","NumberOfUserCheckedIn":0},{"PlaceID":"2b6fd3d0-0355-012f-b33e-003048cacd48","Name":"Sushi Tokyo","Address":"12214 La Mirada Blvd","Phone":"(562) 943-3507","Email":null,"Website":"http:\/\/www.sushitk.com","Lat":33.919304,"Lon":-118.011818,"Category":[{"Name":"Food & Beverage > Restaurants","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"5.616","NumberOfUserCheckedIn":0},{"PlaceID":"f87570a9-fed2-4e12-b809-79a1b50f5dfd","Name":"Sushi","Address":"1864 Montebello Town Ctr","Phone":"(323) 720-1955","Email":null,"Website":null,"Lat":34.0392,"Lon":-118.091003,"Category":[{"Name":"Food & Beverage > Restaurants > Sushi","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"5.873","NumberOfUserCheckedIn":0},{"PlaceID":"962f4178-84f2-4a4f-a7b1-d42df53c22ab","Name":"Sushi Nagano","Address":"2560 E Amar Rd","Phone":"(626) 581-4148","Email":null,"Website":null,"Lat":34.0273832,"Lon":-117.8963925,"Category":[{"Name":"Food & Beverage > Restaurants > Sushi","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"6.228","NumberOfUserCheckedIn":0},{"PlaceID":"d65ae0ca-341f-4bd6-8f07-693791238e7a","Name":"Sushi Boy","Address":"1420 S Azusa Ave","Phone":"(626) 918-1299","Email":null,"Website":"http:\/\/sushiboy.net\/","Lat":34.048563,"Lon":-117.908102,"Category":[{"Name":"Food & Beverage > Restaurants > Sushi","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"6.241","NumberOfUserCheckedIn":0},{"PlaceID":"2ea9890d-4aae-4425-a153-f29b20f18882","Name":"Sushi Hero","Address":"15288 Rosecrans Ave","Phone":"(714) 994-6933","Email":null,"Website":null,"Lat":33.89875,"Lon":-118.00556,"Category":[{"Name":null,"Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"7.003","NumberOfUserCheckedIn":0},{"PlaceID":"69c25617-bbdd-43c8-b753-e6381d1e3d4c","Name":"Sushi Hero","Address":"15290 Rosecrans Ave","Phone":"(714) 994-6933","Email":null,"Website":null,"Lat":33.898657,"Lon":-118.005267,"Category":[{"Name":"Food & Beverage > Restaurants","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"7.008","NumberOfUserCheckedIn":0},{"PlaceID":"478a898b-a1f5-417d-a5b6-8975d6b10e4b","Name":"Sushi","Address":"5835 Temple City Blvd","Phone":"(626) 286-8018","Email":null,"Website":null,"Lat":34.105567,"Lon":-118.061156,"Category":[{"Name":"Food & Beverage > Restaurants > Sushi","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"8.090","NumberOfUserCheckedIn":0},{"PlaceID":"bcb102a5-88ba-41f8-ad12-450c4cc6afc3","Name":"Sushi Tatsu","Address":"5679 Sultana Ave","Phone":"(818) 761-7011","Email":null,"Website":null,"Lat":34.102676,"Lon":-118.071694,"Category":[{"Name":"Food & Beverage > Restaurants","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"8.195","NumberOfUserCheckedIn":0},{"PlaceID":"1529b20c-5f12-46ea-b0e8-a1a10bae5430","Name":"Sushi in The Box","Address":"5927 Cloverly Ave","Phone":"(626) 286-5786","Email":null,"Website":"http:\/\/www.sushiinthebox.net","Lat":34.106813,"Lon":-118.063858,"Category":[{"Name":"Food & Beverage > Restaurants > Sushi","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"8.235","NumberOfUserCheckedIn":0},{"PlaceID":"71192cd0-1e18-012f-0643-0030487f54d8","Name":"Sushi Momo","Address":"1979 Sunnycrest Dr","Phone":"(714) 525-8974","Email":null,"Website":null,"Lat":33.892541,"Lon":-117.931639,"Category":[{"Name":"Food & Beverage > Restaurants","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"8.395","NumberOfUserCheckedIn":0},{"PlaceID":"c3219cdc-510d-42fe-a558-1fd5f210140a","Name":"Sushi Sai Japanese Restaurant","Address":"625 S Atlantic Blvd","Phone":"(626) 300-8495","Email":null,"Website":"http:\/\/www.sushisai.com\/","Lat":34.055599,"Lon":-118.136169,"Category":[{"Name":"Food & Beverage > Restaurants > Japanese","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"8.692","NumberOfUserCheckedIn":0},{"PlaceID":"5837b536-42bf-40c6-badd-e458f569ddd9","Name":"Sushi Pacific Suppliers","Address":"4930 Zambrano St","Phone":"(323) 278-9202","Email":null,"Website":null,"Lat":33.987921,"Lon":-118.15292,"Category":[{"Name":"Food & Beverage","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"8.799","NumberOfUserCheckedIn":0},{"PlaceID":"0ca94970-cd40-012e-5616-003048cad9da","Name":"Sushiboy","Address":"1420 N Azusa Ave","Phone":"(626) 918-1299","Email":null,"Website":null,"Lat":34.1052444,"Lon":-117.9076113,"Category":[{"Name":null,"Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"8.991","NumberOfUserCheckedIn":0},{"PlaceID":"2d6afe05-27f0-4f0b-b88b-fa0fa0dc8987","Name":"Sushi Ka","Address":"1225 N Grand Ave","Phone":"(909) 595-1217","Email":null,"Website":null,"Lat":34.043926,"Lon":-117.84854,"Category":[{"Name":"Food & Beverage > Restaurants > Seafood","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"9.189","NumberOfUserCheckedIn":0},{"PlaceID":"626d6dae-5d76-491a-a2f7-453b3ecb6dcb","Name":"Sushi Teriyaki","Address":"4854 S Eastern Ave","Phone":"(323) 725-1496","Email":null,"Website":null,"Lat":33.989467,"Lon":-118.161148,"Category":[{"Name":"Food & Beverage > Restaurants","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"9.259","NumberOfUserCheckedIn":0}],"code":"OK"}</span></pre>
<p>A JSON response contains three root elements:</p>
<ul>
  <li><code>"pagetoken"</code> contains a token that can be used to return up to 20 additional results.</li>
  <li><code>"TotalPage"</code> total page</li>
  <li><code>"results"</code> contains an array of Places, each place include (PlaceID, Name, Address, Distance (miles), NumberOfUserCheckedIn)</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#SearchByKeywordStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SearchByKeywordStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> indicates that no errors occurred; the place was successfully detected and at least one result was returned.</li>
  <li><code>ZERO_RESULTS</code> indicates that the search was successful but returned no results. This may occur if the search was passed a <code>latlng</code> in a remote location.</li>
  <li><code>DB_ERROR</code> - indicates that database error.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="SearchByKeywordV2">Search place with keyword from a user with user location V2 (with post method)</h2>
<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/search_by_keyword_v2');?>/#token/#lat/#lon/#page/#pageToken</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>lat</code> (<em>required</em>) - The latitude/longitude around which to retrieve Place information. This must be specified as <em>latitude</em>,<em>longitude</em></li>
  <li><code>lon</code> (<em>required</em>) - ...</li>
  <li><code>page</code> (<em>optional</em>) - default page is 1</li>
  <li><code>pageToken</code> (<em>optional</em>) - (just for Google search -  Returns the next 20 results from a previously run search)</li>
 
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>keyword</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/place/search_by_keyword_v2/IWPMXYTFVCVXVWYAPRB9/33.988692/-117.992606/')?>">
	keyword<input type="text" name="keyword" value="!@#$%^&"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint">
<span class="str">{"pagetoken":"","TotalPage":2,"results":[{"PlaceID":"1ea69649-c722-4ce4-a506-01e4aa13dd15","Name":"Sushi Land","Address":"2129 S Hacienda Blvd","Phone":"(626) 369-1143","Email":null,"Website":null,"Lat":33.995064,"Lon":-117.96853,"Category":[{"Name":"Food & Beverage > Restaurants > Sushi","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"1.835","NumberOfUserCheckedIn":0},{"PlaceID":"3566f3f3-adda-4784-a871-2ad935478cf3","Name":"Sushi Umi","Address":"3135 S Hacienda Blvd","Phone":"(626) 934-7731","Email":null,"Website":null,"Lat":33.980381,"Lon":-117.973,"Category":[{"Name":"Food & Beverage > Restaurants","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"2.057","NumberOfUserCheckedIn":0},{"PlaceID":"3fd3a80c-3798-45a5-8871-ecc445f4de28","Name":"Sushimax","Address":"7007 Greenleaf Ave","Phone":"(562) 698-4047","Email":null,"Website":null,"Lat":33.978725,"Lon":-118.037366,"Category":[{"Name":"Food & Beverage > Restaurants","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"2.597","NumberOfUserCheckedIn":0},{"PlaceID":"3270cba8-752c-48d3-addc-23c41b4a7cc8","Name":"Sushi from East Restaurant","Address":"15334 Whittier Blvd","Phone":"(562) 943-8997","Email":null,"Website":null,"Lat":33.94655,"Lon":-117.99996,"Category":[{"Name":"Food & Beverage > Restaurants","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"3.693","NumberOfUserCheckedIn":0},{"PlaceID":"c66e080d-420c-4fce-945a-57623f1594d8","Name":"Sushi Iwa","Address":"15334 Whittier Blvd","Phone":"(562) 943-8997","Email":null,"Website":null,"Lat":33.946548,"Lon":-117.999962,"Category":[{"Name":null,"Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"3.693","NumberOfUserCheckedIn":0},{"PlaceID":"2b6fd3d0-0355-012f-b33e-003048cacd48","Name":"Sushi Tokyo","Address":"12214 La Mirada Blvd","Phone":"(562) 943-3507","Email":null,"Website":"http:\/\/www.sushitk.com","Lat":33.919304,"Lon":-118.011818,"Category":[{"Name":"Food & Beverage > Restaurants","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"5.616","NumberOfUserCheckedIn":0},{"PlaceID":"f87570a9-fed2-4e12-b809-79a1b50f5dfd","Name":"Sushi","Address":"1864 Montebello Town Ctr","Phone":"(323) 720-1955","Email":null,"Website":null,"Lat":34.0392,"Lon":-118.091003,"Category":[{"Name":"Food & Beverage > Restaurants > Sushi","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"5.873","NumberOfUserCheckedIn":0},{"PlaceID":"962f4178-84f2-4a4f-a7b1-d42df53c22ab","Name":"Sushi Nagano","Address":"2560 E Amar Rd","Phone":"(626) 581-4148","Email":null,"Website":null,"Lat":34.0273832,"Lon":-117.8963925,"Category":[{"Name":"Food & Beverage > Restaurants > Sushi","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"6.228","NumberOfUserCheckedIn":0},{"PlaceID":"d65ae0ca-341f-4bd6-8f07-693791238e7a","Name":"Sushi Boy","Address":"1420 S Azusa Ave","Phone":"(626) 918-1299","Email":null,"Website":"http:\/\/sushiboy.net\/","Lat":34.048563,"Lon":-117.908102,"Category":[{"Name":"Food & Beverage > Restaurants > Sushi","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"6.241","NumberOfUserCheckedIn":0},{"PlaceID":"2ea9890d-4aae-4425-a153-f29b20f18882","Name":"Sushi Hero","Address":"15288 Rosecrans Ave","Phone":"(714) 994-6933","Email":null,"Website":null,"Lat":33.89875,"Lon":-118.00556,"Category":[{"Name":null,"Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"7.003","NumberOfUserCheckedIn":0},{"PlaceID":"69c25617-bbdd-43c8-b753-e6381d1e3d4c","Name":"Sushi Hero","Address":"15290 Rosecrans Ave","Phone":"(714) 994-6933","Email":null,"Website":null,"Lat":33.898657,"Lon":-118.005267,"Category":[{"Name":"Food & Beverage > Restaurants","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"7.008","NumberOfUserCheckedIn":0},{"PlaceID":"478a898b-a1f5-417d-a5b6-8975d6b10e4b","Name":"Sushi","Address":"5835 Temple City Blvd","Phone":"(626) 286-8018","Email":null,"Website":null,"Lat":34.105567,"Lon":-118.061156,"Category":[{"Name":"Food & Beverage > Restaurants > Sushi","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"8.090","NumberOfUserCheckedIn":0},{"PlaceID":"bcb102a5-88ba-41f8-ad12-450c4cc6afc3","Name":"Sushi Tatsu","Address":"5679 Sultana Ave","Phone":"(818) 761-7011","Email":null,"Website":null,"Lat":34.102676,"Lon":-118.071694,"Category":[{"Name":"Food & Beverage > Restaurants","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"8.195","NumberOfUserCheckedIn":0},{"PlaceID":"1529b20c-5f12-46ea-b0e8-a1a10bae5430","Name":"Sushi in The Box","Address":"5927 Cloverly Ave","Phone":"(626) 286-5786","Email":null,"Website":"http:\/\/www.sushiinthebox.net","Lat":34.106813,"Lon":-118.063858,"Category":[{"Name":"Food & Beverage > Restaurants > Sushi","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"8.235","NumberOfUserCheckedIn":0},{"PlaceID":"71192cd0-1e18-012f-0643-0030487f54d8","Name":"Sushi Momo","Address":"1979 Sunnycrest Dr","Phone":"(714) 525-8974","Email":null,"Website":null,"Lat":33.892541,"Lon":-117.931639,"Category":[{"Name":"Food & Beverage > Restaurants","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"8.395","NumberOfUserCheckedIn":0},{"PlaceID":"c3219cdc-510d-42fe-a558-1fd5f210140a","Name":"Sushi Sai Japanese Restaurant","Address":"625 S Atlantic Blvd","Phone":"(626) 300-8495","Email":null,"Website":"http:\/\/www.sushisai.com\/","Lat":34.055599,"Lon":-118.136169,"Category":[{"Name":"Food & Beverage > Restaurants > Japanese","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"8.692","NumberOfUserCheckedIn":0},{"PlaceID":"5837b536-42bf-40c6-badd-e458f569ddd9","Name":"Sushi Pacific Suppliers","Address":"4930 Zambrano St","Phone":"(323) 278-9202","Email":null,"Website":null,"Lat":33.987921,"Lon":-118.15292,"Category":[{"Name":"Food & Beverage","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"8.799","NumberOfUserCheckedIn":0},{"PlaceID":"0ca94970-cd40-012e-5616-003048cad9da","Name":"Sushiboy","Address":"1420 N Azusa Ave","Phone":"(626) 918-1299","Email":null,"Website":null,"Lat":34.1052444,"Lon":-117.9076113,"Category":[{"Name":null,"Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"8.991","NumberOfUserCheckedIn":0},{"PlaceID":"2d6afe05-27f0-4f0b-b88b-fa0fa0dc8987","Name":"Sushi Ka","Address":"1225 N Grand Ave","Phone":"(909) 595-1217","Email":null,"Website":null,"Lat":34.043926,"Lon":-117.84854,"Category":[{"Name":"Food & Beverage > Restaurants > Seafood","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"9.189","NumberOfUserCheckedIn":0},{"PlaceID":"626d6dae-5d76-491a-a2f7-453b3ecb6dcb","Name":"Sushi Teriyaki","Address":"4854 S Eastern Ave","Phone":"(323) 725-1496","Email":null,"Website":null,"Lat":33.989467,"Lon":-118.161148,"Category":[{"Name":"Food & Beverage > Restaurants","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"9.259","NumberOfUserCheckedIn":0}],"code":"OK"}</span></pre>
<p>A JSON response contains three root elements:</p>
<ul>
  <li><code>"pagetoken"</code> contains a token that can be used to return up to 20 additional results.</li>
  <li><code>"TotalPage"</code> total page</li>
  <li><code>"results"</code> contains an array of Places, each place include (PlaceID, Name, Address, Distance (miles), NumberOfUserCheckedIn)</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#SearchByKeywordV2StatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SearchByKeywordV2StatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> indicates that no errors occurred; the place was successfully detected and at least one result was returned.</li>
  <li><code>ZERO_RESULTS</code> indicates that the search was successful but returned no results. This may occur if the search was passed a <code>latlng</code> in a remote location.</li>
  <li><code>DB_ERROR</code> - indicates that database error.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="SearchByKeywordV3">Search place with keyword from a user with user location V3 (with post method & add Neighborhood as first record)</h2>
<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/search_by_keyword_v3');?>/#token/#lat/#lon/#page/#pageToken</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>lat</code> (<em>required</em>) - The latitude/longitude around which to retrieve Place information. This must be specified as <em>latitude</em>,<em>longitude</em></li>
  <li><code>lon</code> (<em>required</em>) - ...</li>
  <li><code>page</code> (<em>optional</em>) - default page is 1</li>
  <li><code>pageToken</code> (<em>optional</em>) - (just for Google search -  Returns the next 20 results from a previously run search)</li>
 
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>keyword</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/place/search_by_keyword_v3/IWPMXYTFVCVXVWYAPRB9/33.988692/-117.992606/')?>">
	keyword<input type="text" name="keyword" value="!@#$%^&"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint">
<span class="str">{"pagetoken":"CqQEGwIAAAXcJ-LwcLGUWvm9SU0zuAiAb7UfAMekNFk6Ul53XflId2Xujj0d1Vz2okatFOSQE6kqnPdRSsR2XDfTxWZKyi9rMl_czuNOBrnLBTqn7HgNjfW2RvsQiyjdSI2MvHtwaNuEBdmQ4rOPWuJdhpOjzRiSfgZUQ2s2CJ2V2zCXXo6grZ_T6uyeygBYtbsg2YRlSNiLH8WDPJED0btCqoPbk86Rv2L-0BzG1AOie6A-Tjcyd4qx5TZyVUxMRMd8GnRbOWSxnzJPLCij8FzQUJ4oI8QgQN1a79JtkERk6ZZCOvDyHkCJ_MaDvycy_KyhqU35OzdVFceqw4wZhwP7ps9tvHkxHU0pxw2zFmZCfnorZpIBAG1UGVxnvHyW9EcAYHzDXiIu2HHhk5u6v6zuwR9Tkchb45VrtqT1JZrxBM1xGodquhSIzoX-Q9ygdtNb9y1whTO6MwkiikB6wD__Jzn9ICLZgizbm7wWoRvH1P5TzMu85XbK2yNRiXKFwRAo35QiBT5SR7dO8PGWie8vxN1KB2BCQvgZGRxWVP_ncGH_BNKeIsiGgfK088q1yihJKuEUcuObOqsok0QFm7Qtl9QpDDgQgAW_RuvETkbYQJYwPgBWech5i_Rnuujjrn3FTsE2rJ3K8A3_E-DAFOGr3XSqI3P-7ilvUEVyFwDb4URPcIjO5Ngzbiks1m4H3oJG3AYlwdRZ0HR-QzFzObKBtZ57EykSEPyzm7XGJ8Ram6AuA_LawhMaFN7fIjPoRYfGaKktz5-jGDDTTzDp","TotalPage":1,"results":[{"PlaceID":"2296","Name":"Industry","NumberOfUserCheckedIn":3,"Address":"","Phone":"","Email":"","Website":"","Lat":"","Lon":"","Category":"","Distance":"","Type":"neighborhood","IsCheckedIn":"1","Photo":""},{"PlaceID":"da37912976f815fc7ba37af0d88bc4c23216fe32","Name":"Stan's Drapery Services","Address":"3022 Cardillo Avenue, Hacienda Heights","Phone":"","Email":null,"Website":"","Lat":33.986969,"Lon":-117.985374,"Category":[{"Name":"store","Icon":"http:\/\/localhost\/anomo\/public\/icons\/store.png"},{"Name":"laundry","Icon":"http:\/\/localhost\/anomo\/public\/icons\/laundry.png"},{"Name":"establishment","Icon":"http:\/\/localhost\/anomo\/public\/icons\/establishment.png"}],"Distance":"0.431","NumberOfUserCheckedIn":0,"IsCheckedIn":0,"Photo":"http:\/\/localhost\/anomo\/public\/icons\/store.png","Type":"place"},...],"code":"OK"}</span></pre>
<p>A JSON response contains three root elements:</p>
<ul>
  <li><code>"pagetoken"</code> contains a token that can be used to return up to 20 additional results.</li>
  <li><code>"TotalPage"</code> total page</li>
  <li><code>"results"</code> contains an array of Places, each place include (PlaceID, Name, Address, Distance (miles), NumberOfUserCheckedIn, Type (place / neighborhood))</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#SearchByKeywordV2StatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SearchByKeywordV2StatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> indicates that no errors occurred; the place was successfully detected and at least one result was returned.</li>
  <li><code>ZERO_RESULTS</code> indicates that the search was successful but returned no results. This may occur if the search was passed a <code>latlng</code> in a remote location.</li>
  <li><code>DB_ERROR</code> - indicates that database error.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>



<h2 id="SearchPeople">Search (checkin/spectate)people nearby from a location</h2>
<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/search_people_nearby');?>/#token/#UserID/#lat/#lon</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>

<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>lat</code> (<em>required</em>) - The latitude/longitude around which to retrieve Place information. This must be specified as <em>latitude</em>,<em>longitude</em></li>
  <li><code>lon</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/search_people_nearby');?>/IWPMXYTFVCVXVWYAPRB9/1/34/-118</span></em></pre>


<p>A JSON response example:</p>
<pre class="prettyprint">
<!--<span class="str">{"TotalRows":1639300,"Page":"1","ListCheckedIn":[{"UserID":"2","UserName":"Mi Tran","Avatar":"http:\/\/localhost:81\/anomo\/PHP Webservice\/public\/upload\/a.jpg","Gender":"1","ProfileStatus":null,"PlaceID":"8b41bf70-ad3e-4562-9b68-ebf36e8d66ac","ConnectMeter":"2","Tags":[],"Name":"Lasser Realty","Address":"14161 Skyline Dr","Lat":33.999063,"Lon":-118.00504}],"ListSpectator":[],"code":"OK"}
</span>-->
<span class="str">{"ListUser":[{"UserID":"2","UserName":"Mi Tran","Avatar":"http:\/\/localhost:81\/anomo\/PHP Webservice\/public\/upload\/a.jpg","Gender":"1","ProfileStatus":null,"Status":"1","PlaceID":"8b41bf70-ad3e-4562-9b68-ebf36e8d66ac","Name":"Lasser Realty","Address":"14161 Skyline Dr","Lat":"33.9991","Lon":"-118.005","ConnectMeter":"1","Tags":[]},{"UserID":"3","UserName":"abac","Avatar":"","Gender":"1","ProfileStatus":null,"Status":"1","PlaceID":"460a72eb-076d-4056-afd5-16f60305c912","Name":"Artistic Ceramics","Address":"14816 Edgeridge Dr","Lat":"33.9999","Lon":"-117.992","ConnectMeter":0,"Tags":[]},{"UserID":"4","UserName":"abac","Avatar":"","Gender":"1","ProfileStatus":null,"Status":"1","PlaceID":"26b897e4-2f14-4094-8cba-f67565910237","Name":"Go Motors","Address":"14725 Finisterra Pl","Lat":"33.9942","Lon":"-117.993","ConnectMeter":0,"Tags":[]}],"code":"OK"}
</span>
</pre>

<p>A JSON response contains three root elements:</p>
<ul>
  <!--  <li><code>"TotalRows"</code> - total record </li>
  <li><code>"Page"</code> - current page</li>-->
  <li><code>"ListUser"</code> - list checked in / spectate user (Status: 1 - checked in , 3: spectate)</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#SearchPeopleStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SearchPeopleStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> indicates that no errors occurred.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetPlaceDetail">Get A Place Detail</h2>

<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_detail_place');?>/#token/#UserID/#PlaceID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>

<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_detail_place');?>/IWPMXYTFVCVXVWYAPRB9/1/8b41bf70-ad3e-4562-9b68-ebf36e8d66ac</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{"results":{"Name":"Lasser Realty","Address":"14161 Skyline Dr","Lat":33.999063,"Lon":-118.00504,"Phone":"(626) 330-3000","Email":null,"Website":null,"Category":[{"Name":"Real Estate & Home Improvement","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"CheckInStatus":0,"ListUser":[{"UserID":"2","UserName":"oh_yes!","Avatar":"http:\/\/localhost:81\/anomo\/PHP Webservice\/public\/upload\/24.jpg","Gender":"1","ProfileStatus":"123","Status":"1","CheckInDate":"2012-02-28 08:15:13","ConnectMeter":"1","Tags":[]}],"ListLogout":[],"ListPost":[{"ID":"3","Content":null,"Photo":"http:\/\/localhost:81\/anomo\/PHP Webservice\/public\/photo\/abc.jpg","CreatedDate":"2012-01-09 14:51:43","UserID":"3","UserName":"Mi","Avatar":"","ConnectMeter":0,"Comments":""},{"ID":"2","Content":"bla bla llaalall lalalalla allalalla","Photo":null,"CreatedDate":"2012-01-09 11:51:50","UserID":"2","UserName":"Mi","Avatar":"","ConnectMeter":"1","Comments":""},{"ID":"1","Content":"this is post of place content bla bla bla bla","Photo":null,"CreatedDate":"2012-01-09 11:51:24","UserID":"1","UserName":"Mi","Avatar":"http:\/\/localhost:81\/anomo\/PHP Webservice\/public\/avatar\/cuoc_doi_wa_nhung_cai_chai1.jpg","ConnectMeter":0,"Comments":[{"ID":"1","Content":"comment","CreatedDate":"2012-01-09 14:32:37","UserID":"2","UserName":"Mi","Avatar":"","Gender":"1","ConnectMeter":"1"},{"ID":"2","Content":"comment 2 ","CreatedDate":"2012-01-09 14:32:41","UserID":"1","UserName":"Mi","Avatar":"http:\/\/localhost:81\/anomo\/PHP Webservice\/public\/avatar\/cuoc_doi_wa_nhung_cai_chai1.jpg","Gender":"1","ConnectMeter":0}]}]},"code":"OK"}
</span></pre>
<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"CheckInStatus"</code> 0: not check in / 1: checked in / 3: spectate</li>
  <li><code>"ListUser"</code> Status: 1 - checked in / 3 - specatate; list of user checkin / spectate on this place (user id, avatar, name, connect meter, tags, gender, status) </li>
  <li><code>"ListLogout"</code>List user logout</li>
  <li><code>"ListPost"</code> list post on this place [post id, picture url, text, date, user id, avatar, name, gender, connect meter, array of comments (name, avatar , user id, text, date, connect meter, gender)] </li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetPlaceDetailStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetPlaceDetailStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>NOT_FOUND</code> indicates that place is not found or UserID is not exist</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetPlaceDetailV2">Get A Place Detail V2</h2>

<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_detail_place_v2');?>/#token/#UserID/#PlaceID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>

<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_detail_place_v2');?>/IWPMXYTFVCVXVWYAPRB9/1/8b41bf70-ad3e-4562-9b68-ebf36e8d66ac</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{"results":{"Name":"Lasser Realty","Address":"14161 Skyline Dr","Lat":33.999063,"Lon":-118.00504,"Phone":"(626) 330-3000","Email":null,"Website":null,"Category":[{"Name":"Real Estate & Home Improvement","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"CheckInStatus":"3","ListAnomo":[{"Avatar":"http:\/\/localhost\/anomo\/public\/upload\/e15ba8ff5336b639ac0a32ab5b87dad4.jpg"},{"Avatar":"http:\/\/localhost\/anomo\/public\/upload\/5a45a7de092d713c61d5d1a6207291ff.jpg"}],"NumberAnomo":2,"ListTextPost":[{"UserID":"1","UserName":"Mi Tran son nguyen mi son son mi son son","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/afd8ef304baa7fc97707b6f535e91e1d.jpg","ID":"146","Content":"THis is content dfdsf","Photo":"","CreatedDate":"2012-04-16 09:01:00","ConnectMeter":0}],"NumberTextPost":2,"ListPicturePost":[],"NumberPicturePost":0,"ListTag":[{"TagID":"2","Name":"music","Count":"1"},{"TagID":"6","Name":"girl","Count":"1"},{"TagID":"8","Name":"car","Count":"1"},{"TagID":"9","Name":"family","Count":"1"},{"TagID":"170","Name":"foot ball","Count":"1"},{"TagID":"171","Name":"dota","Count":"1"},{"TagID":"189","Name":"ddaydddddd","Count":"1"},{"TagID":"190","Name":"songoku","Count":"1"},{"TagID":"191","Name":"teppi","Count":"1"},{"TagID":"193","Name":"hello","Count":"1"}],"NumberOfUserCheckedIn":2,"CanCheckIn":1},"code":"OK"}</span></pre>
<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"CheckInStatus"</code> 0: not check in / 1: checked in / 3: spectate</li>
  <li><code>"ListAnomo"</code> Status: 1 - checked in / 3 - specatate; list of user checkin / spectate on this place (avatar) </li>
  <li><code>"ListTextPost"</code> list post on this place [post id, picture url, text, date, user id, avatar, name, gender, connect meter] </li>
  <li><code>"ListPicturePost"</code> list post on this place [ picture url] </li>
  <li><code>"NumberAnomo"</code> total user is checkin-spectate </li>
  <li><code>"NumberAnomoPast"</code> total user is checkin-spectate in past</li>
  <li><code>"NumberTextPost"</code> total the text post on this place </li>
  <li><code>"NumberPicturePost"</code> total the picture post on this place  </li>
  <li><code>"ListTag"</code> tagid, tag, tag count </li>
  <li><code>"NumberTagPast"</code> total tags of user checkin in past</li>
  <li><code>"NumberOfUserCheckedIn"</code> number of user check in this place </li>
  <li><code>"CanCheckIn"</code> Status: 1 - can check in; 0: can't check in ; check in to place</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetPlaceDetailStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetPlaceDetailStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>NOT_FOUND</code> indicates that place is not found or UserID is not exist</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<h2 id="GetPlaceDetailV3">Get A Place Detail V3</h2>

<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_detail_place_v3');?>/#token/#UserID/#PlaceID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>

<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_detail_place_v3');?>/IWPMXYTFVCVXVWYAPRB9/1/b36e0ce0067adcf6ba43ccefb43595c7a3091a57</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{"results":{"PlaceID":"b36e0ce0067adcf6ba43ccefb43595c7a3091a57","Name":"C\u1eeda H\u00e0ng Th\u1eddi Trang Tr\u1ebb Em Thanh Th\u00fay","Address":"586 Nguy\u1ec5n \u0110\u00ecnh Chi\u1ec3u, 4, Ho Chi Minh City, Vietnam","Lat":10.770176,"Lon":106.68238,"Category":[{"Name":"establishment","Icon":"http:\/\/localhost\/Anomo\/public\/icons\/icon.png"}],"CheckInStatus":"3","ListTag":[]},"code":"OK"}</span></pre>
<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"CheckInStatus"</code> 0: not check in / 1: checked in / 3: spectate</li>
  <li><code>"ListTag"</code> tagid, tag </li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetPlaceDetailV3StatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="#GetPlaceDetailV3StatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>NOT_FOUND</code> indicates that place is not found or UserID is not exist</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<h2 id="CheckIns">Check In / Spectate</h2>

<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/check_in');?>/#token/#UserID/#PlaceID/#Status</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>

<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
  <li><code>Status</code> (<em>required</em>) - integer (1: Check in / 3: Spectate)</li>
</ul>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Photo</code> (<em>optional</em>) - ...</li>
  <li><code>Content</code> (<em>optional</em>) - ...</li>
</ul>

<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/place/check_in')?>/IWPMXYTFVCVXVWYAPRB9/1/b36e0ce0067adcf6ba43ccefb43595c7a3091a57/1">
	Photo	<input type="file" name="Photo"/>
	Content	<input type="text" name="Content"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>
<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/check_in');?>/IWPMXYTFVCVXVWYAPRB9/1/b36e0ce0067adcf6ba43ccefb43595c7a3091a57/1</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#CheckInStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="CheckInStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field within the result response object contains the status of the request, and may contain debugging information
  to help you track down why this request failed. The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>DB_ERROR</code> - indicates that database error.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="CheckOut">Check Out / Unspectate</h2>

<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/check_out');?>/#token/#UserID/#PlaceID/#Status</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>

<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
  <li><code>Status</code> (<em>required</em>) - 1: checkout / 3: unspectate (default is 1).</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/check_out');?>/IWPMXYTFVCVXVWYAPRB9/1/8b41bf70-ad3e-4562-9b68-ebf36e8d66ac</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#CheckOutStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="CheckOutStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field within the result response object contains the status of the request, and may contain debugging information
  to help you track down why this request failed. The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid or user is not checked in/spectate this place.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="SendMsgPlace">Post message/picture to place</h2>

<p>This request is an HTTP URL with POST method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/post_to_place');?>/#token/#UserID/#PlaceID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Content</code> <em></em> - text</li>
  <li><code>Photo</code> <em></em> - </li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/place/post_to_place/IWPMXYTFVCVXVWYAPRB9/1/8b41bf70-ad3e-4562-9b68-ebf36e8d66ac')?>">
	Content	<input type="text" name="Content"/>
	Photo 	<input type="file" name="Photo">
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"post":{"ID":"7","PlaceID":"8b41bf70-ad3e-4562-9b68-ebf36e8d66ac","UserID":"1","Content":"new post","Photo":"","CreatedDate":"2012-01-19 09:32:22"},"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
 <li><code>"post"</code> contains information of the post. Only return when the status code is OK</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#SendMsgPlaceStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SendMsgPlaceStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>DB_ERROR</code> - indicates that database error.</li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param or place or user is not exist </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="PostComment">Post a comment to a post of place</h2>

<p>This request is an HTTP URL with POST method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/post_comment');?>/#token/#UserID/#PostID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>PostID</code> (<em>required</em>) - ...</li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Content</code> (<em>required</em>) - text</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/place/post_comment/IWPMXYTFVCVXVWYAPRB9/1/1')?>">
	Content	<input type="text" name="Content"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"comment":{"ID":"5","Content":"new comment","CreatedDate":"2012-01-20 03:59:09","UserID":"1","UserName":"Mi Tran","Avatar":"","Gender":"1"},"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"comment"</code> contains information of the comment. Only return when the status code is OK</li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>BLOCK </code> - if current user blocked author of post</li>
  <li><code>BLOCK_TARGET</code> - if author of post blocked current user</li>
  <li><code>DB_ERROR</code> - indicates that database error.</li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param or post or user is not exist </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="ListActivity">Get list activity</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_activity');?>/#token/#UserID/#Lat/#Lon/#Page</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>Lat</code> (<em>required</em>) - ...</li>
  <li><code>Lon</code> (<em>required</em>) - ...</li>
  <li><code>Page</code> (<em>optional</em>) - default is 1</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_activity');?>/IWPMXYTFVCVXVWYAPRB9/88/34/-118</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"TotalPage":1,"Result":[{"PlaceID":"8b41bf70-ad3e-4562-9b68-ebf36e8d66ac","Name":"Lasser Realty","Type":"0","UserID":"0","PostID":"1","CreatedDate":"2012-04-13 00:00:00","Content":"test new","Photo":"","UserName":"Ban test","Avatar":"","CheckInStatus":0},{"PlaceID":"c829191f-02b2-45eb-9e4a-31e4fd37ea7b","Name":"Emerald Mountain View Investment Incorporation","Type":"1","UserID":"2","PostID":"0","CreatedDate":"2012-04-16 00:00:00","Content":"","Photo":"","UserName":"oh-yessss","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/e15ba8ff5336b639ac0a32ab5b87dad4.jpg","CheckInStatus":"1"}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"TotalPage"</code> total page </li>
	<li><code>"Result"</code> contains: Type (0:Post / 1:Checkin / 3:spectate) ... </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param or post or user is not exist </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="AnomoPick">GetAnomoPick</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_anomo_pick');?>/#token/#Lat/#Lon/#Page</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>Lat</code> (<em>required</em>) - ...</li>
  <li><code>Lon</code> (<em>required</em>) - ...</li>
  <li><code>Page</code> (<em>optional</em>) - default is 1</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_anomo_pick');?>/IWPMXYTFVCVXVWYAPRB9/34/-118</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"Result":[{"PlaceID":"8b41bf70-ad3e-4562-9b68-ebf36e8d66ac","Name":"Lasser Realty","NumberAnomo":"3","Distance":"0.293","PictureNum":"0","TextNum":"2"},{"PlaceID":"c829191f-02b2-45eb-9e4a-31e4fd37ea7b","Name":"Emerald Mountain View Investment Incorporation","NumberAnomo":"1","Distance":"0.468","PictureNum":"0","TextNum":"0"}],"TotalPage":1,"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"TotalPage"</code> total page </li>
	<li><code>"Result"</code> contains: placeid, placename, distance, anomos num, picture num, wall post num </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="AnomoPickV2">GetAnomoPickV2</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_anomo_pick_v2');?>/#token/#Lat/#Lon/#Page</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>Lat</code> (<em>required</em>) - ...</li>
  <li><code>Lon</code> (<em>required</em>) - ...</li>
  <li><code>Page</code> (<em>optional</em>) - default is 1</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_anomo_pick_v2');?>/IWPMXYTFVCVXVWYAPRB9/34/-118</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"Result":[{"PlaceID":"b36e0ce0067adcf6ba43ccefb43595c7a3091a57","Name":"C\u1eeda H\u00e0ng Th\u1eddi Trang Tr\u1ebb Em Alibaba","Address":"504 Nguy\u1ec5n \u0110\u00ecnh Chi\u1ec3u, 4, 3","NumberAnomo":"1","Distance":"8175.397","PictureNum":"2","TextNum":"1","CheckinNum":"11"}],"TotalPage":1,"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"TotalPage"</code> total page </li>
	<li><code>"Result"</code> contains: placeid, placename, distance, anomos num, picture num, wall post num </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param</li>
</ul>



<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetTextWall">Get Text Wall</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_text_wall');?>/#token/#UserID/#PlaceID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_text_wall');?>/IWPMXYTFVCVXVWYAPRB9/2/8b41bf70-ad3e-4562-9b68-ebf36e8d66ac</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"results":[{"UserID":"1","UserName":"Son Nguyen","Avatar":"http:\/\/sandbox240.vinasource.com\/public\/upload\/1b3dd6774ced1f10dbf812921bc909a3.jpg","ID":"200","Content":"here ?????","CreatedDate":"2012-04-16 09:09:24","ConnectMeter":"2","Comments":[]}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"ListTextPost"</code> list post on this place [post id, text, date, user id, avatar, name, gender, connect meter, array of comments (name, avatar , user id, text, date, connect meter, gender)]  </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetPictureWall">Get Picture Wall</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_picture_wall');?>/#token/#UserID/#PlaceID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_picture_wall');?>/IWPMXYTFVCVXVWYAPRB9/2/8b41bf70-ad3e-4562-9b68-ebf36e8d66ac</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"results":[{"UserID":"1","UserName":"Mi Tran son nguyen mi son son mi son son","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/afd8ef304baa7fc97707b6f535e91e1d.jpg","ID":"146","Content":"THis is content dfdsf","CreatedDate":"2012-04-16 09:01:00","ConnectMeter":"2","Comments":[]},{"UserID":"1","UserName":"Mi Tran son nguyen mi son son mi son son","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/afd8ef304baa7fc97707b6f535e91e1d.jpg","ID":"145","Content":"THis is content","CreatedDate":"2012-04-16 09:00:03","ConnectMeter":"2","Comments":[]}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"results"</code> list post on this place [post id, text, date, user id, avatar, name, gender, connect meter, array of comments (name, avatar , user id, text, date, connect meter, gender)]  </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<h2 id="GetPictureWallV2">Get Picture Wall V2</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_picture_wall');?>/#token/#PlaceID/#Page</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
  <li><code>Page</code> (<em>required</em>) - default is 1</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_picture_wall_v2');?>/IWPMXYTFVCVXVWYAPRB9/b36e0ce0067adcf6ba43ccefb43595c7a3091a57</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"results":{"TotalPage":1,"CurrentPage":"1","Posts":[{"UserID":"7","UserName":"tung","Avatar":"http:\/\/localhost\/Anomo\/public\/upload\/487abd5bdf850b7596b468bf02a1f81d.jpg","ID":"2","Content":"Hello all","Photo":"http:\/\/localhost\/Anomo\/public\/upload\/fa3d0aaff9c8d2426b4660618666b42c.jpg","CreatedDate":"2012-08-02 08:20:26","TotalComment":"0"},{"UserID":"7","UserName":"tung","Avatar":"http:\/\/localhost\/Anomo\/public\/upload\/487abd5bdf850b7596b468bf02a1f81d.jpg","ID":"1","Content":"Beatiful girl ^^","Photo":"http:\/\/localhost\/Anomo\/public\/upload\/2fcef92860eea13217148ceaf8ecd25b.jpg","CreatedDate":"2012-08-02 08:19:47","TotalComment":"0"}]},"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"results"</code> list post on this place [post id, text, date, user id, avatar, name, gender, connect meter, array of comments (name, avatar , user id, text, date, connect meter, gender)]  </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>




<h2 id="GetWallPost">Get wall post</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_wall_post');?>/#token/#UserID/#PlaceID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_wall_post');?>/IWPMXYTFVCVXVWYAPRB9/2/8b41bf70-ad3e-4562-9b68-ebf36e8d66ac</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"results":[{"UserID":"1","UserName":"Mi Tran son nguyen mi son son mi son son","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/afd8ef304baa7fc97707b6f535e91e1d.jpg","ID":"146","Content":"THis is content dfdsf","CreatedDate":"2012-04-16 09:01:00","ConnectMeter":"2","Comments":[]},{"UserID":"1","UserName":"Mi Tran son nguyen mi son son mi son son","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/afd8ef304baa7fc97707b6f535e91e1d.jpg","ID":"145","Content":"THis is content","CreatedDate":"2012-04-16 09:00:03","ConnectMeter":"2","Comments":[]}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"results"</code> list post on this place [post id, picture url, text, date, user id, avatar, name, gender, connect meter, array of comments (name, avatar , user id, text, date, connect meter, gender)]  </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>



<h2 id="GetWallPostV2">Get wall post v2</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_wall_post_v2');?>/#token/#PlaceID/#Page</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
  <li><code>Page</code> (<em>required</em>) - default is 1</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_wall_post_v2');?>/IWPMXYTFVCVXVWYAPRB9/b36e0ce0067adcf6ba43ccefb43595c7a3091a57</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"TotalPage":1,"CurrentPage":"1","WallPostList":[{"UserID":"28","UserName":"dung","Avatar":"http:\/\/localhost\/Anomo\/public\/upload\/af97b548984b0c8e86bb575b0ebca863.jpg","ID":"3","Content":"Hi","Photo":"","CreatedDate":"2012-08-02 10:33:33","TotalComment":"0"},{"UserID":"7","UserName":"tung","Avatar":"http:\/\/localhost\/Anomo\/public\/upload\/487abd5bdf850b7596b468bf02a1f81d.jpg","ID":"2","Content":"Hello all","Photo":"http:\/\/localhost\/Anomo\/public\/upload\/fa3d0aaff9c8d2426b4660618666b42c.jpg","CreatedDate":"2012-08-02 08:20:26","TotalComment":"0"},{"UserID":"7","UserName":"tung","Avatar":"http:\/\/localhost\/Anomo\/public\/upload\/487abd5bdf850b7596b468bf02a1f81d.jpg","ID":"1","Content":"Beatiful girl ^^","Photo":"http:\/\/localhost\/Anomo\/public\/upload\/2fcef92860eea13217148ceaf8ecd25b.jpg","CreatedDate":"2012-08-02 08:19:47","TotalComment":"0"}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"WallPostList"</code> list post on this place [post id, picture url, text, date, user id, avatar, name, total comment]  </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#GetWallPostV2StatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetWallPostV2StatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>



<h2 id="GetAnomos">Get Anomos</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_anomos');?>/#token/#UserID/#PlaceID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_anomos');?>/IWPMXYTFVCVXVWYAPRB9/2/8b41bf70-ad3e-4562-9b68-ebf36e8d66ac</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"ListUser":[{"UserID":"1","UserName":"Mi Tran son nguyen mi son son mi son son","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/afd8ef304baa7fc97707b6f535e91e1d.jpg","Gender":"1","ProfileStatus":"hello every one","Status":"3","CheckInDate":"2012-04-06 07:12:00","ConnectMeter":"2","Tags":[{"TagID":"193","Name":"hello"}]},{"UserID":"3","UserName":"zsniperz","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/5a45a7de092d713c61d5d1a6207291ff.jpg","Gender":"1","ProfileStatus":"hello, i'm sniper","Status":"1","CheckInDate":"2012-04-05 03:58:00","ConnectMeter":"1","Tags":[{"TagID":"6","Name":"girl"},{"TagID":"8","Name":"car"}]}],"ListLogout":[{"UserID":"88","UserName":"minhtran","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/cf120914d5fc07f522c834ee2b3633c1.jpg","Gender":"1","ProfileStatus":"You have nothing...","Status":"1","CheckInDate":"2012-04-16 08:56:20","ConnectMeter":"1","Tags":[{"TagID":"96","Name":"OK"}]}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"ListUser"</code> Status: 1 - checked in / 3 - specatate; list of user checkin / spectate on this place (user id, avatar, name, connect meter, tags, gender, status) </li>
  	<li><code>"ListLogout"</code> list of user logout on this place (user id, avatar, name, connect meter, tags, gender, status) </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>



<h2 id="GetAnomosV2">Get Anomos v2</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_anomos_v2');?>/#token/#UserID/#PlaceID/#Page</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
  <li><code>Page</code> (<em>required</em>) - default is 1</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_anomos_v2');?>/IWPMXYTFVCVXVWYAPRB9/2/8b41bf70-ad3e-4562-9b68-ebf36e8d66ac</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"TotalPage":1,"CurrentPage":1,"ListUser":[{"UserID":"1","UserName":"Teo","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/12d8a7be5192abbd05521885325bdb48.jpg","ProfileStatus":"","Status":"1","Tags":[],"IsOnline":1},{"UserID":"2","UserName":"tung","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/487abd5bdf850b7596b468bf02a1f81d.jpg","ProfileStatus":"","Status":"1","Tags":[],"IsOnline":1}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"ListUser"</code> Status: 1 - checked in / 3 - specatate; list of user checkin / spectate on this place (user id, avatar, tags, profile status, status) </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#GetAnomosV2StatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetAnomosV2StatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param or incorrect type of param</li>
  <li><code>INVALID_PARAMETER</code> - indicates that incorrect param</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>




<h2 id="GetAnomoByTag">Get list anomo base on tag web service</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_anomo_by_tag');?>/#token/#PlaceID/#TagID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
  <li><code>TagID</code> (<em>required</em>) - ...</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_anomo_by_tag');?>/IWPMXYTFVCVXVWYAPRB9/8b41bf70-ad3e-4562-9b68-ebf36e8d66ac/1</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"ListUserCheckIn":[{"UserID":"2","UserName":"oh-yessss","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/e15ba8ff5336b639ac0a32ab5b87dad4.jpg","Gender":"1","ProfileStatus":"Happyyyy","ConnectMeter":0,"Tags":[{"TagID":"2","Name":"music"},{"TagID":"9","Name":"family"},{"TagID":"170","Name":"foot ball"},{"TagID":"171","Name":"dota"},{"TagID":"189","Name":"ddaydddddd"},{"TagID":"190","Name":"songoku"},{"TagID":"191","Name":"teppi"}]}],"ListLogout":[],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"ListUser"</code> list of user checkin / spectate on this place (user id, avatar, name, connect meter, tags, gender, status) </li>
  	<li><code>"ListLogout"</code> list of user logout on this place (user id, avatar, name, connect meter, tags, gender, status) </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetUserStatus">Get user status</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_user_status');?>/#token/#UserID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_user_status');?>/IWPMXYTFVCVXVWYAPRB9/2</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"PlaceID":"8b41bf70-ad3e-4562-9b68-ebf36e8d66ac","Name":"Lasser Realty","Status":1,"TotalGift":"27","code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"Status"</code>   + 0 : not check in any where
  								+ 1 : checked in. </li>
  	<li><code>"PlaceID, Name"</code> place name and place id where user is checking in.(In this case Status = 1) </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetTags">Get tags</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_tags');?>/#token/#PlaceID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_tags');?>/IWPMXYTFVCVXVWYAPRB9/8b41bf70-ad3e-4562-9b68-ebf36e8d66ac</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"ListTag":[{"TagID":"2","Name":"music","Count":"1"},{"TagID":"6","Name":"girl","Count":"1"},{"TagID":"8","Name":"car","Count":"1"},{"TagID":"9","Name":"family","Count":"1"},{"TagID":"96","Name":"OK","Count":"1"},{"TagID":"170","Name":"foot ball","Count":"1"},{"TagID":"171","Name":"dota","Count":"1"},{"TagID":"189","Name":"ddaydddddd","Count":"1"},{"TagID":"190","Name":"songoku","Count":"1"},{"TagID":"191","Name":"teppi","Count":"1"}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"ListTag"</code> (TagID, Name, Count) </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetRandom">Get random</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_random');?>/#token/#UserID/#Lat/#Lon</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>Lat</code> (<em>required</em>) - ...</li>
  <li><code>Lon</code> (<em>required</em>) - ...</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_random');?>/IWPMXYTFVCVXVWYAPRB9/1/34/-118</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"PlaceList":[{"PlaceID":"7569c3e0-1e18-012f-0643-0030487f54d8","Name":"Laser Shop Inc.","NumberAnomo":"1"},{"PlaceID":"8b41bf70-ad3e-4562-9b68-ebf36e8d66ac","Name":"Lasser Realty","NumberAnomo":"4"},{"PlaceID":"fb3b192f-bb5f-4d2b-8b9e-4960e2b40a3c","Name":"Wonderland - Baby Store and Baby Clothing","NumberAnomo":"1"},{"PlaceID":"bf7ac856-faa5-4dc8-8885-5e6355235614","Name":"Baby Babaloo","NumberAnomo":"1"}],"UserList":[{"UserID":"3","UserName":"zsniperz","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/5a45a7de092d713c61d5d1a6207291ff.jpg","Name":"Lasser Realty"},{"UserID":"2","UserName":"oh-yessss","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/e15ba8ff5336b639ac0a32ab5b87dad4.jpg","Name":"Emerald Mountain View Investment Incorporation"},{"UserID":"88","UserName":"minhtran","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/cf120914d5fc07f522c834ee2b3633c1.jpg","Name":"Lasser Realty"},{"UserID":"183","UserName":"utest","Avatar":"","Name":"Babies R Us The Baby Superstore"}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"PlaceList"</code> (PlaceID, Name, NumberAnomo) </li>
  	<li><code>"UserList"</code> (UserID, UserName, Avatar, PlaceName, PlaceID) </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetRandomV2">Get random V2</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_random_v2');?>/#token/#UserID/#Lat/#Lon/#Page</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>Lat</code> (<em>required</em>) - ...</li>
  <li><code>Lon</code> (<em>required</em>) - ...</li>
  <li><code>Page</code> (<em>optional</em>) - ...</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_random_v2');?>/IWPMXYTFVCVXVWYAPRB9/1/34/-118</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"UserList":[{"UserID":"2","UserName":"oh-yes","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/666e6cc327fc7be4a282790ba97be105.jpg","FullAvatar":"http:\/\/localhost\/anomo\/public\/upload\/77eb5fc6cf5eb561029f8521c6c436d6.jpg","PlaceID":null,"Name":null,"IsBlock":0,"IsBlockTarget":0},{"UserID":"3","UserName":"zsniperz","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/b7c87d8dc43da0978c0b143204cde01a.jpg","FullAvatar":"http:\/\/localhost\/anomo\/public\/upload\/23e55f94732e6e6d86e410aec2536e23.jpg","PlaceID":null,"Name":null,"IsBlock":0,"IsBlockTarget":0},{"UserID":"5","UserName":"Mi","Avatar":"","FullAvatar":null,"PlaceID":null,"Name":null,"IsBlock":0,"IsBlockTarget":0}],"TotalPage":1,"CurrentPage":1,"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"UserList"</code> (UserID, UserName, Avatar, PlaceName, PlaceID) </li>
  	<li><code>"CurrentPage"</code> current page </li>
  	<li><code>"TotalPage"</code> total page </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetSpectatePlace">Get spectate place</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_spectate_place');?>/#token/#UserID/#Lat/#Lon</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>Lat</code> (<em>required</em>) - ...</li>
  <li><code>Lon</code> (<em>required</em>) - ...</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_spectate_place');?>/IWPMXYTFVCVXVWYAPRB9/1/34/-118</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"results":[{"PlaceID":"8b41bf70-ad3e-4562-9b68-ebf36e8d66ac","Name":"Lasser Realty","Address":"14161 Skyline Dr","Phone":"(626) 330-3000","Email":null,"Website":null,"Lat":33.999063,"Lon":-118.00504,"Category":[{"Name":"Real Estate & Home Improvement","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.296","NumberOfUserCheckedIn":0},{"PlaceID":"c829191f-02b2-45eb-9e4a-31e4fd37ea7b","Name":"Emerald Mountain View Investment Incorporation","Address":"14545 Blue Sky Rd","Phone":"(562) 693-7888","Email":null,"Website":null,"Lat":33.995439,"Lon":-117.993826,"Category":[{"Name":"Legal & Financial > Financial Planning & Investments","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.474","NumberOfUserCheckedIn":0},{"PlaceID":"460a72eb-076d-4056-afd5-16f60305c912","Name":"Artistic Ceramics","Address":"14816 Edgeridge Dr","Phone":"(562) 201-2344","Email":null,"Website":null,"Lat":33.999902,"Lon":-117.991681,"Category":[{"Name":"Travel & Tourism > Monuments","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.477","NumberOfUserCheckedIn":0},{"PlaceID":"6a399a44-2a33-49d7-ab65-276390604c19","Name":"Diversified Disposal Service","Address":"1947 Old Canyon Dr","Phone":"(562) 698-8799","Email":null,"Website":null,"Lat":34.007569,"Lon":-117.998611,"Category":[{"Name":"Community & Government > Environmental Conservation & Ecological Organizations","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.529","NumberOfUserCheckedIn":0},{"PlaceID":"26b897e4-2f14-4094-8cba-f67565910237","Name":"Go Motors","Address":"14725 Finisterra Pl","Phone":"(562) 693-5090","Email":null,"Website":null,"Lat":33.994249,"Lon":-117.993329,"Category":[{"Name":"Automotive > Car Parts & Accessories","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.551","NumberOfUserCheckedIn":0},{"PlaceID":"ca493ea0-656a-4540-89fc-8370cea7ca4b","Name":"Nic","Address":"14725 Finisterra Pl","Phone":"(714) 237-7733","Email":null,"Website":null,"Lat":33.994219,"Lon":-117.993329,"Category":[{"Name":"Automotive > Car Parts & Accessories","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.553","NumberOfUserCheckedIn":0},{"PlaceID":"e871a794-50c4-4c14-83fa-d00d263d8796","Name":"M and M Motors","Address":"14457 Eadbrook Dr","Phone":"(626) 968-3353","Email":null,"Website":null,"Lat":34.007747,"Lon":-117.996207,"Category":[{"Name":"Automotive > Car Dealers","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.578","NumberOfUserCheckedIn":0},{"PlaceID":"c7f22ea6-1520-49bd-84cb-462a51a8f8f0","Name":"Bejarano Family Partnership","Address":"14533 Langhill Dr","Phone":"(626) 330-9627","Email":null,"Website":null,"Lat":34.009266,"Lon":-117.997235,"Category":[{"Name":"Real Estate & Home Improvement > Real Estate Agents","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.659","NumberOfUserCheckedIn":0},{"PlaceID":"4987a4ec-27ee-4865-a3be-f701feb6859a","Name":"Putnam International","Address":"2250 Turnbull Canyon Rd","Phone":"(626) 336-0932","Email":null,"Website":null,"Lat":34.00191,"Lon":-117.98708,"Category":[{"Name":null,"Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.752","NumberOfUserCheckedIn":0},{"PlaceID":"6bcbd92d-7bdb-41a9-8125-f14888195d7c","Name":"Orange Grove Middle School","Address":"14505 Orange Grove Ave","Phone":"(626) 933-7001","Email":null,"Website":"http:\/\/www.orange-grove.hlpusd.k12.ca.us\/","Lat":34.010235,"Lon":-117.99524,"Category":[{"Name":"Education > Schools","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.758","NumberOfUserCheckedIn":0},{"PlaceID":"07218730-c0a8-4922-8c4e-4f208e36ee7c","Name":"Orange Grove Elementary School","Address":"10626 Orange Grove Ave","Phone":"(562) 789-3200","Email":null,"Website":null,"Lat":34.0094,"Lon":-117.9927,"Category":[{"Name":"Education > Schools","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.772","NumberOfUserCheckedIn":0},{"PlaceID":"473ad4f0-b40f-4482-83d6-e5059bc190b4","Name":"Hacienda Pipeline","Address":"14621 Orange Grove Ave","Phone":"(626) 333-8386","Email":null,"Website":null,"Lat":34.00974,"Lon":-117.99307,"Category":[{"Name":"Real Estate & Home Improvement > Plumbing","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.781","NumberOfUserCheckedIn":0},{"PlaceID":"ad13e314-49dc-43ad-a7d7-4ffde5aac1a1","Name":"Pacific Grading","Address":"15163 Los Altos Dr","Phone":"(562) 694-2288","Email":null,"Website":null,"Lat":33.993233,"Lon":-117.988004,"Category":[{"Name":"Real Estate & Home Improvement","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.831","NumberOfUserCheckedIn":0},{"PlaceID":"752fab78-73bc-4f23-8fd3-1ac165e3d49d","Name":"Pacific Grating","Address":"15163 Los Altos Dr","Phone":"(626) 446-0809","Email":null,"Website":null,"Lat":33.99324,"Lon":-117.98798,"Category":[{"Name":null,"Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.832","NumberOfUserCheckedIn":0},{"PlaceID":"cef1c527-e8f8-4bc5-ae90-a53b191e4779","Name":"Warehouse Equipment Erectors","Address":"1631 Doverfield Ave","Phone":"(626) 333-5165","Email":null,"Website":null,"Lat":34.010559,"Lon":-117.992241,"Category":[{"Name":"Business & Professional Services > Equipment, Supplies & Services > Industrial Machinery & Vehicles","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.854","NumberOfUserCheckedIn":0},{"PlaceID":"4eb7e90e-4d5f-4e0d-95df-dfb1bb95a762","Name":"Friends of Senator Alarcon","Address":"14704 Lujon St","Phone":"(626) 369-5923","Email":null,"Website":null,"Lat":34.010423,"Lon":-117.990687,"Category":[{"Name":"Community & Government > Organizations & Associations","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.896","NumberOfUserCheckedIn":0},{"PlaceID":"06e3fdc5-5d86-4d93-b36f-b18e314c8a6b","Name":"White Dove Releases","Address":"1549 7th Ave","Phone":"(626) 330-2129","Email":null,"Website":"http:\/\/whitedoverelease.com\/","Lat":34.011558,"Lon":-117.9928,"Category":[{"Name":"Shopping > Pet Shops & Pet Supplies","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.899","NumberOfUserCheckedIn":0},{"PlaceID":"2c49ae72-716d-4c5b-a85b-6d2ac8f666f4","Name":"Allview Image of America","Address":"2578 Daytona Ave","Phone":"(626) 968-3798","Email":null,"Website":"http:\/\/www.allviewmirror.com\/","Lat":33.996429,"Lon":-117.9847,"Category":[{"Name":"Automotive > Car Parts & Accessories","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.910","NumberOfUserCheckedIn":0},{"PlaceID":"ca41f99e-062b-4e1d-bdb8-bd7014112621","Name":"Ambience Sound and Video","Address":"2588 Daytona Ave","Phone":"(626) 968-4446","Email":null,"Website":null,"Lat":33.996524,"Lon":-117.984649,"Category":[{"Name":"Business & Professional Services > Equipment, Supplies & Services > Audiovisual","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.912","NumberOfUserCheckedIn":0},{"PlaceID":"0fdab88d-8db3-4be8-992d-6d7c0652227d","Name":"Lotus Arts and Gifts","Address":"14428 Crystal Lantern Dr","Phone":"(626) 968-7875","Email":null,"Website":null,"Lat":34.013171,"Lon":-117.996217,"Category":[{"Name":"Shopping > Gift & Novelty Stores","Icon":"http:\/\/localhost\/anomo\/public\/icons\/icon.png"}],"Distance":"0.935","NumberOfUserCheckedIn":0}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"results"</code>contains an array of Places, each place include (PlaceID, Name, Address, Distance (miles), NumberOfUserCheckedIn)</li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="PrepareChatOnPlace">Prepare Chat On Place</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/prepare_chat_on_place');?>/#token/#UserID/#PlaceID/#Lat/#Lon</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
  <li><code>Lat</code> (<em>required</em>) - ...</li>
  <li><code>Lon</code> (<em>required</em>) - ...</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/prepare_chat_on_place');?>/IWPMXYTFVCVXVWYAPRB9/1/c370cc723d55c875d2f2f3cf66fe48dfcf79c1c1/10.773952/106.669296</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"place":{"Name":"C\u00f4ng ty C\u1ed5 Ph\u1ea7n Nh\u1eadt Thi\u00ean Minh","Address":"796\/17 S\u01b0 V\u1ea1n H\u1ea1nh, TP H\u1ed3 Ch\u00ed Minh, H\u1ed3 Ch\u00ed Minh, Vietnam","Lat":10.773952,"Lon":106.669296,"Phone":"08 3868 4338","Email":null,"Website":"http:\/\/www.ntm.com.vn\/","CheckInStatus":0,"ListAnomo":[],"NumberAnomo":0,"NumberAnomoPast":0,"ListTextPost":[],"NumberTextPost":0,"ListPicturePost":[],"NumberPicturePost":0,"ListTag":[],"NumberTagPast":0,"NumberOfUserCheckedIn":0},"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"place"</code> (Name, Address, Lat, Lon, Phone, Email, CheckInStatus, ListAnomo, NumberAnomo, NumberAnomoPast, ListTextPost, NumberTextPost, ListPicturePost, NumberPicturePost, ListTag, NumberTagPast, NumberOfUserCheckedIn) </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PrepareChatOnPlaceStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PrepareChatOnPlaceStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>DB_ERROR</code> - indicates that database error.</li>
  <li><code>NOT_FOUND</code> indicates that place is not found or UserID is not exist</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>



<h2 id="UpdatePlaceStatus">Update place status</h2>

<p>Update place status request is an HTTP URL with POST method of the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/update_place_status');?>/#token/#UserID/#PlaceID</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
</ul>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Status</code> (<em>required</em>) - Integer (1:Checkin, 2:Checkout)</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<ul>
<li>Request Header :</li>
	+ Name: Content-Type
	+ Value: application/x-www-form-urlencoded
<li>Request Body :</li>
	Status=1
</ul>
</pre>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/place/update_place_status/IWPMXYTFVCVXVWYAPRB9/1/b36e0ce0067adcf6ba43ccefb43595c7a3091a57')?>">
	Status	<input type="text" name="Status"/>

	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#UpdatePlaceStatusStatusCodes">Status Codes</a> below.</li>  
</ul>

<h3 id="UpdatePlaceStatusStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; A new user was created successfully.</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> request without post method, or parameters are missing.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<h2 id="SendMessageInPlace">Send message in place</h2>

<p>Send message in place request is an HTTP URL with POST method of the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/send_message');?>/#token/#UserID/#PlaceID</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
</ul>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Content</code> (<em>option</em>) - ...</li>
  <li><code>Photo</code> (<em>option</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<ul>
<li>Request Header :</li>
	+ Name: Content-Type
	+ Value: application/x-www-form-urlencoded
<li>Request Body :</li>
</ul>
</pre>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/place/send_message/IWPMXYTFVCVXVWYAPRB9/2/b36e0ce0067adcf6ba43ccefb43595c7a3091a57')?>">
	Content	<input type="text" name="Content"/>
	Photo	<input type="file" name="Photo"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"results":{"PlaceID":"b36e0ce0067adcf6ba43ccefb43595c7a3091a57","UserID":"2","Content":"aa","Photo":"","Photo100":"","Photo200":"","Photo300":"","CreatedDate":"2013-02-27 03:25:35","ID":3},"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#SendMessageInPlaceStatusCodes">Status Codes</a> below.</li>  
</ul>

<h3 id="SendMessageInPlaceStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; A new user was created successfully.</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> request without post method, or parameters are missing.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>



<h2 id="GetMessagesInPlace">Get messages in place</h2>

<p>Get messages in place request is an HTTP URL with GET method of the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_messages');?>/#token/#UserID/#PlaceID</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>PlaceID</code> (<em>required</em>) - ...</li>
  <li><code>MessageID</code> (<em>required</em>) - (-1:get 50 latest messages, messageID: get latest messages from messageID)</li>
  <li><code>PositionFlag</code> (<em>required</em>) - (1:load latest, 2: load more)</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_messages');?>/IWPMXYTFVCVXVWYAPRB9/1/b36e0ce0067adcf6ba43ccefb43595c7a3091a57/-1/2</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"results":{"Messages":[{"ID":"12","Content":"sdfs\u0111fg","CreatedDate":"2012-09-26 10:26:07","ColorID":"1","UserID":"1","UserName":"sdfasdf","Avatar":"a8ecd4d95a756c7d8be4fd07395860ed.jpg"},{"ID":"13","Content":"sdfs\u0111fg","CreatedDate":"2012-09-26 10:28:42","ColorID":"1","UserID":"1","UserName":"sdfasdf","Avatar":"a8ecd4d95a756c7d8be4fd07395860ed.jpg"},{"ID":"14","Content":"werwer","CreatedDate":"2012-09-26 10:34:15","ColorID":"1","UserID":"1","UserName":"sdfasdf","Avatar":"a8ecd4d95a756c7d8be4fd07395860ed.jpg"}],"NumOfChattingUser":1},"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetMessagesInPlaceStatusCodes">Status Codes</a> below.</li>  
</ul>

<h3 id="GetMessagesInPlaceStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; A new user was created successfully.</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> request without post method, or parameters are missing.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>



<h2 id="CommentOfPost">Get comment of post</h2>

<p>Get messages in place request is an HTTP URL with GET method of the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_comment');?>/#token/#PostID</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>PostID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/get_comment');?>/IWPMXYTFVCVXVWYAPRB9/1/b36e0ce0067adcf6ba43ccefb43595c7a3091a57/-1/2</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"result":[{"ID":"1","Content":"Comment","CreatedDate":"2012-08-17 04:19:46","UserID":"6","UserName":"Frian Nguyen","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/12d8a7be5192abbd05521885325bdb48.jpg","Gender":"0","ConnectMeter":0},{"ID":"2","Content":"Aaaa","CreatedDate":"2012-08-17 04:19:59","UserID":"100","UserName":"bb","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/1b6d26c965f5611e9804c1f0abf2f01f.jpg","Gender":"1","ConnectMeter":"1"}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"result"</code>list comment (name, avatar , user id, text, date, connect meter, gender)</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetMessagesInPlaceStatusCodes">Status Codes</a> below.</li>  
</ul>

<h3 id="GetMessagesInPlaceStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; A new user was created successfully.</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> request without post method, or parameters are missing.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="AddLocalPlace">Add local place</h2>

<p>This request is an HTTP URL with POST method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/add_place');?>/#token</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Name</code> (<em>required</em>) - text</li>
  <li><code>Address</code> (<em>required</em>) - text</li>
  <li><code>Lat</code> (<em>required</em>) - ...</li>
  <li><code>Lon</code> (<em>required</em>) - ...</li>
  <li><code>Category</code> (<em>required</em>) - Category should be separated with a pipe symbol (type1|type2|etc)</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/place/add_place/IWPMXYTFVCVXVWYAPRB9')?>">
	Name	<input type="text" name="Name"/>
	Address	<input type="text" name="Address"/>
	Lat	<input type="text" name="Lat"/>
	Lon	<input type="text" name="Lon"/>
	Category	<input type="text" name="Category"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str"></span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param or post or user is not exist </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="DeletePost">Delete Post</h2>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/delete_post');?>/#token/#PostID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>PostID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/place/delete_post');?>/IWPMXYTFVCVXVWYAPRB9/2</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetContactOfUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetContactOfUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid or user is not existed; </li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
