<h1 id="ActivityStream" style="margin:1em 0 0 -10px;">Activity Stream</h1>
<p>Action Type:</p>
<pre class="prettyprint">
0: checkin
1: change profile status
2: post picture to place
5: share content
6: post text to place
7: post picture to neighborhood
8: post text to neighborhood
9: send gift
27: post picture status
28: new register user
29: Announcement card
<!-- 

10: change tag (*)-->
<!--
3: comment picture post to place
11: comment post text to place
12: comment post picture to nbh
13: comment post text to nbh
14: comment checkin
15: comment change profile status
16: comment send gift
  17: comment change tag (*)
 28: comment post picture status-->
<!--
18: like checkin
19: like change profile status
20: like post picture to place
21: like post text to place
22: like post picture to nbh
23: like post text to nbh
24: like send gift
25: like change tag (*)
29: like picture status-->
<!-- 
5: share picture post to place
26: share picture post to nbh
30: like picture status
 4: like (**)-->
</pre>

<p id="ContentType">Content Type:</p>
<pre class="prettyprint">
0: checkin
1: change profile status
2: post picture to place
6: post text to place
7: post picture to neighborhood
8: post text to neighborhood
9: send gift
27: post picture status
28: new register user
29: Announcement card

</pre>
<ol class="toc">
	<li><a href="#GetActivityStream">Get Activity Stream</a></li>
	<li><a href="#LikeContent">Like Content</a></li>
	<li><a href="#ShareContent">Share Content</a></li>
	<li><a href="#CommentContent">Comment Content</a></li>
	<li><a href="#DetailContent">Get Content Detail</a></li>
        <li><a href="#ListUserLikePost">Like user who liked this post</a></li>
        <li><a href="#LikeComment">Like comment</a></li>
        <li><a href="#ListUserLikeComment">Like user who liked this comment</a></li>
        <li><a href="#StopReceiveNotify">Turn ON / OFF receiving notifications on specific threads</a></li>
        <li><a href="#ClosedAnnouncement">Close announcement card</a></li>
        <li><a href="#ListTrendingHashTag">List trending hashtag</a></li>
        <li><a href="#OnBoardingHashTag">List onboarding hashtag</a></li>
        <li><a href="#DeleteComment">Delete comment</a></li>
</ol>

<!-- ---------------------------------------------- -->
<h2 id="GetActivityStream">Get Activity Stream</h2>
<p>The API Get Activity Stream is a web service that returns list activity. You can get list recent/nearby/popular/following activity or search hashtag.
Its depend on TYPE you input. Please see <a href="#Type">Type</a> detail below. Its also filtering by Age, Gender.
This web service is called by mobile app on activity stream screen.
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/get_activities');?>/#Token/#Page/#Type/#Radius/#Gender/#AgeFrom/#AgeTo/#ActivityID/#TotalPage</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>Page</code> (<em>required</em>) - default is 1</li>
  <li><code>Type</code> (<em>required</em>) - default is 0</li>
  <li><code>Radius</code> (<em>optional</em>) - default is -1</li>
  <li><code>Gender</code> (<em>optional</em>) - (0: both, 1: male, 2: female) </li>
  <li><code>AgeFrom</code> (<em>optional</em>) - </li>
  <li><code>AgeTo</code> (<em>optional</em>) - </li>
  <li><code>ActivityID</code> (<em>optional</em>) - for fixing duplicate record, default is 0. If have params the web service returns list activity with ActivityID less than #ActivityID</li>
  <li><code>TotalPage</code> (<em>optional</em>) - for cached, default is 0 </li>
</ul>
<p id="Type">Type</p>
<pre class="prettyprint">
0: Recent
1: Nearby
2: Popular
3: Following
4: Search hashtag (anywhere)
5: Search nearby hashtag
6: Search following hashtag
7: Favorite feed
</pre>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/get_activities');?>/IWPMXYTFVCVXVWYAPRB9</span></pre>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/activity/get_activities')?>/IWPMXYTFVCVXVWYAPRB9/1/4">
	HashTag	<input type="text" name="HashTag"/>
        TopicID	<input type="text" name="TopicID"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
ListTrending: [
    {
    HashTag: "test"
    },
    {
    HashTag: "123"
    },
    {
    HashTag: "123456789"
    },
    {
    HashTag: "anomorocks"
    }
]
Activities: [
    {
    ActivityID: "52",
    RefID: "52",
    Message: "Confession Tuesday is back! dfsdf Confession Tuesday is back! dfsdf Confession Tuesday is back! dfsdf Confession Tuesday is back! dfsdf Confession Tuesday is back! dfsdf",
    Title: "Confession Tuesday is back! dfsdf",
    Type: "29",
    ActionType: "29",
    CreatedDate: "2014-04-10 08:45:18",
    IsBlock: "0",
    IsBlockTarget: "0"
    },
    {
    ActivityID: "10682",
    FromUserID: "33805",
    Message: "{ "message" : "Add a new fresh status @duc3 ", "message_tags" : [ { "id" : "34177", "name" : "@duc3" } ] }",
    Image: "",
    RefID: "2122",
    Type: "1",
    ActionType: "1",
    PlaceID: "",
    PlaceName: "",
    PlaceIcon: "",
    PlaceAddress: "",
    Lat: "0.0000000",
    Lon: "0.0000000",
    CreatedDate: "2014-04-10 09:01:55",
    IsInvalid: "0",
    LastUpdate: "",
    Gender: "0",
    BirthDate: "1994-04-10",
    OwnerID: "",
    OrginalDate: "",
    GiftReceiverID: "0",
    CheckinPlaceID: "",
    CheckinPlaceName: "",
    NeighborhoodID: "29698",
    Avatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/crop_asian_new_fem_5.png",
    OwnerAvatar: "",
    IsBlock: "0",
    IsBlockTarget: "0",
    IsReported: "0",
    FacebookID: "100004794702245",
    FromUserName: "mthuong",
    OwnerName: "",
    GiftReceiverName: "",
    IsStopReceiveNotify: "0",
    CheckinStatus: "0",
    NeighborhoodName: "Earth",
    IsLike: "0",
    Tags: [ ],
    Share: "0",
    IsComment: "0",
    Comment: "0",
    Like: "0",
    LatestComment: { },
    LatestLike: { },
    IsInternalShared: 0,
    OwnerCheckinPlaceID: "",
    OwnerCheckinPlaceName: "",
    OwnerCheckinStatus: 0,
    OwnerNeighborhoodID: "",
    OwnerNeighborhoodName: ""
    },
    {..}
]
,"Page":1
,"TotalPage":1
,"code":"OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
  <li><code>FAIL</code> - indicates that db error</li>
  <li><code>TOPIC_BANNED</code> - indicates that you are banned by the mod of topic</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="LikeContent">Like Content</h2>
<p>This is a web service allows you to like a content. It requires the id and type of content to be liked</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/like');?>/#Token/#ContentID/#Type</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a> </li>
  <li><code>ContentID</code> (<em>required</em>) - the id of the content to be liked</li>
  <li><code>Type</code> (<em>required</em>) - type of content to be liked. See detail <a href="#ContentType">Content Type</a></li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/like');?>/IWPMXYTFVCVXVWYAPRB9/60/2</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"ContentID":"60","NumberOfLike":6,"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
  <li><code>FAIL</code> - indicates that db error</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<h2 id="ShareContent">Share Content</h2>
<p class="warning">Status: Deprecated</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/share');?>/#Token/#ContentID/#Type/#ShareType</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a> </li>
  <li><code>ContentID</code> (<em>required</em>) - the id of the content to be shared</li>
  <li><code>Type</code> (<em>required</em>) - type of content to be shared. See detail <a href="#ContentType">Content Type</a></li>
  <li><code>ShareType</code> (<em>required</em>) - 0: Facebook, 1: Internal, 2: Both</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/share');?>/IWPMXYTFVCVXVWYAPRB9/60/2</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
  <li><code>FAIL</code> - indicates that db error</li>
  <li><code>SHARED</code> - indicates that content is shared</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="CommentContent">Comment Content</h2>
<p>This is a web service allows you to post a comment on a post. It requires the id and type of content to be commented</p>
<p class="note">Status: Active</p>
<p>This request is an HTTP URL with POST method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/comment');?>/#token/#ContentID/#Type</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>ContentID</code> (<em>required</em>) - the id of content to be commented</li>
  <li><code>Type</code> (<em>required</em>) - type of content. See detail <a href="#ContentType">Content Type</a></li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Content</code> (<em>required</em>) - text</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/activity/comment/IWPMXYTFVCVXVWYAPRB9/3/0')?>">
	Content	<input type="text" name="Content"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK","comment":{"ID":6,"UserID":"1","UserName":"","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/12d8a7be5192abbd05521885325bdb48.jpg","Content":"fun","CreatedDate":"2013-03-29 10:45:14","PlaceID":"7a124e279c55d464b21c9aa21640e47696f14eb2","PlaceName":"Ph\u1edf Chua","NeighborhoodID":"2296","NeighborhoodName":"Industry"}}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"comment"</code> contains information of the comment. Only return when the status code is OK</li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>FAIL</code> - indicates that database error.</li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param or post or user is not exist </li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="DetailContent">Get Content Detail</h2>
<p>The Get Content Detail Service is a web service that returns information of a post. It is called in the activity detail screen</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/detail');?>/#Token/#ContentID/#Type</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>ContentID</code> (<em>required</em>) - the id of content </li>
  <li><code>Type</code> (<em>required</em>) - type of content. See detail <a href="#ContentType">Content Type</a></li>
</ul>
<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/detail');?>/IWPMXYTFVCVXVWYAPRB9/60/2</span></pre>


<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">

{
    Activity: {
        ActivityID: "10737",
        FromUserID: "34212",
        FromUserName: "mthuong1",
        Avatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/crop_asian_fem_1.png",
        Message: "{"message":"@mthuong","message_tags":[{"id":"33805","name":"@mthuong"}]}",
        Image: "",
        RefID: "2151",
        Type: "1",
        ActionType: "1",
        PlaceID: "",
        PlaceName: "",
        PlaceIcon: "",
        PlaceAddress: "",
        Lat: null,
        Lon: null,
        CreatedDate: "2014-04-12 02:14:14",
        Location: "",
        IsInvalid: "0",
        LastUpdate: "",
        Gender: "2",
        BirthDate: "1988-12-04",
        OwnerID: "",
        OwnerName: "",
        OwnerAvatar: "",
        OrginalDate: "",
        GiftReceiverID: "0",
        GiftReceiverName: "",
        CheckinPlaceID: "",
        CheckinPlaceName: "",
        NeighborhoodID: "",
        IsLike: 0,
        IsStopReceiveNotify: "0",
        IsComment: "0",
        NeighborhoodName: "",
        IsBlock: "0",
        IsBlockTarget: "0",
        IsReported: "0",
        IsFavorite: "0",
        FacebookID: "",
        Share: "0",
        Comment: "1",
        Like: "1",
        ListComment: [
            {
            ID: "1207",
            UserID: "34177",
            UserName: "duc3",
            BirthDate: "1994-04-05",
            Content: "vhh",
            CreatedDate: "2014-04-12 10:11:50",
            NumberOfLike: "1",
            IsLike: "0",
            IsReported: "0",
            FacebookID: "",
            IsFavorite: "0",
            Avatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/crop_afr_fem_2.png",
            NeighborhoodID: "29696",
            NeighborhoodName: "Ho Chi Minh City, Vietnam"
            }
        ],
        CheckinStatus: 0
        },
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
  <li><code>FAIL</code> - indicates that db error</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<h2 id="LikeComment">Like Comment</h2>
<p>This is a web service allows you to like on comment. It requires the id of comment and type of post</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/comment/like');?>/#Token/#CommentID/#ContentType</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>CommentID</code> (<em>required</em>) - the id of the comment to be liked</li>
  <li><code>Type</code> (<em>required</em>) - type of content to be liked. See detail <a href="#ContentType">Content Type</a></li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/comment/like');?>/IWPMXYTFVCVXVWYAPRB9/60/2</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"CommentID":"60","NumberOfLike":6,"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
  <li><code>FAIL</code> - indicates that db error</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>



<h2 id="ListUserLikeComment">Like user who liked this comment</h2>
<p>Return list of users  who liked a comment</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/comment/likelist');?>/#Token/#CommentID/#ContentType</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a> </li>
  <li><code>CommentID</code> (<em>required</em>) - the id of the comment to be liked</li>
  <li><code>Type</code> (<em>required</em>) - type of content to be liked. See detail <a href="#ContentType">Content Type</a></li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/comment/likelist');?>/IWPMXYTFVCVXVWYAPRB9/60/2</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">

{
    likes: [
            {
            UserID: "1",
            UserName: "test",
            Avatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/female_avatar_default.jpg"
            }
        ],
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
  <li><code>FAIL</code> - indicates that db error</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>



<h2 id="ListUserLikePost">Like user who liked this post</h2>
<p>Return list of users who liked a post. It is called when tapping on #Like of post</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/likelist');?>/#Token/#ContentID/#Type</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>ContentID</code> (<em>required</em>) - the id of the post to be liked</li>
  <li><code>Type</code> (<em>required</em>) - type of content to be liked. See detail <a href="#ContentType">Content Type</a></li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/likelist');?>/IWPMXYTFVCVXVWYAPRB9/60/2</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    likes: [
        {
            UserID: "34177",
            UserName: "duc3",
            BirthDate: "1994-04-05",
            Avatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/crop_afr_fem_2.png",
            NeighborhoodID: "29696",
            NeighborhoodName: "Ho Chi Minh City, Vietnam"
        }
    ],
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
  <li><code>FAIL</code> - indicates that db error</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="StopReceiveNotify">Turn ON / OFF receiving notifications on specific threads</h2>
<p>This is a web service allows you to turn ON / OFF receiving notification on a post</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/comment/stop_receive_notify');?>/#Token/#ContentID/#ContentType</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a> </li>
  <li><code>ContentID</code> (<em>required</em>) - the id of the content to be unfollow</li>
  <li><code>Type</code> (<em>required</em>) - type of content to be liked. See detail <a href="#ContentType">Content Type</a></li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/comment/stop_receive_notify');?>/IWPMXYTFVCVXVWYAPRB9/60/2</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
  <li><code>FAIL</code> - indicates that db error</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="ClosedAnnouncement">Close announcement card</h2>
<p>This is a web service allows you to close an announcement card on activity stream screen</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/close_announcement');?>/#Token/#AnnouncementID</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>AnnouncementID</code> (<em>required</em>) - the id of the announcement to be closed</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/close_announcement');?>/IWPMXYTFVCVXVWYAPRB9/1</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
  <li><code>FAIL</code> - indicates that db error</li>
  <li><code>ANNOUNCEMENT_CLOSED</code> - indicates that announcement is closed</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
<h2 id="ListTrendingHashTag">List trending hashtag</h2>
<p>Return list trending hashtag (maximum 10 hashtags)</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/trending_hashtag');?>/#Token/#CateID/#TopicID</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a> </li>
  <li><code>CateID</code> (<em>optional</em>) - Topic category id</li>
  <li><code>TopicID</code> (<em>optional</em>) - topic id</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/trending_hashtag');?>/IWPMXYTFVCVXVWYAPRB9/1/0</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "ListTrending": [
        {
            "HashTag": "princess"
        },
        {
            "HashTag": "anomo"
        },
        {
            "HashTag": "anomorocks"
        },
        {
            "HashTag": "duc"
        },
        {
            "HashTag": "seahawks"
        }
    ],
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
  <li><code>FAIL</code> - indicates that db error</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
<h2 id="OnBoardingHashTag">List onboarding hashtag</h2>
<p>Return list onboarding hashtag (configure via the admin dashboard)</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/onboarding_hashtag');?>/#Token</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a> </li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/onboarding_hashtag');?>/IWPMXYTFVCVXVWYAPRB9</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "ListTrending": [
        {
            "HashTag": "princess"
        },
        {
            "HashTag": "anomo"
        },
        {
            "HashTag": "anomorocks"
        }
    ],
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
  <li><code>FAIL</code> - indicates that db error</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="DeleteComment">Delete comment</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/comment/delete');?>/#Token/#CommentID</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a> </li>
  <li><code>CommentID</code> (<em>required</em>) - comment id</li>
  
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/comment/delete');?>/IWPMXYTFVCVXVWYAPRB9/60</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
</ul>