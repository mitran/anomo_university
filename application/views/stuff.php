<h1 id="Stuff" style="margin:1em 0 0 -10px;">Stuff</h1>
<p>Description for Stuff.</p>

<ol class="toc">
	<li><a href="#PurchaseCredit">Purchase Credit</a></li>
	<li><a href="#SendGift">(UPDATE) Send Gift</a></li>
	<li><a href="#GetAllStuff">Get all stuff</a></li>
	<li><a href="#GetStuffOfUser">Get stuff of user</a></li>
	<li><a href="#GetAllSystemGift">Get all gifts</a></li>
	<li><a href="#GetStuffHistory">Get stuff history</a></li>
	<li><a href="#GetAllPackage">Get all package</a></li>
	<li><a href="#GetTotalPush">(UPDATE) Get total Msg, Gift, Reveal to Push Notification</a></li>
</ol>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="SendGift">Send gift </h2>
<p class="warning">Status: Deprecated</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/send_gift');?>/#token/#TargetUserID/#GiftID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>TargetUserID</code> (<em>required</em>) - ...</li>
  <li><code>GiftID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/send_gift');?>/IWPMXYTFVCVXVWYAPRB9/2/1</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"Credits":350,"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"Credits"</code> credit of user</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#SendGiftStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SendGiftStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>BLOCK</code> - if current user blocked target user</li>
  <li><code>BLOCK_TARGET</code> - if target user blocked current user</li>
  <li><code>FAIL</code> - indicates that errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
  <li><code>NOT_ENOUGH_CREDITS</code> - indicates that this user does not have enough credits to send this gift. </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="PurchaseCredit">Purchase Credit</h2>
<p class="warning">Status: Deprecated</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/purchase_credit');?>/#token</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
</ul>
 
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>CreditPackageID</code> (<em>required</em>) - id of credit</li>
  <li><code>Platform</code> (<em>required</em>) - ios/android</li>
  <li><code>Receipt</code> (<em>required</em>) - ...</li>
  <li><code>Signature</code> (<em>required / optional</em>) - (Android: required / ios: optional)</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/purchase_credit/IWPMXYTFVCVXVWYAPRB9')?>">
	CreditPackageID	<input type="text" name="CreditPackageID"/>
	Platform	<input type="text" name="Platform"/>
	Receipt		<input type="text" name="Receipt"/>
	Signature	<input type="text" name="Signature"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>


<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"UserCredits":290,"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#SendGiftStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SendGiftStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>FAIL</code> - indicates that errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
  <li><code>INVALID_RECEIPT</code> - indicates that receipt is invalid. </li>
  <li><code>RECEIPT_EXISTED</code> - indicates that receipt is existed. </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetAllStuff">Get all stuff</h2>
<p class="warning">Status: Deprecated</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_all_stuff');?>/#token/#UserID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_all_stuff');?>/IWPMXYTFVCVXVWYAPRB9/1</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"MyStuff":[{"CategoryName":"New Year","Gift":{"GiftID":"2","Name":"Pig","Description":"blablabla bla bla","DescriptionOnBuying":"description","Credits":"20","Photo":null,"SendUserID":"3","UserName":"zSniperz","Gender":"2","CreatedDate":"2011-12-26 14:26:07"}}],"GiftHistory":{"GiftHistory":[{"GiftID":"1","Name":"Pig1 3","SendUserID":"1","ReceiveUserID":"2","CreatedDate":"2011-12-26 14:25:52","SendUserName":"MiTran","ReceiveUserName":"oh_yes!"},{"GiftID":"2","Name":"Pig","SendUserID":"3","ReceiveUserID":"1","CreatedDate":"2011-12-26 14:26:07","SendUserName":"zSniperz","ReceiveUserName":"MiTran"},{"GiftID":"2","Name":"Pig","SendUserID":"1","ReceiveUserID":"2","CreatedDate":"2012-02-01 03:56:02","SendUserName":"MiTran","ReceiveUserName":"oh_yes!"}],"CreditHistory":[]},"ListCredit":[{"ID":"1","Credits":"100","Price":"10","ProductID":"tretretertvtertre"},{"ID":"2","Credits":"500","Price":"20","ProductID":null}],"ListGift":[{"GiftID":"1","Name":"Pig1 3","GiftCategoryID":"1","Description":"description","DescriptionOnBuying":"description","Credits":"10","Photo":"640_a.png","PricePhoto":null},{"GiftID":"2","Name":"Pig","GiftCategoryID":"2","Description":"blablabla bla bla","DescriptionOnBuying":"description","Credits":"20","Photo":null,"PricePhoto":null},{"GiftID":"3","Name":"New gift","GiftCategoryID":"2","Description":"this is description","DescriptionOnBuying":"description","Credits":"20","Photo":"379077_271666309538909_266074066764800_807492_451501143_n4.jpg","PricePhoto":null},{"GiftID":"4","Name":"aa","GiftCategoryID":"1","Description":"","DescriptionOnBuying":"description","Credits":"5","Photo":"640_b3.png","PricePhoto":"640_b11.png"}],"CurrentPoint":"1000","code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"MyStuff"</code>- ...</li>
  <li><code>"GiftHistory"</code>- ...</li>
  <li><code>"ListCredit"</code>- ...</li>
  <li><code>"ListGift"</code>- ...</li>
  <li><code>"CurrentPoint"</code>- ...</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetAllStuffStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetAllStuffStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetStuffOfUser">Get stuff of user</h2>
<p class="warning">Status: Deprecated</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_stuffs');?>/#token</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_stuffs');?>/IWPMXYTFVCVXVWYAPRB9</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"ListStuff":[{"CategoryName":"Noel","ListGift":[{"GiftID":"1","Name":"Pig1 3","Description":"description","DescriptionOnBuying":"description","Credits":"10","Photo":"http:\/\/localhost:81\/anomo\/PHP Webservice\/public\/upload\/640_a.png","SendUserID":"1","UserName":"MiTran","Gender":"2","CreatedDate":"2011-12-26 14:25:52"}]},{"CategoryName":"New Year","ListGift":[{"GiftID":"2","Name":"Pig","Description":"blablabla bla bla","DescriptionOnBuying":"description","Credits":"20","Photo":null,"SendUserID":"3","UserName":"zSniperz","Gender":"2","CreatedDate":"2011-12-26 14:26:07"},{"GiftID":"2","Name":"Pig","Description":"blablabla bla bla","DescriptionOnBuying":"description","Credits":"20","Photo":null,"SendUserID":"1","UserName":"MiTran","Gender":"2","CreatedDate":"2012-02-01 03:56:02"}]}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"ListStuff"</code>- ...</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetAllStuffStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetAllStuffStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetAllSystemGift">Get all gifts</h2>
<p class="warning">Status: Deprecated</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_all_gift');?>/#token</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_all_gift');?>/IWPMXYTFVCVXVWYAPRB9</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"Gifts":[{"CategoryName":"Noel","ListGift":[{"GiftID":"1","Name":"Pig1 3","GiftCategoryID":"1","Description":"description","DescriptionOnBuying":"description","Credits":"10","Photo":"http:\/\/localhost:81\/anomo\/PHP Webservice\/public\/upload\/640_a.png","PricePhoto":null},{"GiftID":"4","Name":"aa","GiftCategoryID":"1","Description":"","DescriptionOnBuying":"description","Credits":"5","Photo":"http:\/\/localhost:81\/anomo\/PHP Webservice\/public\/upload\/640_b3.png","PricePhoto":"http:\/\/localhost:81\/anomo\/PHP Webservice\/public\/upload\/640_b11.png"}]},{"CategoryName":"New Year","ListGift":[{"GiftID":"2","Name":"Pig","GiftCategoryID":"2","Description":"blablabla bla bla","DescriptionOnBuying":"description","Credits":"20","Photo":null,"PricePhoto":null},{"GiftID":"3","Name":"New gift","GiftCategoryID":"2","Description":"this is description","DescriptionOnBuying":"description","Credits":"20","Photo":"http:\/\/localhost:81\/anomo\/PHP Webservice\/public\/upload\/379077_271666309538909_266074066764800_807492_451501143_n4.jpg","PricePhoto":null}]}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"Gifts"</code>- list gift</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetAllStuffStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetAllStuffStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetStuffHistory">Get stuff history</h2>
<p class="warning">Status: Deprecated</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_stuff_history');?>/#token</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_stuff_history');?>/IWPMXYTFVCVXVWYAPRB9</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"StuffHistory":{"GiftHistory":[{"GiftID":"1","Name":"Pig1 3","SendUserID":"1","ReceiveUserID":"2","CreatedDate":"2011-12-26 14:25:52","SendUserName":"Mi","ReceiveUserName":"oh_yes!"},{"GiftID":"2","Name":"Pig","SendUserID":"1","ReceiveUserID":"2","CreatedDate":"2012-02-01 03:56:02","SendUserName":"Mi","ReceiveUserName":"oh_yes!"},{"GiftID":"1","Name":"Pig1 3","SendUserID":"2","ReceiveUserID":"3","CreatedDate":"2012-03-07 03:32:58","SendUserName":"oh_yes!","ReceiveUserName":"zSniperz"}],"CreditHistory":[{"TransactionID":"1","CreditPackageID":"1","Credits":"100","Price":"10","CreatedDate":"2011-12-26 14:31:03"},{"TransactionID":"3","CreditPackageID":"1","Credits":"100","Price":"10","CreatedDate":"2012-02-02 04:39:18"},{"TransactionID":"4","CreditPackageID":"1","Credits":"100","Price":"10","CreatedDate":"2012-02-02 09:48:11"}]},"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"StuffHistory"</code>- ...</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetAllStuffStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetAllStuffStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetAllPackage">Get all package</h2>
<p class="warning">Status: Deprecated</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_all_package');?>/#token</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_all_package');?>/IWPMXYTFVCVXVWYAPRB9</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"UserCredits":"2950","ListPackage":[{"ID":"1","Credits":"500","Price":"0.99","ProductID":"com.vinasource.anomotest1","Platform":"ios"},{"ID":"2","Credits":"2000","Price":"2.99","ProductID":"com.vinasource.anomotest12","Platform":"ios"},{"ID":"3","Credits":"5000","Price":"4.99","ProductID":"com.vinasource.anomotest13","Platform":"ios"},{"ID":"4","Credits":"15000","Price":"9.99","ProductID":"com.vinasource.anomotest14","Platform":"ios"},{"ID":"5","Credits":"500","Price":"0.99","ProductID":"google.anomo.test","Platform":"android"}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"UserCredits"</code>- credits of user</li>
  <li><code>"ListPackage"</code>- list package</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetAllStuffStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetAllStuffStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
</ul>



<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetTotalPush">Get total Msg, Gift, Reveal to Push Notification</h2>
<p class="warning">Status: Deprecated</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_total_push');?>/#token</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_total_push');?>/IWPMXYTFVCVXVWYAPRB9</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"Msg":4,"Gift":0,"Reveal":1,"Anomotion":0,"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"Msg"</code>- number msg of user</li>
  <li><code>"Gift"</code>- number gift of user</li>
  <li><code>"Reveal"</code>- number reveal of user</li>
  <li><code>"Anomotion"</code>- number anomotion of user</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetAllStuffStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetAllStuffStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
