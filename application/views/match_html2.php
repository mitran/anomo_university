<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>anomo match</title>
<style>
* {
	margin: 0;
	padding: 0;
}
.clearfix:before, .clearfix:after{content:"";display:table;}
.clearfix:after{clear:both;}
.clearfix{zoom:1;}
.clear{clear:both;height:0;line-height:0;}

.table-css{display:table;}
.table-row{display:table-row;}
.table-cell{display:table-cell;}
.anomo-match{width:640px;height:960px;background:url(<?php echo base_url();?>public/assets/img/bg.jpg);background-size:cover;/*min-height:960px;*/color:#ffffff;font-size:34px;line-height:38px;font-family: 'allerlight', sans-serif;overflow: hidden;}
.bg-header{background:url(<?php echo base_url();?>public/assets/img/bg_header.jpg) no-repeat left top;background-size:contain;padding:50px 0;}
.section-profile{padding:0 50px;margin-top:100px;}
.section-profile header{display:block;margin-bottom:32px;font-family:'pacificoregular', cursive;font-size:50px;line-height:54px;text-align:center;}
.section-profile .table-profile{width:100%;}
.section-profile .table-profile .table-cell{font-size:28px;line-height:32px;vertical-align:middle;}
.section-profile .table-profile .table-cell:first-child{width:155px;}
.section-profile .avatar{width:140px;height:140px;background:center center no-repeat;border:3px solid #ffffff;-webkit-border-radius:100%;-moz-border-radius:100%;border-radius:100%;overflow:hidden;}
.section-profile h3{margin:0 0 5px;font-size:30px;line-height:34px;font-family: 'allerregular', sans-serif;}
.section-profile .table-profile-info .table-cell{vertical-align:top;}
.section-profile .table-profile-info .table-cell:first-child{width:35px;}
.section-profile .age{}
.section-profile address{display:list-item;margin-left:40px;font-style:normal;}
</style>
</head>
<body>
<div class="anomo-match">	
	<div class="bg-header">
		<section class="section-profile">
			<header>			
			</header>
			<div class="table-css table-profile">
				<div class="table-row">
					<div class="table-cell">
						
					</div>
					<div class="table-cell">
						<h3><?php echo $info->UserName?></h3>
                        <div class="table-css table-profile-info">
                            <div class="table-row">
                                <div class="table-cell">
                                	<span class="age"><?php echo $info->Age?></span>
                                </div>
                                <div class="table-cell">
                                	<address><?php echo $info->NeighborhoodName?></address>
                                </div>
                            </div>
                        </div>						 
					</div>
				</div>
			</div>    	
		</section>
	</div>
</div>
</body>
</html>