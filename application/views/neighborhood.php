<h1 id="Neighborhood" style="margin:1em 0 0 -10px;">Neighborhood</h1>
<ol class="toc">
	<li><a href="#get_neighborhood_by_coordinates">Get neighborhood by coordinates</a></li>
	<li><a href="#GetNeighborhoodDetail">Get neighborhood detail</a></li>
	<li><a href="#PostToNeighborhood">Post text/picture to neighborhood</a></li>
	<li><a href="#NeighborhoodComment">Post comment</a></li>
	<li><a href="#CommentOfPostOfNeighborhood">Get comment of post</a></li>
	<li><a href="#GetPictureWallNeighborhood">Get picture wall</a></li>
	<li><a href="#GetWallPostV2Neighborhood">Get wall post</a></li>
	<li><a href="#SendMessageInNeighborhood">Send message in neighborhood</a></li>
	<li><a href="#GetMessagesInNeighborhood">Get messages in neighborhood</a></li>
	<li><a href="#GetAnomoNeighborhood">Get Anomos</a></li>
	<li><a href="#DeletePostNbh">Delete post</a></li>
</ol>

<!-- ---------------------------------------------- -->
<h2 id="get_neighborhood_by_coordinates">Get neighborhood by coordinates</h2>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/get_neighborhood_by_coordinates');?>/#token/#lat/#lon</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>lat</code> (<em>required</em>) - ...</li>
  <li><code>lon</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/get_neighborhood_by_coordinates');?>/IWPMXYTFVCVXVWYAPRB9/61/-149.3</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"results":{"OGR_FID":"219","STATE":"AK","COUNTY":"Anchorage","CITY":"Anchorage","NAME":"Turnagain Arm","REGIONID":"275783","SHAPE":null},"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
</ul>

<h2 id="GetNeighborhoodDetail">Get neighborhood detail</h2>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/get_detail');?>/#token/#UserID/#NeighborhoodID</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>NeighborhoodID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/get_detail');?>/IWPMXYTFVCVXVWYAPRB9/1/191</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"results":{"PlaceID":"191","Name":"Northeast","Address":"","Lat":"","Lon":"","Category":[],"Phone":"","CheckInStatus":0,"ListTag":[],"ListAnomo":[],"NumberAnomo":0,"NumberAnomoPast":0,"ListTextPost":[{"UserID":"1","UserName":"Frian Nguyen","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/12d8a7be5192abbd05521885325bdb48.jpg","Gender":"0","ID":"5","Content":"afdafds","Photo":"","CreatedDate":"2012-12-03 09:19:11","ConnectMeter":0}],"NumberTextPost":4,"ListPicturePost":[{"Photo":"http:\/\/localhost\/anomo\/public\/upload\/4617d8477f2a1379c5f29f5007f8c97e.jpg"}],"NumberPicturePost":1,"NumberTagPast":0,"NumberOfUserCheckedIn":0},"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"CheckInStatus"</code> 0: not check in / 1: checked in / 3: spectate</li>
  <li><code>"ListAnomo"</code> Status: 1 - checked in / 3 - specatate; list of user checkin / spectate on this place (avatar) </li>
  <li><code>"ListTextPost"</code> list post on this place [post id, picture url, text, date, user id, avatar, name, gender, connect meter] </li>
  <li><code>"ListPicturePost"</code> list post on this place [ picture url] </li>
  <li><code>"NumberAnomo"</code> total user is checkin-spectate </li>
  <li><code>"NumberAnomoPast"</code> total user is checkin-spectate in past</li>
  <li><code>"NumberTextPost"</code> total the text post on this place </li>
  <li><code>"NumberPicturePost"</code> total the picture post on this place  </li>
  <li><code>"ListTag"</code> tagid, tag, tag count </li>
  <li><code>"NumberTagPast"</code> total tags of user checkin in past</li>
  <li><code>"NumberOfUserCheckedIn"</code> number of user check in this place </li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="PostToNeighborhood">Post text/picture to neighborhood</h2>

<p>This request is an HTTP URL with POST method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/post_to_neighborhood');?>/#token/#UserID/#NeighborhoodID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>NeighborhoodID</code> (<em>required</em>) - ...</li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Content</code> <em></em> - text</li>
  <li><code>Photo</code> <em></em> - </li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/neighborhood/post_to_neighborhood/IWPMXYTFVCVXVWYAPRB9/1/191')?>">
	Content	<input type="text" name="Content"/>
	Photo 	<input type="file" name="Photo">
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"post":{"ID":"3","NeighborhoodID":"191","UserID":"1","Content":"test post contnet","Photo":"","IsMark":"0","CreatedDate":"2012-11-30 03:08:20","UserName":"Frian Nguyen","Gender":"0","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/12d8a7be5192abbd05521885325bdb48.jpg"},"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
 <li><code>"post"</code> contains information of the post. Only return when the status code is OK</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#SendMsgPlaceStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SendMsgPlaceStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param or place or user is not exist </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="NeighborhoodComment">Post a comment to a post of neighborhood</h2>

<p>This request is an HTTP URL with POST method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/post_comment');?>/#token/#UserID/#PostID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>PostID</code> (<em>required</em>) - ...</li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Content</code> (<em>required</em>) - text</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/neighborhood/post_comment/IWPMXYTFVCVXVWYAPRB9/1/1')?>">
	Content	<input type="text" name="Content"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"comment":{"ID":"5","Content":"new comment","CreatedDate":"2012-01-20 03:59:09","UserID":"1","UserName":"Mi Tran","Avatar":"","Gender":"1"},"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"comment"</code> contains information of the comment. Only return when the status code is OK</li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>BLOCK </code> - if current user blocked author of post</li>
  <li><code>BLOCK_TARGET</code> - if author of post blocked current user</li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param or post or user is not exist </li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>



<h2 id="CommentOfPostOfNeighborhood">Get comment of post</h2>

<p>Get messages in place request is an HTTP URL with GET method of the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/get_comment');?>/#token/#PostID</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>PostID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/get_comment');?>/IWPMXYTFVCVXVWYAPRB9/3</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"Total":2,"result":[{"ID":"1","Content":"this is comment","CreatedDate":"2012-11-30 03:22:41","UserID":"1","UserName":"Frian Nguyen","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/12d8a7be5192abbd05521885325bdb48.jpg","Gender":"0","ConnectMeter":0},{"ID":"2","Content":"teste","CreatedDate":"2012-11-30 03:36:37","UserID":"1","UserName":"Frian Nguyen","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/12d8a7be5192abbd05521885325bdb48.jpg","Gender":"0","ConnectMeter":0}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"result"</code>list comment (name, avatar , user id, text, date, connect meter, gender)</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetMessagesInPlaceStatusCodes">Status Codes</a> below.</li>  
</ul>

<h3 id="GetMessagesInPlaceStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; A new user was created successfully.</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> request without post method, or parameters are missing.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<h2 id="GetPictureWallNeighborhood">Get picture wall</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/get_picture_wall');?>/#token/#NeighborhoodID/#Page</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>NeighborhoodID</code> (<em>required</em>) - ...</li>
  <li><code>Page</code> (<em>required</em>) - default is 1</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/get_picture_wall_v2');?>/IWPMXYTFVCVXVWYAPRB9/191</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"results":{"TotalPage":1,"CurrentPage":1,"Posts":[{"UserID":"7","UserName":"tung","Avatar":"http:\/\/localhost\/Anomo\/public\/upload\/487abd5bdf850b7596b468bf02a1f81d.jpg","ID":"2","Content":"Hello all","Photo":"http:\/\/localhost\/Anomo\/public\/upload\/fa3d0aaff9c8d2426b4660618666b42c.jpg","CreatedDate":"2012-08-02 08:20:26","TotalComment":"0"},{"UserID":"7","UserName":"tung","Avatar":"http:\/\/localhost\/Anomo\/public\/upload\/487abd5bdf850b7596b468bf02a1f81d.jpg","ID":"1","Content":"Beatiful girl ^^","Photo":"http:\/\/localhost\/Anomo\/public\/upload\/2fcef92860eea13217148ceaf8ecd25b.jpg","CreatedDate":"2012-08-02 08:19:47","TotalComment":"0"}]},"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"results"</code> list post on this place [post id, text, date, user id, avatar, name, gender, connect meter, array of comments (name, avatar , user id, text, date, connect meter, gender)]  </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>



<h2 id="GetWallPostV2Neighborhood">Get wall post</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/get_wall_post_v2');?>/#token/#NeighborhoodID/#Page</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>NeighborhoodID</code> (<em>required</em>) - ...</li>
  <li><code>Page</code> (<em>required</em>) - default is 1</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/get_wall_post_v2');?>/IWPMXYTFVCVXVWYAPRB9/191</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"TotalPage":1,"CurrentPage":1,"WallPostList":[{"UserID":"28","UserName":"dung","Avatar":"http:\/\/localhost\/Anomo\/public\/upload\/af97b548984b0c8e86bb575b0ebca863.jpg","ID":"3","Content":"Hi","Photo":"","CreatedDate":"2012-08-02 10:33:33","TotalComment":"0"},{"UserID":"7","UserName":"tung","Avatar":"http:\/\/localhost\/Anomo\/public\/upload\/487abd5bdf850b7596b468bf02a1f81d.jpg","ID":"2","Content":"Hello all","Photo":"http:\/\/localhost\/Anomo\/public\/upload\/fa3d0aaff9c8d2426b4660618666b42c.jpg","CreatedDate":"2012-08-02 08:20:26","TotalComment":"0"},{"UserID":"7","UserName":"tung","Avatar":"http:\/\/localhost\/Anomo\/public\/upload\/487abd5bdf850b7596b468bf02a1f81d.jpg","ID":"1","Content":"Beatiful girl ^^","Photo":"http:\/\/localhost\/Anomo\/public\/upload\/2fcef92860eea13217148ceaf8ecd25b.jpg","CreatedDate":"2012-08-02 08:19:47","TotalComment":"0"}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"WallPostList"</code> list post on this place [post id, picture url, text, date, user id, avatar, name, total comment]  </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#GetWallPostV2StatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetWallPostV2StatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<h2 id="SendMessageInNeighborhood">Send message in neighborhood</h2>

<p>Send message in place request is an HTTP URL with POST method of the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/send_message');?>/#token/#UserID/#NeighborhoodID</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>NeighborhoodID</code> (<em>required</em>) - ...</li>
</ul>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Content</code> (<em>option</em>) - ...</li>
  <li><code>Photo</code> (<em>option</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<ul>
<li>Request Header :</li>
	+ Name: Content-Type
	+ Value: application/x-www-form-urlencoded
<li>Request Body :</li>
</ul>
</pre>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/neighborhood/send_message/IWPMXYTFVCVXVWYAPRB9/2/191')?>">
	Content	<input type="text" name="Content"/>
	Photo	<input type="file" name="Photo"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"results":{"NeighborhoodID":"191","UserID":"2","Content":"","Photo":"http:\/\/localhost\/anomo\/public\/upload\/fed4c510d542f47296c9e3a9f3ab45d1.jpg","Photo100":"http:\/\/localhost\/anomo\/public\/upload\/fed4c510d542f47296c9e3a9f3ab45d1_thumb_100x150.jpg","Photo200":"http:\/\/localhost\/anomo\/public\/upload\/fed4c510d542f47296c9e3a9f3ab45d1_thumb_200x300.jpg","Photo300":"http:\/\/localhost\/anomo\/public\/upload\/fed4c510d542f47296c9e3a9f3ab45d1_thumb_300x450.jpg","CreatedDate":"2013-03-01 10:43:54","ID":3},"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#SendMessageInPlaceStatusCodes">Status Codes</a> below.</li>  
</ul>

<h3 id="SendMessageInPlaceStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; A new user was created successfully.</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> request without post method, or parameters are missing.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>



<h2 id="GetMessagesInNeighborhood">Get messages in neighborhood</h2>

<p>Get messages in place request is an HTTP URL with GET method of the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/get_messages');?>/#token/#UserID/#NeighborhoodID</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>NeighborhoodID</code> (<em>required</em>) - ...</li>
  <li><code>MessageID</code> (<em>required</em>) - (-1:get 50 latest messages, messageID: get latest messages from messageID)</li>
  <li><code>PositionFlag</code> (<em>required</em>) - (1:load latest, 2: load more)</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/get_messages');?>/IWPMXYTFVCVXVWYAPRB9/1/191/-1/2</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"results":{"Messages":[{"ID":"12","Content":"sdfs\u0111fg","CreatedDate":"2012-09-26 10:26:07","ColorID":"1","UserID":"1","UserName":"sdfasdf","Avatar":"a8ecd4d95a756c7d8be4fd07395860ed.jpg"},{"ID":"13","Content":"sdfs\u0111fg","CreatedDate":"2012-09-26 10:28:42","ColorID":"1","UserID":"1","UserName":"sdfasdf","Avatar":"a8ecd4d95a756c7d8be4fd07395860ed.jpg"},{"ID":"14","Content":"werwer","CreatedDate":"2012-09-26 10:34:15","ColorID":"1","UserID":"1","UserName":"sdfasdf","Avatar":"a8ecd4d95a756c7d8be4fd07395860ed.jpg"}],"NumOfChattingUser":1},"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetMessagesInPlaceStatusCodes">Status Codes</a> below.</li>  
</ul>

<h3 id="GetMessagesInPlaceStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; A new user was created successfully.</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> request without post method, or parameters are missing.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>



<h2 id="GetAnomoNeighborhood">Get Anomos</h2>

<p>This request is an HTTP URL with GET method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/get_anomos_v2');?>/#token/#UserID/#NeighborhoodID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
  <li><code>NeighborhoodID</code> (<em>required</em>) - ...</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/get_anomos_v2');?>/IWPMXYTFVCVXVWYAPRB9/2/191</span></em></pre>
 
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"TotalPage":1,"CurrentPage":1,"ListUser":[{"UserID":"1","UserName":"Teo","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/12d8a7be5192abbd05521885325bdb48.jpg","ProfileStatus":"","Status":0,"Tags":[],"IsOnline":1},{"UserID":"10","UserName":"MyTa","Avatar":"http:\/\/localhost\/anomo\/public\/upload\/4569e03211275ed5b9ab57b57b27bef5.jpg","ProfileStatus":null,"Status":0,"Tags":[{"TagID":"12","Name":"game"}],"IsOnline":0},{"UserID":"11","UserName":"Mi","Avatar":"","ProfileStatus":null,"Status":0,"Tags":[],"IsOnline":0}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"ListUser"</code> Status: 1 - checked in;  list of user checkin on this place (user id, avatar, tags, profile status, status) </li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#GetAnomosV2StatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetAnomosV2StatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param or incorrect type of param</li>
  <li><code>INVALID_PARAMETER</code> - indicates that incorrect param</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="DeletePostNbh">Delete Post</h2>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/delete_post');?>/#token/#PostID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>PostID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/neighborhood/delete_post');?>/IWPMXYTFVCVXVWYAPRB9/2</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetContactOfUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetContactOfUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid or user is not existed; </li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
