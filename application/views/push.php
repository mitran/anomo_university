<h1 id="PushNotification" style="margin:1em 0 0 -10px;">Push notification</h1>

With Type:
<ul>
  <li><code>1 </code>:Msg</li>
  <li><code>2 </code>:Gift</li>
<!--  <li><code>3 </code>:Anomotion</li>-->
  
<!--  <li><code>5 </code>:Comment Picture Post</li>
  <li><code>6 </code>:Comment Text Post</li>-->
  <li><code>7 </code>:Comment Activity</li>
  <li><code>8 </code>:Like Activity</li>
  <li><code>9 </code>:Follow</li>
  <li><code>10 </code>:Invite game</li>
  <li><code>11 </code>:Finished game</li>
<!--  <li><code>12 </code>:Complete game</li>-->
  <li><code>13 </code>:mention user (@name)</li>
  <li><code>14 </code>:like on comment</li>
  
  <li><code>4 </code>:Request reveal</li>
  <li><code>15 </code>:Accept reveal</li>

</ul>

ANDROID

<pre class="prettyprint">
{
	Type: 8,
	SenderName: "Test9090",
	Message: "Test9090 likes your post",
	NumberBagde: "2",
	SenderAvatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/0a2b1d07d82f950e26e5eafe6f101584.jpg",
	SenderID: "1",
	ReceiverID: "1017",
	ContentType: "1",
	ContentID: "2250",
	AnomotionID: -1,
	HistoryID: "18523",
        RevealType: -1,
        RevealContent: "",
        RevealPictureID: -1
}
</pre>

IOS
<pre class="prettyprint">
         * t: type
         * f: sender id
         * u: receiver id
         * n: sender name
         * p: sender avatar
         * c: content type
         * i: content id
         * a: anomotion id
         * h: history id
         * r: reveal type
         * e: reveal content
         * v: reveal picture id
</pre>
<pre class="prettyprint">
{
	aps: {
		alert: "Test9090 likes your post",
		badge: 1,
		sound: "abc.aiff"
	},
	t: 8,
	f: "1",
	u: "33805",
	n: "Test9090",
	p: "http://anomo-sandbox.s3.amazonaws.com/Upload/0a2b1d07d82f950e26e5eafe6f101584.jpg",
	c: "1",
	i: "2242",
	a: -1,
	h: "18522",
        r: -1,
        e: "",
        v: -1
}
</pre>

<h1 id="ChatPush" style="margin:1em 0 0 -10px;">IOS Chat Push </h1>

<ol class="toc">
	<li><a href="#SendIOSChatPush">Send IOS Chat Push</a></li>
	<li><a href="#UpdateClientNotificationBadgeCount ">Update Client Notification Badge Count</a></li>
</ol>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="SendIOSChatPush">Send IOS Chat Push</h2>
<p>This is a web service allows you to send a push notification <br />
Message push: <br />
<pre class="prettyprint"> {"aps":{"alert":"[userName] says, [part of msg]","sound":"receiveText.aiff","badge”:[count]},"AIM":"YES"} </pre>
<pre class="prettyprint"> {"aps":{"alert":"[userName] sent you a picture.","sound":"receiveText.aiff","badge”:[count]},"AIM":"YES"} </pre>
</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/chat/send_ios_chat_push');?>/#token/#ReceiverID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>ReceiverID</code> (<em>required</em>) - ...</li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Message</code> (<em>required</em>) - text</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/chat/send_ios_chat_push/IWPMXYTFVCVXVWYAPRB9/2')?>">
	Message	<input type="text" name="Message"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#SendGiftStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SendGiftStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="UpdateClientNotificationBadgeCount">Update Client Notification Badge Count</h2>
<p>This web service will be update Chat Count to the value passed in by the client</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/chat/update_client_notification_badge_count');?>/#token/#chat_count</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/chat/update_client_notification_badge_count');?>/IWPMXYTFVCVXVWYAPRB9/1</span></pre>


<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#SendGiftStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SendGiftStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that invalid request</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>FAIL</code> - indicates that db error.</li>
</ul>