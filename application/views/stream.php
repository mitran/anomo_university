<h1 id="Stream" style="margin:1em 0 0 -10px;">Stream</h1>
<ol class="toc">
	<li><a href="#GetListCategory">Get list category</a></li>
    <li><a href="#GetListTopic">Get list topic</a></li>
        <li><a href="#GetListFavourite">Get list favourite</a></li>
        
        <li><a href="#AddTopicFavourite">Add / Remove Favourite</a></li>
        <li><a href="#SearchTopic">Search topic</a></li>
        <li><a href="#GetTopic">Get topic detail</a></li>
        <li><a href="#AddTopic">Add new topic</a></li>
        <li><a href="#EditTopic">Edit topic</a></li>
        <li><a href="#GetTopic">Get topic detail</a></li>
        <li><a href="#DeleteTopic">Delete topic</a></li>
        <li><a href="#CloseTopic">Close topic</a></li>
        <li><a href="#BanUserOnTopic">Ban / Unban user on topic</a></li>
</ol>

<h2 id="GetListCategory">Get list category</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/get_list_category');?>/#Token</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/get_list_category');?>/IWPMXYTFVCVXVWYAPRB9</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "results": {
        "ListCategory": [
            {
                "CateID": "11",
                "CateName": "Do you wanna?",
                "OrderID": "1",
                "AllowCreateTopic": "1",
                "Photo": "http://uw-sandbox.s3.amazonaws.com/Stream/Athletics2@3x.png",
                "TotalTopic": "0"
            },
            {
                "CateID": "10",
                "CateName": "Social",
                "OrderID": "2",
                "AllowCreateTopic": "1",
                "Photo": "http://uw-sandbox.s3.amazonaws.com/Stream/Parties@3x.png",
                "TotalTopic": "4"
            },
            {
                "CateID": "3",
                "CateName": "Major",
                "OrderID": "3",
                "AllowCreateTopic": "1",
                "Photo": "http://uw-sandbox.s3.amazonaws.com/Stream/Major@3x.png",
                "TotalTopic": "4"
            },
            {
                "CateID": "14",
                "CateName": "Classes",
                "OrderID": "4",
                "AllowCreateTopic": "1",
                "Photo": "http://uw-sandbox.s3.amazonaws.com/Stream/classes@3x.png",
                "TotalTopic": "0"
            }
            
        ],
        "ListTopic": [
            {
                "TopicID": "1",
                "TopicName": "Random Thoughts",
                "TotalPost": "0",
                "UserID": "1",
                "Desc": "bla bla",
                "Photo": "http://uw-sandbox.s3.amazonaws.com/Stream/General.png",
                "Photo100": "http://uw-sandbox.s3.amazonaws.com/Stream/General.png"
            }
        ]
    },
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<!-- Get list topic -->
<h2 id="GetListTopic">Get list topic</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/get_list_topic');?>/#Token/#CateID/#Page</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>CateID</code> (<em>required</em>) - Category ID</li>
  <li><code>Page</code> (<em>optional</em>) - Page ID</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/get_list_topic');?>/IWPMXYTFVCVXVWYAPRB9/1/1</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "results": [
        {
            "TopicID": "3",
            "TopicName": "Choi Chim",
            "UserID": "1",
            "Desc": "Birds (class Aves or clade Avialae) are feathered, winged, two-legged, warm-blooded, egg-laying vertebrates. Modern birds are characterised by feathers, .",
            "CreatedDate": "2014-12-24 11:01:59",
            "TotalPost": "0",
            "Photo": "http://anomo-sandbox.s3.amazonaws.com/Stream/anomo.png",
            "Photo100": "http://anomo-sandbox.s3.amazonaws.com/Stream/anomo.png",
            "IsFavorite": "0"
        },
        {
            "TopicID": "2",
            "TopicName": "Football",
            "UserID": "2",
            "Desc": "Football refers to a number of sports that involve, to varying degrees, kicking a ball with the foot to score a goal. Unqualified, the word football is understood to ",
            "CreatedDate": "2014-12-24 10:58:46",
            "TotalPost": "0",
            "Photo": "http://anomo-sandbox.s3.amazonaws.com/Stream/anomo.png",
            "Photo100": "http://anomo-sandbox.s3.amazonaws.com/Stream/anomo.png",
            "IsFavorite": "0"
        },
        {
            "TopicID": "1",
            "TopicName": "Ping Pong",
            "UserID": "1",
            "Desc": "Table tennis (also called ping pong) is a sport in which two or four players hit a lightweight ball back and forth using a table tennis bat. The game takes place on a hard table divided by a net. Except for the initial serve, players must allow a bal",
            "CreatedDate": "2014-12-24 10:57:53",
            "TotalPost": "11",
            "Photo": "http://anomo-sandbox.s3.amazonaws.com/Stream/anomo.png",
            "Photo100": "http://anomo-sandbox.s3.amazonaws.com/Stream/anomo.png",
            "IsFavorite": "0"
        }
    ],
    "TotalPage": 2,
    "CurrentPage": "2",
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetListFavourite">Get list favourite</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/get_list_favourite');?>/#Token/#Page</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>Page</code> (<em>required</em>) - default is 1</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/get_list_favourite');?>/IWPMXYTFVCVXVWYAPRB9</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "results": [
        {
            "TopicID": "1",
            "TopicName": "Ping Pong",
            "UserID": "1",
            "Desc": "Table tennis (also called ping pong) is a sport in which two or four players hit a lightweight ball back and forth using a table tennis bat. The game takes place on a hard table divided by a net. Except for the initial serve, players must allow a bal",
            "CreatedDate": "2014-12-24 10:57:53",
            "TotalPost": "0",
            "Photo": "http://anomo-sandbox.s3.amazonaws.com/Upload/ping_pong.jpg"
        },
        {
            "TopicID": "3",
            "TopicName": "Choi Chim",
            "UserID": "1",
            "Desc": "Birds (class Aves or clade Avialae) are feathered, winged, two-legged, warm-blooded, egg-laying vertebrates. Modern birds are characterised by feathers, .",
            "CreatedDate": "2014-12-24 11:01:59",
            "TotalPost": "0",
            "Photo": "http://anomo-sandbox.s3.amazonaws.com/Upload/chim.jpg"
        }
    ],
    "TotalPage": 1,
    "CurrentPage": 1,
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="AddTopicFavourite">Add / Remove Favourite</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/add_favourite');?>/#Token/#TopicID</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>TopicID</code> (<em>required</em>) - is id of topic</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/add_favourite');?>/IWPMXYTFVCVXVWYAPRB9/1</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that missing parameters.</li>
  <li><code>FAIL</code> - indicates that some errors occurred.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<h2 id="SearchTopic">Search topic</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/search_topic');?>/#Token/#Page</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>Page</code> (<em>required</em>) - default is 1</li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Keyword</code> (<em>required</em>) - </li>
  <li><code>CateID</code> (<em>optional</em>) - </li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/stream/search_topic/IWPMXYTFVCVXVWYAPRB9')?>">
	Keyword		<input type="text" name="Keyword"/>
	<input type="submit" value="Submit" class="button"/>
</form></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "results": [
        {
            "TopicID": "3",
            "TopicName": "Choi Chim",
            "Desc": "Birds (class Aves or clade Avialae) are feathered, winged, two-legged, warm-blooded, egg-laying vertebrates. Modern birds are characterised by feathers, .",
            "TotalPost": "0",
            "CreatedDate": "2014-12-24 11:01:59",
            "UserID": "1",
            "CateID": "1",
            "Photo": "http://anomo-sandbox.s3.amazonaws.com/Upload/chim.jpg",
            "IsFavorite": "1"
        }
    ],
    "TotalPage": 3,
    "CurrentPage": "3",
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that missing parameters.</li>
  <li><code>FAIL</code> - indicates that some errors occurred.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetTopic">Get topic detail</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/get_topic');?>/#Token/#TopicID</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>TopicID</code> (<em>required</em>) - is id of topic</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/get_topic');?>/IWPMXYTFVCVXVWYAPRB9/1</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    results: {
        TopicID: "1",
        TopicName: "Ping Pong",
        Desc: "Table tennis (also called ping pong) is a sport in which two or four players hit a lightweight ball back and forth using a table tennis bat. The game takes place on a hard table divided by a net. Except for the initial serve, players must allow a bal",
        TotalPost: "0",
        CreatedDate: "2014-12-24 10:57:53",
        UserID: "1",
        UserName: "Test000",
        CateID: "1",
        CateName: "Sports",
        Photo: "http://anomo-sandbox.s3.amazonaws.com/Upload/ping_pong.jpg",
        Avatar: "",
        IsFavorite: "1"
    },
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that missing parameters.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<!-- Add New Topic -->
<h2 id="AddTopic">Add New Topic</h2>
<p class="note">Status: Active</p>

<p>Add new Topic request is an HTTP URL with POST method of the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/add_topic');?>/#token</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>TopicName</code> (<em>required</em>) - </li>
  <li><code>Desc</code> (<em>required</em>) - </li>
  <li><code>Photo</code> (<em>required</em>) - Topic's photo</li>
  <li><code>CateID</code> (<em>required</em>) - Integer</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/stream/add_topic/IWPMXYTFVCVXVWYAPRB9')?>">
TopicName   <input type="text" name="TopicName"/>
Desc        <input type="text" name="Desc"/>	
Photo       <input type="file" name="Photo"/>	
CateID      <input type="text" name="CateID"/>
<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint">
<span class="str">
{
    "results": {
        "TopicID": "33",
        "UserID": "34110",
        "TopicName": "Dancing3334",
        "Desc": "desc",
        "Photo": "http://anomo-sandbox.s3.amazonaws.com/Stream/c181f46c534d3b79fd0b0cf4a749d8c6.png",
        "Photo100": "http://anomo-sandbox.s3.amazonaws.com/Stream/c181f46c534d3b79fd0b0cf4a749d8c6_thumb_100x100.png",
        "CateID": "1",
        "IsDelete": "0",
        "TotalPost": "0",
        "CreatedDate": "2014-12-30 07:23:51",
        "LastModified": "2014-12-30 07:23:51"
    },
    "code": "OK"
}
</span>
</pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#UpdateUserStatusCodes">Status Codes</a> below.</li>

</ul>

<h3 id="UpdateUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; A new topic was created successfully.</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_PARAMETER</code> - indicates that parameters are missing.</li>    
  <li><code>TOPIC_EXISTED</code> indicates that topic name is existed.</li>
  <li><code>NOT_ALLOW_CREATE_TOPIC</code> indicates that you don't have permission to create topic.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
<!-- End Add new topic -->


<!-- Edit Topic -->
<h2 id="EditTopic">Edit Topic</h2>
<p class="note">Status: Active</p>

<p>Edit topic request is an HTTP URL with POST method of the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/edit_topic');?>/#token</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>TopicName</code> (<em>required</em>) - </li>
  <li><code>Desc</code> (<em>required</em>) - </li>
  <li><code>Photo</code> (<em>required</em>) - Topic's photo</li>
  <li><code>TopicID</code> (<em>required</em>) - Integer</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/stream/edit_topic/IWPMXYTFVCVXVWYAPRB9')?>">
TopicName   <input type="text" name="TopicName"/>
Desc        <input type="text" name="Desc"/>	
Photo       <input type="file" name="Photo"/>	
TopicID      <input type="text" name="TopicID"/>
<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint">
<span class="str">
{
    "results": {
        "TopicID": "33",
        "UserID": "34110",
        "TopicName": "Dancing3334",
        "Desc": "desc",
        "Photo": "http://anomo-sandbox.s3.amazonaws.com/Stream/c181f46c534d3b79fd0b0cf4a749d8c6.png",
        "Photo100": "http://anomo-sandbox.s3.amazonaws.com/Stream/c181f46c534d3b79fd0b0cf4a749d8c6_thumb_100x100.png",
        "CateID": "1",
        "IsDelete": "0",
        "TotalPost": "0",
        "CreatedDate": "2014-12-30 07:23:51",
        "LastModified": "2014-12-30 07:23:51"
    },
    "code": "OK"
}
</span>
</pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#UpdateUserStatusCodes">Status Codes</a> below.</li>

</ul>

<h3 id="UpdateUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; A new topic was created successfully.</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_PARAMETER</code> - indicates that parameters are missing.</li>    
  <li><code>TOPIC_EXISTED</code> indicates that topic name is existed.</li>
  <li><code>INVALID_REQUEST</code> indicates that topic is not exist.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
<!-- End Add new topic -->


<h2 id="DeleteTopic">Delete topic</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/delete_topic');?>/#Token/#TopicID</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>TopicID</code> (<em>required</em>) - is id of topic</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/delete_topic');?>/IWPMXYTFVCVXVWYAPRB9/1</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that missing parameters</li>
  <li><code>FAIL</code> - indicates that some errors occurred.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="CloseTopic">Close topic</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/close_topic');?>/#Token/#TopicID</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>TopicID</code> (<em>required</em>) - is id of topic</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/close_topic');?>/IWPMXYTFVCVXVWYAPRB9/1</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that missing parameters</li>
  <li><code>FAIL</code> - indicates that some errors occurred.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>





<h1 id="Sticker" style="margin:1em 0 0 -10px;">Sticker</h1>
<ol class="toc">
	<li><a href="#GetListStickerCategory">Get list category</a></li>
        <li><a href="#GetListSticker">Get list sticker</a></li>
        <li><a href="#StickerCheckVerison">Check version</a></li>
</ol>


<h2 id="GetListStickerCategory">Get list category</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/sticker/get_list_category');?>/#Token</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/sticker/get_list_category');?>/IWPMXYTFVCVXVWYAPRB9</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    results: [
        {
            CateID: "1",
            Name: "CupCake 1"
        },
        {
            CateID: "2",
            Name: "CupCake 2"
        },
        {
            CateID: "3",
            Name: "RedPanda 1"
        },
        {
            CateID: "4",
            Name: "RedPanda 2"
        }
    ],
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<!-- Get list topic -->
<h2 id="GetListSticker">Get list sticker</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/sticker/get_list_sticker');?>/#Token/#CateID</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>CateID</code> (<em>required</em>) - Category ID</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/sticker/get_list_sticker');?>/IWPMXYTFVCVXVWYAPRB9/1</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    results: [
        {
            ID: "1",
            Photo: "http://anomo-sandbox.s3.amazonaws.com/Sticker/1.png"
        },
        {
            ID: "2",
            Photo: "http://anomo-sandbox.s3.amazonaws.com/Sticker/2.png"
        },
        {
            ID: "3",
            Photo: "http://anomo-sandbox.s3.amazonaws.com/Sticker/3.png"
        },
        {
            ID: "4",
            Photo: "http://anomo-sandbox.s3.amazonaws.com/Sticker/4.png"
        }
    ],
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<h2 id="StickerCheckVerison">Check version</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/sticker/check_version');?>/#Token</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Sticker</code> (<em>required</em>) text - format as below</li>
</ul>

<pre class="prettyprint"><span class="str">

{
    "Sticker":[
        {"CateID":"1","Version":"1"},
        {"CateID":"2","Version":"1"},
        {"CateID":"3","Version":"1"},
        {"CateID":"4","Version":"1"}
    ]
}
</span></pre>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/sticker/check_version/IWPMXYTFVCVXVWYAPRB9')?>">
	Sticker		<input type="text" name="Sticker"/>
	<input type="submit" value="Submit" class="button"/>
</form></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "results": [
        {
            "ID": "13",
            "CateID": "2",
            "Photo": "http://anomo-sandbox.s3.amazonaws.com/Sticker/sticker_cupcake_1.png",
            "Name": "CupCake",
            "Version": "2"
        },
        {
            "ID": "14",
            "CateID": "2",
            "Photo": "http://anomo-sandbox.s3.amazonaws.com/Sticker/sticker_cupcake_10.png",
            "Name": "CupCake",
            "Version": "2"
        },
        {
            "ID": "15",
            "CateID": "2",
            "Photo": "http://anomo-sandbox.s3.amazonaws.com/Sticker/sticker_cupcake_11.png",
            "Name": "CupCake",
            "Version": "2"
        },
        {
            "ID": "16",
            "CateID": "2",
            "Photo": "http://anomo-sandbox.s3.amazonaws.com/Sticker/sticker_cupcake_12.png",
            "Name": "CupCake",
            "Version": "2"
        }
        ]
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that missing parameters.</li>
  <li><code>FAIL</code> - indicates that some errors occurred.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="BanUserOnTopic">Ban / Unban user on topic</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/ban_user');?>/#Token/#UserID/#TopicID</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>UserID</code> (<em>required</em>) - is id of user who will be banned</li>
  <li><code>TopicID</code> (<em>required</em>) - is id of topic</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/stream/ban_user');?>/IWPMXYTFVCVXVWYAPRB9/1/1</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that missing parameters.</li>
  <li><code>FAIL</code> - indicates that some errors occurred.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>



