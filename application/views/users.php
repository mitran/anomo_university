 <h1 id="Users" style="margin:1em 0 0 -10px;">Users</h1>
<ol class="toc">
	<li><a href="#Register">Register </a></li> 
	<li><a href="#RegisterDevice">Register Device</a></li>  
	<li><a href="#UpdateUser">Update User Information</a></li> 
        <li><a href="#UpdateUserEmail">Update User email</a></li>     
	<li><a href="#UpdatePrivacy">Update privacy</a></li>       
	<li><a href="#UpdateLocation">Update User Location</a></li>
	<li><a href="#LoginWithUserNamePwd">Login with UserName & Password </a></li>
	<li><a href="#LoginWithFB">Login with Facebook </a></li>
        <li><a href="#LoginTrialAccount">Login for trial account </a></li>
	<li><a href="#Logout">Logout</a></li> 
        <li><a href="#CheckUserNameOrEmailIsExisted">Check username or email is existed</a></li>
        <li><a href="#GetListInterests">List Interests</a></li>
        <li><a href="#ListIntent">List Intent</a></li>
        <li><a href="#ListMajor">List Major</a></li>
        <li><a href="#ListAvatar">List Avatar</a></li>
	<li><a href="#ForgotPassword">Forgot password</a></li>
        <li><a href="#GetUserDetail">Get A User Detail</a></li>
        <li><a href="#ListUserPost">List user post</a></li>
        <li><a href="#GetAllPostOfUser">List user post (current and archived)</a></li>
        <li><a href="#ListGiftReceived">List gift received</a></li>
        <li><a href="#GetNotificationHistory">Get list notification history of user</a></li>
        <li><a href="#DecreaseBadgeNumber">Decrease #Badge and mark a history as read</a></li>
        
        <li><a href="#TrendingHashtag">Get list trending hashtag</a></li>
        
        <li><a href="#BlockUser">Block / Unblock user</a></li>
        <li><a href="#FollowUser">Follow / Unfollow user</a></li>
        
        <li><a href="#FlagUser">Flag User</a></li>
	<li><a href="#FlagContent">Report Content / Comment</a></li>
        
        <li><a href="#ListFollower">Get list follower</a></li>
	<li><a href="#ListFollowing">Get list following</a></li>
        <li><a href="#DeleteActivity">Delete activity</a></li>
        <li><a href="#BlockAnonymous">Block Anonymous</a></li>
	<li><a href="#PicturePostActivity">Picture post activity</a></li>
        <li><a href="#SolrSearch">[Mention/Link usernames with @ symbol ]Search User</a></li>
        <li><a href="#CheckAppVersion">Check App Version</a></li>
        <li><a href="#SearchUserV2"> Search user</a></li>

	<li><a href="#GetBlockUser">Get blocked users</a></li>

	<li><a href="#XMPPUploadImage">XMPP upload image</a></li>
        
        <li><a href="#RequestRevealV3">Request reveal</a></li>
        <li><a href="#ResponseRevealV2">Response reveal</a></li>
        <li><a href="#UpdateRevealV4">Update reveal info</a></li>
        <li><a href="#GetRevealInfoV3">Get reveal info</a></li>
        
        <li><a href="#UpdateSecretPictureV2">Update secret picture</a></li>
        <li><a href="#GetListSecretPicture">Get list secret picture</a></li>
        <li><a href="#DeleteSecretPicture">Delete secret picture</a></li>
        
        <li><a href="#SearchActivityUser">Search User [in Activity > Search screen]</a></li>
        <li><a href="#SearchActivityHashTag">Search HashTag [in Activity > Search screen]</a></li>
        <li><a href="#SearchActivityTag">Search Tag [in Activity > Search screen]</a></li>
        <li><a href="#SearchActivityTopic">Search Topic [in Activity > Search screen]</a></li>
        
        
       <li><a href="#InfluencePointChart">[Influence points] chart</a></li>
       <li><a href="#InfluencePointTotal">[Influence points] total points</a></li>
       
       <li><a href="#InviteSMS">Invite friend via SMS</a></li>
       <li><a href="#ShareContentFB">Give 10 points each time someone shares content on their FB Wall</a></li>
       
       <li><a href="#SendVerificationCode">Send Verification Code</a></li>
       <li><a href="#VerifyCode">Verify Code</a></li>
       <li><a href="#CheckVerificationStatus">Check Verification Status</a></li>
       <li><a href="#CheckMatchUser">Check Match User</a></li> 
       <li><a href="#GetMatchUser">Get Match User</a></li>
       <li><a href="#GetMatchInfo">Get History Match Info</a></li> 
       <li><a href="#GetMatchQuestion">Get History Match Question</a></li>
       
       <li><a href="#GetUserTag">Get User Tag</a></li>
       <li><a href="#GetSetting">Get Setting</a></li>
       <li><a href="#ValidatePromoCode">Validate promo code</a></li>
       <li><a href="#SendEmailCode">Send Email Code</a></li>
       <li><a href="#ValidateEmailCode">[Setting] Validate Email Code</a></li>
       
       <li><a href="#VerifyEmailCode">Verify Email Code</a></li>
       <li><a href="#CheckEmailVerificationStatus">Check Email Verification Status</a></li>
        
</ol>

<h2 id="Register">Register</h2>
<p >This is a web service allow you to register new account. The parameters are required as below. It returns a token, which is then used for future calls.</p>
<p class="note">Status: Active</p>



<p>Register request is an HTTP URL with POST method of the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/register');?></span></em></pre>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Email</code> (<em>required</em>) - client will check valid email before posting</li>
  <li><code>Password</code> (<em>required</em>) - </li>
  <li><code>UserName</code> (<em>required</em>) - </li>
  <li><code>BirthDate</code> (<em>required</em>) - Format: Y-m-d </li>
  <li><code>Gender</code> (<em>required</em>) - Integer (1:Man, 2:Woman)</li>
  <li><code>FacebookID</code> (<em>optional</em>) - </li>
  <li><code>FbEmail</code> (<em>optional</em>) - </li>
  <li><code>Photo</code> (<em>optional</em>) - avatar file name</li>
  <li><code>FullPhoto</code> (<em>optional</em>) - full photo file name</li>
  <li><code>Tags</code> (<em>optional</em>) - list tagname separated with a comma symbol (Tag1,Tag2,Tag3,...)</li>
   <li><code>IntentIDs</code> (<em>optional</em>) - list intentId separated with a comma symbol (IntentID1,IntentID2,IntentID3,...)</li>
   <li><code>MajorIDs</code> (<em>optional</em>) - list majorId separated with a comma symbol (MajorID1,MajorID2,MajorID3,...)</li>
   <li><code>GenderDating</code> (<em>required</em>) - Integer (1:Man, 2:Woman, 3: Both)</li>  
   <li><code>PromoCode</code> (<em>optional</em>) - ... </li>  
   <li><code>EmailCode</code> (<em>optional</em>) - ...</li>  
</ul>

<p>An example request is below:</p>

<pre class="prettyprint">

<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/register')?>">
	Email		<input type="text" name="Email"/>
	Password	<input type="text" name="Password"/>
	UserName	<input type="text" name="UserName"/>
	BirthDate	<input type="text" name="BirthDate"/>
	Gender		<input type="text" name="Gender"/>
	FacebookID	<input type="text" name="FacebookID"/>
	FbEmail		<input type="text" name="FbEmail"/>
	Photo		<input type="text" name="Photo"/>
	FullPhoto	<input type="text" name="FullPhoto"/>
	Tags		<input type="text" name="Tags"/>
        IntentIDs	<input type="text" name="IntentIDs"/>
        MajorIDs	<input type="text" name="MajorIDs"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">

{
    "UserID": "34122",
    "Email": "mi@yahoo.com",
    "UserName": "Mi133",
    "Gender": "1",
    "BirthDate": "2000-09-09",
    "FbEmail": "",
    "FacebookID": "",
    "Photo": "http://anomo-sandbox.s3.amazonaws.com/Upload/d55c40137cd827abf2f3f7d90977f060.jpg",
    "FullPhoto": "http://anomo-sandbox.s3.amazonaws.com/Upload/d55c40137cd827abf2f3f7d90977f060.jpg",
    "Tags": [
        {
            "TagID": "1119",
            "Name": "film"
        },
        {
            "TagID": "448",
            "Name": "love"
        },
        {
            "TagID": "13",
            "Name": "music"
        }
    ],
    Intents: [
        {
            IntentID: "4",
            Name: "Humor"
        },
        {
            IntentID: "2",
            Name: "Love"
        },
        {
            IntentID: "1",
            Name: "Meeting Friends"
        }
    ],
    "token": "NDF8UPAWX65DCBURMIVW",
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"UserID"</code> return User ID when register successfully.</li>
  <li><code>"token"</code> return token when register successfully.</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#RegisterStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="RegisterStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; A new user was created successfully.</li>
  <li><code>EMAIL_EXISTED</code> - indicates that already email existed.</li>
  <li><code>USERNAME_EXISTED</code> - indicates that already username existed.</li>
  <li><code>DB_ERROR</code> - indicates that database error.</li>
  <li><code>INVALID_REQUEST</code> request without post method, or parameters are missing.</li>

</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="RegisterDevice">Register Device</h2>
<p>Our server will store register_id (android/GCM) / device token (ios) for pushing notification service. It is called after a user login</p>

<p class="note">Status: Active</p>

<p>Register request is an HTTP URL with POST method of the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/register_device');?>/#Token</span></em></pre>
<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>DeviceID</code> (<em>required</em>) - deviceID</li>
  <li><code>Platform</code> (<em>required</em>) - string (ios/android)</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint">

<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/register_device')?>/IWPMXYTFVCVXVWYAPRB9">
	DeviceID	<input type="text" name="DeviceID"/>
	Platform	<input type="text" name="Platform"/>
				<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#RegisterDeviceStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="RegisterDeviceStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; A new user was created successfully.</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> request without post method, or parameters are missing.</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->

<h2 id="UpdateUser">Update User Information</h2>
<p>Update information of user. It is called in edit profile screen</p>
<p class="note">Status: Active</p>

<p>Update User request is an HTTP URL with POST method of the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/update');?>/#token</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>UserName</code> (<em>optional</em>) - </li>
  <li><code>BirthDate</code> (<em>optional</em>) - </li>
  <li><code>Password</code> (<em>optional</em>) - </li>
  <li><code>Gender</code> (<em>optional</em>) - Integer (1:Man, 2:Woman)</li>
  <li><code>FacebookID</code> (<em>optional</em>) - </li>
  <li><code>FbEmail</code> (<em>optional</em>) - </li>
  <li><code>ProfileStatus</code> (<em>optional</em>) - </li>
  <li><code>Photo</code> (<em>optional</em>) - avatar file name </li>
  <li><code>FullPhoto</code> (<em>optional</em>) - full avatar file name</li>
  <li><code>CoverPicture</code> (<em>optional</em>) - </li>
  <li><code>AboutMe</code> (<em>optional</em>) - </li>
  <li><code>RealName</code> (<em>optional</em>) - </li>
  <li><code>College</code> (<em>optional</em>) - </li>
  <li><code>Tags</code> (<em>optional</em>) - list tag name separated with a comma symbol (Tag1,Tag2,Tag3,...)</li>
  <li><code>IntentIDs</code> (<em>optional</em>) - list intentId separated with a comma symbol (IntentID1,IntentID2,IntentID3,...)</li>
  <li><code>MajorIDs</code> (<em>optional</em>) - list majorId separated with a comma symbol (MajorID1,MajorID2,MajorID3,...)</li>
  <li><code>GenderDating</code> (<em>required</em>) - Integer (1:Man, 2:Woman, 3: Both)</li>
  <li><code>FanPage</code> (<em>optional</em>) - Integer (UserId)</li>
  <li><code>TopicID</code> (<em>optional</em>) - Integer</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<ul>
<li>Request Header :</li>
	+ Name: Content-Type
	+ Value: application/x-www-form-urlencoded
<li>Request Body :</li>
	UserName=MiTran&BirthDate=1980-7-7&Gender=2&Tag[]=talk&Tag[]=film&Tag[]=music&Tag[]=song 
</ul>
</pre>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/update/IWPMXYTFVCVXVWYAPRB9')?>">
	UserName	<input type="text" name="UserName"/>
	BirthDate	<input type="text" name="BirthDate"/>
	Gender		<input type="text" name="Gender"/>
	FacebookID	<input type="text" name="FacebookID"/>
	FbEmail		<input type="text" name="FbEmail"/>
	RealName	<input type="text" name="RealName"/>
	College		<input type="text" name="College"/>	
	AboutMe		<input type="text" name="AboutMe"/>
	ProfileStatus	<input type="text" name="ProfileStatus"/>
	Photo		<input type="text" name="Photo"/>
	FullPhoto	<input type="text" name="FullPhoto"/>
	CoverPicture	<input type="file" name="CoverPicture"/>
	Tags		<input type="text" name="Tags"/>
    IntentIDs		<input type="text" name="IntentIDs"/>
    FanPage		<input type="text" name="FanPage"/>
    TopicID		<input type="text" name="TopicID"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"UserID":"1","UserName":"Mi Tran","Email":"son@vinasource.com","Credits":null,"Gender":"2","BirthDate":"1980-09-09","Avatar":"http:\/\/localhost:81\/anomo\/PHP Webservice\/public\/upload\/9096cde490cc9c7ddfba6a9c9de609b8.jpg","FbEmail":"","Phone":"","ProfileStatus":"","AllowReceiveMessage":null,"AllowDisplayAge":null,"AllowAddFavoriteNotice":null,"AllowSendGiftNotice":null,"AllowRevealNotice":null,"AllowChatNotice":null,"Tags":[],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#UpdateUserStatusCodes">Status Codes</a> below.</li>

</ul>

<h3 id="UpdateUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; A new user was created successfully.</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>USERNAME_EXISTED</code> - indicates that already username existed.</li>
  <li><code>DB_ERROR</code> - indicates that database error.</li>
  <li><code>NOT_ALLOW_CHANGE_USER_NAME</code> - indicates that this user doesn't allow to change their user name.</li>
  <li><code>INVALID_REQUEST</code> request without post method, or parameters are missing.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->

<h2 id="UpdatePrivacy">Update privacy</h2>
<p>Turn ON / OFF setting value </p>
<p class="note">Status: Active</p>

<p>Update User request is an HTTP URL with POST method of the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/update_privacy');?>/#token</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>AllowSendGiftNotice</code> (<em>required</em>) - 1: yes, 0: no</li>
  <li><code>AllowRevealNotice</code> (<em>required</em>) - 1: yes, 0: no</li>
  <li><code>AllowChatNotice</code> (<em>required</em>) - 1: yes, 0: no</li>
  <li><code>AllowAnomotionNotice</code> (<em>required</em>) - 1: yes, 0: no</li>
  <li><code>AllowCommentActivityNotice</code> (<em>required</em>) - 1: yes, 0: no</li>
  <li><code>AllowLikeActivityNotice</code> (<em>required</em>) - 1: yes, 0: no</li>
  <li><code>AllowFollowNotice</code> (<em>required</em>) - 1: yes, 0: no</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/update_privacy/IWPMXYTFVCVXVWYAPRB9/1')?>">
	AllowSendGiftNotice		<input type="text" name="AllowSendGiftNotice"/>
	AllowRevealNotice		<input type="text" name="AllowRevealNotice"/>
	AllowChatNotice			<input type="text" name="AllowChatNotice"/>
	AllowAnomotionNotice 	<input type="text" name="AllowAnomotionNotice"/>	
	AllowCommentActivityNotice 	<input type="text" name="AllowCommentActivityNotice"/>
	AllowLikeActivityNotice 	<input type="text" name="AllowLikeActivityNotice"/>
	AllowFollowNotice 	<input type="text" name="AllowFollowNotice"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#UpdatePrivacyStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="UpdatePrivacyStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; A new user was created successfully.</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> request without post method, or parameters are missing.</li>
  <li><code>FAIL</code>  indicates that db error.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="UpdateLocation">Update User Location</h2>
<p>Every time you change location. We should update them. </p>
<p class="note">Status: Active</p>

<p>Update User request is an HTTP URL with POST method of the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/update_user_location');?>/#token/#Lat/#Lon</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>Lat</code> (<em>required</em>) - ...</li>
  <li><code>Lon</code> (<em>required</em>) - ...</li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Country</code> (<em>required</em>) - ...</li>
  <li><code>City</code> (<em>required</em>) - ...</li>
  <li><code>State</code> (<em>required</em>) - ...</li>
</ul>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{   code:"OK",
    NeighborhoodID: "2011",
    NeighborhoodName: "San Francisco"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#UpdatePrivacyStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="UpdatePrivacyStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; A new user was created successfully.</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> request without post method, or parameters are missing.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<h2 id="LoginWithUserNamePwd">Login with UserName & Password</h2>
<p>Login with UserName / Password. The password should be hashed md5 before sending to server.  It returns a token, which is then used for future calls.</p>
<p class="note">Status: Active</p>
<p>URL with following format:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/login');?></span></em></pre>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>UserName</code> (<em>required</em>) - ...</li>
  <li><code>Password</code> (<em>optional</em>) - ...</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/login')?>">
	UserName	<input type="text" name="UserName"/>
	Password	<input type="text" name="Password"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "UserID": "34175",
    "UserName": "duc2",
    "Gender": "1",
    "BirthDate": "1988-02-02",
    "Email": "mylove_2288@yahoo.com",
    "FacebookID": "100001596995261",
    "FbEmail": "mylove_2288@yahoo.com",
    "ProfileStatus": "",
    "AboutMe": "",
    "Avatar": "http://anomo-sandbox.s3.amazonaws.com/Upload/crop_cau_male_1.png",
    "FullPhoto": "",
    "CoverPicture": "",
    "AllowSendGiftNotice": "1",
    "AllowRevealNotice": "1",
    "AllowChatNotice": "1",
    "AllowAnomotionNotice": "1",
    "AllowCommentActivityNotice": "1",
    "AllowLikeActivityNotice": "1",
    "AllowFollowNotice": "1",
    "GenderDating":"1",
    "Tags": [
        {
            "TagID": "404",
            "Name": "06110 SPKT"
        },
        {
            "TagID": "406",
            "Name": "12A1"
        }
    ],
    "Intents": [
        {
            "IntentID": "3",
            "Name": "Flirting & Fun"
        }
    ],
    "token": "W6BNPDK97WCHF1Q2FDLM",
    "code": "OK"
}

</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"UserID"</code> ... </li>
  <li><code>"Email"</code> ...</li>
  <li><code>"UserName"</code> ... </li>
  <li><code>...</code> ... </li>
  <li><code>"token"</code> ..</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#LoginStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="#LoginStatusCodes">Status Codes</h3>
<p>The <code>code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; login successfully.</li>
  <li><code>FAIL</code> - user name & password does not match .</li>
  <li><code>INVALID_REQUEST</code> - this userID is not exist on server.</li>
  <li><code>BANNED</code> - this user is banned.</li>
  <li><code>DEACTIVE</code> - this user is deactive.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<h2 id="LoginWithFB">Login with Facebook</h2>
<p>Login with Facebook. It returns a token, which is then used for future calls.</p>
<p class="note">Status: Active</p>
<p>URL with following format:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/login_with_fb');?></span></em></pre>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>FacebookID</code> (<em>required</em>) - ...</li>
  <li><code>FbAccessToken</code> (<em>required</em>) - ...</li>
  <li><code>Email</code> (<em>optional</em>) - ...</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/login_with_fb')?>">
	FacebookID	<input type="text" name="FacebookID"/>
	FbAccessToken	<input type="text" name="FbAccessToken"/>
        Email	<input type="text" name="FbAccessToken"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">

{
    "UserID": "34175",
    "UserName": "duc2",
    "Gender": "1",
    "BirthDate": "1988-02-02",
    "IsPassword": "1",
    "Email": "mylove_2288@yahoo.com",
    "FacebookID": "100001596995261",
    "FbEmail": "mylove_2288@yahoo.com",
    "ProfileStatus": "",
    "AboutMe": "",
    "Avatar": "http://anomo-sandbox.s3.amazonaws.com/Upload/crop_cau_male_1.png",
    "FullPhoto": "",
    "CoverPicture": "",
    "AllowSendGiftNotice": "1",
    "AllowRevealNotice": "1",
    "AllowChatNotice": "1",
    "AllowAnomotionNotice": "1",
    "AllowCommentActivityNotice": "1",
    "AllowLikeActivityNotice": "1",
    "AllowFollowNotice": "1",
    "GenderDating":"1",
    "Tags": [
        {
            "TagID": "404",
            "Name": "06110 SPKT"
        },
        {
            "TagID": "406",
            "Name": "12A1"
        }
    ],
    "Intents": [
        {
            "IntentID": "3",
            "Name": "Flirting & Fun"
        }
    ],
    "token": "W6BNPDK97WCHF1Q2FDLM",
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#LoginStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="#LoginStatusCodes">Status Codes</h3>
<p>The <code>code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; login successfully.</li>
  <li><code>NOT_REGISTER</code> - this fbID is not exist on server.</li>
  <li><code>BANNED</code> - this user is banned.</li>
  <li><code>DEACTIVE</code> - this user is deactive.</li>
  <li><code>FAIL</code> - the FBid and access token are invalid </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<h2 id="LoginTrialAccount">Login for trial acccount</h2>
<p>Login for trial account.  It returns a token, which is then used for future calls.</p>
<p class="note">Status: Active</p>
<p>URL with following format:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/login_trial_account');?>/#UserID</span></em></pre>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint">
<?php echo site_url('webservice/user/login_trial_account');?>/2
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">

{
    "UserID": "34175",
    "UserName": "duc2",
    "Gender": "1",
    "BirthDate": "1988-02-02",
    "IsPassword": "1",
    "Email": "mylove_2288@yahoo.com",
    "FacebookID": "100001596995261",
    "FbEmail": "mylove_2288@yahoo.com",
    "ProfileStatus": "",
    "AboutMe": "",
    "Avatar": "http://anomo-sandbox.s3.amazonaws.com/Upload/crop_cau_male_1.png",
    "FullPhoto": "",
    "CoverPicture": "",
    "AllowSendGiftNotice": "1",
    "AllowRevealNotice": "1",
    "AllowChatNotice": "1",
    "AllowAnomotionNotice": "1",
    "AllowCommentActivityNotice": "1",
    "AllowLikeActivityNotice": "1",
    "AllowFollowNotice": "1",
    "GenderDating":"1",
    "Tags": [
        {
            "TagID": "404",
            "Name": "06110 SPKT"
        },
        {
            "TagID": "406",
            "Name": "12A1"
        }
    ],
    "Intents": [
        {
            "IntentID": "3",
            "Name": "Flirting & Fun"
        }
    ],
    "token": "W6BNPDK97WCHF1Q2FDLM",
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#LoginStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="#LoginStatusCodes">Status Codes</h3>
<p>The <code>code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; login successfully.</li>
  <li><code>NOT_REGISTER</code> - this fbID is not exist on server.</li>
  <li><code>BANNED</code> - this user is banned.</li>
  <li><code>DEACTIVE</code> - this user is deactive.</li>
  <li><code>FAIL</code> - indicates that some error occurred. </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="Logout">Logout</h2>
<p>Logout</p>
<p class="note">Status: Active</p>
<p>URL with following format:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/logout');?>/#token</span></em></pre>

<p>The list of parameters:</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><?php echo site_url('webservice/user/logout/IWPMXYTFVCVXVWYAPRB9');?></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<h3 id="#LogoutStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - request with wrong url or parameters are missing or user is not existed.</li>
  <li><code>OK</code> - indicates that no errors occurred; logout successfully.</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="CheckUserNameOrEmailIsExisted">Check username or email is existed</h2>
<p>Check a UserName or Email is existed in our database. It's called by client site on register process</p>
<p class="note">Status: Active</p>
<p>URL with following format:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/check_username_or_email_is_existed');?></span></em></pre>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>UserName</code> (<em>required</em>) - ...</li>
  <li><code>Email</code> (<em>required</em>) - ...</li>
</ul>

<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/check_username_or_email_is_existed')?>">
	UserName	<input type="text" name="UserName"/>
    Email	<input type="text" name="Email"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<h3 id="#ChangeEmailStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>EMAIL_EXISTED</code> - indicates that already email existed..</li>
  <li><code>USERNAME_EXISTED</code> - indicates that already username existed..</li>
  <li><code>INVALID_REQUEST</code> - indicates that missing parameter .</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="ForgotPassword">Forgot password</h2>
<p>Forgot password function. It will send an email to them with a link to reset password</p>
<p class="note">Status: Active</p>
<p>URL with following format:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/forgot_password');?></span></em></pre>

<p>The list of parameters on POST method and their possible values are enumerated below</p>
<ul>
  <li><code>Email</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/forgot_password')?>">
	Email	<input type="text" name="Email"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<h3 id="#ForgottenPasswordStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; send email to user successfully.</li>
  <li><code>FAIL</code> - indicates that error when sending email to user.</li>
  <li><code>INVALID_EMAIL</code> - indicates that email is invalid or email is not registed.</li>
  <li><code>INVALID_REQUEST</code> - indicates that missing parameters.</li>
  <li><code>FB_ACCOUNT</code> - indicates that existing FB user (without a password.</li>
</ul>




<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetListInterests">List Intertests</h2>
<p>Return list interests. It's called by client site on register process</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/list_interests');?></span></em></pre>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/list_interests');?></span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
ListInterests: [
    {
        Category: "Academics",
        TagList: [
            {
            TagID: "10",
            Name: "travel"
            },
            {
            TagID: "11",
            Name: "shopping"
            }
        ]
    },
    {
        Category: "Activities",
        TagList: [
            {
            TagID: "14",
            Name: "School"
            },
            {
            TagID: "15",
            Name: "Vinasource"
            },
            {
            TagID: "16",
            Name: "Senior iOS Developer"
            }
        ]
    },
    {
        Category: "Brands",
        TagList: [
            {
            TagID: "25",
            Name: "MP3"
            },
            {
            TagID: "30",
            Name: "Friends"
            }
        ]
    },
    {
        Category: "Music",
        TagList: [
            {
            TagID: "36",
            Name: "TMS Friends"
            },
            {
            TagID: "37",
            Name: "reading"
            },
            {
            TagID: "46",
            Name: "Dog meat hoi"
            },
            {
            TagID: "47",
            Name: "A15 Hung Vuong"
            }
        ]
    }
    ],
    code: "OK"
}
</span></pre>

<li><code>"code"</code> contains metadata on the request. See <a href="#GetUserDetailStatusCodes">Status Codes</a> below.</li>
</ul>
<h3 id="GetUserDetailStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field within the result response object contains the status of the request, and may contain debugging information
  to help you track down why this request failed. The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="ListIntent">List Intent</h2>
<p>Return list intent. It's called by client site on register process</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/list_intent');?></span></em></pre>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/list_intent');?></span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    ListIntent: [
        {
            IntentID: "1",
            Name: "Meeting Friends"
        },
        {
            IntentID: "2",
            Name: "Love"
        },
        {
            IntentID: "3",
            Name: "Flirting & Fun"
        },
        {
            IntentID: "4",
            Name: "Humor"
        },
        {
            IntentID: "5",
            Name: "Something Else"
        }
    ],
    code: "OK"
}
</span></pre>

<li><code>"code"</code> contains metadata on the request. See <a href="#GetUserDetailStatusCodes">Status Codes</a> below.</li>
</ul>
<h3 id="GetUserDetailStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field within the result response object contains the status of the request, and may contain debugging information
  to help you track down why this request failed. The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="ListMajor">List Major</h2>
<p>Return list major. It's called by client site on register process</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/list_major');?></span></em></pre>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/list_major');?></span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "ListMajor": [
        {
            "Category": "Arts and Sciences",
            "MajorCategoryID": "1",
            "MajorList": [
                {
                    "MajorID": "1",
                    "Name": "American Ethnic Studies"
                },
                {
                    "MajorID": "2",
                    "Name": "American Indian Studies"
                },
                {
                    "MajorID": "3",
                    "Name": "Anthropology"
                },
                {
                    "MajorID": "4",
                    "Name": "Applied and Computational Mathmetical Sciences"
                },
                {
                    "MajorID": "5",
                    "Name": "Art History"
                },
                {
                    "MajorID": "6",
                    "Name": "Asian Studies"
                },
                {
                    "MajorID": "7",
                    "Name": "Astronomy"
                },
                {
                    "MajorID": "9",
                    "Name": "Bilogy"
                },
                {
                    "MajorID": "8",
                    "Name": "Biochemistry"
                }
            ]
        },
        {
            "Category": "Built Design",
            "MajorCategoryID": "2",
            "MajorList": [
                {
                    "MajorID": "10",
                    "Name": "Architectural Design"
                },
                {
                    "MajorID": "11",
                    "Name": "Architecture"
                },
                {
                    "MajorID": "12",
                    "Name": "Construction Management"
                }
            ]
        },
        {
            "Category": "Education",
            "MajorCategoryID": "3",
            "MajorList": [
                {
                    "MajorID": "13",
                    "Name": "Early Childhood and Family Studies"
                }
            ]
        },
        {
            "Category": "Engineering",
            "MajorCategoryID": "4",
            "MajorList": [
                {
                    "MajorID": "14",
                    "Name": "Aeronautics and Astronautics"
                }
            ]
        }
    ],
    "code": "OK"
}
</span></pre>

<li><code>"code"</code> contains metadata on the request. See <a href="#GetUserDetailStatusCodes">Status Codes</a> below.</li>
</ul>
<h3 id="GetUserDetailStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field within the result response object contains the status of the request, and may contain debugging information
  to help you track down why this request failed. The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="ListUserPost">List user post</h2>
<p>Return user's recent post. It's called by mobile app in user profile screen</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_user_post');?>/#token/#TargetUserID/#Page</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>

<ul>
    <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
    <li><code>TargetUserID</code> (<em>required</em>) - ...</li>
    <li><code>Page</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_user_post');?>/IWPMXYTFVCVXVWYAPRB9/2</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">

{
    Activities: [
        {
        ActivityID: "9130",
        FromUserID: "1017",
        Message: "123",
        Image: "",
        RefID: "1509",
        Type: "1",
        ActionType: "1",
        PlaceID: "",
        PlaceName: "",
        PlaceIcon: "",
        PlaceAddress: "",
        Lat: "10.7628817",
        Lon: "106.6669263",
        CreatedDate: "2014-01-08 07:05:21",
        IsInvalid: "0",
        LastUpdate: "2014-01-08 07:05:21",
        Gender: "1",
        BirthDate: "1984-09-27",
        OwnerID: null,
        OrginalDate: null,
        GiftReceiverID: "0",
        CheckinPlaceID: "",
        CheckinPlaceName: "",
        NeighborhoodID: "29590",
        Avatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/eb5520cf4dcdd1018964f6c0058bf1a4.jpg",
        IsBlock: 0,
        IsBlockTarget: 0,
        IsReported: "0",
        FacebookID: "100000684574014",
        FromUserName: "Hung Pham",
        OwnerName: "",
        GiftReceiverName: "",
        IsStopReceiveNotify: "0",
        CheckinStatus: "0",
        NeighborhoodName: "",
        IsLike: "0",
        IsComment: "0",
        Comment: "0",
        Like: "0"
        },
        {
        ...
        }
    ],
    Page: 1,
    TotalPage: 4,
    code: "OK"
}

</span></pre>

<li><code>"code"</code> contains metadata on the request. See <a href="#GetUserDetailStatusCodes">Status Codes</a> below.</li>
</ul>
<h3 id="GetUserDetailStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field within the result response object contains the status of the request, and may contain debugging information
  to help you track down why this request failed. The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> indicates that parameters are missing or user is not existed.</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetAllPostOfUser">List user post (current and archived)</h2>
<p>Return user's recent post. It's called by mobile app in user profile screen</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_all_user_post');?>/#token/#TargetUserID/#isFanPage/#ActivityID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>

<ul>
    <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
    <li><code>TargetUserID</code> (<em>required</em>) - ...</li>
    <li><code>isFanPage</code> (<em>required</em>) - Integer (1,0)</li>
    <li><code>ActivityID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_all_user_post');?>/IWPMXYTFVCVXVWYAPRB9/1/1</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">

{
    Activities: [
        {
        ActivityID: "9130",
        FromUserID: "1017",
        Message: "123",
        Image: "",
        RefID: "1509",
        Type: "1",
        ActionType: "1",
        PlaceID: "",
        PlaceName: "",
        PlaceIcon: "",
        PlaceAddress: "",
        Lat: "10.7628817",
        Lon: "106.6669263",
        CreatedDate: "2014-01-08 07:05:21",
        IsInvalid: "0",
        LastUpdate: "2014-01-08 07:05:21",
        Gender: "1",
        BirthDate: "1984-09-27",
        OwnerID: null,
        OrginalDate: null,
        GiftReceiverID: "0",
        CheckinPlaceID: "",
        CheckinPlaceName: "",
        NeighborhoodID: "29590",
        Avatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/eb5520cf4dcdd1018964f6c0058bf1a4.jpg",
        IsBlock: 0,
        IsBlockTarget: 0,
        IsReported: "0",
        FacebookID: "100000684574014",
        FromUserName: "Hung Pham",
        OwnerName: "",
        GiftReceiverName: "",
        IsStopReceiveNotify: "0",
        CheckinStatus: "0",
        NeighborhoodName: "",
        IsLike: "0",
        IsComment: "0",
        Comment: "0",
        Like: "0"
        },
        {
        ...
        }
    ],
    code: "OK"
}

</span></pre>

<li><code>"code"</code> contains metadata on the request. See <a href="#GetUserDetailStatusCodes">Status Codes</a> below.</li>
</ul>
<h3 id="GetUserDetailStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field within the result response object contains the status of the request, and may contain debugging information
  to help you track down why this request failed. The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> indicates that parameters are missing or user is not existed.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="ListGiftReceived">List gift received</h2>
<p>Return user's received gift. It's called by mobile app in user profile screen</p>
<p class="note">Status: Deprecated</p>

<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_gift_received');?>/#token/#TargetUserID/#Page</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>

<ul>
    <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
    <li><code>TargetUserID</code> (<em>required</em>) - ...</li>
    <li><code>Page</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_gift_received');?>/IWPMXYTFVCVXVWYAPRB9/2</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    ListGiftReceived: [
        {
            SendUserID: "1796",
            SendUserName: "android fb",
            BirthDate: "1995-10-16",
            Description: "Sometimes, words aren’t enough. In those cases, a thumbs up will usually suffice. They’ll know what you mean.",
            CreatedDate: "2013-06-17 10:37:25",
            Avatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/5778a7d14a984061f424018f1bce4791.jpg",
            Photo: "http://anomo-sandbox.s3.amazonaws.com/Upload/cb39ab54b0cf1ca6d6db847471712635.png",
            Photo194: "http://anomo-sandbox.s3.amazonaws.com/Upload/7ea6952ebe305232873a3a70b544ee52.png",
            Photo454: "http://anomo-sandbox.s3.amazonaws.com/Upload/75caddb693972f9262b146407973cee4.png",
            NeighborhoodID: "29696",
            NeighborhoodName: "Ho Chi Minh City, Vietnam"
        },
        {
            SendUserID: "1796",
            SendUserName: "android fb",
            BirthDate: "1995-10-16",
            Description: "Sometimes, words aren’t enough. In those cases, a thumbs up will usually suffice. They’ll know what you mean.",
            CreatedDate: "2013-06-17 10:43:07",
            Avatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/5778a7d14a984061f424018f1bce4791.jpg",
            Photo: "http://anomo-sandbox.s3.amazonaws.com/Upload/cb39ab54b0cf1ca6d6db847471712635.png",
            Photo194: "http://anomo-sandbox.s3.amazonaws.com/Upload/7ea6952ebe305232873a3a70b544ee52.png",
            Photo454: "http://anomo-sandbox.s3.amazonaws.com/Upload/75caddb693972f9262b146407973cee4.png",
            NeighborhoodID: "29696",
            NeighborhoodName: "Ho Chi Minh City, Vietnam"
        }
    ],
    Page: 1,
    TotalPage: 2,
    code: "OK"
}

</span></pre>

<li><code>"code"</code> contains metadata on the request. See <a href="#GetUserDetailStatusCodes">Status Codes</a> below.</li>
</ul>
<h3 id="GetUserDetailStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field within the result response object contains the status of the request, and may contain debugging information
  to help you track down why this request failed. The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> indicates that parameters are missing or user is not existed.</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="TrendingHashtag">List trending hashtag</h2>
<p>Return list trending hashtag</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/trending_hashtag');?>/#token</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>

<ul>
    <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/trending_hashtag');?>/IWPMXYTFVCVXVWYAPRB9</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    ListTrending: [
        {
        HashTag: "test"
        },
        {
        HashTag: "123"
        },
        {
        HashTag: "123456789"
        },
        {
        HashTag: "princess"
        },
        {
        HashTag: "anomorocks"
        },
        {
        HashTag: "hashtag"
        }
    ],
    code: "OK"
}

</span></pre>

<li><code>"code"</code> contains metadata on the request. See <a href="#GetUserDetailStatusCodes">Status Codes</a> below.</li>
</ul>
<h3 id="GetUserDetailStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field within the result response object contains the status of the request, and may contain debugging information
  to help you track down why this request failed. The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> indicates that parameters are missing or user is not existed.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="ListAvatar">List Avatar</h2>
<p>Return list avatar by gender. It's called by mobile app on register process or change avatar's user in edit profile screen</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/list_avatar');?>/#Gender</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>

<ul>
    <li><code>Gender</code> (<em>required</em>) - 1: Male / 2: Female</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/list_avatar');?>/1</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    ListAvatar: [
        {
            AvatarName: "88f84ebc537528f2bb7b645cb38c0abd.jpg",
            FullAvatarName: "7663006fcd7f783482511364ae040d5b.jpg",
            Avatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/88f84ebc537528f2bb7b645cb38c0abd.jpg",
            FullAvatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/7663006fcd7f783482511364ae040d5b.jpg"
        },
        {
            AvatarName: "a0e4a1324a031ac640ba048dfe0f34a0.png",
            FullAvatarName: "a0273fe14fbf9cf4e3a3218980396a68.png",
            Avatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/a0e4a1324a031ac640ba048dfe0f34a0.png",
            FullAvatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/a0273fe14fbf9cf4e3a3218980396a68.png"
        }
    ],
    code: "OK"
}

</span></pre>

<li><code>"code"</code> contains metadata on the request. See <a href="#GetUserDetailStatusCodes">Status Codes</a> below.</li>
</ul>
<h3 id="GetUserDetailStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field within the result response object contains the status of the request, and may contain debugging information
  to help you track down why this request failed. The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> indicates that parameters are missing.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetUserDetail">Get A User Detail</h2>
<p>Return information of user. It's called by mobile app in user profile screen</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_user_info');?>/#token/#TargetUserID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>

<ul>
    <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
    <li><code>TargetUserID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_user_info');?>/IWPMXYTFVCVXVWYAPRB9/2</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
results: {
    UserID: "78",
    UserName: "tester22",
    Gender: "1",
    BirthDate: "1986-01-22",
    Credits: "100",
    NumberOfFollower: "0",
    NumberOfFollowing: "0",
    Email: "tester22@vinasource.com",
    FacebookID: "99805819322",
    FbEmail: "http://www.facebook.com/tester22",
    ProfileStatus: "",
    AboutMe: "",
    Avatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/0a2b1d07d82f950e26e5eafe6f101584.jpg",
    FullPhoto: "http://anomo-sandbox.s3.amazonaws.com/Upload/0b8a4215dc4792851a8bce3f8a7f9847.jpg",
    CoverPicture: "",
    NeighborhoodID: "",
    NeighborhoodName: "",
    IsFavorite: "0",
    Tags: [ 
        {
            TagID: "89",
            Name: "Institute of Post telecommunication and Communication"
        }
    ],
    ListIntent: [ ]
    },
    code: "OK"
}

</span></pre>

<li><code>"code"</code> contains metadata on the request. See <a href="#GetUserDetailStatusCodes">Status Codes</a> below.</li>
</ul>
<h3 id="GetUserDetailStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field within the result response object contains the status of the request, and may contain debugging information
  to help you track down why this request failed. The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>BLOCK_TARGET</code> - in case user is blocked by target user (first priority)  </li>
  <li><code>BLOCK</code> - in case user block target user </li>
  <li><code>INVALID_REQUEST</code> indicates that parameters are missing or user is not existed.</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->

<h2 id="GetBlockUser">Get list blocked users</h2>
<p>Return list blocked users by other user</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_blocked_users');?>/#token</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_blocked_users');?>/IWPMXYTFVCVXVWYAPRB9</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    results: [
        {
            UserID: "1",
            UserName: "Test9090",
            Email: "test@yahoo.com1",
            ProfileStatus: "new status test #badge",
            Avatar: "",
            BirthDate: "1985-04-07",
            Gender: "1",
            FbEmail: "",
            Phone: "",
            RealName: "",
            RealPhoto: ""
        },
        {},
        {}
    ],
    code: "OK"
}


</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#BlockUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="BlockUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->

<h2 id="BlockUser">Block / Unblock user </h2>
<p>Block / UnBlock user. It calls when you want to block / unblock user</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/block_user');?>/#token/#TargetUserID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>TargetUserID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/block_user');?>/IWPMXYTFVCVXVWYAPRB9/2</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#BlockUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="BlockUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>FAIL</code> - indicates that param is invalid</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="FollowUser">Follow / UnFollow user</h2>
<p>Follow / Unfollow user</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/follow');?>/#token/#TargetUserID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>TargetUserID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/follow');?>/IWPMXYTFVCVXVWYAPRB9/2</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#AddFavoriteStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="AddFavoriteStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid or user is not existed; </li>
  <li><code>BLOCK_TARGET</code> - in case user is blocked by target user (first priority)  </li>
  <li><code>BLOCK</code> - in case user block target user </li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
</ul>



<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="CheckAppVersion">Check App Version</h2>
<p></p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com">https://domain/version/check_app_version/#AppVersion/#Platform/#token</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>AppVersion</code> (<em>required</em>) - ...</li>
  <li><code>Platform</code> (<em>required</em>) - (iOS/android)</li>
  <li><code>Token</code> (<em>optional</em>)</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com">https://domain/version/check_app_version/1.0/iOS/IWPMXYTFVCVXVWYAPRB9</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#CheckAppVersionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="CheckAppVersionStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; app_version == latest_version</li>
  <li><code>SUPPORT</code> indicates that app_version is old, system is still support this app_version</li>
  <li><code>UNSUPPORT</code> indicates that app_version is old, this app_version is unsupported by current system</li>
  <li><code>INVALID_REQUEST</code> - indicates that wrong params.</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetNotificationHistory">Get list notification history of user</h2>
<p>Return list notification history of user. It calls in left menu (only new notify) and show all in history screen</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/push_notification/get_notification_history');?>/#Token/#IsGetNew/#Page</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>IsGetNew</code> (<em>required</em>) - 0: get full list / 1: get only new notification history</li>
  <li><code>Page</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/push_notification/get_notification_history');?>/IWPMXYTFVCVXVWYAPRB9</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">

{
    NotificationHistory: [
        {
        ID: "18181",
        SendUserID: "34209",
        Type: "13",
        CreatedDate: "2014-04-11 13:27:43",
        Total: "1",
        UnView: "0",
        RefID: "2267",
        AnomotionID: "0",
        AnomotionStatus: "0",
        Score: "",
        RevealType: "",
        PictureOrder: "",
        IsFacebookGame: "0",
        ContentType: "1",
        UserName: "patricknlewis",
        Gender: "1",
        Avatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/crop_new_male_8.png",
        FacebookID: "",
        FacebookName: "",
        Content: "",
        Photo: "",
        GiftName: "",
        PostID: "",
        PostCreatedDate: "",
        PostContent: "",
        ContentID: "2147",
        PostOwnerID: "1",
        PostOwnerName: ""
        },
        {},
        ..
    ],
    NewNotificationsNumber: "6",
    Page: "1",
    TotalPage: 1,
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"NotificationHistory"</code> list notification history of user with 
  	<div>
  	<code>+ Type </code></br>
  		0: Message </br>
  		1: Gift </br>
  		2: Reveal </br>
  		3: Anomotion request</br>
  		4: Anomotion finished </br>
  		5: Anomotion accept
  		6: Not accepted reveal</br>
  		7: Unreveal</br>
  		8: Request reveal </br>
  		9:comment text post </br>
  		10:comment picture post</br>
  		11: comment photo neighborhood</br>
  		12: comment post neighborhood</br>
  		13: like feed</br>
  		14: comment feed</br>
                15: follow</br>
                16: mention user (@name)</br>
                17: like on comment</br>
  	<code>+ AnomotionStatus</code>: (0: Waiting - 1: Accepted - 2: Cancel)</br>
  	<code>+ ReavalType</code></br>
  		 - 0: RevealPicture</br>
		 - 1: RevealFacebook</br>
		 - 2: RevealName</br>
		 - 3: RevealSchool</br>
		 - 4: RevealLooks</br>
		 - 5: RevealParty</br>
		 - 6: RevealFlirty</br>
  	</div>
  	</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetCreditsOfUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetCreditsOfUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="DecreaseBadgeNumber">Decrease #badge and mark a history as read</h2>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/push_notification/read');?>/#Token/#HistoryID/#ContentID/#ContentType</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>HistoryID</code> (<em>required</em>) - the id of history, if dont have historyid please input -1</li>
  <li><code>ContentID</code> (<em>required</em>) - the id of content</li>
  <li><code>ContentType</code> (<em>required</em>) - type of content </li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/push_notification/read');?>/IWPMXYTFVCVXVWYAPRB9/1</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetCreditsOfUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetCreditsOfUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>FAIL</code> - indicates that db error.</li>
</ul>





<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->

<h2 id="SearchUserV2">Search user</h2>
<p>We have 2 cases: have Keyword or not <br />
- If have KEYWORD: The service responds with a list of Users matching the text string<br />
- If don't have KEYWORD: The service will only return a list of ONLINE users <br />
</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/search_user');?>/#token/#userID/#lat/#lon/#page/#Gender/#AgeFrom/#AgeTo</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>userID</code> (<em>required</em>) - ...</li>
  <li><code>lat</code> (<em>option</em>) - ...</li>
  <li><code>lon</code> (<em>option</em>) - ...</li>
  <li><code>page</code> (<em>option</em>) - default: 1</li>
  <li><code>Gender</code> (<em>optional</em>) - (0: both, 1: male, 2: female) </li>
  <li><code>AgeFrom</code> (<em>optional</em>) - </li>
  <li><code>AgeTo</code> (<em>optional</em>) - </li>
</ul>


<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Keyword</code> (<em>option</em>) - ...</li>
</ul>


<p>An example request is below:</p>

<pre class="prettyprint"><ul>
<li>Request Header :</li>
	+ Name: Content-Type
	+ Value: application/x-www-form-urlencoded
<li>Request Body :</li>
	Keyword=mitran 
</ul>
</pre>

<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/search_user');?>/IWPMXYTFVCVXVWYAPRB9/1/37.785834/-122.406417">
	Keyword		<input type="text" name="Keyword">
				<input type="submit" value="Submit" class="button">
</form>
</pre>


<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "results": {
        "ListUser": [
            {
                "UserID": "24",
                "UserName": "testing",
                "Lat": "10.7716753",
                "Lon": "106.6833671",
                "Avatar": "http://anomo-sandbox.s3.amazonaws.com/Upload/a209aede18bfe43aab9b095fa543b8b3.jpg",
                "ProfileStatus": "hello",
                "Email": "",
                "NeighborhoodID": "29575",
                "BirthDate": "1972-09-01",
                "Gender": "1",
                "FacebookID": "100000976436372",
                "IsOnline": "0",
                "PlaceID": "",
                "Status": "0",
                "IsFavorite": "0",
                "IsBlock": "0",
                "IsBlockTarget": "0",
                "PlaceName": "Vietnam",
                "NeighborhoodName": "",
                "Tags": [
                    {
                        "TagID": "104",
                        "Name": "Roosevelt High School"
                    },
                    {
                        "TagID": "105",
                        "Name": "University of Washington"
                    }
                ],
                "RecentPicture": [
                    {
                        "ID": "55",
                        "Photo": "http://anomo-sandbox.s3.amazonaws.com/Upload/fe034a6cf8ddea356e51354ba25041ad.jpg"
                    }
                ]
            },
            {
                "UserID": "25",
                "UserName": "jsnooptest",
                "Lat": "47.6750420",
                "Lon": "-122.1940440",
                "Avatar": "http://anomo-sandbox.s3.amazonaws.com/Upload/2eb88d095929295250ea9b29f13fb4e6.jpg",
                "ProfileStatus": "{\n  \"message\" : \"Hello testing\",\n  \"message_tags\" : [\n\n  ]\n}",
                "Email": "",
                "NeighborhoodID": "29662",
                "BirthDate": "1978-05-16",
                "Gender": "1",
                "FacebookID": "748229777",
                "IsOnline": "0",
                "PlaceID": "",
                "Status": "0",
                "IsFavorite": "0",
                "IsBlock": "0",
                "IsBlockTarget": "0",
                "PlaceName": "Kirkland, WA",
                "NeighborhoodName": "",
                "Tags": [
                    {
                        "TagID": "10",
                        "Name": "travel"
                    },
                    {
                        "TagID": "105",
                        "Name": "University of Washington"
                    },
                    {
                        "TagID": "123",
                        "Name": "Whistler Trip 2012"
                    },
                    {
                        "TagID": "127",
                        "Name": "You know when you are from Federal Way when.."
                    }
                ],
                "RecentPicture": [
                    {
                        "ID": "2920",
                        "Photo": "http://anomo-sandbox.s3.amazonaws.com/Upload/67c439a1fc4e1f35c1fe10607b9dbd6c.jpg"
                    },
                    {
                        "ID": "2919",
                        "Photo": "http://anomo-sandbox.s3.amazonaws.com/Upload/057e67d19d67bf818de2ba211dc193c5.jpg"
                    },
                    {
                        "ID": "1314",
                        "Photo": "http://anomo-sandbox.s3.amazonaws.com/Upload/226b920fc4bd1105e92dbe016bae39a2.jpg"
                    },
                    {
                        "ID": "1305",
                        "Photo": "http://anomo-sandbox.s3.amazonaws.com/Upload/909f6832753bd8e635a7a69a52eb6e4e.jpg"
                    }
                ]
            },
            {}
        ],
        "CurrentPage": 1,
        "TotalPage": 205
    },
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"CurrentPage"</code> - current page</li>
	<li><code>"TotalPage"</code> - total page</li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#SearchUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SearchUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>



<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="DeleteActivity">Delete activity</h2>
<p>Use to delete a post in profile screen</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/delete_activity');?>/#token/#ID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>ID</code> (<em>required</em>) - the id of activity</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/delete_activity');?>/IWPMXYTFVCVXVWYAPRB9/1</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetContactOfUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetContactOfUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid or user is not existed; </li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="BlockAnonymous">Block Anonymous</h2>
<p>Use to block all anoymous post of the same anonymous post owner</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/block_anonymous');?>/#token/#ID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>ID</code> (<em>required</em>) - the id of activity</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/block_anonymous');?>/IWPMXYTFVCVXVWYAPRB9/180187</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetContactOfUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetContactOfUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid, activity is not existed, block your own post, the post is not anonymous; </li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="PicturePostActivity">Picture post activity</h2>
<p>This is a web service allows you to post a picture with text to server.</p>
<p class="note">Status: Active</p>

<p>This request is an HTTP URL with POST method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/post_picture_activity');?>/#token</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>PictureCaption</code> <em></em> - text</li>
  <li><code>FanPage</code> <em>optional</em> - Integer (UserId)</li>
  <li><code>TopicID</code> <em>optional</em> - Integer</li>
  <li><code>Photo</code> <em></em> - </li>
  <li><code>Video</code> <em></em> - </li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/post_picture_activity/IWPMXYTFVCVXVWYAPRB9')?>">
PictureCaption  <input type="text" name="PictureCaption"/>
FanPage <input type="text" name="FanPage"/>
TopicID <input type="text" name="TopicID"/>
Photo   <input type="file" name="Photo">
Video   <input type="file" name="Video">
<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    Activities: {
        ID: 10589,
        PlaceID: "",
        Name: "",
        CreatedDate: "2014-03-26 10:11:25",
        Content: "testsfd",
        Type: 12,
        Photo: "http://anomo-sandbox.s3.amazonaws.com/Upload/2b5f77969b10922e36e0d4dfa93604f7.jpg",
        Photo100: "",
        Video: "http://uw-staging.s3.amazonaws.com/Video/9a076decb3c58552b81f8895e6d04082.mp4"
    },
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
 <li><code>"post"</code> contains information of the post. Only return when the status code is OK</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#SendMsgPlaceStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SendMsgPlaceStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>DB_ERROR</code> - indicates that database error.</li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param or place or user is not exist </li>
</ul>






<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="FlagUser">Report User</h2>
<p>This web service allows you to report a user. An email will be sent to report@anomo.com </p>
<p class="note">Status: Active</p>

<p>This request is an HTTP URL with POST method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/flag/add');?>/#token/#ToUserID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>ToUserID</code> (<em>required</em>) - ...</li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Content</code> (<em>required</em>) - text</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/flag/add/IWPMXYTFVCVXVWYAPRB9/2')?>">
	Content	<input type="text" name="Content"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>FAIL</code> - indicates that database error.</li>
  <li><code>FLAG_ADDED</code> - indicates that user is already reported by this user.</li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param or user is not existed </li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="FlagContent">Report Content / Comment</h2>
<p>This ws allows you to report a post or comment. An email will be sent to report@anomo.com </p>
<p class="note">Status: Active</p>

<p>This request is an HTTP URL with POST method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/flag/content');?>/#token/#ID/#type/#IsComment</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>ID</code> (<em>required</em>) - the id of the content / comment to be reported</li>
  <li><code>Type</code> (<em>required</em>) - type of content to be reported</li>
  <li><code>IsComment</code> (<em>required</em>) - 0: report content / 1: report comment</li>
</ul>
<pre class="prettyprint">

0: checkin
1: change profile status
2: post picture to place

7: post picture to neighborhood
27: post picture status

</pre>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Content</code> (<em>required</em>) - text</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/flag/content/IWPMXYTFVCVXVWYAPRB9/15/27')?>">
	Content	<input type="text" name="Content"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#PostCommentStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="PostCommentStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>FAIL</code> - indicates that database error.</li>
  <li><code>FLAG_ADDED</code> - indicates that content is already reported by this user.</li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param or user is not existed </li>
</ul>



<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="ListFollower">Get list follower</h2>
<p>Return list of users who followed you. It allows you to search by keyword</p>
<p class="note">Status: Active</p>
<p>URL with following format:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_list_follower');?>/#token/#targetUserID/#page</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>targetUserID</code> (<em>required</em>) -  </li>
  <li><code>page</code> (<em>required</em>) - default is 1 </li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Keyword</code> (<em>optional</em>) - ...</li>
</ul>

<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/get_list_follower')?>/IWPMXYTFVCVXVWYAPRB9/1">
	Keyword	<input type="text" name="Keyword"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_list_follower');?>/IWPMXYTFVCVXVWYAPRB9/1</span></em></pre>


<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"TotalPage":1,"CurrentPage":1,"NumberOfFollower":10,"ListFollower":[{"UserID":"121","UserName":"jamespirq","BirthDate":"1977-02-14","Gender":"0","NeighborhoodID":"2296","Avatar":"http:\/\/localhost\/wsanomo\/public\/upload\/d6fca8d0243c0081111c7c2b13bb42e9.jpg","IsFavorite":0,"PlaceID":"","PlaceName":"","CheckinStatus":0,"NeighborhoodName":"Industry, CA"}],"code":"OK"}</span></pre>

<h3 id="#ChangeEmailStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="ListFollowing">Get list following</h2>
<p class="note">Status: Active</p>
<p>URL with following format:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_list_following');?>/#token/#targetUserID/#page</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>targetUserID</code> (<em>required</em>) -  </li>
  <li><code>page</code> (<em>required</em>) - default is 1 </li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Keyword</code> (<em>optional</em>) - ...</li>
</ul>

<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/get_list_following')?>/IWPMXYTFVCVXVWYAPRB9/1">
	Keyword	<input type="text" name="Keyword"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>
<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_list_following');?>/IWPMXYTFVCVXVWYAPRB9/1</span></em></pre>


<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"TotalPage":1,"CurrentPage":1, "NumberOfFollowing": 10,"ListFollowing":[{"UserID":"121","UserName":"jamespirq","BirthDate":"1977-02-14","Gender":"0","NeighborhoodID":"2296","Avatar":"http:\/\/localhost\/wsanomo\/public\/upload\/d6fca8d0243c0081111c7c2b13bb42e9.jpg","IsFavorite":0,"PlaceID":"","PlaceName":"","CheckinStatus":0,"NeighborhoodName":"Industry, CA"}],"code":"OK"}</span></pre>

<h3 id="#ChangeEmailStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="XMPPUploadImage">XMPP Upload Image</h2>
<p>This is a web service lets you to upload an image to server. It returns a link to image</p>
<p class="note">Status: Active</p>
<p>URL with following format:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/upload_image');?>/#token</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Photo</code> (<em>required</em>) - ...</li>
</ul>

<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/upload_image')?>/IWPMXYTFVCVXVWYAPRB9">
	Photo	<input type="file" name="Photo"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{
code: "OK",
Photo: "http://localhost/wsanomo/public/upload/9088bfce2fed89d9003f4abd0ecfdd5d.jpg",
Photo100: "http://localhost/wsanomo/public/upload/9088bfce2fed89d9003f4abd0ecfdd5d_thumb_100x150.jpg",
Photo200: "http://localhost/wsanomo/public/upload/9088bfce2fed89d9003f4abd0ecfdd5d_thumb_200x300.jpg",
Photo300: "http://localhost/wsanomo/public/upload/9088bfce2fed89d9003f4abd0ecfdd5d_thumb_300x450.jpg"
}</span></pre>

<h3 id="#ChangeEmailStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - indicates that missing param.</li>
  <li><code>INVALID_IMAGE</code> - indicates that invalid image (too big, the file type not support).</li>
  <li><code>FAIL</code> - indicates that errors occurred</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->

<h2 id="SolrSearch">[Mention/Link usernames with @ symbol ]Search User</h2>
<p></p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/search/user');?>/#token/#page</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>page</code> (<em>option</em>) - default: 1</li>
</ul>


<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Keyword</code> (<em>required</em>) - ...</li>
</ul>


<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/search/user/IWPMXYTFVCVXVWYAPRB9');?>">
	Keyword		<input type="text" name="Keyword">
				<input type="submit" value="Submit" class="button">
</form>
</pre>


<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    code: "OK",
    ListUser: [
        {
        UserID: "2",
        UserName: "Quyen rom"
        },
        {
        UserID: "5",
        UserName: "Mom rom"
        }
    ],
    Page: "1",
    TotalPage: 1
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"ListUser"</code> - list user</li>
	<li><code>"CurrentPage"</code> - current page</li>
	<li><code>"TotalPage"</code> - total page</li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#SearchUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SearchUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
<!-- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->

<h2 id="RequestRevealV3">Request reveal</h2>
<p>Allow you to send a reveal request to other user</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/request_reveal');?>/#token/#targetUserID/#type/#pictureID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
    <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
    <li><code>targetUserID</code> (<em>required</em>) - ...</li>
    <li><code>type</code> (<em>required</em>) -
        
    </li>
    <pre class="prettyprint">
        + 0: SecretPicture
        + 1: FbEmail
        + 2: RealName
        + 3: Collage
        + 4: Looks
        + 5: Party
        + 6: Flirty
        + 7: Major
    </pre>
    <li><code>PictureID</code> (<em>option</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/request_reveal');?>/IWPMXYTFVCVXVWYAPRB9/2/1</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
    <li><code>"code"</code> contains metadata on the request. See <a href="#GetNewestMessageStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetNewestMessageStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
    <li><code>OK</code> - indicates that no errors occurred;</li>
    <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
    <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
    <li><code>REQUEST_SENT</code> - indicates that request is sent. </li>
    <li><code>REVEALED</code> - indicates that reveal request is accepted. </li>
    <li><code>BLOCK_TARGET</code> - in case user is blocked by target user (first priority)  </li>
    <li><code>BLOCK</code> - in case user block target user </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<!-- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->

<h2 id="ResponseRevealV2">Response reveal</h2>
<p>Allow you to accept a reveal request or reveal something to them</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/response_reveal');?>/#token/#targetUserID/#type/#isAccept/#pictureIDs</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>targetUserID</code> (<em>required</em>) - ...</li>
  <li><code>type</code> (<em>required</em>) - </li>
  <pre class="prettyprint">
        + 0: SecretPicture
        + 1: FbEmail
        + 2: RealName
        + 3: Collage
        + 4: Looks
        + 5: Party
        + 6: Flirty
        + 7: Major
    </pre>
  <li><code>isAccept</code> (<em>required</em>) - YES / NO</li>
  <li><code>pictureIDs</code> (<em>option</em>) - when type = 0</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/response_reveal');?>/IWPMXYTFVCVXVWYAPRB9/2/0/YES</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetNewestMessageStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetNewestMessageStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
  <li><code>REVEALED</code> - indicates that reveal request is accepted. </li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
<!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->

<h2 id="UpdateRevealV4">Update reveal info</h2>
<p>This is a web service lets you to update your real information such as: RealName, School, Collage, Major</p>
<p class="note">Status: Active</p>

<p>This request is an HTTP URL with POST method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/update_reveal');?>/#token</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
    <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
    <li><code>RealName</code> (<em>optional</em>) - ...</li>
    <li><code>FbEmail</code> (<em>optional</em>) - ...</li>
    <li><code>College </code> (<em>optional</em>) - ...</li>
    <li><code>Looks</code> (<em>optional</em>) - ...</li>
    <li><code>Look_description</code> (<em>optional</em>) - ...</li>
    <li><code>Party </code> (<em>optional</em>) - ...</li>
    <li><code>Party_description</code> (<em>optional</em>) - ...</li>
    <li><code>Flirty</code> (<em>optional</em>) - ...</li>
    <li><code>Flirty_description</code> (<em>optional</em>) - ...</li>
    <li><code>Major</code> (<em>optional</em>) - ...</li>
    <li><code>Major_description</code> (<em>optional</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/update_reveal/IWPMXYTFVCVXVWYAPRB9')?>">
    RealName<input type="text" name="RealName"/>
    FbEmail<input type="text" name="FbEmail"/>
    College<input type="text" name="College"/>
    Looks<input type="text" name="Looks"/>
    Looks Description<input type="text" name="Look_description"/>
    Party<input type="text" name="Party"/>
    Party Description<input type="text" name="Party_description"/>
    Flirty<input type="text" name="Flirty"/>
    Flirty Description<input type="text" name="Flirty_description"/>
    Major<input type="text" name="Major"/>
    Major Description<input type="text" name="Major_description"/>
    <input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
    <li><code>"code"</code> contains metadata on the request. See <a href="#SendRevealStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SendRevealStatusCodes1">Status Codes</h3>
<p>The <code>"code"</code> field within the result response object contains the status of the request, and may contain debugging information
    to help you track down why this request failed. The <code>"code"</code> field may contain the following values:</p>
<ul>
    <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
    <li><code>OK</code> - indicates that no errors occurred; </li>
    <li><code>INVALID_REQUEST</code> - request without post method, or parameters are missing, or user is not existed.</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<!-- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
<h2 id="GetRevealInfoV3">Get reveal info</h2>
<p>Return all reveal info between 2 users</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_reveal_info');?>/#token/#targetUserID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
    <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
    <li><code>targetUserID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_reveal_info');?>/IWPMXYTFVCVXVWYAPRB9/2</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    UserRevealInfo: {
        ListSecretPicture: {
            isRequest: 1,
            isReveal: 1
        },
        FbEmail: {},
        RealName: {},
        College: {},
        Major: {
            content: "",
            description: "",
            isReveal: 0,
            isRequest: 1
        }
    },
    TargetRevealInfo: {
        ListSecretPicture: {
            isRequest: 0,
            isReveal: 0
        },
        FbEmail: {},
        RealName: {},
        College: {
            content: "",
            isReveal: 0,
            isRequest: 0
        },
        Major: {
            content: "",
            description: "",
            isReveal: 0,
            isRequest: 0
        }
    },
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>

    <pre class="prettyprint">
           UserRevealInfo: 
               + information of current user 
               + isRequest: a flag to know target user requested reveal to you 
               + isReveal: a flag to know you revealed to target user
           TargetRevealInfo: 
               + information of target user 
               + isRequest: a flag to know you requested reveal to target user
               + isReveal: a flag to know target user revealed to you
    </pre>
<ul>
    <li><code>"code"</code> contains metadata on the request. See <a href="#GetRevealInfoV3StatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetRevealInfoV3StatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
    <li><code>OK</code> - indicates that no errors occurred;</li>
    <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
    <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>



<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -->

<h2 id="UpdateSecretPictureV2">Update secret picture</h2>
<p>This is a web service allows you to add new secret picture or update a secret picture</p>
<p class="note">Status: Active</p>

<p>This request is an HTTP URL with POST method of the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/update_secret_picture');?>/#token/#PictureID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>PictureID</code> (<em>option</em>) - when delete picture ...</li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>SecretPicture</code> (<em>optional</em>) - When field BasicPicture = delete (delete is a string), image will be removed.</li>
  <li><code>ImageLink</code> (<em>optional</em>)</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/update_secret_picture/IWPMXYTFVCVXVWYAPRB9/2')?>">
	SecretPicture  <input type="file" name="SecretPicture">
	ImageLink  <input type="text" name="ImageLink">
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"PictureID":1,"PictureUrl":"http:\/\/localhost\/anomo\/public\/upload\/6764c565b47c7fa90d6606194d764108.jpg","code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#SendRevealStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SendRevealStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field within the result response object contains the status of the request, and may contain debugging information
  to help you track down why this request failed. The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> - request without post method, or parameters are missing, or user is not existed.</li>
  <li><code>UNAVAILABLE_LINK</code> - unavailable link.</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetListSecretPicture">Get list secret picture</h2>
<p>Return list secret picture of user. It includes requesting status, reveal status to know the target user requests or reveal them</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_secret_picture');?>/#token/#TargetUserID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>

<ul>
    <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
    <li><code>TargetUserID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_secret_picture');?>/IWPMXYTFVCVXVWYAPRB9/1</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    UserRevealInfo: {
        ListSecretPicture: [ ]
    },
    TargetRevealInfo: {
        ListSecretPicture: [
        {
            PictureID: "574",
            Order: "0",
            FileName: "http://anomo-sandbox.s3.amazonaws.com/Upload/142483f1ec216b2dee49cace7804ebf4.jpg",
            Photo100: "http://anomo-sandbox.s3.amazonaws.com/Upload/142483f1ec216b2dee49cace7804ebf4_thumb_100x150.jpg",
            Photo200: "http://anomo-sandbox.s3.amazonaws.com/Upload/142483f1ec216b2dee49cace7804ebf4_thumb_200x300.jpg",
            Photo300: "http://anomo-sandbox.s3.amazonaws.com/Upload/142483f1ec216b2dee49cace7804ebf4_thumb_300x450.jpg",
            isReveal: 1,
            isRequest: 1
        },
        {
            PictureID: "575",
            Order: "0",
            FileName: "http://anomo-production.s3.amazonaws.com/Share/anomoinc.png",
            Photo100: "http://anomo-production.s3.amazonaws.com/Share/anomoinc.png",
            Photo200: "http://anomo-production.s3.amazonaws.com/Share/anomoinc.png",
            Photo300: "http://anomo-production.s3.amazonaws.com/Share/anomoinc.png",
            isReveal: 0,
            isRequest: 1
        }
        ]
    },
    code: "OK"
}
</span></pre>

<h3 id="GetUserDetailStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field within the result response object contains the status of the request, and may contain debugging information
  to help you track down why this request failed. The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_REQUEST</code> indicates that parameters are missing or user is not existed.</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="DeleteSecretPicture">Delete secret picture</h2>
<p>Delete a secret picture</p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/delete_secret_picture');?>/#token/#PictureID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>ID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/delete_secret_picture');?>/IWPMXYTFVCVXVWYAPRB9/1</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetContactOfUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetContactOfUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid or user is not existed; </li>
  <li><code>OK</code> - indicates that no errors occurred; </li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
<h2 id="SearchActivityUser">Search User [in Activity > Search screen]</h2>
<p>Return maximum 3 users matching with keyword</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/search_user');?>/#token</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Keyword</code> (<em>required</em>) - ...</li>
</ul>
<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/activity/search_user');?>/IWPMXYTFVCVXVWYAPRB9">
	Keyword		<input type="text" name="Keyword">
				<input type="submit" value="Submit" class="button">
</form>
</pre>
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "ListUser": [
        {
            "UserID": "24",
            "UserName": "testing",
            "Avatar": "http://anomo-sandbox.s3.amazonaws.com/Upload/a209aede18bfe43aab9b095fa543b8b3.jpg",
            "BirthDate": "1972-09-01",
            "NeighborhoodID": "29575",
            "NeighborhoodName": "Vietnam"
        },
        {
            "UserID": "25",
            "UserName": "jsnooptest",
            "Avatar": "http://anomo-sandbox.s3.amazonaws.com/Upload/2eb88d095929295250ea9b29f13fb4e6.jpg",
            "BirthDate": "1978-05-16",
            "NeighborhoodID": "29662",
            "NeighborhoodName": "Kirkland, WA"
        },
        {
            "UserID": "43",
            "UserName": "Tester",
            "Avatar": "http://anomo-sandbox.s3.amazonaws.com/Upload/4d4880bad31dbf06b7f33a8284735a6a.jpg",
            "BirthDate": "2000-10-10",
            "NeighborhoodID": "27177",
            "NeighborhoodName": "Ashburn"
        }
    ],
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#SearchUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SearchUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
<h2 id="SearchActivityHashTag">Search HashTag [in Activity > Search screen]</h2>
<p>Return maximum 3 hash tags matching with keyword</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/search_hashtag');?>/#token</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Keyword</code> (<em>required</em>) - ...</li>
  <li><code>CateID</code> (<em>optional</em>) - category id</li>
  <li><code>TopicID</code> (<em>optional</em>) - topic id</li>
</ul>
<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/activity/search_hashtag');?>/IWPMXYTFVCVXVWYAPRB9">
	Keyword		<input type="text" name="Keyword">
        CateID		<input type="text" name="CateID">
        TopicID		<input type="text" name="TopicID">
				<input type="submit" value="Submit" class="button">
</form>
</pre>
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "ListHashTag": [
        {
            "HashTag": "test"
        },
        {
            "HashTag": "testing"
        }
    ],
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#SearchUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SearchUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
<h2 id="SearchActivityTag">Search Tag [in Activity > Search screen]</h2>
<p>It returns a flag [IsHaveTag] to know have a tag matching with keyword or not <br />
IsHaveTag = 1: have a tag matching with keyword<br />
IsHaveTag = 0: don't have any tag matching with keyword
</p> 
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/search_tag');?>/#token</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Keyword</code> (<em>required</em>) - ...</li>
</ul>
<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/activity/search_tag');?>/IWPMXYTFVCVXVWYAPRB9">
	Keyword		<input type="text" name="Keyword">
				<input type="submit" value="Submit" class="button">
</form>
</pre>
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "IsHaveTag": 1,
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#SearchUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SearchUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
<h2 id="SearchActivityTopic">Search Topic [in Activity > Search screen]</h2>
<p>Return maximum 3 topics matching with keyword</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/activity/search_topic');?>/#token</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Keyword</code> (<em>required</em>) - ...</li>
  <li><code>CateID</code> (<em>optional</em>) - category id</li>
</ul>
<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/activity/search_topic');?>/IWPMXYTFVCVXVWYAPRB9">
	Keyword		<input type="text" name="Keyword">
        CateID		<input type="text" name="CateID">
				<input type="submit" value="Submit" class="button">
</form>
</pre>
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "results": [
        {
            "TopicID": "23",
            "TopicName": "testingabc",
            "TotalPost": "0",
            "Desc": "bal bale",
            "IsFavorite": "0",
            "Photo": "http://anomo-sandbox.s3.amazonaws.com/Stream/044f44ef57d444b9afa405eff0f13699.jpg"
        },
        {
            "TopicID": "48",
            "TopicName": "testests",
            "TotalPost": "0",
            "Desc": "bal bale",
            "IsFavorite": "0",
            "Photo": "http://anomo-sandbox.s3.amazonaws.com/Stream/b19e2af1188bdaef450b80f45742fcb5.jpg"
        },
        {
            "TopicID": "47",
            "TopicName": "Abss test",
            "TotalPost": "0",
            "Desc": "bal bale",
            "IsFavorite": "0",
            "Photo": "http://anomo-sandbox.s3.amazonaws.com/Stream/24970821b406c245c4956c826c2b60af.jpg"
        }
    ],
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#SearchUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SearchUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
<h2 id="InfluencePointChart">[Influence points] chart</h2>
<p>
</p> 
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/influence_point/week_chart');?>/#token/#TargetUserID</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>TargetUserID</code> (<em>required</em>)</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<span class="com"><?php echo site_url('webservice/influence_point/week_chart');?>/IWPMXYTFVCVXVWYAPRB9/1</span>
</pre>
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    result: [
        {
            Week: 6,
            Point: 0
        },
        {
            Week: 5,
            Point: 0
        },
        {
            Week: 4,
            Point: 13
        },
        {
            Week: 3,
            Point: 0
        },
        {
            Week: 2,
            Point: 0
        },
        {
            Week: 1,
            Point: 3
        }
    ],
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#SearchUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SearchUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
<h2 id="InfluencePointTotal">[Influence points] total points</h2>
<p>
</p> 
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/influence_point/total_point');?>/#token/#TargetUserID</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>TargetUserID</code> (<em>required</em>)</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<span class="com"><?php echo site_url('webservice/influence_point/total_point');?>/IWPMXYTFVCVXVWYAPRB9/1</span>
</pre>
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    result: [
        {
            Type: "1",
            Name: "Likes +3pts",
            Point: "6",
            Message: "Any time a user likes your status or comment, you earn 3 reputation pts."
        },
        {
            Type: "2",
            Name: "Follower +10pts",
            Point: "80",
            Message: "Attracting followers earns 10 points per follower and –10 pts for being un-followed."
        },
        {
            Type: "3",
            Name: "Friend Invite +25pts",
            Point: "0",
            Message: "Sending an SMS invitation to friends earns 25 reputation points per invite."
        }
    ],
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#SearchUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SearchUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
<h2 id="InviteSMS">Invite friend via SMS</h2>
<p>
</p> 
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/invite_friend/sms');?>/#token</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>PhoneNumbers</code> (<em>required</em>) - list PhoneNumber separated with a semicolon symbol (#number1;#number2;#number3,...)</li>
</ul>
<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/invite_friend/sms');?>/IWPMXYTFVCVXVWYAPRB9">
	PhoneNumbers		<input type="text" name="PhoneNumbers">
				<input type="submit" value="Submit" class="button">
</form>
</pre>
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#SearchUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SearchUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
<h2 id="ShareContentFB">Give 10 points each time someone shares content on their FB Wall</h2>
<p>
</p> 
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/share/share_content_to_fb');?>/#token</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<span class="com"><?php echo site_url('webservice/share/share_content_to_fb');?>/IWPMXYTFVCVXVWYAPRB9</span>
</pre>
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#SearchUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SearchUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
</ul>
<!-- Send Verification Code -->
<h2 id="SendVerificationCode">Send Verification Code</h2>
<p>
</p> 
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/send_verification_code');?>/#token</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Phone</code> (<em>required</em>) - Phone number </li>
</ul>
<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/send_verification_code');?>/IWPMXYTFVCVXVWYAPRB9">
Phone <input type="text" name="Phone"> <input type="submit" value="Submit" class="button">
</form>
</pre>
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#SearchUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SearchUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that user has been verified, verifying limit (5 minutes per request). </li>
  <li><code>INVALID_PARAMETER</code> - indicates that param is invalid or missed. </li>
  <li><code>PHONE_EXISTED</code> - indicates that phone number is existed. </li>
  <li><code>FAIL</code> - indicates that phone number is wrong or not available, send SMS failed. </li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<!-- Verify Code -->
<h2 id="VerifyCode">Verify Code</h2>
<p>
</p> 
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/verify_code');?>/#token</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Code</code> (<em>required</em>) - unlock code </li>
</ul>
<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/verify_code');?>/IWPMXYTFVCVXVWYAPRB9">
Code <input type="text" name="Code"> <input type="submit" value="Submit" class="button">
</form>
</pre>
<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#SearchUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SearchUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that user has been verified, verifying limit (6 attempts). </li>
  <li><code>INVALID_PARAMETER</code> - indicates that param is invalid or missed. </li>
  <li><code>INVALID_CODE</code> - indicates that unlock code is invalid. </li>  
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<!-- Check Verification Status -->
<h2 id="CheckVerificationStatus">Check Verification Status</h2>
<p></p>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/check_verification_status');?>/#token</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/check_verification_status');?>/IWPMXYTFVCVXVWYAPRB9</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"VERIFIED"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#CheckAppVersionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="CheckAppVersionStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>VERIFIED</code> - indicates that user verified</li>
  <li><code>UNVERIFIED</code> indicates that user unverified</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>  
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<!-- Get Match User -->
<h2 id="GetMatchUser">Get Match User</h2>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_match');?>/#token/#Gender/#AgeFrom/#AgeTo</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>      
  <li><code>Gender</code> (<em>optional</em>) - (0: both, 1: male, 2: female) </li>
  <li><code>AgeFrom</code> (<em>optional</em>) - </li>
  <li><code>AgeTo</code> (<em>optional</em>) - </li>
</ul>

<p>An example request is below:</p>

<pre class="prettyprint">
<?php echo site_url('webservice/user/get_match');?>/IWPMXYTFVCVXVWYAPRB9/0/17/100
</pre>


<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
  "results": {
    "ListUser": [
      {
        "UserID": "34085",
        "UserName": "blackpanda",
        "Lat": "47.6763382",
        "Lon": "-122.1253917",
        "Avatar": "http:\/\/anomo-sandbox.s3.amazonaws.com\/Upload\/http:\/\/anomo-sandbox.s3.amazonaws.com\/Upload\/58a3ca5537d7b9ab402697d2093026db.jpg",
        "FullPhoto": "http:\/\/anomo-sandbox.s3.amazonaws.com\/Upload\/f9a425334b45e99eba2eaa2f960d8d04.jpg",
        "ProfileStatus": "",
        "Email": "",
        "NeighborhoodID": "29672",
        "BirthDate": "1991-05-21",
        "Gender": "1",
        "FacebookID": "1",
        "IsOnline": "0",
        "PlaceID": "",
        "Status": "0",
        "IsFavorite": "0",
        "IsBlock": "0",
        "IsBlockTarget": "0",
        "NumberOfFollower": "0",
        "NumberOfFollowing": "2",
        "Point": "0",
        "PlaceName": "Redmond, WA",
        "NeighborhoodName": "",
        "SharedInterest": "6",
        "SharedQuestion": "8",
        "SharedMajor": "3",
        "Matching": "100",
        "Majors": [
            {
                MajorID: "1",
                Name: "Aeronautics & Astronautics ",
                Matched: "1"
            },
            {
                MajorID: "5",
                Name: "Afro-American Studies ",
                Matched: "1"
            },
            {
                MajorID: "4",
                Name: "American Ethnic Studies ",
                Matched: "1"
            }
        ],
        "Tags": [
          {
            "TagID": "10",
            "Name": "Anime and Cartoons",
            "TagCategoryID": "1",
            "Matched": "1"
          },
          {
            "TagID": "11",
            "Name": "Astrology",
            "TagCategoryID": "1",
            "Matched": "1"
          },
          {
            "TagID": "12",
            "Name": "Camping\/Outdoors",
            "TagCategoryID": "1",
            "Matched": "1"
          },
          {
            "TagID": "13",
            "Name": "Chess",
            "TagCategoryID": "1",
            "Matched": "1"
          },
          {
            "TagID": "14",
            "Name": "Cinema",
            "TagCategoryID": "1",
            "Matched": "1"
          },
          {
            "TagID": "15",
            "Name": "Martial Arts",
            "TagCategoryID": "2",
            "Matched": "1"
          }          
        ]
      }
    ]
  },
  "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>	
	<li><code>"timeleft"</code> - timeleft for next match</li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#SearchUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="SearchUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>
  <li><code>INVALID_PARAMETER</code> - indicates that param is invalid. </li>
  <li><code>WAIT_FOR_NEXT_MATCH</code> - indicates that user've gotten match and wait for next match (12hrs). </li> 
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<!-- Check Match User -->
<h2 id="CheckMatchUser">Check Match User</h2>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/check_match');?>/#token</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>      
</ul>

<p>An example request is below:</p>

<pre class="prettyprint">
<?php echo site_url('webservice/user/check_match');?>/IWPMXYTFVCVXVWYAPRB9
</pre>


<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "previous": {
        "UserID": "34110",
        "UserName": "Luongluu",
        "Gender": "1",
        "BirthDate": "1986-01-06",
        "Credits": "0",
        "NumberOfFollower": "1",
        "NumberOfFollowing": "7",
        "GenderDating": "2",
        "Point": "130001",
        "IsVendor": "0",
        "IsAgeLess": "0",
        "IsAdmin": "0",
        "HasFanPage": "1",
        "IsVerify": "0",
        "IsFavorite": 0,
        "IsReported": 0,
        "Email": "",
        "FacebookID": "1",
        "FbEmail": "",
        "ProfileStatus": "{\"message\":\"Anon post from L\",\"message_tags\":[]}",
        "AboutMe": "",
        "Avatar": "http://anomo-sandbox.s3.amazonaws.com/Upload/41dd119e8df014d772aa62d20b6c6372.jpg",
        "FullPhoto": "http://anomo-sandbox.s3.amazonaws.com/Upload/b2e256f1fcffdc21a7163fe8b84969fd.jpg",
        "CoverPicture": "http://anomo-sandbox.s3.amazonaws.com/Upload/conver_picture_bg.png",
        "NeighborhoodID": "29698",
        "NeighborhoodName": "Earth",
        "IsBlock": 0,
        "IsBlockTarget": 0,
        "Tags": [
            {
                "TagID": "10",
                "Name": "Anime and Cartoons",
                "TagCategoryID": "1"
            },
            {
                "TagID": "11",
                "Name": "Astrology",
                "TagCategoryID": "1"
            },                     
            {
                "TagID": "1398",
                "Name": "Ban Be",
                "TagCategoryID": "0"
            },
            {
                "TagID": "1849",
                "Name": "Bowling",
                "TagCategoryID": "2"
            },
            {
                "TagID": "12",
                "Name": "Camping/Outdoors",
                "TagCategoryID": "1"
            },            
            {
                "TagID": "13",
                "Name": "Chess",
                "TagCategoryID": "1"
            },
            {
                "TagID": "14",
                "Name": "Cinema",
                "TagCategoryID": "1"
            },
            {
                "TagID": "1404",
                "Name": "DH HOA SEN",
                "TagCategoryID": "0"
            },            
            {
                "TagID": "2163",
                "Name": "h?ng v??ng highschool",
                "TagCategoryID": "0"
            },
            {
                "TagID": "15",
                "Name": "Martial Arts",
                "TagCategoryID": "2"
            },            
            {
                "TagID": "1843",
                "Name": "Watching TV",
                "TagCategoryID": "1"
            }
        ]        
    },
    "code": "WAIT_FOR_NEXT_MATCH",
    "timeleft": 24468
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
	<li><code>"previous"</code> - previous matched user</li>
	<li><code>"timeleft"</code> - timeleft for next match</li>
  	<li><code>"code"</code> contains metadata on the request. See <a href="#CheckMatchUserStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="CheckMatchUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>  
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>OK</code> - indicates that no errors occurred and allow user get match</li>
  <li><code>PLAY_ICEBREAKER</code> - indicates that user must play ice breaker before get match. </li>
  <li><code>WAIT_FOR_NEXT_MATCH</code> - indicates that user've gotten match and wait for next match (12hrs). </li>  
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<!-- Get Previous Match Info -->
<h2 id="GetMatchInfo">Get History Match User</h2>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_match_info');?>/#token/#TargetUserID</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>      
  <li><code>TargetUserID</code> (<em>required</em>) - Target UserID </li>  
</ul>

<p>An example request is below:</p>

<pre class="prettyprint">
<?php echo site_url('webservice/user/get_match_info');?>/IWPMXYTFVCVXVWYAPRB9/34110
</pre>


<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
  "previous": {
    "UserID": "34085",
    "UserName": "blackpanda",
    "Gender": "1",
    "BirthDate": "1991-05-21",
    "Credits": "2000",
    "NumberOfFollower": "0",
    "NumberOfFollowing": "2",
    "GenderDating": "0",
    "Point": "0",
    "IsVendor": "0",
    "IsAgeLess": "0",
    "IsAdmin": "0",
    "HasFanPage": "0",
    "IsVerify": "1",
    "IsFavorite": 0,
    "IsReported": 0,
    "Email": "",
    "FacebookID": "1",
    "FbEmail": "",
    "ProfileStatus": "{\"message\":\"kirkland\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\ntest\",\"message_tags\":[]}",
    "AboutMe": "",
    "Avatar": "http:\/\/anomo-sandbox.s3.amazonaws.com\/Upload\/http:\/\/anomo-sandbox.s3.amazonaws.com\/Upload\/58a3ca5537d7b9ab402697d2093026db.jpg",
    "FullPhoto": "http:\/\/anomo-sandbox.s3.amazonaws.com\/Upload\/f9a425334b45e99eba2eaa2f960d8d04.jpg",
    "CoverPicture": "http:\/\/anomo-sandbox.s3.amazonaws.com\/Upload\/conver_picture_bg.png",
    "NeighborhoodID": "29672",
    "NeighborhoodName": "Redmond, WA",
    "IsBlock": 0,
    "IsBlockTarget": 0,
    "Majors": [
        {
            MajorID: "1",
            Name: "Aeronautics & Astronautics ",
            Matched: "1"
        },
        {
            MajorID: "5",
            Name: "Afro-American Studies ",
            Matched: "1"
        },
        {
            MajorID: "4",
            Name: "American Ethnic Studies ",
            Matched: "1"
        }
    ],
    "Tags": [
      {
        "TagID": "10",
        "Name": "Anime and Cartoons",
        "TagCategoryID": "1",
        "Matched": "1"
      },
      {
        "TagID": "11",
        "Name": "Astrology",
        "TagCategoryID": "1",
        "Matched": "1"
      },
      {
        "TagID": "12",
        "Name": "Camping\/Outdoors",
        "TagCategoryID": "1",
        "Matched": "1"
      },
      {
        "TagID": "13",
        "Name": "Chess",
        "TagCategoryID": "1",
        "Matched": "1"
      },
      {
        "TagID": "14",
        "Name": "Cinema",
        "TagCategoryID": "1",
        "Matched": "1"
      },
      {
        "TagID": "15",
        "Name": "Martial Arts",
        "TagCategoryID": "2",
        "Matched": "1"
      },
      {
        "TagID": "1357",
        "Name": "Adv. 2D Animation Drawing Club_ Group 1",
        "TagCategoryID": "0",
        "Matched": "0"
      },
      {
        "TagID": "1352",
        "Name": "AIS Filmmaking",
        "TagCategoryID": "0",
        "Matched": "0"
      },
      {
        "TagID": "1406",
        "Name": "anomo",
        "TagCategoryID": "0",
        "Matched": "0"
      },
      {
        "TagID": "1354",
        "Name": "Anomo Ambassadors",
        "TagCategoryID": "0",
        "Matched": "0"
      },
      {
        "TagID": "1359",
        "Name": "Art Institute of Seattle Print Center\/ Technical Support Center",
        "TagCategoryID": "0",
        "Matched": "0"
      },
      {
        "TagID": "1355",
        "Name": "Artist's Pokedex",
        "TagCategoryID": "0",
        "Matched": "0"
      },
      {
        "TagID": "1358",
        "Name": "Buisness law",
        "TagCategoryID": "0",
        "Matched": "0"
      },
      {
        "TagID": "1363",
        "Name": "Creative Director",
        "TagCategoryID": "0",
        "Matched": "0"
      },
      {
        "TagID": "1351",
        "Name": "Creative Exchange",
        "TagCategoryID": "0",
        "Matched": "0"
      },
      {
        "TagID": "1360",
        "Name": "Guild Wars 2",
        "TagCategoryID": "0",
        "Matched": "0"
      },
      {
        "TagID": "1368",
        "Name": "HobbyTown USA",
        "TagCategoryID": "0",
        "Matched": "0"
      },
      {
        "TagID": "1353",
        "Name": "Out of Time!",
        "TagCategoryID": "0",
        "Matched": "0"
      }      
    ]
  },
  "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>		
  	<li><code>"code"</code> contains metadata on the request. See <a href="#GetMatchInfoStatusCode">Status Codes</a> below.</li>
</ul>

<h3 id="GetMatchInfoStatusCode">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>    
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<!-- Get Previous Match Question -->
<h2 id="GetMatchQuestion">Get History Match Question</h2>
<p class="note">Status: Active</p>

<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_match_question');?>/#token/#TargetUserID/#LastQuestionID</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>      
  <li><code>TargetUserID</code> (<em>required</em>) - Target UserID </li>
  <li><code>LastQuestionID</code> (<em>optional</em>) - the last QuestionID of previous page </li>    
</ul>

<p>An example request is below:</p>

<pre class="prettyprint">
<?php echo site_url('webservice/user/get_match_question');?>/IWPMXYTFVCVXVWYAPRB9/34110
</pre>


<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
  "results": [
    {
      "QuestionID": "3597",
      "AnswerID": "10559",
      "Question": "I am:",
      "Answer": "Single"
    },
    {
      "QuestionID": "3598",
      "AnswerID": "10563",
      "Question": "My thoughts on credit cards:",
      "Answer": "Maxed it out"
    },
    {
      "QuestionID": "3600",
      "AnswerID": "10571",
      "Question": "My sexual preference is:",
      "Answer": "Gay"
    },
    {
      "QuestionID": "3602",
      "AnswerID": "10579",
      "Question": "Who was the last person in your family to move to the USA?",
      "Answer": "Me"
    },
    {
      "QuestionID": "3603",
      "AnswerID": "10583",
      "Question": "Right now, I'm living with",
      "Answer": "My parents"
    },
    {
      "QuestionID": "3604",
      "AnswerID": "10587",
      "Question": "My mode of transportion is",
      "Answer": "Own car baby!"
    },
    {
      "QuestionID": "3605",
      "AnswerID": "10591",
      "Question": "Where would you most likely shop for new clothes?",
      "Answer": "Shopping mall"
    },
    {
      "QuestionID": "3606",
      "AnswerID": "10595",
      "Question": "What describes your eating preference?",
      "Answer": "Vegetarian"
    }
  ],
  "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>		
  	<li><code>"code"</code> contains metadata on the request. See <a href="#GetMatchQuestionStatusCode">Status Codes</a> below.</li>
</ul>

<h3 id="GetMatchQuestionStatusCode">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred;</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid. </li>    
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetUserTag">Get user tag</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_user_tag');?>/#Token</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/get_user_tag');?>/IWPMXYTFVCVXVWYAPRB9</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    Tags: [
        {
            TagID: "12",
            Name: "Camping/Outdoors",
            TagCategoryID: "1"
        },
        {
            TagID: "1320",
            Name: "Cars",
            TagCategoryID: "1"
        },
        {
            TagID: "337",
            Name: "Drinking",
            TagCategoryID: "1"
        }
    ],
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>




<h2 id="GetSetting">Get setting</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/setting/get_setting');?></span></pre>


<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/setting/get_setting');?></span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    "results": {
        "UsePromoCode": "1"
    },
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>



<h2 id="ValidatePromoCode">Validate Promo Code</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/setting/validate_promo_code');?></span></pre>



<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Code</code> (<em>required</em>) - </li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/setting/validate_promo_code')?>">
	Code		<input type="text" name="Code"/>
	<input type="submit" value="Submit" class="button"/>
</form></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{

    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_CODE</code> - indicates that promo code is invalid.</li>
  <li><code>INVALID_PARAMETER</code> - indicates that missing parameter.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<h2 id="SendEmailCode">Send Email Code</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/setting/send_email_code');?></span></pre>



<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Email</code> (<em>required</em>) - </li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/setting/send_email_code')?>">
	Email		<input type="text" name="Email"/>
	<input type="submit" value="Submit" class="button"/>
</form></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{

    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_PARAMETER</code> - indicates that missing parameter.</li>
  <li><code>INVALID_EMAIL</code> - indicates that Email is invalid.</li>
  <li><code>EMAIL_EXISTED</code> - indicates that Email is registered</li>
  <li><code>FAILED</code> - indicates that error occurred when sending email.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<h2 id="ValidateEmailCode">Validate Email Code</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/setting/validate_email_code');?></span></pre>



<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Email</code> (<em>required</em>) - </li>
  <li><code>Code</code> (<em>required</em>) - </li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/setting/validate_email_code')?>">
	Email		<input type="text" name="Email"/>
        Code		<input type="text" name="Code"/>
	<input type="submit" value="Submit" class="button"/>
</form></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{

    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_PARAMETER</code> - indicates that missing parameter.</li>
  <li><code>INVALID_EMAIL</code> - indicates that Email is invalid.</li>
  <li><code>EMAIL_EXISTED</code> - indicates that Email is registered</li>
  <li><code>INVALID_CODE</code> - indicates that the code is invalid.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>



<h2 id="VerifyEmailCode">Verify Email Code</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/verify_email_code/#TOKEN');?></span></pre>



<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>EmailCode</code> (<em>required</em>) - </li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/verify_email_code/IWPMXYTFVCVXVWYAPRB9')?>">
	EmailCode		<input type="text" name="EmailCode"/>
	<input type="submit" value="Submit" class="button"/>
</form></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{

    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_PARAMETER</code> - indicates that missing parameter.</li>
  <li><code>INVALID_CODE</code> - indicates that the code is invalid.</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>


<h2 id="CheckEmailVerificationStatus">Check Email Verification Status</h2>
<p></p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/check_email_verification_status');?>/#Token</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>Token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/check_email_verification_status');?>/IWPMXYTFVCVXVWYAPRB9</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    code: "OK",
    NeedVerify: "1"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is invalid.</li>
</ul>

<h2 id="UpdateUserEmail">Update User email</h2>
<p>Update information of user. It is called in edit profile screen</p>
<p class="note">Status: Active</p>

<p>Update User request is an HTTP URL with POST method of the following form:</p>

<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/user/update_email');?>/#token</span></em></pre>

<p>The list of parameters on URL and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>
<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>Email</code> (<em>required</em>) - </li> 
</ul>

<p>An example request is below:</p>

<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/user/update_email/IWPMXYTFVCVXVWYAPRB9')?>">
	Email	<input type="text" name="Email"/>
	
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#UpdateUserStatusCodes">Status Codes</a> below.</li>

</ul>

<h3 id="UpdateUserStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; A new user was created successfully.</li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_EMAIL</code> - indicates that Email is invalid.</li>
  <li><code>EMAIL_EXISTED </code> - indicates that Email is registered</li>
  <li><code>FAILED</code> - indicates that error occurred when updating db.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
