<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/assets/css/style.css">



<div class="primary-screen">
  <div class="header-screen">
    <div class="top-msg">Download app and play now!</div>
  </div>
  <div class="body-screen">
  	<?php foreach ($questions as $question):?>
    <div class="question">
      <p>Q<?php echo $question->Order?>:  <?php echo $question->QuestionContent?></p>
      <ul>
      	<?php foreach ($question->Answers as $answer):?>
        <li class="<?php echo ($answer->AnswerID == $question->AnswerID)?"star":""?>"><?php echo $answer->Content?></li>
        <?php endforeach;?>
      </ul>
      
    </div>
    <?php endforeach;?>
  </div>
  <div class="footer-screen"> MY ICE BREAKER RESULTS </div>
</div>

