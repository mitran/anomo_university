<h1 id="Anomotion" style="margin:1em 0 0 -10px;">Game IceBreaker</h1>
<p>Description for Game IceBreaker.</p>

<ol class="toc">
        <li><a href="#RandomUserPlayGame">Random user (for game 4 peoples)</a></li>
        <li><a href="#ReturnListQuestion">Return list question (for game 4 peoples)</a></li>
        <li><a href="#CreateGame4People">Create game 4 people</a></li>
        <li><a href="#RequestSingleGame">Request single game</a></li>
    
    
	<li><a href="#GetAllAnomotionBetween2User">Get All Anomotions Between 2 User</a></li>
	<li><a href="#DeleteAnomotion">Delete anomotion</a></li>
	<li><a href="#DeleteAllAnomotion">Delete all anomotion</a></li>
	<li><a href="#AnomotionShareFB">Anomotion share FB</a></li>
        <li><a href="#GetFinishAnomotion">Get all info of a finish anomotion</a></li>

	
        
        <li><a href="#CheckIsAnomoUser">[FB GAME]Check fb account is already have anomo account</a></li>
        <li><a href="#ListFbGameInvited">[FB GAME] Return list fb game invited</a></li>
        <li><a href="#CreateFBGame">[FB GAME] Create FB game</a></li>
        <li><a href="#GetListUserPlayFBGame">[FB GAME] Return list user who played fb game</a></li>
</ol>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="RequestSingleGame">Request single game</h2>
<p>This is a web service allows you to play game with other people. It returns list question and game id if existed a pending game.</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/anomotion/request_single_game');?>/#token/#TargetUserID/#AnomotionID</span></em></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>TargetUserID</code> (<em>required</em>) - the id of target user</li>
  <li><code>AnomotionID</code> (<em>optional</em>) -  if you have pending game and want to play continues, please input it.</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/anomotion/request_single_game');?>/IWPMXYTFVCVXVWYAPRB9/2</span></em></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    AnomotionID: "4250",
    code: "OK",
    UserID: "1540",
    UserName: "cattwomenhelloy",
    Avatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/2073a11eea84588a426e87874a6071a2.jpg",
    CoverPicture: "http://anomo-sandbox.s3.amazonaws.com/Upload/conver_picture_bg.png",
    Credits: "875",
    NeighborhoodID: "29696",
    NeighborhoodName: "Ho Chi Minh City, Vietnam",
    Question: [
        {
            QuestionID: "577",
            QuestionGroupID: "116",
            Content: "Be honest. Do you drink milk out of the bottle?",
            QuestionType: "2",
            Order: "1",
            CreatedDate: "2013-04-23 10:00:23",
            ExpiredDate: null,
            Answer: [
                {
                AnswerID: "1750",
                Content: "Yes"
                },
                {
                AnswerID: "1751",
                Content: "No"
                },
                {
                AnswerID: "1752",
                Content: "Sometimes"
                }
            ]
        },
        {},
        {},
        {},
        {}
    ]
}

</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#RequestAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="RequestAnomotionStatusCodes">Status Codes</h3>

<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>COMPLETED</code> - indicates that this game is completed. You've already answered all questions and waiting partner.
      <li><code>FINISHED</code> - indicates that game is finished</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
  <li><code>BLOCK_TARGET</code> - in case user is blocked by target user (first priority)  </li>
  <li><code>BLOCK</code> - in case user block target user </li>
</ul>




<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetAllAnomotion">Get All Anomotions</h2>
<p class="warning">Status: Deprecated</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/anomotion/get_all');?>/#token/#UserID</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>UserID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/anomotion/get_all');?>/IWPMXYTFVCVXVWYAPRB9/1</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK","results":[{"AnomotionID":"1","QuestionGroupID":"2","Status":"IN_PROGRESS","CreatedDate":"2012-05-29 00:00:00","TargetUserID":"2","TargetUserName":"oh-yes","TargetAvatar":"http:\/\/localhost\/anomo\/public\/upload\/666e6cc327fc7be4a282790ba97be105.jpg","ConnectMeter":"2"}]}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"results"</code> ...</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetAllAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<p>A JSON response contains results elements:</p>
<ul>
  <li><code>"AnomotionID"</code> ...</li>
  <li><code>"QuestionGroupID"</code> ...</li>
  <li><code>"Status"</code> IN_PROGRESS, FINISHED, CANCEL, WAITING (waiting for acceptation from targetUser), REQUEST_WAITING (received request from targetUser but not accepted yet) </li>
  <li><code>"CreatedDate"</code> ...</li>
  <li><code>"TargetUserID"</code> ...</li>
  <li><code>"TargetUserName"</code> ...</li>
  <li><code>"TargetAvatar"</code> ...</li>
  <li><code>"ConnectMeter"</code> ...</li>
</ul>

<h3 id="GetAllAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetAllAnomotionBetween2User">Get All Anomotions Between 2 User</h2>
<p class="warning">Status: Deprecated</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/anomotion/get_all_anomotion_between_2_user');?>/#token/#TargetUserID</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>TargetUserID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/anomotion/get_all_anomotion_between_2_user');?>/IWPMXYTFVCVXVWYAPRB9/2</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK","results":[{"AnomotionID":"1","QuestionGroupID":"2","Status":"IN_PROGRESS","CreatedDate":"2012-05-29 00:00:00","TargetUserID":"2","TargetUserName":"oh-yes","TargetAvatar":"http:\/\/localhost\/anomo\/public\/upload\/666e6cc327fc7be4a282790ba97be105.jpg","ConnectMeter":"2"}]}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"results"</code> ...</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetAllAnomotionBetween2UserStatusCodes">Status Codes</a> below.</li>
</ul>

<p>A JSON response contains results elements:</p>
<ul>
  <li><code>"AnomotionID"</code> ...</li>
  <li><code>"QuestionGroupID"</code> ...</li>
  <li><code>"Status"</code> IN_PROGRESS, FINISHED, WAITING (waiting for acceptation from targetUser), REQUEST_WAITING (received request from targetUser but not accepted yet) </li>
  <li><code>"CreatedDate"</code> ...</li>
  <li><code>"UserID"</code> ...</li>
  <li><code>"TargetUserID"</code> ...</li>
  <li><code>"UserName"</code> ...</li>
  <li><code>"TargetUserName"</code> ...</li>
  <li><code>"Avatar"</code> ...</li>
  <li><code>"TargetAvatar"</code> ...</li>
  <li><code>"ConnectMeter"</code> ...</li>
</ul>

<h3 id="GetAllAnomotionBetween2UserStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetFinishAnomotion">Get all info of a finish anomotion</h2>
<p>Return result of a game. It includes score, answer of each player, and information of partner</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/anomotion/get_finish_anomotion');?>/#token/#anomotionID</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>anomotionID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/anomotion/get_finish_anomotion');?>/IWPMXYTFVCVXVWYAPRB9/1</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">
{
    CreatedDate: "2014-06-02 03:47:03",
    Score: "100",
    Image: "",
    Result: [
        {
            Question: "Be honest. Do you drink milk out of the bottle?",
            Answer: [
                {
                UserID: "2",
                Answer: "Yes"
                },
                {
                UserID: "1",
                Answer: "Yes"
                }
            ]
        },
        {},
        {},
        {},
        {}
    ],
    code: "OK",
    TargetUserID: "2",
    TargetUserName: "Quyen rom",
    Avatar: "http://anomo-sandbox.s3.amazonaws.com/Upload/male_avatar_default.jpg",
    BirthDate: "1987-02-05",
    FacebookID: "100002895125403",
    CoverPicture: "http://anomo-sandbox.s3.amazonaws.com/Upload/conver_picture_bg.png",
    Credits: "1100",
    NeighborhoodID: "2011",
    NeighborhoodName: "San Francisco"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"Score"</code> percent matching of result 2 user</li>
  <li><code>"Result"</code>Question, Answer</li>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="DeleteAnomotion">Delete anomotion</h2>
<p class="warning">Status: Deprecated</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/anomotion/delete_anomotion');?>/#token/#anomotionID</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>anomotionID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/anomotion/delete_anomotion');?>/IWPMXYTFVCVXVWYAPRB9/1</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="DeleteAllAnomotion">Delete all anomotion</h2>
<p class="warning">Status: Deprecated</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/anomotion/delete_all_game');?>/#token/#targetUserID</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
  <li><code>targetUserID</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/anomotion/delete_all_game');?>/IWPMXYTFVCVXVWYAPRB9/121</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="AnomotionShareFB">Anomotion share FB</h2>
<p>This is a web service allows you to get the url of image result of a game. It lets you to share game result to facebook</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/share/fb_anomotion');?>/#token/#anomotionID</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
  <li><code>anomotionID</code> (<em>required</em>) - the id of game</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/share/fb_anomotion');?>/IWPMXYTFVCVXVWYAPRB9/1</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"Image":"http:\/\/localhost\/anomo\/public\/share\/1_1.png","code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUEST</code> - indicates that param is invalid.</li>
  <li><code>INVALID</code> - indicates that game is not finished.</li>
  <li><code>ERROR</code> - indicates that generate image is failed .</li>
</ul>
<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="RandomUserPlayGame">Return 4 users for play game</h2>
<p>This is a web service allows you to random 4 peoples for playing group game </p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/anomotion/random_anomo');?>/#token</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/anomotion/random_anomo');?>/IWPMXYTFVCVXVWYAPRB9</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">{"Users":[{"UserID":"60","UserName":"zionn","Gender":"0","BirthDate":"1979-03-14","NeighborhoodID":"2296","Avatar":"http:\/\/localhost\/wsanomo\/public\/upload\/51125f11e2470bda79433cdb4a733437.jpg","Tags":[{"TagID":"28","Name":"tennis"},{"TagID":"34","Name":"software"},{"TagID":"35","Name":"football"}],"CheckinPlaceID":"","CheckinPlaceName":"","CheckinStatus":0,"NeighborhoodName":"Industry"},{"UserID":"87","UserName":"Cr","Gender":"1","BirthDate":"1998-08-08","NeighborhoodID":"2296","Avatar":"http:\/\/localhost\/wsanomo\/public\/upload\/a0bee7cd5c62343d84f0b2525a04f6b4.jpg","Tags":[{"TagID":"35","Name":"football"}],"CheckinPlaceID":"","CheckinPlaceName":"","CheckinStatus":0,"NeighborhoodName":"Industry"},{"UserID":"65","UserName":"Oh-my-god","Gender":"0","BirthDate":"1986-01-26","NeighborhoodID":"2296","Avatar":"http:\/\/localhost\/wsanomo\/public\/upload\/8a60ef4b40a7f0eff20992f6e77e0f10.jpg","Tags":[{"TagID":"7","Name":"sport"}],"CheckinPlaceID":"","CheckinPlaceName":"","CheckinStatus":0,"NeighborhoodName":"Industry"},{"UserID":"101","UserName":"soso","Gender":"1","BirthDate":"1999-08-17","NeighborhoodID":"2296","Avatar":"http:\/\/localhost\/wsanomo\/public\/upload\/b4b835245a4053e5658b100b1d5c8dce.jpg","Tags":[{"TagID":"76","Name":"it"}],"CheckinPlaceID":"","CheckinPlaceName":"","CheckinStatus":0,"NeighborhoodName":"Industry"}],"code":"OK"}</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>FAIL</code> - indicates that failed</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="ReturnListQuestion">Return list question (for game 4 people)</h2>
<p>After you get 4 peoples then you should get a list question to play group game. It random a list question</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/anomotion/random_question');?>/#token</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>

</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/anomotion/random_question');?>/IWPMXYTFVCVXVWYAPRB9</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">

 {"Question":[{"QuestionID":"1397","QuestionGroupID":"281","Content":"Have you ever been in a car crash?","QuestionType":"2","Order":"1","CreatedDate":"2013-09-16 10:53:51","ExpiredDate":null,"Answer":[{"AnswerID":"3118","Content":"Yes"},{"AnswerID":"3119","Content":"No"},{"AnswerID":"3120","Content":"Almost"}]},{"QuestionID":"1398","QuestionGroupID":"281","Content":"Favorite meal of the day?","QuestionType":"2","Order":"2","CreatedDate":"2013-09-16 10:53:51","ExpiredDate":null,"Answer":[{"AnswerID":"3121","Content":"Breakfast"},{"AnswerID":"3122","Content":"Lunch"},{"AnswerID":"3123","Content":"Dinner"},{"AnswerID":"3124","Content":"Snacks"}]},{"QuestionID":"1399","QuestionGroupID":"281","Content":"Have you ever been in a physical fight?","QuestionType":"2","Order":"3","CreatedDate":"2013-09-16 10:53:51","ExpiredDate":null,"Answer":[{"AnswerID":"3125","Content":"Yes"},{"AnswerID":"3126","Content":"No"}]},{"QuestionID":"1400","QuestionGroupID":"281","Content":"Biggest career accomplishment?","QuestionType":"2","Order":"4","CreatedDate":"2013-09-16 10:53:51","ExpiredDate":null,"Answer":[{"AnswerID":"3127","Content":"Landing a promotion"},{"AnswerID":"3128","Content":"Doing what I love"},{"AnswerID":"3129","Content":"Getting my degree"},{"AnswerID":"3130","Content":"Starting a company"}]},{"QuestionID":"1401","QuestionGroupID":"281","Content":"Would you shave your head if a friend had cancer and went bald?","QuestionType":"2","Order":"5","CreatedDate":"2013-09-16 10:53:51","ExpiredDate":null,"Answer":[{"AnswerID":"3131","Content":"Absolutely"},{"AnswerID":"3132","Content":"Hell no!"},{"AnswerID":"3133","Content":"Maybe"}]}],"code":"OK"}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>FAIL</code> - indicates that failed</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="CreateGame4People">Create game 4 people</h2>
<p>This is a web service lets you to push your answer to server. The GameResult must be in JSON format as below.</p>
<p class="note">Status: Active</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/anomotion/create_game_4people');?>/#token</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - is an opaque string that identifies a user. You can get it at <a href="#LoginWithUserNamePwd">Login</a></li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>GameResult</code> (<em>required</em>) - json format as below</li>
</ul>

<pre class="prettyprint"><span class="str">

{
"UserID":[1,2,3,4],
"Questions":[
    {"QuestionID":1,"AnswerID":2,"Order":1},
    {"QuestionID":2,"AnswerID":3,"Order":2},
    {"QuestionID":3,"AnswerID":4,"Order":3},
    {"QuestionID":4,"AnswerID":5,"Order":4},
    {"QuestionID":5,"AnswerID":6,"Order":5}
],
"QuestionGroupID":100,
"AnomotionID": -1
}
 
</span></pre>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/anomotion/create_game_4people/IWPMXYTFVCVXVWYAPRB9')?>">
	GameResult<input type="text" name="GameResult"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">

 {"code":"OK"}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing or invalid.</li>
  <li><code>INVALID_PARAMETER</code> - indicates that game result is invalid</li>
  <li><code>FINISHED</code> - indicates that game is finished</li>
  <li><code>FAIL</code> - indicates that failed or db error occurred</li>
</ul>



<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="CheckIsGamePlaying">[FB GAME]Check fb account is already have playing game with request user</h2>
<p class="warning">Status: Deprecated</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/fbanomotion/check_is_game_playing');?>/#token</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>FacebookIDs</code> (<em>required</em>) - list FacebookIDs separated with a comma symbol (FBID1,FBID2,...)</li>
</ul>


<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/fbanomotion/check_is_game_playing/IWPMXYTFVCVXVWYAPRB9')?>">
	FacebookIDs<input type="text" name="FacebookIDs"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">

{
    result: [
        {
            FacebookID: "100004452998527",
            FacebookName: "FB name 20",
            IsTargetRequest: "0"
        },
        {
            FacebookID: "123454",
            FacebookName: "FacebookName4",
            IsTargetRequest: "1"
        }
    ],
    code: "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_REQUESTs</code> - indicates that missing post params</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="ListFbGameInvited">[FB GAME] Return list fb game invited</h2>
<p class="warning">Status: Deprecated</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/fbanomotion/get_list_fb_invited');?>/#token/#page</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/fbanomotion/get_list_fb_invited');?>/IWPMXYTFVCVXVWYAPRB9</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str"><span>
{
    ListGame: [
        {
            AnomotionID: "3",
            UserID: "32854",
            FacebookName: "FacebookName3",
            FacebookID: "123453",
            CreatedDate: "2013-11-25 03:18:01"
        },
        {
            AnomotionID: "4",
            UserID: "3",
            FacebookName: "FB name 20",
            FacebookID: "100004452998527",
            CreatedDate: "2013-11-25 03:18:01"
        },
        {
            AnomotionID: "5",
            UserID: "2",
            FacebookName: "FacebookName4",
            FacebookID: "123454",
            CreatedDate: "2013-11-25 03:18:01"
        }
    ],
    CurrentPage: 1,
    TotalPage: 1,
    code: "OK"
}


</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
</ul>

<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="CreateFBGame">[FB GAME] Create FB game</h2>
<p class="warning">Status: Deprecated</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/fbanomotion/create_fbgame');?>/#token</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
</ul>

<p>The list of parameters on POST method and their possible values are enumerated below.</p>
<ul>
  <li><code>GameResult</code> (<em>required</em>) - json format as below</li>
</ul>

<pre class="prettyprint"><span class="str">


{"FBs":[{"FacebookID":"123451","FacebookName":"FacebookName1"},{"FacebookID":"123452","FacebookName":"FacebookName2"},{"FacebookID":"123453","FacebookName":"FacebookName3"},{"FacebookID":"123454","FacebookName":"FacebookName4"},{"FacebookID":"123455","FacebookName":"FacebookName5"}],"Questions":[{"QuestionID":1,"AnswerID":2,"Order":1},{"QuestionID":2,"AnswerID":3,"Order":2},{"QuestionID":3,"AnswerID":4,"Order":3},{"QuestionID":4,"AnswerID":5,"Order":4},{"QuestionID":5,"AnswerID":6,"Order":5}],"QuestionGroupID":100}
</span></pre>

<p>An example request is below:</p>
<pre class="prettyprint">
<form enctype="multipart/form-data" method="post" action="<?php echo site_url('webservice/fbanomotion/create_fbgame/IWPMXYTFVCVXVWYAPRB9')?>">
	GameResult<input type="text" name="GameResult"/>
	<input type="submit" value="Submit" class="button"/>
</form>
</pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str">

{
    "code": "OK"
}
</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
  <li><code>INVALID_PARAMETER</code> - indicates that param is invalid</li>
  <li><code>FAIL</code> - indicates that failed</li>
</ul>


<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

<h2 id="GetListUserPlayFBGame">[FB GAME] Return list user who played fb game</h2>
<p class="warning">Status: Deprecated</p>
<p>URL with the following form:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/fbanomotion/list_user_play_fbgame');?>/#token/#page</span></pre>

<p>Certain parameters are required. The list of parameters and their possible values are enumerated below.</p>
<ul>
  <li><code>token</code> (<em>required</em>) - ...</li>
</ul>

<p>An example request is below:</p>
<pre class="prettyprint"><span class="com"><?php echo site_url('webservice/fbanomotion/list_user_play_fbgame');?>/IWPMXYTFVCVXVWYAPRB9</span></pre>

<p>A JSON response example:</p>
<pre class="prettyprint"><span class="str"><span>
{
    CurrentPage: "1",
    TotalPage: 1,
    ListConversation: [
        {
            AnomotionID: "4",
            UserID: "32855",
            FacebookID: "123454",
            FacebookName: "FacebookName4",
            CreatedDate: "2013-11-20 08:44:15",
            IsGame: "1"
        },
        {
            AnomotionID: "5",
            UserID: "32856",
            FacebookID: "123455",
            FacebookName: "FacebookName5",
            CreatedDate: "2013-11-20 08:44:15",
            IsGame: "1"
        },
        {
            AnomotionID: "1",
            UserID: "32852",
            FacebookID: "123451",
            FacebookName: "FacebookName1",
            CreatedDate: "2013-11-20 08:44:15",
            IsGame: "1"
        }
    ],
    code: "OK"
}

</span></pre>

<p>A JSON response contains root elements:</p>
<ul>
  <li><code>"code"</code> contains metadata on the request. See <a href="#GetFinishAnomotionStatusCodes">Status Codes</a> below.</li>
</ul>

<h3 id="GetFinishAnomotionStatusCodes">Status Codes</h3>
<p>The <code>"code"</code> field may contain the following values:</p>
<ul>
  <li><code>OK</code> - indicates that no errors occurred; </li>
  <li><code>INVALID_TOKEN</code> - indicates that a token param is missing.</li>
</ul>