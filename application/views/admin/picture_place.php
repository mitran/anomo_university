<?php include '_header.php'; ?>
<script>
jQuery(function() {
    jQuery("img.lazy").lazyload();
});
</script>
<div id="content">
    <div id="main">
        <div class="highlight">
            <div class="lev4">

            </div>
            <h2>Pictures</h2>
        </div>
        <div class="highlight" style="text-align: left">
            <a class="active menu-picture" href="<?php echo base_url() ?>admin/picture/index">Place pictures</a> | 
            <a href="<?php echo base_url() ?>admin/picture/nbh_pics">Neighborhood pictures</a> | 
            <a href="<?php echo base_url() ?>admin/picture/status_pics">Status pictures</a> 
        </div>
        <br class="clear">
        <div class='color-red'><?php echo (isset($this->uri->vn_param['delete'])) ? $this->uri->vn_param['delete'] : '' ?></div>
        <div style='color:red'>Found <?php echo $total ?> result(s)</div>
        <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>

        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>


                <tr>
                    <th>
                        <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'date-asc' || $this->uri->vn_param['sort'] == 'date-desc')): ?>
                            <?php if ($this->uri->vn_param['sort'] == 'date-asc'): ?>	
                                <a href="<?php echo uri_sort('date-desc') ?>" class='color-red'>Created Date<img src="<?php echo base_url() ?>public/admin/images/s_desc.png" border="0"></a>
                            <?php else: ?>
                                <a href="<?php echo uri_sort('date-asc') ?>" class='color-red'>Created Date<img src="<?php echo base_url() ?>public/admin/images/s_asc.png" border="0"></a>
                            <?php endif; ?>
                        <?php else: ?>
                            <a href="<?php echo uri_sort('date-asc') ?>">Created Date</a>
                        <?php endif; ?>
                    </th>
                    <th>User Name</th>
                    <th>Place</th>
                    <th>Post</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>

                <?php if (count($results) > 0): ?>
                    <?php foreach ($results as $row): ?>
                        <tr>
                            <td><?php echo date('Y-m-d H:i:s', strtotime(PST.' hour', strtotime($row->CreatedDate))) ?></td>
                            <td><?php echo $row->UserName ?></td>
                            <td><?php echo $row->Name ?></td>
                            <td><?php echo $this->Dashboard_model->formatMsg($row->Content) ?></td>
                            <td>
                                <a href="<?php echo $row->Photo ?>" data-lightbox="<?php echo $row->FileName ?>"  >
                                    <img class="lazy" data-original="<?php echo $row->Photo200 ?>"  />
                                </a>
                            </td>

                            <td><a href="#" onclick="delete_content(<?php echo $row->ID ?>, 2, 'content_<?php echo $row->ID ?>_2')" id="content_<?php echo $row->ID ?>_2">Delete</a></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>

    </div>
    <br class="clear">
   <p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
</div>
<?php include '_footer.php'; ?>