

<?php include '_header.php'; ?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-ui-1.8.9.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery.datetimepicker.js"></script>
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/smoothness/jquery-ui-1.8.9.custom.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/add_datetimepicker.css" rel="stylesheet" type="text/css" media="screen">

<script>
$(document).ready(function() {
	$( "#expire_date" ).datepicker({dateFormat: "yy-mm-dd"});
});
</script>

<div id="content">
    <div id="main">
        <div class="highlight">
            <div class="lev4">
                <ul>
                    <li><a href="<?php echo base_url() ?>admin/announcement"><img src="<?php echo $this->config->item('base_url'); ?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">Back to list</a></li>
                </ul>
            </div>
            <h2>Posting Announcement</h2>
        </div>
        <div style="color:red">
            <?php echo validation_errors(); ?>
        </div>
        <?php echo form_open_multipart('admin/announcement/post/' . $id) ?>
        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
                <tr>
                    <td>Type <span class="color-red">(*)</span></td>
                    <td>
                        <?php 
                            $type = array('0' => 'Both', '1' => 'New Users', '2' => 'Old Users');
                            if ($id > 0){
                                $type = array('1' => 'New Users', '2' => 'Old Users');
                            }
                        ?>
                        <?php echo form_dropdown('type', $type, isset($this->validation->type) ? $this->validation->type : @$info->Type, "style='width:200px'") ?>
                    </td>
                </tr>
                <tr>
                    <td>Title <span class="color-red">(*)</span></td>
                    <td><input type="text" name="title" size=60 value="<?php echo isset($this->validation->title) ? $this->validation->title : @$info->Title ?>"></td>
                </tr>
                <tr>
                    <td>Content <span class="color-red">(*)</span></td>
                    <td>
                        <textarea rows="6" cols="10" name="content" style="width: 695px; height: 200px;"><?php echo isset($this->validation->content) ? $this->validation->content : @$info->Content; ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td>Ref Type <span class="color-red">(*)</span></td>
                    <td>
                        <?php 
                            $ref_type = array('1' => 'Message', '2' => 'Fan Page');                            
                        ?>
                        <?php echo form_dropdown('reftype', $ref_type, isset($this->validation->reftype) ? $this->validation->reftype : @$info->RefType, "style='width:200px'") ?>
                    </td>
                </tr>
                <tr>
                    <td>Ref ID</td>
                    <td><input type="text" name="refid" size=60 value="<?php echo isset($this->validation->refid) ? $this->validation->refid : @$info->RefID ?>"></td>
                </tr>
                <tr>
                    <td>Expire Date (PST Time)<span class="color-red">(*)</span></td>
                    <td><input type="text" name="expire_date" id="expire_date"  value="<?php echo isset($this->validation->expire_date) ? $this->validation->expire_date : @$info->ExpireDate ?>"></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input value="Submit" type="submit" name="submit" id="submit" > &nbsp <input value="Cancel" type="reset" ></td>
                </tr>
            </tbody>
        </table>
    </div>
    <br class="clear">
</div>
<?php include '_footer.php'; ?>