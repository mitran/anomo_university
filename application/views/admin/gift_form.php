<?php include '_header.php';?>
<div id="content">
    <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <li><a href="<?php echo base_url()?>admin/gift"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">Back to list</a></li>
              </ul>
            </div>
            <h2>Posting Gift</h2>
          </div>
         <div style="color:red">
         	<?php echo validation_errors(); ?>
         	<?php echo @$upload_error;?>
         	<?php echo @$upload_price_error;?>
         	<?php echo @$upload_194_error;?>
         	<?php echo @$upload_454_error;
             $http=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https://" : "http://";
            ?>
         </div>
         <?php echo form_open_multipart('admin/gift/post/'.$gift_id)?>
         <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <td>Name <span class="color-red">(*)</span></td>
                <td><input type="text" name="name" size=60 value="<?php echo isset($this->form_validation->name)?$this->form_validation->name:@$info->Name?>"></td>
              </tr>
			  <tr>
                <td>Photo<span class="color-red">(*)</span></td>
                <td>
                	<input type="file" name="photo" />
                	<?php if ($gift_id > 0):?>
                   <img src="<?php echo $this->config->item('s3_photo_url').$info->Photo?>" width="120" height="100" />
                    
                	<?php endif;?>
                </td>
              </tr>
              
              <tr>
                <td>Price Photo<span class="color-red">(*)</span></td>
                <td>
                	<input type="file" name="price_photo" />
                	<?php if ($gift_id > 0):?>
                    <img src="<?php echo $this->config->item('s3_photo_url').$info->PricePhoto?>" width="120" height="100" />
                	<?php endif;?>
                </td>
              </tr>
              
              <tr>
                <td>Photo194<span class="color-red">(*)</span></td>
                <td>
                	<input type="file" name="photo194" />
                	<?php if ($gift_id > 0):?>
                    
                    <img src="<?php echo $this->config->item('s3_photo_url').$info->Photo194?>" width="120" height="100" />
                	<?php endif;?>
                </td>
              </tr>
              
              <tr>
                <td>Photo454<span class="color-red">(*)</span></td>
                <td>
                	<input type="file" name="photo454" />
                	<?php if ($gift_id > 0):?>
                   
                    <img src="<?php echo $this->config->item('s3_photo_url').$info->Photo454?>" width="120" height="100" />
                	<?php endif;?>
                </td>
              </tr>
              
              <tr>
                <td>Gift category<span cl ass="color-red">(*)</span></td>
                <td>
                	<?php echo form_dropdown('category_id', $categories, isset($this->form_validation->category_id)?$this->form_validation->category_id:@$info->GiftCategoryID, "style='width:100px;'")?>
                </td>
              </tr>
              <tr>
                <td>Credits <span class="color-red">(*)</span></td>
                <td><input type="text" name="credits" size=60 value="<?php echo isset($this->form_validation->credits)?$this->form_validation->credits:@$info->Credits;?>"></td>
              </tr>
              <tr>
                <td>Description Of Sender</td>
                <td><input type="text" name="description" size=100 value="<?php echo isset($this->form_validation->description)?$this->form_validation->description:@$info->Description;?>"></td>
              </tr>
              <tr>
                <td>Description Of Receiver</td>
                <td><input type="text" name="description_on_buying" size=100 value="<?php echo isset($this->form_validation->description_on_buying)?$this->form_validation->description_on_buying:@$info->DescriptionOnBuying;?>"></td>
              </tr>
              <tr>
                <td colspan="2" align="center"><input value="Submit" type="submit" name="submit" id="submit" > &nbsp <input value="Cancel" type="reset" ></td>
             </tr>
			 </tbody>
          </table>
   </div>
   <br class="clear">
</div>
<?php include '_footer.php';?>