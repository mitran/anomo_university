<?php include '_header.php' ?>
<title>Q and A</title>

<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/theme/jquery.ui.theme.css" type="text/css"
      media="screen"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/theme/jquery-ui.css" type="text/css"
      media="screen"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/jquery.dataTables_themeroller.css"
      type="text/css" media="screen"/>

<script type="text/javascript" src="<?php echo base_url(); ?>public/admin/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/admin/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/admin/js/jquery.jeditable.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/admin/js/jquery.jeditable.datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/admin/js/jquery-ui.custom.min.js"></script>

<br>

<div class="wrapper">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#newqg').bind('change', function () {
                if ($(this).is(':checked')) {
                    $("#QGroupID").val("");
                    $("#QGroupID").prop('disabled', true);
                    $("#divQuestionCategory").show();
                    $("#divQuestionExpireDate").show();
                }
                else {
                    $("#QGroupID").prop('disabled', false);
                    $("#divQuestionCategory").hide();
                    $("#divQuestionExpireDate").hide();
                }
            });

            $("#QGroupExpireDate").datepicker({dateFormat: "yy-mm-dd"});

            $("button").button();
            var oTable = $('#big_table').DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bSortClasses": false,
                "sAjaxSource": '<?php echo base_url(); ?>index.php/admin/qanda/datatable',
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "iDisplayStart ": 20,
                "oLanguage": {
                    "sProcessing": "<img src='<?php echo base_url(); ?>public/admin/images/ajax-loader_dark.gif'>",
                },
                "aoColumns": [
                    { "mData": "QID", "sClass": "QuestionID", "bSearchable": false, "bVisible": false },
                    { "mData": "QGroupID", "sClass": "QuestionGroupID", "bSearchable": false, sDefaultContent: "n/a", "sWidth": "2%"},
                    { "mData": "QCategory", "sClass": "Category", "bSearchable": false, sDefaultContent: "n/a", "sWidth": "2%"},
                    { "mData": "QExpireDate", "sClass": "ExpireDate", "bSearchable": false, sDefaultContent: "n/a", "sWidth": "2%"},
                    { "mData": "Questions", "sClass": "Content", sDefaultContent: "n/a", "sWidth": "45%"},
                    { "mData": "QType", "sClass": "QuestionType", "bSearchable": false, sDefaultContent: "n/a", "sWidth": "1%"},
                    { "mData": "QOrder", "sClass": "Order", "bSearchable": false, sDefaultContent: "n/a", "sWidth": "1%"},
                    { "mData": "QWeight", "sClass": "Weight", "bSearchable": false, sDefaultContent: "n/a", "sWidth": "1%"},
                    { "mData": "AnswerID", "sClass": "AnswerID", "bSearchable": false, "bVisible": false},
                    { "mData": "Answers", "sClass": "Answers", sDefaultContent: "n/a", "sWidth": "70%"},
                    { "mData": "DT_RowId", "sClass": "center", "bSearchable": false, "bSortable": false, sDefaultContent: "n/a", "sWidth": "5%",

                        "mRender": function (data, type, full) {
                            return "<button class='delete' id='" + data + "'>Delete</button>";
                        }
                    }
                ],
                'fnServerData': function (sSource, aoData, fnCallback) {
                    $.ajax
                    ({
                        'dataType': 'json',
                        'type': 'POST',
                        'url': sSource,
                        'data': aoData,
                        'success': fnCallback
                    });
                },

                "fnDrawCallback": function (oSettings) {
                    // Initialize delete buttons
                    $("button.delete").button({
                        icons: { primary: "ui-icon-trash" }, text: false
                    });
                    // Initialize inplace editors
                    $("#big_table tbody td.QuestionGroupID, #big_table tbody td.Content,#big_table tbody td.QuestionType,#big_table tbody td.Order,#big_table tbody td.Weight,#big_table tbody td.Answers").editable(function (value, settings) {
                        var submitdata = {
                            "id": $(this).parent("tr").attr("id"),
                            "columnname": $(this).attr("class"),
                            "value": value
                        };

                        $.post("<?php echo base_url(); ?>index.php/admin/qanda/edit", submitdata);
                        return value;
                    }, {
                        "tooltip": "Click to edit"
                    });

                    // Initialize inplace editor for Category
                    $("#big_table tbody td.Category").editable(function (value, settings) {
                        var submitdata = {
                            "id": $(this).parent("tr").attr("id"),
                            "columnname": $(this).attr("class"),
                            "value": value
                        };

                        $.post("<?php echo base_url(); ?>index.php/admin/qanda/edit", submitdata, function () {
                            oTable.draw(false);
                        });

                        return value;
                    }, {
                        tooltip: "Click to edit",
                        data: "{'EVERYONE':'EVERYONE', 'MM':'MM', 'MF':'MF', 'FF':'FF', 'TEEN': 'TEEN'}",
                        type: "select",
                        submit: "Ok",
                    });

                    // Initialize inplace editor for Expire Date
                    $("#big_table tbody td.ExpireDate").editable(function (value, settings) {
                        var submitdata = {
                            "id": $(this).parent("tr").attr("id"),
                            "columnname": $(this).attr("class"),
                            "value": value
                        };

                        $.post("<?php echo base_url(); ?>index.php/admin/qanda/edit", submitdata, function () {
                            oTable.draw(false);
                        });

                        return value;
                    }, {
                        type: 'datepicker',
                        datepicker: {
                            dateFormat: 'yy-mm-dd',
                        }
                    });
                }
            });

            $(document).on("click", "button.delete", function (e) {
                alert("Feature Disabled! Don't delete stuffs, period")
                /*$.post("<?php echo base_url(); ?>index.php/admin/qanda/delete", { "id": $(this).attr("id") }, function () {
                 oTable.draw(false);
                 });*/
            });

            $("#formAddNewRow").submit(function (e) {
                e.preventDefault();
                var isFormValid = validateForm();
                $form = $(this);

                if (isFormValid) {
                    $.post("<?php echo base_url(); ?>index.php/admin/qanda/add", $form.serialize(),function (result) {
                        if (result == 'OK') {
                            oTable.draw(false);
                            $form[0].reset();
                            $('#newqg').removeAttr('checked'); // nipple... unchecked by default biatch ^,__,^
                            $("#QGroupID").prop('disabled', false);
                            $("#divQuestionCategory").hide();
                        } else
                            alert(data);
                    }).fail(function () {
                        alert("Error submitting request. Please make sure all data are correctly inserted");
                    });
                }
            });

            function validateForm() {
                // validate QGroupID correctness..
                if (!$('#newqg').is(':checked')) {
                    if ($("#QGroupID").val().trim() == "") {
                        alert("Question Group ID cannot be empty");
                        return false;
                    }
                }

                if ($("#QContent").val().trim() == "") {
                    alert("Question Content cannot be empty");
                    return false;
                }

                if ($("#AContent").val().trim() == "") {
                    alert("Answer Content cannot be empty");
                    return false;
                }

                return true;
            }
        });
    </script>
</div>

<table cellpadding="0" cellspacing="0" border="0" class="display" id="big_table">
    <thead>
    <tr>
        <th>QID</th>
        <th>QGroupID</th>
        <th>QCategory</th>
        <th>QExpireDate</th>
        <th>Questions</th>
        <th>QType</th>
        <th>QOrder</th>
        <th>QWeight</th>
        <th>AnswerID</th>
        <th>Answers</th>
        <th>&nbsp;</th>
    </tr>
    </thead>
</table>
<div class="spacer">&nbsp;</div>

<h><b>Add New Record</b></h><br/><br/>

<form id="formAddNewRow" action="#" title="Add new"">
<div>
    <label for="QGroupID">Question Group ID</label><br/>
    <input type="text" name="QGroupID" id="QGroupID" class="required" size="10"/> |
    <input type="checkbox" id="newqg" name="newqg" value="1"/>Create new Question Group
    <br/><br/>
</div>

<div id="divQuestionCategory" style="display: none">
    <label for="QGroupCategory">Question Group Category</label>
    <select name="QGroupCategory" id="QGroupCategory">
        <option value="EVERYONE" selected="selected">Everyone</option>
        <option value="MM">Male - Male</option>
        <option value="MF">Male - Female</option>
        <option value="FF">Female - Female</option>
        <option value="TEEN">Teen</option>
    </select>
    <br/><br/>
</div>

<div id="divQuestionExpireDate" style="display: none">
    <label for="QGroupExpireDate">Expire Date</label>
    <input id="QGroupExpireDate" name="QGroupExpireDate" value="" type="text" style="width:100px;">
    <br/><br/>
</div>
<div>
    <label for="QWeight">Weight</label><br/>
    <input type="text" name="QWeight" id="QWeight" class="required" size="100"/>
    <br/><br/>
</div>
<div>
    <label for="QContent">Question</label><br/>
    <input type="text" name="QContent" id="QContent" class="required" size="100"/>
    <br/><br/>
</div>

<div>
    <label for="AContent">Answers(seperated by comma)</label><br/>
    <input type="text" name="AContent" id="AContent" class="required" size="100"/>
    <br/><br/>
</div>

<button type="submit">Save Data</button>
</td>
</form>
<br/>

</body>
<br>

<?php include '_footer.php'; ?>
