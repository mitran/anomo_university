<?php include '_header.php'; ?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/message.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-ui-1.8.9.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery.datetimepicker.js"></script>

<link href="<?php echo $this->config->item('base_url');?>public/admin/css/smoothness/jquery-ui-1.8.9.custom.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/add_datetimepicker.css" rel="stylesheet" type="text/css" media="screen">

<script>
    $(document).ready(function() {
        $( "#expire_date" ).datepicker({dateFormat: "yy-mm-dd"});
    });
    
    function validate(){
        var message = $('#message').val();
        var expire_date = $('#expire_date').val();
        var photo = '';
        if ($("#photo").length > 0){
            photo = $('#photo').val();
        }

        if (! message.trim() && photo == "") {
            alert('Please input message or choose a picture.');
            return false;
        }else if (! expire_date.trim()){
            alert('Please input expire date.');
            return false;
        }else{
            $('#formMessage').submit();
        }
    }
</script>

<div id="content">
    <div id="main">
        <div class="highlight" style="text-align: left">
            <!--<a href="<?php echo base_url() ?>admin/message/send">Admin mass message</a> | -->
            <a  href="<?php echo base_url() ?>admin/vegas_msg/send">Admin mass message</a> | 
            <a class="active menu-picture" href="<?php echo base_url() ?>admin/auto_msg/send">Auto mass message</a> 
            
        </div>
        
        <div class="highlight">
            <div class="lev4">
                <ul>
                    <li><a href="<?php echo base_url() ?>admin/auto_msg/listmsg"><img src="<?php echo $this->config->item('base_url'); ?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">List Msg</a></li>
                </ul>
            </div>
            <h2>POSTING MESSAGE</h2>
        </div>
        <div style="color:red">
            <?php echo validation_errors(); ?>
            <?php echo $this->session->flashdata('msg'); ?>
            <?php echo @$upload_error;?>
        </div>
        <form action="<?php echo base_url() ?>admin/auto_msg/send/<?php echo $id?>" id="formMessage" enctype="multipart/form-data" method="post">
            <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
                <tbody>
                    <tr>
                        <td style="width: 200px">Message <span class="color-red">(*)</span></td>
                        <td>
                            <textarea name="message" id="message" cols="100" rows="10"><?php echo isset($this->form_validation->message) ? $this->form_validation->message : @$info->message?></textarea>
                        </td>
                    </tr>

                    <tr>
                        <td style="width: 200px">Photo </td>
                        <td>
                            
                            <input type="file" name="photo" id="photo" /> (Maximum: 1000KB)
                            <?php if ($id > 0 && @$info->photo): ?>
                                <img src="<?php echo $this->config->item('s3_photo_url') . $info->photo ?>" width="120" height="100" />
                            <?php endif; ?>
                        </td>
                    </tr>

                    <tr>
                        <td style="width: 200px">Location <span class="color-red">(*)</span></td>
                        <td>
                            <?php echo form_dropdown('promo_location_id', $promos_location, isset($this->form_validation->promo_location_id) ? $this->form_validation->promo_location_id : @$info->promo_location_id, "style='width:200px;'") ?>
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="width: 200px">Gender</td>
                        <td>
                            <?php $gender = array(
                                       ''   => 'All',
                                       1 => 'Male',
                                       2 => 'Female'
                                    );
                            ?>
                            <?php echo form_dropdown('gender', $gender, isset($this->form_validation->gender) ? $this->form_validation->gender : @$info->gender, "style='width:200px;'") ?>
                        
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 200px">Age</td>
                        <td>
                            From <input id="from_age" name="age_from" value="<?php echo isset($this->form_validation->age_from) ? $this->form_validation->age_from : @$info->age_from?>" type="text" style="width:100px;">
                            To <input id="to_age" name="age_to" value="<?php echo isset($this->form_validation->age_to) ? $this->form_validation->age_to : @$info->age_to?>" type="text" style="width:100px;">
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="width: 200px">Event Occurring</td>
                        <td>
                            Tap Action 
                            <?php $tap_action = array(
                                       ''   => '--- Select ---',
                                       1 => 'Tag Search',
    
                                    );
                            ?>
                            <?php echo form_dropdown('tap_action', $tap_action, isset($this->form_validation->tap_action) ? $this->form_validation->tap_action : @$info->tap_action, "style='width:200px;'") ?>
                            Tap Parameter <input id="tap_param" name="tap_param" value="<?php echo isset($this->form_validation->tap_param) ? $this->form_validation->tap_param : @$info->tap_param?>" type="text" style="width:300px;">
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="width: 200px">Expire Date<span class="color-red">(*)</span></td>
                        <td>
                            <input id="expire_date" name="expire_date" value="<?php echo isset($this->form_validation->expire_date) ? $this->form_validation->expire_date : @$info->expire_date?>" type="text" style="width:100px;">
                           
                        </td>
                    </tr>
                    <tr>
                        <td>Active</td>
                        <td><input type="checkbox" name="status" size=100 value="1" <?php echo ((isset($this->form_validation->status) && $this->form_validation->status == 1) || @$info->status == 1 )? "checked": ''?>></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input value="Submit" type="button" name="btn_submit" id="btn_submit" onclick="validate()" /> &nbsp;
                            <input value="Cancel" type="reset" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
    <br class="clear" />
</div>

<?php include '_footer.php'; ?>