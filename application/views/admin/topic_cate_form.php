<?php include '_header.php';?>
<div id="content">
    <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <li><a href="<?php echo base_url()?>admin/topic_category/index"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">Back to list</a></li>
              </ul>
            </div>
            <h2>Topic Category</h2>
          </div>
         <div style="color:red">
         	<?php echo validation_errors(); ?>
         	<?php echo @$upload_error;
            ?>
         </div>
         <?php echo form_open_multipart('admin/topic_category/add/'.$id)?>
        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
                <tr>
                    <td>Name <span class="color-red">(*)</span></td>
                    <td><input type="text" name="name" size=60 value="<?php echo isset($this->form_validation->name) ? $this->form_validation->name : @$info->CateName ?>"></td>
                </tr>
                <tr>
                    <td>Photo<span class="color-red">(*)</span></td>
                    <td>
                        <input type="file" name="photo" />
                        <?php if ($id > 0): ?>
                            <img src="<?php echo $this->config->item('s3_stream_url') . $info->Photo ?>" width="210" height="210" />

                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td>Desc <span class="color-red">(*)</span></td>
                    <td><input type="text" name="desc" size=60 value="<?php echo isset($this->form_validation->desc) ? $this->form_validation->desc : @$info->Desc ?>"></td>
                </tr>
                <tr>
                    <td>Allow create topic <span class="color-red">(*)</span></td>
                    <td><?php echo form_dropdown('allow_create_topic', array(1 => 'Yes', 0 => 'No'), isset($this->form_validation->allow_create_topic) ? $this->form_validation->allow_create_topic : @$info->AllowCreateTopic, "style='width:100px;'")?></td>
                </tr>
                <tr>
                    <td>Order <span class="color-red">(*)</span></td>
                    <td><input type="text" name="order" size=60 value="<?php echo isset($this->form_validation->order) ? $this->form_validation->order : @$info->OrderID ?>"></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input value="Submit" type="submit" name="submit" id="submit" > &nbsp <input value="Cancel" type="reset" ></td>
                </tr>
            </tbody>
        </table>
   </div>
   <br class="clear">
</div>
<?php include '_footer.php';?>