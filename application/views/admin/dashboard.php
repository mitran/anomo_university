<?php include '_header.php'; ?>
<div id="content">
    <div id="main">
        <div class="highlight">
            <h2>Dashboard</h2>
        </div>
        <br class="clear">
        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="80%">
            <tbody>
                <tr>
                    <th></th>
                    <th><b>Lifetime</b> <br />(All records)</th>
                    <th><b>Month</b> <br />(Current Month To Date)</th>
                    <th><b>Last 7 days</b></th>
                    <th><b>Day</b> <br />(Current Day (PST Time = GMT time - 7 hours))</th>
                </tr>
                <tr>
                    <td width="25%"><b>Total Users</b> </td>
                    <td><?php echo $resgister_all; ?></td>
                    <td><?php echo $resgister_month ?></td>
                    <td><?php echo $resgister_week ?></td>
                    <td><?php echo $resgister_day ?></td>
                </tr>

                <tr>
                    <td width="25%"><b>Verified Users</b> </td>
                    <td><?php echo $verified_all; ?></td>
                    <td><?php echo $verified_month ?></td>
                    <td><?php echo $verified_week ?></td>
                    <td><?php echo $verified_day ?></td>
                </tr>

                <tr>
                    <td width="25%"><b>Unverified Users</b> </td>
                    <td><?php echo $unverified_all; ?></td>
                    <td><?php echo $unverified_month ?></td>
                    <td><?php echo $unverified_week ?></td>
                    <td><?php echo $unverified_day ?></td> 
                </tr>

                <tr>
                    <td width="25%"><b>Ice Breaker Games</b> <br />(number of games played)</td>
                    <td><?php echo $game_all ?></td>
                    <td><?php echo $game_month ?></td>
                    <td><?php echo $game_week ?></td>
                    <td><?php echo $game_day ?></td>
                </tr>

                <tr>
                    <td width="25%"><b>Gifts Sent</b> <br />(total number of gifts sent)</td>
                    <td><?php echo $gift_sent_all ?></td>
                    <td><?php echo $gift_sent_month ?></td>
                    <td><?php echo $gift_sent_week ?></td>
                    <td><?php echo $gift_sent_day ?></td>
                </tr>
                <tr>
                    <td width="25%"><b>Reveals</b> <br />(# of times users revealed themselves)</td>
                    <td><?php echo $reveal_all ?></td>
                    <td><?php echo $reveal_month ?></td>
                    <td><?php echo $reveal_week ?></td>
                    <td><?php echo $reveal_day ?></td>
                </tr>

                <tr>
                    <td width="25%"><b>FB Shares</b> <br />(# of all FB share (including total shares of each user))</td>
                    <td><?php echo $share_all ?></td>
                    <td><?php echo $share_month ?></td>
                    <td><?php echo $share_week ?></td>
                    <td><?php echo $share_day ?></td>
                </tr>


                <tr>
                    <td width="25%"><b>FB Shares</b> <br />(# of FB share to get 1000pt)</td>
                    <td><?php echo $share_pt_all ?></td>
                    <td><?php echo $share_pt_month ?></td>
                    <td><?php echo $share_pt_week ?></td>
                    <td><?php echo $share_pt_day ?></td>
                </tr>

                <tr>
                    <td width="25%"><b>Blocks</b> <br />(# of blocked users)</td>
                    <td><?php echo $blocked_all ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td width="25%"><b>Status</b> <br />(# of status posted)</td>
                    <td><?php echo $total_status ?></td>
                    <td><?php echo $total_status_month ?></td>
                    <td><?php echo $total_status_week ?></td>
                    <td><?php echo $total_status_day ?></td>
                </tr>
                <tr>
                    <td width="25%"><b>Comments</b> <br />(# of comments posted)</td>
                    <td><?php echo $total_comment ?></td>
                    <td><?php echo $total_comment_month ?></td>
                    <td><?php echo $total_comment_week ?></td>
                    <td><?php echo $total_comment_day ?></td>
                </tr>
                <tr>
                    <td width="25%"><b>Likes</b> <br />(# of likes)</td>
                    <td><?php echo $total_likes ?></td>
                    <td><?php echo $total_likes_month ?></td>
                    <td><?php echo $total_likes_week ?></td>
                    <td><?php echo $total_likes_day ?></td>
                </tr>

                <tr>
                    <td width="25%"><b>Total Activities Count</b> <br />(in the last 6 hours)</td>
                    <td><?php echo $activity_6h ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="25%"><b>Total Activities Count</b> <br />(in the last 12 hours)</td>
                    <td><?php echo $activity_12h ?></td>
                    <td></td>
                    <td></td>
                    <td></td>

                </tr>
                <tr>
                    <td width="25%"><b>Total Activities Count</b> <br />(in the last 24 hours)</td>
                    <td><?php echo $activity_24h ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td width="25%"><b>Total Activities Count</b> <br />(in the last week)</td>
                    <td><?php echo $activity_1w ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="25%"><b>Activity Feeds Count</b> <br />(in the last 6 hours)</td>
                    <td><?php echo $activity_feeds_6h ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="25%"><b>Activity Feeds Count</b> <br />(in the last 12 hours)</td>
                    <td><?php echo $activity_feeds_12h ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="25%"><b>Activity Feeds Count</b> <br />(in the last 24 hours)</td>
                    <td><?php echo $activity_feeds_24h ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <tr>
                    <td width="25%"><b>Invite friend and get the 1000 free pts on the first time </td>
                    <td>
                        <?php if ($share_to_wall_first_time > 0): ?>
                            <a class="active" href="<?php echo base_url() ?>admin/user/p_share_to_wall/first_time" target="_blank"><?php echo $share_to_wall_first_time ?></a>
                        <?php else: ?>
                            <?php echo $share_to_wall_first_time ?> 
                        <?php endif; ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <tr>
                    <td width="25%"><b>Users clicked on "Share to Wall" popup for the 1000 free pts after 3 days</td>
                    <td>
                         <?php if ($share_to_wall_3days > 0): ?>
                            <a class="active" href="<?php echo base_url() ?>admin/user/p_share_to_wall/3days" target="_blank"><?php echo $share_to_wall_3days ?></a>
                        <?php else: ?>
                            <?php echo $share_to_wall_3days ?> 
                        <?php endif; ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                
                <tr>
                    <td width="25%"><b>Number of friend are invited</td>
                    <td><?php echo $number_friend_invite ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="25%"><b>Vegas count</td>
                    <td><?php echo $vegas_count ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                
            </tbody>
        </table>
    </div>
    <br class="clear">
</div>

<?php include '_footer.php'; ?>
