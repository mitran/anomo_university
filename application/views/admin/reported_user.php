<?php include '_header.php'; ?>

<script type="text/javascript"
	src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.9.0.js"></script>
<link
	href="<?php echo $this->config->item('base_url'); ?>public/admin/css/screen.css"
	rel="stylesheet" type="text/css">

<script>

$(document).ready(function() {

	function scrollToElement(selector, verticalOffset) {
	    verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
	    element = $(selector);
	    offset = element.offset();
	    offsetTop = offset.top + verticalOffset;
	    $('html, body').animate({
	        scrollTop: offsetTop
	    });
	}

	
	$("table.tblAdminDetail tr td a.forgiverow").click(function() {

    
        var clickedID = $(this).attr('id').split('_');
        var DbNumberID = clickedID[2];
        var ToUserID = clickedID[3];        
        var IsForgiven = clickedID[4];
        var currentOffset = clickedID[5];
        var myData = 'recordToUpdate='+ ToUserID;   
        var url = '<?php echo base_url() ?>admin/reportban_user/forgive_user/'+ToUserID+'/'+IsForgiven+'/'+currentOffset;       
        var row = $("#"+'row_'+DbNumberID);
        scrollToElement("#"+'row_'+DbNumberID,-200);
		
        $.ajax({
           'type': "POST",
           'url': url,
           'dataType':"text",
           'data':myData, 
           beforeSend : function (){
           	$('.spinner').show();
           }, 
           success:function(response){
//        	   row.find("td").remove();         	      	   	
           },
           error:function (xhr, ajaxOptions, thrownError){
               alert(thrownError);
           },
	        complete: function(){
	        	$('.spinner').hide();
	        	row.find("td.tick").append("<img src='<?php echo base_url(); ?>public/admin/images/tick.jpg'/>");
	        }	
         });

	});
	

	$("table.tblAdminDetail tr td a.banrow").click(function() {


        var clickedID = $(this).attr('id').split('_');
        var DbNumberID = clickedID[2];
        var ToUserID = clickedID[3]; 
        var IsBanned = clickedID[4];
        var currentOffset = clickedID[5];
        var myData = 'recordToDeleteUpdate='+ ToUserID;   
        var url =  '<?php echo base_url() ?>admin/reportban_user/ban_reported_user/'+ToUserID+'/'+IsBanned+'/'+currentOffset;            
		var row = $("#"+'row_'+DbNumberID);
		scrollToElement("#"+'row_'+DbNumberID,-200);		
        $.ajax({
           'type': "POST",
           'url': url,         
           'dataType':"text",
           'data': myData, 
           beforeSend : function (){
           	$('.spinner').show();
           }, 
           success:function(response){
//        	   row.find("td").remove();                  
           },
           error:function (xhr, ajaxOptions, thrownError){
               alert(thrownError);
           },
	        complete: function(){
	        	$('.spinner').hide();
	        	row.find("td.tick").append("<img src='<?php echo base_url(); ?>public/admin/images/tick.jpg'/>");
	        }	
         });

	});
		
});
</script>

<div id="content">
    <div id="main">
        <div class="highlight">
        	<h2>Reporting and Banning</h2>
        </div>
        <br class="clear">
        <div class="highlight" style="text-align: left">        
                <a 
				href="<?php echo base_url() ?>admin/reportban_user/reported_content">Reported Content
				</a> | 
				<a class="active menu-picture"
				href="<?php echo base_url() ?>admin/reportban_user/reported_user">Reported Users</a> |
				<a href="<?php echo base_url() ?>admin/reportban_user/exclude_reporting_users">Exclude Reporting Users</a> |
                                <a  href="<?php echo base_url().'admin/deleted_content/index/' ?>">Deleted Content</a> 
        </div>
        <div id = "data"></div>				
        <br class="clear">
        <div class='color-red'><?php echo (isset($this->uri->vn_param['delete'])) ? 'Delete successful' : '' ?></div>
        <div style='color:red'>Found <?php echo $total ?> result(s)</div>
        <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>
			
            <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
                <tbody>
                    <tr>
                    	<th><div style="width: 75px;"><b>Time</b></div></th>
                        <th><div style="width: 125px;"><b>ReportingUsers</b></div></th>
                        <th><div style="width: 50px;"><b>Times Reported</b></div></th>   
                        <th><div style="width: 100px;"><b>ReportedUser</b></div></th>
                        <th><div style="width: 150px;"><b>Comments</b></div></th>                        
                        <th><div style="width: 45px;"><b>Action</b></div></th>
                        <th><div style="width: 25px;"><b>Review</b></div></th>
                    </tr>
					<?php $tz = new DateTimeZone('America/Los_Angeles'); ?>
                    <?php if (count($results) > 0): ?>
                        <?php foreach ($results as $row): ?>
                        	<?php if (is_null($row->IsForgiven)): ?>
                            <tr id="row_<?php echo $row->ID ?>">
                            	<td><div style="width: 75px;"><b>
                            	<?php $date = new DateTime($row->CreatedDate);
						  				$date->setTimeZone($tz);
						  				echo $date->format('Y-m-d H:i:s'); ?></b></td>                                                         
                                <td><div style="width: 125px;">
                                	<?php $reportingusers = split(":", $row->ReportingUsers);?>	                        		
	                        		<?php if (count($reportingusers) != 1):?>	 
	                        			<ul>                       	                        	
	                        			<?php foreach ($reportingusers as $reportinguser):?>		                        				
	                        				<?php if (strlen($reportinguser) > 15):?>                        			
	                        					<li><?php echo '-> '. substr($reportinguser,0,15). '....'?></li>
	                        				<?php else:?>
	                        					<li><?php echo '-> '. $reportinguser?></li>	
	                        				<?php endif;?>                        		
	                        			<?php endforeach ?>
	                        			</ul>
	                        		<?php else: ?>
	                        			<?php if (strlen($reportingusers[0]) > 15):?>  
	                        				<?php echo substr($reportingusers[0],0,15). '....'?>
	                        			<?php else:
	                        				echo $reportingusers[0];?>
	                        			<?php endif;?>
	                        		<?php endif;?>                        		
	                        	</td> 	
                                <td><div style="width: 50px;"><?php echo $row->c_users ?></td>
                                <td><div style="width: 100px;">         		
	                        	<a href="<?php echo base_url() ?>admin/chat_history/index/<?php echo $row->ToUserID ?>"><?php echo $row->ReportedUser ?></a>                       		
                      			</td>
                                <td><div style="width: 150px;">
                                	<?php $reportingcontents = split(":", $row->Contents);?>
	                        		<?php if (count($reportingcontents) != 1):?>
	                        		<ul>
	                        			<?php foreach ($reportingcontents as $reportingcontent):?>	                        			                        			
	                        				<li><font color="red"><?php echo $reportingcontent ?></font></li> 					
	                        			<?php endforeach ?>
	                        		</ul>
	                        		<?php else: ?>
	                        		<font color="red"><?php echo $reportingcontents[0]?></font>
	                        		<?php endif;?>
	                        	<br>
	                        	</td>                                                               
                                <td><div style="width: 45px;">
                                <?php if ($row->IsBanned != '1'): ?>    
                                		<a class = "banrow" href="#" id="user_ban_<?php echo $row->ID . '_' . $row->ToUserID . '_' . $row->IsBanned . '_' . $current_offset?>">
								  			<?php echo ($row->IsBanned == 0) ? 'Ban' : 'UnBan' ?> 
								  		</a>								  		
										<a class = "forgiverow" href="#" id="user_forgive_<?php echo $row->ID . '_' . $row->ToUserID . '_' . 'null' . '_' . $current_offset ?>">
								  			Forgive</a>
								    	</a>   
								 <?php else: ?>						        	 
	                            		Ban | Forgive </div>    								    	                         
                            	<?php endif; ?>	                            					    							  	
                            	</td>   
                            	<td class = "tick">                            	
	                            	<div style="display: none"></div>
								</td>                                                  
                            </tr>
	                        
	                        <div class="spinner">
								<img class="img-spinner" src='<?php echo base_url(); ?>public/admin/images/ajax-loader.gif'
								id="img-spinner" alt="Loading" />
							</div>
							<?php endif; ?>		                            
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
            
    </div>
    <br class="clear">
    <p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
</div>
<?php include '_footer.php'; ?>