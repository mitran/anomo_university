<?php include '_header.php';?>
<div id="content">
      <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <!--<li><a href=""><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_09.gif" alt="" border="0" height="39" width="32">Delete</a></li>-->
                <li><a href="<?php echo base_url()?>admin/promo_code/post"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_13.gif" alt="" border="0" height="39" width="32">Create</a></li>
              </ul>
            </div>
            <h2>PromoCode</h2>
          </div>
          <br class="clear">
          <div class='color-red'><?php echo (isset($this->uri->vn_param['delete']))?$this->uri->vn_param['delete']:''?></div>
          <div style='color:red'>Found <?php echo  $total ?> result(s)</div>
          <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param)?></div>
          <form action="<?php echo base_url()?>admin/promo_code/index" id="fm_filter_promo_code" name="fm_filter_promo_code" method="post">
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
             <tr>
                <td colspan="11" width="5%">
				Status: <?php echo form_dropdown('status', array('' => '--- select ---', 0 => 'Inactive', 1 => 'Active'), isset($this->uri->vn_param['status'])?$this->uri->vn_param['status']:'', "id='status' style='width:100px;'")?>
				<input value="Filter" type="submit" name='btn_submit' id="btn_submit" onclick="filter_promo_code()">
				<input value="Cancel" type="reset" name='cancel_list' id="cancel_list" onclick="javascript:window.location='<?php echo base_url()?>admin/gift/index'">
                </td>
              </tr>
              
              <tr>
                <th scope="col" width="5%">
				 <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'id-asc' || $this->uri->vn_param['sort'] == 'id-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'id-asc'):?>	
                		<a href="<?php echo uri_sort('id-desc')?>" class='color-red'>ID<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('id-asc')?>" class='color-red'>ID<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('id-asc')?>">ID</a>
                <?php endif;?>
				</th>
               
                <th scope="col" width="15%">
                <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'code-asc' || $this->uri->vn_param['sort'] == 'code-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'credits-asc'):?>	
                		<a href="<?php echo uri_sort('code-desc')?>" class='color-red'>Code<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('code-asc')?>" class='color-red'>Code<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('code-asc')?>">Code</a>
                <?php endif;?>
                </th>
                
                 <th scope="col" width="15%">
                <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'name-asc' || $this->uri->vn_param['sort'] == 'name-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'name-asc'):?>	
                		<a href="<?php echo uri_sort('name-desc')?>" class='color-red'>Name<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('name-asc')?>" class='color-red'>Name<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('name-asc')?>">Name</a>
                <?php endif;?>
                </th>
                 <th scope="col" width="15%">
                <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'point-asc' || $this->uri->vn_param['sort'] == 'point-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'point-asc'):?>	
                		<a href="<?php echo uri_sort('point-desc')?>" class='color-red'>Point<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('point-asc')?>" class='color-red'>Point<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('point-asc')?>">Point</a>
                <?php endif;?>
                </th>
                <th scope="col" width="">Status</th>
                 <th scope="col" width="15%">
                <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'created-asc' || $this->uri->vn_param['sort'] == 'created-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'created-asc'):?>	
                		<a href="<?php echo uri_sort('created-desc')?>" class='color-red'>Created Date<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('created-asc')?>" class='color-red'>Created Date<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('created-asc')?>">Created Date</a>
                <?php endif;?>
                </th>
                <th scope="col" width="">Action</th>
              </tr>
            <?php if (count($results) > 0):?>
			<?php foreach ($results as $row): ?>
              <tr>
                <td><?php echo $row->ID?></td>
                <td><?php echo $row->Code?></td>
                <td><?php echo $row->Name?></td>
                <td><?php echo $row->Point?></td>
                <td>
                	<?php $status = array(0 => 'Inactive', 1 => 'Active');
                	 echo $status[$row->Status]?>
                </td>
                <td><?php echo $row->CreatedDate?></td>
                <td>
                	<a href="<?php echo base_url()?>admin/promo_code/post/<?php echo $row->ID?>">edit </a> | 
                	<a id="promo_<?php echo $row->ID?>" href="#" onclick="delete_promo_code(<?php echo $row->ID?>)">delete </a>
                </td>
              </tr>
            <?php endforeach;?>
            <?php endif;?>
            </tbody>
          </table>
          </form>
      </div>
      <br class="clear">
    </div>
<?php include '_footer.php';?>