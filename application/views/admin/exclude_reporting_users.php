<?php include '_header.php'; ?>
<script type="text/javascript"
	src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.9.0.js"></script>
<script>
	$(document).ready(function() {

	$("#submit").click(function() {
		var url =  '<?php echo base_url() ?>admin/reportban_user/exclude_reporting_users/';
		var excludeUser = $("#excludeUser").val();
        $.ajax({
        	'type': "POST", 
	        'url': url,
            'data': $("#excludeUser").val(), 
           success:function(data){
        	   var container = $('#data');
				container.append(data);      		                
           }
         });        
	});
	});
</script>	

<div id="content">
    <div id="main">
        <div class="highlight">
        	<h2>Reporting and Banning</h2>
        </div>
        <br class="clear">
        <div class="highlight" style="text-align: left">          
        		<a href="<?php echo base_url() ?>admin/reportban_user/reported_content">Reported Content
				</a> | 
				<a href="<?php echo base_url() ?>admin/reportban_user/reported_user">Reported Users</a> |
				<a class="active menu-picture" href="<?php echo base_url() ?>admin/reportban_user/exclude_reporting_users">Exclude Reporting Users</a> |
				<a  href="<?php echo base_url().'admin/deleted_content/index/' ?>">Deleted Content</a>  
        </div>
        <br class="clear">
        <form id = "addNew" method = "post" 
        <b>Add User to exclude</b><br>
        <input type="text" name = "excludeUser" id="excludeUser" />
        <br>
        <input id="submit" class="submit" value="submit" type="submit" style="padding: 1000x 50px;"/>
        </form>
        <br><br>
        <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>
            <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
                <tbody>
                    <tr>
                    	<th><div style="width: 150px;"><b>Users</b></div></th>
                    </tr>
                    <?php foreach ($usernames as $row): ?> 
                    <tr>
                    	<td><div style="width: 150px;">                                        
						<b><?php echo $row->UserNames; ?></b><br></td>	
					</tr>
					<?php endforeach;?>					
				</tbody>
			</table>
		</div>
	</div>
	<p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
	<div id="data"></div>	
<?php include '_footer.php'; ?>
                    
