<?php include '_header.php'; ?>

<script>
jQuery(function() {
    jQuery("img.lazy").lazyload();
});
</script>
<div id="content">
    <div id="main">
        <div class="highlight">
            <h2>Users</h2>
        </div>
        <br class="clear">
        <div style='color:red'>Found <?php echo $total ?> result(s)</div>
        <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>
        <form action="<?php echo base_url() ?>admin/user/new_user" id="fm_filter_user" name="fm_filter_user" method="post">
            <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
                <tbody>
                    <tr>
                        <td colspan="16" width="5%">
                            Keyword: <input name="keyword" value="<?php echo isset($keyword) ? htmlspecialchars($keyword) : '' ?>" type="text" id="keyword">
                            <input value="Filter" type="submit" name='btn_submit' id="btn_submit">
                            <input value="Cancel" type="reset" name='cancel_list' id="cancel_list" onclick="javascript:window.location = '<?php echo base_url() ?>admin/user/new_user'">
                        </td>
                    </tr>
                    <?php if (count($results) > 0): ?>
                    <tr style="font-size:11px;font-weight:bold;">
                        <th width="5%">UserID
                        </th>

                        <th width="10%">UserName</th>
                        <th width="5%">Avatar</th>
                        <th width="20%">Email</th>
                        <th width="20%">PromoCode</th>
                        <th width="10%">Signup Date</th>
                        <th width="5%">IsEmailVerify</th>
                        <th >FacebookID</th>
                        <!--
                        <th width="3%">VerifyType</th>
                        <th width="5%">SMS Send Status</th>
                        <th width="5%">Phone</th>
                        <th width="5%">VerifiedDate</th>
                        -->
                        <th width="5%">DailyMatch</th>
                    </tr>
                        <?php foreach ($results as $row): ?>
                            <tr>
                                <td><?php echo $row->UserID ?></td>
                                <td><div style="width:150px;"><?php echo $row->UserName ?></div></td>
                                <td width="5%">
                                    <img class="lazy" data-original="<?php echo $this->config->item('s3_photo_url').$row->Photo ?>" width="46px" heigh="100px"/>
                                </td>
                                <td ><div style="width:150px;"><?php echo $row->Email ?></div></td>
                                <td ><?php echo $row->PromoCode ?></td>
                                <td  style="font-size:11px;"><?php echo date('Y-m-d H:i:s', strtotime(PST.' hour', strtotime($row->SignUpDate))) ?></td>
                                <td ><?php echo $row->IsEmailVerify == 1? 'Yes': 'No' ?></td>
                                <td><?php echo $row->FacebookID ?></td>
                                <!--
                                <td width="5%">
                                    <?php
                                        if ($row->FacebookID > 0){
                                            echo 'Facebook';
                                        }elseif($row->VerifiedPhone){
                                            echo 'SMS';
                                        }
                                    ?>
                                </td>
                                <td >
                                    <?php
                                        if ($row->SendStatus == 1){
                                            echo 'Success';
                                        }elseif($row->VerifiedPhone && $row->SendStatus == 0){
                                            echo 'Fail';
                                        }
                                    ?>
                                </td>
                                <td width="10%"><?php echo $row->VerifiedPhone ?></td>
                                <td width="10%" style="font-size:11px;"><?php echo $row->VerifiedDate ?></td>
                                -->
                                <td><?php if ( $row->DailyMatch > 0):?>
                                    <a class="active" href="<?php echo base_url().'admin/user/daily_match/'.$row->UserID?>" target="_blank"><?php echo $row->DailyMatch?></a>
                                    <?php else:?>
                                        <?php echo $row->DailyMatch?>
                                    <?php endif;?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </form>
    </div>
    <br class="clear">
    <p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
</div>
<?php include '_footer.php'; ?>