<?php include '_header.php';?>
    <div id="content">
      <form action="<?php echo base_url()?>admin/index/profile" method="post" id="siteinformation" enctype="multipart/form-data">
        <div id="main">
          <div class="highlight">
            <h2>Your Profile</h2>
          </div>
          <br class="clear">
          <div style="color:red">
         	<?php echo isset($this->validation->error_string)?$this->validation->error_string:''; ?>
         	<p><?php echo @$pass_error?></p>
         	<p><?php echo (isset($this->uri->vn_param['update']) && $this->uri->vn_param['update'] == 'success')?'Your profile updated':''?></p>
         </div>
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="80%">
            <tbody>
              <tr>
                <td width="25%">Username:</td>
                <td width=""><input name="user_name" size="50" value="<?php echo $admin->UserName?>" type="text" disabled>
                </td>
              </tr>
              <tr>
                <td width="25%">Nickname:</td>
                <td width=""><input name="nick_name" size="50" value="<?php echo isset($this->validation->nick_name)?htmlspecialchars($this->validation->nick_name):htmlspecialchars($admin->NickName)?>" type="text">
                </td>
              </tr>
              <tr>
                <td width="25%">Email:</td>
                <td width=""><input name="email" size="50" value="<?php echo isset($this->validation->email)?$this->validation->email:$admin->Email?>" type="text">
                </td>
              </tr>
              <tr>
                <td width="25%">New password:</td>
                <td width=""><input name="new_password" size="50" value="<?php echo isset($this->validation->new_password)?$this->validation->new_password:''?>" type="password">
                </td>
              </tr>
              <tr>
                <td width="25%">Re-enter new password:</td>
                <td width=""><input name="re_password" size="50" value="<?php echo isset($this->validation->re_password)?$this->validation->re_password:''?>" type="password">
                </td>
              </tr>
              <tr>
                <td colspan="2"><input value="Save" name="save" id="save" type="submit">
                  <input value="Cancel" type="reset">
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </form>
      <br class="clear">
    </div>

<?php include '_footer.php';?>
