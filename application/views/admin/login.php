<?php include '_header.php';?>
    <div id="content">
      <form class="frmLogin" name="form1" method="post" action="<?php echo base_url()?>admin/index/login/<?php echo @$redirect_uri?>">
        <h2>Login</h2>
        	<div style="color:red">
        		<?php echo isset($this->validation->error_string)?$this->validation->error_string:''; ?>
        		<p><?php echo @$login_error;?></p>
    	 	</div>
        <p>
          <label>Username</label>
          <input name="user_name" size="20" type="text" value="<?php echo isset($this->validation->user_name)?$this->validation->user_name:''?>">
        </p>
        <p>
          <label>Password</label>
          <input name="password" size="20" type="password" value="<?php echo isset($this->validation->password)?$this->validation->password:''?>">
          <input value="" name="submit" class="btnSignIn" type="submit">
        </p>
        <p><a href="<?php echo base_url()?>admin/index/forgot_password">Forgot password?</a></p>
      </form>
      <br class="clear">
    </div>
<?php include '_footer.php';?>
