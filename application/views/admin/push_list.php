<?php include '_header.php';?>
<div id="content">
      <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <!--<li><a href=""><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_09.gif" alt="" border="0" height="39" width="32">Delete</a></li>-->
                <li><a href="<?php echo base_url()?>admin/credit/post"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_13.gif" alt="" border="0" height="39" width="32">Create</a></li>
              </ul>
            </div>
            <h2>Push Notification</h2>
          </div>
          <br class="clear">
          <div class='color-red'><?php echo (isset($this->uri->vn_param['delete']))?$this->uri->vn_param['delete']:''?></div>
          <div style='color:red'>Found <?php echo  $total ?> result(s)</div>
          <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param)?></div>
          <form action="<?php echo base_url()?>admin/gift/index" id="fm_filter_credit" name="fm_filter_credit" method="post">
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <th scope="col" width="40%">
				 <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'date-asc' || $this->uri->vn_param['sort'] == 'date-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'date-asc'):?>	
                		<a href="<?php echo uri_sort('date-desc')?>" class='color-red'>Date<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('date-asc')?>" class='color-red'>Date<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('date-asc')?>">Date</a>
                <?php endif;?>
				</th>
                <th scope="col" width="">Platform</th>
                 <th scope="col" width="30%">
				 <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'total-asc' || $this->uri->vn_param['sort'] == 'total-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'total-asc'):?>	
                		<a href="<?php echo uri_sort('total-desc')?>" class='color-red'>Total<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('total-asc')?>" class='color-red'>Total<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('total-asc')?>">Total</a>
                <?php endif;?>
				</th>
              </tr>
            <?php if (count($results) > 0):?>
			<?php foreach ($results as $row): ?>
              <tr>
             	<td><?php echo date('Y-m-d', strtotime($row->CreatedDate))?></td>
                <td><?php echo $row->Platform?></td>
                <td><?php echo $row->Number?></td>
              </tr>
            <?php endforeach;?>
            <?php endif;?>
            </tbody>
          </table>
          </form>
      </div>
      <br class="clear">
    </div>
<?php include '_footer.php';?>