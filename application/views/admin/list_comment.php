<?php include '_header.php';?>

<div id="content">
      <div id="main">
          <div class="highlight" style="text-align: left">            
            <a href="<?php echo base_url() ?>admin/promos" class="active menu-picture">Promos Post</a> | 
            <a  href="<?php echo base_url() ?>admin/promos/fanpage">Fan's Posts</a> 
          </div>   
          <div class="highlight">
            <div class="lev4">
              <ul>
                <li><a href="<?php echo base_url() ?>admin/promos"><img src="<?php echo $this->config->item('base_url'); ?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">Back to list</a></li>
              </ul>
            </div>
            <h2>Promos comment</h2>
          </div>
          <br class="clear">
          
          <?php if (isset($info)): ?>
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <th scope="col" width="5%">ID</th>
                <th scope="col" width="">Vendor</th>
                <th scope="col" width="15%">Content</th>
                <th scope="col" width="">Photo</th>
                <th scope="col" width="">Link</th>
                <th scope="col" width="">Published</th>
              </tr>
            
              <tr>
                <td><?php echo $info->ID?></td>
                <td><?php echo $info->UserName?></td>
                <td><?php echo $info->Content?></td>
                <td>
                    <?php if ($info->Photo):?>
                    <img src="<?php echo $this->config->item('s3_photo_url').$info->Photo;?>" style="width:80px; height:80px;"/>
                    <?php endif; ?>
                </td>
                <td><?php echo $info->Link?></td>
                <td><?php echo $info->IsPublished == 1? 'Yes': 'No'?></td>
              </tr>
            </tbody>
          </table>
          <br class="clear">
          <h3>Reply comment</h3>
          <form action="<?php echo base_url()?>admin/promos/post_comment/<?php echo $info->ID?>" id="fm_post_comment" method="post" >
              <div id="post_comment">
                  <span style="font-weight:bold;"><?php echo $info->UserName?> </span>
                  <span><textarea rows="8" cols="90" name="txt_comment" id="txt_comment"></textarea></span> <span class="color-red">(*)</span>
                  <span><input type="button" id="btn_comment" value="Post" onclick="post_comment()"></span>
              </div>
          </form> 
          <?php if ($list_comment):?>
           <br class="clear">
          <h3>Comments </h3>
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <th scope="col" width="5%">UserID</th>
                <th scope="col" width="">UserName</th>
                <th scope="col" width="45%">Content</th>
                <th scope="col" width="5%">NumberOfLike</th>
                <th scope="col" width="">CreatedDate</th>
              </tr>
             <?php foreach($list_comment as $comment):?>
              <tr>
                <td><?php echo $comment->UserID?></td>
                <td><?php echo $comment->UserName?></td>
                <td><?php echo htmlspecialchars($comment->Content)?></td>
                <td><?php echo $comment->NumberOfLike?></td>
                <td><?php echo $comment->CreatedDate?></td>
              </tr>
              <?php endforeach;?>
            </tbody>
          </table>
          
          <?php endif; ?>
          
       <?php endif; ?>
      </div>
      <br class="clear">
    </div>
<?php include '_footer.php';?>