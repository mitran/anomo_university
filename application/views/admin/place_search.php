<?php include '_header.php';?>
<div id="content">
  <div id="main">
    <div class="highlight"><h2>Factual Search</h2></div>
    <br class="clear">
    <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
      <tbody>
        <tr>
          <td>
            <form action="<?php echo base_url()?>admin/place" id="place_search" name="place_search" method="post">
              Latitude: <input name="lat" value="<?php echo isset($lat)?$lat:'';?>" type="text" style="width:100px;">
              Longitude: <input name="lng" value="<?php echo isset($lng)?$lng:'';?>" type="text" style="width:100px;">
              Category: <?php echo form_dropdown('category', $optionCategories, isset($category)?$category:'', "style='width:350px;'")?>
              Keyword: <input name="keyword" value="<?php echo isset($keyword)?$keyword:'';?>" type="text">
              <input value="Search" type="submit" name='btn_submit' id="btn_submit">
              <input value="Cancel" type="reset" name='cancel_list' id="cancel_list">
            </form>
          </td>
        </tr>
        <?php if (isset($results)) {?>
        <tr>
          <td><pre><?php print_r($results);?></pre></td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
  <br class="clear">
</div>
<?php include '_footer.php';?>