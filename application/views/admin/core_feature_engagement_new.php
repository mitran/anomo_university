<?php include '_header.php'; ?>
<script type="text/javascript"
	src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.9.0.js"></script>

<link
	href="<?php echo $this->config->item('base_url'); ?>public/admin/css/screen.css"
	rel="stylesheet" type="text/css">

<script>
	var link1 = "<?php echo base_url() ?>admin/marketing/core_feature_engagement_days";
	var link2 = "<?php echo base_url() ?>admin/marketing/core_feature_engagement_currentweek";

	var flagd = 1;var flagw = 1;var flag_days; var flag_week;
	
	function load_data_ajax_days(){
		$('.spinner').show();
        flagd += 1;	
/*    	function keep_alive() {
        	http_request = new XMLHttpRequest();
        	http_request.open('GET', link1);        	
        	http_request.send(null);
    	};
    	setInterval(keep_alive,10000);	
*/    	
    	function keep_alive(){
	        $.ajax({
	        	'type' : 'GET',
	        	'data' : {'type' : 'days'},
	           url: link2,
	           cache: false,
	           success: function(data){
	           	var container = $('#data');        	
	          		container.append(data);
	          		$('#data').load('core_feature_engagement_days');  	          		  		
	           },
	        });

		};
		    	
//		setInterval(keep_alive,10000);	
		
    	$.ajax({
        	'url' : link1,
            'type' : 'POST', //the way you want to send data to your URL
            'data' : {'type' : 'days'}, 
            'async' : 'true',                       
            beforeSend : function (){
            	$('.spinner').show();
            },               
            success: function(data){ 
            	var container = $('#data');
				container.append(data);	
				$('#data').load('core_feature_engagement_days'); 	
	        },
	        complete: function(){
		        flag_days = 0;flagd = 1;
		        getflag();
	        }	      
        });
		if(flagd >= 1){
			$(".submitBtn1").attr("disabled", true);
		}
	}
	function load_data_ajax_currentweek(){
		$('.spinner').show();	
        flagw += 1;

/*        function keep_alive() {
        	http_request = new XMLHttpRequest();
        	http_request.open('GET', link2);
        	http_request.send(null);
    	};

    	setInterval(keep_alive,10000);	     
*/
    	function keep_alive(){
	        $.ajax({
	        	'type' : 'GET',
	        	'data' : {'type' : 'currentweek'},
	           url: link2,
	           cache: false,
	           success: function(data){
	           	var container = $('#data');        	
	          		container.append(data);
	          		$('#data').load('core_feature_engagement_currentweek');  	          		  		
	           },
	        });

		};
//		setInterval(keep_alive,10000);	  
		 	
		$.ajax({
        'url' : link2,
        'type' : 'POST', //the way you want to send data to your URL
        'data' : {'type' : 'currentweek'},
        'async' : 'true', 
        beforeSend : function (){
        	$('.spinner').show();

        },
        success: function(data){
        	var container = $('#data');        	
       		container.append(data);
       		$('#data').load('core_feature_engagement_currentweek');       		
        },
        complete: function(){
	        flag_week = 0;flagw = 1;
	        getflag();
	        keep_alive();
        }	       
        });
		if(flagw >= 1){
        	$(".submitBtn2").attr("disabled", true);
		}
	}
    function getflag(){
    	if((flag_days == 0) && (flag_week == 0)){
    		$('.spinner').hide();
    	}
    	else if((flag_days != 0) && (flag_week == 0)){
        	if(flagd > 1){
    		$('.spinner').show();
        	}
        	else{
        	$('.spinner').hide();
        	}	
    	}
    	else if((flag_days == 0) && (flag_week != 0)){
        	if(flagw > 1){
        	$('.spinner').show();
            }
            else{
            $('.spinner').hide();
            }
    	}  	    	  	
    }        
</script>
<br>
<div class="spinner">
	<img class="img-spinner"
		src='<?php echo base_url(); ?>public/admin/images/ajax-loader.gif'
		id="img-spinner" alt="Loading" />
</div>

<input onclick="load_data_ajax_days();" id="submitBtn1" type="submit" class = "submitBtn1" value="Get last 7 days"/>
<input onclick="load_data_ajax_currentweek();" id="submitBtn2" type="submit" class = "submitBtn2" value="Current week"/>

<div id="data"></div>

<br>
<h3>LAST UPDATED WEEKLY RESULTS</h3>
<div style="height: 10px;"></div>
<?php if ($weekdata):?>
<?php echo "Last updated  " ?>
<?php echo $weekdata[0]['cdate'];?> 
<?php echo " (PST)  " ?>
<?php endif;?>
<br>
<table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1"
	width="80%">
	<tbody>
		<tr>
			<th><b>WeekLastUpdated</b></th>
			<th><b>#Posts</b></th>
			<th><b>#Comments</b></th>
			<th><b>#Likes</b></th>
			<th><b>#Followers Added</b></th>
			<th><b>#Ice Breakers</b></th>
			<th><b>#Chat Messages</b></th>
			<th><b>#Reveals</b></th>
			</tr>
        
        <?php foreach ($weekdata as $row):?>
        <tr>
			<td width="25%"><?php echo DATE("Y-m-d",strtotime($row['sunday']))?></td>
			<td><?php echo $row['posts']?></td>
			<td><?php echo $row['acomments'] + $row['pcomments'] + $row['ncomments']?></td>
			<td><?php echo $row['likes']?></td>
			<td><?php echo $row['followers_added']?></td>
			<td><?php echo $row['ice_breakers']?></td>
			<td><?php echo $row['chat_messages']?></td>
			<td><?php echo $row['reveals']?></td>
		</tr>
        <?php endforeach;?>                 
	</tbody>
</table>
<br>


<br>
<h3>LAST UPDATED DAILY STATS</h3>
<div style="height: 10px;"></div>
<?php if ($daysdata):?>
<?php echo "Last updated  " ?>
<?php echo date('Y-m-d',strtotime($daysdata[0]['cdate']));?> 
<?php echo "0:00:00";?> 
<?php echo " (PST)  " ?>
<?php endif;?>
<br>
<table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1"
	width="80%">
	<tbody>
		<tr>
			<th><b>Date</b></th>
			<th><b>#Posts</b></th>
			<th><b>#Comments</b></th>
			<th><b>#Likes</b></th>
			<th><b>#Followers Added</b></th>
			<th><b>#Ice Breakers</b></th>
			<th><b>#Chat Messages</b></th>
			<th><b>#Reveals</b></th>
		</tr>
        
        <?php foreach ($daysdata as $row):?>
        <tr>
			<td width="25%"><?php echo DATE("Y-m-d",strtotime($row['cdate']))?></td>
			<td><?php echo $row['posts']?></td>
			<td><?php echo $row['acomments'] + $row['pcomments'] + $row['ncomments']?></td>
			<td><?php echo $row['likes']?></td>
			<td><?php echo $row['followers_added']?></td>
			<td><?php echo $row['ice_breakers']?></td>
			<td><?php echo $row['chat_messages']?></td>
			<td><?php echo $row['reveals']?></td>
		</tr>
        <?php endforeach;?>                 
	</tbody>
</table>
<br>


<br>
<h3>LAST 26 WEEKS</h3>
<div style="height: 10px;"></div>


<table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1"
	width="80%">
	<tbody>
		<tr>
			<th><b>WeekEndingOn</b></th>
			<th><b>#Posts</b></th>
			<th><b>#Comments</b></th>
			<th><b>#Likes</b></th>
			<th><b>#Followers Added</b></th>
			<th><b>#Ice Breakers</b></th>
			<th><b>#Chat Messages</b></th>
			<th><b>#Reveals</b></th>
		</tr>
        
        <?php foreach ($result as $row):?>
        <tr>
			<td width="25%"><?php echo $row['Week']?></td>
			<td><?php echo $row['Posts']?></td>
			<td><?php echo $row['Comments']?></td>
			<td><?php echo $row['Likes']?></td>
			<td><?php echo $row['FollowersAdded']?></td>
			<td><?php echo $row['IceBreakers']?></td>
			<td><?php echo $row['ChatMessages']?></td>
			<td><?php echo $row['Reveals']?></td>
		</tr>
        <?php endforeach;?>                 
	</tbody>
</table>
<br>


<?php include '_footer.php'; ?>

