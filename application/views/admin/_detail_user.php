
<?php if ($row):?>

<script>
    var user_id = "<?php echo $row->UserID?>";
    jQuery('.notes').editable(base_url+'admin/user/save_notes/'+user_id, {
         type       : 'textarea',
         cancel     :  'Cancel',
         submit     : 'OK',
         tooltip    : 'Click to edit...',
         id         : 'elementid',
         name       : 'newvalue'
     });
</script>
<table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
    <tr>
        <td class="bold" width="15%">UserID</td>
        <td  width="15%"><?php echo $row->UserID ?></td>
    </tr>
    <tr>
        <td class="bold" width="15%">UserName</td>
        <td width="15%"><?php echo $row->UserName ?></td>
    </tr>
    <tr>
        <td class="bold"width="15%">BirthDate</td>
        <td  width="15%"><?php echo $row->BirthDate ?></td>
    </tr>
    <tr>
        <td class="bold" width="15%">SignUpDate</td>
        <td  width="15%"><?php echo $row->SignUpDate ?></td>
    </tr>
    <tr>
        <td class="bold" width="15%">#Post</td>
        <td  width="15%"><?php echo $row->NumberPost + $row->NumberPostArchive ?></td>
    </tr>
    <tr>
        <td class="bold" width="15%">#Comment</td>
        <td  width="15%"><?php echo $row->NumberComment ?></td>
    </tr>
    <tr>
        <td class="bold" width="15%">#Follower</td>
        <td  width="15%"><?php echo $row->NumberFollower ?></td>
    </tr>
    <tr>
        <td class="bold" width="15%">#Icebreakers games played </td>
        <td scope="col" width="15%"><?php echo $row->NumberGame ?></td>
    </tr>
    <tr>
        <td class="bold" width="15%">Chat history</td>
        <td scope="col" width="15%"><a target="_blank" href="<?php echo base_url() . 'admin/chat_history/index/'. $row->UserID?>">Click here</a></td>
    </tr>
    <tr>
        <td class="bold" width="15%">Notes</td>
        <td scope="col" width="15%">
            <div class="notes" id="notes" style="width:400px; height: 100px;">
            <?php echo $row->Notes ?>
            </div>
        </td>
    </tr>
</table>
<?php endif; ?>
