<?php include '_header.php';?>

<script>
jQuery(document).ready(function() {
	jQuery( "#datepicker" ).datepicker({ dateFormat: "yy-mm-dd" });
	jQuery( "#isForever" ).click( function(){
		if( jQuery(this).is(':checked') )
			jQuery( "#datepicker" ).hide();
		else
			jQuery( "#datepicker" ).show();
	});
});
</script>
<div id="content">
    <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <li><a href="<?php echo base_url()?>admin/question"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">Back to list</a></li>
              </ul>
            </div>
            <h2>Posting Question</h2>
          </div>
         <div style="color:red"><?php echo validation_errors(); ?></div>
         <?php echo form_open_multipart('admin/question/post/'.$questionID)?>
         <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <?php if ($questionID==0) {?>
              <tr>
                <td>Question Group<span class="color-red">(*)</span></td>
                <td>
                	<?php echo form_dropdown('groupID', $groups, isset($this->form_validation->groupID)?$this->form_validation->groupID:@$info->QuestionGroupID, "style='width:100px;'")?>
                	<input value="Add Group" type="button" name="add_group" onclick="addQuestionGroup();">
                </td>
              </tr>
              <?php }?>
              <tr>
                <td>Content <span class="color-red">(*)</span></td>
                <td>
                <textarea rows="6" cols="10" name="content" style="width: 695px; height: 200px;"><?php echo isset($this->form_validation->content)?$this->form_validation->content:@$info->Content;?></textarea>
                </td>
              </tr>
              <tr>
                <td>Question Type<span class="color-red">(*)</span></td>
                <td>
                	<?php echo form_dropdown('questionType', array('1'=>'Single', '2'=>'Multiple'), isset($this->form_validation->questionType)?$this->form_validation->questionType:@$info->QuestionType, "style='width:100px;'")?>
                </td>
              </tr>
              <!-- 
              <tr>
                <td>Expired Date<span class="color-red">(*)</span></td>
                <td>
                	<input type="checkbox" id="isForever" name="isForever" value="true" <?php echo (@$info && @$info->ExpiredDate)?'':'checked';?>>Forever
                	<input type="text" id="datepicker" name="expiredDate" value="<?php echo (@$info && @$info->ExpiredDate);?>" <?php echo (@$info && @$info->ExpiredDate)?'':'style="display:none;"';?>>
                </td>
              </tr>
               -->
            </tbody>
          </table>
          <br/>
          <br/>
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <?php $answer_row=1;foreach ($answers as $item) { ?>
              <tr id="answer_tr_<?php echo $answer_row;?>">
                <td valign="top">Answer <?php echo $answer_row;?></td>
                <td>
                  <textarea rows="3" cols="10" name="answer[<?php echo $answer_row;?>][Content]" style="width: 695px; height: 200px;"><?php echo isset($item->Content)?$item->Content:$item['Content'];?></textarea>
                  <input type="hidden" name="answer[<?php echo $answer_row;?>][AnswerID]" value="<?php echo isset($item->AnswerID)?$item->AnswerID:$item['AnswerID'];?>">
                  <?php if (!isset($item->AnswerID) && $item['AnswerID']=='') {?>
                  <a href="javascript:" onclick="jQuery('#answer_tr_<?php echo $answer_row;?>').remove();"><span style="font-weight: bold;">Remove</span></a>
                  <?php }?>
                </td>
              </tr>
              <?php $answer_row++;}?>
              <tr id="answer_add">
                <td colspan="3"><a href="javascript:" onclick="addAnswer();"><span style="font-weight: bold;">Add Answer</span></a></td>
              </tr>
            </tbody>
          </table>
          <br/>
          <br/>
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <td align="center"><input value="Submit" type="submit" name="submit" id="submit" > &nbsp <input value="Cancel" type="reset" ></td>
              </tr>
            </tbody>
          </table>
        <?php echo form_close();?>  
   </div>
   <br class="clear">
</div>
<script>
var answer_row = <?php echo $answer_row;?>;

function addQuestionGroup() {
	jQuery.post(base_url+'admin/question/add_question_group/', {}, function(data){
		window.location.reload();
	});
}
function addAnswer() {
	if (jQuery('input[type=hidden]').length == 5) {
		alert('Maximum number of answer is 5!');
		return;
	}
	var html  = '';
    html += '<tr id="answer_tr_' + answer_row + '">'; 
    html += '<td valign="top">Answer ' + answer_row + '</td>';
    html += '<td><textarea rows="3" cols="10" name="answer[' + answer_row + '][Content]" style="width: 695px; height: 200px;"></textarea><input type="hidden" name="answer[' + answer_row + '][AnswerID]" value="">';
    html += '<a href="javascript:" onclick="jQuery(\'#answer_tr_' + answer_row + '\').remove();"><span style="font-weight: bold;">Remove</span></a></td>';
	html += '</tr>';
	
    jQuery('#answer_add').before(html);
	
    answer_row++;
}
</script>
<?php include '_footer.php';?>