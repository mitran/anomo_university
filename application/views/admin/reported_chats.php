<?php include '_header.php'; ?>

<script type="text/javascript"
	src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.9.0.js"></script>


	
<div id="content">
    <div id="main">
        <div class="highlight">
        	<h2>Chat conversations</h2>
        </div>
        <br class="clear">
		<link href="<?php echo $this->config->item('base_url'); ?>public/admin/css/verticalnavmenu.css" rel="stylesheet" type="text/css" media="screen">
 
		<script>
			$(document).ready(function() {
	    		$("#toc li a").click(function() {
	 
        	$.ajax({ 
		        	'url': this.href,
		        	'type' : 'POST',
		        	'data' : {'type' : 'chat'},	
		        	'async' : 'true',	        		       
		        	success: function(data) {
	            		$("#content_wrap").empty().append(data);
	            	}
	    		});
	    			return false;
	    		});
			});
       
		</script>

		<div id="main_wrap">
		<div id="content_wrap"></div>
		<div id = "sidebar_wrap" ><b>Users</b>
        	<?php if (count($chatUsers) > 0): ?>						
				<ul id = "toc" class="scroll">
					<?php $flag = 0;?>
					<?php $sendusername = $this->reported_content_model->get_username($SendUserID);?>				
					<?php if($chatUsers):?>
      					<?php foreach ($chatUsers as $chatUser):?>       				   						
    						<li><a href="<?php echo base_url() ?>admin/reportban_user/reported_chats_info/<?php echo $SendUserID?>/<?php echo $chatUser->ReceiveUserID?>">  				
	    					<?php $receiveusername = $this->reported_content_model->get_username($chatUser->ReceiveUserID);?>
	    					<?php if ($sendusername[0]->UserName != $receiveusername[0]->UserName):?>
	    						<?php echo $receiveusername[0]->UserName;?>
	    						<?php $flag = 1;?>
							<?php endif;?>
    						</a></li>
    					<?php endforeach;?>  
    				<?php if(!$flag):?>	
	    				<?php echo "No chat users!"?>
	    			<?php endif;?>
    				<?php endif;?>	
				</ul>
			
    		<?php else:?>
    			<br>
    			<?php echo "No chat conversations yet!"?>
			<?php endif;?>	
		</div>
		</div>
	</div>
    <br class="clear">
    <p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>

</div>
<?php include '_footer.php'; ?>                   