<?php include '_header.php'; ?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-ui-1.8.9.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery.datetimepicker.js"></script>
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/smoothness/jquery-ui-1.8.9.custom.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/add_datetimepicker.css" rel="stylesheet" type="text/css" media="screen">
<script>
$(document).ready(function() {
	$( "#start_date_contest_winner" ).datepicker({dateFormat: "yy-mm-dd"});
        $( "#end_date_contest_winner" ).datepicker({dateFormat: "yy-mm-dd"});
});
</script>
<div id="content">
    <div id="main">
        <div class="highlight">
            <div class="lev4">
                <ul>
                </ul>
            </div>
            <h2>Config</h2>
        </div>
        <div style="color:red">
            <?php echo (isset($this->uri->vn_param['update'])) ? $this->uri->vn_param['update'] : ''?>
            <?php echo validation_errors(); ?>
        </div>
        <?php echo form_open_multipart('admin/config/index')?>
        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
            <tr>
                <td>Radius default (meter)<span class="color-red">(*)</span></td>
                <td><input type="text" name="radius" size=60
                           value="<?php echo isset($this->form_validation->radius) ? $this->form_validation->radius : @$config['SEARCH_RADIUS']?>">
                </td>
            </tr>
            <tr>
                <td>Radius with search keyword (meter)<span class="color-red">(*)</span></td>
                <td><input type="text" name="radius_keyword" size=60
                           value="<?php echo isset($this->form_validation->radius_keyword) ? $this->form_validation->radius_keyword : @$config['SEARCH_RADIUS_KEYWORD']?>">
                </td>
            </tr>
            <tr>
                <td>Validation timeline (hour) <span class="color-red">(*)</span></td>
                <td><input type="text" name="validation_timeline" size=60
                           value="<?php echo isset($this->form_validation->validation_timeline) ? $this->form_validation->validation_timeline : @$config['VALIDATION_TIMELINE']?>">
                </td>
            </tr>
            <tr>
                <td>Email validation template<span class="color-red">(*)</span></td>
                <td>
                    <textarea rows="6" cols="10" name="email_validate_template" style="width: 695px; height: 200px;">
                        <?php echo isset($this->form_validation->email_validate_template) ? $this->form_validation->email_validate_template : @$config['EMAIL_VALIDATE_TEMPLATE']?>
                    </textarea>
                </td>
            </tr>

            <tr>
                <td>Remind validation timeline (hour) <span class="color-red">(*)</span></td>
                <td><input type="text" name="remind_validation_timeline" size=60
                           value="<?php echo isset($this->form_validation->remind_validation_timeline) ? $this->form_validation->remind_validation_timeline : @$config['REMIND_VALIDATION_TIMELINE']?>">
                </td>
            </tr>
            <tr>
                <td>Remind email validation template<span class="color-red">(*)</span></td>
                <td>
                    <textarea rows="6" cols="10" name="remind_email_validate_template"
                              style="width: 695px; height: 200px;">
                        <?php echo isset($this->form_validation->remind_email_validate_template) ? $this->form_validation->remind_email_validate_template : @$config['REMIND_EMAIL_VALIDATE_TEMPLATE']?>
                    </textarea>
                </td>
            </tr>

            <tr>
                <td>Search service provider <span class="color-red">(*)</span></td>
                <td><?php echo form_dropdown('search_service_provider', array('factual' => 'Factual', 'google' => 'Google'), isset($this->form_validation->search_service_provider) ? $this->form_validation->search_service_provider : @$config['SEARCH_SERVICE_PROVIDER'], "style='width:100px;'")?></td>
            </tr>

            <tr>
                <td>Rankby (Google search)<span class="color-red">(*)</span></td>
                <td>
                    <input type="radio" name="rankby"
                           value="distance" <?php echo (@$config['RANKBY'] == 'distance') ? "checked=true" : ""?>>
                    distance
                    <input type="radio" name="rankby"
                           value="prominence" <?php echo (@$config['RANKBY'] == 'prominence') ? "checked=true" : ""?>>
                    prominence
                </td>
            </tr>

            <tr>
                <td>Number of return venue (for searching) <span class="color-red">(*)</span></td>
                <td><input type="text" name="number_venue" size=60
                           value="<?php echo isset($this->form_validation->number_venue) ? $this->form_validation->number_venue : @$config['NUMBER_VENUE']?>">
                </td>
            </tr>

            <tr>
                <td>Push notification mode <span class="color-red">(*)</span></td>
                <td><?php echo form_dropdown('push_notification_mode', array('sandbox' => 'Sandbox', 'production' => 'Production'), isset($this->form_validation->push_notification_mode) ? $this->form_validation->push_notification_mode : @$config['PUSH_NOTIFICATION_MODE'], "style='width:100px;'")?></td>
            </tr>

            <tr>
                <td>List of notification history return within ... (day) <span class="color-red">(*)</span></td>
                <td><input type="text" name="notification_history" size=60
                           value="<?php echo isset($this->form_validation->notification_history) ? $this->form_validation->notification_history : @$config['NOTIFICATION_HISTORY_RETURN_WITHIN']?>">
                </td>
            </tr>

            <tr>
                <td>Number mark limit <span class="color-red">(*)</span></td>
                <td><input type="text" name="number_mark_limit" size=60
                           value="<?php echo isset($this->form_validation->number_mark_limit) ? $this->form_validation->number_mark_limit : @$config['NUMBER_MARK_LIMIT']?>">
                </td>
            </tr>

            <tr>
                <td>Number AnomoPick <span class="color-red">(*)</span></td>
                <td><input type="text" name="number_anomo_pick" size=60
                           value="<?php echo isset($this->form_validation->number_anomo_pick) ? $this->form_validation->number_anomo_pick : @$config['NUMBER_ANOMO_PICK']?>">
                </td>
            </tr>

            <tr>
                <td>Time for user try using app without fbID (hour) <span class="color-red">(*)</span></td>
                <td><input type="text" name="time_use_without_fb" size=60
                           value="<?php echo isset($this->form_validation->time_use_without_fb) ? $this->form_validation->time_use_without_fb : @$config['TIME_USE_WITHOUT_FB']?>">
                </td>
            </tr>

            <tr>
                <td>Activity Feed <span class="color-red">(*)</span></td>
                <td><?php echo form_dropdown('activity_feed', array('on' => 'On', 'off' => 'Off'), isset($this->form_validation->activity_feed) ? $this->form_validation->activity_feed : @$config['ACTIVITY_FEED'], "style='width:100px;'")?></td>
            </tr>

            <tr>
                <td>Nearby Search Radius Set (miles) <span class="color-red">(*)</span></td>
                <td><input type="text" name="search_radius_set" size=60
                           value="<?php echo isset($this->form_validation->search_radius_set) ? $this->form_validation->search_radius_set : @$config['SEARCH_RADIUS_SET']?>">
                </td>
            </tr>

            <tr>
                <td>Nearby Search Cutoff Value (activities) <span class="color-red">(*)</span></td>
                <td><input type="text" name="search_cutoff_val" size=60
                           value="<?php echo isset($this->form_validation->search_cutoff_val) ? $this->form_validation->search_cutoff_val : @$config['SEARCH_CUTOFF_VAL']?>">
                </td>
            </tr>

            <tr>
                <td>Nearby Search Time Constraint (hours) <span class="color-red">(*)</span></td>
                <td><input type="text" name="search_time_limit" size=60
                           value="<?php echo isset($this->form_validation->search_time_limit) ? $this->form_validation->search_time_limit : @$config['SEARCH_TIME_LIMIT']?>">
                </td>
            </tr>
             <tr>
                <td>Check in Radius Set (miles) <span class="color-red">(*)</span></td>
                <td><input type="text" name="checkin_radius_set" size=60
                           value="<?php echo isset($this->form_validation->checkin_radius_set) ? $this->validation->checkin_radius_set : @$config['CHECKIN_RADIUS_SET']?>">
                </td>
            </tr>
            <tr>
                <td>Username send message<span class="color-red">(*)</span></td>
                <td><input type="text" name="username_send_msg" size=60
                           value="<?php echo isset($this->form_validation->username_send_msg) ? $this->form_validation->username_send_msg : @$config['USERNAME_SEND_MSG']?>">
                </td>
            </tr>
			<!--<tr>
                <td>Filter nearby (find people) (mile) <span class="color-red">(*)</span></td>
                <td><input type="text" name="filter_distance" size=60
                           value="<?php echo isset($this->form_validation->filter_distance) ? $this->form_validation->filter_distance : @$config['FILTER_DISTANCE']?>">
                </td>
            </tr>
            -->
            
            <tr>
                <td>Time for show popup share FB (hour)<span class="color-red">(*)</span></td>
                <td><input type="text" name="time_popup_share_fb" size=60
                           value="<?php echo isset($this->form_validation->time_popup_share_fb) ? $this->form_validation->time_popup_share_fb : @$config['TIME_POPUP_SHARE_FB']?>">
                </td>
            </tr>
            <tr>
                <td>Time for show popup convert account (hour)<span class="color-red">(*)</span></td>
                <td><input type="text" name="time_popup_convert_account" size=60
                           value="<?php echo isset($this->form_validation->time_popup_convert_account) ? $this->form_validation->time_popup_convert_account : @$config['TIME_POPUP_CONVERT_ACCOUNT']?>">
                </td>
            </tr>
            
            <tr>
                <td>New User Activity card <span class="color-red">(*)</span></td>
                <td><?php echo form_dropdown('new_user_activity_card', array('on' => 'On', 'off' => 'Off'), isset($this->form_validation->new_user_activity_card) ? $this->form_validation->new_user_activity_card : @$config['NEW_USER_ACTIVITY_CARD'], "style='width:100px;'")?></td>
            </tr>

            <tr>
                <td>Cache time out period (seconds)<span class="color-red">(*)</span></td>
                <td><input type="text" name="cache_time_out" size=60
                           value="<?php echo isset($this->form_validation->cache_time_out) ? $this->form_validation->cache_time_out : @$config['CACHE_TIME_OUT']?>">
                </td>
            </tr>

            <tr>
                <td>Number of pages to cache for activity stream (seconds)<span class="color-red">(*)</span></td>
                <td><input type="text" name="cache_page_count" size=60
                           value="<?php echo isset($this->form_validation->cache_page_count) ? $this->form_validation->cache_page_count : @$config['CACHE_PAGE_COUNT']?>">
                </td>
            </tr>
            
            <tr>
                <td>Cache trending card time out (second)<span class="color-red">(*)</span></td>
                <td><input type="text" name="cache_trending_card_time_out" size=60
                           value="<?php echo isset($this->form_validation->cache_trending_card_time_out) ? $this->form_validation->cache_trending_card_time_out : @$config['CACHE_TRENDING_CARD_TIME_OUT']?>">
                </td>
            </tr>
            
            <tr>
                <td>Time to show trending card again (hour)<span class="color-red">(*)</span></td>
                <td><input type="text" name="time_show_trending_card_again" size=60
                           value="<?php echo isset($this->form_validation->time_show_trending_card_again) ? $this->form_validation->time_show_trending_card_again : @$config['TIME_SHOW_TRENDING_CARD_AGAIN']?>">
                </td>
            </tr>

            <tr>
                <td>Trending tag search interval (hour)<span class="color-red">(*)</span></td>
                <td><input type="text" name="trending_tag_interval" size=60
                           value="<?php echo isset($this->form_validation->trending_tag_interval) ? $this->form_validation->trending_tag_interval : @$config['TRENDING_TAG_INTERVAL']?>">
                </td>
            </tr>
            
            <tr>
                <td>Admin email addresses (list email separated with a comma symbol)<span class="color-red">(*)</span></td>
                <td><input type="text" name="admin_email" size=60
                           value="<?php echo isset($this->form_validation->admin_email) ? $this->form_validation->admin_email : @$config['ADMIN_EMAIL']?>">
                </td>
            </tr>
            
            <tr>
                <td>Turn ON/OFF #vegas on trending hashtag <span class="color-red">(*)</span></td>
                <td><?php echo form_dropdown('turnon_vegas_hashtag', array('on' => 'On', 'off' => 'Off'), isset($this->form_validation->turnon_vegas_hashtag) ? $this->form_validation->turnon_vegas_hashtag : @$config['TURNON_VEGAS_HASHTAG'], "style='width:100px;'")?></td>
            </tr>
            
            <tr>
                <td>Vegas <span class="color-red">(*)</span></td>
                <td>
                    Lat <input type="text" name="vegas_lat" size=20
                           value="<?php echo isset($this->form_validation->vegas_lat) ? $this->form_validation->vegas_lat : @$config['VEGAS_LAT']?>">
                    Lon <input type="text" name="vegas_lon" size=20
                           value="<?php echo isset($this->form_validation->vegas_lon) ? $this->form_validation->vegas_lon : @$config['VEGAS_LON']?>">
                    Radius <input type="text" name="vegas_radius" size=20
                           value="<?php echo isset($this->form_validation->vegas_radius) ? $this->form_validation->vegas_radius : @$config['VEGAS_RADIUS']?>"> miles
                </td> 
            </tr>
            
            <tr>
                <td>Onboarding Hashtag (list hashtag separated with a comma symbol)<span class="color-red">(*)</span></td>
                <td>
                    <input type="text" name="onboarding_hashtag"  id="onboarding_hashtag" size=60
                           value="<?php echo isset($this->form_validation->onboarding_hashtag) ? $this->form_validation->onboarding_hashtag : @$config['ONBOARDING_HASHTAG']?>">
                    <?php if ($trending_hashtag): ?>
                    <span onclick="openDivPopularHashTag()" class="openlink"> <i>Show current popular hashtag</i> </span>  
                    <div id="trending_hashtag" style="display:none;" class="trending-hashtag">
                        <?php foreach($trending_hashtag as $tag):?>
                            <input type="checkbox" value="<?php echo $tag->HashTag ?>" id="<?php echo $tag->HashTag ?>" onclick="fillHashTag('<?php echo $tag->HashTag?>')">
                            <span><?php echo $tag->HashTag?></span> <br />
                         <?php endforeach;?>
                    </div>
                    <?php endif;?>
                </td>
            </tr>
            
            <tr>
                <td>Mingle screen show users active within (day)<span class="color-red">(*)</span></td>
                <td><input type="text" name="mingle_active_day" size=60
                           value="<?php echo isset($this->form_validation->mingle_active_day) ? $this->form_validation->mingle_active_day : @$config['MINGLE_ACTIVE_DAY']?>">
                </td>
            </tr>
            
            <tr>
                <td>Maximum of # pts per day (shares content on FB Wall)<span class="color-red">(*)</span></td>
                <td><input type="text" name="max_point_per_day" size=60
                           value="<?php echo isset($this->form_validation->max_point_per_day) ? $this->form_validation->max_point_per_day : @$config['MAX_POINT_PER_DAY']?>">
                </td>
            </tr>
            
            <tr>
                <td>(Daily Match) Limit of # matched users <span class="color-red">(*)</span></td>
                <td><input type="text" name="match_user_limit" size=60
                           value="<?php echo isset($this->form_validation->match_user_limit) ? $this->form_validation->match_user_limit : @$config['MATCH_USER_LIMIT']?>">
                </td>
            </tr>
            
            <tr>
                <td>(Daily Match) num of user's active days <span class="color-red">(*)</span></td>
                <td><input type="text" name="match_active_day" size=60
                           value="<?php echo isset($this->form_validation->match_active_day) ? $this->form_validation->match_active_day : @$config['MATCH_ACTIVE_DAY']?>">
                </td>
            </tr>
            
            <tr>
                <td>(Daily Match) num of user's ice breaker required <span class="color-red">(*)</span></td>
                <td><input type="text" name="match_icebreaker_require" size=60
                           value="<?php echo isset($this->form_validation->match_icebreaker_require) ? $this->form_validation->match_icebreaker_require : @$config['MATCH_ICEBREAKER_REQUIRE']?>">
                </td>
            </tr>
            
            <tr>
                <td>(Daily Match) get match limitation <span class="color-red">(*)</span></td>
                <td><input type="text" name="match_repeat_limitation" size=60
                           value="<?php echo isset($this->form_validation->match_repeat_limitation) ? $this->form_validation->match_repeat_limitation : @$config['MATCH_REPEAT_LIMITATION']?>"> hours
                </td>
            </tr>
            <tr>
                <td>(Daily Match) get match limitation (Users who get no response within limitation hours) <span class="color-red">(*)</span></td>
                <td><input type="text" name="match_no_response_limit" size=60
                           value="<?php echo isset($this->form_validation->match_no_response_limit) ? $this->form_validation->match_no_response_limit : @$config['MATCH_NO_RESPONSE_LIMIT']?>"> hours
                </td>
            </tr>
            
            <tr>
                <td>(Daily Match) Minimum of shared interest <span class="color-red">(*)</span></td>
                <td><input type="text" name="match_minimum_interest" size=60
                           value="<?php echo isset($this->form_validation->match_minimum_interest) ? $this->form_validation->match_minimum_interest : @$config['MATCH_MINIMUM_INTEREST']?>">
                </td>
            </tr>
            <tr>
                <td>(Daily Match) Minimum of shared major <span class="color-red">(*)</span></td>
                <td><input type="text" name="match_minimum_major" size=60
                           value="<?php echo isset($this->form_validation->match_minimum_major) ? $this->form_validation->match_minimum_major : @$config['MATCH_MINIMUM_MAJOR']?>">
                </td>
            </tr>
            
            <tr>
                <td>(Daily Match) Minimum of shared icebreaker question <span class="color-red">(*)</span></td>
                <td><input type="text" name="match_minimum_question" size=60
                           value="<?php echo isset($this->form_validation->match_minimum_question) ? $this->form_validation->match_minimum_question : @$config['MATCH_MINIMUM_QUESTION']?>">
                </td>
            </tr>
            
            <tr>
                <td>UserID of contest winners (list UserID separated with a comma symbol)<span class="color-red">(*)</span></td>
                <td><input type="text" name="user_id_contest_winner" size=60
                           value="<?php echo isset($this->form_validation->user_id_contest_winner) ? $this->form_validation->user_id_contest_winner : @$config['USERID_CONTEST_WINNER']?>">
                </td>
            </tr>
            <tr>
                <td>(Daily Match) get match limitation (for contest winners)<span class="color-red">(*)</span></td>
                <td><input type="text" name="match_repeat_limitation_contest_winner" size=60
                           value="<?php echo isset($this->form_validation->match_repeat_limitation_contest_winner) ? $this->form_validation->match_repeat_limitation_contest_winner : @$config['MATCH_REPEAT_LIMITATION_CONTEST_WINNER']?>"> hours
                </td>
            </tr>

            <tr>
                <td>Contest time<span class="color-red">(*)</span></td>
                <td>Start: <input type="text" name="start_date_contest_winner"  id="start_date_contest_winner" size=20
                           value="<?php echo isset($this->form_validation->start_date_contest_winner) ? $this->form_validation->start_date_contest_winner : @$config['START_DATE_CONTEST_WINNER']?>"> 
                    End: <input type="text" name="end_date_contest_winner" id="end_date_contest_winner" size=20
                           value="<?php echo isset($this->form_validation->end_date_contest_winner) ? $this->form_validation->end_date_contest_winner : @$config['END_DATE_CONTEST_WINNER']?>"> 
                    (Y-m-d)
                </td>
            </tr>
            
            <tr>
                <td>Turn ON/OFF Promo code <span class="color-red">(*)</span></td>
                <td><?php echo form_dropdown('use_promo_code', array('1' => 'On', '0' => 'Off'), isset($this->form_validation->use_promo_code) ? $this->form_validation->use_promo_code : @$config['UsePromoCode'], "style='width:100px;'")?></td>
            </tr>
            
            <tr>
                <td>Disable Popular tab - min total post <span class="color-red">(*)</span></td>
                <td><input type="text" name="min_total_post" size=60
                           value="<?php echo isset($this->form_validation->min_total_post) ? $this->form_validation->min_total_post : @$config['MIN_TOTAL_POST']?>"></td>
            </tr>
            
            <tr>
                <td>Disable Popular tab - min total popular <span class="color-red">(*)</span></td>
                <td><input type="text" name="min_total_popular" size=60
                           value="<?php echo isset($this->form_validation->min_total_popular) ? $this->form_validation->min_total_popular : @$config['MIN_TOTAL_POPULAR']?>"></td>
            </tr>
            
            <tr>
                <td>EmailValidationInterval<span class="color-red">(*) </span></td>
                <td><input type="text" name="EmailValidationInterval" size=60
                           value="<?php echo isset($this->form_validation->EmailValidationInterval) ? $this->form_validation->EmailValidationInterval : @$config['EmailValidationInterval']?>"> hours</td>
            </tr>
            
            <tr>
                <td>Limit topic per user<span class="color-red">(*) </span></td>
                <td><input type="text" name="LIMIT_TOPIC_PER_USER" size=60
                           value="<?php echo isset($this->form_validation->LIMIT_TOPIC_PER_USER) ? $this->form_validation->LIMIT_TOPIC_PER_USER : @$config['LIMIT_TOPIC_PER_USER']?>"> hours</td>
            </tr>
            
            <tr>
                <td colspan="2" align="center"><input value="Submit" type="submit" name="submit" id="submit"> &nbsp
                    <input value="Cancel" type="reset"></td>
            </tr>
            </tbody>
        </table>
    </div>
    <br class="clear">
</div>
<?php include '_footer.php'; ?>