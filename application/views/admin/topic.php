<?php include '_header.php' ?>
<title>Topic Management </title>
<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/theme/jquery.ui.theme.css" type="text/css"
      media="screen"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/theme/jquery-ui.css" type="text/css"
      media="screen"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/jquery.dataTables_themeroller.css"
      type="text/css" media="screen"/>

<script type="text/javascript" src="<?php echo base_url(); ?>public/admin/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/admin/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/admin/js/jquery.jeditable.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/admin/js/jquery.jeditable.datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/admin/js/jquery-ui.custom.min.js"></script>

<br>

<div class="lev4">
    <ul>
        <li><a href="<?php echo base_url() ?>admin/topic_category/index"><img src="<?php echo $this->config->item('base_url'); ?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">Topic Category</a></li>
    </ul>
</div>

<div class="wrapper">
    <script type="text/javascript">
        $(document).ready(function () {

            $("button").button();
            var oTable = $('#big_table').DataTable({
                "bProcessing": true,
                "bServerSide": true,
                "bSortClasses": false,
                "sAjaxSource": '<?php echo base_url(); ?>index.php/admin/topic/datatable',
                "bJQueryUI": true,
                "sPaginationType": "full_numbers",
                "iDisplayStart ": 20,
                "oLanguage": {
                    "sProcessing": "<img src='<?php echo base_url(); ?>public/admin/images/ajax-loader_dark.gif'>",
                },
                "aoColumns": [
                    { "mData": "TopicID", "sClass": "TopicID", "bSearchable": false, "bVisible": false },
                    { "mData": "TopicName", "sClass": "TopicName", "bSearchable": false, sDefaultContent: "n/a", "sWidth": "10%"},
                    { "mData": "Desc", "sClass": "Desc", "bSearchable": false, sDefaultContent: "n/a", "sWidth": "10%"},
                    { "mData": "TotalPost", "sClass": "TotalPost", "bSearchable": false, sDefaultContent: "n/a", "sWidth": "2%"},
                    { "mData": "Moderator", "sClass": "Moderator", "bSearchable": false, sDefaultContent: "n/a", "sWidth": "2%"},
                    { "mData": "CreatedDate", "sClass": "CreatedDate", sDefaultContent: "n/a", "sWidth": "10%"},
                    { "mData": "LastModified", "sClass": "LastModified", "bSearchable": false, sDefaultContent: "n/a", "sWidth": "10%"},
                    { "mData": "TopicID", "sClass": "center", "bSearchable": false, "bSortable": false, sDefaultContent: "n/a", "sWidth": "5%",

                        "mRender": function (data, type, full) {
                            return "<a target='_blank' href='<?php echo base_url().'admin/post/index/'?>" +data + "'>View post</a>";
                        }
                    },
                  
                    { "mData": "TopicID", "sClass": "center", "bSearchable": false, "bSortable": false, sDefaultContent: "n/a", "sWidth": "5%",

                        "mRender": function (data, type, full) {
                            return "<button class='delete' id='" + data + "'>Delete</button>";
                        }
                    }
                ],
                'fnServerData': function (sSource, aoData, fnCallback) {
                    $.ajax
                    ({
                        'dataType': 'json',
                        'type': 'POST',
                        'url': sSource,
                        'data': aoData,
                        'success': fnCallback
                    });
                },

                "fnDrawCallback": function (oSettings) {
                    // Initialize delete buttons
                    $("button.delete").button({
                        icons: { primary: "ui-icon-trash" }, text: false
                    });
                    // Initialize inplace editors
                    $("#big_table tbody td.TopicName, #big_table tbody td.Desc").editable(function (value, settings) {
                        var submitdata = {
                            "id": $(this).parent("tr").attr("id"),
                            "columnname": $(this).attr("class"),
                            "value": value
                        };

                        $.post("<?php echo base_url(); ?>index.php/admin/topic/edit", submitdata);
                        return value;
                    }, {
                        "tooltip": "Click to edit"
                    });

                }
            });

            $(document).on("click", "button.delete", function (e) {
            
                if ( confirm("Do you want to delete this topic?")){
                    $.post("<?php echo base_url(); ?>index.php/admin/topic/delete", { "id": $(this).attr("id") }, function () {
                    oTable.draw(false);
                    });
                }

                
            });

            

        });
    </script>
</div>

<table cellpadding="0" cellspacing="0" border="0" class="display" id="big_table">
    <thead>
    <tr>
        <th>TopicID</th>
        <th>TopicName</th>
        <th>Desc</th>
        <th>TotalPost</th>
        <th>Moderator</th>
        <th>CreatedDate</th>
        <th>LastModified</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
    </tr>
    </thead>
</table>
<div class="spacer">&nbsp;</div>


<br/>

</body>
<br>

<?php include '_footer.php'; ?>
