<?php include '_header.php';?>
<div id="content">
      <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <!--<li><a href=""><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_09.gif" alt="" border="0" height="39" width="32">Delete</a></li>-->
                <li><a href="<?php echo base_url()?>admin/credit/post"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_13.gif" alt="" border="0" height="39" width="32">Create</a></li>
              </ul>
            </div>
            <h2>Credit</h2>
          </div>
          <br class="clear">
          <div class='color-red'><?php echo (isset($this->uri->vn_param['delete']))?'Delete successful':''?></div>
          <div style='color:red'>Found <?php echo  $total ?> result(s)</div>
          <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param)?></div>
          <form action="<?php echo base_url()?>admin/credit/index" id="fm_filter_credit" name="fm_filter_credit" method="post">
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <th scope="col" width="5%">
				 <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'id-asc' || $this->uri->vn_param['sort'] == 'id-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'id-asc'):?>	
                		<a href="<?php echo uri_sort('id-desc')?>" class='color-red'>ID<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('id-asc')?>" class='color-red'>ID<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('id-asc')?>">ID</a>
                <?php endif;?>
				</th>
               
                <th scope="col" width="15%">
                <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'credits-asc' || $this->uri->vn_param['sort'] == 'credits-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'credits-asc'):?>	
                		<a href="<?php echo uri_sort('credits-desc')?>" class='color-red'>Credits<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('credits-asc')?>" class='color-red'>Credits<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('credits-asc')?>">Credits</a>
                <?php endif;?>
                </th>
                
                 <th scope="col" width="15%">
                <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'price-asc' || $this->uri->vn_param['sort'] == 'price-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'price-asc'):?>	
                		<a href="<?php echo uri_sort('price-desc')?>" class='color-red'>Price<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('price-asc')?>" class='color-red'>Price<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('price-asc')?>">Price</a>
                <?php endif;?>
                </th>
                <th scope="col" width="">ProductID</th>
                <th scope="col" width="">Platform</th>
                <th scope="col" width="">Action</th>
              </tr>
            <?php if (count($results) > 0):?>
			<?php foreach ($results as $row): ?>
              <tr>
                <td><?php echo $row->ID?></td>
                <td><?php echo $row->Credits?></td>
                <td><?php echo $row->Price?></td>
                <td><?php echo $row->ProductID?></td>
                <td><?php echo $row->Platform?></td>
                <td>
                	<a href="<?php echo base_url()?>admin/credit/post/<?php echo $row->ID?>">edit </a>
                </td>
              </tr>
            <?php endforeach;?>
            <?php endif;?>
            </tbody>
          </table>
          </form>
      </div>
      <br class="clear">
    </div>
<?php include '_footer.php';?>