<?php include '_header.php';?>
<div id="content">
      <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <!--<li><a href=""><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_09.gif" alt="" border="0" height="39" width="32">Delete</a></li>-->
                <li><a href="<?php echo base_url()?>admin/gift_category/post"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_13.gif" alt="" border="0" height="39" width="32">Create</a></li>
              </ul>
            </div>
            <h2>Gift Category</h2>
          </div>
          <br class="clear">
          <div class='color-red'><?php echo (isset($this->uri->vn_param['delete']))?'Delete successful':''?></div>
          <div style='color:red'>Found <?php echo  $total ?> result(s)</div>
          <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param)?></div>
          <form action="<?php echo base_url()?>admin/gift/index" id="fm_filter_credit" name="fm_filter_credit" method="post">
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <th scope="col" width="5%">
				 <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'id-asc' || $this->uri->vn_param['sort'] == 'id-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'id-asc'):?>	
                		<a href="<?php echo uri_sort('id-desc')?>" class='color-red'>ID<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('id-asc')?>" class='color-red'>ID<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('id-asc')?>">ID</a>
                <?php endif;?>
				</th>
               
                <th scope="col" width="15%">
                <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'name-asc' || $this->uri->vn_param['sort'] == 'name-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'name-asc'):?>	
                		<a href="<?php echo uri_sort('name-desc')?>" class='color-red'>Name<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('name-asc')?>" class='color-red'>Name<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('name-asc')?>">Name</a>
                <?php endif;?>
                </th>
                   
                <th scope="col" width="">Action</th>
              </tr>
            <?php if (count($results) > 0):?>
			<?php foreach ($results as $row): ?>
              <tr>
                <td><?php echo $row->ID?></td>
                <td><?php echo $row->Name?></td>
                <td>
                	<a href="<?php echo base_url()?>admin/gift_category/post/<?php echo $row->ID?>">edit </a> 
                </td>
              </tr>
            <?php endforeach;?>
            <?php endif;?>
            </tbody>
          </table>
          </form>
      </div>
      <br class="clear">
    </div>
<?php include '_footer.php';?>