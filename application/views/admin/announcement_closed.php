<?php include '_header.php'; ?>
<div id="content">
    <div id="main">
        <div class="highlight">

            <h2>Announcement Closed</h2>
        </div>
        <br class="clear">
        <div class='color-red'><?php echo (isset($this->uri->vn_param['delete'])) ? 'Delete successful' : '' ?></div>
        <div style='color:red'>Found <?php echo $total ?> result(s)</div>
        <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>
        <form action="<?php echo base_url() ?>admin/announcement/closed" id="fm_filter_credit" name="fm_filter_credit" method="post">
            <?php if (count($results) > 0): ?>
                <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
                    <tbody>

                    <th scope="col" width="15%">UserName</th>
                    <th scope="col" width="">Announcement Card</th>
                    <th scope="col" width="">Content</th>
                    <th scope="col" width="15%">Created Date</th>

                    </tr>

                    <?php foreach ($results as $row): ?>
                        <tr>
                            <td><?php echo $row->UserName?></td>
                            <td><a href="<?php echo base_url() ?>admin/announcement/post/<?php echo $row->AnnouncementID ?>" title="edit"><?php echo $row->Title ?></a></td>
                            <td><?php echo $row->Content ?></td>
                            <td><?php echo $row->CreatedDate?></td>

                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            <?php endif; ?>
        </form>
    </div>
    <br class="clear">
</div>
<?php include '_footer.php'; ?>