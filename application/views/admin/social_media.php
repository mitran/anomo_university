<?php include '_header.php';?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-ui-1.8.9.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery.datetimepicker.js"></script>
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/smoothness/jquery-ui-1.8.9.custom.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/add_datetimepicker.css" rel="stylesheet" type="text/css" media="screen">

<script>
$(document).ready(function() {

	$('#submit').click(function() {
		var fb_ad_click = $("#fb_ad_click").val();
		if (fb_ad_click == '' || isNaN(fb_ad_click)){
			$("#fb_ad_click").focus();
			return false;
		}
		var week = $("#week").val();
		if (week == ''){
			$("#week").focus();
			return false;
		}

		var fb_dowloads = $("#fb_dowloads").val();
		if (fb_dowloads == ''|| isNaN(fb_dowloads)){
			$("#fb_dowloads").focus();
			return false;
		}

		var fb_likes = $("#fb_likes").val();
		if (fb_likes == ''|| isNaN(fb_likes)){
			$("#fb_likes").focus();
			return false;
		}

		var twitter_followers = $("#twitter_followers").val();
		if (twitter_followers == ''|| isNaN(twitter_followers)){
			$("#twitter_followers").focus();
			return false;
		}
	});

	
	$( "#week" ).datepicker({dateFormat: "yy-mm-dd"});

});
</script>

<style>
.tblAdminDetail td {
	text-align: none;
}
</style>
<div id="content">
  <div id="main">
    <div class="highlight">
      <h2>Marketing</h2>
    </div>
    <br class="clear">
     <br class="clear">
        <div class="highlight" style="text-align: left">
            <a href="<?php echo base_url() ?>admin/marketing/value">Value</a> | 
            <a  href="<?php echo base_url() ?>admin/marketing/core_feature_engagement_new">Core Feature Engagement New</a> |            
            <a  href="<?php echo base_url() ?>admin/marketing/content_engagement">Content Engagement</a> | 
            <a  href="<?php echo base_url() ?>admin/marketing/life_cycle_tracking">Life Cycle Tracking</a> |  
            <a  href="<?php echo base_url() ?>admin/marketing/kpi">KPI</a> |
            <a  class="active menu-picture"  href="<?php echo base_url() ?>admin/marketing/social_media">Social Media</a>      
        </div>
        <br class="clear">
    <form action="<?php echo base_url()?>admin/marketing/add_social_media" id="gplaces" name="gplaces" method="post">
    <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="80%">
            <tbody>
			<tr>
				<th>Week</th>
				<th>FB Ad Clicks</th>
                <th>FB Downloads</th>
                <th>FB Likes</th>
                <th>Twitter Followers</th>
              </tr>
             </tbody>
             <tr>
             	<td><input type="text" name="week" id="week"/></td>
             	<td><input type="text" name="fb_ad_click" id="fb_ad_click" size=5/></td>
             	<td><input type="text" name="fb_dowloads"  id="fb_dowloads"/></td>
             	<td><input type="text" name="fb_likes" id="fb_likes" size=5/></td>
             	<td><input type="text" name="twitter_followers"  id="twitter_followers"/></td>
             </tr>
             <tr>
             	<td colspan=12><input value="Submit" type="submit" name='submit' id="submit"></td>
             </tr>
    </table>
    </form>
    <div style="height:20px;"></div>
     <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param)?></div>
    <?php if (isset($result) && count($result) > 0):?>
    <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
			<tr>
				<th width="10%">Week</th>
				<th width="5%">FB Ad Clicks</th>
                <th width="10%">FB Downloads</th>
                <th width="10%">FB Likes</th>
                <th width="10%">Twitter Followers</th>
              </tr>
             </tbody>
             <?php foreach ($result as $re):?>
             <tr>
             	<td><?php echo $re->Week?></td>
             	<td><?php echo $re->FBAdClicks ?></td>
             	<td><?php echo $re->FBDownloads?></td>
             	<td><?php echo $re->FBLikes?></td>
             	<td><?php echo $re->TwitterFollowers?></td>
             </tr>
             <?php endforeach;?>
    </table>
    <?php endif;?>
  </div>
  <br class="clear">
</div>

<?php include '_footer.php';?>

