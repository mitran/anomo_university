<?php include '_header.php';?>
<div id="content">
      <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <!--<li><a href=""><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_09.gif" alt="" border="0" height="39" width="32">Delete</a></li>-->
                <li><a href="<?php echo base_url()?>admin/lplaces/post"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_13.gif" alt="" border="0" height="39" width="32">Create</a></li>
              </ul>
            </div>
            <h2>Local Places</h2>
          </div>
          <br class="clear">
          <div class='color-red'><?php echo (isset($this->uri->vn_param['delete']))?$this->uri->vn_param['delete']:''?></div>
          <div style='color:red'>Found <?php echo  $total ?> result(s)</div>
          <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param)?></div>
          <form action="<?php echo base_url()?>admin/lplaces/index" id="fm_filter_lplaces" name="fm_filter_lplaces" method="post">
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
            <!--
              <tr>
                <td colspan="11" width="5%">
				Keyword: <input name="keyword" value="<?php echo isset($this->uri->vn_param['keyword'])?htmlspecialchars($this->uri->vn_param['keyword']):''?>" type="text" id="keyword">
				<?php $status = array('' => '---select---',0 => 'Not Active', 1 => 'Active')?>
				Status: <?php echo form_dropdown('status', $status, isset($this->uri->vn_param['status'])?$this->uri->vn_param['status']:'', "id='status' style='width:100px;'")?>
				<input value="Filter" type="submit" name='btn_submit' id="btn_submit" onclick="filter_user()">
				<input value="Cancel" type="reset" name='cancel_list' id="cancel_list" onclick="javascript:window.location='<?php echo base_url()?>admin/index/list_user'">
                </td>
              </tr>
            --> 
              <tr>
              	<th>ID</th>
                <th>Name</th>
                <th>Create Date</th>
                <th>Lat</th>
                <th>Lon</th>
                <th>Address</th>
                <th>Category</th>
                <th>Provider</th>
                <th width="80px">Action</th>
              </tr>
            
            <?php if (count($results) > 0):?>
			<?php foreach ($results as $row): ?>
              <tr>
              	<td><?php echo $row->ID?></td>
              	<td><?php echo $row->Name?></td>
              	<td><?php echo $row->CreatedDate?>
              	<td><?php echo $row->Lat?></td>
                <td><?php echo $row->Lon?></td>
                <td><?php echo $row->Address?></td>
                <td><?php echo $row->Category?></td>
                <td><?php echo $row->Provider?></td>
                <td>
                    <a href="<?php echo base_url()?>admin/lplaces/post/<?php echo $row->ID?>">Edit</a>&nbsp;|
                    <a id="delete<?php echo $row->ID?>" href="#" onclick="delete_local_place(<?php echo $row->ID?>)">Delete</a>
                </td>
              </tr>
           <?php endforeach;?>
           <?php endif;?>
           	</tbody>
          </table>
		  </form>
</div>
<br class="clear">
</div>
<?php include '_footer.php';?>
<script lang="javascript">
function delete_local_place(id){
	if (confirm('Are you sure you want to delete this Local Place?')){
		jQuery('#delete'+id).attr('href', base_url+'admin/lplaces/delete/'+id);
	}else{
		return false;
	}
	
}
</script>