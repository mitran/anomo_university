<?php include '_header.php';?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-ui-1.8.9.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery.datetimepicker.js"></script>
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/smoothness/jquery-ui-1.8.9.custom.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/add_datetimepicker.css" rel="stylesheet" type="text/css" media="screen">

<script>
$(document).ready(function() {

	$('#submit').click(function() {
		var from = $("#from").val();
		if (from == ''){
			return false;
		}

		var to = $("#to").val();
		if (to == ''){
			return false;
		}

		var android = $("#android").val();
		if (android == ''){
			return false;
		}

		var ios = $("#ios").val();
		if (ios == ''){
			return false;
		}
	});

	
	$( "#from" ).datepicker({dateFormat: "yy-mm-dd"});
	$( "#to" ).datepicker({dateFormat: "yy-mm-dd"});

});
</script>

<style>
.tblAdminDetail td {
	text-align: none;
}
</style>
<div id="content">
  <div id="main">
    <div class="highlight">
      <h2>Advance Metrics</h2>
    </div>
    <br class="clear">
    <form action="<?php echo base_url()?>admin/report/add_metrics" id="gplaces" name="gplaces" method="post">
    <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
      <tbody>
        <tr>
        	<td>
              Weekly 
          </td>
          <td>
              From: <input id="from" name="from" value="" type="text" style="width:100px;">
              To: <input id="to" name="to" value="" type="text" style="width:100px;">
          </td>
        </tr>
        <tr>
        	<td>
              Android 
          </td>
          <td>
               <input name="android" type="text" id="android">
          </td>
        </tr>
        <tr>
        	<td>
              Ios 
          </td>
          <td>
              <input name="ios" type="text" id="ios">
          </td>
        </tr>
        <tr>
        
          <td>
              <input value="Submit" type="submit" name='submit' id="submit">
          </td>
          <td>
          </td>
        </tr>
        
      </tbody>
    </table>
    </form>
    <div style="height:20px;"></div>
     <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param)?></div>
    <?php if (isset($result) && count($result) > 0):?>
    <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="80%">
            <tbody>
			<tr>
				<th>From</th>
				<th>To</th>
                <th>Google play</th>
                <th>AppStore</th>
                <th>Total</th>
              </tr>
             </tbody>
             <?php foreach ($result as $re):?>
             <tr>
             	<td><?php echo $re->From?></td>
             	<td><?php echo $re->To?></td>
             	<td><?php echo $re->Android?></td>
             	<td><?php echo $re->Ios?></td>
             	<td><?php echo $re->Android + $re->Ios?></td>
             </tr>
             <?php endforeach;?>
    </table>
    <?php endif;?>
  </div>
  <br class="clear">
</div>

<?php include '_footer.php';?>

