<?php include '_header.php';?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-ui-1.8.9.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery.datetimepicker.js"></script>
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/smoothness/jquery-ui-1.8.9.custom.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/add_datetimepicker.css" rel="stylesheet" type="text/css" media="screen">

<script>
$(document).ready(function() {
	$( "#day" ).datepicker({dateFormat: "yy-mm-dd"});
        
});
</script>

<div id="content">
  <div id="main">
    <div class="highlight">
      <h2>Top Like</h2>
    </div>
    <br class="clear">
    <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
      <tbody>
        <tr>
          <td>
            <form action="<?php echo base_url()?>admin/report/top_like" id="gplaces" name="gplaces" method="post">
              HashTag <input id="hashtag" name="hashtag" value="<?php echo isset($hashtag)?$hashtag:'';?>" type="text" style="width:200px;">
              Date: <input id="day" name="day" value="<?php echo isset($day)?$day:'';?>" type="text" style="width:100px;">
              <input value="Show" type="submit" name='btn_submit' id="btn_submit">
			  
            </form>
          </td>
        </tr>
      </tbody>
    </table>
    <div style="height:20px;"></div>
    <?php if (isset($result) && sizeof($result) > 0):?>
    <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%" 
        <tbody>
            <tr>
            
                <th>UserID</th>
                <th>UserName</th>
                <th>Message</th>
                <th>Image</th>
                <th>CreatedDate</th>
                <th>Like</th>
            </tr>        
       </tbody>
       <?php foreach($result as $re):?>
            <tr>
                
                <td><?php echo $re->FromUserID?></td>
                <td><?php echo $re->FromUserName?></td>
                <td style="width:200px;"><?php echo $this->Dashboard_model->formatMsg($re->Message)?></td>
                <td><?php  if ($re->Image != ''):?>
                    <a href="<?php echo $re->Image ?>" data-lightbox="<?php echo $re->Image ?>"  >
                         <img style="max-width:150px;" src="<?php echo $re->Image ?>"  />
                    </a>
                    
                    <?php endif;?>
                </td>
                <td><?php echo $re->CreatedDate?></td>
                <td><?php echo $re->TotalLike?></td>
            </tr>
       <?php endforeach;?>
    </table>
    <?php endif;?>
  </div>
  <br class="clear">
</div>

<?php include '_footer.php';?>

