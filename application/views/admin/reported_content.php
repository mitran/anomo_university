<?php include '_header.php'; ?>

<script type="text/javascript"
	src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.9.0.js"></script>
<link
	href="<?php echo $this->config->item('base_url'); ?>public/admin/css/screen.css"
	rel="stylesheet" type="text/css">

<script>

$(document).ready(function() {

	function scrollToElement(selector, verticalOffset) {
	    verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
	    element = $(selector);
	    offset = element.offset();
	    offsetTop = offset.top + verticalOffset;
	    $('html, body').animate({
	        scrollTop: offsetTop
	    });
	}


	$("table.tblAdminDetail tr td a.deleterow").click(function() {
        var clickedID = $(this).attr('id').split('_');
        var DbNumberID = clickedID[1];
        var ContentID = clickedID[2]; 
        var rowType = clickedID[3];
        var isComment = clickedID[4]
        var currentOffset = clickedID[5];
        var myData = 'recordToDelete='+ DbNumberID;        
        var url = '<?php echo base_url() ?>admin/reportban_user/delete_content/'+ContentID+'/'+rowType+'/' + isComment+'/' +currentOffset;
		var row = $("#"+'row_'+DbNumberID);
		scrollToElement("#"+'row_'+DbNumberID,-200);
        $.ajax({
        	'type': "POST", 
	        'url': url,			
           'dataType':"text",
           'data': 
               myData, 
           beforeSend : function (){
           	$('.spinner').show();
           }, 
           success:function(response){
//        		row.find("td").remove();         		                
           },
           error:function (xhr, ajaxOptions, thrownError){
               alert(thrownError);
           },
	        complete: function(){
	        	$('.spinner').hide();
	        	row.find("td.tick").append("<img src='<?php echo base_url(); ?>public/admin/images/tick.jpg'/>");
	        }	
         });
        
	});


	$("table.tblAdminDetail tr td a.forgiverow").click(function() {
		
        var clickedID = $(this).attr('id').split('_');
        var DbNumberID = clickedID[2];
        var ContentID = clickedID[3]; 
        var rowType = clickedID[4];
        var IsForgivenValue = clickedID[5];
        var isComment = clickedID[6];
        var currentOffset = clickedID[7];
        var myData = 'recordToUpdate='+ ContentID;   
        var url = '<?php echo base_url() ?>admin/reportban_user/forgive_content/'+ContentID+'/'+rowType+'/'+IsForgivenValue+'/'+ isComment +'/'+currentOffset;     
		var row = $("#"+'row_'+DbNumberID);
		scrollToElement("#"+'row_'+DbNumberID,-200);
        $.ajax({
           'type': "POST",
           'url': url,
           'dataType':"text",
           'data':myData, 
           beforeSend : function (){
           	$('.spinner').show();
           }, 
           success:function(response){
//        	   row.find("td").remove();
        	   
           },
           error:function (xhr, ajaxOptions, thrownError){
               alert(thrownError);
           },
	        complete: function(){
	        	$('.spinner').hide();
	        	row.find("td.tick").append("<img src='<?php echo base_url(); ?>public/admin/images/tick.jpg'/>");	        	
	        }	
         });
        
	});
	

	$("table.tblAdminDetail tr td a.deletebanrow").click(function() {


        var clickedID = $(this).attr('id').split('_');
        var DbNumberID = clickedID[3];
        var ContentID = clickedID[4]; 
        var rowType = clickedID[5];
        var ReportedUserID = clickedID[6];
        var IsBanned = clickedID[7];
        var isComment = clickedID[8];
        var currentOffset = clickedID[9];
        var myData = 'recordToDeleteUpdate='+ ContentID;   
        var url =  '<?php echo base_url() ?>admin/reportban_user/delete_content_ban_user/'+ContentID+'/'+rowType+'/'+ReportedUserID+'/'+IsBanned+'/'+ isComment +'/'+currentOffset;    
		var row = $("#"+'row_'+DbNumberID);
		scrollToElement("#"+'row_'+DbNumberID,-200);
        $.ajax({
           'type': "POST",
           'url': url,         
           'dataType':"text",
           'data': myData, 
           beforeSend : function (){
           	$('.spinner').show();
           }, 
           success:function(response){
//        	   row.find("td").remove();                  
           },
           error:function (xhr, ajaxOptions, thrownError){
               alert(thrownError);
           },
	        complete: function(){
	        	$('.spinner').hide();
	        	row.find("td.tick").append("<img src='<?php echo base_url(); ?>public/admin/images/tick.jpg'/>");
	        }	
         });
        
	});
		
});
</script>
<br>

<div id="content">
    <div id="main">
        <div class="highlight">
        	<h2>Reporting and Banning</h2>
        </div>
        <br class="clear">
        <div class="highlight" style="text-align: left">          
        		<a class="active menu-picture"
				href="<?php echo base_url() ?>admin/reportban_user/reported_content">Reported Content
				</a> | 
				<a href="<?php echo base_url() ?>admin/reportban_user/reported_user">Reported Users</a> |
				<a href="<?php echo base_url() ?>admin/reportban_user/exclude_reporting_users">Exclude Reporting Users</a> |
				<a  href="<?php echo base_url().'admin/deleted_content/index/' ?>">Deleted Content</a> 
        </div>
        <br class="clear">
        <div id = "data">
        <div class='color-red'><?php echo (isset($this->uri->vn_param['delete'])) ? $this->uri->vn_param['delete'] : '' ?></div>
        <div style='color:red'>Found <?php echo $total ?> result(s)</div>
        <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>
        <?php if (count($results) > 0): ?>

            <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
                <tbody>
                    <tr>
                    	<th><div style="width: 75px;"><b>Time</b></div></th>
                        <th><div style="width: 125px;"><b>ReportingUsers</b></div></th>
                        <th><div style="width: 50px;"><b>Times Reported</b></div></th>
                        <th><div style="width: 100px;"><b>ReportedUser</b></div></th>                        
                        <th><div style="width: 200px;"><b>Report</b></div></th>
                        <th><div style="width: 75px;"><b>Action</b></div></th>
                        <th><div style="width: 25px;"><b>Review</b></div></th>
                    </tr>	
					<?php $tz = new DateTimeZone('America/Los_Angeles'); ?>
                    <?php foreach ($results as $row): ?> 
	                        <?php
                                if ($row->IsComment == 0){
                                    $contentInfo = $this->activity_model->getContentInfo($row->ContentID, $row->Type);
                                }else{
                                    $contentInfo = $this->Comment_model->getCommentDetail($row->ContentID, $row->Type);
                                }
                                $ownerUserName = $row->ReportedUser;
                                if (is_null($ownerUserName)) {
                                    $ownerUserName = ($contentInfo) ? $contentInfo->UserName : '';
                                }
                                // ANOMO-10664 
                                $is_anonymous = 0;
                                if ($contentInfo && isset($contentInfo->IsAnonymous) && $contentInfo->IsAnonymous == 1){
                                    $is_anonymous = 1;
                                }
	                        ?>

	                        	<tr id="row_<?php echo $row->ID ?>">
	                        	
	                        	<td><div style="width: 75px;"><b>
	                        	<?php $date = new DateTime($row->CreatedDate);
						  				$date->setTimeZone($tz);
						  				echo $date->format('Y-m-d H:i:s'); ?></b></td>	                        	                      
	                        	<td><div style="width: 125px;">	             
	                        		<?php $reportingusers = split(":", $row->ReportingUsers);?>
	                        		<?php if (count($reportingusers) != 1):?>	 
	                        			<?php $reportingcontents = split(":", $row->Contents);	                        			
	                            			if (count($reportingcontents) != 1):
	                            				$i = 0;?>
	                            			                            		
	                        				<ul>                       	                        	
	                        					<?php foreach ($reportingusers as $reportinguser):?>		                        				
	                        						<?php if (strlen($reportinguser) > 15):?>   	                        					                     			
	                        							<li><?php echo '-> '. substr($reportinguser,0,15). '....' . ':'?></li>
	                        							<br>
                            							<li><?php echo $reportingcontents[$i] ?></li>
	                            						
	                        						<?php else:?>
	                        							<li><?php echo '-> '. $reportinguser . ':'?></li>
	                        						<br>
	                        							<?php echo $reportingcontents[$i]?>		                        							
	                        						<?php endif;?> 
	                        						<?php $i++;?>                       		
	                        					<?php endforeach ?>
	                        					 
	                        				</ul>
	                        				<?php endif;?>	
	                        		<?php else: ?>
	                        			<?php if (strlen($reportinguser[0]) > 15):?>  
	                        				<?php echo substr($reportingusers[0],0,15). '....' . ':'?>
	                        				<br>
	                        				<li><?php echo $row->Contents ?></li>
	                        			<?php else:
	                        				echo $reportingusers[0]. ':';?>
	                        				<br>
	                        				<?php echo $row->Contents ?>
	                        			<?php endif;?>
	                        		<?php endif;?>	                        		
	                        	</td>
	                        	<td><div style="width: 50px;"><?php echo $row->c_users ?></td>	                            
	                            
	                            <td><div style="width: 100px;">         		
	                        	<a href="<?php echo base_url() ?>admin/chat_history/index/<?php echo $row->ContentOwnerID ?>">
	                        	<?php if (strlen($ownerUserName) > 15):?>
	                        		<?php echo substr($ownerUserName,0,15). '....' ?>  
	                        	<?php else: 
	                        		echo $ownerUserName; ?>
	                        	<?php endif;?>	
                                         
                                            
					</a>
                                        <?php if ($is_anonymous == 1): ?>
                                             <br /> (Anonymous)
                                        <?php endif; ?>
                      			</td>
                        		
	                        	<td><div style="width: 200px;"> 	
	                            <?php
	                            if ($contentInfo) {
	                                if (isset($contentInfo->Photo)) {
	                                    echo '<img style="max-width:100px;" src=' . $this->config->item('s3_photo_url') . $contentInfo->Photo . ' />';
	                                } 

	                                if (isset($contentInfo->Content)) {
	                                    echo '<b>' . $this->Dashboard_model->formatMsg($contentInfo->Content) . '</b>';
	                                } elseif (isset($contentInfo->PictureCaption)) {
	                                    echo '<b>' . $this->Dashboard_model->formatMsg($contentInfo->PictureCaption) . '</b>';
	                                }
                                        
                                        if (isset($contentInfo->VideoID)){
                                            echo '<br /><b>' . YOUTUBE_VIDEO_URL.$contentInfo->VideoID. '</b>';
                                        }
	                            }
	                            else{
									echo "Content Deleted";
								}
	                            ?>								
								<br>
								</td>
									                        	                       		                            
	                            
	                            <td><div style="width: 75px;"><?php if ($contentInfo): ?>		                                                            

										<a class = "deleterow" href="#" id="content_<?php echo $row->ID . '_' . $row->ContentID . '_' . $row->Type . '_' .$row->IsComment.'_'. $current_offset?>">Delete  
										</a> 
										<a class = "forgiverow" href="#" id="content_forgive_<?php echo $row->ID . '_' . $row->ContentID . '_' . $row->Type . '_' . 'null' . '_'.$row->IsComment.'_' . $current_offset ?>">												
										 Forgive  </a>											 
										<a class = "deletebanrow" href="#" id="content_delete_ban_<?php echo $row->ID . '_' . $row->ContentID . '_' . $row->Type . '_' . $row->ContentOwnerID . '_' . $row->IsBanned . '_' .$row->IsComment.'_'. $current_offset ?>">Delete & Ban 
										</a>										
										                                                                   
																							
						        	<?php else: ?>Delete | Forgive | Delete & Ban
	                            	<?php endif; ?>
	                            </td>
	                            <td class = "tick">
	                            	<div style="display: none"></div>
								</td>					 
	                            	                           
	                        </tr>	                      	
	                        <div class="spinner">
								<img class="img-spinner" src='<?php echo base_url(); ?>public/admin/images/ajax-loader.gif'
								id="img-spinner" alt="Loading" />
							</div>		
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
    </div>
    <br class="clear">
    <p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
</div>
<?php include '_footer.php'; ?>
