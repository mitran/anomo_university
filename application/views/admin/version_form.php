<?php include '_header.php';?>
<div id="content">
    <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <li><a href="<?php echo base_url()?>admin/version"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">Back to list</a></li>
              </ul>
            </div>
            <h2>Posting Version</h2>
          </div>
         <div style="color:red">
         	<?php echo validation_errors(); ?>
         </div>
         <?php echo form_open_multipart('admin/version/post/'.$version_id)?>
         <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <td>Platform <span class="color-red">(*)</span></td>
                <td>
                	<?php echo form_dropdown('platform', array('iOS' => 'iOS', 'android' => 'android'), isset($this->form_validation->platform)?$this->form_validation->platform:@$info->Platform, "style='width:100px'")?>
                </td>
              </tr>
			 <tr>
                <td>App version <span class="color-red">(*)</span></td>
                <td><input type="text" name="app_version" size=60 value="<?php echo isset($this->form_validation->app_version)?$this->form_validation->app_version:@$info->AppVersion?>"></td>
              </tr>
              <tr>
                <td>Is Latest <span class="color-red">(*)</span></td>
                <td>
                	<input type="checkbox" name="is_latest"  value="1" <?php echo ((isset($this->form_validation->is_latest) && $this->form_validation->is_latest == 1)||@$info->IsLatest == 1)?"checked":"" ?>>
                </td>
              </tr>
              <tr>
                <td>Is Support <span class="color-red">(*)</span></td>
                <td>
                	<input type="checkbox" name="is_support"  value="1" <?php echo ((isset($this->form_validation->is_support) && $this->form_validation->is_support == 1)||@$info->IsSupport == 1)?"checked":"" ?>>
                </td>
              </tr>
              <tr>
                <td colspan="2" align="center"><input value="Submit" type="submit" name="submit" id="submit" > &nbsp <input value="Cancel" type="reset" ></td>
             </tr>
			 </tbody>
          </table>
   </div>
   <br class="clear">
</div>
<?php include '_footer.php';?>