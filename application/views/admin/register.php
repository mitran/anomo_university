<?php include '_header.php';?>
    <div id="content">
      <form action="<?php echo base_url()?>admin/index/register" method="post" id="siteinformation" enctype="multipart/form-data">
        <div id="main">
          <div class="highlight">
          	<div class="lev4">
              <ul>
                <li><a href="<?php echo base_url()?>admin/index/list_user"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">Back to list</a></li>
              </ul>
            </div>
            <h2>Add new user</h2>
          </div>
          <br class="clear">
          <div style="color:red">
         	<?php echo isset($this->validation->error_string)?$this->validation->error_string:''; ?>
         </div>
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="80%">
            <tbody>
              <tr>
                <td width="25%">Username:</td>
                <td width=""><input name="user_name" size="50" value="<?php echo isset($this->validation->user_name)?htmlspecialchars($this->validation->user_name):''?>" type="text" >
                </td>
              </tr>
              <tr>
                <td width="25%">Nickname:</td>
                <td width=""><input name="nick_name" size="50" value="<?php echo isset($this->validation->nick_name)?htmlspecialchars($this->validation->nick_name):''?>" type="text">
                </td>
              </tr>
              <tr>
                <td width="25%">Email:</td>
                <td width=""><input name="email" size="50" value="<?php echo isset($this->validation->email)?$this->validation->email:''?>" type="text">
                </td>
              </tr>
              <tr>
                <td width="25%">Password:</td>
                <td width=""><input name="password" size="50" value="<?php echo isset($this->validation->password)?$this->validation->password:''?>" type="password">
                </td>
              </tr>
              <tr>
                <td width="25%">Re-enter password:</td>
                <td width=""><input name="re_password" size="50" value="<?php echo isset($this->validation->re_password)?$this->validation->re_password:''?>" type="password">
                </td>
              </tr>
              <tr>
                <td colspan="2"><input value="Submit" name="submit" id="submit" type="submit">
                  <input value="Cancel" type="reset">
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </form>
      <br class="clear">
    </div>

<?php include '_footer.php';?>
