<?php include '_header.php';?>
<div id="content">
    <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <li><a href="<?php echo base_url()?>admin/avatar"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">Back to list</a></li>
              </ul>
            </div>
            <h2>Posting Avatar</h2>
          </div>
         <div style="color:red">
         	<?php echo @$upload_error;?>
         </div>
         <?php echo form_open_multipart('admin/avatar/post/'.$id)?>
         <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>

                <tr>
                <td>Avatar<span class="color-red">(*)</span></td>
                <td>
                	<input type="file" name="photo" />
                	<?php if ($id > 0):?>
                            <img src="<?php echo $this->config->item('s3_photo_url').$info->Avatar?>" width="120" height="100" />    
                	<?php endif;?>
                </td>
              </tr>
              
                <tr>
                <td>Full Avatar<span class="color-red">(*)</span></td>
                <td>
                	<input type="file" name="fullphoto" />
                	<?php if ($id > 0):?>
                            <img src="<?php echo $this->config->item('s3_photo_url').$info->FullAvatar?>" width="120" height="100" />    
                	<?php endif;?>
                </td>
              </tr>
  
              <tr>
                <td>Gender<span class="color-red">(*)</span></td>
                <td>
                	<?php echo form_dropdown('gender', array(1 => 'Male', 2 => 'Female'), isset($this->form_validation->gender)?$this->form_validation->gender:@$info->Gender, "style='width:100px;'")?>
                </td>
              </tr>
              
              <tr>
                <td colspan="2" align="center"><input value="Submit" type="submit" name="submit" id="submit" > &nbsp <input value="Cancel" type="reset" ></td>
             </tr>
			 </tbody>
          </table>
   </div>
   <br class="clear">
</div>
<?php include '_footer.php';?>