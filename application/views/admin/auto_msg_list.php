<?php include '_header.php'; ?>
<div id="content">
    <div id="main">
        <div class="highlight" style="text-align: left">
            <!--<a href="<?php echo base_url() ?>admin/message/send">Admin mass message</a> | -->
            <a  href="<?php echo base_url() ?>admin/vegas_msg/send">Admin mass message</a> | 
            <a class="active menu-picture" href="<?php echo base_url() ?>admin/auto_msg/send">Auto mass message</a> 
            
        </div>
        <div class="highlight">
            <div class="lev4">
                <ul>
                    <li><a href="<?php echo base_url()?>admin/auto_msg/send"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_13.gif" alt="" border="0" height="39" width="32">Create</a></li>
                </ul>
            </div>
            <h2>List Message</h2>
        </div>
        <br class="clear">
        <div class='color-red'>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
        <div style='color:red'>Found <?php echo $total ?> result(s)</div>
        <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>
        <?php if ($results): ?>
        <form action="#" id="fm_filter_user" name="fm_filter_user" method="post">
            <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
                <tbody>
                    <tr>
                        <th>Message </th>
                        <th>Photo </th>
                        <th>Location</th>
                        <th>Radius</th>
                        <th>Gender</th>
                        <th>Age</th>           
                        <th>Status</th>             
                        <th>ExpireDate</th>
                        <th>CreatedDate</th>
                        <th>Action</th>
                    </tr>

                    
                        <?php foreach ($results as $row): ?>
                            <tr>
                                <td><?php echo $row->message ?></td>
                                <td>
                                    <?php if ($row->photo):?>
                                        <img src="<?php echo $this->config->item('s3_photo_url').$row->photo;?>" style="width:80px; height:80px;"/>
                                    <?php endif;?>
                                </td>
                                <td><?php echo $row->location_name ?></td>
                                <td><?php echo number_format($row->radius) ?></td>
                                <td><?php if($row->gender == 1){ echo "Male";}else if($row->gender == 2){ echo "Female";}else{ echo "All";} ?></td>
                                <td><?php echo ($row->age_from == $row->age_to)? $row->age_from:$row->age_from."-".$row->age_to ?></td>
                                <td><?php echo ($row->status == 1)?"Active":"Deactive" ?></td>
                                <td><?php echo $row->expire_date ?></td>
                                <td><?php echo $row->created_date ?></td>
                                <td>
                                    <a href="<?php echo base_url()?>admin/auto_msg/send/<?php echo $row->message_id?>"> Edit  </a> |
                                    <a href="#" onclick="delete_auto_msg(<?php echo $row->message_id ?>, 'auto_msg_<?php echo $row->message_id ?>')" id="auto_msg_<?php echo $row->message_id ?>">  Delete</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    
                </tbody>
            </table>
        </form>
        <?php endif; ?>
    </div>
    <br class="clear">
    <p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
</div>
<?php include '_footer.php'; ?>