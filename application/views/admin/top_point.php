<?php include '_header.php';?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-ui-1.8.9.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery.datetimepicker.js"></script>
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/smoothness/jquery-ui-1.8.9.custom.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/add_datetimepicker.css" rel="stylesheet" type="text/css" media="screen">

<script>
$(document).ready(function() {
	$( "#from" ).datepicker({dateFormat: "yy-mm-dd"});
	$( "#to" ).datepicker({dateFormat: "yy-mm-dd"});
});
</script>


<div id="content">
  <div id="main">
      <div class="highlight" style="text-align: left">            
         <a href="#" class="active menu-picture" >TOP 20 OVERALL</a> 
      </div> 
    <div class="highlight">
      <h2>LeaderBoard</h2>
    </div>

    <br class="clear">
    <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
      <tbody>
        <tr>
          <td>
            <form action="<?php echo base_url()?>admin/leaderboard" id="gplaces" name="gplaces" method="post">
              From: <input id="from" name="from" value="<?php echo isset($from)?$from:'';?>" type="text" style="width:100px;">
              To: <input id="to" name="to" value="<?php echo isset($to)?$to:'';?>" type="text" style="width:100px;">
              <input value="Search" type="submit" name='btn_submit' id="btn_submit">
			  
            </form>
          </td>
        </tr>
      </tbody>
    </table>
    <div style="height:20px;"></div>
    <?php if (isset($result)):?>
    <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
            <tr>
            <th scope="col" width="15%">UserID</th>
            <th scope="col" width="15%">UserName</th>
            <th scope="col" width="">Avatar</th>
            <th scope="col" width="">BirthDate</th>
            <th scope="col" width="10%">Point</th>
            </tr>

            <?php foreach ($result as $row): ?>
                <tr>
                    <td><?php echo $row->UserID ?></td>
                    <td ><?php echo $row->UserName;?></td>
                    <td>
                        <?php if ($row->Avatar): ?>
                            <img src="<?php echo $row->Avatar?>" width="42px;">
                        <?php endif;?>
                    </td>
                    <td><?php echo $row->BirthDate ?></td>
                    <td><?php echo $row->Point ?></td>
                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
    <?php endif;?>
  </div>
  <br class="clear">
</div>

<?php include '_footer.php';?>

