<?php include '_header.php'; ?>
<div id="content">
    <div id="main">
        <div class="highlight">
            <h2>Marketing</h2>
        </div>
        <div class="highlight" style="text-align: left">
            <a href="<?php echo base_url() ?>admin/marketing/value">Value</a> | 
            <a  href="<?php echo base_url() ?>admin/marketing/core_feature_engagement_new">Core Feature Engagement New</a> |            
            <a class="active menu-picture" href="<?php echo base_url() ?>admin/marketing/content_engagement">Content Engagement</a> | 
            <a  href="<?php echo base_url() ?>admin/marketing/life_cycle_tracking">Life Cycle Tracking</a> |  
            <a  href="<?php echo base_url() ?>admin/marketing/kpi">KPI</a> |
            <a  href="<?php echo base_url() ?>admin/marketing/social_media">Social Media</a>       
        </div>
        <br class="clear">
        <br class="clear">
        <h3>IN HOUSE CONTENT</h3>
        <div style="height:20px;"></div>
        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="80%">
            <tbody>
                <tr>
                    <th><b>Week</b></th>
                    <th><b>#Posts</b></th>
                    <th><b>#Comments</b></th>
                    <th><b>#Likes</b></th>
                    <th><b>#Ext Shares</b></th>
                </tr>
                <?php foreach ($data as $row):?>
                <tr>
                    <td width="25%"><?php echo $row['Week']?></td>
                    <td><?php echo $row['Post']?></td>
                    <td><?php echo $row['Comment']?></td>
                    <td><?php echo $row['Like']?></td>
                    <td><?php echo $row['ExtShare']?></td>
                </tr>
                <?php endforeach;?>
                
            </tbody>
        </table>
        <div style="height:20px;"></div>
         <br class="clear">
         <h3>USER GENERATED CONTENT</h3>
         <div style="height:20px;"></div>
        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="80%">
            <tbody>
                <tr>
                    <th><b>Week</b></th>
                    <th><b>#Posts</b></th>
                    <th><b>#Comments</b></th>
                    <th><b>#Likes</b></th>
                    <th><b>#Ext Shares</b></th>
                </tr>
                <?php foreach ($userContent as $row):?>
                <tr>
                    <td width="25%"><?php echo $row['Week']?></td>
                    <td><?php echo $row['Post']?></td>
                    <td><?php echo $row['Comment']?></td>
                    <td><?php echo $row['Like']?></td>
                    <td><?php echo $row['ExtShare']?></td>
                </tr>
                <?php endforeach;?>
                
            </tbody>
        </table>
    </div>
    <br class="clear">
    <div style="height:20px;"></div>
         <br class="clear">
         <h3>APP INVITE FRIENDS</h3>
         <div style="height:20px;"></div>
        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="80%">
            <tbody>
                <tr>
                    <th><b>Week</b></th>
                    <th><b>#Share</b></th>
                </tr>
                <?php foreach ($appShare as $row):?>
                <tr>
                    <td width="25%"><?php echo $row['Week']?></td>
                    <td><?php echo $row['Share']?></td>
                </tr>
                <?php endforeach;?>
                
            </tbody>
        </table>
</div>

<?php include '_footer.php'; ?>
