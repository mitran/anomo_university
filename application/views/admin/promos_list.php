<?php include '_header.php';?>
<div id="content">
      <div id="main">
          <div class="highlight" style="text-align: left">            
            <a href="<?php echo base_url() ?>admin/promos" class="active menu-picture" >Promos Post</a> | 
            <a  href="<?php echo base_url() ?>admin/promos/fanpage">Fan's Posts</a> 
          </div>  
          <div class="highlight">
            <div class="lev4">
              <ul>
                <li><a href="<?php echo base_url()?>admin/promos/post"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_13.gif" alt="" border="0" height="39" width="32">Create</a></li>
              </ul>
            </div>
            <h2>Promos</h2>
          </div>
          <br class="clear">
          <div class='color-red'><?php echo (isset($this->uri->vn_param['delete']))?$this->uri->vn_param['delete']:''?></div>
          <div style='color:red'>Found <?php echo  $total ?> result(s)</div>
          <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param)?></div>
          <form action="<?php echo base_url()?>admin/promos/index" id="fm_filter_promos" name="fm_filter_promos" method="post">
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <td colspan="11" width="5%">		
                    Vendor <?php echo form_dropdown('user_id', $vendors, isset($this->uri->vn_param['user_id'])?$this->uri->vn_param['user_id']:'', "id='user_id' style='width:200px;'")?>
                    <input value="Filter" type="submit" name='btn_submit' id="btn_submit" onclick="filter_promos()">
                    <input value="Cancel" type="reset" name='cancel_list' id="cancel_list" onclick="javascript:window.location='<?php echo base_url()?>admin/promos/index'">
                </td>
              </tr>
              <tr>
                <th scope="col" width="5%">ID</th>
                <th scope="col" width="">Vendor</th>
                <th scope="col" width="15%">Content</th>
                <th scope="col" width="">Photo</th>
                <th scope="col" width="">Link</th>
                <th scope="col" width="">Published</th>
                <th scope="col" width="">Action</th>
              </tr>
            <?php if ($results):?>
            <?php foreach ($results as $row): ?>
              <tr>
                <td><?php echo $row->ID?></td>
                <td><?php echo @$vendors[$row->UserID]?></td>
                <td><?php echo $row->Content?></td>
                <td>
                    <?php if ($row->Photo):?>
                    <img src="<?php echo $this->config->item('s3_photo_url').$row->Photo;?>" style="width:80px; height:80px;"/>
                    <?php endif; ?>
                </td>
                <td><?php echo $row->Link?></td>
                <td><?php echo $row->IsPublished == 1? 'Yes': 'No'?></td>
                <td>
                    <a href="<?php echo base_url()?>admin/promos/comment/<?php echo $row->ID?>"> Comment  </a> |
                    <a href="<?php echo base_url()?>admin/promos/post/<?php echo $row->ID?>"> Edit  </a> |
                    <a href="#" onclick="delete_promos(<?php echo $row->ID ?>, 'promos_<?php echo $row->ID ?>')" id="promos_<?php echo $row->ID ?>">  Delete</a>
                </td>
              </tr>
            <?php endforeach;?>
            <?php endif;?>
            </tbody>
          </table>
          </form>
      </div>
      <br class="clear">
    </div>
<?php include '_footer.php';?>