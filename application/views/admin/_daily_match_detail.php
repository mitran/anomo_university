
<?php if ($row):?>

<table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
    <tr>
        <td class="bold" width="10%">UserID</td>
        <td><?php echo $row->UserID ?></td>
    </tr>
    <tr>
        <td class="bold">UserName</td>
        <td><?php echo $row->UserName ?></td>
    </tr>
    <tr>
        <td class="bold">BirthDate</td>
        <td><?php echo $row->BirthDate ?></td>
    </tr>
    <tr>
        <td class="bold">SignUpDate</td>
        <td><?php echo $row->SignUpDate ?></td>
    </tr>
    <tr>
        <td class="bold" >Verified</td>
        <td><?php echo $row->IsVerify == 1? 'Yes': 'No' ?></td>
    </tr>
    <tr>
        <td class="bold">#Post</td>
        <td ><?php echo $row->NumberPost + $row->NumberPostArchive ?></td>
    </tr>
    <tr>
        <td class="bold">#Comment</td>
        <td ><?php echo $row->NumberComment ?></td>
    </tr>
    <tr>
        <td class="bold">#Follower</td>
        <td ><?php echo $row->NumberFollower ?></td>
    </tr>
    <tr>
        <td class="bold">#Icebreakers played </td>
        <td scope="col"><?php echo $row->NumberGame ?></td>
    </tr>
    <tr>
        <td class="bold" >#Chat between the two users</td>
        <td scope="col"><?php echo $row->NumberOfChat ?></td>
    </tr>
    <tr>
        <td class="bold" >Interests</td>
        <td>
            <?php if ($interest):?>
                <?php foreach($interest as $obj):?>
                    <span> <?php echo $obj->Name?></span> <br />
                <?php endforeach; ?>
            <?php endif;?>
        </td>
    </tr>
    <tr>
        <td class="bold">IceBreakers</td>
        <td>
            <?php if ($icebreakers):?>
            <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
                <tr>
                    <th>Question</th>
                    <th>Answer</th>
                </tr>
                <?php foreach($icebreakers as $obj):?>
                <tr>
                    <td>
                    <?php echo $obj->Question?>
                    </td>
                    <td>
                    <span style="float:right">
                    <?php echo $obj->Answer?>
                    </span>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            <?php endif;?>
        </td>
    </tr>
    <?php //var_dump($interest)?>
    <?php //var_dump($icebreakers)?>
</table>

<?php endif; ?>
