<?php include '_header.php'; ?>
<div id="content">
    <div id="main">
        <div class="highlight">
            <div class="lev4">
            </div>
            <h2>Anonymous Post</h2>
        </div>
        <br class="clear">
        
        <div style='color:red'>Found <?php echo $total ?> result(s)</div>
        <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>
        
            <?php if (count($results) > 0): ?>
                <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
                    <tbody>

                    <th scope="col" width="10%">UserID</th>
                    <th scope="col" width="20%">UserName</th>
                    <th scope="col" width="">Content</th>
                    <th scope="col" width="15%">Image</th>
                    <th scope="col" width="15%">CreatedDate</th>
                    </tr>

                    <?php foreach ($results as $row): ?>
                        <tr>
                            <td><?php echo $row->FromUserID ?></td>
                            <td style="max-width: 200px"><?php echo $row->UserName ?></td>
                            <td style="max-width: 500px"><?php echo $this->Dashboard_model->formatMsg($row->Message) ?></td>
                            <td>
                                <?php if(!empty($row->Image)):?>
                                    <img src="<?php echo $row->Image;?>" style="width:80px; height:80px;"/>
                                <?php endif; ?>
                            </td>
                            <td><?php echo date('Y-m-d H:i:s', strtotime(PST.' hour', strtotime($row->CreatedDate)))?></td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            <?php endif; ?>
   
    </div>
    <br class="clear">
</div>
<?php include '_footer.php'; ?>