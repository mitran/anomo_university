<?php include '_header.php';?>
<div id="content">
      <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                 <li><a href="<?php echo base_url()?>admin/interests/cats"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">Tag Category</a></li>  
                <li><a href="<?php echo base_url()?>admin/interests/post"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_13.gif" alt="" border="0" height="39" width="32">Create</a></li>
              </ul>
            </div>
            <h2>Interests</h2>
          </div>
          <br class="clear">
         
          <div style='color:red'>Found <?php echo  $total ?> result(s)</div>
          <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param)?></div>
          <?php if (count($results) > 0):?>
          <form action="<?php echo base_url()?>admin/interests/index" id="fm_filter_gift" name="fm_filter_gift" method="post">
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <td colspan="11" width="5%">
			Category: <?php echo form_dropdown('category', $categories, isset($this->uri->vn_param['category'])?$this->uri->vn_param['category']:'', "id='category' style='width:100px;'")?>
			<input value="Filter" type="submit" name='btn_submit' id="btn_submit" onclick="filter_interests()">
			<input value="Cancel" type="reset" name='cancel_list' id="cancel_list" onclick="javascript:window.location='<?php echo base_url()?>admin/interests/index'">
                </td>
              </tr>
              <tr>
                <th scope="col" width="5%"> ID </th>
                <th scope="col" width="15%">Name </th>
                <th scope="col" width="">Category</th>
                <th scope="col" width="">Action</th>
              </tr>
            
            <?php foreach ($results as $row): ?>
              <tr>
                <td><?php echo $row->TagID?></td>
                <td><?php echo $row->Name?></td>
                <td><?php echo @$categories[$row->TagCategoryID]?></td>
 
                <td><a href="<?php echo base_url()?>admin/interests/post/<?php echo $row->TagID?>">edit </a></td>
              </tr>
            <?php endforeach;?>
            <?php endif;?>
            </tbody>
          </table>
          </form>
      </div>
      <br class="clear">
    </div>
<?php include '_footer.php';?>