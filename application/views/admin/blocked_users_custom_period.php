<?php include '_header.php';?>
<script type="text/javascript"
	src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript"
	src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-ui-1.8.9.custom.min.js"></script>
<script type="text/javascript"
	src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery.datetimepicker.js"></script>
<link
	href="<?php echo $this->config->item('base_url');?>public/admin/css/smoothness/jquery-ui-1.8.9.custom.css"
	rel="stylesheet" type="text/css" media="screen">
<link
	href="<?php echo $this->config->item('base_url');?>public/admin/css/add_datetimepicker.css"
	rel="stylesheet" type="text/css" media="screen">

<script type="text/javascript">
$(function() {
	var link = "<?php echo base_url() ?>admin/blocked_users/customperiod";
	var flag;
	$("#submit").click(function() {	
        flag += 1;	
		var startdate = $("#startdate").val();
		var enddate = $("#enddate").val();
		var dataString = $("#custom").serialize();

		if(startdate == '' ) 
		{
  			$('.error').fadeOut(500).show();
  			$("#startdate").focus();

		}
		else if(enddate == '' ) 
		{
  			$('.error').fadeOut(500).show();
  			$("#enddate").focus(); 			
 
		}
		else if(startdate > enddate){
			$('.error').fadeOut(500).show();
		}
		else
		{
			$('.spinner').show();
			$(".submit").attr("disabled", true);
  			$.ajax({
    			type: 'post',
    			url: link,
    			data: dataString,    	
    			success: function(data){
                	var container = $('#data');
    				container.append(data);
    			},
    	        complete: function(){
    	        	$('.spinner').hide();
   	        		$(".submit").attr("disabled", true);	        	
    	        }
  			});
		}
		return false;
	});
	$( "#startdate" ).datepicker({dateFormat: "yy-mm-dd"});
	$( "#enddate" ).datepicker({dateFormat: "yy-mm-dd"}); 


});
</script>

<h2>Blocked Users</h2>

<div class="spinner">
	<img class="img-spinner"
		src='<?php echo base_url(); ?>public/admin/images/ajax-loader.gif'
		id="img-spinner" alt="Loading" />
</div>

<form action="<?php echo base_url()?>admin/blocked_users/customperiod"
	id="custom" name="custom" method="post">

	<table class="tblAdminDetail" border="0" cellpadding="0"
		cellspacing="1" width="100%">
		<tbody>
			<tr>
				<th width="10%">Start Date</th>
				<th width="10%">End Date</th>

			</tr>
		</tbody>

		<tr>
			<td><input type="text" name="startdate" id="startdate" /></td>
			<td><input type="text" name="enddate" id="enddate" /></td>
		</tr>
		<tr>
			<td colspan=12><input id = "submit" class = "submit" value="Submit" type="submit"></td>
		</tr>
	</table>
	<span class="error" style="display: none"> Please Enter Valid Data</span>
</form>
<div id="data"></div>
<br class="clear">
<br>
<br>
<?php include '_footer.php'; ?>