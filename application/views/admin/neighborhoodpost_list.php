<?php include '_header.php';?>
<div id="content">
      <div id="main">
          <div class="highlight">
            <div class="lev4">
             
            </div>
            <h2>Neighborhood Post</h2>
          </div>
          <br class="clear">
          <div class='color-red'><?php echo (isset($this->uri->vn_param['delete']))?$this->uri->vn_param['delete']:''?></div>
          <div style='color:red'>Found <?php echo  $total ?> result(s)</div>
          <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param)?></div>
          
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <td colspan="11" width="5%">
                <form action="<?php echo base_url()?>admin/neighborhoodpost/index" id="fm_filter_user" name="fm_filter_user" method="post">
				Keyword: <input name="keyword" value="<?php echo isset($this->uri->vn_param['keyword'])?htmlspecialchars($this->uri->vn_param['keyword']):''?>" type="text" id="keyword">
				<?php $status = array('' => '---select---',0 => 'Inactive', 1 => 'Active')?>
				Status: <?php echo form_dropdown('status', $status, isset($this->uri->vn_param['status'])?$this->uri->vn_param['status']:'', "id='status' style='width:100px;'")?>
				<input value="Filter" type="submit" name='btn_submit' id="btn_submit" onclick="filter_user()">
				<input value="Cancel" type="reset" name='cancel_list' id="cancel_list" onclick="javascript:window.location='<?php echo base_url()?>admin/neighborhoodpost'">
                </form>
                </td>
              </tr>
             
              <tr>
              	<th>
				<?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'date-asc' || $this->uri->vn_param['sort'] == 'date-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'date-asc'):?>	
                		<a href="<?php echo uri_sort('date-desc')?>" class='color-red'>Created Date<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('date-asc')?>" class='color-red'>Created Date<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('date-asc')?>">Created Date</a>
                <?php endif;?>
				</th>
				<th>User Name</th>
              	<th>Place</th>
				<th>Post</th>
              	<th>Image</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            
            <?php if (count($results) > 0):?>
			<?php foreach ($results as $row): ?>
              <tr>
              	<td><?php echo $row->CreatedDate?></td>
              	<td><?php echo $row->Username?></td>
              	<td><?php echo $row->Name?></td>
              	<td>
                    <div id="viewcontent<?php echo $row->ID?>"><?php echo $row->Content?></div>
                    <div id="editcontent<?php echo $row->ID?>" style="display: none;" >
                        <form action="<?php echo base_url()?>admin/neighborhoodpost/post/<?php echo $row->ID?>" 
                                  id="frmeditcontent<?php echo $row->ID?>"
                                  name="frmeditcontent<?php echo $row->ID?>" 
                                  method="post"
                                  >
                            <textarea 
                                id="content<?php echo $row->ID?>" 
                                name="content" >
                                <?php echo @$row->Content?>
                            </textarea>
                        </form>
                    </div>
               </td>
              	<td><img src="<?php echo $this->Image_model->resize(@$row->Photo, 120, 100)?>" /></td>
              	<td>
              		<?php 
              			if(!isset($row->Status) || $row->Status ==1)
                            echo "Active";
                        else
                            echo "Inactive";
              		?>
              	</td>
            	 <td>
                    <div id="viewaction<?php echo $row->ID?>">
                        <a id="edit<?php echo $row->ID?>" href="#" onclick="showeditform(<?php echo $row->ID?>)">Edit</a>
                        <?php if(!isset($row->Status) || $row->Status ==1){?>
                        &nbsp;|<a id="delete<?php echo $row->ID?>" href="#" onclick="updatestatus(<?php echo $row->ID?>,0)">Delete</a>
                        <?php }?>
                    </div>
                    <div id="editaction<?php echo $row->ID?>" style="display: none;">
                        <a id="save<?php echo $row->ID?>" href="#" onclick="submiteditform(<?php echo $row->ID?>)">Save</a>&nbsp;|
                        <a id="cancel<?php echo $row->ID?>" href="#" onclick="canceleditform(<?php echo $row->ID?>)">Cancel</a>
                    </div>
                 </td>
              </tr>
           <?php endforeach;?>
           <?php endif;?>
           	</tbody>
          </table>
		  
</div>
<br class="clear">
</div>

<input type="hidden" value="<?php echo base_url()?>"/>
<script lang="javascript">
   
    
    //display edit form
    function showeditform(id){
        document.getElementById("editcontent"+id).style.display="block";
        document.getElementById("editaction"+id).style.display="block";
        document.getElementById("viewcontent"+id).style.display="none";
        document.getElementById("viewaction"+id).style.display="none";
        
        //set data for edit form
        var view_element = document.getElementById("viewcontent"+id);
        var view_value = view_element.innerHTML;
        var content_element = document.getElementById("content"+id);
        
        //set style and focus for Content textarea
        content_element.style.width = "300px";
        content_element.style.height = "100px";
        content_element.focus();
        content_element.value = "";
        content_element.value = view_value;
    }
    
    //cancel edit form
    function canceleditform(id){
        document.getElementById("editcontent"+id).style.display="none";
        document.getElementById("editaction"+id).style.display="none";
        document.getElementById("viewcontent"+id).style.display="block";
        document.getElementById("viewaction"+id).style.display="block";
    }
   
    //set updated data for view div
    function setUpdatedContent(id){
        var temp_element = document.getElementById("content"+id)
        var temp_value = temp_element.value;
        document.getElementById("viewcontent"+id).innerHTML = temp_value;
    }
    
    //ajax submit form
    function submiteditform(id)
	{    
		nocache = Math.random();
        var frm_id = 'frmeditcontent'+id;
        var post_url = base_url+'admin/neighborhoodpost/post/'+id+'/'+nocache;
         
         $.ajax({
            type:'POST', 
            url: post_url, 
            data:$('#'+frm_id).serialize(), 
            success: function(response) {
                var response = response;
    			if(response.toLowerCase() == 'true'){
                    setUpdatedContent(id);
                    canceleditform(id);
    			}else{
                    canceleditform(id);
                }
            }
        });
	}
    
    //ajax submit form
    function updatestatus(id,status)
	{    
	   var msg = confirm('Are you sure you would like to delete this post?');
        if(msg == true) {
            jQuery('#delete'+id).attr('href', base_url+'admin/neighborhoodpost/updatestatus/'+id+'/'+status);
        }else{
            return false;
        }
	}
	
    //keep page position on Anchor click
    $('a[href^="#"]').click(function(e) {
        e.preventDefault();
    });
</script>
<?php include '_footer.php';?>