<?php include '_header.php'; ?>
<div id="content">
    <div id="main">
        <div class="highlight">
            <h2>Marketing</h2>
        </div>
        <br class="clear">
        <div class="highlight" style="text-align: left">
            <a href="<?php echo base_url() ?>admin/marketing/value">Value</a> | 
            <!--<a href="<?php echo base_url() ?>admin/marketing/schedule">Schedule</a> |--> 
            <a  class="active menu-picture" href="<?php echo base_url() ?>admin/marketing/core_feature_engagement">Core Feature Engagement</a> |
            <a  href="<?php echo base_url() ?>admin/marketing/core_feature_engagement_new">New Core Feature Engagement</a> |
            <a  href="<?php echo base_url() ?>admin/marketing/content_engagement">Content Engagement</a> | 
            <a  href="<?php echo base_url() ?>admin/marketing/life_cycle_tracking">Life Cycle Tracking</a> |  
            <a  href="<?php echo base_url() ?>admin/marketing/kpi">KPI</a> |
            <a  href="<?php echo base_url() ?>admin/marketing/social_media">Social Media</a>       
        </div>
        <br class="clear">
        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="80%">
            <tbody>
                <tr>
                    <th><b>Week</b></th>
                    <th><b>#Posts</b></th>
                    <th><b>#Comments</b></th>
                    <th><b>#Likes</b></th>
                    <th><b>#Followers Added</b></th>
                    <th><b>#Ice Breakers</b></th>
                    <th><b>#Chat Sessions</b></th>
                    <th><b>#Chat Messages</b></th>
                    <th><b>#Reveals</b></th>
                    <th><b>#Checkins</b></th>
                </tr>
				<?php foreach ($result as $row):?>
                <tr>
                    <td width="25%"><?php echo $row['Week']?></td>
                    <td><?php echo $row['Posts']?></td>
                    <td><?php echo $row['Comments']?></td>
                    <td><?php echo $row['Likes']?></td>
                    <td><?php echo $row['FollowersAdded']?></td>
                    <td><?php echo $row['IceBreakers']?></td>
                    <td><?php echo $row['ChatSessions']?></td>
                    <td><?php echo $row['ChatMessages']?></td>
                    <td><?php echo $row['Reveals']?></td>
                    <td><?php echo $row['Checkins']?></td>
                </tr>
                <?php endforeach;?>

                 
            </tbody>
        </table>
    </div>
    <br class="clear">
</div>

<?php include '_footer.php'; ?>
