<?php include '_header.php';?>
<div id="content">
    <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <li><a href="<?php echo base_url()?>admin/lplaces"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">Back to list</a></li>
              </ul>
            </div>
            <h2>Posting Local Place</h2>
          </div>
         <div style="color:red">
         	<?php echo isset($this->validation->error_string)?$this->validation->error_string:''; ?>
            <?php echo isset($localplaceid_error)?$localplaceid_error:''; ?>
         </div>
         <?php echo form_open_multipart('admin/lplaces/post/'.$id)?>
         <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
			 <tr>
                <td>Name <span class="color-red">(*)</span></td>
                <td><input type="text" name="Name" size=60 value="<?php echo isset($this->validation->Name)?$this->validation->Name:@$info->Name?>"></td>
              </tr>
              <tr>
                <td>Lat <span class="color-red">(*)</span></td>
                <td><input type="text" name="Lat" size=60 value="<?php echo isset($this->validation->Lat)?$this->validation->Lat:@$info->Lat?>"></td>
              </tr>
              <tr>
                <td>Lon <span class="color-red">(*)</span></td>
                <td><input type="text" name="Lon" size=60 value="<?php echo isset($this->validation->Lon)?$this->validation->Lon:@$info->Lon?>"></td>
              </tr>
              <tr>
                <td>Address <span class="color-red">(*)</span></td>
                <td><input type="text" name="Address" size=60 value="<?php echo isset($this->validation->Address)?$this->validation->Address:@$info->Address?>"></td>
              </tr>
              <tr>
                <td>Category <span class="color-red">(*)</span></td>
                <td>
                   <table border="0" width="100%">
        			  	
        				<?php
                        
        				$row = round(count($optionCategories)/5);
        				for($i=0;$i<$row;$i++){
        				?>
        				<tr>
        					<?php 
        					
        					for($j=0;$j<5;$j++){
        						$index = $i*5+$j;
        						if(isset($optionCategories[$index]['Name'])){
        							$label = str_replace("_"," ",$optionCategories[$index]['Name']);
        							$label = ucfirst($label);
        						?>
        					<td>
                                <input type="checkbox" 
                                       name="Category[]"
                                       value="<?php echo $optionCategories[$index]['Name'];?>" 
                                       <?php if(isset($arr_category) && in_array($optionCategories[$index]['Name'],$arr_category)){?>checked<?php }?>/>
                                                                              
                            </td>
        					<td><?php echo $label;?></td>
        						<?php 
        						}else{
        						?>
        					<td>&nbsp;</td>
        					<?php
        						}
        					}
        					?>
        				</tr>
        				<?php 
        				}
        				?>
        			  </table>
                </td>
              </tr>
              <tr>
                <td>Provider <span class="color-red">(*)</span></td>
                <td><input type="text" name="Provider" size=60 value="local" readonly="true"></td>
              </tr>
             
              <tr>
                <td colspan="2" align="center"><input value="Submit" type="submit" name="submit" id="submit" > &nbsp <input value="Cancel" type="reset" ></td>
             </tr>
			 </tbody>
          </table>
   </div>
   <br class="clear">
</div>
<?php include '_footer.php';?>