 <div class="scroll" id = "content_wrap" ><b>Content</b>
<br>
<?php $tz = new DateTimeZone('America/Los_Angeles'); ?>
<?php if (count($chatInfo) > 0): ?>
        <?php foreach ($chatInfo as $rowchatInfo): ?>
        	<div style="width: 500px;">
        	<b><?php echo $rowchatInfo->SendUserName?></b>:        	
        	<?php if(preg_match('/"message":"https?/',$rowchatInfo->Content)):?>
        		<?php echo '<div style="width: 200px;"><img style="max-width:200px;" src=' .$this->Dashboard_model->formatMsg($rowchatInfo->Content) . ' /></div>'; ?>        		   	
        	<?php elseif(preg_match('%^((https?://)|(www\.))([a-z0-9-].?)+(:[0-9]+)?(/.*)?$%i',$rowchatInfo->Content)):?>
        		<?php echo '<div style="width: 200px;"><img style="max-width:200px;" src= $rowchatInfo->Content /></div>'; ?>
        	<?php elseif(preg_match('/https?/',$rowchatInfo->Photo)):?>
        		<?php echo '<div style="width: 200px;"><img style="max-width:200px;" src=' .$this->Dashboard_model->formatMsg($rowchatInfo->Photo) . ' /></div>'; ?>        	
        	<?php else:?>
	        	<?php echo $rowchatInfo->Content;?></div>	        	    	
        	<?php endif;?>
        	<?php $date = new DateTime($rowchatInfo->CreatedDate);
					$date->setTimeZone($tz);
					echo $date->format('Y-m-d H:i:s'); ?>      	
        <?php endforeach;?>	
<?php endif;?>
</div>
<div class="clear"></div>