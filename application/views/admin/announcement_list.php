<?php include '_header.php'; ?>
<div id="content">
    <div id="main">
        <div class="highlight">
            <div class="lev4">
                <ul>
                    <li><a href="<?php echo base_url() ?>admin/announcement/closed"><img src="<?php echo $this->config->item('base_url'); ?>public/admin/images/ico_lev4_09.gif" alt="" border="0" height="39" width="32">Announcement Closed</a></li>
                    <li><a href="<?php echo base_url() ?>admin/announcement/post"><img src="<?php echo $this->config->item('base_url'); ?>public/admin/images/ico_lev4_13.gif" alt="" border="0" height="39" width="32">Create</a></li>
                </ul>
            </div>
            <h2>Announcement Card</h2>
        </div>
        <br class="clear">
        <div class='color-red'><?php echo (isset($this->uri->vn_param['delete'])) ? 'Delete successful': '' ?></div>
        <div style='color:red'>Found <?php echo $total ?> result(s)</div>
        <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>
        <form action="<?php echo base_url() ?>admin/announcement/index" id="fm_filter_credit" name="fm_filter_credit" method="post">
            <?php if (count($results) > 0): ?>
                <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
                    <tbody>

                    <th scope="col" width="15%">Created Date (PST Time)</th>
                    <th scope="col" width="15%">Type</th>
                    <th scope="col" width="">Title</th>
                    <th scope="col" width="">Content</th>
                    <th scope="col" width="">RefType</th>
                    <th scope="col" width="">RefID</th>
                    <th scope="col" width="15%">Expire Date (PST Time)</th>
                    <th scope="col" width="10%">Action</th>
                    </tr>

                    <?php foreach ($results as $row): ?>
                        <tr>
                            <td><?php echo date('Y-m-d H:i:s', strtotime('-7 hour', strtotime($row->CreatedDate))) ?></td>
                            <td><?php
                                $types = array('1' => 'New Users', '2' => 'Old Users');
                                echo in_array($row->Type, array_keys($types))?$types[$row->Type]:'';
                                ?>
                            </td>
                            <td><?php echo $row->Title ?></td>
                            <td><?php echo $row->Content ?></td>
                            <td><?php
                                $reftypes = array('1' => 'Message', '2' => 'Fan page');
                                echo in_array($row->RefType, array_keys($reftypes))?$reftypes[$row->RefType]:'';
                                ?>
                            </td>
                            <td><?php echo $row->RefID ?></td>
                            <td><?php echo date('Y-m-d', strtotime('-7 hour', strtotime($row->ExpireDate))) ?></td>
                            <td>
                                <a href="<?php echo base_url() ?>admin/announcement/post/<?php echo $row->ID ?>" title="edit">edit </a>
                               | <a href="#" onclick="delete_announcement(<?php echo $row->ID ?>)" id="announcement_del_<?php echo $row->ID ?>" title="delete">delete </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            <?php endif; ?>
        </form>
    </div>
    <br class="clear">
</div>
<?php include '_footer.php'; ?>