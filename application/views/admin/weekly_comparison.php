<?php include '_header.php';?>
<div id="content">
    <div id="main">
        <div class="highlight">
            <h2>Weekly Comparison</h2>
        </div>
        <br class="clear">
        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="80%">
            <tbody>
                <tr>
                    <th></th>
                    <th><b>Week 12</b></th>
                    <th><b>Week 11</b></th>
                    <th><b>Week 10</b></th>
                    <th><b>Week 9</b></th>
                    <th><b>Week 8</b></th>
                    <th><b>Week 7</b></th>
                    <th><b>Week 6</b></th>
                    <th><b>Week 5</b></th>
                    <th><b>Week 4</b></th>
                    <th><b>Week 3</b></th>
                    <th><b>Week 2</b></th>
                    <th><b>Current Week</b></th>
                </tr>
                <tr>
                    <td width="25%"><b>Total Users</b> </td>
                    <td><?php echo $register_last11; ?></td>
                    <td><?php echo $register_last10; ?></td>
                    <td><?php echo $register_last9; ?></td>
                    <td><?php echo $register_last8; ?></td>
                    <td><?php echo $register_last7; ?></td>
                    <td><?php echo $register_last6; ?></td>
                    <td><?php echo $register_last5; ?></td>
                    <td><?php echo $register_last4; ?></td>
                    <td><?php echo $register_last3; ?></td>
                    <td><?php echo $register_last2; ?></td>
                    <td><?php echo $register_last; ?></td>
                    <td><?php echo $register_current; ?></td>
                </tr>
                
                <tr>
                    <td width="25%"><b>Verified Users</b> </td>
                    <td><?php echo $verified_last11; ?></td>
                    <td><?php echo $verified_last10; ?></td>
                    <td><?php echo $verified_last9; ?></td>
                    <td><?php echo $verified_last8; ?></td>
                    <td><?php echo $verified_last7; ?></td>
                    <td><?php echo $verified_last6; ?></td>
                    <td><?php echo $verified_last5; ?></td>
                    <td><?php echo $verified_last4; ?></td>
                    <td><?php echo $verified_last3; ?></td>
                    <td><?php echo $verified_last2; ?></td>
                    <td><?php echo $verified_last; ?></td>
                    <td><?php echo $verified_current; ?></td>
                </tr>
                
                <tr>
                    <td width="25%"><b>Unverified Users</b> </td>
                    <td><?php echo $unverified_last11; ?></td>
                    <td><?php echo $unverified_last10; ?></td>
                    <td><?php echo $unverified_last9; ?></td>
                    <td><?php echo $unverified_last8; ?></td>
                    <td><?php echo $unverified_last7; ?></td>
                    <td><?php echo $unverified_last6; ?></td>
                    <td><?php echo $unverified_last5; ?></td>
                    <td><?php echo $unverified_last4; ?></td>
                    <td><?php echo $unverified_last3; ?></td>
                    <td><?php echo $unverified_last2; ?></td>
                    <td><?php echo $unverified_last; ?></td>
                    <td><?php echo $unverified_current; ?></td>
                </tr>
                
                <tr>
                    <td width="25%"><b>DAU</b> </td>
                    <td><?php echo $dau_last11; ?></td>
                    <td><?php echo $dau_last10; ?></td>
                    <td><?php echo $dau_last9; ?></td>
                    <td><?php echo $dau_last8; ?></td>
                    <td><?php echo $dau_last7; ?></td>
                    <td><?php echo $dau_last6; ?></td>
                    <td><?php echo $dau_last5; ?></td>
                    <td><?php echo $dau_last4; ?></td>
                    <td><?php echo $dau_last3; ?></td>
                    <td><?php echo $dau_last2; ?></td>
                    <td><?php echo $dau_last; ?></td>
                    <td><?php echo $dau_current; ?></td>
                </tr>
                
                <tr>
                    <td width="25%"><b>iPhone Users  </b> </td>
                    <td><?php echo $ios_last11; ?></td>
                    <td><?php echo $ios_last10; ?></td>
                    <td><?php echo $ios_last9; ?></td>
                    <td><?php echo $ios_last8; ?></td>
                    <td><?php echo $ios_last7; ?></td>
                    <td><?php echo $ios_last6; ?></td>
                    <td><?php echo $ios_last5; ?></td>
                    <td><?php echo $ios_last4; ?></td>
                    <td><?php echo $ios_last3; ?></td>
                    <td><?php echo $ios_last2; ?></td>
                    <td><?php echo $ios_last; ?></td>
                    <td><?php echo $ios_current; ?></td>
                </tr>
                
                <tr>
                    <td width="25%"><b>Android Users</b> </td>
                    <td><?php echo $android_last11; ?></td>
                    <td><?php echo $android_last10; ?></td>
                    <td><?php echo $android_last9; ?></td>
                    <td><?php echo $android_last8; ?></td>
                    <td><?php echo $android_last7; ?></td>
                    <td><?php echo $android_last6; ?></td>
                    <td><?php echo $android_last5; ?></td>
                    <td><?php echo $android_last4; ?></td>
                    <td><?php echo $android_last3; ?></td>
                    <td><?php echo $android_last2; ?></td>
                    <td><?php echo $android_last; ?></td>
                    <td><?php echo $android_current; ?></td>
                </tr>
                
                <tr>
                    <td width="25%"><b>Ice Breaker Games</b> <br />(number of games played)</td>
                    <td><?php echo $game_last11; ?></td>
                    <td><?php echo $game_last10; ?></td>
                    <td><?php echo $game_last9; ?></td>
                    <td><?php echo $game_last8; ?></td>
                    <td><?php echo $game_last7; ?></td>
                    <td><?php echo $game_last6; ?></td>
                    <td><?php echo $game_last5; ?></td>
                    <td><?php echo $game_last4; ?></td>
                    <td><?php echo $game_last3; ?></td>
                    <td><?php echo $game_last2; ?></td>
                    <td><?php echo $game_last; ?></td>
                    <td><?php echo $game_current; ?></td>
                </tr>
                
                <tr>
                    <td width="25%"><b>Comments</b> <br />(# of comments posted)</td>
                    <td><?php echo $comment_last11; ?></td>
                    <td><?php echo $comment_last10; ?></td>
                    <td><?php echo $comment_last9; ?></td>
                    <td><?php echo $comment_last8; ?></td>
                    <td><?php echo $comment_last7; ?></td>
                    <td><?php echo $comment_last6; ?></td>
                    <td><?php echo $comment_last5; ?></td>
                    <td><?php echo $comment_last4; ?></td>
                    <td><?php echo $comment_last3; ?></td>
                    <td><?php echo $comment_last2; ?></td>          
                    <td><?php echo $comment_last; ?></td>
                    <td><?php echo $comment_current; ?></td>
                </tr>
                <tr>
                    <td width="25%"><b>Likes</b> <br />(# of likes)</td>
                    <td><?php echo $like_last11; ?></td>
                    <td><?php echo $like_last10; ?></td>
                    <td><?php echo $like_last8; ?></td>
                    <td><?php echo $like_last8; ?></td>
                    <td><?php echo $like_last7; ?></td>
                    <td><?php echo $like_last6; ?></td>
                    <td><?php echo $like_last5; ?></td>
                    <td><?php echo $like_last4; ?></td>
                    <td><?php echo $like_last3; ?></td>
                    <td><?php echo $like_last2; ?></td>      
                    <td><?php echo $like_last; ?></td>
                    <td><?php echo $like_current; ?></td>
                </tr>
                <tr>
                    <td width="25%"><b>Pictures Activity</b> <br />(# of Pictures uploaded from a users profile )</td>
                    <td><?php echo $picture_last11; ?></td>
                    <td><?php echo $picture_last10; ?></td>
                    <td><?php echo $picture_last9; ?></td>
                    <td><?php echo $picture_last8; ?></td>
                    <td><?php echo $picture_last7; ?></td>
                    <td><?php echo $picture_last6; ?></td>
                    <td><?php echo $picture_last5; ?></td>
                    <td><?php echo $picture_last4; ?></td>
                    <td><?php echo $picture_last3; ?></td>
                    <td><?php echo $picture_last2; ?></td>       
                    <td><?php echo $picture_last; ?></td>
                    <td><?php echo $picture_current; ?></td>
                </tr>
                
                <tr>
                    <td width="25%"><b>Gift sent</b></td>
                    <td><?php echo $gift_last11; ?></td>
                    <td><?php echo $gift_last10; ?></td>
                    <td><?php echo $gift_last9; ?></td>
                    <td><?php echo $gift_last8; ?></td>
                    <td><?php echo $gift_last7; ?></td>
                    <td><?php echo $gift_last6; ?></td>
                    <td><?php echo $gift_last5; ?></td>
                    <td><?php echo $gift_last4; ?></td>
                    <td><?php echo $gift_last3; ?></td>
                    <td><?php echo $gift_last2; ?></td>       
                    <td><?php echo $gift_last; ?></td>
                    <td><?php echo $gift_current; ?></td>
                </tr>
                
                <tr>
                    <td width="25%"><b>Activity Feeds</b></td>
                    <td><?php echo $activity_last11; ?></td>
                    <td><?php echo $activity_last10; ?></td>
                    <td><?php echo $activity_last9; ?></td>
                    <td><?php echo $activity_last8; ?></td>
                    <td><?php echo $activity_last7; ?></td>
                    <td><?php echo $activity_last6; ?></td>
                    <td><?php echo $activity_last5; ?></td>
                    <td><?php echo $activity_last4; ?></td>
                    <td><?php echo $activity_last3; ?></td>
                    <td><?php echo $activity_last2; ?></td>       
                    <td><?php echo $activity_last; ?></td>
                    <td><?php echo $activity_current; ?></td>
                </tr>
                
            </tbody>
        </table>
    </div>
    <br class="clear">
</div>

<?php include '_footer.php';?>
