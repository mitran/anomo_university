<?php include '_header.php'; ?>
<script>
jQuery(function() {
    jQuery("img.lazy").lazyload();
});
</script>
<div id="content">
    <div id="main">
        <div class="highlight">
            <div class="lev4">

            </div>
            <h2>List Post</h2>
        </div>

        <br class="clear">
        <div class='color-red'><?php echo (isset($this->uri->vn_param['delete'])) ? 'Delete successful': '' ?></div>
        <div style='color:red'>Found <?php echo $total ?> result(s)</div>
        <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>
        <?php if (count($results) > 0): ?>
        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>

                <tr>
                    <th>UserID</th>
                    <th>User Name</th>
                    <th style="width: 400px">Content</th>
                    <th>Image</th>
                    <th>Topic Name</th>
                    <th>CreatedDate</th>
                    <th>Action</th>
                </tr>

                
                    <?php foreach ($results as $row): ?>
                        <tr>
                            <td><?php echo $row->FromUserID ?></td>
                            <td><?php echo $row->UserName ?></td>
                            <td style="max-width: 400px"><?php echo $this->Dashboard_model->formatMsg($row->Message) ?></td>
                            <td>
                                <a href="<?php echo $row->Image ?>" data-lightbox="<?php echo $row->Image ?>"  >
                                    <img style="max-width:150px;" class="lazy" data-original="<?php echo $row->Image ?>"  />
                                </a>
                            </td>
                            <td><?php echo $row->TopicName ?></td>
                            <td><?php echo date('Y-m-d H:i:s', strtotime(PST.' hour', strtotime($row->CreatedDate)))  ?></td>
                            <td><a href="#" onclick="delete_item('content_<?php echo $row->ActivityID ?>', '<?php echo base_url().'admin/post/delete_content/'.$row->ActivityID?>')" id="content_<?php echo $row->ActivityID ?>">Delete</a></td>
                        </tr>
                    <?php endforeach; ?>
                
            </tbody>
        </table>
        <?php endif; ?>
    </div>
    <br class="clear">
      <p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
</div>
<?php include '_footer.php'; ?>