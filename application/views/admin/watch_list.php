<?php include '_header.php';?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/admin/js/jquery.jeditable.js"></script>
<script>
    function detail_user(user_id){
        jQuery('#detail').html('Loading ......');
        jQuery('#detail').show();
        jQuery('#detail').load(base_url+'admin/user/detail_user/' + user_id, {}, function(){
            jQuery('#detail').show();
            jQuery('.conversation .user_watch_list').removeClass('select_active');
            jQuery('#user_'+user_id).addClass( 'select_active');
        });
    }
</script>

<div id="content">
  <div id="main">
     <div class="highlight" style="text-align: left">            
         <a href="<?php echo base_url() ?>admin/user/interaction"  >NEW USER INTERACTION</a> 
     </div> 
    <div class="highlight">
      <h2> WATCH LIST</h2>
    </div>
    <br class="clear">
    <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>

    <div style="height:20px;"></div>
    <div id="conversation" class="conversation">
    <?php if (isset($result)):?>
    <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
            <tr>
            <th scope="col" width="15%">UserID</th>
            <th scope="col" width="15%">UserName</th>
            </tr>

            <?php foreach ($result as $row): ?>
                <tr>
                    <td colspan="2">
                        <div class="user_watch_list" id="user_<?php echo $row->UserID?>"onclick="detail_user(<?php echo $row->UserID?>)">
                            <span style="float:left;"><?php echo $row->UserID  ?></span>
                            <span style="float:right;"><?php echo $row->UserName ?></span>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
    <?php endif;?>
    </div>
    <div id="detail" class="detail" style="display:none;"></div>
  </div>
    
  <br class="clear">
</div>

<?php include '_footer.php';?>

