<?php include '_header.php'; ?>
<div id="content">
    <div id="main">
        <div class="highlight" style="text-align: left">            
            <a href="<?php echo base_url() ?>admin/promos" class="active menu-picture" >Promos Post</a> | 
            <a  href="<?php echo base_url() ?>admin/promos/fanpage">Fan's Posts</a> 
          </div>
        <div class="highlight">
            <div class="lev4">
                <ul>
                    <li><a href="<?php echo base_url() ?>admin/promos"><img src="<?php echo $this->config->item('base_url'); ?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">Back to list</a></li>
                </ul>
            </div>
            <h2>Posting Promos</h2>
        </div>
        <div style="color:red">
            <?php echo validation_errors(); ?>
            <?php echo @$upload_error; ?>
            
        </div>
        <?php echo form_open_multipart('admin/promos/post/' . $id) ?>
        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
                <tr>
                    <td>Select vendor<span class="color-red"> (*)</span></td>
                    <td>
                        <?php echo form_dropdown('user_id', $vendors, isset($this->form_validation->user_id) ? $this->form_validation->user_id : @$info->UserID, "style='width:200px;'") ?>
                    </td>
                </tr>
                <tr>
                    <td>Content <span class="color-red"> (*)</span></td>
                    <td><input type="text" name="content" size=60 value="<?php echo isset($this->form_validation->content) ? $this->form_validation->content : @$info->Content ?>"></td>
                </tr>
                <tr>
                    <td>Photo </td>
                    <td>
                        <input type="file" name="photo" />
                        <?php if ($id > 0): ?>
                            <img src="<?php echo $this->config->item('s3_photo_url') . $info->Photo ?>" width="120" height="100" />
                        <?php endif; ?>
                    </td>
                </tr>

                <tr>
                    <td>Link <span class="color-red"> (*)</span></td>
                    <td><input type="text" name="link" size=100 value="<?php echo isset($this->form_validation->link) ? $this->form_validation->link : @$info->Link; ?>"></td>
                </tr>

                <tr>
                    <td>Publish</td>
                    <td><input type="checkbox" name="is_published" size=100 value="1" <?php echo ((isset($this->form_validation->is_published) && $this->form_validation->is_published == 1) || @$info->IsPublished == 1 )? "checked": ''?>></td>
                </tr>
                
                <tr>
                    <td colspan="2" align="center"><input value="Submit" type="submit" name="submit" id="submit" > &nbsp <input value="Cancel" type="reset" ></td>
                </tr>
            </tbody>
        </table>
    </div>
    <br class="clear">
</div>
<?php include '_footer.php'; ?>