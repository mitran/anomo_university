<?php include '_header.php';?>
<div id="content">
    <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <li><a href="<?php echo base_url()?>admin/credit"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">Back to list</a></li>
              </ul>
            </div>
            <h2>Posting Credit</h2>
          </div>
         <div style="color:red">
         	<?php echo validation_errors(); ?>
         </div>
         <?php echo form_open_multipart('admin/credit/post/'.$credit_id)?>
         <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
             <tr>
                <td>Platform <span class="color-red">(*)</span></td>
                <td>
                	<?php echo form_dropdown('platform', array('ios' => 'ios', 'android' => 'android'), isset($this->validation->platform)?$this->validation->platform:@$info->Platform, "style='width:100px'")?>
                </td>
              </tr>
              <tr>
                <td>Credits <span class="color-red">(*)</span></td>
                <td><input type="text" name="credits" size=60 value="<?php echo isset($this->form_validation->credits)?$this->form_validation->credits:@$info->Credits?>"></td>
              </tr>
			 <tr>
                <td>Price <span class="color-red">(*)</span></td>
                <td><input type="text" name="price" size=60 value="<?php echo isset($this->form_validation->price)?$this->form_validation->price:@$info->Price?>"></td>
              </tr>
              <tr>
                <td>ProductID <span class="color-red">(*)</span></td>
                <td><input type="text" name="product_id" size=60 value="<?php echo isset($this->form_validation->product_id)?$this->form_validation->product_id:@$info->ProductID?>"></td>
              </tr>
              <tr>
                <td colspan="2" align="center"><input value="Submit" type="submit" name="submit" id="submit" > &nbsp <input value="Cancel" type="reset" ></td>
             </tr>
			 </tbody>
          </table>
   </div>
   <br class="clear">
</div>
<?php include '_footer.php';?>