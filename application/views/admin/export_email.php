<?php include '_header.php';?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-ui-1.8.9.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery.datetimepicker.js"></script>

<link href="<?php echo $this->config->item('base_url');?>public/admin/css/smoothness/jquery-ui-1.8.9.custom.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/add_datetimepicker.css" rel="stylesheet" type="text/css" media="screen">

<script>
    $(document).ready(function() {
        $("#from").datepicker({dateFormat: "yy-mm-dd"});
        $("#to").datepicker({dateFormat: "yy-mm-dd"});
    });
</script>
<div id="content">
    <div id="main">
        <div class="highlight">
            <h2>Export Email</h2>
        </div>
        <br class="clear">
        <form id="frmExportEmail" method="Post" action="">
        From <input id="from" name="from" value="<?php echo isset($from) ? $from : ''; ?>" type="text" style="width:100px;">
        To <input id="to" name="to" value="<?php echo isset($to) ? $to : ''; ?>" type="text" style="width:100px;">

        <select name="type" id ="type">
            <option value="">Get All emails</option>
            <option <?php echo $type == 'last7Day'?'selected':'' ?> value="last7Day">New emails within the last 7 days</option>
            <option <?php echo $type == 'newEmailsOfflinePreviousDay'?'selected':'' ?> value="newEmailsOfflinePreviousDay">New emails of user who did not log in from previous day</option>
        </select>

        <input type="submit" value="View Only" onclick="view_email()"/>
        
        <input type="button" value="Export" onclick="export_email()"/>
        <input type="button" value="Cancel" onclick="javascript:window.location = '<?php echo base_url() ?>admin/export_email/index'"/>
        </form>
        <br class="clear">
        <br class="clear">
        
        <div style='color:red'>Found <?php echo $total ?> result(s)</div>
        <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>
        <?php if (isset($results) && count($results) > 0):?>
        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <th>UserID</th>
                <th>UserName</th>
                <th>Email</th>
                <th>SignUpDate</th>
            </tr>
            <?php foreach ($results as $row):?>
            <tr>
                <td><?php echo $row->UserID?></td>
                <td><?php echo $row->UserName?></td>
                <td><?php echo $row->Email?></td>
                <td><?php echo $row->SignUpDate?></td>
            </tr>
            <?php endforeach; ?>
        </table>
        
        <?php endif;?>
        
    </div>
    <br class="clear">
</div>

<?php include '_footer.php';?>