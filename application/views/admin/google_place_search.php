<?php include '_header.php';?>
<div id="content">
  <div id="main">
    <div class="highlight">
      <h2>Google Places Search</h2>
    </div>
    <br class="clear">
    <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
      <tbody>
        <tr>
          <td>
            <form action="<?php echo base_url()?>admin/gplaces" id="gplaces" name="gplaces" method="post">
              Latitude: <input name="lat" value="<?php echo isset($lat)?$lat:'';?>" type="text" style="width:100px;">
              Longitude: <input name="lng" value="<?php echo isset($lng)?$lng:'';?>" type="text" style="width:100px;">
			  Radius(m): <input name="radius" id="radius" onkeypress="return isNumberKey(event)" value="<?php echo isset($radius)?$radius:'';?>" type="text" style="width:100px;">
              Keyword: <input name="keyword" value="<?php echo isset($keyword)?$keyword:'';?>" type="text">
              
              Rankby <input type="radio" name="rankby" value="prominence" <?php echo (isset($rankby) && $rankby == 'prominence')?"checked=true":""?>>prominence
              <input type="radio" name="rankby" value="distance" <?php echo (isset($rankby) && $rankby == 'distance')?"checked=true":""?>>distance
              <input value="Search" type="submit" name='btn_submit' id="btn_submit">
              <input value="Cancel" type="reset" name='cancel_list' id="cancel_list">
			  <table border="1">
			  	<tr>
					<td colspan="8"> Category <input type="checkbox" name="all" onclick="checkedAll(this.checked)"> All</td>
				</tr>
				<?php
				$row = round(count($optionCategories)/4);
				for($i=0;$i<$row;$i++){
				?>
				<tr>
					<?php 
					$arr_category = array();
					if(isset($type)){
						$arr_category = array_values($type);
					}
					for($j=0;$j<4;$j++){
						$index = $i*4+$j;
						if(isset($optionCategories[$index])){
							$label = str_replace("_"," ",$optionCategories[$index]);
							$label = ucfirst($label);
						?>
					<td><input type="checkbox" name="type[]" id="<?php echo $optionCategories[$index];?>" value="<?php echo $optionCategories[$index];?>" <?php if(in_array($optionCategories[$index],$arr_category)){?>checked="checked"<?php }?>/></td>
					<td><?php echo $label;?></td>
						<?php 
						}else{
						?>
					<td>&nbsp;</td>
					<?php
						}
					}
					?>
				</tr>
				<?php 
				}
				?>
			  </table>
            </form>
          </td>
        </tr>
        <?php if (isset($results)) {?>
        <tr>
          <td><pre><?php print_r($results);?></pre></td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
  <br class="clear">
</div>
<script>
function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
 	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	if(document.getElementById('radius').value>50000){
		alert('input value radius: 1->50000');
	}
	return true;
}

function checkedAll (checked) {
	for (var i = 0; i < document.getElementsByName('type[]').length; i++) {
	  document.getElementsByName('type[]')[i].checked = checked;
	}
}
</script>
<?php include '_footer.php';?>