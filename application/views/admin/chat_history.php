<?php include '_header.php';?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.4.4.min.js"></script>
<script>
function showDetail(user_id, partner_id){
    $("#detail").show();
    $( "#detail" ).children().hide();
    $('.'+user_id+'_'+partner_id).show();
    $("#active_class").val(user_id+'_'+partner_id);
}

function load_more(user_id){
    var last_time = $('#last_time').val();
    $('#loading').show();
    $('#btn_loading').attr("disabled", "disabled");
    
     $.ajax({
            type:"POST",
            url: base_url+'admin/chat_history/load_more/' + user_id + '/' + last_time,
            data:$('#formMessage').serialize(),
            success:function(response) {
                if (response.details && response.details != ''){ // still have chat history
                    if (response.conversations){
                        $.each( response.conversations, function( i, item ) {
                            var partner_id = item.partner_id;
                            var partner_name = item.partner_name;
                            if ($('#'+partner_id).length == 0) {
                                var html = "<div class='user' id='"+partner_id+"' onclick='showDetail("+user_id+","+partner_id+ ")'>"+partner_name+"</div>";
                                $('#conversation').append(html);
                            }
                        });
                    }

                    $("#detail").prepend(response.details);
                    $("#last_time").val(response.last_time);

                    var active_class = $("#active_class").val();
                    if ( active_class != ''){
                        $('.'+active_class).show();
                    }

                    $('#loading').hide();
                    $('#btn_loading').removeAttr("disabled");
                }else{
                    // no more chat
                    $('#loading').hide();
                    $('#btn_loading').removeAttr("disabled");
                    $('#loadmore').html('No more result!');
                }
            },
            error: function() {
                alert('Error getting chat history, please try again');
                $('#btn_loading').removeAttr("disabled");
                $('#loading').hide();
            },
            dataType: 'json'
     });
}
</script>

<div id="content">
      <div id="main">
          <div class="highlight">

            <h2>CHAT HISTORY</h2>
          </div>
          <br class="clear">
          
          <?php if (sizeof($conversations) > 0):?>

          <span id="loadmore"><input class="btn_loading" type="button" name="btn_loading" id="btn_loading" value="Load more" onclick="load_more(<?php echo $user_id?>)"></span>
          <span id="loading" style="display: none;">  <img src="<?php echo $this->config->item('base_url'); ?>public/admin/images/ajax-loader.gif"></span>
          <br class="clear" />
          <br class="clear" />
          <input type="hidden" name="last_time" id="last_time" value="<?php echo $last_time ?>">
          <input type="hidden" name="active_class" id="active_class" value="">
          <div id="conversation" class="conversation">
              <?php foreach ($conversations as $partner_id => $partner_name): ?>
                <div class="user" id="<?php echo $partner_id?>" onclick="showDetail(<?php echo $user_id?>, <?php echo $partner_id?>)"> <?php echo $partner_name?></div>
              <?php endforeach;?>
          </div>
          
          
          <div id="detail" class="detail" style="display:none;">
              <?php foreach ($details as  $detail): ?>
                    <div class="<?php echo $detail['user_id'].'_'.$detail['partner_id']?>" style="display:none;">
                        <div class="msgs">
                            <span id="username" class="username"><?php echo $detail['sender_name'] ?>: </span>
                            <span class="msg">
                                <?php if ($detail['photo'] != ''): ?>
                                    <a href="<?php echo $detail['photo'] ?>" data-lightbox="<?php echo $detail['photo'] ?>"  >
                                        <img style="max-width:50px;" src="<?php echo $detail['photo'] ?>"  />
                                    </a>
                                    
                                <?php else: ?>
                                    <?php echo $detail['text']; ?>
                                <?php endif; ?>
                            </span>
                            <span class="time"> <?php echo $detail['time']?></span>
                            
                        </div>
                    </div>   
              <?php endforeach;?>
          </div>
       
      <?php else: ?>
          No chat conversations yet!
      <?php endif; ?>   
          
          
      </div>    
    </div>
<?php include '_footer.php';?>