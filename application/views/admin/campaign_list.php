<?php include '_header.php';?>
<div id="content">
      <div id="main">          
          <div class="highlight">
            <div class="lev4">
              <ul>
                <li><a href="#"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_13.gif" alt="" border="0" height="39" width="32">Create</a></li>
              </ul>
            </div>
            <h2>Celebrity Campaigns</h2>
          </div>
          <br class="clear">
          <div class='color-red'><?php echo (isset($this->uri->vn_param['delete']))?'Delete successful':''?></div>
          <div style='color:red'>Found <?php echo  $total ?> result(s)</div>
          <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param)?></div>
          <form action="<?php echo base_url()?>admin/promos/index" id="fm_filter_promos" name="fm_filter_promos" method="post">
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>              
              <tr>
                <th scope="col" width="5%">ID</th>
                <th scope="col" width="">Title</th>
                <th scope="col" width="">Description</th>
                <th scope="col" width="">CelebrityID</th>
                <th scope="col" width="15%">Celebrity Name</th>
                <th scope="col" width="">Avatar</th>
                <th scope="col" width="">Total CAU</th>
                <th scope="col" width="">Start Date</th>
                <th scope="col" width="">Expiry Date</th>
                <th scope="col" width="">Created Date</th>
                <th scope="col" width="">Status</th>
                <th scope="col" width="">Action</th>
              </tr>
            <?php if ($results):?>
            <?php foreach ($results as $row): ?>
              <tr>
                <td><?php echo $row->CampaignID?></td>
                <td><?php echo @$row->title?></td>
                <td><?php echo @$row->desc?></td>
                <td><?php echo $row->CelebrityID?></td>
                <td><?php echo $row->UserName?></td>
                <td>
                    <?php if ($row->Photo):?>
                    <img src="<?php echo $this->config->item('s3_photo_url').$row->Photo;?>" style="width:80px; height:80px;"/>
                    <?php endif; ?>
                </td>
                <td><?php echo $row->TotalCAU?></td>
                <td><?php echo date('Y-m-d', strtotime($row->StartDate))?></td>
                <td><?php echo date('Y-m-d', strtotime($row->ExpiredDate))?></td>
                <td><?php echo date('Y-m-d', strtotime($row->CreatedDate))?></td>
                <td><?php echo $row->Status == 1? 'Active': 'Inactive'?></td>
                <td>
                    <a href="<?php echo base_url()?>admin/campaign/dailyreport/<?php echo $row->CampaignID?>"> Daily Report</a>
                </td>
              </tr>
            <?php endforeach;?>
            <?php endif;?>
            </tbody>
          </table>
          </form>
      </div>
      <br class="clear">
    </div>
<?php include '_footer.php';?>