<?php include '_header.php';?>
<div id="content">
      <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <li><a href="<?php echo base_url()?>admin/question/import"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_09.gif" alt="" border="0" height="39" width="32">Import</a></li>
                <li><a href="<?php echo base_url()?>admin/question/post"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_13.gif" alt="" border="0" height="39" width="32">Create</a></li>
              </ul>
            </div>
            <h2>Question</h2>
          </div>
          <br class="clear">
          <div class='color-red'><?php echo (isset($this->uri->vn_param['delete']))?$this->uri->vn_param['delete']:''?></div>
          <div style='color:red'>Found <?php echo  $total ?> result(s)</div>
          <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param)?></div>
          <form action="<?php echo base_url()?>admin/question/index" id="fm_filter_question" name="fm_filter_question" method="post">
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <td colspan="11" width="5%">
				Keyword: <input name="keyword" value="<?php echo isset($this->uri->vn_param['keyword'])?$this->uri->vn_param['keyword']:''?>" type="text" id="keyword">
				Group: <?php echo form_dropdown('group', $groups, isset($this->uri->vn_param['group'])?$this->uri->vn_param['group']:'', "id='group' style='width:100px;'")?>
				<input value="Filter" type="button" name='btn_submit' id="btn_submit" onclick="filter_question();">
				<input value="Cancel" type="reset" name='cancel_list' id="cancel_list" onclick="javascript:window.location='<?php echo base_url()?>admin/question'">
                </td>
              </tr>
              <tr>
                <th scope="col" width="5%" style="text-align:center;">
				<?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'id-asc' || $this->uri->vn_param['sort'] == 'id-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'id-asc'):?>	
                		<a href="<?php echo uri_sort('id-desc')?>" class='color-red'>ID<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('id-asc')?>" class='color-red'>ID<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('id-asc')?>">ID</a>
                <?php endif;?>
				</th>
                
                <th scope="col" width="12%">
                <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'group-asc' || $this->uri->vn_param['sort'] == 'group-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'group-asc'):?>	
                		<a href="<?php echo uri_sort('group-desc')?>" class='color-red'>Question Group<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('group-asc')?>" class='color-red'>Question Group<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('group-asc')?>">Question Group</a>
                <?php endif;?>
                </th>
         
                <th scope="col" >Content</th>
                <th scope="col" width="10%">Category</th>
                <th scope="col" width="10%">Question Type</th>
                <th scope="col" width="7%" style="text-align:center;">Order</th>
                <th scope="col" width="13%" style="text-align:center;">Expired Date</th>
                <th scope="col" width="7%" style="text-align:center;">Action</th>
              </tr>
            <?php if (count($results) > 0):?>
			<?php foreach ($results as $row): ?>
              <tr>
                <td style="text-align:center;"><?php echo $row->QuestionID;?></td>
                <td>Group <?php echo $row->QuestionGroupID;?></td>
                <td><?php echo $row->Content;?></td>
                <td><?php echo $row->Category;?></td>
                <td><?php echo $row->QuestionType==1?'Single':'Multiple';?></td>
                <td style="text-align:center;"><?php echo $row->Order;?></td>
                <td style="text-align:center;"><?php echo $row->ExpireDate?$row->ExpireDate:'Forever';?></td>
                <td style="text-align:center;"><a href="<?php echo base_url()?>admin/question/post/<?php echo $row->QuestionID?>">Edit</a></td>
              </tr>
            <?php endforeach;?>
            <?php endif;?>
            </tbody>
          </table>
          </form>
      </div>
      <br class="clear">
    </div>
<script>
function filter_question(){
	var action = jQuery('#fm_filter_question').attr('action');
	var keyword = jQuery('#keyword').val();
	if (keyword != ''){
		action += '/p_keyword/'+keyword;
	}
	var group = jQuery('#group').val();
	if (group != ''){
		action += '/p_group/'+group;
	}
	
	jQuery('#fm_filter_question').attr('action', action);
	jQuery('#fm_filter_question').submit();
}
</script>
<?php include '_footer.php';?>