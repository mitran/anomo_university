<?php include '_header.php';?>
<div id="content">
    <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <li><a href="<?php echo base_url()?>admin/promo_code"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">Back to list</a></li>
              </ul>
            </div>
            <h2>Posting PromoCode</h2>
          </div>
         <div style="color:red">
         	<?php echo isset($this->validation->error_string)?$this->validation->error_string:''; ?>
         </div>
         <?php echo form_open_multipart('admin/promo_code/post/'.$promo_code_id)?>
         <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <td>Code <span class="color-red">(*)</span></td>
                <td><input type="text" name="code" size=60 value="<?php echo isset($this->validation->code)?$this->validation->code:@$info->Code?>"></td>
              </tr>
			 <tr>
                <td>Name <span class="color-red">(*)</span></td>
                <td><input type="text" name="name" size=60 value="<?php echo isset($this->validation->name)?$this->validation->name:@$info->Name?>"></td>
              </tr>
              <tr>
                <td>Point <span class="color-red">(*)</span></td>
                <td><input type="text" name="point" size=60 value="<?php echo isset($this->validation->point)?$this->validation->point:@$info->Point?>"></td>
              </tr>
              <tr>
                <td>Status <span class="color-red">(*)</span></td>
                <td><?php echo form_dropdown('status', array(0 => 'Inactive', 1 => 'Active'), isset($this->validation->status)?$this->validation->status:@$info->Status, "style='width:100px;'")?>
                </td>
              </tr>
              <tr>
                <td colspan="2" align="center"><input value="Submit" type="submit" name="submit" id="submit" > &nbsp <input value="Cancel" type="reset" ></td>
             </tr>
			 </tbody>
          </table>
   </div>
   <br class="clear">
</div>
<?php include '_footer.php';?>