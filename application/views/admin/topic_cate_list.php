<?php include '_header.php'; ?>
<div id="content">
    <div id="main">
        <div class="highlight">
            <div class="lev4">
                <ul>
                    <li><a href="<?php echo base_url() ?>admin/topic_category/add"><img src="<?php echo $this->config->item('base_url'); ?>public/admin/images/ico_lev4_13.gif" alt="" border="0" height="39" width="32">Create</a></li>
                </ul>
            </div>
            <h2>Topic Category</h2>
        </div>
        <br class="clear">
        <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>
        <form action="<?php echo base_url() ?>admin/topic_category/index" id="fm_filter_credit" name="fm_filter_credit" method="post">
            <?php if (count($results) > 0): ?>
                <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
                    <tbody>
                    <th scope="col" width="15%">CateID</th>
                    <th scope="col" width="15%">Name</th>
                    <th scope="col" width="15%">Desc</th>
                    <th scope="col" width="15%">Photo</th>
                    <th scope="col" width="15%">AllowCreateTopic</th>
                    <th scope="col" width="15%">Order</th>
                    <th scope="col" width="10%">Action</th>
                    </tr>

                    <?php foreach ($results as $row): ?>
                        <tr>
                            <td><?php echo $row->CateID?></td>
                            <td><?php echo $row->CateName?></td>
                            <td><?php echo $row->Desc?></td>
                            <td><img style="max-width:150px;" src="<?php echo $row->Photo ?>"  /></td>
                            <td><?php echo $row->AllowCreateTopic == 1 ? "Yes" : "No" ?></td>
                            <td><?php echo $row->OrderID?></td>
                            <td>
                                <a href="<?php echo base_url() ?>admin/topic_category/add/<?php echo $row->CateID ?>" title="edit">Edit </a> 
                                
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            <?php endif; ?>
        </form>
    </div>
    <br class="clear">
</div>
<?php include '_footer.php'; ?>