<?php include '_header.php'; ?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/message.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-ui-1.8.9.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery.datetimepicker.js"></script>

<link href="<?php echo $this->config->item('base_url');?>public/admin/css/smoothness/jquery-ui-1.8.9.custom.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/add_datetimepicker.css" rel="stylesheet" type="text/css" media="screen">

<script>
    $(document).ready(function() {
        $("#from").datepicker({dateFormat: "yy-mm-dd"});
        $("#to").datepicker({dateFormat: "yy-mm-dd"});
    });
</script>


<div id="content">
    <div id="main">
        <div class="highlight" style="text-align: left">
            <!--<a href="<?php echo base_url() ?>admin/message/send">Admin mass message</a> | 
            <a class="active menu-picture" href="<?php echo base_url() ?>admin/vegas_msg/send">Admin mass message</a> | 
            <a href="<?php echo base_url() ?>admin/auto_msg/send">Auto mass message</a> -->
            
        </div>
        
        <div class="highlight">
            <div class="lev4">
                <ul>
                    <li><a href="<?php echo base_url() ?>admin/vegas_msg/listmsg"><img src="<?php echo $this->config->item('base_url'); ?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">List Msg</a></li>
                </ul>
            </div>
            <h2>posting message</h2>
        </div>
        <div style="color:red">
            <?php echo validation_errors(); ?>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
        <form id="formMessage" enctype="multipart/form-data" method="post">
            <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
                <tbody>
                    <tr>
                        <td style="width: 200px">Message <span class="color-red">(*)</span></td>
                        <td>
                            <textarea name="message" id="message" cols="100" rows="10"><?php echo isset($this->form_validation->message) ? $this->form_validation->message : ""; ?></textarea>
                        </td>
                    </tr>

                    <tr>
                        <td style="width: 200px">Photo </td>
                        <td>
                            <input type="file" name="photo" id="photo" /> (Maximum: 1000KB)
                        </td>
                    </tr>

                    <tr>
                        <td style="width: 200px">Location</td>
                        <td>
                            Lat <input type="text" name="lat" /> 
                            Lon <input type="text" name="lon" />
                            Radius <input type="text" name="radius" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 200px">Gender</td>
                        <td>
                            <select name="gender" id="gender">
                                <option value="">All</option>
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 200px">Used App (7 days)</td>
                        <td>
                            <select name="app_used" id="app_used">
                                <option value="">All</option>
                                <option value="Used">Used</option>
                                <option value="Not Used">Not Used</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 200px">Age</td>
                        <td>
                            From <input id="from_age" name="from_age" value="" type="text" style="width:100px;">
                            To <input id="to_age" name="to_age" value="" type="text" style="width:100px;">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 200px">Registerd Date</td>
                        <td>
                            From <input id="from" name="from" value="<?php echo isset($from) ? $from : ''; ?>" type="text" style="width:100px;">
                            To <input id="to" name="to" value="<?php echo isset($to) ? $to : ''; ?>" type="text" style="width:100px;">
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="width: 200px">Event Occurring</td>
                        <td>
                            Tap Action <select name="tap_action" id="tap_action">
                                            <option value="">--- Select ---</option>
                                            <option value="1">Tag Search</option>
                                        </select>
                            Tap Parameter <input id="tap_param" name="tap_param" value="" type="text" style="width:300px;">
                        </td>
                    </tr>
                    
                    <tr>
                        <td></td>
                        <td>
                            <input value="Submit" type="button" name="submit" id="submit" onclick="validMessage()" /> &nbsp;
                            <input value="Cancel" type="reset" />
                            <input type="submit" value="requestsubmit" name="requestsubmit" id="requestsubmit" style="visibility:hidden" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
    <br class="clear" />
</div>

<?php include '_footer.php'; ?>