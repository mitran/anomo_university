<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <link href='<?php echo base_url(); ?>public/admin/images/favicon.ico' rel='icon' type='image/vnd.microsoft.icon'/>
            <link rel="shortcut icon" href="<?php echo base_url(); ?>public/admin/images/favicon.ico" type="image/x-icon" />
            <title>Anomo Administrator</title>
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/admin/js/jquery-1.11.0.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/admin/js/jquery.lazyload.js"></script>
            
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/admin/lightbox/css/lightbox.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
            <script src="<?php echo $this->config->item('base_url'); ?>public/admin/lightbox/js/lightbox.min.js" type="text/javascript" charset="utf-8"></script>

            <script type="text/javascript">
                jQuery.noConflict();
                var base_url = "<?php echo base_url() ?>";
            </script>

            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/admin/js/message.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/admin/js/main.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/admin/js/admin.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/admin/js/jquery-ui-1.8.9.custom.min.js"></script>
            <!--<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/admin/js/jquery-1.9.0.js"></script>-->

            
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/admin/js/jquery.ui.datetime.src.js"></script>
<link href="<?php echo $this->config->item('base_url'); ?>public/admin/css/jquery.ui.datetime.css" rel="stylesheet" type="text/css" media="screen">

      
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/admin/js/jquery.datetimepicker.js"></script>
            <link href="<?php echo $this->config->item('base_url'); ?>public/admin/css/smoothness/jquery-ui-1.8.9.custom.css" rel="stylesheet" type="text/css" media="screen">
                <link href="<?php echo $this->config->item('base_url'); ?>public/admin/css/add_datetimepicker.css" rel="stylesheet" type="text/css" media="screen">

                    <link href="<?php echo $this->config->item('base_url'); ?>public/admin/css/screen.css" rel="stylesheet" type="text/css" media="screen">



                        </head>
                        <body>
                            <div id="cont">
                                <div id="container">
                                    <a id="top"></a>
                                    <div id="header">
                                        <?php if ($this->session->userdata('role') == 'admin'): ?>
                                            <ul id="lev1">
                                                <li class="number"><a href="<?php echo base_url() ?>admin/index/profile"><?php echo $this->session->userdata('nick_name') ?></a></li>
                                                <li class="number"><img src="<?php echo $this->config->item('base_url'); ?>public/admin/images/ico_admin.gif" alt=""><a href="<?php echo base_url() ?>admin/index/profile">Profile</a></li>
                                                <li class="logout"><img src="<?php echo $this->config->item('base_url'); ?>public/admin/images/oc.gif" alt=""><a href="<?php echo base_url() ?>admin/index/logout">Logout</a></li>
                                            </ul>
                                        <?php endif; ?>
                                        <h1><a href="<?php echo base_url() ?>admin/dashboard"><img src="<?php echo $this->config->item('base_url'); ?>public/admin/images/logo.gif" alt="" border="0" height="53" width="185"></a></h1>
                                        <br class="clear">
                                            <div class="lev3">
                                                <?php if ($this->session->userdata('role') == 'admin'): ?>
                                                    <ul class="lev1Menu">
        	<li class="<?php echo ($this->uri->segment(2) == 'dashboard')?'active':''?>"><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'gift') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/gift">Gift</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'gift_category') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/gift_category">Gift Category</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'credit') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/credit">Credit</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'config') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/config">Config</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'version') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/version">Version</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'user' && $this->uri->segment(3) == 'index') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/user/index">User</a></li>
                                                        <!--<li class="<?php echo ($this->uri->segment(2) == 'index') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/index/list_user">Admin</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'promo_code') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/promo_code">PromoCode</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'push') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/push">Push</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'place') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/place">FPlaces</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'gplaces') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/gplaces">GPlaces</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'lplaces') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/lplaces">LPlaces</a></li>-->
                                                        <li class="<?php echo ($this->uri->segment(2) == 'question') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/qanda">Question</a></li>
                                                        <!--<li class="<?php echo ($this->uri->segment(2) == 'neighborhood') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/neighborhood">Neighborhood</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'neighborhoodpost') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/neighborhoodpost">NPost</a></li>
                                                         <li class="<?php echo ($this->uri->segment(2) == 'placepost') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/placepost">PPost</a></li>-->
                                                         <li class="<?php echo ($this->uri->segment(2) == 'avatar') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/avatar">Avatar</a></li>
                                                         <li class="<?php echo ($this->uri->segment(2) == 'intent') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/intent">Intents</a></li>
                                                         <li class="<?php echo ($this->uri->segment(2) == 'interests') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/interests">Interests</a></li>
                                                         <li class="<?php echo ($this->uri->segment(2) == 'major') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/major">Major</a></li>
                                                        <!--<li class="<?php echo ($this->uri->segment(2) == 'flag' && $this->uri->segment(3) == 'index') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/flag/index">FlagUsers</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'flag' && $this->uri->segment(3) == 'content') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/flag/content">FlagContent</a></li>-->
                                                         <li class="<?php echo ($this->uri->segment(2) == 'report' && $this->uri->segment(3) == 'index') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/report/index">Daily Report</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'report' && $this->uri->segment(3) == 'metrics') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/report/metrics">Advance Metrics</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2)  == 'promos')?'active':''?>"><a href="<?php echo base_url()?>admin/promos">Promos post</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2)  == 'leaderboard')?'active':''?>"><a href="<?php echo base_url()?>admin/leaderboard">Leaderboard</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2)  == 'sticker')?'active':''?>"><a href="<?php echo base_url()?>admin/sticker/list_sticker_category">Sticker</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2)  == 'topic')?'active':''?>"><a href="<?php echo base_url()?>admin/topic">Topic</a></li>
                                                    </ul>
                                                    <ul class="lev1Menu">

                                                       
                                                        
                                                        
                                                        <li class="<?php echo ($this->uri->segment(2) == 'picture' || $this->uri->segment(2) == 'userstatus') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/userstatus/index">UserPost</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'message' || $this->uri->segment(2) == 'vegas_msg' || $this->uri->segment(2) == 'auto_msg') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/vegas_msg/send">Mass Msg</a></li>      
                                                        <li class="<?php echo ($this->uri->segment(2) == 'weekly_comparison') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/weekly_comparison">Weekly Comparison</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'marketing') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/marketing/value">Marketing</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'exportemail') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/export_email">Export</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'announcement') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/announcement">Announcement</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'report' && $this->uri->segment(3) == 'top_like') ? 'active' : '' ?>"><a href="<?php echo base_url() ?>admin/report/top_like">TopLike</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'blocked_users')?'active':''?>"><a href="<?php echo base_url()?>admin/blocked_users">Blocked Users</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'reportban_user' && $this->uri->segment(3) == 'reportedcontent')?'active':''?>"><a href="<?php echo base_url()?>admin/reportban_user/reported_content">Report and Ban</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'anonymous_post' )?'active':''?>"><a href="<?php echo base_url()?>admin/anonymous_post">Anonymous Post</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'campaign' )?'active':''?>"><a href="<?php echo base_url()?>admin/campaign">Campaign</a></li>
                                                        <li class="<?php echo ($this->uri->segment(2) == 'user' && $this->uri->segment(3) == 'new_user')?'active':''?>"><a href="<?php echo base_url()?>admin/user/new_user">NewUser</a></li>
                                                    </ul>



                                                <?php endif; ?>
                                            </div>
                                    </div>
                                    <br class="clear">
