<?php include '_header.php';?>
<div id="content">
      <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <li><a href="<?php echo base_url()?>admin/index/register"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_13.gif" alt="" border="0" height="39" width="32">Add new user</a></li>
              </ul>
            </div>
            <h2>Users</h2>
          </div>
          <br class="clear">
          <div class='color-red'><?php echo (isset($this->uri->vn_param['delete']))?$this->uri->vn_param['delete']:''?></div>
          <div style='color:red'>Found <?php echo  $total ?> result(s)</div>
          <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param)?></div>
          <form action="<?php echo base_url()?>admin/index/list_user" id="fm_filter_user" name="fm_filter_user" method="post">
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <td colspan="11" width="5%">
				Keyword: <input name="keyword" value="<?php echo isset($this->uri->vn_param['keyword'])?htmlspecialchars($this->uri->vn_param['keyword']):''?>" type="text" id="keyword">
				<?php $status = array('' => '---select---',0 => 'Not Active', 1 => 'Active', 2=> 'Banned')?>
				Status: <?php echo form_dropdown('status', $status, isset($this->uri->vn_param['status'])?$this->uri->vn_param['status']:'', "id='status' style='width:100px;'")?>
				<input value="Filter" type="submit" name='btn_submit' id="btn_submit" onclick="filter_user()">
				<input value="Cancel" type="reset" name='cancel_list' id="cancel_list" onclick="javascript:window.location='<?php echo base_url()?>admin/index/list_user'">
                </td>
              </tr>
             
              <tr>
              	<th>
				<?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'id-asc' || $this->uri->vn_param['sort'] == 'id-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'id-asc'):?>	
                		<a href="<?php echo uri_sort('id-desc')?>" class='color-red'>ID<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('id-asc')?>" class='color-red'>ID<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('id-asc')?>">ID</a>
                <?php endif;?>
				</th>
              	<th>
				<?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'user-name-asc' || $this->uri->vn_param['sort'] == 'user-name-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'user-name-asc'):?>	
                		<a href="<?php echo uri_sort('user-name-desc')?>" class='color-red'>User name<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('user-name-asc')?>" class='color-red'>User name<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('user-name-asc')?>">User name</a>
                <?php endif;?>
				</th>
              	<th>
				<?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'nick-name-asc' || $this->uri->vn_param['sort'] == 'nick-name-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'nick-name-asc'):?>	
                		<a href="<?php echo uri_sort('nick-name-desc')?>" class='color-red'>Nick name<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('nick-name-asc')?>" class='color-red'>Nick name<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('nick-name-asc')?>">Nick name</a>
                <?php endif;?>
				</th>
              	<th>
				<?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'email-asc' || $this->uri->vn_param['sort'] == 'email-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'email-asc'):?>	
                		<a href="<?php echo uri_sort('email-desc')?>" class='color-red'>Email<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('email-asc')?>" class='color-red'>Email<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('email-asc')?>">Email</a>
                <?php endif;?>
				</th>
              	<th>Status</th>
              	<th>Action</th>
              </tr>
            
            <?php if (count($results) > 0):?>
			<?php foreach ($results as $row): ?>
              <tr>
              	<td><?php echo $row->AdminID?></td>
              	<td><?php echo $row->UserName?></td>
              	<td><?php echo $row->NickName?></td>
              	<td><?php echo $row->Email?></td>
              	<td>
              		<?php 
              			if ($row->Status == 1 || $row->Status == 2){
              				$s = array('1' => 'Active', 2=> 'Banned');
              			}
              			if ($row->Status == 0){
              				$s = array(0=> 'Not Active', 1 => 'Active');
              			}

              			$disabled = ($row->AdminID == $this->session->userdata('admin_id'))?"disabled=true":"";
              		?>
              		
              		<?php echo form_dropdown('status_'.$row->AdminID, $s, $row->Status, "$disabled id='status_{$row->AdminID}' onchange='change_status_user({$row->AdminID})' style='width:100px;'")?>
              	</td>
              	<td>
              		<?php if ($row->AdminID == $this->session->userdata('admin_id')):?>
              			delete
              		<?php else:?>
              		<a id="user_del_<?php echo $row->AdminID?>" href="javascript:;" onclick="delete_user(<?php echo $row->AdminID?>)">delete</a>
              		<?php endif;?>
              	</td>
              </tr>
           <?php endforeach;?>
           <?php endif;?>
           	</tbody>
          </table>
		  </form>
</div>
<br class="clear">
</div>
<?php include '_footer.php';?>