<?php include '_header.php'; ?>
<script>
jQuery(function() {
    jQuery("img.lazy").lazyload();
});
</script>
<div id="content">
    <div id="main">
        <div class="highlight">
            <div class="lev4">
                <ul>
                </ul>
            </div>
            <h2>Flag Contents</h2>
        </div>
        <br class="clear">
        <div class='color-red'><?php echo (isset($this->uri->vn_param['delete'])) ? 'Delete successful' : '' ?></div>
        <div style='color:red'>Found <?php echo $total ?> result(s)</div>
        <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>
        <?php if (count($results) > 0): ?>
        <div style="overflow: auto;">
            <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
                <tbody>
                    <tr>
                        <th>
                            UserID
                        </th>
                        <th>
                            UserName
                        </th>
                        <th>ContentOwner</th>
                        <th>
                            ContentID
                        </th>
                        <th style="width: 150px;">Type</th>
                        <th>Comment</th>
                        <th>Content</th>
                        <th>Image</th>
                        <th>CreatedDate</th>
                        <th>Action</th>
                    </tr>


                    <?php foreach ($results as $row): ?>
                        <?php
                        if ($row->IsComment == 0){
                            $contentInfo = $this->Activity_model->getContentInfo($row->ContentID, $row->Type);
                        }else{
                            $contentInfo = $this->Comment_model->getCommentDetail($row->ContentID, $row->Type);
                        }
                        $ownerUserName = $row->ContentOwner;
                        if (is_null($ownerUserName)) {
                            $ownerUserName = ($contentInfo) ? $contentInfo->UserName : '';
                        }
                        ?>
                        <tr>
                            <td><div style="width: 50px;"><?php echo $row->UserID ?></div></td>
                            <td><div style="width: 80px;"><?php echo $row->UserName ?></div></td>
                            <td><div style="width: 80px;"><a href="<?php echo base_url() ?>admin/user/index/p_keyword/<?php echo $ownerUserName ?>"><?php echo $ownerUserName ?></a></div></td>
                            <td><div style="width: 50px;"><?php echo $row->ContentID ?></div></td>
                            <td><div style="width: 80px;">
                                <?php echo $type = ($row->IsComment == 0)?@$types[$row->Type]:"Comment"; ?>
                             </div></td>
                            <td><div style="width: 100px;"><?php echo $row->Content ?></div></td>
                            <?php if ($contentInfo):
                                $content = isset($contentInfo->Content)?$this->Dashboard_model->formatMsg($contentInfo->Content):'';
                                if (empty($content)){
                                    $content = isset($contentInfo->PictureCaption)?$this->Dashboard_model->formatMsg($contentInfo->PictureCaption):'';
                                }
                            ?>
                                <td class="contentImage"><div style="width: 150px;"><?php echo $content;?></div></td>
                                <td><div style="width: 150px;">
                                        <?php if (isset($contentInfo->Photo)):?>
                                            <a href="<?php echo  $this->config->item('s3_photo_url') . $contentInfo->Photo ?>" data-lightbox="<?php echo $contentInfo->Photo ?>"  >
                                                <img style="max-width:150px;" class="lazy" data-original="<?php echo $this->config->item('s3_photo_url') . $contentInfo->Photo?>"  />
                                            </a>
                                            
                                        <?php endif; ?>
                                </div></td>
                            <?php else:?>
                                <td><div style="width: 150px;"><span style="color:red">This content was already deleted</span></div></td>
                                <td><div style="width: 150px;">
                                        <?php if (in_array($row->Type, array(2, 7, 27))):?>
                                        <span style="color:red"> This image was already deleted</span>
                                        <?php endif;?>
                                   </div></td>
                            <?php endif;?>
                            <td><div style="width: 70px;"><?php echo $row->CreatedDate ?></div></td>
                            <td>
                                <?php if ($contentInfo): ?>
                                    <div style="width: 50px;"><a href="#" onclick="delete_report_content(<?php echo $row->ContentID ?>,<?php echo $row->Type ?>, <?php echo $row->IsComment ?>, 'content_<?php echo $row->ID . '_' . $row->Type ?>', '<?php echo $current_offset?>')" id="content_<?php echo $row->ID . '_' . $row->Type ?>">Delete</a></div>
                                <?php else: ?>
                                    <div style="width: 50px;">Delete</div>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
            </div>
        <?php endif; ?>
    </div>
    <br class="clear">
    <p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
</div>
<?php include '_footer.php'; ?>