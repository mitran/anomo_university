<?php include '_header.php';?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-ui-1.8.9.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery.datetimepicker.js"></script>
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/smoothness/jquery-ui-1.8.9.custom.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/add_datetimepicker.css" rel="stylesheet" type="text/css" media="screen">
<script>
$(document).ready(function() {
	$( "#from" ).datepicker({dateFormat: "yy-mm-dd"});
	$( "#to" ).datepicker({dateFormat: "yy-mm-dd"});
});
</script>
<div id="content">
      <div id="main">          
          <div class="highlight">
            <div class="lev4">
              <ul>
                <li><a href="<?php echo base_url() ?>admin/campaign"><img src="<?php echo $this->config->item('base_url'); ?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">Back to list</a></li>
              </ul>
            </div>
            <h2>Campaign Daily Report</h2>
          </div>
          <br class="clear">
          <div class='color-red'><?php echo (isset($this->uri->vn_param['delete']))?'Delete successful':''?></div>                 
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>              
              <tr>
                <th scope="col" width="5%">ID</th>
                <th scope="col" width="">Title</th>
                <th scope="col" width="">Description</th>
                <th scope="col" width="">CelebrityID</th>
                <th scope="col" width="15%">Celebrity Name</th>
                <th scope="col" width="">Avatar</th>
                <th scope="col" width="">Total CAU</th>
                <th scope="col" width="">Start Date</th>
                <th scope="col" width="">Expiry Date</th>
                <th scope="col" width="">Created Date</th>
                <th scope="col" width="">Status</th>                
              </tr>
            <?php if ($info):?>            
              <tr>
                <td><?php echo $info->CampaignID?></td>
                <td><?php echo @$info->title?></td>
                <td><?php echo @$info->desc?></td>
                <td><?php echo $info->CelebrityID?></td>
                <td><?php echo $info->UserName?></td>
                <td>
                    <?php if ($info->Photo):?>
                    <img src="<?php echo $this->config->item('s3_photo_url').$info->Photo;?>" style="width:80px; height:80px;"/>
                    <?php endif; ?>
                </td>
                <td><?php echo $info->TotalCAU?></td>
                <td><?php echo date('Y-m-d', strtotime($info->StartDate))?></td>
                <td><?php echo date('Y-m-d', strtotime($info->ExpiredDate))?></td>
                <td><?php echo date('Y-m-d', strtotime($info->CreatedDate))?></td>
                <td><?php echo $info->Status == 1? 'Active': 'Inactive'?></td>               
              </tr>            
            <?php endif;?>
            </tbody>
          </table>
          <br />
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
              <tbody>
                <tr>
                  <td>
                    <form action="<?php echo base_url()?>admin/campaign/dailyreport/<?php echo $CampaignID?>" id="gplaces" name="gplaces" method="post">
                      From: <input id="from" name="from" value="<?php echo isset($from)?$from:'';?>" type="text" style="width:100px;">
                      To: <input id="to" name="to" value="<?php echo isset($to)?$to:'';?>" type="text" style="width:100px;">
                      <input value="Search" type="submit" name='btn_submit' id="btn_submit">
        			  
                    </form>
                  </td>
                </tr>
              </tbody>
            </table>   
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>              
              <tr>
                <th scope="col" width="40%">Date</th>
                <th scope="col" width="">Daily CAU in campaign</th>
                <th scope="col" width="">Daily CAU after campaign</th>
                <th scope="col" width="">Daily Active Fan</th>
                <th scope="col" width="">Total Daily Active Fan</th>               
              </tr>
            <?php if ($results):?>
            <?php foreach ($results as $row): ?>
              <tr>
                <td><?php echo $row->Date?></td>
                <td><?php echo $row->DailyCAU?></td>
                <td><?php echo $row->DailyCAUAfter?></td>
                <td><?php echo $row->CAUActive?></td>
                <td><?php echo $row->TotalCAUActive?></td>                
              </tr>
            <?php endforeach;?>
            <?php endif;?>
            </tbody>
          </table>          
      </div>
      <br class="clear">
    </div>
<?php include '_footer.php';?>