<?php include '_header.php';?>
<script>
jQuery(function() {
    jQuery("img.lazy").lazyload();
});
</script>
<div id="content">
      <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <!--<li><a href=""><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_09.gif" alt="" border="0" height="39" width="32">Delete</a></li>-->
                <li><a href="<?php echo base_url()?>admin/avatar/post"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_13.gif" alt="" border="0" height="39" width="32">Create</a></li>
              </ul>
            </div>
            <h2>Avatar</h2>
          </div>
          <br class="clear">
          <div style='color:red'>Found <?php echo  $total ?> result(s)</div>
          <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param)?></div>
          <?php if (count($results) > 0):?>
          <form action="<?php echo base_url()?>admin/avatar/index" id="fm_filter_gift" name="fm_filter_gift" method="post">
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <td colspan="4" width="5%">
				
				Gender: <?php echo form_dropdown('gender', array('' => '---Select---', 1 => 'Male', 2 => 'Female'), isset($this->uri->vn_param['gender'])?$this->uri->vn_param['gender']:'', "id='gender' style='width:100px;'")?>
				<input value="Filter" type="submit" name='btn_submit' id="btn_submit" onclick="filter_avatar()">
				<input value="Cancel" type="reset" name='cancel_list' id="cancel_list" onclick="javascript:window.location='<?php echo base_url()?>admin/gift/index'">
                </td>
              </tr>
              <tr>
                <th scope="col" >Avatar</th>
                <th scope="col" >Full Avatar</th>
                <th scope="col" width="">Gender</th>
                <th scope="col" width="">Action</th>
              </tr>
            

            <?php foreach ($results as $row): ?>
              <tr>
                <td><img class="lazy" data-original="<?php echo $row->Avatar ?>" width="80px" heigh="80px"/></td>
                <td><img class="lazy" data-original="<?php echo $row->FullAvatar ?>" width="46px" heigh="100px"/></td>
                <td>
                    <?php $gender = array(1 => 'Male', 2 => 'Female');
                        echo @$gender[$row->Gender]
                    ?>
                </td>
                <td><a href="<?php echo base_url()?>admin/avatar/post/<?php echo $row->ID?>">Edit </a></td>
              </tr>
            <?php endforeach;?>
            <?php endif;?>
            </tbody>
          </table>
          </form>
      </div>
      <br class="clear">
    </div>
<?php include '_footer.php';?>