<?php include '_header.php';?>
<div id="content">
<div id="main">
          <div class="highlight">
            <div class="lev4">
            </div>
            <h2>Reset Password</h2>
          </div>
          <br class="clear">
          
    <?php if(isset($user_name)):?>
	<form class="frmLogin1" method="post" action="<?php echo base_url()?>admin/index/reset_password/<?php echo $user_name?>/<?php echo $key?>">
	<div style="color:red">
        <?php echo isset($this->validation->error_string)?$this->validation->error_string:''; ?>
    </div>
	<p>
		<label style="width:120px;">Password:</label>
		<input type="password" name="password" value="<?php echo @$this->validation->password?>">
	</p>
	<p>
		<label style="width:120px;">Re-enter password:</label>
		<input type="password" name="re_password" value="<?php echo @$this->validation->re_password?>">
	</p>
	<p style="margin-left:250px"><input value="Submit" name="submit" type="submit"><a href="<?php echo base_url()?>admin/index/login">Login</a></p>
	</form>
	<?php else:?>
	<p class='color-red'><?php echo isset($this->uri->vn_param['do'])?$this->uri->vn_param['do']:@$msg?></p>
	<a href="<?php echo base_url()?>admin/index/login">Login</a>
	<?php endif;?>
</div>
</div>
<?php include '_footer.php';?>