<?php include '_header.php'; ?>
<div id="content">
    <div id="main">
        <div class="highlight">
            <div class="lev4">
                <ul>
                    <li><a href="<?php echo base_url()?>admin/user/index"><img
                        src="<?php echo $this->config->item('base_url');?>public/admin/images/back_32.gif" alt=""
                        border="0" height="32" width="32">Back to list</a></li>
                </ul>
            </div>
            <h2>User</h2>
        </div>
        <div style="color:red">
            <?php echo validation_errors(); ?>
            <?php echo @$upload_error;?>
            <?php echo @$fbid_msg_error;?>
        </div>
        <?php echo form_open_multipart('admin/user/post/' . $user_id)?>
        <?php $http = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https://" : "http://"; ?>
        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
            <tr>
                <td>Name <span class="color-red">(*)</span></td>
                <td><input type="text" name="name" size=60
                           value="<?php echo isset($this->form_validation->name) ? $this->form_validation->name : @$info->UserName?>">
                </td>
            </tr>
            <tr>
                <td>FacebookID <span class="color-red">(*)</span></td>
                <td><input type="text" name="fbid" size=60
                           value="<?php echo isset($this->form_validation->fbid) ? $this->form_validation->fbid : @$info->FacebookID?>">
                </td>
            </tr>
            <tr>
                <td>Edit Credits <span class="color-red">(*)</span></td>
                <td><input type="text" name="credits" size=60
                           value="<?php echo isset($this->form_validation->credits) ? $this->form_validation->credits : @$info->Credits?>">
                </td>
            </tr>
            <tr>
                <td>Photo<span class="color-red">(*)</span></td>
                <td>
                    <input type="file" name="photo"/>
                    <?php if ($user_id > 0): ?>
                        <img src="<?php echo  $this->config->item('s3_photo_url').@$info->Photo?> " width="100px" heigh="100px"/>
                    <?php endif;?>
                </td>
            </tr>
            <tr>
                <td>FullPhoto<span class="color-red">(*)</span></td>
                <td>
                    <input type="file" name="full_photo"/>
                    <?php if ($user_id > 0): ?>
                        <img src="<?php echo   $this->config->item('s3_photo_url').@$info->FullPhoto ?>" width="46px" heigh="100px"/>
                    <?php endif;?>
                </td>
            </tr>

            <tr>
                <td colspan="2" align="center"><input value="Submit" type="submit" name="submit" id="submit"> &nbsp
                    <input value="Cancel" type="reset"></td>
            </tr>
            </tbody>
        </table>
    </div>
    <br class="clear">
</div>
<?php include '_footer.php'; ?>