<?php include '_header.php';?>
<div id="content">
      <div id="main">
          <div class="highlight" style="text-align: left">            
            <a href="<?php echo base_url() ?>admin/promos">Promos Post</a> | 
            <a  href="<?php echo base_url() ?>admin/promos/fanpage" class="active menu-picture">Fan's Posts</a> 
          </div>  
          <div class="highlight">
            <div class="lev4">
              <ul>
                <li><a href="<?php echo base_url()?>admin/promos/post"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_13.gif" alt="" border="0" height="39" width="32">Create</a></li>
              </ul>
            </div>
            <h2>Fan's posts</h2>
          </div>
          <br class="clear">
          <div class='color-red'><?php echo (isset($this->uri->vn_param['delete']))?urldecode($this->uri->vn_param['delete']):''?></div>
          <div style='color:red'>Found <?php echo  $total ?> result(s)</div>
          <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param)?></div>
          <form action="<?php echo base_url()?>admin/promos/index" id="fm_filter_promos" name="fm_filter_promos" method="post">
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <td colspan="11" width="5%">		
                    Vendor <?php echo form_dropdown('user_id', $vendors, isset($this->uri->vn_param['user_id'])?$this->uri->vn_param['user_id']:'', "id='user_id' style='width:200px;'")?>
                    <input value="Filter" type="submit" name='btn_submit' id="btn_submit" onclick="filter_promos()">
                    <input value="Cancel" type="reset" name='cancel_list' id="cancel_list" onclick="javascript:window.location='<?php echo base_url()?>admin/promos/index'">
                </td>
              </tr>
              <tr>
                <th scope="col" width="5%">ID</th>
                <th scope="col" width="">Vendor</th>
                <th scope="col" width="">Message</th>
                <th scope="col" width="">Image</th>
                <th scope="col" width="15%">From UserID</th>
                <th scope="col" width="">From Username</th>
                <th scope="col" width="">Avatar</th>
                <!--<th scope="col" width="">Age</th>-->
                <th scope="col" width="">Location</th>
                <th scope="col" width="">Date</th>
                <th scope="col" width="">Action</th>
              </tr>
            <?php if ($results):?>
            <?php foreach ($results as $row): ?>
              <tr>
                <td><?php echo $row->ActivityID?></td>
                <td><?php echo $row->VendorName?></td>
                <td><?php echo $row->Message?></td>
                <td>
                    <?php if ($row->Image):?>
                    <img src="<?php echo $row->Image;?>" style="width:80px; height:80px;"/>
                    <?php endif; ?>
                </td>
                
                <td><?php echo $row->FromUserID?></td>
                <td><?php echo $row->FromUserName ?></td>
                <td>
                    <?php if ($row->Avatar):?>
                    <img src="<?php echo $row->Avatar;?>" style="width:80px; height:80px;"/>
                    <?php endif; ?>
                </td>
                <!--<td><?php echo $row->BirthDate ?></td>-->
                <td><?php echo $row->NeighborhoodName ?></td>
                <td><?php echo $row->CreatedDate ?></td>
                <td>
                    <a href="<?php echo base_url()?>admin/promos/fanpage_comment/<?php echo $row->ActivityID?>"> Comment  </a> |
                    <a href="#" onclick="delete_promos_fanpost(<?php echo $row->ActivityID ?>, 'promos_<?php echo $row->ActivityID ?>')" id="promos_<?php echo $row->ActivityID ?>">  Delete</a>
                </td>
              </tr>
            <?php endforeach;?>
            <?php endif;?>
            </tbody>
          </table>
          </form>
      </div>
      <br class="clear">
    </div>
<?php include '_footer.php';?>