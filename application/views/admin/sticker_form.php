<?php include '_header.php';?>
<div id="content">
    <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <li><a href="<?php echo base_url()?>admin/sticker/list_sticker/<?php echo $cate_id?>"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/back_32.gif" alt="" border="0" height="32" width="32">Back to list</a></li>
              </ul>
            </div>
            <h2>Posting Sticker</h2>
          </div>
         <div style="color:red">
         	<?php echo validation_errors(); ?>
         	<?php echo @$upload_error;
            ?>
         </div>
         <?php echo form_open_multipart('admin/sticker/add_sticker/'.$cate_id.'/'.$id)?>
        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <td>Sticker category<span class="color-red">(*)</span></td>
                <td>
                    <?php echo form_dropdown('cate_id', $cate, isset($this->form_validation->cate_id)?$this->form_validation->cate_id:@$info->CateID, "style='width:150px;'")?>
                </td>
              </tr>
              
                <tr>
                    <td>Photo<span class="color-red">(*)</span></td>
                    <td>
                        <input type="file" name="photo" />
                        <?php if ($id > 0): ?>
                            <img src="<?php echo $this->config->item('s3_sticker_url') . $info->Photo ?>" width="120" height="100" />
                        <?php endif; ?>
                    </td>
                </tr>

                <tr>
                    <td colspan="2" align="center"><input value="Submit" type="submit" name="submit" id="submit" > &nbsp <input value="Cancel" type="reset" ></td>
                </tr>
            </tbody>
        </table>
   </div>
   <br class="clear">
</div>
<?php include '_footer.php';?>