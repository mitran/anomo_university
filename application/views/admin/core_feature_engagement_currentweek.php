<br>
<h3>CURRENT WEEK</h3>
<div style="height: 10px;"></div>
<?php if ($data[0]['cdate']): ?>
<?php echo "Last updated  " ?>
<?php echo $data[0]['cdate'];?>  
<?php endif;?>
<br>
<table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1"
	width="80%">
            <tbody>
                <tr>
                    <th><b>WeekEndingOn</b></th>
                    <th><b>#Posts</b></th>
                    <th><b>#Comments</b></th>
                    <th><b>#Likes</b></th>
                    <th><b>#Followers Added</b></th>
                    <th><b>#Ice Breakers</b></th>
                    <th><b>#Chat Messages</b></th>
                    <th><b>#Reveals</b></th>
                </tr>
		
        <?php foreach ($data as $row):?>  
                <tr>
			<td width="25%"><?php echo DATE("Y-m-d",strtotime($row['end']))?></td>
			<td><?php echo $row['posts']?></td>
			<td><?php echo $row['acomments'] + $row['pcomments'] + $row['ncomments']?></td>
			<td><?php echo $row['likes']?></td>
			<td><?php echo $row['followers_added']?></td>
			<td><?php echo $row['ice_breakers']?></td>
			<td><?php echo $row['chat_messages']?></td>
			<td><?php echo $row['reveals']?></td>
		</tr>
        <?php endforeach;?>        
        <br>    
            </tbody>
</table>
<br>