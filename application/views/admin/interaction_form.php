<?php include '_header.php';?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-ui-1.8.9.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery.datetimepicker.js"></script>
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/smoothness/jquery-ui-1.8.9.custom.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/add_datetimepicker.css" rel="stylesheet" type="text/css" media="screen">

<script>
$(document).ready(function() {
	$( "#date" ).datepicker({dateFormat: "yy-mm-dd"});
});

function add_to_watch_list(user_id){
    $.post(
            base_url+'admin/user/add_to_watch_list/'+user_id
            , {}, function(response){
                if (response.status == 'ok'){
                    $('#btn_add_'+user_id).html('Added');
                    $('#btn_add_'+user_id).attr('class', 'color-red');
                }
            }
            , 'json'
     );
}
</script>


<div id="content">
  <div id="main">
     <div class="highlight" style="text-align: left">            
         <a href="<?php echo base_url() ?>admin/user/watch_list"  >WATCH LIST</a> 
     </div> 
    <div class="highlight">
      <h2>NEW USER INTERACTION</h2>
    </div>
    <br class="clear">
    <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>
    <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
      <tbody>
        <tr>
          <td>
            <form action="<?php echo base_url()?>admin/user/interaction" id="gplaces" name="gplaces" method="post">
              Date: <input id="date" name="date" value="<?php echo isset($date)?$date:'';?>" type="text" style="width:100px;">
              <input value="Search" type="submit" name='btn_submit' id="btn_submit">
			  
            </form>
          </td>
        </tr>
      </tbody>
    </table>
    <div style="height:20px;"></div>
    <?php if (isset($result)):?>
    
    <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%" >
            <tbody>
            <tr>
            <th scope="col" width="15%">UserID</th>
            <th scope="col" width="40%">UserName</th>
            <th scope="col" width="">Avatar</th>
            <th scope="col" width="">BirthDate</th>
            <th scope="col" width="">AddToWatchList</th>
            </tr>

            <?php foreach ($result as $row): ?>
                <tr>
                    <td><?php echo $row->UserID ?></td>
                    <td style="max-width: 200px"><?php echo $row->UserName;?></td>
                    <td>
                        <?php if ($row->Avatar): ?>
                            <img src="<?php echo $row->Avatar?>" width="42px;">
                        <?php endif;?>
                    </td>
                    <td><?php echo $row->BirthDate ?></td>
                    <td> 
                        <?php if ($row->IsAddToWatchList == 1):?>
                        <span class="color-red">Added</span>
                        <?php else: ?>
                        <span class="btn_add_watch_list" id="btn_add_<?php echo $row->UserID?>" onclick="add_to_watch_list(<?php echo $row->UserID?>)">
                            Add
                        </span>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
    <?php endif;?>

  </div>
  <br class="clear">
</div>

<?php include '_footer.php';?>

