<?php include '_header.php'; ?>
<div id="content">
    <div id="main">
        <div class="highlight">
            <div class="lev4">
                <ul>
                </ul>
            </div>
            <h2>Flag Users</h2>
        </div>
        <br class="clear">
        <div class='color-red'><?php echo (isset($this->uri->vn_param['delete'])) ? 'Delete successful' : '' ?></div>
        <div style='color:red'>Found <?php echo $total ?> result(s)</div>
        <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>
        <form action="<?php echo base_url() ?>admin/user/index" id="fm_filter_user" name="fm_filter_user" method="post">
            <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
                <tbody>
                    <tr>
                        <th>
                            FromUserID
                        </th>

                        <th>
                            FromUserName
                        </th>
                        <th>
                            ToUserID
                        </th>
                        <th>ToUserName</th>
                        <th>Comment</th>
                        <th>CreatedDate</th>
                    </tr>

                    <?php if (count($results) > 0): ?>
                        <?php foreach ($results as $row): ?>
                            <tr>
                                <td><?php echo $row->FromUserID ?></td>
                                <td><?php echo $row->FromUserName ?></td>
                                <td><?php echo $row->ToUserID ?></td>
                                <td><?php echo $row->ToUserName ?></td>
                                <td><?php echo $row->Content ?></td>
                                <td><?php echo $row->CreatedDate ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </form>
    </div>
    <br class="clear">
    <p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
</div>
<?php include '_footer.php'; ?>