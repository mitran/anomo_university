<?php include '_header.php'; ?>
<div id="content">
    <div id="main">
        <div class="highlight">
            <div class="lev4">
                <ul>
                </ul>
            </div>
            <h2>Status</h2>
        </div>
        
        <div class="highlight" style="text-align: left">
            <a class="active menu-picture" href="<?php echo base_url() ?>admin/userstatus/index">Text post</a> | 
            <a  href="<?php echo base_url() ?>admin/picture/status_pics">Picture post</a> |
            <a  href="<?php echo base_url() ?>admin/userstatus/video">Video</a> 
        </div>
        
        <br class="clear">
        <div
            class='color-red'><?php echo (isset($this->uri->vn_param['delete'])) ?'Delete successful' : '' ?></div>
        <div style='color:red'>Found <?php echo $total ?> result(s)</div>
        <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>
        <form action="<?php echo base_url() ?>admin/userstatus/index" id="fm_filter_user" name="fm_filter_user" method="post">
        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <td colspan="5" width="5%">
                    
                    <?php $types = array('content' => 'Content', 'user_id' => 'UserID') ?>
                    Filter by: <?php echo form_dropdown('type', $types, isset($type) ? $type : '', "id='type' style='width:100px;'") ?>
                    with keyword: <input name="keyword" value="<?php echo isset($keyword) ? htmlspecialchars($keyword) : '' ?>" type="text" id="keyword">
                    <input value="Filter" type="submit" name='btn_submit' id="btn_submit" >
                    <input value="Cancel" type="reset" name='cancel_list' id="cancel_list" onclick="javascript:window.location = '<?php echo base_url() ?>admin/userstatus'">
                </td>
            </tr>
            <?php if (count($results) > 0): ?>
            <tbody>
                <tr>
                    <th>
                        UserID
                    </th>
                    <th style="width: 200px">
                        UserName
                    </th>
                    <th style="width: 600px">
                        Status
                    </th>
                    <th >CreatedDate</th>
                    <th>Action</th>
                </tr>

                <?php foreach ($results as $row): ?>
                    <tr>
                        <td><?php echo $row->UserID ?></td>
                        <td style="max-width: 200px"><?php echo $row->UserName ?></td>
                        <td style="max-width: 600px"><?php echo $this->Dashboard_model->formatMsg($row->Content) ?></td>
                        <td><?php echo date('Y-m-d H:i:s', strtotime(PST.' hour', strtotime($row->CreatedDate))) ?></td>
                        <td><a href="#" onclick="delete_item('content_<?php echo $row->ID ?>', '<?php echo base_url().'admin/userstatus/delete_content/'.$row->ID.'/1/1'?>')"
                               id="content_<?php echo $row->ID ?>">Delete</a></td>
                    </tr>
                <?php endforeach; ?>

            </tbody>
            <?php endif; ?>
        </table>
        </form>
    </div>
    <br class="clear">
    <p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
</div>
<?php include '_footer.php'; ?>