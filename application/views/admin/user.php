<?php include '_header.php'; ?>

<script>
jQuery(function() {
    jQuery("img.lazy").lazyload();
});
</script>
<div id="content">
    <div id="main">
        <div class="highlight" style="text-align: left">            
         <a href="<?php echo base_url() ?>admin/user/interaction"  >NEW USER INTERACTION</a> 
        </div> 
        <div class="highlight">
            <div class="lev4">
                <ul>
                    <li><a href="<?php echo base_url() ?>admin/user/export_csv"><img src="<?php echo $this->config->item('base_url'); ?>public/admin/images/ico_lev4_09.gif" alt="" border="0" height="39" width="32">Export FB Share users</a></li>
                </ul>
            </div>
            <h2>Users</h2>
        </div>
        <br class="clear">
        <div class='color-red'><?php echo (isset($this->uri->vn_param['delete'])) ? 'Delete successful' : '' ?></div>
        <div style='color:red'>Found <?php echo $total ?> result(s)</div>
        <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param) ?></div>
        <form action="<?php echo base_url() ?>admin/user/index" id="fm_filter_user" name="fm_filter_user" method="post">
            <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
                <tbody>
                    <tr>
                        <td colspan="16" width="5%">
                            Keyword: <input name="keyword" value="<?php echo isset($keyword) ? htmlspecialchars($keyword) : '' ?>" type="text" id="keyword">
                            <?php //$status = array('' => '---select---', 0 => 'Not Active', 1 => 'Active') ?>
                            <!--Status: <?php echo form_dropdown('status', $status, isset($this->uri->vn_param['status']) ? $this->uri->vn_param['status'] : '', "id='status' style='width:100px;'") ?>-->
                            <input value="Filter" type="submit" name='btn_submit' id="btn_submit">
                            <input value="Cancel" type="reset" name='cancel_list' id="cancel_list" onclick="javascript:window.location = '<?php echo base_url() ?>admin/user/index'">
                        </td>
                    </tr>
                    <?php if (count($results) > 0): ?>
                    <tr style="font-size:11px;font-weight:bold;">
                        <th width="5%">
                            <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'id-asc' || $this->uri->vn_param['sort'] == 'id-desc')): ?>
                                <?php if ($this->uri->vn_param['sort'] == 'id-asc'): ?>	
                                    <a href="<?php echo uri_sort('id-desc') ?>" class='color-red'>ID<img src="<?php echo base_url() ?>public/admin/images/s_desc.png" border="0"></a>
                                <?php else: ?>
                                    <a href="<?php echo uri_sort('id-asc') ?>" class='color-red'>ID<img src="<?php echo base_url() ?>public/admin/images/s_asc.png" border="0"></a>
                                <?php endif; ?>
                            <?php else: ?>
                                <a href="<?php echo uri_sort('id-asc') ?>">ID</a>
                            <?php endif; ?>
                        </th>

                        <th width="10%">
                            <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'user-name-asc' || $this->uri->vn_param['sort'] == 'user-name-desc')): ?>
                                <?php if ($this->uri->vn_param['sort'] == 'user-name-asc'): ?>	
                                    <a href="<?php echo uri_sort('user-name-desc') ?>" class='color-red'>User name<img src="<?php echo base_url() ?>public/admin/images/s_desc.png" border="0"></a>
                                <?php else: ?>
                                    <a href="<?php echo uri_sort('user-name-asc') ?>" class='color-red'>User name<img src="<?php echo base_url() ?>public/admin/images/s_asc.png" border="0"></a>
                                <?php endif; ?>
                            <?php else: ?>
                                <a href="<?php echo uri_sort('user-name-asc') ?>">User name</a>
                            <?php endif; ?>
                        </th>
                        
                        
                        <th >Avatar</th>
                        <th >Email</th>
                        <th > FacebookID</th>
                        <th >Ban</th>
                        <th >Signup Date</th>
                        <th >Banned Date</th>
                        <th width="5%">Last Activity</th>
                        <th >
                            <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'flags-asc' || $this->uri->vn_param['sort'] == 'flags-desc')): ?>
                                <?php if ($this->uri->vn_param['sort'] == 'flags-asc'): ?>
                                    <a href="<?php echo uri_sort('flags-desc') ?>" class='color-red'>Flags<img src="<?php echo base_url() ?>public/admin/images/s_desc.png" border="0"></a>
                                <?php else: ?>
                                    <a href="<?php echo uri_sort('flags-asc') ?>" class='color-red'>Flags<img src="<?php echo base_url() ?>public/admin/images/s_asc.png" border="0"></a>
                                <?php endif; ?>
                            <?php else: ?>
                                <a href="<?php echo uri_sort('flags-asc') ?>">Flags</a>
                            <?php endif; ?>
                        </th>
                        <th >
                            <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'flagcontent-asc' || $this->uri->vn_param['sort'] == 'flagcontent-desc')): ?>
                                <?php if ($this->uri->vn_param['sort'] == 'flagcontent-asc'): ?>
                                    <a href="<?php echo uri_sort('flagcontent-desc') ?>" class='color-red'>Flag Content<img src="<?php echo base_url() ?>public/admin/images/s_desc.png" border="0"></a>
                                <?php else: ?>
                                    <a href="<?php echo uri_sort('flagcontent-asc') ?>" class='color-red'>Flag Content<img src="<?php echo base_url() ?>public/admin/images/s_asc.png" border="0"></a>
                                <?php endif; ?>
                            <?php else: ?>
                                <a href="<?php echo uri_sort('flagcontent-asc') ?>">Flag Content</a>
                            <?php endif; ?>
                        </th>
                        <th >
                            <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'block-asc' || $this->uri->vn_param['sort'] == 'block-desc')): ?>
                                <?php if ($this->uri->vn_param['sort'] == 'block-asc'): ?>
                                    <a href="<?php echo uri_sort('block-desc') ?>" class='color-red'>Blocks<img src="<?php echo base_url() ?>public/admin/images/s_desc.png" border="0"></a>
                                <?php else: ?>
                                    <a href="<?php echo uri_sort('block-asc') ?>" class='color-red'>Blocks<img src="<?php echo base_url() ?>public/admin/images/s_asc.png" border="0"></a>
                                <?php endif; ?>
                            <?php else: ?>
                                <a href="<?php echo uri_sort('block-asc') ?>">Blocks</a>
                            <?php endif; ?>
                        </th>
                        <th width="5%">
                            <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'follower-asc' || $this->uri->vn_param['sort'] == 'follower-desc')): ?>
                                <?php if ($this->uri->vn_param['sort'] == 'follower-asc'): ?>
                                    <a href="<?php echo uri_sort('follower-desc') ?>" class='color-red'>Follower<img src="<?php echo base_url() ?>public/admin/images/s_desc.png" border="0"></a>
                                <?php else: ?>
                                    <a href="<?php echo uri_sort('follower-asc') ?>" class='color-red'>Follower<img src="<?php echo base_url() ?>public/admin/images/s_asc.png" border="0"></a>
                                <?php endif; ?>
                            <?php else: ?>
                                <a href="<?php echo uri_sort('follower-asc') ?>">Follower</a>
                            <?php endif; ?>
                        </th>
                        <th width="5%">SMS Invites</th>
                        <th width="5%">IsEmailVerify</th>
                        <th width="5%">Action</th>
                    </tr>

                    
                    <?php $http=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https://" : "http://"; ?>
                        <?php foreach ($results as $row): ?>
                            <tr>
                                <td><?php echo $row->UserID ?></td>
                                <td><div style="width:150px;"><?php echo $row->UserName ?></div></td>

                                <td>
                                   <!-- <img src="<?php echo $this->config->item('s3_photo_url').$row->Photo ?>" width="46px" heigh="100px"/>-->
                                    <img class="lazy" data-original="<?php echo $this->config->item('s3_photo_url').$row->Photo ?>" width="46px" heigh="100px"/>
                                </td>
                                <td><div style="width:90px;"><?php echo $row->Email ?></div></td>
                                <td><div style="width:80px;"><?php echo $row->FacebookID ?></div></td>
                                <!--  <td>
                                <?php
                                $s = array(0 => 'Not Active', 1 => 'Active');
                                ?>
                                        
                                <?php echo form_dropdown('status_' . $row->UserID, $s, $row->EmailAccountStatus, "id='status_{$row->UserID}' onchange='change_email_status({$row->UserID})' style='width:80px;'") ?>
                                </td>-->
                                <td><?php echo ($row->IsBanned == 0) ? 'No' : 'Yes' ?></td>
                                <td style="font-size:11px;"><?php echo date('Y-m-d H:i:s', strtotime(PST.' hour', strtotime($row->SignUpDate))) ?></td>
                                <td style="font-size:11px;"><?php echo $row->IsBanned == 1 && $row->BannedDate?date('Y-m-d H:i:s', strtotime(PST.' hour', strtotime($row->BannedDate))):'' ?></td>
                                <td style="font-size:11px;"><?php echo date('Y-m-d H:i:s', strtotime(PST.' hour', strtotime($row->LastActivity))) ?></td>
                                <td>
                                    <?php if ($row->TotalFlag > 0): ?>
                                        <a class="active" href="<?php echo base_url() ?>admin/flag/index/p_flag/<?php echo $row->UserID?>" target="_blank"> <?php echo $row->TotalFlag ?></a>
                                    <?php else: ?>
                                        <?php echo $row->TotalFlag ?> 
                                    <?php endif; ?>
                                    
                                </td>
                                <td>
                                    <?php if ($row->TotalFlagContent > 0): ?>
                                        <a class="active" href="<?php echo base_url() ?>admin/flag/content/p_flag/<?php echo $row->UserID?>" target="_blank"> <?php echo $row->TotalFlagContent ?></a>
                                    <?php else: ?>
                                        <?php echo $row->TotalFlagContent ?> 
                                    <?php endif; ?>
                                    
                                </td>
                                <td>
                                    <?php if ($row->TotalBlock > 0): ?>
                                        <a class="active" href="<?php echo base_url() ?>admin/user/p_block/<?php echo $row->UserID?>" target="_blank"> <?php echo $row->TotalBlock ?></a>
                                    <?php else: ?>
                                        <?php echo $row->TotalBlock ?> 
                                    <?php endif; ?>
                                    
                                </td>
                                <td><?php echo $row->NumberOfFollower ?></td>
                                <td><?php echo $row->NumberFriendInvite ?></td>
                                <td>
                                	<?php $s = array(0=> 'No', 1 => 'Yes');?>

                                    <?php echo form_dropdown('user_verify_'.$row->UserID, $s, $row->IsEmailVerify, "onchange='verify_account({$row->UserID})' style='width:50px;' id='user_verify_$row->UserID'")?>
                                </td>
                                
                                <td><a href="<?php echo base_url() ?>admin/user/post/<?php echo $row->UserID ?>">Edit</a> | 
                                    <a href="#" onclick="delete_user_account(<?php echo $row->UserID ?>)" id="user_del_<?php echo $row->UserID ?>">Delete</a> |
                                    <a href="#" onclick="ban_user(<?php echo $row->UserID ?>, <?php echo $row->IsBanned ?>)" id="user_ban_<?php echo $row->UserID ?>">
                                        <?php echo ($row->IsBanned == 0) ? 'Ban' : 'UnBan' ?>
                                    </a>
                                    | <a target="_blank" href="<?php echo base_url() ?>admin/chat_history/index/<?php echo $row->UserID ?>">Chat history</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </form>
    </div>
    <br class="clear">
    <p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
</div>
<?php include '_footer.php'; ?>