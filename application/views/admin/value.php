<?php include '_header.php'; ?>
<div id="content">
    <div id="main">
        <div class="highlight">
            <h2>Marketing</h2>
        </div>
        <br class="clear">
        <div class="highlight" style="text-align: left">
            <a class="active menu-picture" href="<?php echo base_url() ?>admin/marketing/value">Value</a> | 
            <a  href="<?php echo base_url() ?>admin/marketing/core_feature_engagement_new">Core Feature Engagement New</a> |            
            <a  href="<?php echo base_url() ?>admin/marketing/content_engagement">Content Engagement</a> | 
            <a  href="<?php echo base_url() ?>admin/marketing/life_cycle_tracking">Life Cycle Tracking</a> |  
            <a  href="<?php echo base_url() ?>admin/marketing/kpi">KPI</a> |
            <a  href="<?php echo base_url() ?>admin/marketing/social_media">Social Media</a>       
        </div>
        <br class="clear">
        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="80%">
            <tbody>
                <tr>
                    <th><b>Day</b></th>
                    <th><b>Daily Active Users</b></th>
                    <th><b>New Users</b></th>
                    <th><b>True DAU</b></th>
                    <th><b>+/- Change</b></th>
                    <th><b>Ad dollars</b></th>
                    <th width="10%"><b>True Cost</b></th>
                    <th><b>Cost per New User </b></th>
                </tr>
                <?php for($i = 0; $i < $numberDay; $i++):?>
                <tr>
                    <td width="25%"><b><span id="date_<?php echo $i?>"><?php echo $data[$i]['date']?></span></b> </td>
                    <td><?php echo $data[$i]['dau']?></td>
                    <td><?php echo $data[$i]['new_user']?></td>
                    <td><span id="true_dau_<?php echo $i?>"><?php echo $data[$i]['true_dau']?></span></td>
                    <td>
                        <span id="change_<?php echo $i?>">
                        <?php echo $change = isset($data[$i + 1])?$data[$i]['true_dau'] - $data[$i + 1]['true_dau']:''?></td>
                        </span>
                    <td>
                        <span id="div_add_dollar_<?php echo $i?>">
                            <?php if ($data[$i]['ad_dollar'] !=  ''):?>
                                <?php echo $data[$i]['ad_dollar']?>
                            <?php else:?>
                                <input type="text" name="ad_dollar_<?php echo $i?>" id="ad_dollar_<?php echo $i?>"value="" onkeyup="calCost(<?php echo $i?>)">
                                <input type="button" id="btnValue" name="btnValue" value="Save" onclick="save_ad_dollar(<?php echo $i?>)">
                            <?php endif;?>
                        </span>
                    </td>
                    <td>
                        <span id="true_cost_<?php echo $i?>">
                        <?php if ($data[$i+1]['ad_dollar'] !=  ''){
                            echo number_format($data[$i+1]['ad_dollar']/$change, 4, '.', ',');
                        }
                            
                        ?>
                        </span>
                    </td>
                    <td><?php echo $data[$i]['cost_per_user'] ;?></td>
                </tr>
                <?php endfor;?>
                
            </tbody>
        </table>
    </div>
    <br class="clear">
</div>

<script>
function calCost(i){
	var intAdDollar = jQuery('#ad_dollar_' + i).val();
        var j = i - 1;
        var intChange = jQuery('#change_' + j).html();
	if (intAdDollar > 0){
	    var intCost = intAdDollar / intChange;
            jQuery('#true_cost_' + j).html(intCost.toFixed(4));
	}
}

function save_ad_dollar(i){
    var j = i - 1;
    var intAdDollar = jQuery('#ad_dollar_' + i).val();
    var date = jQuery('#date_' + i).html();
    var intChange = jQuery('#change_' + j).html();
    if (isNaN(intAdDollar) || intAdDollar < 0){
        jQuery('#ad_dollar_' + i).focus();
        return false;
    }else{
        if (intAdDollar > 0){
            jQuery.post(base_url+'admin/marketing/save_value', {date: date, ad_dollar: intAdDollar}, function(response){
                if (response.status == 'ok'){
                    jQuery('#div_add_dollar_' + i).html(intAdDollar);
                }
            }, 'json');
        }
    }
}
</script>
<?php include '_footer.php'; ?>
