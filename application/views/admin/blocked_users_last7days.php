<?php include '_header.php'; ?>
<script type="text/javascript"
	src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.9.0.js"></script>

<link
	href="<?php echo $this->config->item('base_url'); ?>public/admin/css/screen.css"
	rel="stylesheet" type="text/css">


<div id="content">
	<div id="main">
		<div class="highlight">
			<h2>Blocked Users</h2>
		</div>
		<br class="clear">
		<div class="highlight" style="text-align: left">

			<a href="<?php echo base_url() ?>admin/blocked_users">Blocked Users
				Lifetime</a> | 
			<a class="active menu-picture" 
				href="<?php echo base_url() ?>admin/blocked_users/last7days"
				id="days">Blocked Users Last 7 days</a> | 
			<a href="<?php echo base_url() ?>admin/blocked_users/customperiod"
				id="customdays">Blocked Users Custom Period</a> 

		</div>
		<br class="clear">

		<table class="tblAdminDetail" border="0" cellpadding="0"
			cellspacing="1" width="80%">
			<tbody>
				<tr>
					<th><b>User</b></th>
					<th><b>Number of times blocked</b></th>
				</tr>
	            <?php foreach ($data['week'] as $row):?>
                <tr>
					<td><?php echo $row['UserName']?></td>
					<td><?php echo $row['blocks']?></td>
				</tr>
	            <?php endforeach;?>
	       </tbody>	       
		</table>


	</div>
</div>
<?php include '_footer.php'; ?>	            