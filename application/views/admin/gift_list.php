<?php include '_header.php';?>
<div id="content">
      <div id="main">
          <div class="highlight">
            <div class="lev4">
              <ul>
                <!--<li><a href=""><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_09.gif" alt="" border="0" height="39" width="32">Delete</a></li>-->
                <li><a href="<?php echo base_url()?>admin/gift/post"><img src="<?php echo $this->config->item('base_url');?>public/admin/images/ico_lev4_13.gif" alt="" border="0" height="39" width="32">Create</a></li>
              </ul>
            </div>
            <h2>Gift</h2>
          </div>
          <br class="clear">
          <div class='color-red'><?php echo (isset($this->uri->vn_param['delete']))?$this->uri->vn_param['delete']:''?></div>
          <div style='color:red'>Found <?php echo  $total ?> result(s)</div>
          <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param)?></div>
          <form action="<?php echo base_url()?>admin/gift/index" id="fm_filter_gift" name="fm_filter_gift" method="post">
          <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
              <tr>
                <td colspan="11" width="5%">
				Keyword: <input name="keyword" value="<?php echo isset($this->uri->vn_param['keyword'])?$this->uri->vn_param['keyword']:''?>" type="text" id="keyword">
				Category: <?php echo form_dropdown('category', $categories, isset($this->uri->vn_param['category'])?$this->uri->vn_param['category']:'', "id='category' style='width:100px;'")?>
				<input value="Filter" type="submit" name='btn_submit' id="btn_submit" onclick="filter_gift()">
				<input value="Cancel" type="reset" name='cancel_list' id="cancel_list" onclick="javascript:window.location='<?php echo base_url()?>admin/gift/index'">
                </td>
              </tr>
              <tr>
                <th scope="col" width="5%">
				 <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'id-asc' || $this->uri->vn_param['sort'] == 'id-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'id-asc'):?>	
                		<a href="<?php echo uri_sort('id-desc')?>" class='color-red'>ID<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('id-asc')?>" class='color-red'>ID<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('id-asc')?>">ID</a>
                <?php endif;?>
				</th>
                <th scope="col" width="15%">
                <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'name-asc' || $this->uri->vn_param['sort'] == 'name-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'name-asc'):?>	
                		<a href="<?php echo uri_sort('name-desc')?>" class='color-red'>Name<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('name-asc')?>" class='color-red'>Name<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('name-asc')?>">Name</a>
                <?php endif;?>
                </th>
                <th scope="col" width="">Photo</th>
                <th scope="col" width="">Price Photo</th>
                <th scope="col" width="">Category</th>
                <th scope="col" width="15%">
                <?php if (isset($this->uri->vn_param['sort']) && ($this->uri->vn_param['sort'] == 'credits-asc' || $this->uri->vn_param['sort'] == 'credits-desc')):?>
                	<?php if ($this->uri->vn_param['sort'] == 'credits-asc'):?>	
                		<a href="<?php echo uri_sort('credits-desc')?>" class='color-red'>Credits<img src="<?php echo base_url()?>public/admin/images/s_desc.png" border="0"></a>
                	<?php else:?>
                		<a href="<?php echo uri_sort('credits-asc')?>" class='color-red'>Credits<img src="<?php echo base_url()?>public/admin/images/s_asc.png" border="0"></a>
                	<?php endif;?>
                <?php else:?>
                <a href="<?php echo uri_sort('credits-asc')?>">Credits</a>
                <?php endif;?>
                </th>
                <th scope="col" width="">Action</th>
              </tr>
            <?php if (count($results) > 0):?>
            <?php 
                $http=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https://" : "http://";
            ?>
			<?php foreach ($results as $row): ?>
              <tr>
                <td><?php echo $row->GiftID?></td>
                <td><?php echo $row->Name?></td>
                  <td><img src="<?php echo $this->config->item('s3_photo_url').$row->Photo;?>" style="width:80px; height:80px;"/></td>
                  <td><img src="<?php echo $this->config->item('s3_photo_url').$row->PricePhoto ;?>" style="width:80px; height:80px;"/></td>
                <td><?php echo $categories[$row->GiftCategoryID]?></td>
                <td><?php echo $row->Credits?></td>
                <td><a href="<?php echo base_url()?>admin/gift/post/<?php echo $row->GiftID?>">edit </a></td>
              </tr>
            <?php endforeach;?>
            <?php endif;?>
            </tbody>
          </table>
          </form>
      </div>
      <br class="clear">
    </div>
<?php include '_footer.php';?>