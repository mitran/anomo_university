<?php include '_header.php';?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-ui-1.8.9.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery.datetimepicker.js"></script>
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/smoothness/jquery-ui-1.8.9.custom.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/add_datetimepicker.css" rel="stylesheet" type="text/css" media="screen">

<script>
$(document).ready(function() {

	$('#submit').click(function() {
		var fb_ads = $("#fb_ads").val();
		if (fb_ads == ''|| isNaN(fb_ads)){
			$("#fb_ads").focus();
			return false;
		}
		var week = $("#week").val();
		if (week == ''){
			$("#week").focus();
			return false;
		}
	});

	
	$( "#week" ).datepicker({dateFormat: "yy-mm-dd"});

});
</script>

<style>
.tblAdminDetail td {
	text-align: none;
}
</style>
<div id="content">
  <div id="main">
    <div class="highlight">
      <h2>Marketing</h2>
    </div>
    <br class="clear">
     <br class="clear">
        <div class="highlight" style="text-align: left">
            <a href="<?php echo base_url() ?>admin/marketing/value">Value</a> | 
            <!--<a class="active menu-picture" href="<?php echo base_url() ?>admin/marketing/schedule">Schedule</a> |--> 
            <a  href="<?php echo base_url() ?>admin/marketing/core_feature_engagement">Core Feature Engagement</a> |
            <a  href="<?php echo base_url() ?>admin/marketing/content_engagement">Content Engagement</a> | 
            <a  href="<?php echo base_url() ?>admin/marketing/life_cycle_tracking">Life Cycle Tracking</a> |  
            <a  href="<?php echo base_url() ?>admin/marketing/kpi">KPI</a> |
            <a  href="<?php echo base_url() ?>admin/marketing/social_media">Social Media</a>       
        </div>
        <br class="clear">
    <form action="<?php echo base_url()?>admin/marketing/add_schedule" id="gplaces" name="gplaces" method="post">
    <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="80%">
            <tbody>
			<tr>
				<th>Week</th>
				<th>FBAds</th>
                <th>Twitter</th>
                <th>Content Contest</th>
                <th>Core Feature Contest</th>
                <th>Live Influencer</th>
                <th>Internal Contest</th>
                <th>Broadcast</th>
              </tr>
             </tbody>
             <tr>
             	<td><input type="text" name="week" id="week"/></td>
             	<td><input type="text" name="fb_ads" id="fb_ads" size=5/></td>
             	<td><input type="text" name="twitter"  id="twitter"/></td>
             	<td><input type="text" name="content_contest" id="content_contest" size=5/></td>
             	<td><input type="text" name="core_feature_contest"  id="core_feature_contest"/></td>
             	<td><input type="text" name="live_influencer" id="live_influencer"/></td>
             	<td><input type="text" name="internal_contest"  id="internal_contest"/></td>
             	<td><input type="text" name="broadcast" id="broadcast"/></td>
             </tr>
             <tr>
             	<td colspan=12><input value="Submit" type="submit" name='submit' id="submit"></td>
             </tr>
    </table>
    </form>
    <div style="height:20px;"></div>
     <div id="pagination"><?php echo $this->pagination->create_links($this->uri->vn_param)?></div>
    <?php if (isset($result) && count($result) > 0):?>
    <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
            <tbody>
			<tr>
				<th width="10%">Week</th>
				<th width="5%">FBAds</th>
                <th width="10%">Twitter</th>
                <th width="10%">Content Contest</th>
                <th width="10%">Core Feature Contest</th>
                <th width="10%">Live Influencer</th>
                <th width="10%">Internal Contest</th>
                <th width="10%">Broadcast</th>
                <th width="5%">Budget</th>
              </tr>
             </tbody>
             <?php foreach ($result as $re):?>
             <tr>
             	<td><?php echo $re->Week?></td>
             	<td><?php echo ($re->FBAds > 0)?'$'.$re->FBAds:''?></td>
             	<td><?php echo $re->Twitter?></td>
             	<td><?php echo ($re->ContentContest > 0)?'$'.$re->ContentContest:''?></td>
             	<td><?php echo $re->CoreFeatureContest?></td>
             	<td><?php echo $re->LiveInfluencer?></td>
             	<td><?php echo $re->InternalContest?></td>
             	<td><?php echo $re->Broadcast?></td>
             	<?php $budget = $re->FBAds + $re->ContentContest;?>
             	<td><?php echo ($budget > 0)?'$'.$budget:''?></td>
             </tr>
             <?php endforeach;?>
    </table>
    <?php endif;?>
  </div>
  <br class="clear">
</div>

<?php include '_footer.php';?>

