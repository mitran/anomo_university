<?php include '_header.php'; ?>
<script>
jQuery(function() {
    jQuery("img.lazy").lazyload();
});

function more_deleted_content(){
    var last_activity_id = jQuery('#last_activity_id').val();
    var user_id = jQuery('#user_id').val();
    jQuery('#myTable').append(html);
}

function more_deleted_content(){
    var last_activity_id = jQuery('#last_activity_id').val();
    var user_id = jQuery('#user_id').val();
    jQuery('#loading').show();
    jQuery('#btn_loading').attr("disabled", "disabled");
    
     jQuery.ajax({
            type:"POST",
            url: base_url+'admin/deleted_content/load_more/' + user_id + '/' + last_activity_id,
            success:function(response) {
                if (response.html != ''){
                    jQuery('#myTable').append(response.html);
                    jQuery('#last_activity_id').val(response.last_activity_id);
                    jQuery('#btn_loading').removeAttr("disabled");
                    jQuery('#loading').hide();
                }else{
                    // no more chat
                    jQuery('#loading').hide();
                    jQuery('#btn_loading').removeAttr("disabled");
                    jQuery('#loadmore').html('No more result!');
                }
            }
            , dataType: "json"
     });
}
</script>
<div id="content">
    <div id="main">
        <div class="highlight">
            <div class="lev4">

            </div>
            <h2>Deleted Content</h2>
        </div>

        <div class="highlight" style="text-align: left">
            <a href="<?php echo base_url() ?>admin/reportban_user/reported_content">Reported Content</a> | 
            <a href="<?php echo base_url() ?>admin/reportban_user/reported_user">Reported Users</a> |
            <a href="<?php echo base_url() ?>admin/reportban_user/exclude_reporting_users">Exclude Reporting Users</a> |
            <a class="active menu-picture" href="<?php echo base_url().'admin/deleted_content/index' ?>">Deleted Content</a> 
        </div>
        <br class="clear">
        
        <div style='color:red'>Found <?php echo $total ?> result(s)</div>
        <?php if (count($results) > 0): ?>
        <table id="myTable" class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
                <tr>
                    <th>UserID</th>
                    <th>UserName</th>
                    <th style="width: 400px">Content</th>
                    <th>Image</th>
                    <th>CreatedDate</th>
                    <th>DeletedDate</th>
                </tr>

                <?php foreach ($results as $row): ?>
                    <tr>
                        <td><?php echo $row->UserID ?></td>
                        <td><a href="<?php echo base_url().'admin/deleted_content/index/'.$row->UserID?>"><?php echo $row->UserName ?></a></td>
                        <td style="max-width: 400px"><?php echo substr($this->Dashboard_model->formatMsg($row->Message), 0, 300) ?></td>
                        <td>
                            <?php if ($row->Image):?>
                            <a href="<?php echo $row->Image ?>" data-lightbox="<?php echo $row->Image ?>"  >
                                <img style="max-width:150px;" class="lazy" data-original="<?php echo $row->Image ?>"  />
                            </a>
                            <?php elseif($row->VideoID):?>
                            <div>
                            <iframe class="play_video" type="text/html"   src="<?php echo ($row->VideoSource == 'youtube')?'http://www.youtube.com/embed/'. $row->VideoID: VINE_VIDEO_URL. $row->VideoID?>/embed/simple" frameborder="0" ></iframe>
                            </div>
                            <?php endif; ?>
                        </td>
                        <td><?php echo date('Y-m-d H:i:s', strtotime(PST.' hour', strtotime($row->CreatedDate))) ?></td>
                        <td><?php echo date('Y-m-d H:i:s', strtotime(PST.' hour', strtotime($row->DeletedDate))) ?></td>
                    </tr>
                <?php endforeach; ?>
                
        </table>
        <input id="user_id" type="hidden" value="<?php echo $user_id ?>">
        <input id="last_activity_id" type="hidden" value="<?php echo $results[count($results) - 1]->ActivityID ?>">
        <span id="loadmore" ><input class="btn_loading" type="button" name="btn_loading" id="btn_loading" value="Load more" onclick="more_deleted_content()"></span>
        <span id="loading" style="display: none;">  <img src="<?php echo $this->config->item('base_url'); ?>public/admin/images/ajax-loader.gif"></span>
        <?php endif; ?>
    </div>
    <br class="clear">
    <p style="text-align:right;"><a href="#top"><img id="gc-star" src="<?php echo $this->config->item('base_url');?>public/admin/images/back_to_top.gif"></a></p>
</div>
<?php include '_footer.php'; ?>