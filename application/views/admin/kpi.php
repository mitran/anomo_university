<?php include '_header.php'; ?>
<div id="content">
    <div id="main">
        <div class="highlight">
            <h2>Marketing</h2>
        </div>
        <br class="clear">
        <div class="highlight" style="text-align: left">
            <a href="<?php echo base_url() ?>admin/marketing/value">Value</a> | 
            <a  href="<?php echo base_url() ?>admin/marketing/core_feature_engagement_new">New Core Feature Engagement</a> |
            <a href="<?php echo base_url() ?>admin/marketing/content_engagement">Content Engagement</a> | 
            <a href="<?php echo base_url() ?>admin/marketing/life_cycle_tracking">Life Cycle Tracking</a> |  
            <a class="active menu-picture" href="<?php echo base_url() ?>admin/marketing/kpi">KPI</a> |
            <a href="<?php echo base_url() ?>admin/marketing/social_media">Social Media</a>     
        </div>
        <br class="clear">
        <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="80%">
            <tbody>
                <tr>
                    <th><b>Week</b></th>
                    <th><b>#Download</b></th>
                    <th><b>#Account</b></th>    
                    <th><b>#Avg Daily Actives</b></th>
                    <th><b>#Avg Daily Sessions per User</b></th>
                    <th><b>#WAU</b></th>
                </tr>
                <?php foreach ($data as $row):?>
                <tr>
                    <td width="25%"><?php echo $row['Week']?></td>
                    <td><?php echo $row['Download']?></td>
                    <td><?php echo $row['Account']?></td>
                    <td><?php echo $row['AvgDailyActive']?></td>
                    <td><?php echo $row['AvgDailySessionPerUser']?></td>
                    <td><?php echo $row['DAU']?></td>
                 
                </tr>
                <?php endforeach;?>
                
            </tbody>
        </table>
    </div>
    <br class="clear">
</div>

<?php include '_footer.php'; ?>
