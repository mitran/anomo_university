<?php include '_header.php';?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery-ui-1.8.9.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>public/admin/js/jquery.datetimepicker.js"></script>
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/smoothness/jquery-ui-1.8.9.custom.css" rel="stylesheet" type="text/css" media="screen">
<link href="<?php echo $this->config->item('base_url');?>public/admin/css/add_datetimepicker.css" rel="stylesheet" type="text/css" media="screen">

<script>
$(document).ready(function() {
	$( "#from" ).datepicker({dateFormat: "yy-mm-dd"});
	$( "#to" ).datepicker({dateFormat: "yy-mm-dd"});
});
</script>

<style>
.tblAdminDetail td {
	text-align: none;
}
</style>
<div id="content">
  <div id="main">
    <div class="highlight">
      <h2>Report</h2>
    </div>
    <br class="clear">
    <table class="tblAdminDetail" border="0" cellpadding="0" cellspacing="1" width="100%">
      <tbody>
        <tr>
          <td>
            <form action="<?php echo base_url()?>admin/report" id="gplaces" name="gplaces" method="post">
              From: <input id="from" name="from" value="<?php echo isset($from)?$from:'';?>" type="text" style="width:100px;">
              To: <input id="to" name="to" value="<?php echo isset($to)?$to:'';?>" type="text" style="width:100px;">
              <input value="Search" type="submit" name='btn_submit' id="btn_submit">
			  
            </form>
          </td>
        </tr>
      </tbody>
    </table>
    <div style="height:20px;"></div>
    <?php if (isset($result)):?>
    <table border="0" cellpadding="0" cellspacing="1" width="100%" style="background:#d0ccc9;">
            <tbody>
			<tr>
                <td width="25%" style="background:#FFFFFF; text-align:left; padding:3px 5px;" ><b>Total # of users as of <?php echo date('M d Y', strtotime($to))?> 11:59pm PST:  </b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['total']?></td>
              </tr>
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;" colspan=2><b>New Registrations</b> </td>
              </tr>
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;" colspan=2><b>By Device</b> </td>
              </tr>
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;" ><b>Android  </b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['android']?></td>
              </tr>
               <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;"><b>IOS  </b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['ios']?></td>
              </tr>
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;"><b>Total  </b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['devices']?></td>
              </tr>
              
              <!--
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;" colspan=2><b>By Verified</b> </td>
              </tr>
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;"><b>Verified  </b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['verified']?></td>
              </tr>
               <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;"><b>Unverified   </b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['unverified']?></td>
              </tr>
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;"><b>Total  </b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['register']?></td>
              </tr>
              -->
              
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;" colspan=2><b>Engagement</b> </td>
              </tr>
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;"><b>Ice breaker # of Games  </b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['game']?></td>
              </tr>
              
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;"><b># of gifts   </b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['gift']?></td>
              </tr>
              
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;"><b># of Pictures Uploaded  </b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['picture']?></td>
              </tr>
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;"><b># comments </b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['comment']?></td>
              </tr>
              
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;"><b># likes  </b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['like']?></td>
              </tr>
              <!-- NEW USER -->
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;" colspan=2><b>New User</b> </td>
              </tr>
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;"><b>Total</b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['newuser']?></td>
              </tr>
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;"><b>Total SMS verified out</b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['newuser_sms']?></td>
              </tr>
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;"><b>Total FB verified out</b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['newuser_facebook']?></td>
              </tr>
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;"><b>Total manual verified out</b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['newuser_manual']?></td>
              </tr>
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;"><b>Total not verified out</b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['newuser_unverified']?></td>
              </tr>
              <!-- DAILY MATCH -->             
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;" colspan=2><b>Daily Match</b> </td>
              </tr>
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;"><b># of matches</b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['match']?></td>
              </tr>
              <tr>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;text-align:right;"><b># successful chats from the matches</b> </td>
                <td style="background:#FFFFFF; text-align:left; padding:3px 5px;"><?php echo $result['match_success']?></td>
              </tr>
              
             </tbody>
    </table>
    <?php endif;?>
  </div>
  <br class="clear">
</div>

<?php include '_footer.php';?>

