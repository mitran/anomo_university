<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/assets/css/anomo_match3.css"/>
<div class="anomo-match">
	<div class="section-profile">
		<div class="header"></div>
		<table class="table-profile" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td width="155">
					<span class="avatar" style="background-image:url(<?php echo base_url();?>public/assets/img/match_avatar.jpg);"></span>
				</td>
				<td>
					<h3>WildChild</h3>
					<table class="table-profile-info" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td width="35">
								<span class="age">24</span>
							</td>
							<td>
								<address>Los Angeles, CA</address>
							</td>
						</tr>
					</table>						 
				</td>
			</tr>
		</table>    	
	</div>
	<div class="section-category">
		<div class="header">
			<h2>You both matched:</h2>
		</div>
		<div class="category-list">
			<a class="category-item" title="" href="#"><span class="corner-left"><span class="corner-right"><span class="corner-center">Shopping</span></span></span></a>
			<a class="category-item" title="" href="#"><span class="corner-left"><span class="corner-right"><span class="corner-center">Morning workout</span></span></span></a>
			<a class="category-item" title="" href="#"><span class="corner-left"><span class="corner-right"><span class="corner-center">Cooking</span></span></span></a>
			<a class="category-item" title="" href="#"><span class="corner-left"><span class="corner-right"><span class="corner-center">Horse</span></span></span></a>
			<a class="category-item" title="" href="#"><span class="corner-left"><span class="corner-right"><span class="corner-center">Sports</span></span></span></a>
			<a class="category-item" title="" href="#"><span class="corner-left"><span class="corner-right"><span class="corner-center">Facebook</span></span></span></a>
			<a class="category-item" title="" href="#"><span class="corner-left"><span class="corner-right"><span class="corner-center">Drivingfast</span></span></span></a>			
		</div>
	</div>
	<div class="section-feedback">
		<div class="header">
			<h2>You both answered:</h2>
		</div>
		<div class="feedback-list">
			<div class="feedback-item feedback-i clearfix">
				<table cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td class="corner corner-top-left"></div>
						<td></div>
						<td class="corner corner-top-right"></div>
					</tr>
					<tr>
						<td></td>
						<td>
							Q: If you had the lavish life of a celebrity A-lister, which sinful Vegas hotspot would suit your extravagant nightlife?
						</td>
						<td></td>
					</tr>
					<tr>
						<td class="corner corner-bottom-left"></td>
						<td></td>
						<td class="corner corner-bottom-right"></td>
					</tr>
				</table>
			</div>
			<div class="feedback-item feedback-u clearfix" align="right">
				<table cellspacing="0" cellpadding="0" border="0" align="right">
					<tr>
						<td class="corner corner-top-left"></div>
						<td></div>
						<td class="corner corner-top-right"></div>
					</tr>
					<tr>
						<td></td>
						<td>
							A: Cathouse Luxor
						</td>
						<td></td>
					</tr>
					<tr>
						<td class="corner corner-bottom-left"></td>
						<td></td>
						<td class="corner corner-bottom-right"></td>
					</tr>
				</table>
			</div>
			<div class="feedback-item feedback-i clearfix">
				<table cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td class="corner corner-top-left"></div>
						<td></div>
						<td class="corner corner-top-right"></div>
					</tr>
					<tr>
						<td></td>
						<td>
							Q: What's your favarite drink?
						</td>
						<td></td>
					</tr>
					<tr>
						<td class="corner corner-bottom-left"></td>
						<td></td>
						<td class="corner corner-bottom-right"></td>
					</tr>
				</table>
			</div>
			<div class="feedback-item feedback-u clearfix" align="right">
				<table cellspacing="0" cellpadding="0" border="0" align="right">
					<tr>
						<td class="corner corner-top-left"></div>
						<td></div>
						<td class="corner corner-top-right"></div>
					</tr>
					<tr>
						<td></td>
						<td>
							A: Tea
						</td>
						<td></td>
					</tr>
					<tr>
						<td class="corner corner-bottom-left"></td>
						<td></td>
						<td class="corner corner-bottom-right"></td>
					</tr>
				</table>
			</div>
			<div class="feedback-item feedback-i clearfix">
				<table cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td class="corner corner-top-left"></div>
						<td></div>
						<td class="corner corner-top-right"></div>
					</tr>
					<tr>
						<td></td>
						<td>
							Q: What's your ideal living situation when you are older?
						</td>
						<td></td>
					</tr>
					<tr>
						<td class="corner corner-bottom-left"></td>
						<td></td>
						<td class="corner corner-bottom-right"></td>
					</tr>
				</table>
			</div>
			<div class="feedback-item feedback-u clearfix" align="right">
				<table cellspacing="0" cellpadding="0" border="0" align="right">
					<tr>
						<td class="corner corner-top-left"></div>
						<td></div>
						<td class="corner corner-top-right"></div>
					</tr>
					<tr>
						<td></td>
						<td>
							A: Married with a house
						</td>
						<td></td>
					</tr>
					<tr>
						<td class="corner corner-bottom-left"></td>
						<td></td>
						<td class="corner corner-bottom-right"></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

