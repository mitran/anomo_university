<?php
class Factual {
	private $apiUrl = "http://api.factual.com/v2/tables/s4OOB4/read.json";
//	private $api3Url = "http://api.v3.factual.com/t/places/read?";
	private $apiKey = FACTUAL_API_KEY; 
	private $oAuthKey = "BAUAcLIp1KVMvCFUmbhpMploHf7iZ4f5GuuvXFGy"; // for V3
	private $oAuthSerect = "2D3hOTp3qZ8yAofCwpfFIsqjnQCxKyBKhuMbTZFl"; // for V3
	private $limit = 20;
	
	/**
	 * search by keyword and location
	 *
	 * @param string $keyword
	 * @param float $lat
	 * @param float $lon
	 * @param int $radius
	 * @return array
	 */
	public function searchByKeywordAndLocation($keyword='', $lat, $lon) {
		$CI =& get_instance();
		$CI->load->model('config_model');
		$radius 	= $CI->config_model->getConfig('SEARCH_RADIUS');
		$radius 	= $radius*1609.344; // meters
		$keyword 	= urlencode($keyword);
		$urlToCall 	= $this->apiUrl.'?APIKey='.$this->apiKey.'&filters={"$search":"'.$keyword.'","$loc":{"$within":{"$center":[['.$lat.','.$lon.'],'.$radius.']}}}';
		return $this->send($urlToCall);
	}
	
	/**
	 * search by location
	 *
	 * @param float $lat
	 * @param float $lon
	 * @param int $radius
	 * @return array
	 */
	public function searchByLocation($lat, $lon, $page=1) {
		$CI =& get_instance();
		$CI->load->model('config_model');
		$radius 	= $CI->config_model->getConfig('SEARCH_RADIUS');
		$radius 	= $radius*1609.344; // meters
		$offset 	= ($page -1)*$this->limit;
		$urlToCall 	= $this->apiUrl.'?APIKey='.$this->apiKey.'&limit='.$this->limit.'&offset='.$offset.'&filters={"$loc":{"$within":{"$center":[['.$lat.','.$lon.'],'.$radius.']}}}';
		return $this->send($urlToCall);
	}
	
	/**
	 * get detail of FactualID
	 *
	 * @param string $factual_id
	 * @return array
	 */
	public function getDetail($factual_id) {
		$urlToCall = $this->apiUrl.'?APIKey='.$this->apiKey.'&filters={"factual_id":"'.$factual_id.'"}';
		return $this->send($urlToCall);
	  
	}
	
	public function send($urlToCall) {
		$curl = curl_init();  
		curl_setopt($curl, CURLOPT_URL, $urlToCall);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  
		curl_setopt($curl, CURLOPT_FAILONERROR, true);  
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  
		$response = curl_exec($curl);  
		if (!$response) {  
		    $response = curl_error($curl);  
		}  
		curl_close($curl);  
		return json_decode($response, true); 
	}
	
	/** 
	 * Makes an HTTP request to the specified URL 
	 * @param string $http_method The HTTP method (GET, POST, PUT, DELETE) 
	 * @param string $url Full URL of the resource to access 
	 * @param string $auth_header (optional) Authorization header 
	 * @param string $postData (optional) POST/PUT request body 
	 * @return string Response body from the server 
	 */  
	function send_request($http_method, $url, $auth_header=null, $postData=null) { 
		  $curl = curl_init();  
		  curl_setopt($curl, CURLOPT_URL, $url);
		  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  
		  curl_setopt($curl, CURLOPT_FAILONERROR, true);  
		  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  
		  
		  switch($http_method) {  
		    case 'GET':  
		      if ($auth_header) {  
		        curl_setopt($curl, CURLOPT_HTTPHEADER, array($auth_header));   
		      }  
		      break;  
		    case 'POST':  
		      curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/atom+xml',   
		                                                   $auth_header));   
		      curl_setopt($curl, CURLOPT_POST, 1);                                         
		      curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);  
		      break;  
		    case 'PUT':  
		      curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/atom+xml',   
		                                                   $auth_header));   
		      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $http_method);  
		      curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);  
		      break;  
		    case 'DELETE':  
		      curl_setopt($curl, CURLOPT_HTTPHEADER, array($auth_header));   
		      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $http_method);   
		      break;  
		  }  
		  $response = curl_exec($curl);  
		  if (!$response) {  
		    $response = curl_error($curl);  
		  }  
		  curl_close($curl);  
		  return $response;  
	}  
	  
	/** 
	 * Joins key:value pairs by inner_glue and each pair together by outer_glue 
	 * @param string $inner_glue The HTTP method (GET, POST, PUT, DELETE) 
	 * @param string $outer_glue Full URL of the resource to access 
	 * @param array $array Associative array of query parameters 
	 * @return string Urlencoded string of query parameters 
	 */  
	function implode_assoc($inner_glue, $outer_glue, $array) {  
		  $output = array();  
		  foreach($array as $key => $item) {  
		    $output[] = $key . $inner_glue . urlencode($item);  
		  }  
		  return implode($outer_glue, $output);  
	}  
}
?>