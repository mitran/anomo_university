<?php
require_once(APPPATH . 'libraries/SolrPHPClient/Service.php');
class MySolr{
    function __construct() {
        $this->solr = new Apache_Solr_Service(SOLR_HOSTNAME, SOLR_PORT, SOLR_PATH);
    }
    
    /**
     * 
     * @param type $user_id
     * @param type $target_user_id
     */
    function addIndex($user_id, $target_user_id){
        $sql = "select  $user_id as `name`,
                        (select UserName from `user` where UserID = $user_id) as description,
			$target_user_id as `resourcename`,
			(select UserName from `user` where UserID = $target_user_id) as `keywords`
                ";
        $CI =& get_instance();
        $query = $CI->db->query($sql);           
        $fields = $query->row();
        $documents = array();
        if ($fields){
            $part = new Apache_Solr_Document();
            $part->addField('id', $fields->name . '-' . $fields->resourcename);

            foreach ($fields as $key => $value) {
                if (is_array($value)) {
                    foreach ($value as $data) {
                        $part->addField($key, $data);
                    }
                }
                else {
                    $part->$key = $value;
                }
            }
            $documents[] = $part;

            try {
                $this->solr->addDocuments($documents);;
                $this->solr->commit();
                $this->solr->optimize();
            }
            catch (Exception $e) {
                echo $e->getMessage() . '<br/>';
            }
        }
    }
    
    /** 
     * 
     * @param type $user_id
     * @param type $target_user_id
     */
    function deleteIndex($user_id, $target_user_id){
        $id = $user_id.'-'.$target_user_id;
        $rawPost = "<delete><query>id:$id</query></delete>";
        $this->solr->delete($rawPost);
        $this->solr->commit();
        $this->solr->optimize();
    }
    
    /**
     * when user change username, must re-indexing
     * @param type $user_id
     */
    function reIndex($user_id){
        $CI =& get_instance();
        $sql1 = "select CurrentUserID, TargetUserID from userconnect where IsFavorite = 1 AND (CurrentUserID = $user_id OR TargetUserID = $user_id)";
        $query1 = $CI->db->query($sql1);           
        $result = $query1->result();
        // 1. delete old indexing
        $aId = array();
        if ($result){
            foreach ($result as $re){
                $aId [] = $re->CurrentUserID.'-'.$re->TargetUserID;
            }
            $id = "(".implode('OR', $aId).")"; 
            $rawPost = "<delete><query>id:$id</query></delete>";
            $this->solr->delete($rawPost);
            $this->solr->commit();
            $this->solr->optimize();
        }
        
        //2 . Re-indexing with new UserName
        $sql = "select  CurrentUserID as `name`,
                        (select UserName from `user` where UserID = CurrentUserID) as description,
			            TargetUserID as `resourcename`,
			            (select UserName from `user` where UserID = TargetUserID) as `keywords`
                from userconnect where IsFavorite = 1 AND (CurrentUserID = $user_id OR TargetUserID = $user_id)";
        $query = $CI->db->query($sql);
        
        $documents = array();

        foreach ($query->result() as $fields) {

            $part = new Apache_Solr_Document();
            $part->addField('id', $fields->name . '-' . $fields->resourcename);

            foreach ($fields as $key => $value) {
                if (is_array($value)) {
                    foreach ($value as $data) {
                        $part->addField($key, $data);
                    }
                }
                else {
                    $part->$key = $value;
                }
            }
            $documents[] = $part;
        }

        try {
            $this->solr->addDocuments($documents);
            $this->solr->commit();
            $this->solr->optimize();
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
?>
