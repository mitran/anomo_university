<?php
require_once 'FactualLocationServiceProvider.php';
require_once 'GoogleLocationServiceProvider.php';
class SearchVenue{
	public function getInstance(){
		$CI =& get_instance();
		$CI->load->model('Config_model');
		if ($CI->Config_model->getConfig('SEARCH_SERVICE_PROVIDER') == 'factual')
			return new FactualLocationServiceProvider();
		else 
			return new GoogleLocationServiceProvider();
	}
}
?>