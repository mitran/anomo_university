<?php
require_once 'ILocationServiceProvider.php';
class FactualLocationServiceProvider implements ILocationServiceProvider {
//	private $apiUrl = "http://api.factual.com/v2/tables/s4OOB4/read.json"; // version 2
	private $apiUrl = "http://api.v3.factual.com/t/global"; // version 3
	private $categoryUrl = "http://developer.factual.com/download/attachments/1310739/categories.json";
	private $apiKey = FACTUAL_API_KEY; 
	private $oauthKey = OAUTH_KEY;
	private $oauthSecret = OAUTH_SECRET;
	
	/**
	 * search by keyword and location
	 *
	 * @param string $keyword
	 * @param float $lat
	 * @param float $lon
	 * @param int $radius
	 * @return array
	 */
	public function searchByKeywordAndLocation($keyword='', $lat, $lon, $page=1, $page_token = '') {
		$CI =& get_instance();
		$CI->load->model('Config_model');
		$CI->load->model('place_model');
		$limit 		= $CI->Config_model->getConfig('NUMBER_VENUE');
		$radius 	= $CI->Config_model->getConfig('SEARCH_RADIUS_KEYWORD');
		$offset 	= ($page -1)*$limit;
//		$radius 	= $radius*1609.344; // meters
		$keyword 	= urlencode($keyword);
		$urlToCall 	= $this->apiUrl.'?limit='.$limit.'&offset='.$offset.'&geo={"$circle":{"$center":['.$lat.','.$lon.'],"$meters":'.$radius.'}}&filters={"name":{"$bw":"'.$keyword.'"}}&include_count=true';
		
		$result = $this->send($urlToCall);
		$radius = $radius * 2;
		$num = isset($result['response']['total_row_count']) ? $result['response']['total_row_count'] : 0;
		
		while ($radius < 16093.44 && $num < 10) {
			$urlToCall 	= $this->apiUrl.'?limit='.$limit.'&offset='.$offset.'&geo={"$circle":{"$center":['.$lat.','.$lon.'],"$meters":'.$radius.'}}&filters={"name":{"$bw":"'.$keyword.'"}}&include_count=true';
		
			$result = $this->send($urlToCall);
			$radius = $radius * 2;
			$num = isset($result['response']['total_row_count']) ? $result['response']['total_row_count'] : 0;
		}
		if ($result['status'] == 'ok'){
			if ($result['response']['total_row_count'] > 0){
				$data['pagetoken'] = $page_token;
				$data['total'] = ceil($result['response']['total_row_count']/$limit);
				foreach ($result['response']['data'] as $entry){
					$data['results'][] = array(
						'ID' 		=> $entry['factual_id'],
						'Name'		=> $entry['name'],
						'Address'	=> $entry['address'],
						'Lat'		=> $entry['latitude'],
						'Lon'		=> $entry['longitude'],
						'Phone'		=> isset($entry['tel'])?$entry['tel']:"",
						'Email'		=> isset($entry['email'])?$entry['email']:"",
						'Website'	=> isset($entry['website'])?$entry['website']:"",
						'Reference'	=> NULL,
						'Category'	=> isset($entry['category'])?array($entry['category']): array(),
					);
				}
			}		
		}
		return isset($data)?$data:FALSE;
	}
	
	/**
	 * search by keyword and location
	 *
	 * @param string $keyword
	 * @param float $lat
	 * @param float $lon
	 * @param int $radius
	 * @return array
	 */
	public function searchByKeywordAndLocationV2($lat, $lon, $category='', $keyword='') {
		$CI =& get_instance();
		$CI->load->model('Config_model');
		$radius 	= $CI->Config_model->getConfig('SEARCH_RADIUS_KEYWORD');
//		$radius 	= $radius*1609.344; // meters
		$keyword 	= urlencode(trim($keyword));
		$category 	= urlencode($category);
		$urlToCall 	= $this->apiUrl.'?geo={"$circle":{"$center":['.$lat.','.$lon.'],"$meters":'.$radius.'}}&filters={"name":{"$bw":"'.$keyword.'"},"category":{"$bw":"'.$category.'"}}&include_count=true';
		$result = $this->send($urlToCall);
		return $result;
	}
	
	/**
	 * load categories
	 *
	 * @param string $keyword
	 * @param float $lat
	 * @param float $lon
	 * @param int $radius
	 * @return array
	 */
	public function loadCategories() {
		$results = $this->send($this->categoryUrl);
		if ($results) {
			return $results['categories'];
		}
		return array();
	}
	
	/**
	 * search by location5
	 *
	 * @param float $lat
	 * @param float $lon
	 * @param int $radius
	 * @return array
	 */
	public function searchByLocation($lat, $lon, $page=1, $page_token = '') {
		$CI =& get_instance();
		$CI->load->model('Config_model');
		$limit 		= $CI->Config_model->getConfig('NUMBER_VENUE');
		$radius 	= $CI->Config_model->getConfig('SEARCH_RADIUS');
//		$radius 	= $radius*1609.344; // meters
		$offset 	= ($page -1)*$limit;
		$urlToCall 	= $this->apiUrl.'?limit='.$limit.'&offset='.$offset.'&geo={"$circle":{"$center":['.$lat.','.$lon.'],"$meters":'.$radius.'}}&include_count=true';
		$result = $this->send($urlToCall);

		$radius = $radius * 2;
		$num = isset($result['response']['total_row_count']) ? $result['response']['total_row_count'] : 0;
		
		while ($radius < 16093.44 && $num < 10) {
			$urlToCall 	= $this->apiUrl.'?limit='.$limit.'&offset='.$offset.'&geo={"$circle":{"$center":['.$lat.','.$lon.'],"$meters":'.$radius.'}}&include_count=true';
			$result = $this->send($urlToCall);
			$radius = $radius * 2;
			$num = isset($result['response']['total_row_count']) ? $result['response']['total_row_count'] : 0;
		}

		if ($result['status'] == 'ok'){
			if ($result['response']['total_row_count'] > 0){
				$data['pagetoken'] = $page_token;
				$data['total'] = ceil($result['response']['total_row_count']/$limit);
				foreach ($result['response']['data'] as $entry){
					$data['results'][] = array(
						'ID' 		=> $entry['factual_id'],
						'Name'		=> $entry['name'],
						'Address'	=> $entry['address'],
						'Lat'		=> $entry['latitude'],
						'Lon'		=> $entry['longitude'],
						'Phone'		=> isset($entry['tel'])?$entry['tel']:"",
						'Email'		=> isset($entry['email'])?$entry['email']:"",
						'Website'	=> isset($entry['website'])?$entry['website']:"",
						'Reference'	=> NULL,
						'Category'	=> isset($entry['category'])?array($entry['category']): array(),
					);
				}
			}		
		}
		return isset($data)?$data:FALSE;
	}
	
	/**
	 * get detail of FactualID
	 *
	 * @param string $factual_id
	 * @return array
	 */
	public function getDetail($factual_id) {
		$urlToCall = $this->apiUrl.'?filters={"factual_id":"'.$factual_id.'"}&include_count=true';
		$result = $this->send($urlToCall);
		if ($result['status'] == 'ok'){
			if ($result['response']['total_row_count'] > 0){
				foreach ($result['response']['data'] as $entry){
					$data = array(
						'ID' 		=> $entry['factual_id'],
						'Name'		=> $entry['name'],
						'Address'	=> $entry['address'],
						'Lat'		=> $entry['latitude'],
						'Lon'		=> $entry['longitude'],
						'Phone'		=> isset($entry['tel'])?$entry['tel']:"",
						'Email'		=> isset($entry['email'])?$entry['email']:"",
						'Website'	=> isset($entry['website'])?$entry['website']:"",
						'Reference'	=> NULL,
						'Category'	=> isset($entry['category'])?array($entry['category']): array(),
					);
				}
			}		
		}
		return isset($data)?$data:FALSE;
	}
	
	public function send($urlToCall){
		$curl = curl_init();  
		curl_setopt($curl, CURLOPT_URL, $urlToCall);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  
		//curl_setopt($curl, CURLOPT_FAILONERROR, true);  
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  

		$oauth = array( 'oauth_consumer_key' => $this->oauthKey,
                'oauth_nonce' => time(),
                'oauth_signature_method' => 'HMAC-SHA1',
                'oauth_token' => $this->oauthSecret,
                'oauth_timestamp' => time(),
                'oauth_version' => '1.0');
		$r = 'Authorization: OAuth '; 
		$values = array(); 
    	foreach($oauth as $key=>$value){
        	$values[] = "$key=\"" . rawurlencode($value) . "\"";
    	} 
    	$r .= implode(', ', $values); 
		curl_setopt($curl, CURLOPT_HTTPHEADER, array($r));
		
		$response = curl_exec($curl);
		if (!$response) {  
		    $response = curl_error($curl);  
		}  
		curl_close($curl);  
		return json_decode($response, true); 
	}
}
?>