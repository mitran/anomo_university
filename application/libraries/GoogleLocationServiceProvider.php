<?php
require_once 'ILocationServiceProvider.php';
class GoogleLocationServiceProvider implements ILocationServiceProvider{
	private $apiUrl = "https://maps.googleapis.com/maps/api/place";
	private $apiKey = GOOGLE_API_KEY; 
	private $types = "airport|amusement_park|aquarium|art_gallery|bar|beauty_salon|book_store|bowling_alley|cafe|campground|casino|church|city_hall|clothing_store|department_store|electronics_store|food|grocery_or_supermarket|gym|hindu_temple|hospital|laundry|library|lodging|mosque|movie_theater|museum|night_club|park|place_of_worship|restaurant|school|shopping_mall|spa|stadium|store|subway_station|synagogue|train_station|university|zoo"; //optional - separate tyep with pipe symbol http://code.google.com/apis/maps/documentation/places/supported_types.html
	private $sensor = 'false';
	private $language = 'en'; 
	/**
	 * search by keyword and location
	 *
	 * @param string $keyword
	 * @param float $lat
	 * @param float $lon
	 * @param int $radius
	 * @return array
	 */
	public function searchByKeywordAndLocation($keyword='', $lat, $lon, $page=1, $page_token = '') {
		$CI =& get_instance();
		$CI->load->model('Config_model');
		$limit 		= $CI->Config_model->getConfig('NUMBER_VENUE');
		$radius 	= $CI->Config_model->getConfig('SEARCH_RADIUS_KEYWORD');
		$rankby 	= $CI->Config_model->getConfig('RANKBY');
//		$radius 	= 3000;//$radius*1609.344; // meters
		$keyword 	= urlencode($keyword);
		$urlToCall 	= $this->apiUrl.'/textsearch/json?key='.$this->apiKey.'&types='.$this->types.'&language='.$this->language.'&sensor='.$this->sensor.'&query='.$keyword.'&location='.$lat.','.$lon.'&radius='.$radius;
		if (trim($page_token) != ''){
			$urlToCall .= '&pagetoken='.$page_token;
		}

		//		if ($rankby == 'distance'){
//        	$urlToCall .= '&rankby=distance';
//        }else{
//	        if($radius!=''){
//	             $urlToCall .= '&radius='.$radius;
//	        }
//        }

		$result = $this->send($urlToCall);

		if ($result['status'] == 'OK') {
			$data['total'] = ceil(count($result['results'])/$limit);
			$data['pagetoken'] = isset($result['next_page_token'])?$result['next_page_token']:"";
			foreach ($result['results'] as $entry){
			// if here 
				// michael added here to remove dentist and docter from search result as requested by AA - ANOMO-3403
				//$type = split("|", $entry['types']);
				
				if (in_array('dentist',$entry['types']) == false && in_array('doctor',$entry['types']) == false
						&& in_array('bus_station', $entry['types']) == false) {
					$data['results'][] = array(
						'ID'		=> $entry['id'],
						'Reference'	=> $entry['reference'],
						'Name' 		=> isset($entry['name'])?$entry['name']:'',
						'Address'	=> isset($entry['formatted_address'])?$entry['formatted_address']:'',
						'Website'	=> isset($entry['website'])?$entry['website']:'',
						'Lat'		=> isset($entry['geometry']['location']['lat'])?$entry['geometry']['location']['lat']:'',
						'Lon'		=> isset($entry['geometry']['location']['lng'])?$entry['geometry']['location']['lng']:'',
						'Phone'		=> isset($entry['formatted_phone_number'])?$entry['formatted_phone_number']:'',
						'Email'		=> NULL,
						'Category'	=> $entry['types'],
					);
				}
			}
		}
		return isset($data)?$data:FALSE;
	}
	
	/**
	 * search by location5
	 *
	 * @param float $lat
	 * @param float $lon
	 * @param int $radius (mile)
	 * @return array
	 */
	public function searchByLocation($lat, $lon, $page=1, $page_token = '') {
		$CI =& get_instance();
		$CI->load->model('Config_model');
		$limit 		= $CI->Config_model->getConfig('NUMBER_VENUE');
		$radius 	= $CI->Config_model->getConfig('SEARCH_RADIUS');
		$rankby 	= $CI->Config_model->getConfig('RANKBY');
//		$radius 	= $radius*1609.344; // meters
		
		$urlToCall 	= $this->apiUrl.'/search/json?key='.$this->apiKey.'&language='.$this->language.'&sensor='.$this->sensor.'&location='.$lat.','.$lon.'&types='.$this->types;
		
		if ($rankby == 'distance'){
        	$urlToCall .= '&rankby=distance';
        }else{
	        if($radius!=''){
	             $urlToCall .= '&radius='.$radius;
	        }
        }
        
		if (trim($page_token) != ''){
			$urlToCall .= '&pagetoken='.$page_token;
		}
		$result = $this->send($urlToCall);

		if ($result['status'] == 'OK'){
			$data['total'] = ceil(count($result['results'])/$limit);
			$data['pagetoken'] = isset($result['next_page_token'])?$result['next_page_token']:"";
			foreach ($result['results'] as $entry) {
			//	$type = split("|", $entry['types']);
				
				if (in_array('dentist',$entry['types']) == false && in_array('doctor',$entry['types']) == false
						&& in_array('bus_station', $entry['types']) == false) {
					$data['results'][] = array(
						'ID'		=> $entry['id'],
						'Reference'	=> $entry['reference'],
						'Name' 		=> isset($entry['name'])?$entry['name']:'',
						'Address'	=> isset($entry['vicinity'])?$entry['vicinity']:'',
						'Website'	=> isset($entry['website'])?$entry['website']:'',
						'Lat'		=> isset($entry['geometry']['location']['lat'])?$entry['geometry']['location']['lat']:'',
						'Lon'		=> isset($entry['geometry']['location']['lng'])?$entry['geometry']['location']['lng']:'',
						'Phone'		=> isset($entry['formatted_phone_number'])?$entry['formatted_phone_number']:'',
						'Email'		=> NULL,
						'Category'	=> $entry['types'],
					);
				}
			}
		}
		return isset($data)?$data:FALSE;
	}
	
	/**
	 * get detail of FactualID
	 *
	 * @param string $factual_id
	 * @return array
	 */
	public function getDetail($id){
		$urlToCall = $this->apiUrl.'/details/json?key='.$this->apiKey.'&reference='.$id.'&sensor=false';
		$result = $this->send($urlToCall);

		if ($result['status'] == 'OK'){
			$types = $result['result']['types'];
			if (count($types) > 0){
				foreach ($types as $key => $value){
					if (in_array($value, array('bus_station', 'doctor', 'dentist'))){
						unset($types[$key]);
					}
				}
			}

			$data = array(
				'ID'		=> $result['result']['id'],
				'Name' 		=> $result['result']['name'],
				'Address'	=> $result['result']['formatted_address'],
				'Lat'		=> $result['result']['geometry']['location']['lat'],
				'Lon'		=> $result['result']['geometry']['location']['lng'],
				'Email'		=> NULL,
				'Website'	=> isset($result['result']['website'])?$result['result']['website']:NULL,
				'Phone'		=> isset($result['result']['formatted_phone_number'])?$result['result']['formatted_phone_number']:NULL,
				'Reference'	=> $result['result']['reference'],
				'Category'	=> $types,
			);
		}
		
		return isset($data)?$data:FALSE;
	}
	
	public function send($urlToCall){
		$curl = curl_init();  
		curl_setopt($curl, CURLOPT_URL, $urlToCall);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  
		curl_setopt($curl, CURLOPT_FAILONERROR, true);  
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  
		$response = curl_exec($curl);  
		if (!$response) {  
		    $response = curl_error($curl);  
		}  
		curl_close($curl);  
		return json_decode($response, true); 
	}
    
    
    public function searchPlaces($keyword = '', $lat, $lon, $radius = '', $types='', $rankby = 'prominence'){
        $urlToCall 	= $this->apiUrl.'/search/json?key='.$this->apiKey.'&language='.$this->language.'&sensor='.$this->sensor.'&location='.$lat.','.$lon;
       
    	if (trim($keyword) != ''){
			$keyword = urlencode($keyword);
			$urlToCall .= '&keyword='.$keyword;
		}
		
        if($types !=''){
            $urlToCall .='&types='.$types;
        }
        
        if ($rankby == 'distance'){
        	$urlToCall .= '&rankby=distance';
        }else{
	        if($radius!=''){
	             $urlToCall .= '&radius='.$radius;
	        }
        }
        $result = $this->send($urlToCall);
		return $result;
    }
}
?>