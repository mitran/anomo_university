<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Promotionlib {

    var $CI = NULL;

    /**
     * Constructor
     */
    function Promotionlib() {
        $this->CI = & get_instance();
    }

    /**
     * @param $lat
     * @param $lon
     * @param $userInfo
     */
    public function requestAutoPromoMessage($lat, $lon, $userInfo)
    {
        // a.	Get the ID of the promo-location that enclose this coordinate (lat/lon)
        // b.	Get a list of existing message_id belong to this user (so he's not getting pushed the same message again) from the `auto_message_queue` table
        // c.	Then, get a list of message_id which the user satisfies all constraints which is NOT IN the list of message_id retrieved from step b
        // d.	Add the message content to the auto_message_queue, with status = 0 / expiry date = expiry date of the auto-message entry
        $CI = & get_instance();
        $CI->load->model('Promotion_location_model');
        $CI->load->model('Auto_Msg_model');

        $nearbyLocs = $CI->Promotion_location_model->getNearbyLocations($lat, $lon); // a

        if (sizeof($nearbyLocs)) {
            $existingMsgs = $CI->Auto_Msg_model->getUserQueuedMessage($userInfo->UserID); // b
            $existingMsgIds = array();
            if (sizeof($existingMsgs) > 0) {
                foreach ($existingMsgs as $existingMsg)
                    $existingMsgIds[] = $existingMsg->message_id;
            }

            $messages = $CI->Auto_Msg_model->getListByConstraints($userInfo, $existingMsgIds, $nearbyLocs); // c

            $CI->Auto_Msg_model->addMessageToQueue($userInfo->UserID, $messages); // d
        }
    }
}
