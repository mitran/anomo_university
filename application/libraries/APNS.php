<?PHP
#################################################################################
## Developed by Manifest Interactive, LLC                                      ##
## http://www.manifestinteractive.com                                          ##
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ##
##                                                                             ##
## THIS SOFTWARE IS PROVIDED BY MANIFEST INTERACTIVE 'AS IS' AND ANY           ##
## EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE         ##
## IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR          ##
## PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL MANIFEST INTERACTIVE BE          ##
## LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         ##
## CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        ##
## SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR             ##
## BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,       ##
## WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE        ##
## OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,           ##
## EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.                          ##
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ##
## Author of file: Peter Schmalfeldt                                           ##
#################################################################################

/**
 * @category Apple Push Notification Service using PHP & MySQL
 * @package EasyAPNs
 * @author Peter Schmalfeldt <manifestinteractive@gmail.com>
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @link http://code.google.com/p/easyapns/
 */

/**
 * Begin Document
 */

class APNS
{

    /**
     * Connection to MySQL
     *
     * @var string
     * @access private
     */
    private $db;

    /**
     * Array of APNS Connection Settings
     *
     * @var array
     * @access private
     */
    private $apnsData;

    /**
     * Whether to trigger errors
     *
     * @var bool
     * @access private
     */
    private $showErrors = false;

    /**
     * Whether APNS should log errors
     *
     * @var bool
     * @access private
     */
    private $logErrors = true;

    /**
     * Log path for APNS errors
     *
     * @var string
     * @access private
     */
    private $logPath = APN_LOG;

    /**
     * Max files size of log before it is truncated. 1048576 = 1MB.  Added incase you do not add to a log
     * rotator so this script will not accidently make gigs of error logs if there are issues with install
     *
     * @var int
     * @access private
     */
    private $logMaxSize = 1048576; // max log size before it is truncated

    /**
     * Absolute path to your Production Certificate
     *
     * @var string
     * @access private
     */
    private $certificate = APN_PRO;

    /**
     * Apples Production APNS Gateway
     *
     * @var string
     * @access private
     */
    private $ssl = 'ssl://gateway.push.apple.com:2195';

    /**
     * Apples Production APNS Feedback Service
     *
     * @var string
     * @access private
     */
    private $feedback = 'ssl://feedback.push.apple.com:2196';

    /**
     * Absolute path to your Development Certificate
     *
     * @var string
     * @access private
     */
    private $sandboxCertificate = APN_DEV;

    /**
     * Apples Sandbox APNS Gateway
     *
     * @var string
     * @access private
     */
    private $sandboxSsl = 'ssl://gateway.sandbox.push.apple.com:2195';

    /**
     * Apples Sandbox APNS Feedback Service
     *
     * @var string
     * @access private
     */
    private $sandboxFeedback = 'ssl://feedback.sandbox.push.apple.com:2196';

    /**
     * Message to push to user
     *
     * @var string
     * @access private
     */
    private $message;

    private $apnMode = APN_MODE;

    /**
     * Constructor.
     *
     * Initializes a database connection and perfoms any tasks that have been assigned.
     *
     * Create a new PHP file named apns.php on your website...
     *
     *
     * Your iPhone App Delegate.m file will point to a PHP file with this APNS Object.  The url will end up looking something like:
     * https://secure.yourwebsite.com/apns.php?task=register&appname=My%20App&appversion=1.0.1&deviceuid=e018c2e46efe185d6b1107aa942085a59bb865d9&devicetoken=43df9e97b09ef464a6cf7561f9f339cb1b6ba38d8dc946edd79f1596ac1b0f66&devicename=My%20Awesome%20iPhone&devicemodel=iPhone&deviceversion=3.1.2&pushbadge=enabled&pushalert=disabled&pushsound=enabled
     *
     * @param object $db Database Object
     * @param array $args Optional arguments passed through $argv or $_GET
     * @access     public
     */
    function __construct($certificate = NULL, $sandboxCertificate = NULL)
    {

        if (!empty($certificate) && file_exists($certificate)) {
            $this->certificate = $certificate;
        }

        if (!empty($sandboxCertificate) && file_exists($sandboxCertificate)) {
            $this->sandboxCertificate = $sandboxCertificate;
        }

        $this->checkSetup();
        $this->apnsData = array(
            'production' => array(
                'certificate' => $this->certificate,
                'ssl' => $this->ssl,
                'feedback' => $this->feedback
            ),
            'sandbox' => array(
                'certificate' => $this->sandboxCertificate,
                'ssl' => $this->sandboxSsl,
                'feedback' => $this->sandboxFeedback
            )
        );

    }

    /**
     * Check Setup
     *
     * Check to make sure that the certificates are available and also provide a notice if they are not as secure as they could be.
     *
     * @access private
     */
    private function checkSetup()
    {
        if (!file_exists($this->certificate)) $this->_triggerError('Missing Production Certificate.', E_USER_ERROR);
        if (!file_exists($this->sandboxCertificate)) $this->_triggerError('Missing Sandbox Certificate.', E_USER_ERROR);

        clearstatcache();
//		$certificateMod = substr(sprintf('%o', fileperms($this->certificate)), -3);
//		$sandboxCertificateMod = substr(sprintf('%o', fileperms($this->sandboxCertificate)), -3);

//		if($certificateMod>644)  $this->_triggerError('Production Certificate is insecure! Suggest chmod 644.');
//		if($sandboxCertificateMod>644)  $this->_triggerError('Sandbox Certificate is insecure! Suggest chmod 644.');
    }

    /**
     * Push APNS Messages
     *
     * This gets called automatically by _fetchMessages.  This is what actually deliveres the message.
     *
     * @param int $pid
     * @param string $message JSON encoded string
     * @param string $token 64 character unique device token tied to device id
     * @param string $development Which SSL to connect to, Sandbox or Production
     * @access private
     */
    public function _pushMessage($pushData = null, $message, $token = '', $development = 'sandbox')
    {
        $resp = 0;
        if (strlen($message) == 0) $this->_triggerError('Missing message.', E_USER_ERROR);
        if (strlen($token) == 0) $this->_triggerError('Missing message token.', E_USER_ERROR);
        if (strlen($development) == 0) $this->_triggerError('Missing development status.', E_USER_ERROR);

        try {
            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', $this->apnsData[$development]['certificate']);
            $fp = @stream_socket_client($this->apnsData[$development]['ssl'], $error, $errorString, 100, (STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT), $ctx);

            if (!$fp) {
                $this->_triggerError("Failed to connect to APNS: {$error} {$errorString}.");
            }
            else {
                $msg = chr(0) . pack("n", 32) . pack('H*', $token) . pack("n", strlen($message)) . $message;
                $fwrite = fwrite($fp, $msg);
                if (!$fwrite) {
                    $this->_triggerError("Failed writing to stream.", E_USER_ERROR);
                }
                else {
                    $this->_pushSuccess($pushData, $fwrite);
                    $resp = 1;
                }
                fclose($fp);
            }
            $this->_checkFeedback($development);
        } catch (Exception $e) {
            log_message('error', 'Throw Exception: ' . $e->getMessage());
        }

        return $resp;
    }

    /**
     * Hung mod - 23/10/2013
     * Push APNS Messages Queue
     *
     * This gets called automatically by _fetchMessages.  This is what actually deliveres the message.
     *
     * @param int $pid
     * @param string $message JSON encoded string
     * @param string $token 64 character unique device token tied to device id
     * @param string $development Which SSL to connect to, Sandbox or Production
     * @access private
     */
    public function _pushMessageQueue($messages, $development = 'sandbox')
    {
        $resp = 0;
        $development = $this->apnMode;
        if (strlen($development) == 0) $this->_triggerError('Missing development status.', E_USER_ERROR);

        try {
            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', $this->apnsData[$development]['certificate']);
            $fp = @stream_socket_client($this->apnsData[$development]['ssl'], $error, $errorString, 100, (STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT), $ctx);

            if (!$fp) {
                $this->_triggerError("Failed to connect to APNS: {$error} {$errorString}.");
            }
            else {
                foreach ($messages as $message) {
                    $token = $message['token'];
                    $messageBody = $message['body'];
                    $pushData = $message['push_data'];

                    if (strlen($messageBody) == 0) $this->_triggerError('Missing message.', E_USER_ERROR);
                    if (strlen($token) == 0) $this->_triggerError('Missing message token.', E_USER_ERROR);

                    $msg = chr(0) . pack("n", 32) . pack('H*', $token) . pack("n", strlen($messageBody)) . $messageBody;
                    $fwrite = fwrite($fp, $msg);
                    if (!$fwrite) {
                        $this->_triggerError("Failed writing to stream.", E_USER_ERROR);
                    }
//                    else {
//                        $this->_pushSuccess($pushData, $fwrite);
//                    }
                }
                fclose($fp);
                $resp = 1;
            }
            $this->_checkFeedback($development);
        } catch (Exception $e) {
            log_message('error', 'Throw Exception: ' . $e->getMessage());
        }

        return $resp;
    }

    /**
     * Fetch APNS Messages
     *
     * This gets called automatically by _pushMessage.  This will check with APNS for any invalid tokens and disable them from receiving further notifications.
     *
     * @param string $development Which SSL to connect to, Sandbox or Production
     * @access private
     */
    private function _checkFeedback($development)
    {
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $this->apnsData[$development]['certificate']);
        stream_context_set_option($ctx, 'ssl', 'verify_peer', false);
        $fp = @stream_socket_client($this->apnsData[$development]['feedback'], $error, $errorString, 100, (STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT), $ctx);

        if (!$fp) {
            $this->_triggerError("Failed to connect to device: {$error} {$errorString}.");
        } else {
            while ($devcon = fread($fp, 38)) {
                $arr = unpack("H*", $devcon);
                $rawhex = trim(implode("", $arr));
                $token = substr($rawhex, 12, 64);
                if (!empty($token)) {
                    $this->_triggerError("Unregistering Device Token: {$token}.");
                }
            }
            fclose($fp);
        }
    }

    /**
     * APNS Push Success
     *
     * This gets called automatically by _pushMessage.  When no errors are present, then the message was delivered.
     *
     * @param int $pid Primary ID of message that was delivered
     * @access private
     */
    private function _pushSuccess($pushData, $fwrite)
    {
        if ($pushData == null) return;
        $pushData['Status'] = 1;
        $pushData['Result'] = $fwrite;
        $CI = & get_instance();
        $CI->load->model('Log_Push_Notification_model');
        $CI->Log_Push_Notification_model->add($pushData);
    }

    /**
     * Trigger Error
     *
     * Use PHP error handling to trigger User Errors or Notices.  If logging is enabled, errors will be written to the log as well.
     * Disable on screen errors by setting showErrors to false;
     *
     * @param string $error Error String
     * @param int $type Type of Error to Trigger
     * @access private
     */
    private function _triggerError($error, $type = E_USER_NOTICE)
    {
        $backtrace = debug_backtrace();
        $backtrace = array_reverse($backtrace);
        $error .= "\n";
        $i = 1;
        foreach ($backtrace as $errorcode) {
            $file = ''; //($errorcode['file']!='') ? "-> File: ".basename($errorcode['file'])." (line ".$errorcode['line'].")":"";
//			$error .= "\n\t".$i.") ".$errorcode['class']."::".$errorcode['function']." {$file}";
            $error .= "\n\t" . $i . ") ::"; //.$errorcode['function']." {$file}";
            $i++;
        }
        $error .= "\n\n";
        if ($this->logErrors && file_exists($this->logPath)) {
            if (filesize($this->logPath) > $this->logMaxSize) $fh = fopen($this->logPath, 'w');
            else $fh = fopen($this->logPath, 'a');
            fwrite($fh, $error);
            fclose($fh);
        }
        if ($this->showErrors) trigger_error($error, $type);
    }

    /**
     * JSON Encode
     *
     * Some servers do not have json_encode, so use this instead.
     *
     * @param array $array Data to convert to JSON string.
     * @access private
     * @return string
     */
    private function _jsonEncode($array = false)
    {
        //Using json_encode if exists
        if (function_exists('json_encode')) {
            return json_encode($array);
        }
        if (is_null($array)) return 'null';
        if ($array === false) return 'false';
        if ($array === true) return 'true';
        if (is_scalar($array)) {
            if (is_float($array)) {
                return floatval(str_replace(",", ".", strval($array)));
            }
            if (is_string($array)) {
                static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
                return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $array) . '"';
            }
            else return $array;
        }
        $isList = true;
        for ($i = 0, reset($array); $i < count($array); $i++, next($array)) {
            if (key($array) !== $i) {
                $isList = false;
                break;
            }
        }
        $result = array();
        if ($isList) {
            foreach ($array as $v) $result[] = $this->_jsonEncode($v);
            return '[' . join(',', $result) . ']';
        }
        else {
            foreach ($array as $k => $v) $result[] = $this->_jsonEncode($k) . ':' . $this->_jsonEncode($v);
            return '{' . join(',', $result) . '}';
        }
    }
}

?>