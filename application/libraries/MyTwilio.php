<?php
require_once(APPPATH . 'libraries/Twilio/Twilio.php');
class MyTwilio{
    public $twilio = false;
    function __construct() {
        $this->twilio = new Services_Twilio(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);
    }
    
    function send($to_number, $message = '') {
        $status = 0;
        try {
            $send_result = $this->twilio->account->messages->create(
                    array(
                        "From" => TWILIO_FROM_NUMBER,
                        "To" => $to_number,
                        "Body" => $message
                    )
            );
            if ($send_result){
                if (isset($send_result->status)){
                    $status = 1;
                    if ($send_result->status == 'undelivered' || $send_result->status == 'failed'){
                        $status = 0;
                    }
                }
                
            }
        } catch (Services_Twilio_RestException $e) {
            $status = 0;            
        }
        return $status;
    }

}
?>
