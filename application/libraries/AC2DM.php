<?php
class AC2DM
{
    private $source = AC2DM_SOURCE;
    private $authCode;
    private $username = AC2DM_EMAIL;
    private $password = AC2DM_PASSWORD;

    public function __construct()
    {
    }

    public function setSource($s)
    {
        $this->source = $s;
    }

    public function setUsername($u)
    {
        $this->username = $u;
    }

    public function setPassword($p)
    {
        $this->password = $p;
    }

    public function setAuthCode($a)
    {
        $this->authCode = $a;
    }

    public function getAuthCode()
    {
        if ($this->authCode) {
            return $this->authCode;
        } else {
            return $this->googleAuthenticate();
        }
    }

    /**
     * Get the ClientLogin token for ac2dm. Techinically, you can change the service
     * and get a token for whatever you please
     */
    public function googleAuthenticate($service = "ac2dm")
    {
        if (empty($this->username) || empty($this->password) || empty($this->source)) {
            throw new Exception("Username, password and source must all be set to get auth code");
        }

        $ch = curl_init();
        if (!$ch) {
            throw new Exception("Curl must be enabled");
        }

        curl_setopt($ch, CURLOPT_URL, "https://www.google.com/accounts/ClientLogin");
        $post_fields = "accountType=" . urlencode('HOSTED_OR_GOOGLE') . "&Email=" . urlencode($this->username) . "&Passwd=" . urlencode($this->password) . "&source=" . urlencode($this->source) . "&service=" . urlencode($service);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);

        curl_close($ch);

        if (strpos($response, '200 OK') === false) {
            throw new InvalidResponseException($response);
        }
        // find the auth code
        preg_match("/(Auth=)([\w|-]+)/", $response, $matches);

        if (!$matches[2]) {
            throw new Exception("Can't parse the auth code from the response");
        }

        $this->authCode = $matches[2];
        return $matches[2];
    }

    /**
     * Using C2DM
     * send msg to registrationID
     *
     * @param unknown_type $deviceRegistrationId
     * @param unknown_type $msgType
     * @param unknown_type $messageText
     * @return
     *      201 QuotaExceeded — Too many messages sent by the sender. Retry after a while.
     *      202 DeviceQuotaExceeded — Too many messages sent by the sender to a specific device. Retry after a while.
     *      203 InvalidRegistration — Missing or bad registration_id. Sender should stop sending messages to this device.
     *      203 NotRegistered — The registration_id is no longer valid, for example user has uninstalled the application or turned off notifications. Sender should stop sending messages to this device.
     *      205 MessageTooBig — The payload of the message is too big, see the limitations. Reduce the size of the message.
     *      206 MissingCollapseKey — Collapse key is required. Include collapse key in the request.
     *      401 Indicates that the ClientLogin AUTH_TOKEN used to validate the sender is invalid.
     *      503      Indicates that the server is temporarily unavailable (i.e., because of timeouts, etc ). Sender must retry later, honoring any Retry-After header included in the response. Application servers must implement exponential back off. Senders that create problems risk being blacklisted.

     */
    public function send_c2dm($deviceRegistrationId, $msgType, $messageText, $logData = null)
    {
        $auth = $this->getAuthCode();
        $resp = 0;

        $headers = array('Authorization: GoogleLogin auth=' . $auth);
        $data = array(
            'sender_id' => AC2DM_EMAIL,
            'registration_id' => $deviceRegistrationId,
            'collapse_key' => $msgType,
            'data.message' => $messageText);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://android.apis.google.com/c2dm/send");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response_data = curl_exec($ch);
        $response_info = curl_getinfo($ch);

        curl_close($ch);

        if ($response_info['http_code'] == 200) {
            // Filter message-id or error from response
            if (preg_match("/id=([a-z0-9_%:\\-]+)/i", $response_data, $matches) == 1) {
                if ($logData != null) {
                    $logData['Status'] = 1;
                    $CI =& get_instance();
                    $CI->load->model('Log_Push_Notification_model');
                    $CI->Log_Push_Notification_model->add($logData);
                }
                $resp = 1;
            } else if (preg_match("/Error=([a-z0-9_\\-]+)/i", $response_data, $matches) == 1) {
                $resp = 201;
            }
        } else if ($response_info['http_code'] == 401) {
            $resp = 401;
        } else if ($response_info['http_code'] == 503) {
            $resp = 503;
        }

        return $resp;
    }
    
    
    
    /**
     * Sending Push Notification
     * Using GCM
     */
    public function send($registatoin_ids, $msgType=null, $message, $logData = null) {
        $iResponse = 0;
        if (!is_array($registatoin_ids)){
            $registatoin_ids = array($registatoin_ids);
        }
        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';

        $fields = array(
            'registration_ids' => $registatoin_ids,
             'data' => array('message' =>$message)
        );

        $headers = array(
            'Authorization: key=' . GCM_GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        //application/x-www-form-urlencoded;charset=UTF-8
        //application/json
        
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        
        // Execute post
        $result = curl_exec($ch);
        
         // Close connection
        curl_close($ch);
        if ($result === FALSE) {
            $iResponse = 0;
        }else{
            $oResult = json_decode($result);
            if($oResult && isset($oResult->success) && $oResult->success == 1){
                if ($logData != null) {
                    $logData['Status'] = 1;
                    $CI =& get_instance();
                    $CI->load->model('Log_Push_Notification_model');
                    $CI->Log_Push_Notification_model->add($logData);
                }
                $iResponse = 1;
            }
        }
        
//        echo $result;
        return $iResponse;
    }
}