<?php
interface ILocationServiceProvider {
	public function searchByKeywordAndLocation($keyword, $lat, $lon, $page);
	
	public function searchByLocation($lat, $lon, $page);
	
	public function getDetail($id);
}
?>