<?php
class Ext_Memcache {
	private $conf = array(
		'host' 		=> CACHE_HOST,
		'port'		=> CACHE_PORT,
		'weight'	=> CACHE_WEIGHT,
	);
	private $memcache;
	
	function __construct(){
		if (class_exists('Memcache')){
                        $memcache_enable = CACHE_ENABLED;
                        if ($memcache_enable == 1){
                            $this->memcache = new Memcache;
                            $this->memcache->connect($this->conf['host'], $this->conf['port']) or die ("Could not connect to cache server!");
                        }
		}
	}
	
	function set($key, $data){
		return $this->memcache->set($key, $data);
	}
	
	function get($key){
		return $this->memcache->get($key);
	}
	
	function delete($key){
		return $this->memcache->delete($key);
	}
	
	function clean(){
		return $this->memcache->flush();
	}
	
	function cache_info(){
		return $this->memcache->getStats();
	}
	
	function is_supported(){
		if ( ! extension_loaded('memcache')){
			log_message('error', 'The Memcache Extension must be loaded to use Memcache Cache.');
			return FALSE;
		}
		return TRUE;
		
	}
}
?>