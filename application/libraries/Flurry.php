<?php

class Flurry {

    private $apiUrl = "http://api.flurry.com/appMetrics/";
    private $apiAccessCode = FLURRY_API_ACCESS_CODE;
    private $apiKeyAndroid = FLURRY_API_KEY_ANDROID;
    private $apiKeyIos = FLURRY_API_KEY_IOS;

    public function getTotalFlurryActiveUser($device = 'ios', $type = "day", $from = '', $to = '', $return = 'int', $metric_name = 'ActiveUsers', $group_by = '') {
        $arrRes = '';
        $apiKey = $this->apiKeyIos;
        if (isset($device) && 'android' == $device) {
            $apiKey = $this->apiKeyAndroid;
        }
        $from = !empty($from) ? $from : date('Y-m-d');
        $to = !empty($to) ? $to : date('Y-m-d');
        if ('month' == $type) {
            $to = date("Y-m-t", strtotime("first day of previous month"));
            $from = date("Y-m-1", strtotime("first day of previous month"));
            // $from = date("Y-m-d", strtotime("-1 month"));
        }
        if ('week' == $type) {
            // $to = date("Y-m-d", strtotime("+6 days", strtotime("last week")));
            // $from = date("Y-m-d", strtotime("last week"));
            $from = date("Y-m-d", strtotime("-1 week"));
        }
            $params = array(
                'apiAccessCode' => $this->apiAccessCode,
                'apiKey' => $apiKey,
                'startDate' => $from,
                'endDate' => $to
            );
            if ($group_by != '') {
                $params['groupBy'] = $group_by;
            }
            $urlToCall = $this->apiUrl . $metric_name . '?' . http_build_query($params, null, '&');
            $arrRes = $this->send($urlToCall);
            if ('array' == $return) {
                $aData = array();
                if ('' != $arrRes && isset($arrRes['day']) && 0 < count($arrRes['day'])) {
                    if (isset($arrRes['day']['@value'])) {
                        $aData[$arrRes['day']['@date']] = $arrRes['day']["@value"];
                    } else {
                        foreach ($arrRes['day'] as $v) {
                            $aData[$v['@date']] = $v["@value"];
                        }
                    }
                }
                return $aData;
            } else {
                $itotal = 0;
                if ('' != $arrRes && isset($arrRes['day']) && 0 < count($arrRes['day'])) {
                    if (isset($arrRes['day']['@value'])) {
                        $itotal = $itotal + $arrRes['day']["@value"];
                    } else {
                        foreach ($arrRes['day'] as $v) {
                            $itotal = $itotal + $v["@value"];
                        }
                    }
                }
                return $itotal;
            }
        }

        function send($urlToCall) {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $urlToCall);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_FAILONERROR, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

            $response = curl_exec($curl);
            if (!$response) {
                $response = curl_error($curl);
            }
            curl_close($curl);
            return json_decode($response, true);
        }

    }

?>