<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Commonlib {

    var $CI = NULL;

    /**
     * Constructor
     */
    function Commonlib() {

        $this->CI = & get_instance();
    }

    function gen_uuid($len = 20) {
        $hex = md5("some_salt_please" . uniqid("", true));
        $pack = pack('H*', $hex);
        $uid = base64_encode($pack);
        $uid = @ereg_replace("[^A-Z0-9]", "", strtoupper($uid));
        if ($len < 4)
            $len = 4;
        if ($len > 128)
            $len = 128;
        while (strlen($uid) < $len)
            $uid = $uid . self::gen_uuid(22);
        return substr($uid, 0, $len);
    }

    function checkToken($token) {
        $CI = & get_instance();
        $CI->load->model('Wstoken_model', 'Wstoken_model');
        $CI->load->model('Config_model');
        $info = $CI->Wstoken_model->getToken($token);
        if ($info) {
            return true;
        } else {
            return false;
        }
    }

    //$this is new functino for check token, will return userID in case of valid token, otherwise release -1 
    function checkValidToken($token) {
        $CI = & get_instance();
        $CI->load->model('Wstoken_model', 'Wstoken_model');
        $CI->load->model('Config_model');
        $info = $CI->Wstoken_model->getToken($token);
        //	var_dump($info);
        if ($info) {
            return $info->UserID;
        } else {
            return -1;
        }
    }

    function writeToFile($myFile, $stringData) {
        $result = true;
        $fh = fopen($myFile, 'w') or die("can't open file");
        if (!fwrite($fh, $stringData))
            $result = false;
        fclose($fh);
        return $result;
    }

    /*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /* ::                                                                         : */
    /* ::  This routine calculates the distance between two points (given the     : */
    /* ::  latitude/longitude of those points). It is being used to calculate     : */
    /* ::  the distance between two locations using GeoDataSource(TM) Products    : */
    /* ::                     													 : */
    /* ::  Definitions:                                                           : */
    /* ::    South latitudes are negative, east longitudes are positive           : */
    /* ::                                                                         : */
    /* ::  Passed to function:                                                    : */
    /* ::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  : */
    /* ::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  : */
    /* ::    unit = the unit you desire for results                               : */
    /* ::           where: 'M' is statute miles                                   : */
    /* ::                  'K' is kilometers (default)                            : */
    /* ::                  'N' is nautical miles                                  : */
    /* ::  Worldwide cities and other features databases with latitude longitude  : */
    /* ::  are available at http://www.geodatasource.com                          : */
    /* ::                                                                         : */
    /* ::  For enquiries, please contact sales@geodatasource.com                  : */
    /* ::                                                                         : */
    /* ::  Official Web site: http://www.geodatasource.com                        : */
    /* ::                                                                         : */
    /* ::         GeoDataSource.com (C) All Rights Reserved 2014		  : */
    /* ::                                                                         : */
    /* :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        $lat1 = floatval($lat1);
        $lon1 = floatval($lon1);
        $lat2 = floatval($lat2);
        $lon2 = floatval($lon2);
        
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
    
    function isVegas($lat = '', $lon = ''){
        $isVegas = 0;
        if (trim($lat) != '' && trim($lon) != ''){
            $CI = & get_instance();
            $CI->load->model('Config_model');
            $vegasLat = $CI->Config_model->getConfig('VEGAS_LAT');
            $vegasLon = $CI->Config_model->getConfig('VEGAS_LON');
            $radius = $CI->Config_model->getConfig('VEGAS_RADIUS'); // miles

            if ($vegasLat && $vegasLon && $radius){
                $miles = self::distance($lat, $lon, $vegasLat, $vegasLon, 'M');
                if ($miles < $radius){
                    $isVegas = 1;
                }
            }
        }
        return $isVegas;
    } 

    /**
     * function encrypt password
     *
     * @param string $text
     * @return string
     */
    function encrypt($text) {
        return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, SALT, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
    }

    /**
     * function decrypt password
     *
     * @param string $text
     * @return string
     */
    function decrypt($text) {
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SALT, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
    }

    /**
     * function change field null to empty
     *
     * @param string $object
     * @return string
     */
    function changeNullToEmpty($object) {
        foreach ($object as $key => $value) {
            if ($value === null) {
                $object[$key] = '';
            }
        }
        return $object;
    }

    /**
     * function log
     */
    function aLog($logTitle, $logData) {
        $logMsg = var_export($logData, TRUE);
        log_message('error', $logTitle . ' - ' . $logMsg . ' - ' . date('l jS \of F Y h:i:s A'));
    }
    
    public  function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }
 
    public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
 
    public  function encode($value){ 
        if(!$value){return false;}
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, SALT, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext)); 
    }
 
    public function decode($value){
        if(!$value){return false;}
        $crypttext = $this->safe_b64decode($value); 
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SALT, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }
    
    function myLog($logTitle, $logData) {
        if (LOG_ENABLED == 0) {
            return false;
        }
        
        $config = & get_config();
        $log_path = ($config['log_path'] != '') ? $config['log_path'] : BASEPATH . 'logs/';
        if (!is_dir($log_path) OR !is_really_writable($log_path)) {
            return false;
        }
        
        $filepath = $log_path . 'ws-log-' . date('Y-m-d') . EXT; 
        if (!$fp = @fopen($filepath, FOPEN_WRITE_CREATE)) {
            return FALSE;
        }
        
        $message = '';
        $logMsg = var_export($logData, TRUE);
        $message .=  'WSLOG '.date('Y-m-d H:i:s'). ' --> ' . $logTitle. ' --> ' . $logMsg . "\n";

        flock($fp, LOCK_EX);
        fwrite($fp, $message);
        flock($fp, LOCK_UN);
        fclose($fp);

        @chmod($filepath, FILE_WRITE_MODE);
        return TRUE;
    }

    function dynaLog($logFileName, $logTitle, $logData) {
        if (LOG_ENABLED == 0) {
            return false;
        }

        $config = & get_config();
        $log_path = ($config['log_path'] != '') ? $config['log_path'] : BASEPATH . 'logs/';
        if (!is_dir($log_path) OR !is_really_writable($log_path)) {
            return false;
        }

        $filepath = $log_path . 'ws-log-' . $logFileName . '-' . date('Y-m-d') . EXT;
        if (!$fp = @fopen($filepath, FOPEN_WRITE_CREATE)) {
            return FALSE;
        }

        $message = '';
        $logMsg = var_export($logData, TRUE);
        $message .=  'WSLOG '.date('Y-m-d H:i:s'). ' --> ' . $logTitle. ' --> ' . $logMsg . "\n";

        flock($fp, LOCK_EX);
        fwrite($fp, $message);
        flock($fp, LOCK_UN);
        fclose($fp);

        @chmod($filepath, FILE_WRITE_MODE);
        return TRUE;
    }
    
    function formatBigNumber($number) {
        return number_format($number, 2, '.', ',');
    }
    
    /**return start / end date of a week
     * 
     * @param type $date
     * @return type
     */
    function x_week_range($date = '') {
        if ($date == '')
            $date = date('Y-m-d');

        $ts = strtotime($date);
        $d = date('w',strtotime($date));
        if($d != 0){
            $start = strtotime('sunday this week -1 week', $ts);
            $end = strtotime('sunday this week', $ts);
        }
        else{
            $start = strtotime('monday last week -1 week', $ts);
            $end = strtotime('monday last week', $ts);
        }
        return array(date('Y-m-d', $start), date('Y-m-d', $end));
    }
    
    /**return youtube id from string
     * 
     * @param type $string
     * @return boolean
     */
    function getVideoIDFromString($string = ''){
        if (empty($string)){
            return false;
        }
        $matches = array();
        // First implement just detect youtube link
        preg_match("/(?:http(?:s)?:\/\/)?(?:www\.|m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'\s>]{11})/i"
            , $string, $matches);
        if (sizeof($matches) > 0){
            if (isset($matches[1])){
                $video_id = $matches[1];
                $video_source = 'youtube';
                // check this video is valid
                $is_valid = $this->isValidVideo($video_id, $video_source);
                if ($is_valid){
                     return array('VideoID' => $video_id
                             , 'VideoSource' => $video_source
                             , 'VideoLink' => $matches[0]
                             , 'VideoThumbnail' => ''
                             );
                }
            }
        }
        
        // detect vine video
        $matches = array();
        preg_match("/(?:http(?:s)?:\/\/)?(?:www\.)?vine\.co\/v\/([^\?&\"'\s>]{11})/i"
                    , $string, $matches);
        if (sizeof($matches) > 0){
            if (isset($matches[1])){
                $video_id = $matches[1];
                $video_source = 'vine';
                // check this video is valid
                $is_valid = $this->isValidVideo($video_id, $video_source);
                if ($is_valid){
                     return array(
                         'VideoID' => $video_id
                        , 'VideoSource' => $video_source
                        , 'VideoLink' => $matches[0]
                        , 'VideoThumbnail' => $is_valid
                        );
                }
            }
        }
        return false;
    }
    
    private function isValidVideo($video_id, $video_source = 'youtube'){
        // first implement
        // only check youtube video
        if (empty($video_id)){
            return false;
        }
        if ($video_source == 'youtube'){
            $apiUrl = 'https://www.googleapis.com/youtube/v3/videos?id='.$video_id.'&key='.GCM_GOOGLE_API_KEY.'&part=status';
            $data = @file_get_contents($apiUrl);
            if ($data){
                if ($oData = json_decode($data)){
                    if ($pageInfo = $oData->pageInfo){
                        if ($totalResults = $pageInfo->totalResults){
                            if ($totalResults > 0){
                                return true;
                            }
                        }
                    }
                } 
            }
        }elseif ($video_source == 'vine'){
            // ruturn vine video thumbnail if it is valid else return false
            $matches = array();
            $vine = @file_get_contents(VINE_VIDEO_URL."$video_id");
            preg_match('/property="og:image" content="(.*?)"/', $vine, $matches);
            if (isset($matches[1])){
                $thumb = explode('?', $matches[1]);
                if ($thumb){
                    return isset($thumb[0])?$thumb[0]:false;
                }
            }
        }
        return false;
    }
    
    function validateEmail($email = ''){
        $email = trim($email);
        if (empty($email)) return false;
        $email = strtolower($email);
        $strdomain = UW_DOMAIN; // u.washing.edu        
        $arrDomain = explode(',',$strdomain);        
        foreach ($arrDomain as $domain){            
            $matches = array();
            preg_match("/^[a-zA-Z0-9_.+-]+@$domain$/", $email, $matches);
            if (sizeof($matches) > 0){
                return true;
            }
        }
        return false;
    }

}

?>
