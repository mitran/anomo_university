<?php
class MyPushNotify{
    function __construct() {
        
    }
    /**
     * Type:
     *  1 :Msg
        2 :Gift
        3 :Anomotion
        4 :Request Reveal
     *  15 :Accept Reveal
        5 :Comment Picture Post
        6 :Comment Text Post
        7 :Comment Activity
        8 :Like Activity
        9 :Follow
        10 :Invite game
        11 :Finished game
        12 :Complete game
        13 :mention user (@name)
        14 :like on comment
     * @param type $receiver_id
     * @param type $send_user_id
     * @param type $type
     * @param type $msg
     * @param type $option
     */
    function send($receiver_id, $send_user_id, $type, $msg, $options = array()) {
        $CI = & get_instance();
        $CI->load->model('User_model');
        $CI->load->model('Config_model');
        $CI->load->model('Chat_Count_model');
        $CI->load->library('AC2DM');
        $CI->load->library('APNS');
        $pushStatus = 0;
        $receiverInfo = $CI->User_model->getPushNotificationReceiverInfo($receiver_id);
        if ($receiverInfo && $receiverInfo->DeviceID) {
            $content_id = isset($options['ContentID']) ? $options['ContentID'] : -1;
            $content_type = isset($options['ContentType']) ? $options['ContentType'] : -1;
            $anomotion_id = isset($options['AnomotionID']) ? $options['AnomotionID'] : -1;
            $history_id = isset($options['HistoryID']) ? $options['HistoryID'] : -1;
            $reveal_type = isset($options['RevealType']) ? $options['RevealType'] : -1;
            $reveal_picture_id = isset($options['RevealPictureID']) ? $options['RevealPictureID'] : -1;
            $reveal_content = '';
            $is_anonymous = isset($options['IsAnonymous']) ? $options['IsAnonymous'] : 0;
            $is_anonymous_comment = isset($options['IsAnonymousComment']) ? $options['IsAnonymousComment'] : 0;

            $senderInfo = $CI->User_model->getUserInfo($send_user_id);
            $facebookID = $senderInfo && $senderInfo->FacebookID != ''?$senderInfo->FacebookID:'';
            $senderAvatar = '';
            if (isset($senderInfo->Avatar) && !empty($senderInfo->Avatar)) {
                $senderAvatar = $CI->config->item('s3_photo_url') . $senderInfo->Avatar;
            }
            $senderName = $senderInfo ? $senderInfo->UserName : '';

            if ($type == NOTIFY_MENTION_USER) { // 13 - mention user
                $onwer = $is_anonymous != 1?$senderInfo->UserName:'Anonymous';
                $msg = str_replace('%username%', $onwer, $msg);
                
                // Hung's temp hotfix for anonymous posts:
                if ($is_anonymous == 1) {
                    $senderName = 'Anonymous';
                    $senderAvatar = $CI->config->item('s3_photo_url').'avatar_anonymous.png';
                }
            }
            
            if ($type == NOTIFY_AGELESS_POST){// 19 -Notification to ALL FOLLOWERS of the ageless user
                $msg = str_replace('%username%', $senderName, $msg);
            }

            if ($type == NOTIFY_REQUEST_REVEAL) { // 4- request reveal
                $msg = str_replace('%username%', $senderInfo->UserName, $msg);
            }

            if ($type == NOTIFY_ACCEPT_REVEAL) { //15 - accept reveal
                $msgReveals = array(
                    0 => '%username% just revealed a picture to you',
                    1 => '%username% just revealed their Facebook account to you',
                    2 => '%username% just revealed their real name to you',
                    3 => '%username% just revealed their school to you',
                    4 => '%username% just revealed some info about their looks to you',
                    5 => '%username% just revealed how they party',
                    6 => '%username% just revealed some info about their personality to you',
                    7 => '%username% just revealed their profession to you'
                );
                $msg = $msgReveals[$reveal_type];
                $msg = str_replace('%username%', isset($senderInfo->UserName) ? $senderInfo->UserName : '', $msg);

                // reveal content
                $reveal_field = array(
                    1 => 'FbEmail',
                    2 => 'RealName',
                    3 => 'College',
                    7 => 'Major'
                );
                $field = isset($reveal_field[$reveal_type]) ? $reveal_field[$reveal_type] : "";
                if (!empty($field)) {
                    $reveal_content = isset($senderInfo->$field) ? $senderInfo->$field : "";
                }
            }
            
            if ($type == NOTIFY_COMMENT_ACTIVITY){ // 7
                if($is_anonymous_comment == 1){
                    $senderAvatar = $CI->config->item('s3_photo_url').'avatar_anonymous.png';
                    $senderName = 'Anonymous';
                }
            }

            if (APN_MSG_LENGTH < strlen($msg)) {
                $msg = substr($msg, 0, APN_MSG_LENGTH) . '...';
            }
            
            // notify daily match
            if ($type == NOTIFY_NEW_MATCH){

                $senderName = NAME_UNIVERSITY;

                $senderAvatar = base_url().'public/icons/anomo.png';
            }

            /**
             * t: type
             * f: sender id
             * u: receiver id
             * n: sender name
             * p: sender avatar
             * c: content type
             * i: content id
             * a: anomotion id
             * h: history id
             * r: reveal type
             * e: reveal content
             * v: reveal picture id
             * s: is anonymous
             * k: facebook id
             */
            $total = $receiverInfo->Number;
            // need include #chat count
            $chatCount = $receiverInfo->ChatCount;
            $total += $chatCount;

            $logData['UserID'] = $receiver_id;
            $logData['CreatedDate'] = gmdate('Y-m-d H:i:s');
            // for ios
            if ($receiverInfo->Type == 'ios') {
                $sound = 'abc.aiff';
                $apnMsg['aps']['alert'] = $msg;
                $apnMsg['aps']['badge'] = (int) $total;
                $apnMsg['aps']['sound'] = (string) $sound;
                $apnMsg['t'] = $type;
                $apnMsg['f'] = $send_user_id;
                $apnMsg['u'] = $receiver_id;
                $apnMsg['n'] = $senderName;
//                $apnMsg['p'] = $senderAvatar;
                $apnMsg['c'] = $content_type;
                $apnMsg['i'] = $content_id;
                $apnMsg['a'] = $anomotion_id;
                $apnMsg['h'] = $history_id;
                $apnMsg['r'] = $reveal_type;
                $apnMsg['e'] = $reveal_content;
                $apnMsg['v'] = $reveal_picture_id;
                $apnMsg['k'] = $facebookID;

                $msgToPush = json_encode($apnMsg);
                $logData['Platform'] = 'ios';
                $logData['Message'] = $msgToPush;
                $pushStatus = $CI->apns->_pushMessage($logData, $msgToPush, $receiverInfo->DeviceID, $CI->Config_model->getConfig('PUSH_NOTIFICATION_MODE'));
            }

            // for android
            if ($receiverInfo->Type == 'android') {
                $json['Type'] = $type;
                $json['SenderName'] = $senderName;
                $json['Message'] = $msg;
                $json['NumberBagde'] = $total;
                $json['SenderAvatar'] = isset($senderAvatar) ? $senderAvatar : '';
                $json['SenderID'] = $send_user_id;
                $json['ReceiverID'] = $receiver_id;
                $json['ContentType'] = $content_type;
                $json['ContentID'] = $content_id;
                $json['AnomotionID'] = $anomotion_id;
                $json['HistoryID'] = $history_id;
                $json['RevealType'] = $reveal_type;
                $json['RevealContent'] = $reveal_content;
                $json['RevealPictureID'] = $reveal_picture_id;
                $json['FacebookID'] = $facebookID;

                $c2dmMsg = json_encode($json);
                $logData['Platform'] = 'android';
                $logData['Message'] = $c2dmMsg;
                $pushStatus = $CI->ac2dm->send($receiverInfo->DeviceID, 'text', $c2dmMsg, $logData);
            }
        }

        return $pushStatus;
    }
}
?>
