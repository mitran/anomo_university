<?php

class VerifyingAppReceipts {

    function verify($receipt) {
        // determine which endpoint to use for verifying the receipt
        $CI = & get_instance();
        $CI->load->model('Config_model');
        $mode = $CI->Config_model->getConfig('PUSH_NOTIFICATION_MODE');
        if ($mode == 'sandbox') {
            $endpoint = 'https://sandbox.itunes.apple.com/verifyReceipt';
        } else {
            $endpoint = 'https://buy.itunes.apple.com/verifyReceipt';
        }

        // build the post data
        $postData = json_encode(
                array('receipt-data' => $receipt)
        );

        // create the cURL request
        $ch = curl_init($endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // execute the cURL request and fetch response data
        $response = curl_exec($ch);
        $errno = curl_errno($ch);
        $errmsg = curl_error($ch);
        curl_close($ch);

        // ensure the request succeeded
        if ($errno != 0) {
            //throw new Exception($errmsg, $errno);
            return false;
        }

        // parse the response data
        $data = json_decode($response);
        $CI->commonlib->aLog('verify_receipt_ios:' . $receipt, $response);

        // ensure the expected data is present
        if (isset($data->status) && $data->status == 0) {
            return true;
        } else {
            return false;
        }
    }

    function verify_android_market($signed_data, $signature) {
        $public_key_base64 = ANDROID_PUBLIC_KEY;
        $key = "-----BEGIN PUBLIC KEY-----\n" .
                chunk_split($public_key_base64, 64, "\n") .
                '-----END PUBLIC KEY-----';
        //using PHP to create an RSA key
        $key = openssl_get_publickey($key);
        //$signature should be in binary format, but it comes as BASE64.
        //So, I'll convert it.
        $signature = base64_decode($signature);
        //using PHP's native support to verify the signature
        $result = openssl_verify($signed_data, $signature, $key, OPENSSL_ALGO_SHA1);
        if (0 === $result) {
            return false;
        } else if (1 !== $result) {
            return false;
        } else {
            return true;
        }
    }

}

?>