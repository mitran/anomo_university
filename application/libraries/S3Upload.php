<?php

require_once 'S3.php';

class S3Upload {

    private $accessKey = ACCESS_KEY;
    private $secretKey = SECRET_KEY;
    private $endPoint = END_POINT;
    private $bucketName = BUCKET_NAME;
    private $allowed_image_types = array('png', 'gif', 'jpg', 'jpeg');
    private $allowed_video_types = array('mp4');
    private $max_image_size = PHOTO_SIZE_MAX;
    private $max_video_size = VIDEO_SIZE_MAX;
    public $error = '';

    /**
     * upload a file to S3
     *
     * @param string $field_name
     * @return file_name if success 
     * 		   FALSE if failed
     * 
     * @modified: mode parameter for custom destination folder
     */
    function upload($field_name, $file_name = '', $folder = BUCKET_NAME_UPLOAD, $type = 'photo') {
        if (!$this->_validate($field_name, $type)) {
            return false;
        } else {
            if ($file_name == '') {
                $file_name = $this->_set_new_file_name($_FILES[$field_name]['name']);
                if ($file_name == '') {
                    return false;
                }
            }
            return $this->_uploadS3($_FILES[$field_name]['tmp_name'], $file_name, $_FILES[$field_name]['type'], $folder);
        }
    }


    function upload_multi($field_name) {
        if (!isset($_FILES[$field_name]['name'][0])) {
            $this->error = 'You did not select a file to upload.';
            return FALSE;
        } else {
            $num_files = count($_FILES[$field_name]['name']);
            $file_list = array();
            for ($i = 0; $i < $num_files; $i++) {
                $file_name = $this->_set_new_file_name($_FILES[$field_name]['name'][$i]);
                if ($file_name && isset($_FILES[$field_name]['tmp_name'][$i]) && $_FILES[$field_name]['size'][$i] > 0) {
                    $isUpload = $this->_uploadS3($_FILES[$field_name]['tmp_name'][$i], $file_name, $_FILES[$field_name]['type'][$i]);
                    if ($isUpload) {
                        $file_list[] = $isUpload;
                    }
                }
            }
            return count($file_list) > 0 ? $file_list : false;
        }
    }

    function _set_new_file_name($file_name) {
        $new_file_name = '';
        $x = explode('.', $file_name);
        $ext = end($x);
        mt_srand();
        $name = md5(uniqid(mt_rand()));
        $new_file_name = $name . '.' . $ext;
        $s3 = new S3($this->accessKey, $this->secretKey, false, $this->endPoint);
        if ($s3->getObjectInfo($this->bucketName, $new_file_name)) {
            for ($i = 1; $i < 100; $i++) {
                if ($s3->getObjectInfo($this->bucketName, $name . $i . $ext)) {
                    $new_file_name = $name . $i . '.' . $ext;
                    break;
                }
            }
        }
        return $new_file_name;
    }

    /**
     * validate file to upload 
     *
     * @param string $field_name
     * @return bool
     */
    private function _validate($field_name, $type) {
        if (!isset($_FILES[$field_name])) {
            $this->error = 'You did not select a file to upload.';
            return FALSE;
        }

        if (!is_uploaded_file($_FILES[$field_name]['tmp_name'])) {
            $error = (!isset($_FILES[$field_name]['error'])) ? 4 : $_FILES[$field_name]['error'];

            switch ($error) {
                case 1: // UPLOAD_ERR_INI_SIZE
                    $this->error = 'The uploaded file exceeds the maximum allowed size in your PHP configuration file.';
                    break;
                case 2: // UPLOAD_ERR_FORM_SIZE
                    $this->error = 'The uploaded file exceeds the maximum size allowed by the submission form.';
                    break;
                case 3: // UPLOAD_ERR_PARTIAL
                    $this->error = 'The file was only partially uploaded.';
                    break;
                case 4: // UPLOAD_ERR_NO_FILE
                    $this->error = 'You did not select a file to upload.';
                    break;
                case 6: // UPLOAD_ERR_NO_TMP_DIR
                    $this->error = 'The temporary folder is missing.';
                    break;
                case 7: // UPLOAD_ERR_CANT_WRITE
                    $this->error = 'The file could not be written to disk.';
                    break;
                case 8: // UPLOAD_ERR_EXTENSION
                    $this->error = 'The file upload was stopped by extension.';
                    break;
                default :
                    $this->error = 'You did not select a file to upload.';
                    break;
            }

            return FALSE;
        }

        // Is the file type allowed to be uploaded?
        $x = explode('.', $_FILES[$field_name]['name']);
        $ext = end($x);
        $ext = strtolower($ext);
        $allowed_types = $type == 'photo'?$this->allowed_image_types: $this->allowed_video_types;
        if (!in_array($ext, $allowed_types)) {
            $this->error = 'The filetype you are attempting to upload is not allowed.';
            return FALSE;
        }

        // Is the file size within the allowed maximum?
        $file_size = $_FILES[$field_name]['size'];
        // Convert the file size to kilobytes
        if ($file_size > 0) {
            $file_size = round($file_size / 1024, 2);
        }
        $max_size = $type == 'photo'?$this->max_image_size:$this->max_video_size;
        if ($max_size != 0 && $file_size > $max_size) {
            $this->error = 'The file you are attempting to upload is larger than the permitted size.';
            return FALSE;
        }
        return TRUE;
    }

    /**
     * resize image
     *
     * @param string $source - path to file
     * @param string $file_name
     * @param int $width
     * @param int $height
     * @return file name if success
     * 		   FALSE if failed
     */
    function create_thumbnail($source, $file_name, $width, $height, $folder = BUCKET_NAME_UPLOAD) {
        $config['image_library'] = 'gd2';
//		$source = "http://anomo-sandbox.s3.amazonaws.com/Chrysanthemum.jpg";
        $config['source_image'] = $source;
        $config['create_thumb'] = TRUE;
        $config['thumb_marker'] = '_thumb_' . $width . 'x' . $height;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $width;
        $config['height'] = $height;
        $CI = & get_instance();
        $CI->load->library('image_lib');
        $CI->image_lib->initialize($config);
        $ret = '';
        if ($CI->image_lib->resize()) {
            $ret = $CI->image_lib->full_dst_path;
            $type = $CI->image_lib->mime_type;
        }
        $CI->image_lib->clear();
        if ($ret != '') {
            // then upload to amazonS3
            $xp = $CI->image_lib->explode_name($file_name);
            $new_file_name = $xp['name'] . $config['thumb_marker'] . $xp['ext'];
            return $this->_uploadS3($ret, $new_file_name, $type, $folder);
        }
        return false;
    }

    /**
     * upload file to S3
     *
     * @param string $source
     * @param string $file_name
     * @return file name if Success
     * 			FALSE if failed
     */
    public function _uploadS3($source, $file_name, $content_type = NULL, $folder = BUCKET_NAME_UPLOAD) {
        $s3 = new S3($this->accessKey, $this->secretKey, false, $this->endPoint);
        if ($s3->putObjectFile($source, $this->bucketName, $folder . '/' . $file_name, S3::ACL_PUBLIC_READ, array(), $content_type)) {
            return $file_name;
        }
        return false;
    }

    /**
     * list all file on my bucket
     *
     */
    function listFile() {
        $s3 = new S3($this->accessKey, $this->secretKey, false, $this->endPoint);
        $bucket_contents = $s3->getBucket($this->bucketName);
        foreach ($bucket_contents as $file) {

            $fname = $file['name'];
            $furl = "http://" . $this->bucketName . ".s3.amazonaws.com/" . $fname;
            //output a link to the file
            echo "<a href=\"$furl\">$fname</a><br />";
        }
    }

    /**
     * list all file in specific folder
     *
     */
    function listFileByFolder($folder = BUCKET_NAME_UPLOAD) {
        $s3 = new S3($this->accessKey, $this->secretKey, false, $this->endPoint);
        $bucket_contents = $s3->getBucket($this->bucketName, $folder);
        return $bucket_contents;
    }

    function getInfo($new_file_name) {
        $s3 = new S3($this->accessKey, $this->secretKey, false, $this->endPoint);
        return $s3->getObjectInfo($this->bucketName, $new_file_name);
    }

    /**
     * delete file to S3
     *
     * @param string $url
     * @return true if Success
     * 			false if failed
     */
    function deleteObjectS3($url) {
        $s3 = new S3($this->accessKey, $this->secretKey, false, $this->endPoint);
        if ($s3->deleteObject($this->bucketName, $url)) {
            return true;
        }
        return false;
    }

    /**
     * Display the error message
     *
     * @access	public
     * @param	string
     * @param	string
     * @return	string
     */
    function display_errors($open = '<p>', $close = '</p>') {
        $str = '';
        if (is_array($this->error)){
            foreach ($this->error as $val) {
                $str .= $open . $val . $close;
            }
        }else{
            $str = $this->error;
        }
        return $str;
    }

    function copyS3($sUrl, $desUrl) {
        $s3 = new S3($this->accessKey, $this->secretKey, false, $this->endPoint);
        return $s3->copyObject($this->bucketName, $sUrl, $this->bucketName, $desUrl, S3::ACL_PUBLIC_READ);
    }

    function getS3Object($url, $desFile) {
        $s3 = new S3($this->accessKey, $this->secretKey, false, $this->endPoint);
        return $s3->getObject($this->bucketName, $url, $desFile);
    }

}

?>