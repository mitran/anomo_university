<?php
function uri_sort($value){
	$CI = &get_instance();
	$url = base_url();
	$i = 0;
	foreach ($CI->uri->segments as $seg){
		$i ++;
		$url .= $seg;
		if ($i < count($CI->uri->segments)){
			$url .= '/';
		}
	}
	$vn_param = $CI->uri->vn_param;
	if (count($vn_param)>0){
		$vn_param['sort'] = $value;
		foreach ($vn_param as $key => $val){
			$url .= '/'.P_SIGN.$key.'/'.htmlspecialchars($val);
		}
	}else{
		$url .= '/'.P_SIGN.'sort/'.$value;
	}
	return $url;
}

?>