ALTER TABLE  `user` ADD  `Point` INT(11) NULL default 0 ;
ALTER TABLE  `user_archive` ADD  `Point` INT(11) NULL default 0 ;

ALTER TABLE  `user` ADD  `NumberPostLike` INT(11) NULL default 0 ;
ALTER TABLE  `user_archive` ADD  `NumberPostLike` INT(11) NULL default 0 ;

ALTER TABLE  `user` ADD  `NumberFriendInvite` INT(11) NULL default 0 ;
ALTER TABLE  `user_archive` ADD  `NumberFriendInvite` INT(11) NULL default 0 ;

ALTER TABLE  `activity` ADD  `IsAnonymous` TINYINT(4) NULL default 0 ;
ALTER TABLE  `activity_archive` ADD  `IsAnonymous` TINYINT(4) NULL default 0 ;

ALTER TABLE  `userprofilestatus` ADD  `IsDeleted` TINYINT(4) NULL default 0 ;
ALTER TABLE  `userprofilestatus` ADD  `IsAnonymous` TINYINT(4) NULL default 0 ;

ALTER TABLE  `picturepostactivity` ADD  `IsDeleted` TINYINT(4) NULL default 0 ;

ALTER TABLE  `notificationhistory` ADD  `ContentID` INT(11) NULL  ;
ALTER TABLE  `notificationhistory` ADD  `IsAnonymous` TINYINT(4) NULL default 0 ;
ALTER TABLE  `notificationhistory_archive` ADD  `ContentID` INT(11) NULL ;
ALTER TABLE  `notificationhistory_archive` ADD  `IsAnonymous` TINYINT(4) NULL default 0 ;

ALTER TABLE  `cronmentionuser` ADD  `IsAnonymous` TINYINT(4) NULL default 0 ;

CREATE TABLE `sms_message` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL,
  `PhoneNumber` text,
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

CREATE TABLE `blocked_user_1day` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL,
  `CreatedDate` date DEFAULT NULL,
  `IsSent` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `UserID` (`UserID`),
  KEY `IsSent` (`IsSent`),
  KEY `CreaatedDate` (`CreatedDate`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `vegas_count` (
  `Count` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into `vegas_count` (Count) value (0);

-- Vegas
ALTER TABLE  `user` ADD  `IsVendor` TINYINT(4) NULL default 0 ;
ALTER TABLE  `user_archive` ADD  `IsVendor` TINYINT(4) NULL default 0 ;

-- Vegas mass messaging
CREATE TABLE `vegas_msg` (
  `MessageID` int(11) NOT NULL AUTO_INCREMENT,
  `Message` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Photo` varchar(255) DEFAULT NULL,
  `Status` int(1) DEFAULT NULL COMMENT '1: Send, 0: Not Send',
  `CreatedDate` datetime DEFAULT NULL,
  `TotalTracking` int(11) DEFAULT NULL,
  `TapAction` varchar(255) DEFAULT NULL,
  `TapParam` text,
  PRIMARY KEY (`MessageID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `vegas_msg_tracking` (
  `MessageID` int(11) NOT NULL,
  `UserID` int(11) DEFAULT NULL,
  `Message` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Photo` varchar(255) DEFAULT NULL,
  `Status` int(1) DEFAULT NULL COMMENT '1: Send, 0: Not Send',
  `CreatedDate` datetime DEFAULT NULL,
  `TapAction` varchar(255) DEFAULT NULL,
  `TapParam` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



