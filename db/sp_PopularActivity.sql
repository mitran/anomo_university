CREATE PROCEDURE `sp_PopularActivity`(IN `userId` INT,
			IN `actionTypes` VARCHAR(256),
			IN `photoUrl` VARCHAR(256),
			IN `categoryIconUrl` VARCHAR(256),
			IN `offsetResults` INT,
			IN `limitResults` INT,
			IN `igender` INT,
			IN `fromAge` INT,
			IN `toAge` INT,				IN `prevTotal` INT,
			OUT `totalResults` INT)
BEGIN


SELECT temp.*,
 IF (u.Photo != '', CONCAT(photoUrl,u.Photo), '') AS Avatar,
 '' AS OwnerAvatar,
 IF (Image != '', CONCAT(photoUrl,Image), '') AS Image,
 IF (PlaceIcon != NULL || PlaceIcon != '', CONCAT(categoryIconUrl,PlaceIcon), '') AS PlaceIcon,
 IF (temp.PlaceID != NULL || temp.PlaceID != '', temp.PlaceID, '') AS PlaceID,
 IF (PlaceName != NULL || PlaceName != '', PlaceName, '') AS PlaceName,
 IF (PlaceAddress != '' || PlaceAddress != NULL, PlaceAddress, '' ) AS PlaceAddress,
 IF (ak.ID is not null, 1, 0) as IsLike,
 0 as IsBlock,
 0 as IsBlockTarget,
 IF (fc.ID is not null, 1, 0) as IsReported,
 IF (u.FacebookID is not null, u.FacebookID, '') as FacebookID,
 IF (u.UserName is not null, u.UserName, '') as FromUserName, 
 '' as OwnerName, 
 IF (u2.UserName is not null, u2.UserName, '') as GiftReceiverName, 
 IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,
 -- Hung added
 IF (temp.CheckinPlaceID is not null, temp.CheckinPlaceID, '') AS CheckinPlaceID,
 IF (temp.CheckinPlaceName is not null, temp.CheckinPlaceName, '') AS CheckinPlaceName,
 IF (temp.CheckinPlaceID is not null, 1, 0) AS CheckinStatus,
 IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,
	
 if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
  		if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
 )AS NeighborhoodName
FROM (
	select a.ActivityID, a.FromUserID, a.Message, a.Image, a.RefID, a.Type, a.ActionType, a.PlaceID, a.PlaceName, a.PlaceIcon
				, a.PlaceAddress, a.Lat, a.Lon, a.CreatedDate, a.IsInvalid, a.LastUpdate, a.Gender, a.BirthDate, a.OwnerID, a.OrginalDate
				, a.GiftReceiverID, a.CheckinPlaceID, a.CheckinPlaceName, a.NeighborhoodID
				, ak.TotalLike as `Like`
				,(ak.TotalLike + if(totalcomments, totalcomments, 0)) / 
(POWER(1.25, TIMESTAMPDIFF(HOUR, CreatedDate, NOW()))) as PopularRank
	from
  (
      select ContentID, Type,count(*) as TotalLike, 
(select count(distinct ac.UserID) from activitycomment ac where ac.ContentID=al.ContentID AND ac.Type=al.Type) as totalcomments
					-- 	count(*) / (POWER(1.1, TIMESTAMPDIFF(HOUR, CreatedDate, NOW()))) as AdjustedLike
					, concat(ContentID,'-',Type) as TempID
      from activitylike al
      where CreatedDate > DATE_SUB(Now(), INTERVAL 16 HOUR)
      group by TempID ORDER BY TotalLike DESC Limit 1000
   ) as ak
  left join activity a on a.RefID=ak.ContentID AND a.ActionType=ak.Type
	WHERE FIND_IN_SET(ActionType, actionTypes) > 0 AND IsInvalid != 1
	AND ((igender > 0 AND Gender=igender) OR igender <= 0)
	AND ((fromAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) >= fromAge) OR fromAge <=0)
	AND ((toAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) <= toAge) OR toAge <=0)
	-- ANOMO-7364
	AND FromUserID NOT IN (select TargetUserID from userblock where CurrentUserID = userId)
	-- end ANOMO-7364
	ORDER BY PopularRank DESC LIMIT offsetResults,limitResults
) as temp 

LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=userId

LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=userId
LEFT JOIN `user` u ON u.UserID=temp.FromUserID
LEFT JOIN `user` u2 ON u2.UserID = temp.GiftReceiverID

-- ANOMO-7868 Ability to stop receiving notifications on specific threads
LEFT JOIN stopreceivenotify srn ON srn.UserID = userId AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType

-- Hung added...
LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID;

-- SET totalResults = FOUND_ROWS();
IF prevTotal > 0 THEN
	SET totalResults = prevTotal ; --  - 12;
ELSE
SET totalResults = (select count(a.ActivityID) as TotalRecord
                from
                (
                        select ContentID, Type, concat(ContentID,'-',Type) as TempID
												,count(*) as TotalLike
                        from activitylike
                        where CreatedDate > DATE_SUB(Now(), INTERVAL 16 HOUR)
                        group by TempID ORDER BY TotalLike DESC Limit 1000
                ) as temp
                left join activity a on a.RefID=temp.ContentID AND a.ActionType=temp.Type
						 		WHERE FIND_IN_SET(ActionType, actionTypes) > 0 AND IsInvalid != 1
								AND ((igender > 0 AND Gender=igender) OR igender <= 0)
								AND ((fromAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) >= fromAge) OR fromAge <=0)
								AND ((toAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) <= toAge) OR toAge <=0)
								-- ANOMO-7364
								AND FromUserID NOT IN (select TargetUserID from userblock where CurrentUserID = userId)
);
END IF;

END