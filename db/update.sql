ALTER TABLE  `user` ADD  `AboutMe` TEXT NULL;
ALTER TABLE  `user_archive` ADD  `AboutMe` TEXT NULL;
ALTER TABLE  `flagcontent` ADD  `IsComment` TINYINT(4) NULL default 0  COMMENT '0: report content / 1: report comment';
ALTER TABLE  `tag` ADD  `TagCategoryID` INT(11) NULL default 0 ;

ALTER TABLE  `notificationhistory` ADD  `UnView` TINYINT(4) NULL default 0  COMMENT '0: viewed / 1: unview';


update user set Password = '' where 1;

CREATE TABLE `avatar` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Gender` tinyint(4) DEFAULT NULL COMMENT '1: Male, 2: Female',
  `FullAvatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ind_gender` (`Gender`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- update store procedure

CREATE TABLE `chat_count` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `ChatCount` int(11) DEFAULT '0',
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `intent` (
  `IntentID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IntentID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE `tagcategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `userintent` (
  `IntentID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  PRIMARY KEY (`IntentID`,`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;