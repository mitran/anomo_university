ALTER TABLE `user`
ADD COLUMN `IsVerify`  tinyint(4) NULL DEFAULT 0 AFTER `Notes`;

CREATE TABLE `userverification` (
  `UserID` int(11) NOT NULL DEFAULT '0',
  `Code` varchar(10) DEFAULT NULL,
  `Attempt` int(4) DEFAULT '0',
  `CreatedDate` datetime DEFAULT NULL,
  `VerifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

update `user` set IsVerify = 1;

-- ANOMO-11735
-- "Meeting Friends" will be mapped to "Friendship"
update intent set Name = 'Friendship' where IntentID = 1; 

-- "Humor" will be mapped to "Fun & Entertainment"
-- "Something Else" will be mapped to "Fun & Entertainment"
update intent set Name = 'Fun & Entertainment' where IntentID = 4; 
update IGNORE userintent set IntentID = 4 where IntentID = 5;
delete from userintent where IntentID = 5;
-- delete "Something Else"
delete from intent where IntentID = 5;

-- delete "Love", "Flirting"
delete from intent where IntentID = 2;
delete from intent where IntentID = 3;
delete from userintent where IntentID = 2;
delete from userintent where IntentID = 3;

-- Networking is new category.
insert into intent (`Name`) values ('Networking');
insert into intent (`Name`) values ('Conversion');

-- ANOMO-11749

CREATE TABLE `notify_follower_queue` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL,
  `ContentID` int(11) DEFAULT NULL,
  `ContentType` int(11) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT '0',
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- ANOMO-11755 Vine support 
ALTER TABLE `activity` ADD COLUMN `VideoThumbnail` varchar(255) NULL;
ALTER TABLE `activity_archive` ADD COLUMN `VideoThumbnail` varchar(255) NULL;

ALTER TABLE `video` ADD COLUMN `VideoThumbnail` varchar(255) NULL;

-- Daily Match
CREATE TABLE `dailymatch` (
  `CurrentUserID` int(11) NOT NULL DEFAULT '0',
  `TargetUserID` int(11) NOT NULL DEFAULT '0',
  `Interest` int(11) DEFAULT '0',
  `IceBreaker` int(11) DEFAULT '0',
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`CurrentUserID`,`TargetUserID`),
  KEY `CurrentUserID` (`CurrentUserID`),
  KEY `TargetUserID` (`TargetUserID`),
  KEY `CreatedDate` (`CreatedDate`),
  KEY `curid_targetid` (`CurrentUserID`,`TargetUserID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Daily Match Tracking
CREATE TABLE `dailymatchtracking` (
  `TrackingID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL,
  `UserName` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `NumOfInterest` int(11) DEFAULT NULL,
  `NumOfQuestion` int(11) DEFAULT NULL,
  `Gender` int(1) DEFAULT NULL,
  `GenderDating` int(1) DEFAULT NULL,
  `GenderMatch` int(1) DEFAULT NULL,
  `FromAge` int(3) DEFAULT NULL,
  `ToAge` int(3) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`TrackingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `dailymatch` ADD COLUMN `NumberOfChat`  int(11) NULL DEFAULT 0 ;





