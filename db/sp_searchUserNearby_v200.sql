CREATE  PROCEDURE `sp_searchUserNearby_v200`(IN `_USERID` INT,
	IN `_OFFSET` INT,
	IN `_LIMIT` INT,
	IN `_LAT` DECIMAL(12,7),
	IN `_LON` DECIMAL(12,7),
	IN `_DATESEARCH` DATETIME,
	IN `_GENDER` INT,
	IN `_FROMAGE` INT,
	IN `_TOAGE` INT,
	OUT `_TOTALRESULT` INT)
BEGIN
SELECT
	DISTINCT(u.UserID),u.UserName,u.Lat,u.Lon,u.Photo AS Avatar,u.ProfileStatus,u.Email,u.NeighborhoodID,u.BirthDate,u.Gender,
	if(u.FacebookID is not null,u.FacebookID, '') as FacebookID,
	if( u.LastActivity >= _DATESEARCH ,1,0 ) AS IsOnline,
	'' AS PlaceID,
	0 AS Status ,
	u.Distance,
	if (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,
	0 as IsBlock,
	0 as IsBlockTarget,
-- if (n.`NAME` is not null && n.NAME != '', if(n.STATE is not null && n.STATE != '' && n.NAME != n.STATE, concat(n.NAME, ', ', n.STATE), if (n.COUNTY is not null && n.NAME != n.COUNTY, concat(n.NAME, ', ', n.COUNTY), n.NAME)), if (n.COUNTY is not null, n.COUNTY, '')) AS NeighborhoodName


if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
		if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), nbh.NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
)AS PlaceName

, '' as NeighborhoodName

-- Hung added a minor fix here... no longer need to check _LAT/_LON > 0 bc the php code will take care of it
FROM ( SELECT * , 69.09*DEGREES(acos(sin(radians(_LAT))*sin(radians(Lat)) + cos(radians(_LAT))*cos(radians(Lat))*cos(radians(_LON)-radians(Lon)))) as Distance
			FROM User
			WHERE user.UserID != _USERID and LastActivity >= _DATESEARCH AND IsBanned != 1 
				and Lat is not null and Lon is not null

				AND ((_GENDER > 0 AND Gender=_GENDER) OR _GENDER <= 0)
				AND ((_FROMAGE > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) >= _FROMAGE) OR _FROMAGE <=0)
				AND ((_TOAGE > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) <= _TOAGE) OR _TOAGE <=0)

				-- dont allow search block `user`
				AND UserID NOT IN (select if(CurrentUserID = _USERID, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = _USERID or TargetUserID = _USERID)
				-- 

				ORDER BY distance asc
				LIMIT _OFFSET, _LIMIT
			) as u

			
			LEFT JOIN userconnect cc ON cc.TargetUserID=u.UserID AND cc.CurrentUserID = _USERID

			-- left JOIN userblock ub1 ON ub1.TargetUserID=u.UserID AND ub1.CurrentUserID=_USERID
			-- left JOIN userblock ub2 ON ub2.CurrentUserID=u.UserID AND ub2.TargetUserID=_USERID
			left join neighborhood nbh ON u.NeighborhoodID=nbh.OGR_FID
			;

-- SET _TOTALRESULT = FOUND_ROWS();
SET _TOTALRESULT = (SELECT count(UserID)
				FROM User
				WHERE user.UserID != _USERID and LastActivity >= _DATESEARCH AND IsBanned != 1
				and Lat is not null and Lon is not null
				-- dont allow search block `user`
				AND UserID NOT IN (select if(CurrentUserID = _USERID, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = _USERID or TargetUserID = _USERID)
				-- 
				AND ((_GENDER > 0 AND Gender=_GENDER) OR _GENDER <= 0)
				AND ((_FROMAGE > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) >= _FROMAGE) OR _FROMAGE <=0)
				AND ((_TOAGE > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) <= _TOAGE) OR _TOAGE <=0));
END