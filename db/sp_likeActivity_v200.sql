CREATE  PROCEDURE `sp_likeActivity_v200`(IN `_USERID` INT,
	IN `_CONTENTID` INT,
	IN `_TYPE` INT,
	IN `_USERNAME` VARCHAR(256),
	IN `_AVATAR` VARCHAR(256),
	IN `_LAT` DECIMAL(12,7),
	IN `_LON` DECIMAL(12,7),
	OUT `_NUMBERLIKE` INT,
	OUT `_ISLIKE` INT, -- 1: like/0:unlike
	OUT `_RETURN` INT,  -- 1: ok / 0: invalid
	OUT `_OWNERID` INT, OUT `_HISTORYID` INT)
BEGIN
	DECLARE _LIKEID INT;
	DECLARE _NEWLIKEID INT;
	DECLARE tableName VARCHAR(256);
	DECLARE isLike INT;
	DECLARE numberLike INT;
	DECLARE flag INT;
	DECLARE isStopNotify INT;
	
	SET isStopNotify = 0;
	SET _OWNERID = 0;
	SET isLike = 0; 
	SET flag = 0;
	SET numberLike = 0;
	SET tableName = '';
  SET @userIdField = 'UserID';
  SET _HISTORYID = -1;
	CASE _TYPE
		WHEN 0 THEN
			SET tableName = 'usercheckin';
		WHEN 1 THEN
			SET tableName = 'userprofilestatus';	
		WHEN 2 THEN
			SET tableName = 'placepost';	
		WHEN 6 THEN
			SET tableName = 'placepost';	
		WHEN 7 THEN
			SET tableName = 'neighborhoodpost';		
		WHEN 8 THEN
			SET tableName = 'neighborhoodpost';			
		WHEN 27 THEN
			SET tableName = 'picturepostactivity';
    WHEN 9 THEN 
			SET tableName = 'giftsend';
      SET @userIdField = 'SendUserID';
		ELSE
			SET flag = 0;	
	END CASE;
	
	IF (tableName != '') THEN
		-- get content info
		SET @tempSQL = CONCAT("SELECT ", @userIdField, " INTO @ownerID FROM " , tableName, " WHERE ID= ", _CONTENTID);
		PREPARE stmt FROM @tempSQL;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
		
		IF (@ownerID <= 0) THEN
			-- not exist this content
			SET flag = 0;
		ELSE
			SET _OWNERID = @ownerID;
			SET @tempSQL5 = CONCAT("SELECT `Like` INTO @nLike FROM " , tableName, " WHERE ID= ", _CONTENTID);
			PREPARE stmt5 FROM @tempSQL5;
			EXECUTE stmt5;
			DEALLOCATE PREPARE stmt5;
		 	SET numberLike=@nLike;		

			SELECT ID INTO @likeID FROM activitylike WHERE ContentID=_CONTENTID AND Type=_TYPE AND UserID=_USERID limit 1;
			--  check this user is liked this content
			IF (@likeID > 0) THEN
				SET isLike = 0;
				
				-- become unlike this content
				DELETE FROM activitylike WHERE ContentID=_CONTENTID AND Type=_TYPE AND UserID=_USERID;
				DELETE FROM notificationhistory WHERE RefID=@likeID AND Type=13;
				
				-- decrease #Like
				SET @tempSQL1 = CONCAT("update " , tableName, " SET `Like`=`Like`-1 WHERE ID=", _CONTENTID);
				PREPARE stmt1 FROM @tempSQL1;
				EXECUTE stmt1;
				SET numberLike = numberLike - 1;
			ELSE
				SET isLike = 1;
				
				-- like this content
				INSERT INTO activitylike (`UserID`,`ContentID`,`Type`,`CreatedDate`)
					VALUE(_USERID,_CONTENTID,_TYPE,UTC_TIMESTAMP());
				SET _NEWLIKEID = LAST_INSERT_ID();

					
				-- increase #Like
				SET @tempSQL2 = CONCAT("update " , tableName, " SET `Like`=`Like`+1 WHERE ID=", _CONTENTID);
				PREPARE stmt2 FROM @tempSQL2;
				EXECUTE stmt2;

				SET numberLike = numberLike + 1;
				
				-- add notify history				
				IF (@ownerID != _USERID) THEN
					SET isStopNotify = (select count(ID) from stopreceivenotify where ContentID = _CONTENTID AND Type = _TYPE AND UserID = @ownerID limit 1);
					IF (isStopNotify <= 0) THEN 
						INSERT INTO notificationhistory (`UserID`,`SendUserID`,`Type`,`RefID`,`ContentType`,`IsRead`, `UnView`,`CreatedDate`)
							VALUE (@ownerID,_USERID,13,_NEWLIKEID,_TYPE,1,1,UTC_TIMESTAMP());
						SET _HISTORYID = LAST_INSERT_ID();
					END IF;
				END IF;
										
			END IF;
			
			SET flag = 1;
		END IF;
	ELSE
		SET flag = 0;
	END IF;

	-- output
	SET _RETURN = flag;
	SET _ISLIKE =isLike;
	SET _NUMBERLIKE=numberLike;
END