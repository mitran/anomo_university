CREATE PROCEDURE `sp_searchUserKeyword_v200`(IN `_USERID` INT,
 IN _OFFSET INT,	IN `_LIMIT` INT,
	IN `_KEYWORD` VARCHAR(256) charset utf8,
	IN `_DATESEARCH` DATETIME, OUT _TOTALRESULT  INT)
BEGIN

SELECT 
	DISTINCT(u.UserID),u.UserName,u.Lat,u.Lon,u.Photo AS Avatar,u.ProfileStatus,u.Email,u.NeighborhoodID, u.BirthDate,u.Gender,
	if(u.FacebookID is not null,u.FacebookID, '') as FacebookID,
	if( u.LastActivity >= _DATESEARCH ,1,0 ) AS IsOnline,
	'' AS PlaceID,
	0 AS Status,
	if (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,
	0 as IsBlock,
	0 as IsBlockTarget,

-- if (n.`NAME` is not null && n.NAME != '', if(n.STATE is not null && n.STATE != '' && n.NAME != n.STATE, concat(n.NAME, ', ', n.STATE), if (n.COUNTY is not null && n.NAME != n.COUNTY, concat(n.NAME, ', ', n.COUNTY), n.NAME)), if (n.COUNTY is not null, n.COUNTY, '')) AS NeighborhoodName
if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
		if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), nbh.NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
)AS PlaceName
  ,'' as NeighborhoodName
FROM (
	SELECT DISTINCT(u1.UserID),UserName,Lat,Lon,Photo,ProfileStatus,Email,NeighborhoodID, BirthDate,Gender, LastActivity,LastCheckInID,FacebookID,OnlyShowCountry
    FROM User u1  

		WHERE u1.UserID !=_USERID AND IsBanned != 1 
		-- dont allow search block `user`
		AND UserID NOT IN (select if(CurrentUserID = _USERID, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = _USERID or TargetUserID = _USERID)
		-- 
		AND (LCASE( u1.UserName ) LIKE concat('%',_KEYWORD,'%'))
		LIMIT _OFFSET,_LIMIT

	)as u


LEFT JOIN userconnect cc ON cc.TargetUserID=u.UserID AND cc.CurrentUserID = _USERID

-- left JOIN userblock ub1 ON ub1.TargetUserID=u.UserID AND ub1.CurrentUserID=_USERID
-- left JOIN userblock ub2 ON ub2.CurrentUserID=u.UserID AND ub2.TargetUserID=_USERID
left join neighborhood nbh ON u.NeighborhoodID=nbh.OGR_FID
;

SET _TOTALRESULT = (SELECT count(UserID)
				FROM User u1
				WHERE (LCASE( u1.UserName ) LIKE concat('%',_KEYWORD,'%'))
				AND u1.UserID !=_USERID AND IsBanned != 1 )
				-- dont allow search block `user`
				AND UserID NOT IN (select if(CurrentUserID = _USERID, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = _USERID or TargetUserID = _USERID)
				-- 
				;

END