ALTER TABLE `dailymatch` ADD COLUMN `IsNotify`  tinyint(4) NULL DEFAULT 0 ;

ALTER TABLE `video` ADD COLUMN `DeletedDate`  datetime NULL ;
ALTER TABLE `userprofilestatus` ADD COLUMN `DeletedDate`  datetime NULL ;
ALTER TABLE `picturepostactivity` ADD COLUMN `DeletedDate`  datetime NULL ;


ALTER TABLE `user` ADD COLUMN `BannedDate`  datetime NULL ;
ALTER TABLE `user_archive` ADD COLUMN `BannedDate`  datetime NULL ;

-- New stream flow
ALTER TABLE `activity` ADD COLUMN `TopicID`  int(11) NULL AFTER `VideoThumbnail`;

ALTER TABLE `activity_archive` ADD COLUMN `TopicID`  int(11) NULL AFTER `VideoThumbnail`;


ALTER TABLE `hashtag` ADD COLUMN `CateID`  int(11) NULL ;
ALTER TABLE `hashtag` ADD COLUMN `TopicID`  int(11) NULL ;

CREATE TABLE `user_topic_activity` (
  `TopicID` int(11) NOT NULL DEFAULT '0',
  `UserID` int(11) NOT NULL DEFAULT '0',
  `CloseTopic` tinyint(4) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`TopicID`,`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `sticker` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CateID` int(11) DEFAULT NULL,
  `Photo` varchar(255) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;


CREATE TABLE `sticker_category` (
  `CateID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Photo` varchar(255) DEFAULT NULL,
  `Default` tinyint(4) DEFAULT '0',
  `CreatedDate` datetime DEFAULT NULL,
  `Version` int(11) DEFAULT NULL,
  `HaveUpdate` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`CateID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ANOMO-12269 - Improve matching algorithm
ALTER TABLE `question` ADD COLUMN `Weight`  int(11) NULL ;
ALTER TABLE `resultunique` ADD COLUMN `Weight`  int(11) NULL ;

-- new stream flow
update activity set TopicID = 1 where TopicID is null AND Type != 28;
update topic set TotalPost = (select count(*) from activity where TopicID =1 ) where TopicID = 1;





