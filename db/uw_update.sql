CREATE TABLE `promocode` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` tinyint(4) NOT NULL,
  `UserID` int(11) DEFAULT NULL,
  `ExpireDate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE TABLE `email_verification` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(255) NOT NULL DEFAULT '0',
  `Code` varchar(10) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT '0',
  `CreatedDate` datetime DEFAULT NULL,
  `VerifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `topicban` (
  `UserID` int(11) NOT NULL DEFAULT '0',
  `TopicID` int(11) NOT NULL DEFAULT '0',
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`UserID`,`TopicID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `topiccategory` ADD COLUMN `AllowCreateTopic`  tinyint(4) NULL DEFAULT '1' ;
ALTER TABLE `user` ADD COLUMN `IsEmailVerify`  tinyint(4) NULL DEFAULT '0' ;
ALTER TABLE `Desc` ADD COLUMN `topiccategory`  text NULL ;



CREATE TABLE `major` (
  `MajorID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `MajorCategoryID` int(11) DEFAULT '0',
  PRIMARY KEY (`MajorID`),
  KEY `ind_name` (`Name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE TABLE `majorcategory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `usermajor` (
  `MajorID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`MajorID`,`UserID`),
  KEY `MajorID` (`MajorID`) USING BTREE,
  KEY `UserID` (`UserID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `promocode` ADD COLUMN `ActionType`  varchar(255) NULL ;
ALTER TABLE `promocode` ADD COLUMN `ActionValue`  varchar(255) NULL ;


