ALTER TABLE `user` ADD COLUMN `NumberShareFbWall` int(11) NULL DEFAULT 0;
ALTER TABLE `user_archive` ADD COLUMN `NumberShareFbWall` int(11) NULL DEFAULT 0;


-- Luong added
ALTER TABLE `activitycomment`
ADD COLUMN `IsAnonymous`  tinyint(1) NULL DEFAULT 0 AFTER `NumberOfLike`;

ALTER TABLE `notificationhistory`
ADD COLUMN `IsAnonymousComment`  tinyint(1) NULL DEFAULT 0 AFTER `IsAnonymous`;

ALTER TABLE `notificationhistory_archive`
ADD COLUMN `IsAnonymousComment`  tinyint(1) NULL DEFAULT 0 AFTER `IsAnonymous`;

ALTER TABLE `cronpushnotification`
ADD COLUMN `IsAnonymousComment`  tinyint(1) NULL DEFAULT 0 AFTER `IsAnonymous`;

CREATE TABLE `anonymousblock` (
  `CurrentUserID` int(11) NOT NULL,
  `TargetUserID` int(11) NOT NULL DEFAULT '0',
  `Date` datetime DEFAULT NULL,
  PRIMARY KEY (`CurrentUserID`,`TargetUserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Announcement Card - Fanpage
ALTER TABLE `announcement`
ADD COLUMN `RefType`  int(11) NULL DEFAULT 1 AFTER `Day`,
ADD COLUMN `RefID`  varchar(250) NULL AFTER `RefType`;
Update `announcement` set RefType = 1 where RefType is null;


-- Celebrity Campaign
CREATE TABLE `campaign` (
  `CampaignID` int(11) NOT NULL AUTO_INCREMENT,
  `CelebrityID` int(11) DEFAULT NULL,
  `Title` varchar(250) DEFAULT NULL,
  `Desc` varchar(250) DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `ExpiredDate` datetime DEFAULT NULL,
  `Status` tinyint(1) DEFAULT '0',
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`CampaignID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `campaigncau` (
  `UserID` int(11) NOT NULL DEFAULT '0',
  `CampaignID` int(11) NOT NULL DEFAULT '0',
  `CelebrityID` int(11) NOT NULL DEFAULT '-1',
  `CreatedDate` datetime NOT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `campaigncaudaily` (
  `UserID` int(11) NOT NULL DEFAULT '0',
  `CreatedDate` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`UserID`,`CreatedDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `user` ADD COLUMN `IsAdmin` tinyint(1) NULL DEFAULT 0;
ALTER TABLE `user` ADD COLUMN `IsAddToWatchList` tinyint(1) NULL DEFAULT 0;
ALTER TABLE `user` ADD COLUMN `Notes` text ;

ALTER TABLE `user_archive` ADD COLUMN `IsAdmin` int(1) NULL DEFAULT 0;
ALTER TABLE `user_archive` ADD COLUMN `IsAddToWatchList` int(1) NULL DEFAULT 0;
ALTER TABLE `user_archive` ADD COLUMN `Notes` text ;

CREATE TABLE `video` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL,
  `Content` text,
  `VideoID` text,
  `VideoSource` varchar(255) DEFAULT NULL,
  `Like` int(11) DEFAULT '0',
  `Comment` int(11) DEFAULT '0',
  `Share` int(11) DEFAULT '0',
  `IsDeleted` tinyint(4) DEFAULT '0',
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `track_username` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL,
  `UserName` varchar(255) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


