CREATE PROCEDURE `sp_register_v200`(IN `_UserName` VARCHAR(256),
	IN `_Email` VARCHAR(256),
	IN `_Gender` INT,
	IN `_BirthDate` VARCHAR(256),
	IN `_FbEmail` VARCHAR(256),
	IN `_FacebookID` VARCHAR(256),
	IN `_Photo` VARCHAR(256),
	IN `_FullPhoto` VARCHAR(256),
	IN `_Password` VARCHAR(256),
	IN `_TagIDs` VARCHAR(256), IN `_IntentIDs` VARCHAR(256),
	OUT `_USERID` INT)
BEGIN
	DECLARE _ID INT;
	DECLARE _NUMBERTAG INT;
	DECLARE _NUMBERINTENT INT;
	DECLARE _I INT;
	INSERT INTO User (`UserName`,`Email`,`Gender`,`BirthDate`,`FbEmail`,`FacebookID`,`Photo`,`FullPhoto`,`Password`, `SignUpDate`, `LastActivity`)
				VALUE (_UserName,_Email,_Gender,_BirthDate,_FbEmail,_FacebookID,_Photo,_FullPhoto,_Password, UTC_TIMESTAMP(), UTC_TIMESTAMP());

	SET _ID = LAST_INSERT_ID();
	
	-- ANOMO-7067
	-- add sign up user to activity stream
	-- actiontype = 28
	
	INSERT INTO Activity (`FromUserID`,`FromUserName`,`Avatar`,`Type`,`ActionType`,`Message`,`RefID`,`Lat`,`Lon`,`BirthDate`,`Gender`,`CreatedDate`)
		VALUE(_ID, _UserName,_Photo,28,28,'just joined Anomo!',_ID,null,null,_BirthDate,_Gender,UTC_TIMESTAMP());
	
	
	-- insert tag
	IF (_TagIDs != '') THEN
		SET _NUMBERTAG = (length(_TagIDs)-length(replace(_TagIDs,',','')))/length(',');
		SET _I = 1;
		WHILE _I <= _NUMBERTAG + 1 DO  
			INSERT INTO UserTag (`UserID`, `TagID`) VALUES (_ID, SPLIT_STR(_TagIDs, ',', _I));
			SET _I = _I + 1;  
		END WHILE;
	END IF;

  -- insert intent
	IF (_IntentIDs != '') THEN
		SET _NUMBERINTENT = (length(_IntentIDs)-length(replace(_IntentIDs,',','')))/length(',');
		SET _I = 1;
		WHILE _I <= _NUMBERINTENT + 1 DO  
			INSERT INTO userintent (`UserID`, `IntentID`) VALUES (_ID, SPLIT_STR(_IntentIDs, ',', _I));
			SET _I = _I + 1;  
		END WHILE;
	END IF;
	
	SET _USERID = _ID;
END