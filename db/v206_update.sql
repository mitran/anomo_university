ALTER TABLE `user` ADD COLUMN `IsAgeLess` tinyint(1) NULL DEFAULT 0 AFTER `IsVendor`;
ALTER TABLE `user_archive` ADD COLUMN `IsAgeLess` tinyint(1) NULL DEFAULT 0 AFTER `IsVendor`;

ALTER TABLE `activity`
ADD COLUMN `FanPage` int(11) NULL AFTER `IsAnonymous`;

ALTER TABLE `activity_archive`
ADD COLUMN `FanPage` int(11) NULL AFTER `IsAnonymous`;

ALTER TABLE `announcement`
ADD COLUMN `StartDate`  datetime NULL AFTER `CreatedDate`;
update announcement set StartDate = ExpireDate where StartDate is null;


CREATE TABLE `promos` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL,
  `Photo` varchar(255) DEFAULT NULL,
  `Content` text,
  `Link` text,
  `Like` int(11) DEFAULT '0',
  `Comment` int(11) DEFAULT '0',
  `IsPublished` tinyint(4) DEFAULT '1',
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;