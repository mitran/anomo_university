CREATE PROCEDURE `sp_searchHashTag_v200`(IN `userId` INT,
			IN `actionTypes` VARCHAR(256),
			IN `photoUrl` VARCHAR(256),
			IN `categoryIconUrl` VARCHAR(256),
			IN `offsetResults` INT,
			IN `limitResults` INT,
			IN `igender` INT,
			IN `fromAge` INT,
			IN `toAge` INT,
			IN `iActivityID` INT,
			IN `sHashTag` VARCHAR(256) charset utf8,
			OUT `totalResults` INT)
BEGIN


SELECT temp.*,
 IF (u.Photo != '', CONCAT(photoUrl,u.Photo), '') AS Avatar,
 '' AS OwnerAvatar,
 IF (Image != '', CONCAT(photoUrl,Image), '') AS Image,
 IF (PlaceIcon != NULL || PlaceIcon != '', CONCAT(categoryIconUrl,PlaceIcon), '') AS PlaceIcon,
 IF (temp.PlaceID != NULL || temp.PlaceID != '', temp.PlaceID, '') AS PlaceID,
 IF (PlaceName != NULL || PlaceName != '', PlaceName, '') AS PlaceName,
 IF (PlaceAddress != '' || PlaceAddress != NULL, PlaceAddress, '' ) AS PlaceAddress,
 IF (ak.ID is not null, 1, 0) as IsLike,
 0 as IsBlock,
 0 as IsBlockTarget,
 IF (fc.ID is not null, 1, 0) as IsReported,
 IF (u.FacebookID is not null, u.FacebookID, '') as FacebookID,
 IF (u.UserName is not null, u.UserName, '') as FromUserName, 
	'' as OwnerName, 
 IF (u2.UserName is not null, u2.UserName, '') as GiftReceiverName, 
 IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,
 if (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,

-- Hung added
 IF (temp.CheckinPlaceID is not null, temp.CheckinPlaceID, '') AS CheckinPlaceID,
 IF (temp.CheckinPlaceName is not null, temp.CheckinPlaceName, '') AS CheckinPlaceName,
 IF (temp.CheckinPlaceID is not null, 1, 0) AS CheckinStatus,
 IF (temp.PlaceID is not null, '', temp.NeighborhoodID) AS NeighborhoodID,

-- if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) AS NeighborhoodName
if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
		if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
)AS NeighborhoodName

FROM (
		select ActivityID, FromUserID, Message, Image, RefID, Type, ActionType, PlaceID, PlaceName, PlaceIcon, PlaceAddress, Lat, Lon, CreatedDate, IsInvalid, LastUpdate, Gender, BirthDate, OwnerID, OrginalDate, GiftReceiverID, CheckinPlaceID, CheckinPlaceName, NeighborhoodID
		from activity 
		where 
			ActivityID IN (
					select ActivityID from (select ActivityID from hashtag 
													where
													HashTag = sHashTag 
													GROUP BY ActivityID
													ORDER BY ActivityID DESC
													LIMIT offsetResults,limitResults		
											) as temp
			) 
			AND FIND_IN_SET(ActionType, actionTypes) > 0 AND IsInvalid != 1
			-- ANOMO-7364
			AND FromUserID NOT IN (select if(CurrentUserID = userId, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = userId or TargetUserID = userId)
			-- end ANOMO-7364
		ORDER BY CreatedDate DESC
) as temp
LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=userId
-- LEFT JOIN userblock ub1 ON ub1.TargetUserID=temp.FromUserID AND ub1.CurrentUserID=userId
-- LEFT JOIN userblock ub2 ON ub2.CurrentUserID=temp.FromUserID AND ub2.TargetUserID=userId
LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=userId
LEFT JOIN `user` u ON u.UserID=temp.FromUserID

LEFT JOIN `user` u2 ON u2.UserID = temp.GiftReceiverID
LEFT JOIN userconnect cc ON cc.TargetUserID=temp.FromUserID AND cc.CurrentUserID = userId

-- ANOMO-7868 Ability to stop receiving notifications on specific threads
LEFT JOIN stopreceivenotify srn ON srn.UserID = userId AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType

-- Hung added...
 LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID;


SET totalResults = (
	select count(*) from (
									select ActivityID 
									from hashtag 
									where
											HashTag = sHashTag 
									GROUP BY ActivityID
								) as temp 
);

END