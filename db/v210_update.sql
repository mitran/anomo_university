CREATE TABLE `topicban` (
  `UserID` int(11) NOT NULL DEFAULT '0',
  `TopicID` int(11) NOT NULL DEFAULT '0',
  `CreatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`UserID`,`TopicID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `topiccategory` ADD COLUMN `AllowCreateTopic`  tinyint(4) NULL DEFAULT '1' ;
