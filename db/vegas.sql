CREATE TABLE `vegas_count` (
  `Count` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into vegas_count (Count) value (0);

CREATE TABLE IF NOT EXISTS `auto_message` (
	`message_id`	INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`message`		TEXT,
	`photo`			VARCHAR(255),
	`status`		tinyint,
	`created_date`	datetime,
	`tap_action`	tinyint,
	`tap_param`		TEXT,
	`expire_date`	datetime
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `auto_message_constraints` (
	`message_id`	INT UNSIGNED NOT NULL PRIMARY KEY,
	`cond_key`		VARCHAR(50),
	`value`			TEXT,
	FOREIGN KEY (`message_id`) REFERENCES `auto_message` (`message_id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `auto_message_queue` (
	`user_id`		varchar(11),
	`message_id`	INT UNSIGNED,
	`status`		tinyint,
	`expire_date`	date,
	PRIMARY KEY (`user_id`, `message_id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `promo_location` (
	`id`			INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`name`			VARCHAR(255),
	`sender_id`		TEXT,
	`lat`			VARCHAR(100),
	`lon`			VARCHAR(100),
	`radius`		VARCHAR(100),
	`enable`		tinyint
) ENGINE=InnoDB;