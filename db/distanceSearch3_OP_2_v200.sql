CREATE  PROCEDURE `distanceSearch3_OP_2_v200`(IN `userId` INT,
			IN `hoursSearch` INT,
			IN `actionTypes` VARCHAR(256),
			IN `photoUrl` VARCHAR(256),
			IN `categoryIconUrl` VARCHAR(256),
			IN `distancesArray` VARCHAR(256),
			IN `distancesArrayLength` INT,
			IN `offsetResults` INT,
			IN `limitResults` INT,
			IN `radiusIn` INT,
			IN `cutOffVal` INT,
			IN `igender` INT,
			IN `fromAge` INT,
			IN `toAge` INT,
			IN `iActivityID` INT,
			OUT `radiusOut` INT,
			OUT `totalResults` INT, IN `prevTotal` INT)
BEGIN

DECLARE startLat DECIMAL(12,7);
DECLARE startLon DECIMAL(12,7);
DECLARE distanceType INT;
DECLARE distanceFactor INT;
DECLARE loopCounter INT;
DECLARE loopMaxCounter INT;
DECLARE distTemp VARCHAR(256);
DECLARE dist INT;

DECLARE isMeridian180 INT; -- 0/1
DECLARE lon1 DECIMAL(12,7);
DECLARE lat1 DECIMAL(12,7);
DECLARE lon2 DECIMAL(12,7);
DECLARE lat2 DECIMAL(12,7);
DECLARE lon1_2 DECIMAL(12,7); -- coordinates for additional bounding box
DECLARE lat1_2 DECIMAL(12,7);
DECLARE lon2_2 DECIMAL(12,7);
DECLARE lat2_2 DECIMAL(12,7);

DECLARE queryCount INT;


SET startLat = (SELECT `user`.Lat FROM `user` WHERE `user`.UserID = userId LIMIT 1);
SET startLon = (SELECT `user`.Lon FROM `user` WHERE `user`.UserID = userId LIMIT 1);

SET distanceType = 1;
SET distanceFactor = 69; -- miles
IF distanceType != 1 THEN
SET distanceFactor = 111.04; -- km
END IF;

IF radiusIn > -1 THEN -- ****** radius was provided so use it ******

SET dist = radiusIn;

SET lon1 = startLon - dist / abs(cos(radians(startLat)) * distanceFactor);
SET lon2 = startLon + dist / abs(cos(radians(startLat)) * distanceFactor); 
SET lat1 = startLat - (dist / distanceFactor);
SET lat2 = startLat + (dist / distanceFactor);

SET isMeridian180 = 0;
IF lon1 < -180 THEN
SET isMeridian180 = 1;

SET lon1_2 = 360 + lon1;
SET lon2_2 = 179.999;
SET lat1_2 = lat1;
SET lat2_2 = lat2;

SET lon1 = -179.999;
ELSEIF lon2 > 180 THEN
SET isMeridian180 = 1;

SET lon1_2 = -179.999;
SET lon2_2 = -360 + lon2;
SET lat1_2 = lat1;
SET lat2_2 = lat2;

SET lon2 = 179.999;
END IF;

IF isMeridian180 = 0 THEN
SELECT temp.*,
 IF (u.Photo != '', CONCAT(photoUrl,u.Photo), '') AS Avatar,
 '' AS OwnerAvatar,
 IF (Image != '', CONCAT(photoUrl,Image), '') AS Image,
 IF (PlaceIcon != NULL || PlaceIcon != '', CONCAT(categoryIconUrl,PlaceIcon), '') AS PlaceIcon,
 IF (temp.PlaceID != NULL || temp.PlaceID != '', temp.PlaceID, '') AS PlaceID,
 IF (PlaceName != NULL || PlaceName != '', PlaceName, '') AS PlaceName,
 IF (PlaceAddress != '' || PlaceAddress != NULL, PlaceAddress, '' ) AS PlaceAddress,
--  haversineDistance(startLat, startLon, activity.Lat, activity.Lon, distanceType) AS Distance,
 IF (ak.ID is not null, 1, 0) as IsLike,
 0 as IsBlock,
 0 as IsBlockTarget,
 IF (fc.ID is not null, 1, 0) as IsReported,
 IF (u.FacebookID is not null, u.FacebookID, '') as FacebookID,
 IF (u.UserName is not null, u.UserName, '') as FromUserName, 
 '' as OwnerName, 
 IF (u2.UserName is not null, u2.UserName, '') as GiftReceiverName, 
 IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,
 if (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,
-- Hung added
 IF (temp.CheckinPlaceID is not null, temp.CheckinPlaceID, '') AS CheckinPlaceID,
 IF (temp.CheckinPlaceName is not null, temp.CheckinPlaceName, '') AS CheckinPlaceName,
 IF (temp.CheckinPlaceID is not null, 1, 0) AS CheckinStatus,
IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,

-- if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) AS NeighborhoodName
if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
		if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
)AS NeighborhoodName

FROM (select ActivityID, FromUserID, Message, Image, RefID, Type, ActionType, PlaceID, PlaceName, PlaceIcon, PlaceAddress, Lat, Lon, CreatedDate, IsInvalid, LastUpdate, Gender, BirthDate, OwnerID, OrginalDate, GiftReceiverID, CheckinPlaceID, CheckinPlaceName, NeighborhoodID,
			 haversineDistance(startLat, startLon, activity.Lat, activity.Lon, distanceType) AS Distance
			FROM activity
			WHERE Lat IS NOT NULL AND Lon IS NOT NULL AND FIND_IN_SET(ActionType, actionTypes) > 0 AND IsInvalid != 1
			AND Lon BETWEEN lon1 AND lon2 AND Lat BETWEEN lat1 AND lat2

			-- ANOMO-5958
			AND ((igender > 0 AND Gender=igender) OR igender <= 0)
			AND ((fromAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) >= fromAge) OR fromAge <=0)
			AND ((toAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) <= toAge) OR toAge <=0)

			AND ((iActivityID > 0 AND ActivityID < iActivityID) OR iActivityID <= 0) 
			--

			-- ANOMO-7364
			AND FromUserID NOT IN (select if(CurrentUserID = userId, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = userId or TargetUserID = userId)
			-- end ANOMO-7364

			HAVING Distance < dist ORDER BY CreatedDate DESC LIMIT offsetResults,limitResults
		) as temp 
LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=userId
-- left JOIN userblock ub1 ON ub1.TargetUserID=temp.FromUserID AND ub1.CurrentUserID=userId
-- left JOIN userblock ub2 ON ub2.CurrentUserID=temp.FromUserID AND ub2.TargetUserID=userId
LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=userId
LEFT JOIN `user` u ON u.UserID=temp.FromUserID
LEFT JOIN `user` u2 ON u2.UserID = temp.GiftReceiverID


LEFT JOIN userconnect cc ON cc.TargetUserID=temp.FromUserID AND cc.CurrentUserID = userId


-- ANOMO-7868 Ability to stop receiving notifications on specific threads
LEFT JOIN stopreceivenotify srn ON srn.UserID = userId AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType

-- Hung added...
 LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID;

-- get totalResult
IF prevTotal > 0 THEN
	SET totalResults = prevTotal ; -- - 12;
ELSE
SET totalResults = (select count(*) FROM (
			SELECT haversineDistance(startLat,startLon,activity.Lat,activity.Lon,distanceType) AS Distance
			FROM activity
			WHERE Lat IS NOT NULL AND Lon IS NOT NULL AND FIND_IN_SET(ActionType, actionTypes) > 0 AND IsInvalid != 1
			AND Lon BETWEEN lon1 AND lon2 AND Lat BETWEEN lat1 AND lat2

			-- ANOMO-5958
			AND ((igender > 0 AND Gender=igender) OR igender <= 0)
			AND ((fromAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) >= fromAge) OR fromAge <=0)
			AND ((toAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) <= toAge) OR toAge <=0)

			AND ((iActivityID > 0 AND ActivityID < iActivityID) OR iActivityID <= 0) 
			--

			-- ANOMO-7364
			AND FromUserID NOT IN (select if(CurrentUserID = userId, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = userId or TargetUserID = userId)
			-- end ANOMO-7364

			HAVING  Distance < dist ) as t

);
END IF;
-- end get totalResult

ELSE -- isMeridian180 != 0
SELECT temp.*,
 IF (u.Photo != '', CONCAT(photoUrl,u.Photo), '') AS Avatar,
 '' AS OwnerAvatar,
 IF (Image != '', CONCAT(photoUrl,Image), '') AS Image,
 IF (PlaceIcon != NULL || PlaceIcon != '', CONCAT(categoryIconUrl,PlaceIcon), '') AS PlaceIcon,
 IF (temp.PlaceID != NULL || temp.PlaceID != '', temp.PlaceID, '') AS PlaceID,
 IF (PlaceName != NULL || PlaceName != '', PlaceName, '') AS PlaceName,
 IF (PlaceAddress != '' || PlaceAddress != NULL, PlaceAddress, '' ) AS PlaceAddress,
 -- haversineDistance(startLat, startLon, activity.Lat, activity.Lon, distanceType) AS Distance,
 IF (ak.ID is not null, 1, 0) as IsLike,
 0 as IsBlock,
 0 as IsBlockTarget,
 IF (fc.ID is not null, 1, 0) as IsReported,
 IF (u.FacebookID is not null, u.FacebookID, '') as FacebookID,
 IF (u.UserName is not null, u.UserName, '') as FromUserName, 
 '' as OwnerName, 
 IF (u2.UserName is not null, u2.UserName, '') as GiftReceiverName, 
 IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,
 if (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,
-- Hung added
 IF (temp.CheckinPlaceID is not null, temp.CheckinPlaceID, '') AS CheckinPlaceID,
 IF (temp.CheckinPlaceName is not null, temp.CheckinPlaceName, '') AS CheckinPlaceName,
 IF (temp.CheckinPlaceID is not null, 1, 0) AS CheckinStatus,
IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,
 
-- if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) AS NeighborhoodName
if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
		if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
)AS NeighborhoodName

FROM (select ActivityID, FromUserID, Message, Image, RefID, Type, ActionType, PlaceID, PlaceName, PlaceIcon, PlaceAddress, Lat, Lon, CreatedDate, IsInvalid, LastUpdate, Gender, BirthDate, OwnerID, OrginalDate, GiftReceiverID, CheckinPlaceID, CheckinPlaceName, NeighborhoodID,
		haversineDistance(startLat, startLon, activity.Lat, activity.Lon, distanceType) AS Distance
		FROM activity
		WHERE Lat IS NOT NULL AND Lon IS NOT NULL AND FIND_IN_SET(ActionType, actionTypes) > 0 AND IsInvalid != 1
		AND ( (Lon BETWEEN lon1 AND lon2 AND Lat BETWEEN lat1 AND lat2) OR (Lon BETWEEN lon1_2 AND lon2_2 AND Lat BETWEEN lat1_2 AND lat2_2))

		-- ANOMO-5958
		AND ((igender > 0 AND Gender=igender) OR igender <= 0)
		AND ((fromAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) >= fromAge) OR fromAge <=0)
		AND ((toAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) <= toAge) OR toAge <=0)

		AND ((iActivityID > 0 AND ActivityID < iActivityID) OR iActivityID <= 0) 
		--

		-- ANOMO-7364
		AND FromUserID NOT IN (select if(CurrentUserID = userId, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = userId or TargetUserID = userId)
		-- end ANOMO-7364

		HAVING Distance < dist ORDER BY CreatedDate DESC LIMIT offsetResults,limitResults
		) as temp 
LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=userId
-- left JOIN userblock ub1 ON ub1.TargetUserID=temp.FromUserID AND ub1.CurrentUserID=userId
-- left JOIN userblock ub2 ON ub2.CurrentUserID=temp.FromUserID AND ub2.TargetUserID=userId
LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=userId
LEFT JOIN `user` u ON u.UserID=temp.FromUserID
LEFT JOIN `user` u2 ON u2.UserID = temp.GiftReceiverID
LEFT JOIN userconnect cc ON cc.TargetUserID=temp.FromUserID AND cc.CurrentUserID = userId

-- ANOMO-7868 Ability to stop receiving notifications on specific threads
LEFT JOIN stopreceivenotify srn ON srn.UserID = userId AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType

-- Hung added...
 LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID;

-- get totalResults
IF prevTotal > 0 THEN
	SET totalResults = prevTotal; -- - 12;
ELSE
SET totalResults = (select count(*) FROM (
		SELECT haversineDistance(startLat,startLon,activity.Lat,activity.Lon,distanceType) AS Distance
		FROM activity
		WHERE Lat IS NOT NULL AND Lon IS NOT NULL AND FIND_IN_SET(ActionType, actionTypes) > 0 AND IsInvalid != 1
		AND ( (Lon BETWEEN lon1 AND lon2 AND Lat BETWEEN lat1 AND lat2) OR (Lon BETWEEN lon1_2 AND lon2_2 AND Lat BETWEEN lat1_2 AND lat2_2))

		-- ANOMO-5958
		AND ((igender > 0 AND Gender=igender) OR igender <= 0)
		AND ((fromAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) >= fromAge) OR fromAge <=0)
		AND ((toAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) <= toAge) OR toAge <=0)

		AND ((iActivityID > 0 AND ActivityID < iActivityID) OR iActivityID <= 0) 
		--

		-- ANOMO-7364
		AND FromUserID NOT IN (select if(CurrentUserID = userId, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = userId or TargetUserID = userId)
		-- end ANOMO-7364

		HAVING Distance < dist ) as t
);
END IF;

-- end get totalResults


END IF;

SET radiusOut = dist;
-- SET totalResults = FOUND_ROWS();

ELSE -- ****** radius was not provided so find it ******
SET loopCounter = 1;
SET loopMaxCounter = distancesArrayLength + 1;

REPEAT
SET distTemp = SUBSTRING_INDEX(distancesArray,',',loopCounter);
SET dist = SUBSTRING_INDEX(distTemp,',',-1);

SET lon1 = startLon - dist / abs(cos(radians(startLat)) * distanceFactor);
SET lon2 = startLon + dist / abs(cos(radians(startLat)) * distanceFactor); 
SET lat1 = startLat - (dist / distanceFactor);
SET lat2 = startLat + (dist / distanceFactor);

SET isMeridian180 = 0;
IF lon1 < -180 THEN
SET isMeridian180 = 1;

SET lon1_2 = 360 + lon1; 
SET lon2_2 = 179.999;
SET lat1_2 = lat1;
SET lat2_2 = lat2;

SET lon1 = -179.999;
ELSEIF lon2 > 180 THEN
SET isMeridian180 = 1;

SET lon1_2 = -179.999;
SET lon2_2 = -360 + lon2;
SET lat1_2 = lat1;
SET lat2_2 = lat2;

SET lon2 = 179.999;
END IF;

IF isMeridian180 = 0 THEN
SET queryCount = (SELECT COUNT(*) FROM (SELECT haversineDistance(startLat,startLon,activity.Lat,activity.Lon,distanceType) AS Distance
FROM activity
WHERE FromUserID != userId AND Lat IS NOT NULL AND Lon IS NOT NULL AND FIND_IN_SET(ActionType, actionTypes) > 0 AND IsInvalid != 1 AND CreatedDate >= DATE_SUB(NOW(), INTERVAL hoursSearch HOUR)
AND Lon BETWEEN lon1 AND lon2 AND Lat BETWEEN lat1 AND lat2
-- ANOMO-5958
AND ((igender > 0 AND Gender=igender) OR igender <= 0)
AND ((fromAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) >= fromAge) OR fromAge <=0)
AND ((toAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) <= toAge) OR toAge <=0)

AND ((iActivityID > 0 AND ActivityID < iActivityID) OR iActivityID <= 0) 

--

-- ANOMO-7364
AND FromUserID NOT IN (select if(CurrentUserID = userId, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = userId or TargetUserID = userId)
-- end ANOMO-7364

HAVING Distance < dist ORDER BY CreatedDate DESC LIMIT offsetResults, cutOffVal) AS t);
ELSE -- isMeridian180 != 0
SET queryCount = (SELECT COUNT(*) FROM (SELECT haversineDistance(startLat,startLon,activity.Lat,activity.Lon,distanceType) AS Distance
FROM activity
WHERE FromUserID != userId AND Lat IS NOT NULL AND Lon IS NOT NULL AND FIND_IN_SET(ActionType, actionTypes) > 0 AND IsInvalid != 1 AND CreatedDate >= DATE_SUB(NOW(), INTERVAL hoursSearch HOUR)
AND ( (Lon BETWEEN lon1 AND lon2 AND Lat BETWEEN lat1 AND lat2) OR (Lon BETWEEN lon1_2 AND lon2_2 AND Lat BETWEEN lat1_2 AND lat2_2))
-- ANOMO-5958
AND ((igender > 0 AND Gender=igender) OR igender <= 0)
AND ((fromAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) >= fromAge) OR fromAge <=0)
AND ((toAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) <= toAge) OR toAge <=0)

AND ((iActivityID > 0 AND ActivityID < iActivityID) OR iActivityID <= 0) 
--

-- ANOMO-7364
AND FromUserID NOT IN (select if(CurrentUserID = userId, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = userId or TargetUserID = userId)
-- end ANOMO-7364

HAVING Distance < dist ORDER BY CreatedDate DESC LIMIT offsetResults, cutOffVal) AS t);
END IF;

SET loopCounter = loopCounter + 1;

UNTIL queryCount >= cutOffVal || loopCounter >= loopMaxCounter
END REPEAT;

-- radius was found so use it in the query
IF isMeridian180 = 0 THEN
SELECT temp.*, 
 IF (u.Photo != '', CONCAT(photoUrl,u.Photo), '') AS Avatar,
 '' AS OwnerAvatar,
 IF (Image != '', CONCAT(photoUrl,Image), '') AS Image,
 IF (PlaceIcon != NULL || PlaceIcon != '', CONCAT(categoryIconUrl,PlaceIcon), '') AS PlaceIcon,
 IF (temp.PlaceID != NULL || temp.PlaceID != '', temp.PlaceID, '') AS PlaceID,
 IF (PlaceName != NULL || PlaceName != '', PlaceName, '') AS PlaceName,
 IF (PlaceAddress != '' || PlaceAddress != NULL, PlaceAddress, '' ) AS PlaceAddress,
 -- haversineDistance(startLat, startLon, activity.Lat, activity.Lon, distanceType) AS Distance,
 IF (ak.ID is not null, 1, 0) as IsLike,
0 as IsBlock,
0 as IsBlockTarget,
 IF (fc.ID is not null, 1, 0) as IsReported,
 IF (u.FacebookID is not null, u.FacebookID, '') as FacebookID,
 IF (u.UserName is not null, u.UserName, '') as FromUserName, 
 '' as OwnerName, 
 IF (u2.UserName is not null, u2.UserName, '') as GiftReceiverName, 
 IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,
 if (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,
-- Hung added
 IF (temp.CheckinPlaceID is not null, temp.CheckinPlaceID, '') AS CheckinPlaceID,
 IF (temp.CheckinPlaceName is not null, temp.CheckinPlaceName, '') AS CheckinPlaceName,
 IF (temp.CheckinPlaceID is not null, 1, 0) AS CheckinStatus,
IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,
 
-- if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) AS NeighborhoodName
if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
		if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
)AS NeighborhoodName
FROM (
		select ActivityID, FromUserID, Message, Image, RefID, Type, ActionType, PlaceID, PlaceName, PlaceIcon, PlaceAddress, Lat, Lon, CreatedDate, IsInvalid, LastUpdate, Gender, BirthDate, OwnerID, OrginalDate, GiftReceiverID, CheckinPlaceID, CheckinPlaceName, NeighborhoodID,
		haversineDistance(startLat, startLon, activity.Lat, activity.Lon, distanceType) AS Distance
		FROM activity
		WHERE Lat IS NOT NULL AND Lon IS NOT NULL AND FIND_IN_SET(ActionType, actionTypes) > 0 AND IsInvalid != 1 AND CreatedDate >= DATE_SUB(NOW(), INTERVAL hoursSearch HOUR)
		AND Lon BETWEEN lon1 AND lon2 AND Lat BETWEEN lat1 AND lat2
		-- ANOMO-5958
		AND ((igender > 0 AND Gender=igender) OR igender <= 0)
		AND ((fromAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) >= fromAge) OR fromAge <=0)
		AND ((toAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) <= toAge) OR toAge <=0)

		AND ((iActivityID > 0 AND ActivityID < iActivityID) OR iActivityID <= 0) 
		--

		-- ANOMO-7364
		AND FromUserID NOT IN (select if(CurrentUserID = userId, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = userId or TargetUserID = userId)
		-- end ANOMO-7364

		HAVING Distance < dist ORDER BY CreatedDate DESC LIMIT offsetResults,limitResults
	) as temp
LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=userId
-- left JOIN userblock ub1 ON ub1.TargetUserID=temp.FromUserID AND ub1.CurrentUserID=userId
-- left JOIN userblock ub2 ON ub2.CurrentUserID=temp.FromUserID AND ub2.TargetUserID=userId
LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=userId
LEFT JOIN `user` u ON u.UserID=temp.FromUserID
LEFT JOIN `user` u2 ON u2.UserID = temp.GiftReceiverID

LEFT JOIN userconnect cc ON cc.TargetUserID=temp.FromUserID AND cc.CurrentUserID = userId
-- ANOMO-7868 Ability to stop receiving notifications on specific threads
LEFT JOIN stopreceivenotify srn ON srn.UserID = userId AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType

-- Hung added...
 LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID;

-- get totalResults
IF prevTotal > 0 THEN
	SET totalResults = prevTotal; -- - 12;
ELSE
SET totalResults = (select count(*) FROM (
		SELECT haversineDistance(startLat,startLon,activity.Lat,activity.Lon,distanceType) AS Distance
		FROM activity
		WHERE Lat IS NOT NULL AND Lon IS NOT NULL AND FIND_IN_SET(ActionType, actionTypes) > 0 AND IsInvalid != 1 AND CreatedDate >= DATE_SUB(NOW(), INTERVAL hoursSearch HOUR)
		AND Lon BETWEEN lon1 AND lon2 AND Lat BETWEEN lat1 AND lat2
		-- ANOMO-5958
		AND ((igender > 0 AND Gender=igender) OR igender <= 0)
		AND ((fromAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) >= fromAge) OR fromAge <=0)
		AND ((toAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) <= toAge) OR toAge <=0)

		AND ((iActivityID > 0 AND ActivityID < iActivityID) OR iActivityID <= 0) 
		--

		-- ANOMO-7364
		AND FromUserID NOT IN (select if(CurrentUserID = userId, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = userId or TargetUserID = userId)
		-- end ANOMO-7364

		HAVING Distance < dist ) as t
);
END IF;
-- end get totalResults


ELSE -- isMeridian180 != 0
SELECT temp.*,
 IF (u.Photo != '', CONCAT(photoUrl,u.Photo), '') AS Avatar,
 '' AS OwnerAvatar,
 IF (Image != '', CONCAT(photoUrl,Image), '') AS Image,
 IF (PlaceIcon != NULL || PlaceIcon != '', CONCAT(categoryIconUrl,PlaceIcon), '') AS PlaceIcon,
 IF (temp.PlaceID != NULL || temp.PlaceID != '', temp.PlaceID, '') AS PlaceID,
 IF (PlaceName != NULL || PlaceName != '', PlaceName, '') AS PlaceName,
 IF (PlaceAddress != '' || PlaceAddress != NULL, PlaceAddress, '' ) AS PlaceAddress,
 -- haversineDistance(startLat, startLon, activity.Lat, activity.Lon, distanceType) AS Distance,
 IF (ak.ID is not null, 1, 0) as IsLike,
 0 as IsBlock,
 0 as IsBlockTarget,
 IF (fc.ID is not null, 1, 0) as IsReported,
 IF (u.FacebookID is not null, u.FacebookID, '') as FacebookID,
 IF (u.UserName is not null, u.UserName, '') as FromUserName, 
 '' as OwnerName, 
 IF (u2.UserName is not null, u2.UserName, '') as GiftReceiverName, 
 IF (srn.ID is not null, 1, 0) as IsStopReceiveNotify,
 if (cc.IsFavorite is not null, cc.IsFavorite, 0) as IsFavorite,
-- Hung added
IF (temp.CheckinPlaceID is not null, temp.CheckinPlaceID, '') AS CheckinPlaceID,
 IF (temp.CheckinPlaceName is not null, temp.CheckinPlaceName, '') AS CheckinPlaceName,
 IF (temp.CheckinPlaceID is not null, 1, 0) AS CheckinStatus,
IF (temp.NeighborhoodID is not null,  temp.NeighborhoodID, '') AS NeighborhoodID,

-- if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) AS NeighborhoodName
if (u.OnlyShowCountry = 1, if (nbh.COUNTY = 'United States' && nbh.STATE is  not null, nbh.STATE, nbh.COUNTY ), 
		if (nbh.`NAME` is not null && nbh.NAME != '', if(nbh.STATE is not null && nbh.STATE != '' && nbh.NAME != nbh.STATE, concat(nbh.NAME, ', ', nbh.STATE), if (nbh.COUNTY is not null && nbh.COUNTY != nbh.NAME, concat(nbh.NAME, ', ', nbh.COUNTY), NAME)), if (nbh.COUNTY is not null, nbh.COUNTY, '')) 
)AS NeighborhoodName
FROM (
		select ActivityID, FromUserID, Message, Image, RefID, Type, ActionType, PlaceID, PlaceName, PlaceIcon, PlaceAddress, Lat, Lon, CreatedDate, IsInvalid, LastUpdate, Gender, BirthDate, OwnerID, OrginalDate, GiftReceiverID, CheckinPlaceID, CheckinPlaceName, NeighborhoodID,
		haversineDistance(startLat, startLon, activity.Lat, activity.Lon, distanceType) AS Distance
		FROM activity
		WHERE Lat IS NOT NULL AND Lon IS NOT NULL AND FIND_IN_SET(ActionType, actionTypes) > 0 AND IsInvalid != 1 AND CreatedDate >= DATE_SUB(NOW(), INTERVAL hoursSearch HOUR)
		AND ( (Lon BETWEEN lon1 AND lon2 AND Lat BETWEEN lat1 AND lat2) OR (Lon BETWEEN lon1_2 AND lon2_2 AND Lat BETWEEN lat1_2 AND lat2_2))
		-- ANOMO-5958
		AND ((igender > 0 AND Gender=igender) OR igender <= 0)
		AND ((fromAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) >= fromAge) OR fromAge <=0)
		AND ((toAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) <= toAge) OR toAge <=0)

		AND ((iActivityID > 0 AND ActivityID < iActivityID) OR iActivityID <= 0) 
		--

		-- ANOMO-7364
		AND FromUserID NOT IN (select if(CurrentUserID = userId, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = userId or TargetUserID = userId)
		-- end ANOMO-7364

		HAVING Distance < dist ORDER BY CreatedDate DESC LIMIT offsetResults,limitResults
	) as temp 
LEFT JOIN activitylike ak ON ak.ContentID=temp.RefID AND ak.Type=temp.ActionType AND ak.UserID=userId
-- left JOIN userblock ub1 ON ub1.TargetUserID=temp.FromUserID AND ub1.CurrentUserID=userId
-- left JOIN userblock ub2 ON ub2.CurrentUserID=temp.FromUserID AND ub2.TargetUserID=userId
LEFT JOIN flagcontent fc ON fc.ContentID=temp.RefID AND fc.Type=temp.ActionType AND fc.UserID=userId
LEFT JOIN `user` u ON u.UserID=temp.FromUserID
LEFT JOIN `user` u2 ON u2.UserID = temp.GiftReceiverID
LEFT JOIN userconnect cc ON cc.TargetUserID=temp.FromUserID AND cc.CurrentUserID = userId

-- ANOMO-7868 Ability to stop receiving notifications on specific threads
LEFT JOIN stopreceivenotify srn ON srn.UserID = userId AND srn.ContentID = temp.RefID AND srn.Type = temp.ActionType

-- Hung added...
 LEFT JOIN neighborhood nbh ON temp.NeighborhoodID is not null AND temp.NeighborhoodID = nbh.OGR_FID;

-- get totalResults
IF prevTotal > 0 THEN
	SET totalResults = prevTotal; --  - 12;
ELSE
SET totalResults = (select count(*) FROM (
		SELECT haversineDistance(startLat,startLon,activity.Lat,activity.Lon,distanceType) AS Distance
		FROM activity
		WHERE Lat IS NOT NULL AND Lon IS NOT NULL AND FIND_IN_SET(ActionType, actionTypes) > 0 AND IsInvalid != 1 AND CreatedDate >= DATE_SUB(NOW(), INTERVAL hoursSearch HOUR)
		AND ( (Lon BETWEEN lon1 AND lon2 AND Lat BETWEEN lat1 AND lat2) OR (Lon BETWEEN lon1_2 AND lon2_2 AND Lat BETWEEN lat1_2 AND lat2_2))
		-- ANOMO-5958
		AND ((igender > 0 AND Gender=igender) OR igender <= 0)
		AND ((fromAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) >= fromAge) OR fromAge <=0)
		AND ((toAge > 0 AND YEAR(CURDATE()) - YEAR(BirthDate) <= toAge) OR toAge <=0)

		AND ((iActivityID > 0 AND ActivityID < iActivityID) OR iActivityID <= 0) 
		--

		-- ANOMO-7364
		AND FromUserID NOT IN (select if(CurrentUserID = userId, TargetUserID, CurrentUserID) as uID from userblock where CurrentUserID = userId or TargetUserID = userId)
		-- end ANOMO-7364

		HAVING Distance < dist ) as t
);
END IF;
-- end get totalResults

END IF;

SET radiusOut = dist;
-- SET totalResults = FOUND_ROWS();
END IF; -- ****** radius was provided so use it - end if ******

END